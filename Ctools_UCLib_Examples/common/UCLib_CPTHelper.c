/**
 *   @file  UCLib_CPTHelper.c
 *
 *   @brief
 *      Provides APIs to open, start, end and close CP Tracer specific use case scenarios.
 *
 *      For more details refer to ctools use case library Doxygen API reference
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2014 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "UCLib_CPTHelper.h"
#include "cToolsHelper.h"

#if defined(C6670)
#include "DeviceSpecific_C6670.h"
#elif defined(C6657)
#include "DeviceSpecific_C6657.h"
#elif defined(C6678)
#include "DeviceSpecific_C6678.h"
#elif defined(TCI6614)
#include "DeviceSpecific_C6614.h"
#elif defined(_66AK2Gxx)
#include "DeviceSpecific_66AK2Gxx.h"
#elif defined(C66AK2Hxx) || defined(TCI6630K2L) || defined(C66AK2Exx)
#include "DeviceSpecific_C66AK2Hxx.h"
#endif

#if defined(C66AK2Hxx)
    eCPT_ModID DDR_CPT_ModID = eCPT_DDR3A;
#else
    eCPT_ModID DDR_CPT_ModID = eCPT_DDR;
#endif

#pragma DATA_SECTION(etb_drain_mem, ".app_etb_drain_mem")
#define ETB_SIZE_BYTES 32768
#define ETB_EXTENDED_SIZE_BYTES    (ETB_SIZE_BYTES*4)

#ifdef UCLIB_ETB_EDMA
uint8_t etb_drain_mem[ETB_EXTENDED_SIZE_BYTES];
#else
uint8_t etb_drain_mem[ETB_SIZE_BYTES];
#endif

#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

/*******************************************************************************/
/* Function to extract and export DCM data */
/*******************************************************************************/
int transferDCMdata(uint8_t wrapped)
{
    STM_DCM_InfoObj  DCM_Info;
	char * pDCMFileName;
	FILE* DCM_fp;
	ctools_Result ctools_ret = CTOOLS_SOK;

#if defined(SYS_BANDWIDTH_PROFILE) || defined(SYS_LATENCY_PROFILE)

	pDCMFileName = "C:\\temp\\cpt_sp_etbdata.dcm";

#endif

#ifdef TOTAL_BANDWIDTH_PROFILE

	pDCMFileName = "C:\\temp\\cpt_tp_etbdata.dcm";

#endif

#ifdef MASTER_BANDWIDTH_PROFILE

	pDCMFileName = "C:\\temp\\cpt_mp_etbdata.dcm";

#endif

#ifdef EVENT_PROFILE

	pDCMFileName = "C:\\temp\\cpt_ep_etbdata.dcm";

#endif

	DCM_fp = fopen(pDCMFileName, "wb");
	if(DCM_fp)
	{
	    ctools_ret = ctools_systemtrace_getdcmdata(&DCM_Info);
	    if(ctools_ret != CTOOLS_SOK)
	    {
	        printf ("Error: Get STM DCM data failed %d\n", ctools_ret);
	        return(-1);
	    }

		fprintf(DCM_fp, "STM_data_flip=%d\n", DCM_Info.stm_data_flip);
	    fprintf(DCM_fp, "STM_Buffer_Wrapped=%d\n", wrapped);
	    fprintf(DCM_fp, "HEAD_Present_0=%d\n", DCM_Info.atb_head_present_0);
	    fprintf(DCM_fp, "HEAD_Pointer_0=%d\n", DCM_Info.atb_head_pointer_0);
	    fprintf(DCM_fp, "HEAD_Present_1=%d\n", DCM_Info.atb_head_present_1);
	    fprintf(DCM_fp, "HEAD_Pointer_1=%d\n", DCM_Info.atb_head_pointer_1);
	    fprintf(DCM_fp, "STM_STP_Version=%d\n",DCM_Info.stpVersion);
	    fprintf(DCM_fp, "TWP_Protocol=%d\n", ETB_USING_TWP);

		printf("Successfully exported CPT DCM data - %s\n", pDCMFileName);

		fclose(DCM_fp);
	}
	else
	{
		printf("Error opening file - %s\n", pDCMFileName);
		return(-1);
	}

	//success
	return(0);
}

#endif

// Application SW functions
int SystemMemory_Profile_Start(void)
{
	ctools_Result ctools_ret = CTOOLS_SOK;

#ifdef UCLIB_ETB_CPU

    ctools_etb_config_t etb_config = {0};
    uint32_t idx;
    etb_config.etb_mode = eETB_TI_Mode;

	/* Clear the drain memory */
	for (idx=0; idx<ETB_SIZE_BYTES; idx++)
	{
		etb_drain_mem[idx] = 0xCC;
	}

	ctools_ret = ctools_etb_init(CTOOLS_DRAIN_ETB_CPU, &etb_config, CTOOLS_SYS_ETB);

#endif

#ifdef UCLIB_ETB_EDMA

	uint32_t idx;
	/* Clear the drain memory */
	for (idx=0; idx<ETB_EXTENDED_SIZE_BYTES; idx++)
	{
		etb_drain_mem[idx] = 0xCC;
	}

	ctools_ret = UCLib_Config_EDMA_for_SYSETB((uint32_t)&etb_drain_mem, (ETB_EXTENDED_SIZE_BYTES/4));

#endif


#ifdef UCLIB_NO_ETB

	ctools_ret = ctools_etb_init(CTOOLS_DRAIN_NONE, &etb_config, CTOOLS_SYS_ETB);

#endif

    if(ctools_ret != CTOOLS_SOK)
    {
	    printf (" ETB initialization failed %d\n", ctools_ret);
		return(-1);
    }

    ctools_ret = ctools_systemtrace_init(CTOOLS_SYS_TRACE_ALL);
    if(ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: System Trace init Failed %d\n", ctools_ret);
        return(-1);
    }


#if defined(SYS_BANDWIDTH_PROFILE) || defined(SYS_LATENCY_PROFILE)

    ctools_cpt_sysprofilecfg SysMemory_Profile_Params;

    memset(((void*)&SysMemory_Profile_Params), 0, sizeof(ctools_cpt_sysprofilecfg));

#if defined(_66AK2Gxx)

    ctools_cpt_modidqual SysMemory_ModIDQual[2];

    int i;

    for(i=0;i<2;i++)
    {
        SysMemory_ModIDQual[i].TPCntQual = NULL;
    }

    SysMemory_ModIDQual[0].CPT_ModId = eCPT_MSMC_0;
    SysMemory_ModIDQual[1].CPT_ModId = DDR_CPT_ModID;

    //Populate SysMemory Profiler Params
    SysMemory_Profile_Params.CPT_ModCnt = 2;

#else

    ctools_cpt_modidqual SysMemory_ModIDQual[5];

    int i;

    for(i=0;i<5;i++)
    {
        SysMemory_ModIDQual[i].TPCntQual = NULL;
    }

    SysMemory_ModIDQual[0].CPT_ModId = eCPT_MSMC_0;
    SysMemory_ModIDQual[1].CPT_ModId = eCPT_MSMC_1;
    SysMemory_ModIDQual[2].CPT_ModId = eCPT_MSMC_2;
    SysMemory_ModIDQual[3].CPT_ModId = eCPT_MSMC_3;
    SysMemory_ModIDQual[4].CPT_ModId = DDR_CPT_ModID;

    //Populate SysMemory Profiler Params
    SysMemory_Profile_Params.CPT_ModCnt = 5;

#endif

    SysMemory_Profile_Params.ModIDQual = SysMemory_ModIDQual;
    SysMemory_Profile_Params.CPUClockRateMhz = 983;
    SysMemory_Profile_Params.SampleWindowSize = 81916;

#if defined(SYS_BANDWIDTH_PROFILE)

    // Open System memory (MSMC and DDR3) profiler
    ctools_ret = ctools_cpt_sysbwprofile_open(&SysMemory_Profile_Params);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_sysbandwidthprofile_open Failed %d\n", ctools_ret);
        return(-1);
    }

#elif defined(SYS_LATENCY_PROFILE)

    // Open System memory (MSMC and DDR3) profiler
    ctools_ret = ctools_cpt_syslatprofile_open(&SysMemory_Profile_Params);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_syslatencyprofile_open Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

    // Start System memory (MSMC and DDR3) profiler
    ctools_ret = ctools_cpt_globalstart();
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstart Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef TOTAL_BANDWIDTH_PROFILE

    ctools_cpt_totalprofilecfg Total_Profile_Params;
    ctools_cpt_masteridfiltercfg  MstID_Cfg;

    memset(((void*)&Total_Profile_Params), 0, sizeof(ctools_cpt_totalprofilecfg));
    memset(((void*)&MstID_Cfg), 0, sizeof(ctools_cpt_masteridfiltercfg));

    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM0};

    Total_Profile_Params.CPT_ModId = DDR_CPT_ModID; //Profiling accesses to DDR3 memory
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    MstID_Cfg.CPT_MasterIDCnt = 1;
    Total_Profile_Params.TPCnt_MasterID = &MstID_Cfg;
    Total_Profile_Params.TPCntQual = NULL;
    Total_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Total_Profile_Params.CPUClockRateMhz = 983;
    Total_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Total bandwidth profiler
    ctools_ret = ctools_cpt_totalprofile_open(&Total_Profile_Params);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_totalprofile_open failed %d\n", ctools_ret);
        return(-1);
    }

    // Start DDR3 Total bandwidth profiler
    ctools_ret = ctools_cpt_singlestart(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singlestart Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef MASTER_BANDWIDTH_PROFILE

    ctools_cpt_masterprofilecfg Master_Profile_Params;
    ctools_cpt_masteridfiltercfg  MstID0_Cfg;
    ctools_cpt_masteridfiltercfg  MstID1_Cfg;

    memset(((void*)&Master_Profile_Params), 0, sizeof(ctools_cpt_masterprofilecfg));
    memset(((void*)&MstID0_Cfg), 0, sizeof(ctools_cpt_masteridfiltercfg));
    memset(((void*)&MstID1_Cfg), 0, sizeof(ctools_cpt_masteridfiltercfg));

    eCPT_MasterID MstID0_Array[] = {eCPT_MID_GEM0}; // CPU masters

#if defined (_66AK2Gxx)

    eCPT_MasterID MstID1_Array[] = {eCPT_MID_EDMA0_TC0_RD, eCPT_MID_EDMA0_TC0_WR, eCPT_MID_EDMA0_TC1_RD, eCPT_MID_EDMA0_TC1_WR,
    		                        eCPT_MID_EDMA1_TC0_RD, eCPT_MID_EDMA1_TC0_WR, eCPT_MID_EDMA1_TC1_RD, eCPT_MID_EDMA1_TC1_WR}; // EDMA masters

#else

    eCPT_MasterID MstID1_Array[] = {eCPT_MID_EDMA2_TC0_RD, eCPT_MID_EDMA2_TC0_WR, eCPT_MID_EDMA2_TC1_RD, eCPT_MID_EDMA2_TC1_WR,
    		                        eCPT_MID_EDMA2_TC2_RD, eCPT_MID_EDMA2_TC2_WR, eCPT_MID_EDMA2_TC3_RD, eCPT_MID_EDMA2_TC3_WR}; // EDMA masters

#endif

    Master_Profile_Params.CPT_ModId = DDR_CPT_ModID; //Profiling accesses to DDR3 memory
    MstID0_Cfg.CPT_MasterID = MstID0_Array;
    MstID0_Cfg.MasterIDgroup_Enable = 0;
    MstID1_Cfg.CPT_MasterID = MstID1_Array;
    MstID1_Cfg.MasterIDgroup_Enable = 0;
    MstID0_Cfg.CPT_MasterIDCnt = 1;
    MstID1_Cfg.CPT_MasterIDCnt = 8;
    Master_Profile_Params.TPCnt0_MasterID = &MstID0_Cfg;
    Master_Profile_Params.TPCnt1_MasterID = &MstID1_Cfg;
    Master_Profile_Params.TPCnt0Qual = NULL;
    Master_Profile_Params.TPCnt1Qual = NULL;
    Master_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Master_Profile_Params.CPUClockRateMhz = 983;
    Master_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Master bandwidth profiler
    ctools_ret = ctools_cpt_masterprofile_open(&Master_Profile_Params);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_masterprofile_open failed %d\n", ctools_ret);
        return(-1);
    }

    // Start DDR3 Master bandwidth profiler
    ctools_ret = ctools_cpt_singlestart(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singlestart Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef EVENT_PROFILE

    ctools_cpt_eventtracecfg EventTrace_Params;
    ctools_cpt_masteridfiltercfg  MstID_Cfg;
    ctools_cpt_addressfiltercfg AddrFilter_Cfg;

    memset(((void*)&EventTrace_Params), 0, sizeof(ctools_cpt_eventtracecfg));
    memset(((void*)&MstID_Cfg), 0, sizeof(ctools_cpt_masteridfiltercfg));
    memset(((void*)&AddrFilter_Cfg), 0, sizeof(ctools_cpt_addressfiltercfg));

    //Put a SoC level watchpoint on 0x80100000-0x80100004
    //In keystone devices, by default 0x8000_0000 to 0x8FFF_FFFF is mapped to 0x8_0000_0000 to 0x8_7FFF_FFFF
    AddrFilter_Cfg.AddrFilterMSBs = 0x8;
    AddrFilter_Cfg.CPT_FilterMode = eCPT_FilterMode_Inclusive;
    AddrFilter_Cfg.EndAddrFilterLSBs = 0x00100004;
    AddrFilter_Cfg.StartAddrFilterLSBs = 0x00100000;

#if defined (_66AK2Gxx)

    eCPT_MasterID MstID_Array[] = {eCPT_MID_EDMA0_TC0_RD, eCPT_MID_EDMA0_TC0_WR, eCPT_MID_EDMA0_TC1_RD, eCPT_MID_EDMA0_TC1_WR,
    		                        eCPT_MID_EDMA1_TC0_RD, eCPT_MID_EDMA1_TC0_WR, eCPT_MID_EDMA1_TC1_RD, eCPT_MID_EDMA1_TC1_WR}; // EDMA masters

#else

    eCPT_MasterID MstID_Array[] = {eCPT_MID_EDMA2_TC0_RD, eCPT_MID_EDMA2_TC0_WR, eCPT_MID_EDMA2_TC1_RD, eCPT_MID_EDMA2_TC1_WR,
    		                        eCPT_MID_EDMA2_TC2_RD, eCPT_MID_EDMA2_TC2_WR, eCPT_MID_EDMA2_TC3_RD, eCPT_MID_EDMA2_TC3_WR}; // EDMA masters

#endif

    EventTrace_Params.CPT_ModId = DDR_CPT_ModID; //Capturing New Request event @ DDR3 memory slave
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    MstID_Cfg.CPT_MasterIDCnt = 8;
    EventTrace_Params.EventTrace_MasterID = &MstID_Cfg;
    EventTrace_Params.TPEventQual = NULL;

    EventTrace_Params.Address_Filter_Params = &AddrFilter_Cfg;
    EventTrace_Params.AddrExportMask = 0; // Address Bits 10:0 are exported

    // Open DDR3 New request event capture
    ctools_ret = ctools_cpt_eventtrace_open(&EventTrace_Params);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: New Request event capture open Failed %d\n", ctools_ret);
        return(-1);
    }

    // Start DDR3 New Request event profiler
    ctools_ret = ctools_cpt_singlestart(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: New Request event ctools_cpt_singlestart Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

    //success
    return(0);
}


int SystemMemory_Profile_Stop(void)
{

	ctools_Result ctools_ret = CTOOLS_SOK;

#if defined(SYS_BANDWIDTH_PROFILE) || defined(SYS_LATENCY_PROFILE)

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

	char * systrace_fname = "C:\\temp\\cpt_sp_etbdata.bin";

#endif

    // Stop System memory (MSMC and DDR3) profiler
	ctools_ret = ctools_cpt_globalstop();
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstop Failed %d\n",ctools_ret);
        return(-1);
    }

    // Close System memory (MSMC and DDR3) profiler
    ctools_ret = ctools_cpt_globalclose();
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalclose Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef TOTAL_BANDWIDTH_PROFILE

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

	char * systrace_fname = "C:\\temp\\cpt_tp_etbdata.bin";

#endif

    // Stop DDR3 Total bandwidth profiler
    ctools_ret = ctools_cpt_singlestop(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singlestop Failed %d\n", ctools_ret);
        return(-1);
    }

    // Close DDR3 Total bandwidth profiler
    ctools_ret = ctools_cpt_singleclose(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Total Bandwidth ctools_cpt_singleclose Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef MASTER_BANDWIDTH_PROFILE

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

	char * systrace_fname = "C:\\temp\\cpt_mp_etbdata.bin";

#endif

    // Stop DDR3 Master bandwidth profiler
    ctools_ret = ctools_cpt_singlestop(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singlestop Failed %d\n", ctools_ret);
        return(-1);
    }

    // Close DDR3 Master bandwidth profiler
    ctools_ret = ctools_cpt_singleclose(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: Master Bandwidth ctools_cpt_singleclose Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#ifdef EVENT_PROFILE

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

	char * systrace_fname = "C:\\temp\\cpt_ep_etbdata.bin";

#endif

    // Stop DDR3 New request event profiler
    ctools_ret = ctools_cpt_singlestop(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: New request event ctools_cpt_singlestop Failed %d\n", ctools_ret);
        return(-1);
    }

    // Close DDR3 New request event profiler
    ctools_ret = ctools_cpt_singleclose(DDR_CPT_ModID);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: New request event ctools_cpt_singleclose Failed %d\n", ctools_ret);
        return(-1);
    }

#endif

#if defined(UCLIB_ETB_EDMA) || defined(UCLIB_ETB_CPU)

    uint32_t*   pBuffer = 0;

#if defined(UCLIB_ETB_EDMA)

	ctools_edma_result_t pct_edma_res;

	/* This would copy extended ETB buffer as a linear buffer
	 * containing PC and timing traces to the memory provided here */
	ctools_ret = ctools_etb_edma_drain(&pct_edma_res, CTOOLS_SYS_ETB);

	if(ctools_ret == CTOOLS_SOK)
	{
		printf("EDMA ETB drain successful\n");

		if(pct_edma_res.isWrapped == 1)
		{
			printf("Buffer wrapped Start: Addr - 0x%08x and Available words - %d\n", pct_edma_res.startAddr, pct_edma_res.availableWords);
		}
		else
		{
			printf("Buffer NOT wrapped: Start Addr - 0x%08x and Available words - %d\n", pct_edma_res.startAddr, pct_edma_res.availableWords);
		}
	}
	else
	{
		printf("EDMA ETB drain failed %d\n", ctools_ret);
		return(-1);
	}

#elif defined(UCLIB_ETB_CPU)
	uint32_t size_out, wrap_flag;

	/* This would copy ETB hardware buffer as a linear buffer
	 * containing PC and timing traces to the memory provided here */
	ctools_ret = ctools_etb_cpu_drain(&etb_drain_mem, 32768, &size_out, &wrap_flag, CTOOLS_SYS_ETB);

	if(ctools_ret == CTOOLS_SOK)
	{
		printf("CPU ETB drain successful\n");

		if(wrap_flag == 1)
		{
			printf("Buffer wrapped and Available words - %d\n", size_out);
		}
		else
		{
			printf("Buffer NOT wrapped and Available words - %d\n", size_out);
		}
	}
	else
	{
		printf("CPU ETB drain failed %d\n", ctools_ret);
		return(-1);
	}
#endif

	/* Transport the ETB data captured */
	{
		/* this example uses JTAG debugger via CIO to transport data to host PC */
		/* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
		FILE* fp = fopen(systrace_fname, "wb");
		if(fp)
		{
			uint32_t sz = 0;
			uint32_t i = 1;
   			char *le = (char *) &i;
   			size_t fret;
   			uint32_t etbword;

#if defined(UCLIB_ETB_EDMA)
   			uint32_t  cnt;
   			uint32_t totalSize = 0;

            /* For manual ETB drain buffer read mode, set the buffer ending
             *  address to know when the buffer has wrapped back to the
             *  beginning.
             */
            uint32_t endAddress = pct_edma_res.dbufAddress + (pct_edma_res.dbufWords*4) - 1;
            totalSize = pct_edma_res.availableWords;
            pBuffer = (uint32_t *)pct_edma_res.startAddr;

            sz = 0;
            for(cnt = 0; cnt < totalSize; cnt++)
            {
                etbword = *(pBuffer+sz);
                if(le[0] != 1) //Big endian
                   etbword = BYTE_SWAP32(etbword);

                fret = fwrite((void*) &etbword, 4, 1, fp);
                if ( fret < 1 ) {
                	printf("Error writing data to  - %s \n", systrace_fname);
                	exit (EXIT_FAILURE);
                }
                sz++;

                /* Check for wrap condition with circular buffer */
                if((uint32_t)(pBuffer+sz) > endAddress)
                {
                    pBuffer = (uint32_t *)pct_edma_res.dbufAddress;
                    sz = 0;
                }
            }

            printf("Successfully transported ETB data to %s, %d words\n",systrace_fname, totalSize);

            fclose(fp);

        	/* Extract and export system trace DCM data */
        	if(transferDCMdata(pct_edma_res.isWrapped) < 0)
        	{
        		return(-1);
        	}

#elif defined(UCLIB_ETB_CPU)

   			pBuffer = (uint32_t*) &etb_drain_mem;
			while(sz < size_out)
			{
				etbword = *(pBuffer+sz);
				if(le[0] != 1) //Big endian
				 	etbword = BYTE_SWAP32(etbword);

				fret = fwrite((void*) &etbword, 4, 1, fp);
                if ( fret < 1 ) {
                    printf("Error writing data to  - %s \n", systrace_fname);
                	exit (EXIT_FAILURE);
                }

				sz++;
			}
            fclose(fp);
			printf("Successfully transported ETB data to %s\n",systrace_fname);

        	/* Extract and export system trace DCM data */
        	if(transferDCMdata(wrap_flag) < 0)
        	{
        		return(-1);
        	}
#endif
		}
		else
		{
			printf("Error opening file - %s\n", systrace_fname);
			return(-1);
		}
	}

#endif

    ctools_ret = ctools_systemtrace_shutdown(CTOOLS_SYS_TRACE_ALL);
    if(ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: System Trace shutdown Failed %d\n", ctools_ret);
        return(-1);
    }

    //success
    return(0);
}
