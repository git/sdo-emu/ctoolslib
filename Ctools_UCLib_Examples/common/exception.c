/** 
 *   @file  exception.c
 *
 *   @brief
 *      Define your C66x exception handler in this file.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "c6x.h"
#include "ctools_uclib.h"

#define MAX_DSP_CORES 8

#pragma DATA_SECTION(etb_drain_mem, ".app_etb_drain_mem")
#define ETB_SIZE_BYTES 4096
uint8_t etb_drain_mem[ETB_SIZE_BYTES];

#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

void C66x_exception_handler (void)
{
	ctools_Result trace_status = CTOOLS_SOK;
	uint32_t size_out, wrap_flag;
	uint32_t*   pBuffer = 0;

#if defined(AETINT_MEMWATCH)

	char     core_fname[MAX_DSP_CORES][64]= {
			"C:\\temp\\memwatch_aetint_etbdata_core0.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core1.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core2.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core3.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core4.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core5.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core6.bin",
			"C:\\temp\\memwatch_aetint_etbdata_core7.bin"
	};

#else

	char     core_fname[MAX_DSP_CORES][64]= {
			"C:\\temp\\exc_etbdata_core0.bin",
			"C:\\temp\\exc_etbdata_core1.bin",
			"C:\\temp\\exc_etbdata_core2.bin",
			"C:\\temp\\exc_etbdata_core3.bin",
			"C:\\temp\\exc_etbdata_core4.bin",
			"C:\\temp\\exc_etbdata_core5.bin",
			"C:\\temp\\exc_etbdata_core6.bin",
			"C:\\temp\\exc_etbdata_core7.bin"
	};

#endif

	printf("Inside C66x Exception handler \n");

#if defined(AETINT_MEMWATCH)

    // Close the AET watch point
	trace_status = ctools_memwatch_close();
    if (trace_status != CTOOLS_SOK)
    {
        printf ("Error: ctools_memwatch_close Failed %d\n",trace_status);
    }

#endif

	/* Call Exception trace job close, to stop collecting trace
	 * This would copy ETB hardware buffer as a linear buffer
	 * containing PC and timing traces to the memory provided here */
	trace_status = ctools_etb_cpu_drain(&etb_drain_mem, 4096, &size_out, &wrap_flag, CTOOLS_DSP_ETB);

	if(trace_status == CTOOLS_SOK)
	{
		printf("CPU ETB drain successful\n");

		if(wrap_flag == 1)
		{
			printf("Buffer wrapped and Available words - %d\n", size_out);
		}
		else
		{
			printf("Buffer NOT wrapped and Available words - %d\n", size_out);
		}
	}
	else
	{
		printf("CPU ETB drain failed %d\n", trace_status);
		exit (EXIT_FAILURE);
	}

	/* Close the PCT */
	trace_status = ctools_pct_close ();

	if(trace_status == CTOOLS_SOK)
	{
		printf("PC trace close successful\n");
	}
	else
	{
		printf("PC trace close failed %d\n", trace_status);
		exit (EXIT_FAILURE);
	}

	/* Shutdown the dsp trace */
	trace_status = ctools_dsptrace_shutdown();
	if(trace_status == CTOOLS_SOK)
	{
		printf("DSP trace shutdown successful\n");
	}
	else
	{
		printf("DSP trace shutdown failed %d\n", trace_status);
		exit (EXIT_FAILURE);
	}

	/* Transport the ETB data captured */
	{
		/* this example uses JTAG debugger via CIO to transport data to host PC */
		/* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
		FILE* fp = fopen(core_fname[DNUM], "wb");
		if(fp)
		{
			uint32_t sz = 0;
			uint32_t i = 1;
   			char *le = (char *) &i;
   			pBuffer = (uint32_t*) &etb_drain_mem;
   			size_t fret;

			while(sz < size_out)
			{
				uint32_t etbword = *(pBuffer+sz);
				if(le[0] != 1) //Big endian
				 	etbword = BYTE_SWAP32(etbword);

				fret = fwrite((void*) &etbword, 4, 1, fp);
                if ( fret < 1 ) {
                    printf("Error writing data to  - %s \n", core_fname[DNUM]);
                	exit (EXIT_FAILURE);
                }
				sz++;
			}
            fclose(fp);
			printf("Successfully transported ETB data to %s\n",core_fname[DNUM]);
		}
		else
		{
			printf("Error opening file - %s\n", core_fname[DNUM]);
			exit (EXIT_FAILURE);
		}
	}
}


/**
 *  @b Description
 *  @n
 *      C66x internal (Resource conflict) exception trigger function
 *
 *  @retval
 *      None
 */
void C66x_exception_trigger (void)
{

	asm(" mvk 1, a1; \n \
	      mvk 1, a0; \n \
		  [a1]  mvk 0, b0; \n \
	    ||[a0]  mv a0, b0; ");

	return;
}
