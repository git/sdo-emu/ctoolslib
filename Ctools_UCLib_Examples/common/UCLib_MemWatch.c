/****************************************************************************
CToolsLib - Use-Case Library Trace Example
File: UCLib_MemWatch.c

This example demonstrates how to capture or detect unintended
accesses to a particular variable (single address) or range of
variables (address range) .i.e basically a memory watch capability.
There are two flavors of this use case:
1) AETINT: Triggers the AET RTOS interrupt on the first unintended
access to a particular memory range. This AET RTOS interrupt can be
routed as an exception to the C66x core. This use case can be combined
with the PC trace exception use case to exactly capture the instruction
which performed the first unintended access.
2) PC Trace: A list of all unintended accesses to a particular memory
range can be captured. Each list item has the PC address, instruction op-code
and timing information.

For more details refer to ctools use case library Doxygen API reference

Copyright (c) 2009-2014 Texas Instruments Inc. (www.ti.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "exception.h"
#include "ctools_uclib.h"

/*******************************************************************************
 *    INSTRUCTIONS FOR DECODING BINARY TRACE DATA GENERATED BY THIS EXAMPLE
 *******************************************************************************
 *  - Back trace before exception trigger or App SW fault is collected and stored in
 *    C:\temp\memwatch_<pctrace,aetint>_etbdata_core(n).bin
 *  - bin2tdf utility is required, located in:
 *    -> cd <CCS install dir>\ccs_base\emulation\analysis\bin
 *  - To convert binary trace data captured to TDF, use the following
 *     command line:
 *    -> bin2tdf -bin C:/temp/memwatch_<pctrace,aetint>_etbdata_core(n).bin -app <path to example.out> -procid 66x
 *       -sirev 1 -rcvr ETB -output C:/temp/memwatch_<pctrace,aetint>_etbdata_core(n).tdf
 *  - The memwatch_<pctrace,aetint>_etbdata_core(n).tdf file may be opened from the CCS menu Tools->
 *     Hardware Trace Analyzer->Open File...
 *  - For more details refer to: http://processors.wiki.ti.com/index.php/BIN2TDF
 *******************************************************************************
 ******************************************************************************/

#define C66X_EXP_MASK_REGISTER       (0x018000C0)
#define C66X_INTC_AETINT_EVENT_NUM   (9)

#define N_APP_FUNCTION_RUNS 100

/*******************************************************************************
 * FUNCTIONS AND PARAMETERS USED FOR EXAMPLE APPLICATION
 */
uint32_t globalCntr[10] = {0, };
uint32_t globalMarker = 0;
uint32_t watched_variable = 0;

void endFunction()
{
	volatile int dummy1 =10, dummy2=0;

	dummy2 = dummy1 - 1;

    globalMarker = 10;

    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;

    globalMarker = 20;

    return;
}

void startFunction()
{
   volatile int dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;

    globalCntr[8]++;

    return;
}


void appFunction()
{
   volatile int dummy1 =100, dummy2=0;

   dummy2 = dummy1 - 1;

   while(dummy2 > 1)
   {
      dummy2--;
        if(dummy2 & 1)
        {
            endFunction();
        }
        if(dummy2 & 0x4)
        {
            startFunction();
        }
   }

}

void runFunction()
{
   volatile int dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;
}

void doneFunction(void)
{
    volatile uint32_t temp;
}

#if defined(PCT_MEMWATCH)

#define MAX_DSP_CORES 8

#pragma DATA_SECTION(etb_drain_mem, ".app_etb_drain_mem")
#define ETB_SIZE_BYTES 4096
uint8_t etb_drain_mem[ETB_SIZE_BYTES];

#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

void pct_memwatch_collect_etb_data (void)
{
	ctools_Result trace_status;
	uint32_t size_out, wrap_flag;
	uint32_t*   pBuffer = 0;
	char     core_fname[MAX_DSP_CORES][64]= {
			"C:\\temp\\memwatch_pctrace_etbdata_core0.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core1.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core2.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core3.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core4.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core5.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core6.bin",
			"C:\\temp\\memwatch_pctrace_etbdata_core7.bin"
	};

    // Close the AET watch point
	trace_status = ctools_memwatch_close();
    if (trace_status != CTOOLS_SOK)
    {
        printf ("Error: ctools_memwatch_close Failed %d\n",trace_status);
        exit (EXIT_FAILURE);
    }

	/* This would copy ETB hardware buffer as a linear buffer
	 * containing PC and timing traces to the memory provided here */
	trace_status = ctools_etb_cpu_drain(&etb_drain_mem, 4096, &size_out, &wrap_flag, CTOOLS_DSP_ETB);

	if(trace_status == CTOOLS_SOK)
	{
		printf("CPU ETB drain successful\n");

		if(wrap_flag == 1)
		{
			printf("Buffer wrapped and Available words - %d\n", size_out);
		}
		else
		{
			printf("Buffer NOT wrapped and Available words - %d\n", size_out);
		}
	}
	else
	{
		printf("CPU ETB drain failed %d\n", trace_status);
		exit (EXIT_FAILURE);
	}

	/* Shutdown the dsp trace */
	trace_status = ctools_dsptrace_shutdown();
	if(trace_status == CTOOLS_SOK)
	{
		printf("DSP trace shutdown successful\n");
	}
	else
	{
		printf("DSP trace shutdown failed %d\n", trace_status);
		exit (EXIT_FAILURE);
	}

	/* Transport the ETB data captured */
	{
		/* this example uses JTAG debugger via CIO to transport data to host PC */
		/* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
		FILE* fp = fopen(core_fname[DNUM], "wb");
		if(fp)
		{
			uint32_t sz = 0;
			uint32_t i = 1;
   			char *le = (char *) &i;
   			pBuffer = (uint32_t*) &etb_drain_mem;
   			size_t fret;

			while(sz < size_out)
			{
				uint32_t etbword = *(pBuffer+sz);
				if(le[0] != 1) //Big endian
				 	etbword = BYTE_SWAP32(etbword);

				fret = fwrite((void*) &etbword, 4, 1, fp);
                if ( fret < 1 ) {
                    printf("Error writing data to  - %s \n", core_fname[DNUM]);
                	exit (EXIT_FAILURE);
                }
				sz++;
			}

            fclose(fp);
			printf("Successfully transported ETB data to %s\n",core_fname[DNUM]);
		}
		else
		{
			printf("Error opening file - %s\n", core_fname[DNUM]);
			exit (EXIT_FAILURE);
		}
	}
}

#endif

void main(void)
{
	ctools_Result ctools_ret = CTOOLS_SOK;
	uint32_t idx=0;

	ctools_etb_config_t etb_config = {0};

	etb_config.etb_mode = eETB_TI_Mode;

    /* Initialize the ETB */
	ctools_ret = ctools_etb_init(CTOOLS_DRAIN_ETB_CPU, &etb_config, CTOOLS_DSP_ETB);
	if (ctools_ret != CTOOLS_SOK)
	{
	    printf (" ETB initialization failed %d\n", ctools_ret);
		exit (EXIT_FAILURE);
	}

	/* Initialize the DSP trace
     * Please refer to the ctools use case library Doxygen API reference
     * Guide for the API usage */
	ctools_ret = ctools_dsptrace_init();
	if (ctools_ret != CTOOLS_SOK)
	{
	    printf (" DSP trace initialization failed %d\n", ctools_ret);
		exit (EXIT_FAILURE);
	}

#if defined(AETINT_MEMWATCH)

	/* Start the PC and Timing Trace capture from this point
	 * After completing this function, the core would be logging
	 * the PC and timing trace to the ETB buffer in a circular way
	 * on this core
	 */
	ctools_ret = ctools_pct_start_exc();
	if (ctools_ret != CTOOLS_SOK)
	{
	    printf (" Start PC trace for exception failed %d\n", ctools_ret);
		exit (EXIT_FAILURE);
	}

    // Enable the memory watch point
	ctools_ret = ctools_memwatch_aetint_setup(&watched_variable, &watched_variable);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_memwatch_aetint_setup Failed %d\n", ctools_ret);
        exit (EXIT_FAILURE);
    }

    //Route AETINT as an Exception to C66x core

    unsigned int exp_mask_reg;
    exp_mask_reg = *(volatile unsigned int *)C66X_EXP_MASK_REGISTER;
    *(volatile unsigned int *)C66X_EXP_MASK_REGISTER = exp_mask_reg & (~(0x1 << C66X_INTC_AETINT_EVENT_NUM));

#elif defined(PCT_MEMWATCH)

    // Enable the memory watch point
	ctools_ret = ctools_memwatch_store_pc_setup(&watched_variable, &watched_variable);
    if (ctools_ret != CTOOLS_SOK)
    {
        printf ("Error: ctools_memwatch_store_pc_setup Failed %d\n", ctools_ret);
        exit (EXIT_FAILURE);
    }

#endif

    /* ******************************************************************************** */
    /* ******************************************************************************** */
    /* ********************************** APP CODE ************************************ */

    /***** Now the app runs and generates trace data ****/

    runFunction();

	// Disable the AET watch point
	ctools_ret = ctools_memwatch_disable();
	if (CTOOLS_SOK != ctools_ret)
	{
	   printf ("Error: ctools_memwatch_disable Failed %d\n",ctools_ret);
	   exit (EXIT_FAILURE);
	}

	watched_variable = 10; // Perform a valid access to watched_variable

    // Re-enable the AET watch point
    ctools_ret = ctools_memwatch_enable();
    if (CTOOLS_SOK != ctools_ret)
    {
        printf ("Error: ctools_memwatch_enable Failed %d\n",ctools_ret);
        exit (EXIT_FAILURE);
    }

    for(idx = 0; idx < N_APP_FUNCTION_RUNS; idx++)
    {
        appFunction();
    }

    watched_variable = 20; // Perform a invalid access to watched_variable

    appFunction();

    watched_variable = 30; // Perform a invalid access to watched_variable

    appFunction();

    watched_variable = 40; // Perform a invalid access to watched_variable

    appFunction();

    watched_variable = 50; // Perform a invalid access to watched_variable

    appFunction();

    watched_variable = 60; // Perform a invalid access to watched_variable

    doneFunction();

#if defined(PCT_MEMWATCH)

    pct_memwatch_collect_etb_data();

#endif
}
