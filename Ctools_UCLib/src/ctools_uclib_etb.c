/*
 * file: ctools_uclib_etb.c
 *
 * brief:
 *     ETB specific Initialization, drain and shutdown functions implementation.
 *
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <string.h>
#include "ctools_uclib_osal.h"
#include "ctools_uclib_etb.h"
#include "ctools_uclib_stm_device.h"
#include "ETBAddr.h"
#include "C6x.h"

#pragma DATA_SECTION(CTOOLS_DSPETB_INST, ".bss:ctoolsuc_nonshared")
ctools_etb_inst_t*       CTOOLS_DSPETB_INST;

#pragma DATA_SECTION(CTOOLS_SYSETB_INST, ".bss:ctoolsuc_nonshared")
ctools_etb_inst_t* CTOOLS_SYSETB_INST;

static ctools_Result edma_setup(const ctools_edmaConfig_t* edma_config, ctools_etb_types_e etb_type)
{

    DMAConfig    dmaConfig = {0};

#if !defined(C6657) && !defined(C66AK2Hxx) && !defined(TCI6630K2L) && !defined(C66AK2Exx) && !defined(_66AK2Gxx)

    int32_t edma_clr_ch, edma_etb_ch;

    /* Get the Clear Channel and ETB Drain Channel */
    edma_clr_ch  = *(edma_config->channels_ptr + 0 );
    edma_etb_ch  = *(edma_config->channels_ptr + 1 );

    // Map clr and etb channels to Params 0 and 1
    EDMA3_DCHMAP_REG((uint8_t)edma_config->cc_sel,edma_clr_ch) = ((*(edma_config->param_ptr+0)) << 5);
	EDMA3_DCHMAP_REG((uint8_t)edma_config->cc_sel,edma_etb_ch) = ((*(edma_config->param_ptr+1)) << 5);

   /************************************************************************************
    * **  DMA CONFIGURATION REQUIRED BY APPLICATION CODE  **
    *
    *  A detailed explanation of the configuration requirements for the application
    *   versus the ETB library is in the main page of the ETB library doxygen based
    *   documentation. This main page is at the bottom of the ETBInterface.h file.
    */

    /* Configure the DMA parameters */
    dmaConfig.cc           = edma_config->cc_sel;           /* Using channel edma3 controller selected */
    dmaConfig.cic          = edma_config->cic_sel;          /* Using the Chip Interrupt Controller Selected */
    dmaConfig.clrChannel   = edma_clr_ch;  /* DMA channel used for clearing system event */
    dmaConfig.etbChannel   = edma_etb_ch;  /* DMA channel core used for ETB data transfers */

    dmaConfig.linkparam[0] = *(edma_config->param_ptr+2);  /* 3 additional PaRAM sets for links */
    dmaConfig.linkparam[1] = *(edma_config->param_ptr+3);
    dmaConfig.linkparam[2] = *(edma_config->param_ptr+4);

#else

    dmaConfig.linkparam[0] = *(edma_config->param_ptr);  /* 3 additional PaRAM sets for links */
    dmaConfig.linkparam[1] = *(edma_config->param_ptr+1);
    dmaConfig.linkparam[2] = *(edma_config->param_ptr+2);

#endif

    dmaConfig.dbufAddress  = (uint32_t) edma_config->dbufAddress;
    dmaConfig.dbufWords    = (edma_config->dbufBytes >> 2);    /* Configure DMA buffer words - convert from bytes to words */
    
    if (edma_config->mode == CTOOLS_USECASE_EDMA_CIRCULAR_MODE)        
    {
      dmaConfig.mode         = eDMA_Circular; /* Using circular buffer*/
    }
    else
    {
      dmaConfig.mode         = eDMA_Stop_Buffer; /* Using Stop buffer */    
    }            
 
    if(etb_type == CTOOLS_DSP_ETB)
	{
		ETB_config_dma(CTOOLS_DSPETB_INST->pETBHandle, &dmaConfig);
	}
	else if(etb_type == CTOOLS_SYS_ETB)
	{
		ETB_config_dma(CTOOLS_SYSETB_INST->pETBHandle, &dmaConfig);
	}

   /************************************************************************************
    ***   END OF DMA CONFIGURATION REQUIRED BY APPLICATION CODE  **
    ************************************************************************************/            
     return (CTOOLS_SOK);
}

static int32_t get_core_id(void)
{
	return(DNUM);
}

static ctools_Result etb_shutdown  (ctools_etb_types_e etb_type)
{
	 eETB_Error              etbRet;

	 if(etb_type == CTOOLS_DSP_ETB)
	 {
		 if (CTOOLS_DSPETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
	     {
	         etbRet  = ETB_close(CTOOLS_DSPETB_INST->pETBHandle);
	         if(etbRet != eETB_Success)
	         {
	           return(CTOOLS_ETB_CLOSE_ERROR);
	         }
	     }

		/* Free the allocated memory */
	    if (CTOOLS_DSPETB_INST != NULL)
	    {
	        cTools_memFree((void*)CTOOLS_DSPETB_INST);
	        CTOOLS_DSPETB_INST = NULL;
	    }
	 }
	 else if(etb_type == CTOOLS_SYS_ETB)
	 {
		 if (CTOOLS_SYSETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
	     {
	         etbRet  = ETB_close(CTOOLS_SYSETB_INST->pETBHandle);
	         if(etbRet != eETB_Success)
	         {
	           return(CTOOLS_ETB_CLOSE_ERROR);
	         }
	     }

		/* Free the allocated memory */
	    if (CTOOLS_SYSETB_INST != NULL)
	    {
	        cTools_memFree((void*)CTOOLS_SYSETB_INST);
	        CTOOLS_SYSETB_INST = NULL;
	    }
	 }

	 return (CTOOLS_SOK);
}

ctools_Result ctools_etb_init (const ctools_etb_drain_e etb_drain_opt, const ctools_etb_config_t* config, ctools_etb_types_e etb_type)
{

	 uint32_t                etbWidth;
	 eETB_Error              etbRet;
     ETB_errorCallback       pETBErrCallBack =0;
     ETBProperties etb_properties;
	 ctools_etb_inst_t* etb_inst;
	 eETB_Mode          etb_mode;
	 
	if(etb_type == CTOOLS_DSP_ETB)
	{
		 CTOOLS_DSPETB_INST = (ctools_etb_inst_t*)cTools_memAlloc(sizeof (ctools_etb_inst_t) );
		 memset(CTOOLS_DSPETB_INST, 0, sizeof(ctools_etb_inst_t));
		 etb_inst = CTOOLS_DSPETB_INST; 
    }
	else if(etb_type == CTOOLS_SYS_ETB)
    {
		 CTOOLS_SYSETB_INST = (ctools_etb_inst_t*)cTools_memAlloc(sizeof (ctools_etb_inst_t) );
		 memset(CTOOLS_SYSETB_INST, 0, sizeof(ctools_etb_inst_t));
		 etb_inst = CTOOLS_SYSETB_INST;

		 //Return error, if it is required to configure SYS ETB in stop on full mode (for both ETB and ETB-EDMA extension cases)
		 if((config->etb_mode == eETB_Stop_Buffer) || (config->etb_mode == eETB_TI_Mode_AND_Stop_Buffer) ||
				 (config->edmaConfig.mode == CTOOLS_USECASE_EDMA_STOP_BUF_MODE))
		 {
			 //Return error
			 return(CTOOLS_SYSETB_STOPBUF_NOT_SUPPORTED);
		 }
	} 
	
    if ( (etb_drain_opt == CTOOLS_DRAIN_ETB_EDMA) ||
          (etb_drain_opt == CTOOLS_DRAIN_ETB_CPU) ||
          (etb_drain_opt == CTOOLS_DRAIN_NONE) )
    {
        /* supported case */
		etb_inst->etbDrainOpts    = etb_drain_opt;
    }
    else
    {
       /* Set to CPU Drain Case as Default */
       etb_inst->etbDrainOpts = CTOOLS_DRAIN_ETB_CPU;
    }

    //The ETB is always configured in Circular mode for ETB-EDMA extension
    if (etb_drain_opt == CTOOLS_DRAIN_ETB_EDMA)
    {
    	etb_mode = eETB_TI_Mode;
    }
    else
    {
    	etb_mode = config->etb_mode;
    }

    if (etb_drain_opt != CTOOLS_DRAIN_NONE)
    {
        /*** Setup ETB receiver ***/
        /* Open ETB module in circular mode */
        /* Workaround to clear the status register since ETB does not handle it */
        //    *tetb_icst = 0x0000001F;

        if(etb_type == CTOOLS_DSP_ETB)
    	{
            etbRet = ETB_open(pETBErrCallBack, etb_mode, get_core_id(),
                                                            &etb_inst->pETBHandle, &etbWidth);
    	}
    	else if(etb_type == CTOOLS_SYS_ETB)
    	{
    		etbRet = ETB_open(pETBErrCallBack, etb_mode, SYS_ETB_ID,
    														&etb_inst->pETBHandle, &etbWidth);
    	}

        if(etbRet != eETB_Success)
        {
            return(CTOOLS_ETB_OPEN_ERROR);
        }
     }

     if (etb_drain_opt == CTOOLS_DRAIN_ETB_EDMA)
     {
         if (config == NULL)
            return (CTOOLS_INVALID_PARAM);

         //Check if ETB-EDMA extension is supported or not
         ETB_getProperties(&etb_properties);

         if(etb_properties.is_dma_supported == 1)
         {
        	 edma_setup(&config->edmaConfig, etb_type);
         }
         else
         {
        	 //Return error
        	 return(CTOOLS_ETB_EDMA_DRAIN_NOT_SUPPORTED);
         }
     }

    if (etb_drain_opt != CTOOLS_DRAIN_NONE)
    {
        /* Enable ETB receiver */
        etbRet = ETB_enable(etb_inst->pETBHandle, 0);
        if(etbRet != eETB_Success)
        {
           return(CTOOLS_ETB_ENABLE_ERROR);
        }
    }
	
	return (CTOOLS_SOK);
}

ctools_Result ctools_etb_cpu_drain (const void *data_p, uint32_t size_in, uint32_t *size_out, uint32_t *wrap_flag, ctools_etb_types_e etb_type)
{
    uint32_t*               pBuffer=0;
    ETBStatus               etbStatus;
    uint32_t                retSize;
	uint32_t                readlength;
    eETB_Error              etbRet;
    ctools_etb_inst_t* etb_inst;
    ctools_Result result = CTOOLS_SOK;

    *size_out    = 0;

	if(etb_type == CTOOLS_DSP_ETB)
	{
		etb_inst = CTOOLS_DSPETB_INST;
    }
	else if(etb_type == CTOOLS_SYS_ETB)
    {
		etb_inst = CTOOLS_SYSETB_INST;
	}

	/* Now disable trace capture - ETB receiver, also flush+stop formatter is done */
	etbRet = ETB_disable(etb_inst->pETBHandle);
	if(etbRet != eETB_Success)
	{
	  return(CTOOLS_ETB_DISABLE_ERROR);
	}

	/*** Get ETB data ***/
	/* Check the ETB status */
	etbRet= ETB_status(etb_inst->pETBHandle, &etbStatus);
	if(etbRet != eETB_Success)
	{
	  return(CTOOLS_ETB_GETSTATUS_ERROR);
	}

	//Get ETB wrap status
	*wrap_flag = etbStatus.isWrapped;

	if(etbStatus.canRead == 1)
	{
		pBuffer = (uint32_t*)  data_p; // ETB words are 32 bit long

		if(pBuffer)
		{
		  if(etbStatus.availableWords > size_in)
		  {
			readlength = size_in;
		  }
		  else
		  {
			readlength = etbStatus.availableWords;
		  }
		  
		  etbRet = ETB_read(etb_inst->pETBHandle, pBuffer, size_in, 0, readlength, &retSize);
		  if(etbRet != eETB_Success)
		  {
			return(CTOOLS_ETB_READ_ERROR);
		  }
		}
		else
		{
			return (CTOOLS_NOBUF_FOR_ETBDRAIN);
		}
	}

    /* Update the size_out parameter */
    *size_out = retSize;

    // Shutdown the ETB
    result = etb_shutdown(etb_type);

    return(result);
}

ctools_Result ctools_etb_edma_drain (ctools_edma_result_t* edma_rslt_ptr, ctools_etb_types_e etb_type)
{
    eETB_Error              etbRet;
    DMAStatus               dmaStatus;
    ETBProperties etb_properties;
    ctools_etb_inst_t* etb_inst;
    ctools_Result result = CTOOLS_SOK;

    //Check if ETB-EDMA extension is supported or not
    ETB_getProperties(&etb_properties);

    if(etb_properties.is_dma_supported == 1)
    {

    	if(etb_type == CTOOLS_DSP_ETB)
    	{
    		 etb_inst = CTOOLS_DSPETB_INST;
        }
    	else if(etb_type == CTOOLS_SYS_ETB)
        {
    		 etb_inst = CTOOLS_SYSETB_INST;
    	}

    	etbRet= ETB_flush (etb_inst->pETBHandle);

		if(etbRet != eETB_Success)
		{
		   return(CTOOLS_ETB_FLUSH_ERROR);
		}
		/* NOTE:
		 *  ** THIS FLUSH API CALL IS REQUIRED TO GET THE DMA STATUS **
		 *
		 * Before reading from the ETB drain buffer, make sure all data has been
		 *  read out of the ETB.
		 */

		etbRet = ETB_flush_dma(etb_inst->pETBHandle, &dmaStatus);

		if(etbRet != eETB_Success)
		{
		   return(CTOOLS_ETB_GETSTATUS_ERROR);
		}

		edma_rslt_ptr->availableWords   = dmaStatus.availableWords;
		edma_rslt_ptr->dbufAddress      = dmaStatus.dbufAddress;
		edma_rslt_ptr->dbufWords        = dmaStatus.dbufWords;
		edma_rslt_ptr->isWrapped        = dmaStatus.isWrapped;
		edma_rslt_ptr->startAddr        = dmaStatus.startAddr;

		/* Now disable trace capture - ETB receiver */
		etbRet = ETB_disable(etb_inst->pETBHandle);

		if(etbRet != eETB_Success)
		{
		  return(CTOOLS_ETB_DISABLE_ERROR);
		}

	    // Shutdown the ETB
	    result = etb_shutdown(etb_type);

	    return(result);
    }
    else
    {
   	 //Return error
   	 return(CTOOLS_ETB_EDMA_DRAIN_NOT_SUPPORTED);
    }
}

ctools_Result   ctools_edma_resrc_get (uint32_t* num_params, uint32_t* num_channels)
{  

#if defined(C6657) || defined(C66AK2Hxx) || defined(C66AK2Exx) || defined(TCI6630K2L) || defined(_66AK2Gxx)

   *num_params   = 3;
   *num_channels = 0;

#else

   *num_params   = 5;
   *num_channels = 2;

#endif

   return (CTOOLS_SOK);
}
/**
@}
*/
/* Nothing past this point */
