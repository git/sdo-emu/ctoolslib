/*
 *  file:  ctools_uclib_stat_prof.c
 *
  * brief:
 *      Provides APIs to start and stop statistical profile trace jobs for capturing PC and timing trace at a fixed sampling period
 *       for calculating % cycle distribution among various functions in a given application.
 *  
 *  NOTE:
 *      (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

#include "ctools_uclib_stat_prof.h"
#include "ctools_uclib_osal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <c6x.h>

#pragma DATA_SECTION(STATPROF_INST, ".bss:ctoolsuc_nonshared")
ctools_statprof_resources_pntr STATPROF_INST;

// External variables
extern ctools_dsptrace_inst_t*  CTOOLS_DSPTRC_INST;

ctools_Result ctools_stat_prof_start(uint32_t SamplingPeriod)
{
    
    if (CTOOLS_DSPTRACE_UC_FREE == CTOOLS_DSPTRC_INST->state)
    {
        CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_STATPROF;
    }
    else
    {
        return (CTOOLS_RESOURCE_BUSY);
    }

    //Allocate memory resources for statistical profiling
    STATPROF_INST = (ctools_statprof_resources_pntr)cTools_memAlloc(sizeof(ctools_statprof_resources));
	memset(STATPROF_INST, 0, sizeof(ctools_statprof_resources));

	/* Set up AET for the statistical profile job */

    STATPROF_INST->counter0 = AET_COUNTERCONFIGPARAMS;
    STATPROF_INST->statprof_params = AET_JOBPARAMS;

    // Perform AET initialization
    AET_init();

    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }

    /* Set the counter configuration parameters */
    STATPROF_INST->counter0.configMode = AET_COUNTER_TRAD_COUNTER;
    STATPROF_INST->counter0.counterNumber = AET_CNT_0;
    STATPROF_INST->counter0.reloadValue = SamplingPeriod;

    AET_configCounter(&STATPROF_INST->counter0);

    /*
     Now set up the parameters for the trace trigger job
     */
    STATPROF_INST->statprof_params.traceTriggers = AET_TRACE_PA | AET_TRACE_TIMING;
    STATPROF_INST->statprof_params.counterNumber = AET_CNT_0;

    /* Set up the trace trigger job */
    if (AET_setupJob(AET_JOB_TRIG_ON_TIMER_ZERO, &STATPROF_INST->statprof_params)){

    	return(CTOOLS_AET_SETUPJOB_ERROR);
    }

    STATPROF_INST->statprof_jobnumber = STATPROF_INST->statprof_params.jobIndex;

    /* Enable AET */
    if (AET_enable())
    {
      return(CTOOLS_AET_ENABLE_ERROR);
    }

	return(CTOOLS_SOK);
}

ctools_Result ctools_stat_prof_end (void)
{
	// Release the AET statistical profile job

    AET_releaseJob(STATPROF_INST->statprof_jobnumber);
    AET_release();

	//Free up the allocated memory
	if (STATPROF_INST != NULL)
    {
		cTools_memFree(STATPROF_INST);
		STATPROF_INST = NULL;
	}

	//Success
	return (CTOOLS_SOK);
}

/**
@}
*/
