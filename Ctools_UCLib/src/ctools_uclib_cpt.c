/*
 *  file: ctools_uclib_cpt.c
 *
 *  brief:
 *      Provides APIs to open, start, end and close CP Tracer specific use case scenarios.
 *
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <string.h>
#include "ctools_uclib_cpt.h"
#include "ctools_uclib_osal.h"

//External variables
extern ctools_cpt_resources_pntr cptrace_pntr;

// Internal helper functions

static eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[],
                                    int CPT_MasterIDCnt, unsigned int CPT_MasterIDGrp, CPT_Qualifiers const * const pCPT_TPCntQual);
static eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle,
                                 const eCPT_MasterID CPT_MasterID0[], int CPT_MasterID0Cnt, unsigned int CPT_MasterID0Grp, CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                 const eCPT_MasterID CPT_MasterID1[], int CPT_MasterID1Cnt, unsigned int CPT_MasterID1Grp, CPT_Qualifiers const * const pCPT_TPCnt1Qual);
static eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[], int CPT_MasterIDCnt,
                          CPT_Qualifiers const * const pCPT_TPEventQual);
static eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle, CPT_Qualifiers const * const pCPT_TPCntQual);


static eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[],
                                    int CPT_MasterIDCnt, uint32_t CPT_MasterIDGrp, CPT_Qualifiers const * const pCPT_TPCntQual)
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
	eCPT_MasterState master_state;

    {
        int i;
        for ( i = 0; i < CPT_MasterIDCnt; i++ )
        {
            
			(CPT_MasterIDGrp == DBG_TOOLS_CPT_DISABLE) ? (master_state = eCPT_Mstr_Enable) : (master_state = eCPT_Mstr_Enable_Grp);
			
        	error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[i], eCPT_Throughput_Cnt0, master_state);

            if ( eCPT_Success != error )
            {
                break;
            }
        }

        if(CPT_MasterIDCnt == 0) //MasterID filtering disabled
        {
            //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_EnableAll overrides
            error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt0, eCPT_Mstr_EnableAll);
        }
    }

    if ( eCPT_Success == error ) 
    {  
        error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[0], eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
	
    return error;

}
static eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle, 
		                         const eCPT_MasterID CPT_MasterID0[], int CPT_MasterID0Cnt, uint32_t CPT_MasterID0Grp, CPT_Qualifiers const * const pCPT_TPCnt0Qual,
		                         const eCPT_MasterID CPT_MasterID1[], int CPT_MasterID1Cnt, uint32_t CPT_MasterID1Grp, CPT_Qualifiers const * const pCPT_TPCnt1Qual)
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
	eCPT_MasterState master_state;

    int i;

    {
        for ( i = 0; i < CPT_MasterID0Cnt; i++ )
        {
            
			(CPT_MasterID0Grp == DBG_TOOLS_CPT_DISABLE) ? (master_state = eCPT_Mstr_Enable) : (master_state = eCPT_Mstr_Enable_Grp);
			
        	error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID0[i], eCPT_Throughput_Cnt0, master_state);

            if ( eCPT_Success != error )
            {
                break;
            }
        }

        if(CPT_MasterID0Cnt == 0) //MasterID filtering disabled
        {
            //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_EnableAll overrides
            error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt0, eCPT_Mstr_EnableAll);
        }
    }
    
    if ( eCPT_Success == error ) 
    {
		for ( i = 0; i < CPT_MasterID1Cnt; i++ )
		{
			
			(CPT_MasterID1Grp == DBG_TOOLS_CPT_DISABLE) ? (master_state = eCPT_Mstr_Enable) : (master_state = eCPT_Mstr_Enable_Grp);
			
		    error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID1[i], eCPT_Throughput_Cnt1, master_state);

			if ( eCPT_Success != error )
			{
				break;
			}
		}

		if(CPT_MasterID1Cnt == 0) //MasterID filtering disabled
		{
	        //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_EnableAll overrides
	        error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
		}
    }

    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCnt0Qual, pCPT_TPCnt1Qual, pCPT_TPEventQual, pCPT_TrigQual);
    }

    return error;
}

static eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[], int CPT_MasterIDCnt, 
		                  CPT_Qualifiers const * const pCPT_TPEventQual)
{    
    eCPT_Error error = eCPT_Success;
    CPT_Qualifiers * pCPT_TPCntQual = NULL;
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;    
    
	int i;
	for ( i = 0; i < CPT_MasterIDCnt; i++ )
	{
		error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[i], eCPT_Throughput_Cnt0, eCPT_Mstr_Enable);
		if ( eCPT_Success != error )
		{
			break;
		}
	}

    if(CPT_MasterIDCnt == 0) //MasterID filtering disabled
    {
        //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_EnableAll overrides
        error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt0, eCPT_Mstr_EnableAll);
    }

    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }

    return error;
}            

static eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle, 
                                 CPT_Qualifiers const * const pCPT_TPCntQual)
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;

    //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_DisableALL overrides
    error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt0, eCPT_Mstr_DisableALL);
    if ( eCPT_Success == error ) 
    {  
        //Note that (eCPT_MasterID)(0) is ignored because eCPT_Mstr_EnableAll overrides
        error = CPT_CfgMaster( pCPT_Handle, (eCPT_MasterID)(0), eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    
    return error;
}

static ctools_Result CPT_address_filter_setup (const ctools_cpt_addressfiltercfg * Address_Filter_Params, eCPT_ModID CPT_ModId)
{
	// Configure address filtering
	if(Address_Filter_Params != NULL)
	{
		if (  eCPT_Success != CPT_CfgAddrFilter( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
												 Address_Filter_Params->AddrFilterMSBs,
												 Address_Filter_Params->StartAddrFilterLSBs,
												 Address_Filter_Params->EndAddrFilterLSBs,
												 Address_Filter_Params->CPT_FilterMode ) )
		{
			// Return error
			return(CTOOLS_CPT_ADDRFILTER_ERROR);
		}
	}

	//Success
	return(CTOOLS_SOK);
}

static ctools_Result CPT_sysprofile_open (const ctools_cpt_sysprofilecfg * SysProfile_params, eCPT_UseCase CPT_UseCaseId)
{
	uint32_t i;
	eCPT_ModID CPT_ModId;

	CPT_CfgOptions CPTConfig_Params;
	
	if((SysProfile_params == NULL) || (cptrace_pntr == NULL))
	{
		//Return error (System trace init is not called or SysProfile_params not populated
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	// Populate the CPT Param values

	CPTConfig_Params.pSTMHandle = cptrace_pntr->pSTMhdl;
	CPTConfig_Params.STM_LogMsgEnable = true;                 // Enable STM software message logging from CPTLib
	CPTConfig_Params.STMMessageCh = STM_CPTLogCh;             // Set the STM software message logging channel


	CPTConfig_Params.CPUClockRateMhz = SysProfile_params->CPUClockRateMhz;                   // Provide CPU clock rate
	CPTConfig_Params.DataOptions = 0;                         // Set supress zero data but since this example enables
																// and disables CPT messages as needed there is not really
																// any 0 data generated
	CPTConfig_Params.pCPT_CallBack = NULL;         // Callback on error

	// Get Sample window size
	CPTConfig_Params.SampleWindowSize = SysProfile_params->SampleWindowSize;

	// Get the CPT use-case
	CPTConfig_Params.CPT_UseCaseId = CPT_UseCaseId;

	for ( i=0; i < SysProfile_params->CPT_ModCnt; i++ )
	{
		// Check if the CP Tracer is already in use for some other use case scenario
		if(cptrace_pntr->CPT_JobsList[SysProfile_params->ModIDQual[i].CPT_ModId].in_use_flag != 1)
		{
			// Get the CPT module ID to be programmed
			CPT_ModId = SysProfile_params->ModIDQual[i].CPT_ModId;

			//Update the CPT use-case in the jobs list
			cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId = CPTConfig_Params.CPT_UseCaseId;

			// Open CPT module
			if (  eCPT_Success != CPT_OpenModule(&(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle), CPT_ModId,  &CPTConfig_Params ) )
			{
				// Return error
				return(CTOOLS_CPT_SYSPROFILE_ERROR);
			}

			if(SysProfile_params->ModIDQual[i].TPCntQual != NULL)
			{
				// Configure the CPT for Sys bandwidth profiling, use the qualifiers provided by the user
				if ( eCPT_Success != CPTCfg_SystemProfile(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, SysProfile_params->ModIDQual[i].TPCntQual) )
				{
					// Return error
					return(CTOOLS_CPT_SYSPROFILE_ERROR);
				}
			}
			else
			{
				// Configure the CPT for Sys bandwidth profiling, use default qualifiers as none was provided by the user
				if ( eCPT_Success != CPTCfg_SystemProfile(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, NULL) )
				{
					// Return error
					return(CTOOLS_CPT_SYSPROFILE_ERROR);
				}
			}

			// Set the current CPT to be in use
			cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag = 1;
		}
		else
		{
			// Return error
			return(CTOOLS_CPT_SYSPROFILE_BUSY);
		}
	}

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_syslatprofile_open (const ctools_cpt_sysprofilecfg * SysProfile_params)
{
	ctools_Result result;

	result = CPT_sysprofile_open(SysProfile_params, eCPT_UseCases_SysLatencyProfile);

	return(result);
}

ctools_Result ctools_cpt_sysbwprofile_open (const ctools_cpt_sysprofilecfg * SysProfile_params)
{
	ctools_Result result;

	result = CPT_sysprofile_open(SysProfile_params, eCPT_UseCases_SysBandwidthProfile);

	return(result);
}

ctools_Result ctools_cpt_eventtrace_open (const ctools_cpt_eventtracecfg * EventTrace_params)
{
	eCPT_ModID CPT_ModId;
	CPT_CfgOptions CPTConfig_Params;

	if((EventTrace_params == NULL) || (cptrace_pntr == NULL))
	{
		//Return error (System trace init is not called or EventTrace_params not populated
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

    // Populate the CPT Param values
    CPTConfig_Params.pSTMHandle = cptrace_pntr->pSTMhdl;
    CPTConfig_Params.STM_LogMsgEnable = true;                 // Enable STM software message logging from CPTLib
    CPTConfig_Params.STMMessageCh = STM_CPTLogCh;             // Set the STM software message logging channel

	CPTConfig_Params.DataOptions = 0;                         // Set supress zero data but since this example enables
																// and disables CPT messages as needed there is not really
																// any 0 data generated
	CPTConfig_Params.pCPT_CallBack = NULL;         // Callback on error

	// Get Address export mask
	CPTConfig_Params.AddrExportMask = EventTrace_params->AddrExportMask;

	// Always use eCPT_UseCase_Raw for Event B capture
	CPTConfig_Params.CPT_UseCaseId = eCPT_UseCase_Raw;

	// Check if the CP Tracer is already in use for some other use case scenario
	if(cptrace_pntr->CPT_JobsList[EventTrace_params->CPT_ModId].in_use_flag != 1)
	{
		// Get the CPT module ID to be programmed
		CPT_ModId = EventTrace_params->CPT_ModId;

		//Update the CPT use-case in the jobs list
		cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId = CPTConfig_Params.CPT_UseCaseId;

		// Open CPT module
		if (  eCPT_Success != CPT_OpenModule(&(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle), CPT_ModId,  &CPTConfig_Params ) )
		{
			// Return error
			return(CTOOLS_CPT_EVTPROFILE_ERROR);
		}

		ctools_Result result;

		result = CPT_address_filter_setup (EventTrace_params->Address_Filter_Params, CPT_ModId);

		if(result != CTOOLS_SOK)
		{
			return(result);
		}

	    // For the Event B Trace, if masterID filtering is disabled, enable all masters (no filtering) by default
	    if(EventTrace_params->EventTrace_MasterID == NULL)
	    {
			// Configure the CPT for Event B capture
	    	if ( eCPT_Success != CPTCfg_Events( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
	    			NULL,
	    			0,
	    			EventTrace_params->TPEventQual))
	    	{
				// Return error
				return(CTOOLS_CPT_EVTPROFILE_ERROR);
	    	}
	    }
	    else
	    {
		    // Configure the CPT for Event B capture
    	    if ( eCPT_Success != CPTCfg_Events( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
    			     EventTrace_params->EventTrace_MasterID->CPT_MasterID,
    			     EventTrace_params->EventTrace_MasterID->CPT_MasterIDCnt,
    			     EventTrace_params->TPEventQual))
    	    {
			   // Return error
			   return(CTOOLS_CPT_EVTPROFILE_ERROR);
    	    }
	    }

		// Set the current CPT to be in use
	    cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag = 1;
	}
	else
	{
		// Return error
		return(CTOOLS_CPT_EVTPROFILE_BUSY);
	}

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_masterprofile_open (const ctools_cpt_masterprofilecfg * MasterProfile_params)
{
    eCPT_ModID CPT_ModId;
    CPT_CfgOptions CPTConfig_Params;

	if((MasterProfile_params == NULL) || (cptrace_pntr == NULL))
	{
		//Return error (System trace init is not called or MasterProfile_params not populated
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

    // Populate the CPT Param values
    CPTConfig_Params.pSTMHandle = cptrace_pntr->pSTMhdl;
    CPTConfig_Params.STM_LogMsgEnable = true;                 // Enable STM software message logging from CPTLib
    CPTConfig_Params.STMMessageCh = STM_CPTLogCh;             // Set the STM software message logging channel

    CPTConfig_Params.CPUClockRateMhz = MasterProfile_params->CPUClockRateMhz;                   // Provide CPU clock rate
    CPTConfig_Params.DataOptions = 0;                         // Set supress zero data but since this example enables
                                                                // and disables CPT messages as needed there is not really
                                                                // any 0 data generated
    CPTConfig_Params.pCPT_CallBack = NULL;         // Callback on error

    // Get Sample window size
    CPTConfig_Params.SampleWindowSize = MasterProfile_params->SampleWindowSize;

    // Get the CPT use-case
    CPTConfig_Params.CPT_UseCaseId = eCPT_UseCase_MasterProfile;

    // Check if the CP Tracer is already in use for some other use case scenario
    if(cptrace_pntr->CPT_JobsList[MasterProfile_params->CPT_ModId].in_use_flag != 1)
    {
        // Get the CPT module ID to be programmed
        CPT_ModId = MasterProfile_params->CPT_ModId;

        //Update the CPT use-case in the jobs list
        cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId = CPTConfig_Params.CPT_UseCaseId;

        // Open CPT module
        if (  eCPT_Success != CPT_OpenModule(&(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle), CPT_ModId,  &CPTConfig_Params ) )
        {
            // Return error
            return(CTOOLS_CPT_MASTERPROFILE_ERROR);
        }

		ctools_Result result;

		result = CPT_address_filter_setup (MasterProfile_params->Address_Filter_Params, CPT_ModId);

		if(result != CTOOLS_SOK)
		{
			return(result);
		}

		const eCPT_MasterID * CPT_MasterID0;
		const eCPT_MasterID * CPT_MasterID1;
        uint32_t CPT_MasterIDCnt0, CPT_MasterIDCnt1;
        uint8_t group_enable_disable0, group_enable_disable1;

        // For Bandwidth profiling, if masterID filtering is disabled, enable all masters (no filtering) by default

        if(MasterProfile_params->TPCnt0_MasterID != NULL)
        {
        	CPT_MasterID0 = MasterProfile_params->TPCnt0_MasterID->CPT_MasterID;
        	CPT_MasterIDCnt0 = MasterProfile_params->TPCnt0_MasterID->CPT_MasterIDCnt;
        	group_enable_disable0 = MasterProfile_params->TPCnt0_MasterID->MasterIDgroup_Enable;
        }
        else
        {
        	CPT_MasterID0 = NULL;
        	CPT_MasterIDCnt0 = 0;
        	group_enable_disable0 = DBG_TOOLS_CPT_DISABLE;
        }

        if(MasterProfile_params->TPCnt1_MasterID != NULL)
        {
        	CPT_MasterID1 = MasterProfile_params->TPCnt1_MasterID->CPT_MasterID;
        	CPT_MasterIDCnt1 = MasterProfile_params->TPCnt1_MasterID->CPT_MasterIDCnt;
        	group_enable_disable1 = MasterProfile_params->TPCnt1_MasterID->MasterIDgroup_Enable;
        }
        else
        {
        	CPT_MasterID1 = NULL;
        	CPT_MasterIDCnt1 = 0;
        	group_enable_disable1 = DBG_TOOLS_CPT_DISABLE;
        }

        // Configure the CPT for master Bandwidth profiling
        if(eCPT_Success != CPTCfg_MasterProfile( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
        		CPT_MasterID0,
        		CPT_MasterIDCnt0,
        		group_enable_disable0,
                MasterProfile_params->TPCnt0Qual,
                CPT_MasterID1,
        		CPT_MasterIDCnt1,
        		group_enable_disable1,
                MasterProfile_params->TPCnt1Qual))
        {
            // Return error
            return(CTOOLS_CPT_MASTERPROFILE_ERROR);
        }

        // Set the current CPT to be in use
        cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag = 1;
    }
    else
    {
        // Return error
        return(CTOOLS_CPT_MASTERPROFILE_BUSY);
    }

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_totalprofile_open (const ctools_cpt_totalprofilecfg * TotalProfile_params)
{
    eCPT_ModID CPT_ModId;
    CPT_CfgOptions CPTConfig_Params;

	if((TotalProfile_params == NULL) || (cptrace_pntr == NULL))
	{
		//Return error (System trace init is not called or TotalProfile_params not populated
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

    // Populate the CPT Param values

    CPTConfig_Params.pSTMHandle = cptrace_pntr->pSTMhdl;
    CPTConfig_Params.STM_LogMsgEnable = true;                 // Enable STM software message logging from CPTLib
    CPTConfig_Params.STMMessageCh = STM_CPTLogCh;             // Set the STM software message logging channel

    CPTConfig_Params.CPUClockRateMhz = TotalProfile_params->CPUClockRateMhz;                   // Provide CPU clock rate
    CPTConfig_Params.DataOptions = 0;                         // Set supress zero data but since this example enables
                                                                // and disables CPT messages as needed there is not really
                                                                // any 0 data generated
    CPTConfig_Params.pCPT_CallBack = NULL;         // Callback on error

    // Get Sample window size
    CPTConfig_Params.SampleWindowSize = TotalProfile_params->SampleWindowSize;

    // Get the CPT use-case
    CPTConfig_Params.CPT_UseCaseId = eCPT_UseCase_TotalProfile;

    // Check if the CP Tracer is already in use for some other use case scenario
    if(cptrace_pntr->CPT_JobsList[TotalProfile_params->CPT_ModId].in_use_flag != 1)
    {
        // Get the CPT module ID to be programmed
        CPT_ModId = TotalProfile_params->CPT_ModId;

        //Update the CPT use-case in the jobs list
        cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId = CPTConfig_Params.CPT_UseCaseId;

        // Open CPT module
        if (  eCPT_Success != CPT_OpenModule(&(cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle), CPT_ModId,  &CPTConfig_Params ) )
        {
            // Return error
        	return(CTOOLS_CPT_TOTALPROFILE_ERROR);
        }

		ctools_Result result;

		result = CPT_address_filter_setup (TotalProfile_params->Address_Filter_Params, CPT_ModId);

		if(result != CTOOLS_SOK)
		{
			return(result);
		}

        // For Bandwidth profiling, if masterID filtering is disabled, enable all masters (no filtering) by default
        if(TotalProfile_params->TPCnt_MasterID == NULL)
        {
            // Configure the CPT for Total bandwidth profiling
            if(eCPT_Success != CPTCfg_TotalProfile( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
                    NULL,
                    0,
                    DBG_TOOLS_CPT_DISABLE,
                    TotalProfile_params->TPCntQual))
            {
                // Return error
                return(CTOOLS_CPT_TOTALPROFILE_ERROR);
            }
        }
        else
        {
            // Configure the CPT for Total bandwidth profiling
            if(eCPT_Success != CPTCfg_TotalProfile( cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle,
                    TotalProfile_params->TPCnt_MasterID->CPT_MasterID,
                    TotalProfile_params->TPCnt_MasterID->CPT_MasterIDCnt,
                    TotalProfile_params->TPCnt_MasterID->MasterIDgroup_Enable,
                    TotalProfile_params->TPCntQual))
            {
                // Return error
                return(CTOOLS_CPT_TOTALPROFILE_ERROR);
            }
        }

        // Set the current CPT to be in use
        cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag = 1;
    }
    else
    {
        // Return error
        return(CTOOLS_CPT_TOTALPROFILE_BUSY);
    }

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_singlestart(eCPT_ModID CPT_ModId)
{
	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	if((cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId == eCPT_UseCase_Raw) &&
    		(cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag == 1))
    {
    	// Start CP trace for the required CPT according to the use case associated with them
        if (eCPT_Success != CPT_ModuleEnable (cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, eCPT_MsgSelect_Event_NewReq, eCPT_CntSelect_None))
        {
            // Return error
            return(CTOOLS_CPT_SINGLESTART_ERROR);
        }
    }
	else if((cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId == eCPT_UseCases_SysBandwidthProfile) &&
    		(cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag == 1))
	{
		// Start CP trace for the required CPT according to the use case associated with them
        if (eCPT_Success != CPT_ModuleEnable (cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, eCPT_MsgSelect_Statistics, eCPT_CntSelect_None))
        {
            // Return error
            return(CTOOLS_CPT_SINGLESTART_ERROR);
        }
	}
    else if((cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId == eCPT_UseCase_TotalProfile) ||
            (cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId == eCPT_UseCase_MasterProfile) ||
            (cptrace_pntr->CPT_JobsList[CPT_ModId].CPT_UseCaseId == eCPT_UseCases_SysLatencyProfile) &&
            (cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag == 1))
    {
        // Start CP trace for the required CPT according to the use case associated with them
        if (eCPT_Success != CPT_ModuleEnable (cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, eCPT_MsgSelect_Statistics, eCPT_CntSelect_WaitAndGrant))
        {
            // Return error
            return(CTOOLS_CPT_SINGLESTART_ERROR);
        }
    }
    else
    {
        // Return error
        return(CTOOLS_CPT_NOT_OPEN);
    }

    //Set the current CPT to be started
    cptrace_pntr->CPT_JobsList[CPT_ModId].start_flag = 1;

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_singlestop(eCPT_ModID CPT_ModId)
{
	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	if((cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag == 1) && (cptrace_pntr->CPT_JobsList[CPT_ModId].start_flag == 1))
    {
        // Stop the required CPT
        if( eCPT_Success != CPT_ModuleDisable (cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle, eCPT_WaitEnable))
        {
            // Return error
            return(CTOOLS_CPT_SINGLESTOP_ERROR);
        }

        //Clear the CPT start flag
        cptrace_pntr->CPT_JobsList[CPT_ModId].start_flag = 0;
    }
    else
    {
        // Return error
    	if(cptrace_pntr->CPT_JobsList[CPT_ModId].start_flag != 1)
    	{
    		return(CTOOLS_CPT_NOT_STARTED);
    	}
    	else if(cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag != 1)
    	{
    		return(CTOOLS_CPT_NOT_OPEN);
    	}
    }

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_singleclose(eCPT_ModID CPT_ModId)
{
	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	if(cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag == 1)
    {
        // Close the required CP Tracer
        if( eCPT_Success != CPT_CloseModule (&cptrace_pntr->CPT_JobsList[CPT_ModId].pCPT_Handle))
        {
            // Return error
            return(CTOOLS_CPT_SINGLECLOSE_ERROR);
        }

        //Clear the CPT start flag
        cptrace_pntr->CPT_JobsList[CPT_ModId].start_flag = 0;

        //Clear the in use flag
        cptrace_pntr->CPT_JobsList[CPT_ModId].in_use_flag = 0;
    }
    else
    {
        // Return error
        return(CTOOLS_CPT_NOT_OPEN);
    }

	//Success
	return(CTOOLS_SOK);
}

ctools_Result ctools_cpt_globalstart(void)
{
	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	uint32_t i;
	ctools_Result result, error = CTOOLS_SOK;

	for ( i=0; i < eCPT_ModID_Last; i++ )
	{
		//Start all CP Tracers which are already opened
		if(cptrace_pntr->CPT_JobsList[i].in_use_flag == 1)
		{
			result = ctools_cpt_singlestart((eCPT_ModID)i);

			if(result != CTOOLS_SOK)
			{
                //error
                error = result;
			}
		}
	}

    return(error);
}

ctools_Result ctools_cpt_globalstop(void)
{
	uint32_t i;
    ctools_Result result, error = CTOOLS_SOK;

	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	for ( i=0; i < eCPT_ModID_Last; i++ )
	{
		//Start all CP Tracers which are already opened
		if(cptrace_pntr->CPT_JobsList[i].in_use_flag == 1)
		{
			result = ctools_cpt_singlestop((eCPT_ModID)i);

		    if(result != CTOOLS_SOK)
		    {
		        //error
		        error = result;
		    }
		}
	}

    return(error);
}

ctools_Result ctools_cpt_globalclose(void)
{
    uint32_t i;
    ctools_Result result, error = CTOOLS_SOK;

	if(cptrace_pntr == NULL)
	{
		//Return error (System trace is not initialized)
		return(CTOOLS_CPT_ENABLE_ERROR);
	}

	for ( i=0; i < eCPT_ModID_Last; i++ )
	{
		//Start all CP Tracers which are already opened
		if(cptrace_pntr->CPT_JobsList[i].in_use_flag == 1)
		{
			result = ctools_cpt_singleclose((eCPT_ModID)i);

			if(result != CTOOLS_SOK)
			{
                //error
                error = result;
			}
		}
	}

    return(error);
}

/**
@}
*/
/* Nothing past this point */
