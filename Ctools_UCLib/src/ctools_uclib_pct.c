/*
 *  file:  ctools_uclib_pct.c
 *
 *  brief:   
 *      Provides APIs to open and close trace debug jobs at DSP side
 *
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 
/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

/* Include Files. */
#include <string.h>
#include "ctools_uclib_pct.h"
#include "ctools_uclib_osal.h"
#include "DSPTraceExport.h"

#pragma DATA_SECTION(PCT_INST, ".bss:ctoolsuc_nonshared")
ctools_pct_inst_t* PCT_INST;

//External variables
extern ctools_dsptrace_inst_t*  CTOOLS_DSPTRC_INST;

static void pct_start_func(void)
{
   asm (" nop ");
   return;
}

static void pct_stop_func(void)
{
   asm (" nop ");
   return;
}

static ctools_Result pct_setup(void* start, void* stop)
{
    PCT_INST = (ctools_pct_inst_t*)cTools_memAlloc(sizeof (ctools_pct_inst_t) );
    memset(PCT_INST, 0, sizeof(ctools_pct_inst_t));

    // Perform AET initialization
    AET_init();

    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }

   /* Start the AET JOB for the PC and Timing trace */
   /* Set up AET to start generating trace */
    PCT_INST->pctStartParams    = AET_JOBPARAMS; // Initialize Job Parameter Structure
    PCT_INST->pctStartParams.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA; //Enable capture of timing and program address trace streams
    PCT_INST->pctStartParams.traceActive = AET_TRACE_ACTIVE; //Make Trace active
    PCT_INST->pctStartParams.programAddress = (uint32_t) start ;   

    /* Set up the desired start trace AET job */
    if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &PCT_INST->pctStartParams))
    {
        return(CTOOLS_AET_SETUPJOB_ERROR);
    }

    PCT_INST->pctEndParams      = AET_JOBPARAMS; // Initialize Job Parameter Structure

    /* Setup PC trace stop once we hit the stop symbol for the PC */
    PCT_INST->pctEndParams.traceTriggers = AET_TRACE_STOP_PC | AET_TRACE_STOP_TIMING;
    PCT_INST->pctEndParams.traceActive   = AET_TRACE_INACTIVE;
    PCT_INST->pctEndParams.programAddress = (uint32_t)stop ;

    if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &PCT_INST->pctEndParams))
    {
       return(CTOOLS_AET_SETUPJOB_ERROR);
    }    

    /* Enable AET */
    if (AET_enable())
    {
      return(CTOOLS_AET_ENABLE_ERROR);
    }  

    return (CTOOLS_SOK);
}

ctools_Result ctools_pct_start_exc (void)
{
    if (CTOOLS_DSPTRACE_UC_FREE == CTOOLS_DSPTRC_INST->state)
    {
        CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_PCT_EXC;
    }
    else
    {
        return (CTOOLS_RESOURCE_BUSY);
    }
    
     PCT_INST = (ctools_pct_inst_t*)cTools_memAlloc(sizeof (ctools_pct_inst_t) );
     memset(PCT_INST, 0, sizeof(ctools_pct_inst_t));

    // Perform AET initialization
    AET_init();

    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }
     
    /* Set up AET to start generating trace */
    PCT_INST->pctStartParams    = AET_JOBPARAMS; // Initialize Job Parameter Structure
    PCT_INST->pctStartParams.programAddress = (uint32_t) &pct_start_func ;
    PCT_INST->pctStartParams.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA; //Enable capture of timing and program address trace streams
    PCT_INST->pctStartParams.traceActive = AET_TRACE_ACTIVE; //Make Trace active

    /* Set up the desired start trace AET job */
    if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &PCT_INST->pctStartParams))
    {
      return(CTOOLS_AET_SETUPJOB_ERROR);
    }

    {
       PCT_INST->excEndParams      = AET_JOBPARAMS; // Initialize Job Parameter Structure
       PCT_INST->timeTrcEndParams  = AET_JOBPARAMS; // Initialize Job Parameter Structure       
       
       // Setup PC trace stop during the exception 
       PCT_INST->excEndParams.eventNumber[0] = AET_EVT_MISC_EXC;
       PCT_INST->excEndParams.triggerType = AET_TRIG_TRACE_PCSTOP;
       if (AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &PCT_INST->excEndParams))
       {
         return(CTOOLS_AET_SETUPJOB_ERROR);
       }

       /* Add timing trace stop job to the PC trace stop job created above */
       PCT_INST->timeTrcEndParams.triggerType = AET_TRIG_TRACE_TIMINGSTOP;
       PCT_INST->timeTrcEndParams.jobIndex = PCT_INST->excEndParams.jobIndex;
       PCT_INST->timeTrcEndParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;

       if (AET_setupJob(AET_JOB_ADD_TRIGGER, &PCT_INST->timeTrcEndParams))
       {
          return(CTOOLS_AET_SETUPJOB_ERROR);
       }
    }   

    /* Enable AET */
    if (AET_enable())
    {
      return(CTOOLS_AET_ENABLE_ERROR);
    }

    /* Invoke the trace triggering function for the ETB, if application did not provide it */
    pct_start_func();

    // Exception trace job setup complete    
    return(CTOOLS_SOK);
  
}

ctools_Result ctools_pct_setup(const void* pct_start_symb, const void* pct_stop_symb)
{
    ctools_Result ret; 
    
    /* Check if valid parameters are passed */
    if ((pct_start_symb == (void*) NULL) ||
       (pct_stop_symb   == (void*) NULL))
    return (CTOOLS_INVALID_PARAM);   

    if (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_FREE)
        CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_PCT_SETUP;
    else
        return (CTOOLS_RESOURCE_BUSY);    

    ret =  pct_setup((void*) pct_start_symb, (void*) pct_stop_symb);
    return (ret);
}

ctools_Result ctools_pct_start_now(void)
{
    ctools_Result ret;

    if (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_FREE)
        CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_PCT_NOW;
    else
        return (CTOOLS_RESOURCE_BUSY);    

    ret =  pct_setup((void*) pct_start_func, (void*) pct_stop_func);

    /* triggers the PC trace */
    if (ret == CTOOLS_SOK)
    {
        pct_start_func();    
        return (CTOOLS_SOK);       
    }
    else
    {
        return (ret);
    }

}

ctools_Result ctools_pct_stop_now (void)
{
    if (CTOOLS_DSPTRC_INST->state != CTOOLS_DSPTRACE_UC_PCT_NOW)
    {
        return (CTOOLS_INVALID_CALL_SEQ);    
    }

    /* triggers the PC trace if the start symbol is null */
    pct_stop_func();

    return (CTOOLS_SOK);
}

ctools_Result ctools_pct_terminate_all (void)
{
    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }

	//Terminate all DSP trace
	uint32_t traceState = 0;
	uint32_t new_traceState = DSPTRACE_SET_TEND;
	DSPTrace_getState(CTOOLS_DSPTRC_INST->pDSPHandle, &traceState);
	if ( (traceState & DSPTRACE_STATE_TEND) != DSPTRACE_STATE_TEND ){

		DSPTrace_setState(CTOOLS_DSPTRC_INST->pDSPHandle, new_traceState);
	}

    return (CTOOLS_SOK);
}

ctools_Result ctools_pct_close(void)
{
	AET_releaseJob(PCT_INST->pctStartParams.jobIndex); // Release the start trace AET job

	/* Release the exception jobs if it is for exception case */
	if (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_PCT_EXC)
	{
		AET_releaseJob(PCT_INST->excEndParams.jobIndex); //Release the end trace AET job
		AET_releaseJob(PCT_INST->timeTrcEndParams.jobIndex); // Release the job for time trace
	}
	else if ( (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_PCT_SETUP) ||
			   (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_PCT_NOW) )
	{
		AET_releaseJob(PCT_INST->pctEndParams.jobIndex); //Release the end trace AET job;
	}

	/* Free the allocated memory */
	if (PCT_INST != NULL)
	{
		 cTools_memFree((void*)PCT_INST);
		 PCT_INST = NULL;
	}

	CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_FREE;

	return (CTOOLS_SOK);
}

/* Nothing past this point */
