/*
 *  file:  ctools_uclib_memwatch.c
 *
 *  brief:   
 *      Provides APIs to watch a memory range using debug tools job
 *
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 
/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

/* Include Files */
#include <string.h>
#include "ctools_uclib_memwatch.h"
#include "ctools_uclib_osal.h"

#pragma DATA_SECTION(memwatch_pntr, ".bss:ctoolsuc_nonshared")
ctools_memwatch_resources_pntr memwatch_pntr;

//External variables
extern ctools_dsptrace_inst_t*  CTOOLS_DSPTRC_INST;

// Internal functions

static ctools_Result ctools_memwatch_setup (ctools_memwatch_opts_e memwatch_opt, uint32_t data_StartAddress, uint32_t data_EndAddress)
{
    if(memwatch_opt == CTOOLS_MEMWATCH_AETINT)
    {
        if((TCU_CNTL & 0x00020000) != 0x00020000)
        {
            //Enable AET interrupt in TCU_CNTL register
        	if((TCU_CNTL & 0x80000000) != 0x80000000)
        	{
        		TCU_CNTL = 0x1;
        	}

            TCU_CNTL |= 0x000A0000;
            TCU_CNTL |= 0x80020002;
        }
    }

    // Perform AET initialization
    AET_init();

    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }

    /*
     Initialize AET Job Parameter Structure
     */
    memwatch_pntr->memwatch_params    = AET_JOBPARAMS; // Initialize Job Parameter Structure

    /* Provide Memory Range start and end address, which need to be monitored */ 
    memwatch_pntr->memwatch_params.dataStartAddress = data_StartAddress;
    memwatch_pntr->memwatch_params.dataEndAddress =  data_EndAddress;

    if(memwatch_opt == CTOOLS_MEMWATCH_AETINT)
    {
        //Update memory watch mode
    	memwatch_pntr->memwatch_opt = CTOOLS_MEMWATCH_AETINT;

    	// Select the trigger type to generate an AET interrupt
    	memwatch_pntr->memwatch_params.triggerType = AET_TRIG_AINT;
    }
    else if(memwatch_opt == CTOOLS_MEMWATCH_STORE_PC)
    {
    	//Update memory watch mode
    	memwatch_pntr->memwatch_opt = CTOOLS_MEMWATCH_STORE_PC;

    	// Select the trigger type to store program address
    	memwatch_pntr->memwatch_params.triggerType = AET_TRIG_TRACE_STORE_PC;
    }

    memwatch_pntr->memwatch_params.readWrite = AET_WATCH_WRITE;
    memwatch_pntr->memwatch_params.refSize = AET_REF_SIZE_WORD;

    /* Setup the AET job with selected parameters */
    if (AET_setupJob(AET_JOB_TRIG_ON_DATA_RANGE, &memwatch_pntr->memwatch_params))
    {
        return(CTOOLS_AET_SETUPJOB_ERROR);
    }

    memwatch_pntr->memwatch_jobnumber = memwatch_pntr->memwatch_params.jobIndex;

    /* Enable AET */
    if (AET_enable())
    {
      return(CTOOLS_AET_ENABLE_ERROR);
    }
	
	return(CTOOLS_SOK);
}

ctools_Result ctools_memwatch_store_pc_setup (uint32_t * data_StartAddress, uint32_t * data_EndAddress)
{
	ctools_Result result;

    if (CTOOLS_DSPTRC_INST->state == CTOOLS_DSPTRACE_UC_FREE)
        CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_MEMWATCH;
    else
        return (CTOOLS_RESOURCE_BUSY);
    
    //Allocate memory resources for memory watch
    memwatch_pntr = (ctools_memwatch_resources_pntr)cTools_memAlloc(sizeof(ctools_memwatch_resources));
	
	memset(memwatch_pntr, 0, sizeof(ctools_memwatch_resources));

	result = ctools_memwatch_setup(CTOOLS_MEMWATCH_STORE_PC, (uint32_t)data_StartAddress, (uint32_t)data_EndAddress);

	return(result);
}

ctools_Result ctools_memwatch_aetint_setup (uint32_t * data_StartAddress, uint32_t * data_EndAddress)
{
	ctools_Result result;

   /* memory watch with storing PC trace is setup already */
    if (CTOOLS_DSPTRACE_UC_MEMWATCH == CTOOLS_DSPTRC_INST->state)
    {
    	return (CTOOLS_RESOURCE_BUSY);
    }

    //Allocate memory resources for memory watch
    memwatch_pntr = (ctools_memwatch_resources_pntr)cTools_memAlloc(sizeof(ctools_memwatch_resources));

    memset(memwatch_pntr, 0, sizeof(ctools_memwatch_resources));

	result = ctools_memwatch_setup(CTOOLS_MEMWATCH_AETINT, (uint32_t)data_StartAddress, (uint32_t)data_EndAddress);

	return(result);
}

ctools_Result ctools_memwatch_disable (void)
{
	//Release the memory watch AET job
	AET_releaseJob(memwatch_pntr->memwatch_jobnumber);
	
    //Success
    return (CTOOLS_SOK);
}

ctools_Result ctools_memwatch_enable (void)
{
	//Re-enable the AET memory watch point

	ctools_Result result;

	result = ctools_memwatch_setup(memwatch_pntr->memwatch_opt, memwatch_pntr->memwatch_params.dataStartAddress, memwatch_pntr->memwatch_params.dataEndAddress);

	return(result);
}

ctools_Result ctools_memwatch_close (void)
{
	//Release the memory watch AET job
	AET_releaseJob(memwatch_pntr->memwatch_jobnumber);
	AET_release();

	//Free up the allocated memory
	if (memwatch_pntr != NULL)
    {
		cTools_memFree(memwatch_pntr);
		memwatch_pntr = NULL;
	}

	//Success
	return (CTOOLS_SOK);
}

/**
@}
*/
