/*
 * file: ctools_uclib.c
 *
 * brief:
 *     debug tools public functions implementation. 
 *
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <string.h>
#include "ctools_uclib.h"
#include "ctools_uclib_osal.h"
#include "ctools_uclib_stm_device.h"
#include "StmLibrary.h"
#include "DSPTraceExport.h"

#pragma DATA_SECTION(CTOOLS_DSPTRC_INST, ".bss:ctoolsuc_nonshared")
ctools_dsptrace_inst_t*  CTOOLS_DSPTRC_INST;

#pragma DATA_SECTION(cptrace_pntr, ".bss:ctoolsuc_nonshared")
ctools_cpt_resources_pntr cptrace_pntr;

// External variables
extern ctools_etb_inst_t* CTOOLS_DSPETB_INST;
extern ctools_etb_inst_t* CTOOLS_SYSETB_INST;


ctools_Result ctools_dsptrace_init (void)
{ 
    DSPTrace_errorCallback  pDSPErrorCallBack=0;
    eDSPTrace_Error         dspRet;
    eETB_Error  etbRet;
    ETBStatus etbStatus;
    uint32_t traceState = 0;
    uint32_t new_traceState = DSPTRACE_CLR_TEND;
	
	if(CTOOLS_DSPETB_INST != NULL)
	{
		if(CTOOLS_DSPETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
		{
			//Get ETB trace capture enable status
			etbRet= ETB_status(CTOOLS_DSPETB_INST->pETBHandle, &etbStatus);
			if(etbRet != eETB_Success)
			{
				//Return error
			    return(CTOOLS_DSPETB_NOT_ENABLED);
			}

			if((etbStatus.ETB_TraceCaptureEn & 0x1) == 0)
			{
			    //Return error
		        return(CTOOLS_DSPETB_NOT_ENABLED);
			}
		}
	}

	CTOOLS_DSPTRC_INST = (ctools_dsptrace_inst_t*)cTools_memAlloc(sizeof (ctools_dsptrace_inst_t) );
	memset(CTOOLS_DSPTRC_INST, 0, sizeof(ctools_dsptrace_inst_t));

	CTOOLS_DSPTRC_INST->state = CTOOLS_DSPTRACE_UC_FREE;

	/*** Setup Trace Export ***/
	/* Open DSP Trace export module */
	dspRet = DSPTrace_open( pDSPErrorCallBack,&CTOOLS_DSPTRC_INST->pDSPHandle);
	if(dspRet != eDSPTrace_Success)
	{
	   return(CTOOLS_DSPTRACE_OPEN_ERROR);
	}
	/* Setup trace export clock to FCLK/3 */
	dspRet = DSPTrace_setClock(CTOOLS_DSPTRC_INST->pDSPHandle, 3);
	if(dspRet != eDSPTrace_Success)
	{
	   return(CTOOLS_DSPTRACE_SETCLK_ERROR);
	}

	dspRet= DSPTrace_enable(CTOOLS_DSPTRC_INST->pDSPHandle, 0, 0);
	if(dspRet != eDSPTrace_Success)
	{
	  return(CTOOLS_DSPTRACE_ENABLE_ERROR);
	}

    // Perform AET initialization
    AET_init();

    // Claim the AET resource
    if (AET_claim())
    {
      return(CTOOLS_AET_CLAIM_ERROR);
    }

	//Clear DSP trace TEND state

    DSPTrace_getState(CTOOLS_DSPTRC_INST->pDSPHandle, &traceState);
    if ( (traceState & DSPTRACE_STATE_TEND) == DSPTRACE_STATE_TEND ){

        DSPTrace_setState(CTOOLS_DSPTRC_INST->pDSPHandle, new_traceState);
    }

    DSPTrace_getState(CTOOLS_DSPTRC_INST->pDSPHandle, &traceState);

    if ( (traceState & DSPTRACE_STATE_TEND) == DSPTRACE_STATE_TEND )
    {
    	return(CTOOLS_DSPTRACE_TEND_ERROR);
    }

	return (CTOOLS_SOK);
}

ctools_Result ctools_dsptrace_shutdown  (void)
{
     eDSPTrace_Error         dspRet;

     /* Disable DSP Trace Export */
     dspRet= DSPTrace_disable(CTOOLS_DSPTRC_INST->pDSPHandle);
     if(dspRet != eDSPTrace_Success)
     {
       return(CTOOLS_DSPTRACE_DISABLE_ERROR);
     } 

     dspRet= DSPTrace_close(CTOOLS_DSPTRC_INST->pDSPHandle);
     if(dspRet != eDSPTrace_Success)
     {
       return(CTOOLS_DSPTRACE_CLOSE_ERROR);
     }

    /* Free the allocated memory */
    if (CTOOLS_DSPTRC_INST != NULL)
    {
        cTools_memFree((void*)CTOOLS_DSPTRC_INST);
        CTOOLS_DSPTRC_INST = NULL;
    }    
 
    // Release the ownership of AET resources
     if (AET_release() != AET_SOK)
     {
       return(CTOOLS_AET_RELJOB_ERROR);
     }

     return (CTOOLS_SOK);
}

ctools_Result ctools_systemtrace_init (const ctools_sys_trace_opts_e sys_trace_options)
{
    eETB_Error  etbRet;
    ETBStatus etbStatus;
	
	if(CTOOLS_SYSETB_INST != NULL)
	{
		if(CTOOLS_SYSETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
		{
			//Get ETB trace capture enable status
			etbRet= ETB_status(CTOOLS_SYSETB_INST->pETBHandle, &etbStatus);
			if(etbRet != eETB_Success)
			{
				//Return error
				return(CTOOLS_SYSETB_NOT_ENABLED);
			}
			
			if((etbStatus.ETB_TraceCaptureEn & 0x1) == 0)
			{
				//Return error
				return(CTOOLS_SYSETB_NOT_ENABLED);
			}
		}
	}

	//Allocate memory resources for system trace
	if(sys_trace_options == CTOOLS_SYS_TRACE_SW_MSGS)
	{
		//cTools_memAlloc() only for the resources required for SW messages
	}
	else
	{
		//Allocate resources for CP Tracers
		cptrace_pntr = (ctools_cpt_resources_pntr)cTools_memAlloc(sizeof(ctools_cpt_resources));

		memset(cptrace_pntr, 0, sizeof(ctools_cpt_resources));
	}

	//////////////////////////////////////////////////////
	//Initialize STM Library                            //
	//////////////////////////////////////////////////////

	STMBufObj * pSTMBufInfo = NULL;                     // Set pSTMBufInfo to NULL to set STM Lib for blocking IO
	STMConfigObj STMConfigInfo;                         // STMLib configuration options struct

	STMConfigInfo.STM_XportBaseAddr=STM_XPORT_BASE_ADDR;
	STMConfigInfo.STM_ChannelResolution=STM_CHAN_RESOLUTION;
	STMConfigInfo.STM_CntlBaseAddr = STM_CONFIG_BASE_ADDR;
	STMConfigInfo.xmit_printf_mode = eSend_optimized;
	STMConfigInfo.optimize_strings = true;              // Not forming any strings dynamically so optimized strings ok
	STMConfigInfo.pCallBack = NULL;                     // Set STM Callback to NULL

	// Open STMLib
	if(sys_trace_options == CTOOLS_SYS_TRACE_SW_MSGS)
	{
		//open STM handle and assign to the SW messages only resource object
	}
	else
	{
		cptrace_pntr->pSTMhdl = STMXport_open( pSTMBufInfo, &STMConfigInfo);
		if ( NULL == cptrace_pntr->pSTMhdl )
		{
			// Return error
			return(CTOOLS_SYSTEMTRACE_ENABLE_ERROR);
		}
	}

	//Configure STM, only if using ETB
	if(CTOOLS_SYSETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
	{
		eSTM_STATUS status;

		//Setup STM routing
		Config_STMData_Routing();

		status = Config_STM_for_ETB(cptrace_pntr->pSTMhdl);

		if(status != eSTM_SUCCESS)
		{
			//Return error
			return(CTOOLS_SYSTEMTRACE_ENABLE_ERROR);
		}
	}

	//Success
	return (CTOOLS_SOK);
}

ctools_Result ctools_systemtrace_getdcmdata (STM_DCM_InfoObj * DCM_Info)
{
	if(cptrace_pntr->pSTMhdl != NULL)
	{
		//Get STM DCM data, only if using ETB
		if(CTOOLS_SYSETB_INST->etbDrainOpts != CTOOLS_DRAIN_NONE)
		{
			STMXport_getDCMInfo(cptrace_pntr->pSTMhdl, DCM_Info);
		}

	    // Success
	    return (CTOOLS_SOK);
	}
	else
	{
	    // Return error
	    return (CTOOLS_INVALID_PARAM);
	}
}

ctools_Result ctools_systemtrace_shutdown (ctools_sys_trace_opts_e sys_trace_options)
{
	// Close STMLib handle
	if(sys_trace_options == CTOOLS_SYS_TRACE_SW_MSGS)
	{
	    //close STM handle associated with only SW messages resource object
	}
	else
	{
		STMXport_close (cptrace_pntr->pSTMhdl);
	}
	
    //Free memory resources for system trace
    if(sys_trace_options == CTOOLS_SYS_TRACE_SW_MSGS) 
    {
	    //cTools_memFree() only for the resources required for SW messages
	}
	else
	{
	    //Free resources for CP Tracers
		if (cptrace_pntr != NULL)
        {
			cTools_memFree(cptrace_pntr);
			cptrace_pntr = NULL;
		}
	}
	
    // Success
    return (CTOOLS_SOK);
}

/* Nothing past this point */
