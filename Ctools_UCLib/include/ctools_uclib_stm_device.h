/*
 *   @file  ctools_uclib_stm_device.h
 *
 *   @brief   
 *       This is the CTOOLS use case library device specific STM include file.
 *
  *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
 */

#ifndef __CTOOLS_UCLIB_STM_DEVICE_H
#define __CTOOLS_UCLIB_STM_DEVICE_H

#include <stdio.h>
#include <stdint.h>
#include "StmLibrary.h"

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Base Address Definitions
 ******************************************************************************/

#if defined(C66AK2Hxx) || defined(TCI6630K2L) || defined(C66AK2Exx) || defined(_66AK2Gxx)

#define STM_XPORT_BASE_ADDR       0x20000000
#define STM_CHAN_RESOLUTION       0x1000
#define STM_CONFIG_BASE_ADDR      0x03018000

#define MIPI_STM_TBR_SIZE         0x8000 //32 KB

#define ETB_USING_TWP 1

#elif defined(TCI6614)

#define STM_XPORT_BASE_ADDR       0x20000000
#define STM_CHAN_RESOLUTION       0x1000
#define STM_CONFIG_BASE_ADDR      0x025A1000


/* Trace Funnel */
#define C6614_TF_CNTL_REG         0x025A4000
#define C6614_TF_LOCK_REG         0x025A4FB0

#define TF_STM_ENABLE_VALUE       0x3FF
#define TF_UNLOCK_VALUE           0xC5ACCE55

#define ETB_USING_TWP 0

#else

#define STM_XPORT_BASE_ADDR       0x20000000
#define STM_CHAN_RESOLUTION       0x1000
#define STM_CONFIG_BASE_ADDR      0x02421000

#define ETB_USING_TWP 0

#endif

/*******************************************************************************
 * Required Definitions
 ******************************************************************************/
#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

/*******************************************************************************
 * Device Specific Function Prototypes
 ******************************************************************************/
eSTM_STATUS Config_STM_for_ETB(STMHandle* pSTMHandle);
void Config_STMData_Routing(void);

#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_STM_DEVICE_H*/

/* Nothing past this point */
