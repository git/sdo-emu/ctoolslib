/*
 *   @file  ctools_uclib_etb.h
 *
 *   @brief   
 *       Header file for ETB specific Initialization, drain and shutdown functions.
 *
  *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
 */

#ifndef __CTOOLS_UCLIB_ETB_H
#define __CTOOLS_UCLIB_ETB_H

//Include files
//Using forward slashes in the relative serach path for linux compatibility
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../Ctools_UCLib/include/ctools_uclib_edma3.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */


/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{
*/

/**
 * @brief Ctools ETB configuration structure
 *
 */

typedef struct
{
  ctools_edmaConfig_t edmaConfig; /*!< Edma configuration if ETB is accessed by EDMA */
  eETB_Mode           etb_mode;    /*!< ETB drain mode */
} ctools_etb_config_t;

/**
@}
*/

/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/
 
 /** @addtogroup CTOOLS_USECASE_FUNCTION
@{ 
*/

/** 
 *  @b Description
 *  @n  
 *      Initializes DSP ETB or System ETB
 *
 *  @param[in]  etb_drain_opt
 *      etb drain option for the traces captured in ETB
 *
 *  @param[in] config
 *      pointer to trace configurations
 *
 *  @param[in] etb_type
 *      Specifies whether the ETB being initialized is a DSP ETB or System ETB
 * 
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Result - Init failure
 *
 *  \par Please Note:
 *       In a Multicore environment:
 *       	- DSP ETB initialization should be done seperately from individual cores (Each core has to call its own ETB initialization API).
 *       	  A single core (master core) cannot perform ETB initialization for all the other cores.
 *       	- System ETB initialization should only be done from a single core (master core). If user is not using Emulator (XDS560V2 or XDS560 PRO)
 *       	  for draining system trace, then before calling ::ctools_systemtrace_init all cores except the master core should wait until the system ETB is initialized.
 *       	  ::ctools_systemtrace_init will return error if system ETB is not yet initialized.
 *
 */

ctools_Result ctools_etb_init (const ctools_etb_drain_e etb_drain_opt, const ctools_etb_config_t* config, ctools_etb_types_e etb_type);

/**
 *  @b Description
 *  @n
 *      CPU based draining of DSP ETB or System ETB.
 *
 *   @param[in]  data_p
 *      pointer to the linear buffer
 *
 *   @param[in]  size_in
 *      Size of the 32-bit linear buffer (should be multiple of 4k)
 *
 *   @param[out]  size_out
 *      number of valid 32-bit words in the linear buffer
 *
 *   @param[out]  wrap_flag
 *      pointer to the ETB wrap flag. Specifies whether ETB is wrapped or not
 *
 *   @param[in] etb_type
 *      Specifies whether the ETB being drained is a DSP ETB or System ETB
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - trace job close err return status - CTOOLS_RESOURCE_CLOSE_ERROR
 *
 *  \par Please Note:
 *       In a Multicore environment:
 *       	- DSP ETB drain should be done seperately from individual cores (Each core has to call its own ETB drain API).
 *       	  A single core (master core) cannot perform ETB drain for all the other cores.
 *       	- System ETB drain should only be done from a single core (master core which initialized the system ETB) and all other cores
 *       	  should not perform System ETB drain seperately.
 *
 */

ctools_Result ctools_etb_cpu_drain (const void *data_p, uint32_t size_in, uint32_t *size_out, uint32_t *wrap_flag, ctools_etb_types_e etb_type);

/**
 *  @b Description
 *  @n
 *      EDMA based draining of DSP ETB or System ETB.
 *
 *   @param[in]  pct_res
 *      pointer to the edma result structure
 *
 *   @param[in] etb_type
 *      Specifies whether the ETB being drained is a DSP ETB or System ETB
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - trace job close err return status - CTOOLS_RESOURCE_CLOSE_ERROR
 *
 *  \par Please Note:
 *       In a Multicore environment:
 *       	- DSP ETB drain should be done seperately from individual cores (Each core has to call its own ETB drain API).
 *       	  A single core (master core) cannot perform ETB drain for all the other cores.
 *       	- System ETB drain should only be done from a single core (master core which initialized the system ETB) and all other cores
 *       	  should not perform System ETB drain seperately.
 *
 */

ctools_Result ctools_etb_edma_drain  (ctools_edma_result_t* pct_res, ctools_etb_types_e etb_type);

 /**
 *  @b Description
 *  @n  
 *      Get the EDMA resouces for the ETB drain
 *
 *
 *  @param[in]  num_params
 *      number of parameters needed to drain ETB using EDMA
 *
 *  @param[in]  num_channels
 *      number of channels needed to drain ETB using EDMA
 * 
 *  @retval
 *      Success - DBG_TOOLS_SOK
 *
 */
 
ctools_Result ctools_edma_resrc_get (uint32_t* num_params, uint32_t* num_channels);

/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_ETB_H*/

/* Nothing past this point */
