/*
 *   @file  ctools_uclib_int.h
 *
 *   @brief
 *      Ctools use case library internal functions, definitions.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/


#ifndef __CTOOLS_UCLIB_INT_H
#define __CTOOLS_UCLIB_INT_H

#ifdef __cplusplus
extern "C" {
#endif


//Using forward slashes in the relative serach path for linux compatibility

#include "../../ETBLib/include/ETBInterface.h"
#include "../../DSPTraceLib/include/DSPTraceExport.h"

/* local state of the dsp trace */
typedef enum
{
   CTOOLS_DSPTRACE_UC_PCT_EXC, /*!< DSPTRACE for PCT Exception use case */
   CTOOLS_DSPTRACE_UC_PCT_SETUP, /*!< DSPTRACE for PCT Setup use case*/
   CTOOLS_DSPTRACE_UC_PCT_NOW,   /*!< DSPTRACE for PCT Start Now, Stop Now use case*/   
   CTOOLS_DSPTRACE_UC_MEMWATCH, /*!< DSPTRACE memory watch use case */
   CTOOLS_DSPTRACE_UC_STATPROF, /*!< DSPTRACE statistical profiling use case */   
   CTOOLS_DSPTRACE_UC_FREE  /*!< DSPTRACE use case free */
} ctools_dsptrace_uc_state_e;

/**
@addtogroup CTOOLS_USECASE_SYMBOL
@{
*/

/*! \par CTOOLS_ETB_SIZE
   Size of the Core ETB is 4K Bytes 
*/
#define CTOOLS_ETB_SIZE               4096 

/*! \par CTOOLS_MIN_DATA_SIZE
   Size of the CTOOLS_MIN_DATA_SIZE is 4K Bytes 
*/
#define CTOOLS_MIN_DATA_SIZE          CTOOLS_ETB_SIZE


/*! \par CTOOLS_BYTE_SWAP32
   if upload to file transport is enabled 
*/
#define CTOOLS_BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

/** CTOOLS Low level Driver return and Error Codes */
/** CTOOLS successful return code */
#define CTOOLS_SOK                                    0
/** CTOOLS Error Base */       
#define CTOOLS_EBASE                                  (-128)
/** CTOOLS  invalid parameter */
#define CTOOLS_INVALID_PARAM                          CTOOLS_EBASE-1
/** CTOOLS not initialized */
#define CTOOLS_NOT_INITIALIZED                        CTOOLS_EBASE-2
/** CTOOLS etb open error */
#define CTOOLS_ETB_OPEN_ERROR                         CTOOLS_EBASE-3
/** CTOOLS etb enable error */
#define CTOOLS_ETB_ENABLE_ERROR                       CTOOLS_EBASE-4
/** CTOOLS etb flush error */
#define CTOOLS_ETB_FLUSH_ERROR                        CTOOLS_EBASE-5
/** CTOOLS etb flush dma error */
#define CTOOLS_ETB_FLUSHDMA_ERROR                     CTOOLS_EBASE-6
/** CTOOLS etb get status error */
#define CTOOLS_ETB_GETSTATUS_ERROR                    CTOOLS_EBASE-7
/** CTOOLS etb read error */
#define CTOOLS_ETB_READ_ERROR                         CTOOLS_EBASE-8
/** CTOOLS etb disable error */
#define CTOOLS_ETB_DISABLE_ERROR                      CTOOLS_EBASE-9
/** CTOOLS etb close error */
#define CTOOLS_ETB_CLOSE_ERROR                        CTOOLS_EBASE-10
/** CTOOLS etb general error */
#define CTOOLS_ETB_GEN_ERROR                          CTOOLS_EBASE-11
/** CTOOLS dsptrace open error */
#define CTOOLS_DSPTRACE_OPEN_ERROR                    CTOOLS_EBASE-12
/** CTOOLS dsptrace setclk error */
#define CTOOLS_DSPTRACE_SETCLK_ERROR                  CTOOLS_EBASE-13
/** CTOOLS dsptrace enable error */
#define CTOOLS_DSPTRACE_ENABLE_ERROR                  CTOOLS_EBASE-14
/** CTOOLS dsptrace disable error */
#define CTOOLS_DSPTRACE_DISABLE_ERROR                 CTOOLS_EBASE-15
/** CTOOLS dsptrace close error */
#define CTOOLS_DSPTRACE_CLOSE_ERROR                   CTOOLS_EBASE-16
/** CTOOLS dsptrace general error */
#define CTOOLS_DSPTRACE_GEN_ERROR                     CTOOLS_EBASE-17
/** CTOOLS AET claim error */
#define CTOOLS_AET_CLAIM_ERROR                        CTOOLS_EBASE-18
/** CTOOLS AET setupjob error */
#define CTOOLS_AET_SETUPJOB_ERROR                     CTOOLS_EBASE-19
/** CTOOLS AET release job error */
#define CTOOLS_AET_RELJOB_ERROR                       CTOOLS_EBASE-20
/** CTOOLS AET enable error */
#define CTOOLS_AET_ENABLE_ERROR                       CTOOLS_EBASE-21
/** CTOOLS no buffer for ETB error */
#define CTOOLS_NOBUF_FOR_ETBDRAIN                     CTOOLS_EBASE-22
/** CTOOLS unsupported features */
#define CTOOLS_UNSUPPORTED                            CTOOLS_EBASE-23
/** CTOOLS already running */
#define CTOOLS_RESOURCE_BUSY                          CTOOLS_EBASE-24
/** CTOOLS file operations error*/
#define CTOOLS_FILEOPERATION_ERR                      CTOOLS_EBASE-25
/** CTOOLS invalid call sequence triggered */
#define CTOOLS_INVALID_CALL_SEQ                       CTOOLS_EBASE-26
/** CTOOLS CP Tracer Enable error, unable to enable CP tracer as system trace is not initialized */
#define CTOOLS_CPT_ENABLE_ERROR                       CTOOLS_EBASE-27
/** CTOOLS CP Tracer System profiling error */
#define CTOOLS_CPT_SYSPROFILE_ERROR                   CTOOLS_EBASE-28
/** CTOOLS CP Tracer System profiling busy */
#define CTOOLS_CPT_SYSPROFILE_BUSY                    CTOOLS_EBASE-29
/** CTOOLS CP Tracer Event profiling error */
#define CTOOLS_CPT_EVTPROFILE_ERROR                   CTOOLS_EBASE-30
/** CTOOLS CP Tracer Event profiling busy */
#define CTOOLS_CPT_EVTPROFILE_BUSY                    CTOOLS_EBASE-31
/** CTOOLS CP Tracer Address filter configuration error */
#define CTOOLS_CPT_ADDRFILTER_ERROR                   CTOOLS_EBASE-32
/** CTOOLS CP Tracer Master profiling error */
#define CTOOLS_CPT_MASTERPROFILE_ERROR                CTOOLS_EBASE-33
/** CTOOLS CP Tracer Master profiling busy */
#define CTOOLS_CPT_MASTERPROFILE_BUSY                 CTOOLS_EBASE-34
/** CTOOLS CP Tracer Total profiling error */
#define CTOOLS_CPT_TOTALPROFILE_ERROR                 CTOOLS_EBASE-35
/** CTOOLS CP Tracer Total profiling busy */
#define CTOOLS_CPT_TOTALPROFILE_BUSY                  CTOOLS_EBASE-36
/** CTOOLS CP Tracer single start error */
#define CTOOLS_CPT_SINGLESTART_ERROR                  CTOOLS_EBASE-37
/** CTOOLS CP Tracer single stop error */
#define CTOOLS_CPT_SINGLESTOP_ERROR                   CTOOLS_EBASE-38
/** CTOOLS CP Tracer single close error */
#define CTOOLS_CPT_SINGLECLOSE_ERROR                  CTOOLS_EBASE-39
/** CTOOLS CP Tracer not open */
#define CTOOLS_CPT_NOT_OPEN                           CTOOLS_EBASE-40
/** CTOOLS CP Tracer not started */
#define CTOOLS_CPT_NOT_STARTED                        CTOOLS_EBASE-41
/** CTOOLS System trace enable error */
#define CTOOLS_SYSTEMTRACE_ENABLE_ERROR               CTOOLS_EBASE-42
/** CTOOLS System ETB not enabled */
#define CTOOLS_SYSETB_NOT_ENABLED                     CTOOLS_EBASE-43
/** CTOOLS DSP ETB not enabled */
#define CTOOLS_DSPETB_NOT_ENABLED                     CTOOLS_EBASE-44
/** CTOOLS EDMA ETB extension not supported */
#define CTOOLS_ETB_EDMA_DRAIN_NOT_SUPPORTED           CTOOLS_EBASE-45
/** CTOOLS System ETB Stop on Full mode not supported */
#define CTOOLS_SYSETB_STOPBUF_NOT_SUPPORTED           CTOOLS_EBASE-46
/** CTOOLS dsptrace Trace END (TEND) error  */
#define CTOOLS_DSPTRACE_TEND_ERROR                    CTOOLS_EBASE-47

/**
@}
*/

/**
@addtogroup CTOOLS_USECASE_ENUM
@{
*/

/** 
 * @brief Debug Tool ETB buffer drain options
 */
typedef enum
{
   CTOOLS_DRAIN_ETB_EDMA, /*!< Drain the ETB with DMA */
   CTOOLS_DRAIN_ETB_CPU,  /*!< Drain the ETB with CPU */
   CTOOLS_DRAIN_NONE      /*!< No ETB Drain*/
} ctools_etb_drain_e;

/** 
 * @brief System Trace options
 */
typedef enum
{
   CTOOLS_SYS_TRACE_ALL,      /*!< Both SW messages and CPT messages are captured in the SYS ETB*/
   CTOOLS_SYS_TRACE_CP_TRACER, /*!< Only CPT messages are captured in the SYS ETB */
   CTOOLS_SYS_TRACE_SW_MSGS  /*!< Only SW messages are captured in the SYS ETB */
} ctools_sys_trace_opts_e;

/** 
 * @brief ETB Types
 */
typedef enum
{
   CTOOLS_DSP_ETB,      /*!< DSP ETB*/
   CTOOLS_SYS_ETB       /*!< System ETB */
} ctools_etb_types_e;

/**
@}
*/


/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{ 
*/

/** 
 * @brief dbg_tool return result
 */
typedef int  ctools_Result;
/**
@}
*/

/*
 * Ti dsp trace instance structure for internal operation of the library for the use cases
 */
typedef struct
{
    DSPTraceHandle*                pDSPHandle;         /*!< DSP trace handle for the core */    
    ctools_dsptrace_uc_state_e     state;        /* etb reserved for a use case or not */    
} ctools_dsptrace_inst_t;

/*
 * Ti ETB information instance structure for internal operation of the library for the use cases
 */
typedef struct
{
    ETBHandle*              pETBHandle;         /*!< ETB handle for the core  */
    ctools_etb_drain_e      etbDrainOpts;       /*!< etb drain options values */
    void*                   drainMemPtr;        /*!< drain memory pointer */
    uint32_t                drainMemSize;       /*!< size of the drain memory provided */
} ctools_etb_inst_t;

#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_INT_H*/
