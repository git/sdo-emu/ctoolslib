/*
 *   @file  ctools_uclib_cpt.h
 *
 *   @brief
 *      Ctools use case library CP Tracer include file. Provides APIs to open, start, end and 
 *      close CP Tracer specific use case scenarios.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef __CTOOLS_UCLIB_CPT_H
#define __CTOOLS_UCLIB_CPT_H

#ifdef __cplusplus
extern "C" {
#endif

/** 
 *
 * @page CPTrace_Use_Cases Common Platform Tracer Use Cases
 * CP Trace use case APIs provide the ability to perform system (SoC) level bandwidth, latency and event profiling. Data is
 * captured either by seperate hardware units (Embedded Trace Buffers, called as ETB) and then decoded
 * offline or captured using external emulators (XDS560PRO or XDS560V2) and decoded using CCS.
 *
 * The CP trace setup can be done either via CCS or via 'C' code using the ctools use case library. This page describes
 * the instructions for implementing CP trace features via 'C' functions. The CTOOLS CP Tracer library provides APIs to
 * implement the following use-case scenarios:
 * - \b System \b Latency \b Profiling: Captures latency of data paths from all system masters to the slave (or) slaves 
       associated with either one or multiple CP Tracers. Please, follow the below steps to implement this use-case:
       - Populate ::ctools_cpt_sysprofilecfg structure with the required configuration for one or more CP Tracers
       - Call ::ctools_cpt_syslatprofile_open API
       - Call ::ctools_cpt_singlestart API to start a single CP Tracer configured for system latency use-case.
         Call ::ctools_cpt_globalstart API to start all open CP Tracers irrespective of the use-case.
       - Application code which needs to be profiled
       - Call ::ctools_cpt_singlestop API to stop a single CP Tracer configured for system latency use-case.
         Call ::ctools_cpt_globalstop API to stop all open CP Tracers irrespective of the use-case.
       - Call ::ctools_cpt_singleclose API to close a single CP Tracer configured for system latency use-case.
         Call ::ctools_cpt_globalclose API to close all open CP Tracers irrespective of the use-case.
 * - \b System \b Bandwidth \b Profiling: Captures bandwidth of data paths from all system masters to the slave (or) slaves 
       associated with either one or multiple CP Tracers. Please, follow the below steps to implement this use-case:
       - Populate ::ctools_cpt_sysprofilecfg structure with the required configuration for one or more CP Tracers
       - Call ::ctools_cpt_sysbwprofile_open API
       - Call ::ctools_cpt_singlestart API to start a single CP Tracer configured for system bandwidth use-case.
           Call ::ctools_cpt_globalstart API to start all open CP Tracers irrespective of the use-case.
       - Application code which needs to be profiled
       - Call ::ctools_cpt_singlestop API to stop a single CP Tracer configured for system bandwidth use-case.
           Call ::ctools_cpt_globalstop API to stop all open CP Tracers irrespective of the use-case.
       - Call ::ctools_cpt_singleclose API to close a single CP Tracer configured for system bandwidth use-case.
           Call ::ctools_cpt_globalclose API to close all open CP Tracers irrespective of the use-case.
 * - \b Master \b Bandwidth \b Profiling: The bandwidth of data paths from two different group of masters to the
       slave associated with the CP Tracer is measured and compared. \n 
	   - The following statistics are captured in this use-case:
		   - Slave Bus Bandwidth (bytes per second) utilized by master group 0
		   - Slave Bus Bandwidth (bytes per second) utilized by master group 1
		   - Average Access Size of slave transactions (for both the master groups)
		   - Bus Utilization (transactions per second) (for both the master groups)
		   - Bus Contention Percentage (for both the master groups)
		   - Minimum Average Latency (for both the master groups) \n
	   - Please, follow the below steps to implement this use-case:
		   - Populate ::ctools_cpt_masterprofilecfg structure with the required configuration for the CP Tracer of interest
		   - Call ::ctools_cpt_masterprofile_open API
		   - Call ::ctools_cpt_singlestart API to start a single CP Tracer configured for master bandwidth use-case.
			 Call ::ctools_cpt_globalstart API to start all open CP Tracers irrespective of the use-case.
		   - Application code which needs to be profiled
		   - Call ::ctools_cpt_singlestop API to stop a single CP Tracer configured for master bandwidth use-case.
			 Call ::ctools_cpt_globalstop API to stop all open CP Tracers irrespective of the use-case.
		   - Call ::ctools_cpt_singleclose API to close a single CP Tracer configured for master bandwidth use-case.
			 Call ::ctools_cpt_globalclose API to close all open CP Tracers irrespective of the use-case.
 * - \b Total \b Bandwidth \b Profiling: The bandwidth of data paths from a group of masters to the
       slave associated with the CP Tracer is compared with the total bandwidth from all masters to the slave. \n 
	   - The following statistics are captured in this use-case:
		   - Percentage of total slave activity utilized by selected group of masters
		   - Slave Bus Bandwidth (bytes per second) utilized by the selected group of masters
		   - Average Access Size of slave transactions (for all system masters)
		   - Bus Utilization (transactions per second) (for all system masters)
		   - Bus Contention Percentage (for all system masters)
		   - Minimum Average Latency (for all system masters)  \n
	   - Please, follow the below steps to implement this use-case:
		   - Populate ::ctools_cpt_totalprofilecfg structure with the required configuration for the CP Tracer of interest
		   - Call ::ctools_cpt_totalprofile_open API
		   - Call ::ctools_cpt_singlestart API to start a single CP Tracer configured for total bandwidth use-case.
			 Call ::ctools_cpt_globalstart API to start all open CP Tracers irrespective of the use-case.
		   - Application code which needs to be profiled
		   - Call ::ctools_cpt_singlestop API to stop a single CP Tracer configured for total bandwidth use-case.
			 Call ::ctools_cpt_globalstop API to stop all open CP Tracers irrespective of the use-case.
		   - Call ::ctools_cpt_singleclose API to close a single CP Tracer configured for total bandwidth use-case.
			 Call ::ctools_cpt_globalclose API to close all open CP Tracers irrespective of the use-case.
 * - \b New \b Request \b Event \b Capture: Based on the slave to be monitored, the associated CP Tracer is configured to 
       capture new request transactions accepted by the slave. The required transactions can be captured by filtering 
       based on Master ID or address range. \n 
	   - The following statistics are captured for every new request event:
		   - Master ID which initiated this particular transaction
		   - Bus Transaction ID
		   - Read/Write Transaction
		   - 10 bits of the address (Address export mask selects, which particular 10 bits to export) \n
	   - Please, follow the below steps to implement this use-case:
		   - Populate ::ctools_cpt_eventtracecfg structure with the required configuration for the CP Tracer of interest
		   - Call ::ctools_cpt_eventtrace_open API
		   - Call ::ctools_cpt_singlestart API to start a single CP Tracer configured for new request event capture use-case.
			 Call ::ctools_cpt_globalstart API to start all open CP Tracers irrespective of the use-case.
		   - Application code which needs to be profiled
		   - Call ::ctools_cpt_singlestop API to stop a single CP Tracer configured for new request event capture use-case.
			 Call ::ctools_cpt_globalstop API to stop all open CP Tracers irrespective of the use-case.
		   - Call ::ctools_cpt_singleclose API to close a single CP Tracer configured for new request event capture use-case.
			 Call ::ctools_cpt_globalclose API to close all open CP Tracers irrespective of the use-case.

 * The CP tracer messages (system trace) can be exported out for analysis in 3 ways:
 *     - \par System ETB drain using CPU (capture only 32KB (SYS ETB size in keystone devices) of system trace data)
 * 			- @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_CPU, &etb_config, @ref CTOOLS_SYS_ETB) - Initialize the SYS ETB
 * 			- @ref ctools_systemtrace_init() - Initialize System Trace and allocate the required resources
 * 			- As shown above, do all the steps pertaining to the required CP Tracer use case, that needs to be implemented.
 * 			- @ref ctools_etb_cpu_drain(data_ptr, size_in, &size_out, &wrap_flag, @ref CTOOLS_SYS_ETB) - Drain ETB to specified system memory location.
 *        			The ETB is disabled after the drain is complete.
 * 			- @ref ctools_systemtrace_shutdown() - free up resources initialized/utilized
 * 			- \b Please \b Note: The System ETB does not support Stop On Full mode and an error will be returned, if the user tries to
 * 			  configure the SYS ETB in stop on full mode. Please, use external emulator like XDS560 PRO or XDS560V2, if stop on full mode
 * 			  of operation is required.
 *     - \par System ETB drain using EDMA (ETB extension to capture more than 32KB (SYS ETB size in keystone devices))
 * 			- @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_EDMA, &etb_config, @ref CTOOLS_SYS_ETB) - Initialize the SYS ETB
 * 			- @ref ctools_edma_resrc_get (&num_params, &num_channels) - get the resource requirement for the EDMA params and channels
 * 			- @ref ctools_systemtrace_init() - Initialize System Trace and allocate the required resources
 * 			- As shown above, do all the steps pertaining to the required CP Tracer use case, that needs to be implemented.
 * 			- @ref ctools_etb_edma_drain(pct_res_ptr, @ref CTOOLS_SYS_ETB) - Returns the system trace information in the return structure if EDMA is used a drain for ETB.
 *        			The ETB is disabled after the drain is complete.
 * 			- @ref ctools_systemtrace_shutdown() - free up resources initialized/utilized
 * 			- \b Please \b Note: The System ETB extension with EDMA  does not support Stop On Full mode and an error will be returned, if the user tries to
 * 			  configure the SYS ETB extension with EDMA in stop on full mode. Please, use external emulator like XDS560 PRO or XDS560V2, if stop on full mode
 * 			  of operation is required.
 *     - \par External Emulator like XDS560 PRO or XDS560V2
 * 			- @ref ctools_systemtrace_init() - Initialize System Trace and allocate the required resources
 * 			- As shown above, do all the steps pertaining to the required CP Tracer use case, that needs to be implemented.
 * 			- @ref ctools_systemtrace_shutdown() - free up resources initialized/utilized
 *
 * For more details and examples, Please refer to the following Wiki page: http://processors.wiki.ti.com/index.php/CToolsLib_Article#CPTLib
 */ 

//Using forward slashes in the relative serach path for linux compatibility

#include "../../CPTLib/include/CPTLib.h"
#include "../../STMLib/include/StmLibrary.h"
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"

//STM Channel assignments
#define STM_HelperCh 231
#define STM_CPTLogCh 232

// CPT defines
#define DBG_TOOLS_CPT_DISABLE 0
#define DBG_TOOLS_CPT_ENABLE  1

//Internally used structures

//CP Tracer job Table structure
typedef struct  {
    CPT_Handle_Pntr pCPT_Handle; /* CPT handle pointer */
    eCPT_UseCase CPT_UseCaseId;          /*CPT use case */
    uint8_t in_use_flag; /* CPT in use indication */
	uint8_t start_flag; /* CPT start indication */
}ctools_cpt_jobtable;


//This Structure contains all the required CPT memory resources
typedef struct  {
STMHandle * pSTMhdl;    // STMLib handle pointer
ctools_cpt_jobtable CPT_JobsList[eCPT_ModID_Last];
}ctools_cpt_resources;

/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{
*/

/*! \par ctools_cpt_resources_pntr
 * @brief   Pointer to the internal CP Tracer resources structure
*/
//typedef struct ctools_cpt_resources * ctools_cpt_resources_pntr;

#define ctools_cpt_resources_pntr ctools_cpt_resources*

/*! \par ctools_cpt_masteridfiltercfg
 * @brief   CP Tracer Master ID based filtering configuration
*/
typedef struct  {
    uint32_t MasterIDgroup_Enable; /*!< Master ID grouping enable/disable. Consider eCPT_MID_SRIO_CPPI master for example: If MasterIDgroup_Enable = 1,
	                                        then providing an input of eCPT_MID_SRIO_CPPI_Grp0 will enable all masters in that particular group.
	                                        If MasterIDgroup_Enable = 0, then only eCPT_MID_SRIO_CPPI_Grp0 will be enabled. */
	const eCPT_MasterID * CPT_MasterID; /*!< Array of Master Ids which needs to be enabled. */
	uint32_t CPT_MasterIDCnt; /*!< Count or number of Master Ids which needs to be enabled. */
}ctools_cpt_masteridfiltercfg;

/*! \par ctools_cpt_addressfiltercfg
 * @brief    CP Tracer Address range based filtering configuration
*/
typedef struct  {
	eCPT_FilterMode CPT_FilterMode; /*!< Provides selection for inclusive/exclusive address filter modes.*/
	uint32_t AddrFilterMSBs; /*!< Address filter MSBs (address bits 63:32). The bits used must be valid for the CP Tracer slave (See CPT_getAddrMode()).
                                      These bits are applied to both the start and end address. */
	uint32_t StartAddrFilterLSBs; /*!< Start address filter LSBs (address bits 31:0).*/
	uint32_t EndAddrFilterLSBs; /*!< End address filter LSBs (address bits 31:0). */
}ctools_cpt_addressfiltercfg;

/*! \par ctools_cpt_modidqual
 * @brief    CP Tracer configuration structure containing the module ID and its associated qualifier configuration.
*/
typedef struct  {
    eCPT_ModID CPT_ModId; /*!< CPT Module ID*/
	const CPT_Qualifiers * TPCntQual; /*!< CPT Qualifiers corresponding to the CPT Module Id*/
}ctools_cpt_modidqual;

/*! \par ctools_cpt_sysprofilecfg
 * @brief    CP Tracer configuration parameters for System Profiling Use-cases (Bandwidth or Latency profiling)
*/
typedef struct  {
	const ctools_cpt_modidqual * ModIDQual; /*!< Pointer to an array of structures containing CPT Module Ids and their corresponding qualifiers for 
	                                              which System profiling needs to be enabled.*/
	uint32_t CPT_ModCnt;  /*!< Count or number of CPT Module for which System bandwidth profiling needs to be enabled. */
	uint32_t CPUClockRateMhz;          /*!< CPU clock rate in Mhz. This value is provided to host tools through meta data STM messages and is used
	                                            to calculate throughput. If this value is non-zero the host decoder will convert throughput data to
	                                            Bytes/Second. If this value is zero the host decoder will convert throughput data to Bytes/CPU-Cycle.  */
	uint32_t SampleWindowSize;       /*!< 32-bit non-zero value used to set the sample window size in CP Tracer clocks. Setting to 0 is not legal
										 and will result in CPT_OpenModule() returning eCPT_Error_Invalid_Parameter. If CPT_OpenModule() is called
										 with a NULL CPT_CfgOptions pointer then the default for this parameter is 64K.
										 This value is provided to host tools through meta data STM messages and is used to calculate throughput
										 for statistic messages. */
}ctools_cpt_sysprofilecfg;

/*! \par ctools_cpt_masterprofilecfg
 * @brief    CP Tracer configuration parameters for Master Bandwidth Profiling Use-case
*/
typedef struct  {
	eCPT_ModID CPT_ModId;          /*!< CPT Module Id for which Master bandwidth profiling needs to be enabled on. */
	uint32_t CPUClockRateMhz;          /*!< CPU clock rate in Mhz. This value is provided to host tools through meta data STM messages and is used
                                                to calculate throughput. If this value is non-zero the host decoder will convert throughput data to
                                                Bytes/Second. If this value is zero the host decoder will convert throughput data to Bytes/CPU-Cycle.  */
	uint32_t SampleWindowSize;       /*!< 32-bit non-zero value used to set the sample window size in CP Tracer clocks. Setting to 0 is not legal
										 and will result in CPT_OpenModule() returning eCPT_Error_Invalid_Parameter. If CPT_OpenModule() is called
										 with a NULL CPT_CfgOptions pointer then the default for this parameter is 64K.
										 This value is provided to host tools through meta data STM messages and is used to calculate throughput
										 for statistic messages. */
	const ctools_cpt_addressfiltercfg * Address_Filter_Params; /*!< Address range based filtering configuration inputs for throughput measurements. */
	const ctools_cpt_masteridfiltercfg * TPCnt0_MasterID; /*!< Master ID based filtering configuration inputs for Throughput counter0 measurements. */
	const ctools_cpt_masteridfiltercfg * TPCnt1_MasterID; /*!< Master ID based filtering configuration inputs for Throughput counter1 measurements. */
	const CPT_Qualifiers * TPCnt0Qual; /*!< CPT Qualifiers inputs for Throughput counter0 measurements. */
	const CPT_Qualifiers * TPCnt1Qual; /*!< CPT Qualifiers inputs for Throughput counter1 measurements. */
}ctools_cpt_masterprofilecfg;

/*! \par ctools_cpt_totalprofilecfg
 * @brief    CP Tracer configuration parameters for Total Bandwidth Profiling Use-case
*/
typedef struct  {
    eCPT_ModID CPT_ModId; /*!< CPT Module Id for which Master bandwidth profiling needs to be enabled on. */
    uint32_t CPUClockRateMhz;          /*!< CPU clock rate in Mhz. This value is provided to host tools through meta data STM messages and is used
                                                to calculate throughput. If this value is non-zero the host decoder will convert throughput data to
                                                Bytes/Second. If this value is zero the host decoder will convert throughput data to Bytes/CPU-Cycle.  */
    uint32_t SampleWindowSize;       /*!< 32-bit non-zero value used to set the sample window size in CP Tracer clocks. Setting to 0 is not legal
                                         and will result in CPT_OpenModule() returning eCPT_Error_Invalid_Parameter. If CPT_OpenModule() is called
                                         with a NULL CPT_CfgOptions pointer then the default for this parameter is 64K.
                                         This value is provided to host tools through meta data STM messages and is used to calculate throughput
                                         for statistic messages. */
    const ctools_cpt_addressfiltercfg * Address_Filter_Params; /*!< Address range based filtering configuration inputs for throughput measurements. */
    const ctools_cpt_masteridfiltercfg * TPCnt_MasterID; /*!< Master ID based filtering configuration inputs for Throughput measurement, which will be
                                                        compared against the total throughput. */
    const CPT_Qualifiers * TPCntQual; /*!< CPT Qualifiers inputs for Throughput measurement, which will be compared against the total throughput. */
}ctools_cpt_totalprofilecfg;

/*! \par ctools_cpt_eventtracecfg
 * @brief    CP Tracer configuration parameters for New Request Event capture Use-case
*/
typedef struct  {
    eCPT_ModID CPT_ModId; /*!< CPT Module Id for which New Request Event needs to be captured. */
	uint32_t AddrExportMask;         /*!< Starting bit number of the 10 bits of address to be captured. This is 6-bit value (bits 5:0) in the range 
	                                      of 0 to 37 that defines the 10 address bits used in STM message generation:
	                                               \li 0 - bits 10:0 are exported
	                                               \li 1 - bits 11:1 are exported
	                                               \li ...
	                                               \li 37 - bits 47:37 are exported \n\n
	                                               Bits 32:6 are don't cares. If CPT_OpenModule() is called
	                                               with a NULL CPT_CfgOptions pointer then the default for this parameter is 0.
	                                               This value is provided to host tools through meta data STM messages and is used
	                                               to adjust address display.*/
	const ctools_cpt_addressfiltercfg * Address_Filter_Params; /*!< Address range based filtering configuration inputs for New Request event capture. */
	const ctools_cpt_masteridfiltercfg * EventTrace_MasterID; /*!< Master ID based filtering configuration inputs for New Request event capture. */
	const CPT_Qualifiers * TPEventQual; /*!< CPT Qualifiers inputs for New Request event capture. */
}ctools_cpt_eventtracecfg;

/**
@}
*/

/**
@addtogroup CTOOLS_USECASE_ENUM
@{
*/
/**
@}
*/

/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/

/** @addtogroup CTOOLS_USECASE_FUNCTION
@{
*/
/** @b Description
    @n

    Opens CP Tracer System latency profiler based on user configuration inputs.

    \param[in] SysProfile_params ::ctools_cpt_sysprofilecfg provides user defined configuration structure for System latency profiling.
    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SYSPROFILE_ERROR and CTOOLS_CPT_SYSPROFILE_BUSY
    \par Details:

    Example API call:

    \code

    ctools_cpt_sysprofilecfg SysMemory_Profile_Params = {NULL};

    ctools_cpt_modidqual SysMemory_ModIDQual[5];

    int i;

    for(i=0;i<5;i++)
    {
        SysMemory_ModIDQual[i].TPCntQual = NULL;
    }

    SysMemory_ModIDQual[0].CPT_ModId = eCPT_MSMC_0;
    SysMemory_ModIDQual[1].CPT_ModId = eCPT_MSMC_1;
    SysMemory_ModIDQual[2].CPT_ModId = eCPT_MSMC_2;
    SysMemory_ModIDQual[3].CPT_ModId = eCPT_MSMC_3;
    SysMemory_ModIDQual[4].CPT_ModId = eCPT_DDR;

    //Populate SysMemory Profiler Params
    SysMemory_Profile_Params.ModIDQual = SysMemory_ModIDQual;
    SysMemory_Profile_Params.CPT_ModCnt = 5;

    SysMemory_Profile_Params.CPUClockRateMhz = 983;
    SysMemory_Profile_Params.SampleWindowSize = 81916;

    // Open System memory (MSMC and DDR3) profiler
    if (ctools_cpt_syslatprofile_open(&SysMemory_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_syslatencyprofile_open Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_syslatprofile_open (const ctools_cpt_sysprofilecfg * SysProfile_params);

/** @b Description
    @n

    Opens CP Tracer System bandwidth profiler based on user configuration inputs.

    \param[in] SysProfile_params ::ctools_cpt_sysprofilecfg provides user defined configuration structure for System bandwidth profiling.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SYSPROFILE_ERROR and CTOOLS_CPT_SYSPROFILE_BUSY
    \par Details:

    Example API call:

    \code

    ctools_cpt_sysprofilecfg SysMemory_Profile_Params = {NULL};

    ctools_cpt_modidqual SysMemory_ModIDQual[5];

    int i;

    for(i=0;i<5;i++)
    {
        SysMemory_ModIDQual[i].TPCntQual = NULL;
    }

    SysMemory_ModIDQual[0].CPT_ModId = eCPT_MSMC_0;
    SysMemory_ModIDQual[1].CPT_ModId = eCPT_MSMC_1;
    SysMemory_ModIDQual[2].CPT_ModId = eCPT_MSMC_2;
    SysMemory_ModIDQual[3].CPT_ModId = eCPT_MSMC_3;
    SysMemory_ModIDQual[4].CPT_ModId = eCPT_DDR;

    //Populate SysMemory Profiler Params
    SysMemory_Profile_Params.ModIDQual = SysMemory_ModIDQual;
    SysMemory_Profile_Params.CPT_ModCnt = 5;

    SysMemory_Profile_Params.CPUClockRateMhz = 983;
    SysMemory_Profile_Params.SampleWindowSize = 81916;

    // Open System memory (MSMC and DDR3) profiler
    if (ctools_cpt_sysbwprofile_open(&SysMemory_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_sysbandwidthprofile_open Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_sysbwprofile_open (const ctools_cpt_sysprofilecfg * SysProfile_params);

/** @b Description
    @n

    Opens CP Tracer New Request event capture, based on user configuration inputs.

    \param[in] EventTrace_params ::ctools_cpt_eventtracecfg provides user defined configuration structure for New Request event capture.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_EVTPROFILE_ERROR and CTOOLS_CPT_EVTPROFILE_BUSY
    \par Details:

    Example API call:

    \code

    ctools_cpt_eventtracecfg EventTrace_Params = {NULL};

    ctools_cpt_masteridfiltercfg  MstID_Cfg = {NULL};
    ctools_cpt_addressfiltercfg AddrFilter_Cfg = {NULL};

    //Capture accesses to memory from 0x89112c24 to 0x89112f24
    //In keystone devices, by default XMC MPAX settings map 0x8000_0000 to 0x8FFF_FFFF to 0x8_0000_0000 to 0x8_7FFF_FFFF
    AddrFilter_Cfg.AddrFilterMSBs = 0x8;
    AddrFilter_Cfg.CPT_FilterMode = eCPT_FilterMode_Inclusive;
    AddrFilter_Cfg.EndAddrFilterLSBs = 0x09112f24;
    AddrFilter_Cfg.StartAddrFilterLSBs = 0x09112c24;

    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM0};

    EventTrace_Params.CPT_ModId = eCPT_DDR; //Capturing New Request event @ DDR3 memory slave
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    MstID_Cfg.CPT_MasterIDCnt = 1;
    EventTrace_Params.EventTrace_MasterID = &MstID_Cfg;
    EventTrace_Params.TPEventQual = NULL;

    EventTrace_Params.Address_Filter_Params = &AddrFilter_Cfg;
    EventTrace_Params.AddrExportMask = 0; // Address Bits 10:0 are exported

    // Open DDR3 New request event capture
    if (ctools_cpt_eventtrace_open(&EventTrace_Params) != CTOOLS_SOK)
    {
        printf ("Error: New Request event capture open Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_eventtrace_open (const ctools_cpt_eventtracecfg * EventTrace_params);

/** @b Description
    @n

    Opens CP Tracer Master bandwidth profiler based on user configuration inputs.

    \param[in] MasterProfile_params ::ctools_cpt_masterprofilecfg provides user defined configuration structure for Master bandwidth profiling.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_MASTERPROFILE_ERROR and CTOOLS_CPT_MASTERPROFILE_BUSY
    \par Details:

    Example API call:

    \code

    ctools_cpt_masterprofilecfg Master_Profile_Params = {NULL};
    ctools_cpt_masteridfiltercfg  MstID0_Cfg = {NULL};
    ctools_cpt_masteridfiltercfg  MstID1_Cfg = {NULL};
    eCPT_MasterID MstID0_Array[] = {eCPT_MID_GEM0}; // Master core
    eCPT_MasterID MstID1_Array[] = {eCPT_MID_GEM1, eCPT_MID_GEM2, eCPT_MID_GEM3}; // Slave cores

    Master_Profile_Params.CPT_ModId = eCPT_DDR; //Profiling accesses to DDR3 memory
    MstID0_Cfg.CPT_MasterID = MstID0_Array;
    MstID0_Cfg.MasterIDgroup_Enable = 0;
    MstID1_Cfg.CPT_MasterID = MstID1_Array;
    MstID1_Cfg.MasterIDgroup_Enable = 0;
    MstID0_Cfg.CPT_MasterIDCnt = 1;
    MstID1_Cfg.CPT_MasterIDCnt = 3;
    Master_Profile_Params.TPCnt0_MasterID = &MstID0_Cfg;
    Master_Profile_Params.TPCnt1_MasterID = &MstID1_Cfg;
    Master_Profile_Params.TPCnt0Qual = NULL;
    Master_Profile_Params.TPCnt1Qual = NULL;
    Master_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Master_Profile_Params.CPUClockRateMhz = 983;
    Master_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Master bandwidth profiler
    if (ctools_cpt_masterprofile_open(&Master_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_masterprofile_open failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_masterprofile_open (const ctools_cpt_masterprofilecfg * MasterProfile_params);

/** @b Description
    @n

    Opens CP Tracer Total bandwidth profiler based on user configuration inputs.

    \param[in] TotalProfile_params ::ctools_cpt_totalprofilecfg provides user defined configuration structure for Total bandwidth profiling.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_TOTALPROFILE_ERROR and CTOOLS_CPT_TOTALPROFILE_BUSY
    \par Details:

    Example API call:

    \code

    ctools_cpt_totalprofilecfg Total_Profile_Params = {NULL};
    ctools_cpt_masteridfiltercfg  MstID_Cfg = {NULL};
    eCPT_MasterID MstID_Array[] = {eCPT_MID_GEM1};

    Total_Profile_Params.CPT_ModId = eCPT_DDR; //Profiling accesses to DDR3 memory
    MstID_Cfg.CPT_MasterID = MstID_Array;
    MstID_Cfg.MasterIDgroup_Enable = 0;
    MstID_Cfg.CPT_MasterIDCnt = 1;
    Total_Profile_Params.TPCnt_MasterID = &MstID_Cfg;
    Total_Profile_Params.TPCntQual = NULL;
    Total_Profile_Params.Address_Filter_Params = NULL; //Address filtering is disabled
    Total_Profile_Params.CPUClockRateMhz = 983;
    Total_Profile_Params.SampleWindowSize = 81916;

    // Open DDR3 Total bandwidth profiler
    if (ctools_cpt_totalprofile_open(&Total_Profile_Params) != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_totalprofile_open failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_totalprofile_open (const ctools_cpt_totalprofilecfg * TotalProfile_params);

/** @b Description
    @n

    Start the use-case associated with CPT module passed as an input. The use-case gets associated with an CPT module from the
    Open API call (CPT_EventTrace_open, CPT_MasterProfile_open and CPT_TotalProfile_open).

    \param[in] CPT_ModId eCPT_ModID provides the CPT module ID for which its associated use-case needs to be started.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLESTART_ERROR and CTOOLS_CPT_NOT_OPEN
    \par Details:

    Example API call:

    \code

    // Start DDR3 CP Tracer
    if (ctools_cpt_singlestart(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: DDR3 CP Tracer ctools_cpt_singlestart Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_singlestart(eCPT_ModID CPT_ModId);

/** @b Description
    @n

    Stop the use-case associated with CPT module passed as an input. The use-case gets associated with an CPT module from the
    Open API call (CPT_EventTrace_open, CPT_MasterProfile_open and CPT_TotalProfile_open).

    \param[in] CPT_ModId eCPT_ModID provides the CPT module ID for which its associated use-case needs to be started.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLESTOP_ERROR, CTOOLS_CPT_NOT_STARTED and CTOOLS_CPT_NOT_OPEN
    \par Details:

    Example API call:

    \code

    // Stop DDR3 CP Tracer
    if (ctools_cpt_singlestop(eCPT_DDR) != CTOOLS_SOK)
    {
        printf ("Error: DDR3 CP Tracer ctools_cpt_singlestop Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_singlestop(eCPT_ModID CPT_ModId);

/** @b Description
    @n

    Close the specified CP Tracer module and release all associated resources.

    \param[in] CPT_ModId eCPT_ModID provides the CPT module ID for which its associated use-case needs to be started.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLECLOSE_ERROR and CTOOLS_CPT_NOT_OPEN
    \par Details:

    Example API call:

    \code

    // Close DDR3 CP Tracer
    if (ctools_cpt_singleclose(eCPT_DDR) < 0)
    {
        printf ("Error: DDR3 CP Tracer ctools_cpt_singleclose Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_singleclose(eCPT_ModID CPT_ModId);

/** @b Description
    @n

    Start all open CP Tracer modules which are currently opened for the required use case.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLESTART_ERROR and CTOOLS_CPT_NOT_OPEN
    \par Details:

    Example API call:

    \code

    // Start all CP Tracers
    if (ctools_cpt_globalstart() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstart Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_globalstart(void);

/** @b Description
    @n

    Stop all open CP Tracer modules which are currently opened for the required use case.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLESTOP_ERROR, CTOOLS_CPT_NOT_STARTED and CTOOLS_CPT_NOT_OPEN
    \par Details:

    Example API call:

    \code

    // Stop all CP Tracers
    if (ctools_cpt_globalstop() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalstop Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_globalstop(void);

/** @b Description
    @n

    Close all open CP Tracer modules which are currently opened for the required use case.

    @retval
        Success - CTOOLS_SOK
    @retval
        Failure - CTOOLS_CPT_ENABLE_ERROR, CTOOLS_CPT_SINGLECLOSE_ERROR and CTOOLS_CPT_NOT_OPEN
    \par Details:

    \code

    Example API call:

    // Close all CP Tracers
    if (ctools_cpt_globalclose() != CTOOLS_SOK)
    {
        printf ("Error: ctools_cpt_globalclose Failed\n");
        return(-1);
    }

    \endcode
*/

ctools_Result ctools_cpt_globalclose(void);

/**
@}
*/

#ifdef __cplusplus
}
#endif
#endif /* __CTOOLS_UCLIB_CPT_H */
