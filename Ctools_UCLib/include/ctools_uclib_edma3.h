/*
 *   @file  ctools_uclib_edma3.h
 *
 *   @brief   
 *       This is the Ctools use case library edma3 include file.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef __CTOOLS_UCLIB_EDMA3_H
#define __CTOOLS_UCLIB_EDMA3_H

#ifdef __cplusplus
extern "C" {
#endif

//Using forward slashes in the relative serach path for linux compatibility

#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../ETBLib/include/ETBInterface.h"

#if !defined(C6657) && !defined(C66AK2Hxx) && !defined(TCI6630K2L) && !defined(C66AK2Exx) && !defined(_66AK2Gxx)

#include "../../ETBLib/src/ti/edma_dev-c66xx.h"

#endif

/**
@addtogroup CTOOLS_USECASE_ENUM
@{
*/

/** 
 * @brief Debug Tool DMA mode
 */
typedef enum
{
   CTOOLS_USECASE_EDMA_CIRCULAR_MODE,  /*!< Drain to the ETB with DMA in circular mode */
   CTOOLS_USECASE_EDMA_STOP_BUF_MODE   /*!< Drain to the ETB with DMA in stop mode     */
} ctools_edma_mode_e;

/** 
 * @brief TI CTOOLS EDMA Channel Controller selection
 */
typedef enum
{
#ifdef C6657
   CTOOLS_USECASE_EDMA_CC2 = 2
#elif defined(C66AK2Hxx) || defined(C66AK2Exx)
   CTOOLS_USECASE_EDMA_CC0 = 0,  /*!< Channel Controller 0 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC1 = 1,  /*!< Channel Controller 1 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC2 = 2,   /*!< Channel Controller 2 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC3 = 3,   /*!< Channel Controller 2 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC4 = 4   /*!< Channel Controller 2 for EDMA 3 */
#elif defined(_66AK2Gxx)
   CTOOLS_USECASE_EDMA_CC0 = 0,  /*!< Channel Controller 0 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC1 = 1  /*!< Channel Controller 1 for EDMA 3 */
#else
   CTOOLS_USECASE_EDMA_CC0 = 0,  /*!< Channel Controller 0 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC1 = 1,  /*!< Channel Controller 1 for EDMA 3 */
   CTOOLS_USECASE_EDMA_CC2 = 2   /*!< Channel Controller 2 for EDMA 3 */
#endif
} ctools_edma_cc_select_e;

/**
@}
*/

/**
@addtogroup CTOOLS_USECASE_SYMBOL
@{
*/

/**
@}
*/

/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{ 
*/

/** 
 * @brief Debug Tool EDMA configuration structure, if EDMA is used as an option to drain the ETB
 *          
 */
typedef struct
{
    uint32_t* channels_ptr;   /*!<  DMA channels for DMA draining ETB option */
    uint32_t* param_ptr;     /*!< Parameter RAM entry numbers (used for
                                 linking by the library). Thses must be different
                                 PaRAM entries than those mapped to channel 1 & 2 */
    ctools_edma_cc_select_e cc_sel;     /*!<EDMA3 channel controller Selection */
    eCIC_Select      cic_sel;    /*!< Chip Interrupt Controller number selected */
    uint32_t dbufAddress;  /*!< DMA Drain buffer address */
    uint32_t dbufBytes;    /*!< DMA Drain buffer size in Bytes */                                 
    ctools_edma_mode_e  mode;        /*!< edma mode, either circular or stop mode */
} ctools_edmaConfig_t;

/** 
 * @brief Ctools ETB EDMA drain job result structure to be used to convert 
 *        the cicurlar buffer to linear buffer
 */
typedef struct
{
    uint32_t startAddr; /*!< Current starting lcoation address in DMA drain
                              buffer.
                        */
    uint32_t availableWords; /*!< Total number of 32-bit words that have been written
                                   into the DMA drain buffer.
                             */
    uint32_t isWrapped;    /*!<  Drain buffer is full and has started writing
                                 over previously written words.
                           */
    uint32_t dbufAddress;  /*!< Drain buffer address */
    uint32_t dbufWords;    /*!< Drain buffer size in 32-bit words */
} ctools_edma_result_t;

/**
@}
*/
#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_EDMA3_H*/

/* Nothing past this point */
