/*
 *   @file  ctools_uclib.h
 *
 *   @brief   
 *       This is the CTOOLS use case library include file.
 *
  *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
 */

#ifndef __CTOOLS_UCLIB_H
#define __CTOOLS_UCLIB_H

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @mainpage  CTools use case Library
 *
 * @section Introduction Introduction
 * The Ctools use case library provides easy to use APIs to integrate commonly used Debug
 * and Trace features like PC trace, memory watch point, profiling etc into the application software.
 * This use case library provides APIs to integrate both Core Trace and System (SoC) Trace features:
 *
 * - Core trace provides program counter and instruction timing information
 *    - In C66x CorePAC, both core memory accesses and core events can be captured. 
 *
 * - System Trace provides
 *    - CP Tracer messages, which can be used to perform bandwidth, latency profiling and events capture
 *      at the System (SoC) level.
 *    - SW messages, which can be used to print messages from multiple cores. These SW messages can be 
 *      interleaved with the CP Tracer messages.
 *  
 * The ctools use-case library APIs internally call the low level ctools Libraries (ETBLib, DSPTraceLib,
 * CPTLib, AETLib and STMLib) APIs. For ease of use, the ctools use-case library is designed as a wrapper,
 * which provides complete abstraction from the low level ctools libraries.
 *
 * @section Devices Devices Supported
 * TMS320C6657, TMS320C6670, TMS320C6672, TMS320C6674, TMS320C6678, TMS320TCI6612, TMS320TCI6614, TMS320TCI6616 and TMS320TCI6618
 *
 * @section Revision_History CTOOLS Use Case Library Revision History
 *
 * <TABLE>
 * <TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
 * <TR><TD> 1.0 </TD> <TD> 10/12/2012 </TD> <TD> First release implementation  </TD>
 * <TR><TD> 1.1 </TD> <TD> 12/17/2012 </TD> <TD> Added support for keystone2 (C66AK2Hxx). Added ETB-EDMA support for C6657  </TD>
 * <TR><TD> 1.2 </TD> <TD> 05/15/2013 </TD> <TD> Updates for SDOCM00100802 (cToolsLib examples have some errors), 
												SDOCM00100576 (for keystone2, ctools mc_pc_trace_edma_drn unit test fails to initialize 
												dsp trace function) and SDOCM00101368 (ctools use-case library: PC trace capture on a App SW fault)  </TD>
 * <TR><TD> 1.3 </TD> <TD> 04/07/2014 </TD> <TD> Added support for Edison (C66AK2Exx) and Lamarr (TCI6630K2L) devices  </TD>
 * <TR><TD> 1.4 </TD> <TD> 07/30/2014 </TD> <TD> For all supported PCT use cases, DSPTrace TEND state needs to be cleared before starting PC trace,
 *                                               Fixed a bug: AET needs to claimed before DSPTrace_setState() API can be used to set DSPTrace TEND state</TD>
 * <TR><TD> 1.5 </TD> <TD> 05/13/2015 </TD> <TD> Added support for 66AK2Gxx devices  </TD>
 * </TABLE>
 *
 * @section Ctools_uc_lib Ctools Usecase Library
 * The ctools use case library is intended to demonstrate the following Debug and Trace use cases:
 * - @subpage PC_Trace
 * - @subpage Memory_Watch
 * - @subpage CPTrace_Use_Cases
 * - @subpage Statistical_Profiling
 *
 * @section Integration Integration with Application Software
 * The ctools use case library for all supported devices are available at Ctools_UCLib\\lib. For all the supported devices, the following are the
 * instructions to integrate the ctools use case library with the application software:
 * - \b TMS320C6657:
 * 		- Libraries to link: ctools_uclib.c6657_elf, cpt.c6657_elf, dsptrace.c66xx_elf, tietb.c6657_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags: C6657 and _STM_Logging
 * - \b TMS320C6670, \b TMS320TCI6616 and \b TMS320TCI6618:
 * 		- Libraries to link: ctools_uclib.c6670_elf, cpt.c6670_elf, dsptrace.c66xx_elf, tietb.c66xx_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags: C6670 and _STM_Logging
 * - \b TMS320C6678,\b TMS320C6674 and\b TMS320C6672:
 * 		- Libraries to link: ctools_uclib.c6678_elf, cpt.c6670_elf, dsptrace.c66xx_elf, tietb.c6678_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags:
 * 			- TMS320C6678: C6678 and _STM_Logging
 * 			- TMS320C6674: C6674 and _STM_Logging
 * 			- TMS320C6672: C6672 and _STM_Logging
 * - \b TMS320TCI6614 and \b TMS320TCI6612:
 * 		- Libraries to link: ctools_uclib.tci6614_elf, cpt.tci6614_elf, dsptrace.c66xx_elf, tietb.c66xx_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags:
 * 			- TMS320TCI6614: TCI6614 and _STM_Logging
 * 			- TMS320TCI6612: TCI6612 and _STM_Logging
 * - \b TMS320C66AK2Hxx (Keystone 2 devices):
 * 		- Libraries to link: ctools_uclib.c66AK2Hxx_elf, cpt.c66ak2hxx_elf, dsptrace.c66xx_elf, tietb.c66ak2hxx_c66x_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags: C66AK2Hxx and _STM_Logging
 * - \b TMS320C66AK2Exx:
 * 		- Libraries to link: ctools_uclib.c66AK2Exx_elf, cpt.66ak2exx_elf, dsptrace.c66xx_elf, tietb.c66ak2hxx_c66x_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags: C66AK2Exx and _STM_Logging
 * - \b TMS320TCI6630K2L:
 * 		- Libraries to link: ctools_uclib.tci6630K2L_elf, cpt.tci6630k2l_elf, dsptrace.c66xx_elf, tietb.c66ak2hxx_c66x_elf, aetlib.l66_elf and stm.c66xx_elf
 * 		- Compiler Pre-include flags: TCI6630K2L and _STM_Logging
 *
 * \par Please Note: For Big endian support, use the libraries with 'e' appended to the device part number. For Debug versions, use the libraries with '_d' appended to the library name.
 *
 * @section Rebuilding Rebuilding the Libraries
 * For each of the supported targets, CCSv5 projects available at Ctools_UCLib\\projects. These projects can be imported into CCS,
 * to rebuild the corresponding use case libraries.
 *
 * @section References References
 * - Doxygen for the low level ctools libraries:
 *    - CPTLib (<CTOOLS_INSTALL_DIR>\\CPTLib\\doc\\CPT_####_html\\index.html)
 *    - ETBLib (<CTOOLS_INSTALL_DIR>\\ETBLib\\doc\\html\\index.html)
 *    - DSPTraceLib (<CTOOLS_INSTALL_DIR>\\DSPTraceLib\\doc\\html\\index.html)
 *    - AETLib (<CTOOLS_INSTALL_DIR>\\aet\\doc\\html\\index.html)
 *    - STMLib (<CTOOLS_INSTALL_DIR>\\STMLib\\doc\\STM_html\\index.html)
 *
 * - Wiki Articles:
 *    - http://processors.wiki.ti.com/index.php/CToolsLib_Article#CPTLib
 * 
 *
 */

/* Includes */
//Using forward slashes in the relative serach path for linux compatibility
//Include files
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../Ctools_UCLib/include/ctools_uclib_etb.h"
#include "../../Ctools_UCLib/include/ctools_uclib_pct.h"
#include "../../Ctools_UCLib/include/ctools_uclib_cpt.h"
#include "../../Ctools_UCLib/include/ctools_uclib_stat_prof.h"
#include "../../Ctools_UCLib/include/ctools_uclib_memwatch.h"

// Note - the version definitions must have the end of line immediately after the value (packaging script requirement)
#define CTOOLS_UCLIB_MAJOR_VERSION    (0x1)
                                        /*!< Major version number - Incremented for API changes*/
#define CTOOLS_UCLIB_MINOR_VERSION    (0x5)
                                        /*!< Minor version number - Incremented for bug fixes  */

/**
@defgroup CTOOLS_USECASE_SYMBOL  CTOOLS_USECASE Symbols Defined
@ingroup CTOOLS_USECASE_API
*/
/**
@defgroup CTOOLS_USECASE_ENUM  CTOOLS_USECASE Enums
@ingroup CTOOLS_USECASE_API
*/
/**
@defgroup CTOOLS_USECASE_DATASTRUCT  CTOOLS_USECASE Data Structures
@ingroup CTOOLS_USECASE_API
*/
/**
@defgroup CTOOLS_USECASE_FUNCTION  CTOOLS_USECASE Functions
@ingroup CTOOLS_USECASE_API
*/
/**
@defgroup CTOOLS_USECASE_OSAL  CTOOLS_USECASE OSAL Functions
@ingroup CTOOLS_USECASE_API
*/

/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/
 
 /** @addtogroup CTOOLS_USECASE_FUNCTION
@{ 
*/
 
/** 
 *  @b Description
 *  @n  
 *      The very first function to be called once per system initialization for resource initializations
 * 
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - CTOOLS_DSPTRACE_OPEN_ERROR, CTOOLS_DSPTRACE_SETCLK_ERROR, CTOOLS_DSPTRACE_ENABLE_ERROR and CTOOLS_DSPETB_NOT_ENABLED
 *
 *  \par Please Note:
 *       In a Multicore environment, the DSP trace initialization (::ctools_dsptrace_init) should be done seperately
 *       from individual cores (Each core has to call its own DSP trace initialization API). A single core (master core)
 *       cannot perform DSP trace initialization for all the other cores. If user is not using Emulator (XDS560 PRO or XDS560T)
 *       for draining DSP trace, then DSP Trace initialization(::ctools_dsptrace_init) should be done on individual cores, only
 *       after the DSP ETB is initialized (using ::ctools_etb_init) by the individual cores. ::ctools_dsptrace_init will return
 *       error if DSP ETB is not yet initialized.
 *
 */

ctools_Result ctools_dsptrace_init (void);

/** 
 *  @b Description
 *  @n  
 *      The very final function to be called to release all DSP Trace related resources
 *
 *  @retval
 *      Success - DBG_TOOLS_SOK
 *
 *  @retval
 *      Failure - CTOOLS_DSPTRACE_DISABLE_ERROR, CTOOLS_DSPTRACE_CLOSE_ERROR and CTOOLS_AET_RELJOB_ERROR
 *
 *  \par Please Note:
 *       In a Multicore environment, the DSP trace shutdown (::ctools_dsptrace_shutdown) should be done seperately
 *       from individual cores (Each core has to call its own DSP trace shutdown API). A single core (master core)
 *       cannot perform DSP trace shutdown for all the other cores.
 */

ctools_Result ctools_dsptrace_shutdown  (void);

/** 
 *  @b Description
 *  @n  
 *      The very first function to be called once per system initialization for system trace resource initializations
 *
 *  @param[in] sys_trace_options
 *      Provide option to capture either CPT or SW messages or both CPT and SW messages
 * 
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - CTOOLS_SYSTEMTRACE_ENABLE_ERROR and CTOOLS_SYSETB_NOT_ENABLED
 *
 *  \par Please Note:
 *       In a Multicore environment, the System trace initialization (::ctools_systemtrace_init) should be done seperately
 *       from individual cores (Each core has to call its own system trace initialization API). A single core (master core)
 *       cannot perform system trace initialization for all the other cores. If user is not using Emulator (XDS560V2 or XDS560 PRO)
 *       for draining system trace, then System Trace initialization(::ctools_systemtrace_init) should be done on individual cores, only
 *       after the system ETB is initialized (using ::ctools_etb_init) by the master core. ::ctools_systemtrace_init will return
 *       error if system ETB is not yet initialized.
 */

ctools_Result ctools_systemtrace_init (const ctools_sys_trace_opts_e sys_trace_options);

/** 
 *  @b Description
 *  @n  
 *      This function can be used by the App SW to get DCM data for the current system trace session.

 *  @param[in] DCM_Info
 *      Pointer to STM DCM info object
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - CTOOLS_INVALID_PARAM
 *
 */

ctools_Result ctools_systemtrace_getdcmdata (STM_DCM_InfoObj * DCM_Info);

/**
 *  @b Description
 *  @n
 *      The very final function to be called to release all the resources used for system trace
 
 *  @param[in] sys_trace_options
 *      Provide option to capture either CPT or SW messages or both CPT and SW messages
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  \par Please Note:
 *       In a Multicore environment, the System trace shutdown (::ctools_systemtrace_shutdown) should be done seperately
 *       from individual cores (Each core has to call its own System trace shutdown API). A single core (master core)
 *       cannot perform System trace shutdown for all the other cores.
 *
 */
ctools_Result ctools_systemtrace_shutdown (const ctools_sys_trace_opts_e sys_trace_options);
/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_H*/

/* Nothing past this point */
