/*
 *   @file  ctools_uclib_stat_prof.h
 *
 *   @brief   
 *      Provides APIs to start and stop statistical profile trace jobs for capturing PC and timing trace at a fixed sampling period
 *       for calculating % cycle distribution among various functions in a given application.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/
#ifndef __CTOOLS_UCLIB_STAT_PROF_H
#define __CTOOLS_UCLIB_STAT_PROF_H

#ifdef __cplusplus
extern "C" {
#endif
/** 
 *
 * @page Statistical_Profiling Statistical Profiling
 *
 *   Statistical profiling provides a light weight(based on the number of Trace samples needed to be captured) and coarse profiling of the entire application.
 *   Basically, PC trace is captured at periodic sampling intervals. By analyzing these PC trace samples, a histogram(percent of total execution time and number
 *   of times encountered) for all functions called in the application can be obtained. Please, follow the below steps to perform statistical profiling:
 *
 *   - @ref ctools_stat_prof_start (sampling period) - Start statistical profiling with a particular sampling period.
 *   - Application code
 *   - @ref ctools_stat_prof_end() - Stop statistical profiling
 *
 * The PC Trace can be exported out for analysis in 3 ways:
 *     - \par DSP ETB drain using CPU (capture only 4KB of PC trace data)
 * 			- @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_CPU, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * 			- @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * 			- As shown above, do all the steps for starting and stopping statistical profiling as required
 * 			- @ref ctools_etb_cpu_drain(data_ptr, size_in, &size_out, &wrap_flag, @ref CTOOLS_DSP_ETB) - Drain ETB to specified system memory locations, after the stop function is executed.
 *                  The ETB is disabled after the drain is complete.
 * 			- @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *     - \par DSP ETB drain using EDMA (ETB extension to capture more than 4KB)
 * 			- @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_EDMA, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * 			- @ref ctools_edma_resrc_get (&num_params, &num_channels) - get the resource requirement for the EDMA params and channels
 * 			- @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * 			- As shown above, do all the steps for starting and stopping statistical profiling as required
 * 			- @ref ctools_etb_edma_drain(pct_res_ptr, @ref CTOOLS_DSP_ETB) - Returns the PC trace information in the return structure if EDMA is used a drain for ETB.
 *        			The ETB is disabled after the drain is complete.
 * 			- @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *     - \par External Emulator like XDS560 PRO or XDS560T
 * 			- @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * 			- As shown above, do all the steps for starting and stopping statistical profiling as required
 * 			- @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 * For more details about statistical profiling and post processing scripts refer to the following Wiki page: http://processors.wiki.ti.com/index.php/Statistical_Profiling
 */

//Include files
//Using forward slashes in the relative serach path for linux compatibility
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../aet/include/aet.h"

//This Structure contains all the required statistical profile resources
typedef struct  {
	AET_jobParams statprof_params;  /* Statistical profiling Parameter Structure */
	AET_jobIndex statprof_jobnumber; /* Job Number returned by AETLIB */
	AET_counterConfigParams counter0; /* Parameter structure for AET counter0 */
}ctools_statprof_resources;

/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{
*/

/*! \par ctools_memwatch_resources_pntr
 * @brief   Pointer to the internal memory watch resources structure
*/
//typedef struct _ctools_statprof_resources* ctools_statprof_resources_pntr;
#define ctools_statprof_resources_pntr ctools_statprof_resources*
 
/**
@}
*/

/**
@addtogroup CTOOLS_USECASE_SYMBOL
@{

@}
*/


/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/

/** @addtogroup CTOOLS_USECASE_FUNCTION
@{
*/
/**
 *  @b Description
 *  @n
 *      The function opens and initializes all trace resources required for a statistical profiling job.
 *      The Trace job is started immediately, after it is setup.
 *
 *   @param[in]  SamplingPeriod
 *      Periodic sampling period (CPU cycles) between two PC trace captures. Please, select a prime number to reduce the
 *      probability of coinciding with a periodic event in the application. The Sampling period selected depends on maximum
 *      size of Trace buffer and the application which is profiled statistically. Please, make sure that this period doesn't
 *      coincide with other periodic events in the application.
 *
 *  @retval
 *      CTOOLS_SOK - Success
 *
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR

    \par Details:

    Example API call:

    \code

    #define SAMPLING_INTERVAL 15013 //Select a prime number

    // Start statistical profiling trace capture
    if (ctools_stat_prof_start(SAMPLING_INTERVAL) < 0)
    {
        printf ("Error: Statistical profiling Start Failed\n");
        return;
    }

    \endcode
 */

ctools_Result ctools_stat_prof_start(uint32_t SamplingPeriod);

/**
 *  @b Description
 *  @n

    End and close statistical profiling trace job for profiling the application.

 *  @retval
 *      CTOOLS_SOK - Success

    \par Details:

    Example API call:

    \code

    // End statistical profiling trace capture
    if (ctools_stat_prof_end() < 0)
    {
        printf ("Error: Statistical profiling End Failed\n");
        return;
    }

    \endcode
*/
ctools_Result ctools_stat_prof_end  (void);
/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /* __CTOOLS_UCLIB_STAT_PROF_H */




