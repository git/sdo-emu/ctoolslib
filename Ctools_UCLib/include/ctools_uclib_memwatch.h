/*
 *   @file  ctools_uclib_memwatch.h
 *
 *   @brief   
 *       This is the ctools use case library include file for memory watch interface.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef __CTOOLS_UCLIB_MEMWATCH_H
#define __CTOOLS_UCLIB_MEMWATCH_H

#ifdef __cplusplus
extern "C" {
#endif

/** 
 *
 * @page Memory_Watch Memory Watch 
 * The Advanced Event Triggering (AET) Hardware is setup to watch writes to a particular memory range. For every write to the memory range, 
 * the AET performs a store Program Address and captures the exact location in code, where the write happenned or the AET can be configured
 * to generate an interrupt to the Core Interrupt Controller(Core INTC), which if required can be routed as an exception to the CPU. This is very useful
 * to debug any memory corruption issues from unintended accesses. The idea here is to setup the memory watch job during the system startup
 * and instrument the application to disable the memory watch during intended accesses and re-enable it after it is done with the intended accesses.
 *
 * The sequence for configuring memory watch point is given below:
 * - @ref ctools_memwatch_aetint_setup () or @ref ctools_memwatch_store_pc_setup ()  - Setup and start the memory watchpoint job for either storing PC or
 *        generating AET interrupt on an unintended access.
 * - Application code were unintended accesses to the required memory range might occur.
 * - @ref ctools_memwatch_disable () - Should be called before any valid or intended accesses to the memory range being watched.
 * - Application code were intended accesses to the required memory range happen.
 * - @ref ctools_memwatch_enable() - Should be called after any valid or intended accesses to the memory range being watched.
 * - Application code were unintended accesses to the required memory range might occur.
 * - @ref ctools_memwatch_close() - Close and free up all resources allocated for the memory watch job
 *
 * If @ref ctools_memwatch_store_pc_setup () is used, the captured PC trace samples can be exported for analysis in 2 ways.
 *    - \par DSP ETB drain using CPU (capture only 4KB of PC trace data)
 * 			- @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_CPU, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * 			- @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * 			- As shown above, do all the steps required for setting up the memory watch point
 * 			- @ref ctools_etb_cpu_drain(data_ptr, size_in, &size_out, &wrap_flag, @ref CTOOLS_DSP_ETB) - Drain ETB to specified system memory locations.
 *        			The ETB is disabled after the drain is complete.
 * 			- @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *    - \par External Emulator like XDS560 PRO or XDS560T
 * 			- @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * 			- As shown above, do all the steps required for setting up the memory watch point
 * 			- @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 * \b Please \b Note: If required, using EDMA the DSP ETB can be extended to more than 4 KB for storing PC trace samples. However, generally the number of
 *                    unintended accesses should be small and hence, the in built 4 KB ETB should suffice. The Sequence for EDMA based ETB extension can be
 *                    found in PC Trace use cases section.
 */ 

//Include files
//Using forward slashes in the relative serach path for linux compatibility
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../aet/include/aet.h"

// Internal data structures

/**
 * Memory watch options
 */
typedef enum
{
   CTOOLS_MEMWATCH_AETINT,      /* Memory watch point will trigger an AET interrupt, which can be routed as a exception  */
   CTOOLS_MEMWATCH_STORE_PC /* Only CPT messages are captured in the SYS ETB */
} ctools_memwatch_opts_e;

//This Structure contains all the required memory watch resources
typedef struct  {
	AET_jobParams memwatch_params;  /* Data Watchpoint Parameter Structure */
	AET_jobIndex memwatch_jobnumber; /* Job Number returned by AETLIB */
	ctools_memwatch_opts_e memwatch_opt; /* Memory watch point mode (AET interrupt or ETB) used */

}ctools_memwatch_resources;

/**
@addtogroup CTOOLS_USECASE_SYMBOL
@{
*/

/**
@}
*/

/**
@addtogroup CTOOLS_USECASE_ENUM
@{
*/

/**
@}
*/

/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{ 
*/

/*! \par ctools_memwatch_resources_pntr
 * @brief   Pointer to the internal memory watch resources structure
*/
//typedef struct _ctools_memwatch_resources * ctools_memwatch_resources_pntr;
#define ctools_memwatch_resources_pntr ctools_memwatch_resources*

/**
@}
*/

/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/

/** @addtogroup CTOOLS_USECASE_FUNCTION
@{
*/

/** @b Description
 *  @n
 *      The function opens and enables watch point on a memory range.
 *      For every write to the specified memory range, the AET performs a store Program Address
 *      and captures the exact location in code, where the write happened. The list of writes to
 *      the specified memory range are captured in the ETB.
 *
 * *  @param[in]  data_StartAddress
 *      Start address of the data to be monitored
 *    @param[in]  data_EndAddress
 *      End address of the data to be monitored
 *
 *  @retval
 *      CTOOLS_SOK - Success
 *
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 */

ctools_Result ctools_memwatch_store_pc_setup (uint32_t * data_StartAddress, uint32_t * data_EndAddress);

/** @b Description
 *  @n
 *      The function opens and enables watch point on a memory range.
 *      Whenever a write is done to the specified memory range, the AET generates an interrupt,
 *      which is applied as an input to the Core Interrupt controller (Core INTC). The Core INTC
 *      can be configured to route this interrupt as an exception to the CPU.
 *
 * *  @param[in]  data_StartAddress
 *      Start address of the data to be monitored
 *    @param[in]  data_EndAddress
 *      End address of the data to be monitored
 *
 *  @retval
 *      CTOOLS_SOK - Success
 *
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 */

ctools_Result ctools_memwatch_aetint_setup (uint32_t * data_StartAddress, uint32_t * data_EndAddress);

/** @b Description
 *  @n
 *      The function disables memory watch point.
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 */

ctools_Result ctools_memwatch_disable (void);

/** @b Description
 *  @n
 *      The function enables memory watch point.
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 *
 */

ctools_Result ctools_memwatch_enable (void);

/** @b Description
 *  @n
 *      The function closes and free up resources allocated for memory watch point.
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 */

ctools_Result ctools_memwatch_close (void);

/**
@}
*/

#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_MEMWATCH_H*/

/* Nothing past this point */
