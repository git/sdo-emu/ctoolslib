/*
 *   @file  ctools_uclib_pct.h
 *
 *   @brief   
 *       This is the ctools use case library pc trace specific include file.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef __CTOOLS_UCLIB_PCT_H
#define __CTOOLS_UCLIB_PCT_H

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @page PC_Trace Program Counter Trace (PC Trace)
 * PC Trace use case APIs provide the ability to debug and profile applications in real time. Data is
 * captured either by seperate hardware units (Embedded Trace Buffers, called as ETB) and then decoded
 * offline or captured using external emulators (XDS560PRO or XDS560T) and decoded using CCS.
 *
 * PC trace is useful to debug and profile only on a single core. There is no visibility to what is going
 * on at the same time in the other cores. The PC trace setup can be done either via CCS or via 'C' code
 * using the ctools use case library. This page describes the instructions for implementing PC trace features
 * via 'C' functions. The library provides the following PC Trace use cases:
 * \par Please note: Only one PC trace use case can be implemented at any point of time. It is not possible to implement multiple PC trace use cases at the same time.
 * @section pct_exp PC Trace - ETB drain via CPU during Exception or App SW fault 
 * - Description: PC trace along with timing trace information in 4KB (DSP ETB size in keystone devices) compressed buffer would be logged into the
 * hardware ETB buffer in a circular way and until any exceptions are triggered in the system or any App SW specific faults are encountered. 
 *
 * The following sequence needs to be executed for this use case:
 * - @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_CPU, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * - @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * - @ref ctools_pct_start_exc() - start the PC trace with timing information to ETB
 * - Application code (Inside the App SW fault handler, the first function call would be @ref ctools_pct_terminate_all(). This will gracefully terminate the current exception PC trace job.)
 * - @ref ctools_etb_cpu_drain(data_ptr, size_in, &size_out, @ref CTOOLS_DSP_ETB) - CPU drains ETB data to system specified memory location.
 *        This should be called from the exception handler or the App SW fault handler. The ETB is disabled after the drain is complete.
 * - @ref ctools_pct_close() - Release PC trace jobs. This should be called from the exception handler.
 * - @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 * @section pct_cpu PC Trace - ETB drain via CPU (capture only 4 KB of PC trace)
 * - Description: Capture PC trace along with timing trace information in a 4KB (DSP ETB size in keystone devices) compressed buffer.
 * The PC start function needs to be provided to start the trace and PC end function needs to be provided
 * for ending the PC trace. 
 *
 * The following sequence needs to be executed for this use case:
 * - @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_CPU, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * - @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * - @ref ctools_pct_setup (start_fxn, stop_fxn) or @ref ctools_pct_start_now () - start the PC trace with timing information to ETB
 * - Application code
 * - @ref ctools_pct_stop_now() - This step is required if @ref ctools_pct_start_now () was done in the previous step.
 *        \b Warning: @ref ctools_pct_stop_now() can only be used after @ref ctools_pct_start_now (). Using @ref ctools_pct_stop_now()
 *        after @ref ctools_pct_setup (start_fxn, stop_fxn) is not allowed.
 * - @ref ctools_etb_cpu_drain(data_ptr, size_in, &size_out, &wrap_flag, @ref CTOOLS_DSP_ETB) - Drain ETB to specified system memory locations, after the stop function is executed.
 *        The ETB is disabled after the drain is complete.
 * - @ref ctools_pct_close() - Release PC trace jobs.
 * - @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 * @section pct_edma PC Trace - ETB drain via EDMA (ETB extension to capture more than 4KB)
 * - Description: Capture PC trace along with timing trace information in a buffer greater than 4KB (DSP ETB size in keystone devices).
 * The compressed trace data will be periodically logged into the system memory using the EDMA.
 * The PC start function needs to be provided to start the trace and PC end function needs to be provided
 * for ending the PC trace.
 *
 * The following sequence needs to be executed for this use case:
 * - @ref ctools_etb_init(@ref CTOOLS_DRAIN_ETB_EDMA, &etb_config, @ref CTOOLS_DSP_ETB) - Initialize the DSP ETB
 * - @ref ctools_edma_resrc_get (&num_params, &num_channels) - get the resource requirement for the EDMA params and channels
 * - @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * - @ref ctools_pct_setup (start_fxn, stop_fxn) or @ref ctools_pct_start_now () - start the PC trace with timing information to ETB
 * - Application code
 * - @ref ctools_pct_stop_now() - This step is required if @ref ctools_pct_start_now () was done in the previous step.
 *        \b Warning: @ref ctools_pct_stop_now() can only be used after @ref ctools_pct_start_now (). Using @ref ctools_pct_stop_now()
 *        after @ref ctools_pct_setup (start_fxn, stop_fxn) is not allowed.
 * - @ref ctools_etb_edma_drain(pct_res_ptr, @ref CTOOLS_DSP_ETB) - Returns the PC trace information in the return structure if EDMA is used a drain for ETB.
 *        The ETB is disabled after the drain is complete.
 * - @ref ctools_pct_close() - Release PC trace jobs.
 * - @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 * @section pct_none PC Trace - No ETB drain (The emulator like XDS560T or XDS560PRO is used to export PC Trace)
 * - Description: PC trace along with timing trace information would be logged into the 
 * the advanced emulator's memory. This can be used for either Exception trace or trace between
 * two functions (start and end functions).
 * 
 * The following sequence needs to be executed for this use case:
 * - @ref ctools_dsptrace_init() - Initialize DSP Trace and allocate the required resources
 * - @ref ctools_pct_setup (start_fxn, stop_fxn) or @ref ctools_pct_start_now () - start the PC trace with timing information to ETB
 * - Application code
 * - @ref ctools_pct_stop_now() - This step is required if @ref ctools_pct_start_now () was done in the previous step.
 *        \b Warning: @ref ctools_pct_stop_now() can only be used after @ref ctools_pct_start_now (). Using @ref ctools_pct_stop_now()
 *        after @ref ctools_pct_setup (start_fxn, stop_fxn) is not allowed.
 * - @ref ctools_pct_close() - Release PC trace jobs.
 * - @ref ctools_dsptrace_shutdown() - free up resources initialized/utilized
 *
 */ 

//Include files
//Using forward slashes in the relative serach path for linux compatibility
#include "../../Ctools_UCLib/include/ctools_uclib_int.h"
#include "../../aet/include/aet.h"

/** @addtogroup CTOOLS_USECASE_DATASTRUCT
@{ 
*/

/*
 * ctools instance memory for the PC and Timing Trace use cases
 */
typedef struct
{
    AET_jobParams           pctStartParams;     /*!< jobParams Structures for the Start PC trace jobs */
    AET_jobParams           excEndParams;       /*!< jobParams Structures for the end PC during exception */
    AET_jobParams           pctEndParams;       /*!< jobParams Structures for the End PC trace jobs */
    AET_jobParams           timeTrcEndParams;   /*!< jobParams Structures for the End timing trace jobs */
    AET_jobIndex            startJobIndex;      /*!< jobIndex holders for the Start trace jobs */
    AET_jobIndex            endJobIndex;        /*!< jobIndex holders for the End trace jobs */   
} ctools_pct_inst_t;

/**
@}
*/

/* Exported functions */
/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/
/** @addtogroup CTOOLS_USECASE_FUNCTION
@{
*/

/**
 *  @b Description
 *  @n
 *      Open and initialize the trace job for the pc and timing
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 *
 */

ctools_Result ctools_pct_start_exc(void);

/**
 *  @b Description
 *  @n
 *      Open and initialize the trace job for the pc trace and timing
 *
 *   @param[in]  pct_start_symb
 *      start function to trigger the PC trace start
 *
 *   @param[in]  pct_stop_symb
 *      start function to trigger the PC trace stop
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 *
 */

ctools_Result ctools_pct_setup (const void* pct_start_symb, const void* pct_stop_symb);

/**
 *  @b Description
 *  @n
 *      Start the PC trace and Timing information now
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *  @retval
 *      Failure - CTOOLS_RESOURCE_BUSY, CTOOLS_AET_CLAIM_ERROR, CTOOLS_AET_SETUPJOB_ERROR and CTOOLS_AET_ENABLE_ERROR
 *
 */

ctools_Result ctools_pct_start_now(void);

/**
 *  @b Description
 *  @n
 *      Stop the PC Trace and Timing Information capture now
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *  @retval
 *      Failure - CTOOLS_INVALID_CALL_SEQ
 *
 */

ctools_Result ctools_pct_stop_now(void);

/**
 *  @b Description
 *  @n
 *      This API can be used to gracefully terminate all current PC trace jobs and can be called at any point in one's application SW.
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 */

ctools_Result ctools_pct_terminate_all (void);

/**
 *  @b Description
 *  @n
 *      Close PC trace jobs associated with the current active PC trace use case
 *
 *  @retval
 *      Success - CTOOLS_SOK
 *
 */

ctools_Result ctools_pct_close(void);
/**
@}
*/

extern ctools_pct_inst_t* PCT_INST;
#ifdef __cplusplus
}
#endif

#endif /*__CTOOLS_UCLIB_PCT_H*/

/* Nothing past this point */
