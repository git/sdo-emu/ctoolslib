/*
 * @file ctools_uclib_osal.h
 *
 *   @brief   
 *      This is the sample OS Adaptation layer which is used by the CTOOLS use case
 *      library. The OSAL layer can be ported in either of the following 
 *      manners to a native OS:
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/
 *
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __CTOOLS_UCLIB_OSAL_H
#define __CTOOLS_UCLIB_OSAL_H

#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup CTOOLS_USECASE_OSAL
 @{ */

/**********************************************************************
 ************************* Extern Declarations ************************
 **********************************************************************/

/**
 * @brief   The macro is used by the CTOOLS use case to allocate memory of specified
 * size
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void* cTools_memAlloc (uint32_t numBytes)
    @endverbatim
 *      
 *  <b> Parameter </b>
 *  @n  Number of bytes to be allocated
 *
 *  <b> Return Value </b>
 *  @n  Pointer to the allocated block size
 */

extern void * cTools_memAlloc(size_t sizeInBytes);

/**
 * @brief   The macro is used by the CTOOLS use case to free a allocated block of 
 * memory 
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void cTools_memFree (void *ptr)
    @endverbatim
 *      
 *  <b> Parameter </b>
 *  @n  Pointer to the block of memory to be cleaned up.
 *  @n  Size of the allocated memory which is being freed.
 *
 *  <b> Return Value </b>
 *  @n  Not applicable.
 */

extern void       cTools_memFree(void *);

/**
 * @brief   There OSAL functions required for both Ctools use case library,
 *          CP Tracer library and STM Library are same and should be implemented
 *          in the application. An example implementation of all the required
 *          OSAL functions is available in cToolsHelper.c. Both cToolsHelper.c
 *          and cToolsHelper.h can be found at:
 *          <CTOOLS_INSTALL_DIR>\\Examples\\common\\cToolsHelper.c
 *          The cToolsHelper.c should be compiled as a part of the application SW.
 *          For more information and description of all the required OSAL functions,
 *          which need to be implemented in the application, please refer to CPTLib
 *          Helper functions documentation at:
 *          <CTOOLS_INSTALL_DIR>\\CPTLib\\doc\\CPT_####_html\\index.html   
 *
 */
#define Ctools_all_osal             x

/**
@}
*/
#ifdef __cplusplus
}
#endif
#endif /* __CTOOLS_UCLIB_OSAL_H */

