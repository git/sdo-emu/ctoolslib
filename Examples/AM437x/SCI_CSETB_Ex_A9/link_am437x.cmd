
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x8000
-heap   0x80000                           /* HEAP AREA SIZE                */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
      VECS     : org = 0x80000000  len = 0x00001000  /* VECTOR TABLE  */
      RAM      : org = 0x80001000  len = 0x000FE000  /* 1MB - VECS */
      OCM      : org = 0x40300000  len = 0x10000     /* 64K On Chip Memory - Note: the example uses memory at 0x40310000 as DMA source */
}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > VECS               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > RAM                /* INITIALIZATION TABLES             */
    .const   : {} > RAM                /* CONSTANT DATA                     */

    .bss     : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > OCM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > RAM                /* INITIALIZATION                    */

    .data    : {} > RAM                /* DATA                              */
    .mysect  : {} > RAM                /* User defined section to make large file for benchmarks */
    ETBLib_ExtMem : {} > RAM
 }
