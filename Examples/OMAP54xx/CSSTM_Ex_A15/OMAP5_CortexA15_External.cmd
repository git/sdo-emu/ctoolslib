
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x1000
-heap   0x2000                             /* HEAP AREA SIZE                */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
    VECS     : org = 0x80000000   len = 0x00001000  /* VECTOR TABLE  */
    RAM      : org = 0x80001000   len = 0x0000F000
    RAMBASE  : org = 0x8001F000	  len = 0x00001000

}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > VECS               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > RAM                /* INITIALIZATION TABLES             */
    .const   : {} > RAM                /* CONSTANT DATA                     */

    .bss     : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > RAM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > RAM                /* INITIALIZATION                    */

    .data    : {} > RAM                /* DATA                              */
    .mysect  : {} > RAM                /* User defined section to make large file for benchmarks */
    .ramtab  : {} > RAMBASE            /* TRANSLATION TABLE - this vale is set for use with MMUarm926.asm */

	.dataImageErrStrings:PCMILib : {} > RAM
	.dataImageMsgStrings:PMILib : {} > RAM
	.dataImageMsgStrings:CMILib : {} > RAM
    .dataImageMsgStrings:PCMILib : {} > RAM

 }
