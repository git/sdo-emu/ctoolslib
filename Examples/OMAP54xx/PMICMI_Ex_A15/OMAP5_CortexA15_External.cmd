
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x1000
-heap   0x8000                             /* HEAP AREA SIZE                */

_STM_BaseAddress = 0x54000000;     /* 0x5C400000 for 4K channel boandaries */
_STM_ChannelResolution = 0x1000;    /* 0x1000 for 4K */
_DMA4_BaseAddress = 0x4700A000;    /* DMA4 base address from ARM address space*/

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
    VECS     : org = 0x80000000   len = 0x00001000  /* VECTOR TABLE  */
    RAM      : org = 0x80001000   len = 0x00020000    
    RAMBASE  : org = 0x80021000	  len = 0x00002000  

}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > VECS               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > RAM                /* INITIALIZATION TABLES             */
    .const   : {} > RAM                /* CONSTANT DATA                     */

    .bss     : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > RAM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > RAM                /* INITIALIZATION                    */

    .data    : {} > RAM                /* DATA                              */
    .mysect  : {} > RAM                /* User defined section to make large file for benchmarks */
    .ramtab  : {} > RAMBASE            /* TRANSLATION TABLE - this vale is set for use with MMUarm926.asm */
    
	.dataImageErrStrings:PCMILib : {} > RAM 
	.dataImageMsgStrings:PMILib : {} > RAM
	.dataImageMsgStrings:CMILib : {} > RAM
    .dataImageMsgStrings:PCMILib : {} > RAM
    
 }
