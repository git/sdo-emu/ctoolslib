
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x0A00
-heap   0x2000                             /* HEAP AREA SIZE                */
--entry_point=Entry
--diag_suppress=10063
/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
	DATA     : org = 0x00020000   len = 0x00002000
    RAM      : org = 0x00000000   len = 0x0000A000
}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > RAM               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > DATA                /* INITIALIZATION TABLES             */
    .const   : {} > DATA               /* CONSTANT DATA                     */

    .bss     : {} > DATA                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > RAM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > DATA                /* INITIALIZATION                    */

    .data    : {} > DATA                /* DATA                              */
    .mysect  : {} > DATA                /* User defined section to make large file for benchmarks */

    ETBLib_ExtMem > RAM

 }
