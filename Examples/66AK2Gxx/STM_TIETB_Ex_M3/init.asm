;******************************************************************************
;
; init.asm - Init code routines
;
; Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
; All rights reserved.
;
;******************************************************************************
;****************************** Global Symbols*******************************
    .global Entry
    .global _c_int00

    .text
exmap_0 .long 0x440301a0
exmap_1 .long 0x440301a4

Entry:
    ;*------------------------------------------------------
    ;* INITIALIZE THE CHIP LEVEL MEMORY MAP
    ;*------------------------------------------------------
	ldr r1, exmap_0
    mov r2, #0x0
    str r2, [r1]
    ldr r1, exmap_1
    mov r2, #0x2
    str r2, [r1]
    ;
    ; Call standard entry
    ;
    b _c_int00
