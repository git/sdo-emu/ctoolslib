-stack          0x00004000      /* Stack Size */
-heap           0x00004000      /* Heap Size */

MEMORY
{
    VECS:       o = 0x402F0000  l = 0x00000080
    ARMRAM:     o = 0x402F0080  l = 0x0000FF80  /* 64 kBytes  */
    OCMCSRAM0:  o = 0x40300000  l = 0x00020000  /* 128 kBytes */
    DDR3_0:     o = 0x80000000  l = 0x20000000  /* 256 MBytes */
    DDR3_1:     o = 0xC0000000  l = 0x20000000  /* 256 MBytes */
}

SECTIONS
{
    .bss        >   OCMCSRAM0
    .cinit      >   OCMCSRAM0
    .cio        >   OCMCSRAM0
    .const      >   OCMCSRAM0
    .stack      >   OCMCSRAM0
    .sysmem     >   OCMCSRAM0
    .text       >   OCMCSRAM0
    .ddr2       >   OCMCSRAM0
    .data       >   OCMCSRAM0
    
	.dataImageErrStrings:PCMILib : {} > OCMCSRAM0
	.dataImageMsgStrings:PMILib : {} > OCMCSRAM0
	.dataImageMsgStrings:CMILib : {} > OCMCSRAM0
    .dataImageMsgStrings:PCMILib : {} > OCMCSRAM0

    ETBLib_ExtMem > OCMCSRAM0

 }
