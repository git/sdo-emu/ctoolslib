/****************************************************************************/
/*  lnk.cmd                                                                 */
/*  Copyright (c) 1997-1999  Texas Instruments Incorporated                 */
/****************************************************************************/
-stack 0x800
-heap 0x10000

MEMORY
{
   L1D:		o=0x00f00000 l=0x00008000
   L1P:		o=0x00e00000 l=0x00008000
   ETBLIB_DMA_DATA_MEM: o=0x00811000 l=0x00004000
   L2:      o=0x00815000 l=0x00040000    /* Note that L2 actually starts at 0x00800000, reserved first 64K of L2 for buffer space */
   MSMC_MEM: o=0x0C010000 l= 0x000F0000  /* Note that MSMC starts at 0x0C000000, reserved first 64K of L2 for buffer space */
}
   
SECTIONS
{ 
  .bss        > L2
  .far        > L2
  .text       > L2
  .cinit      > L2
  .const      > L2
  .stack      > L2
  .cio		  > L2
  .sysmem	  > L2
  .switch	  > L2
  .fardata    > L2
  .neardata   > L2
  .rodata     > L2
  
  ETBLib_ExtMem > MSMC_MEM
  ETBLib_dmaData > ETBLIB_DMA_DATA_MEM
}
