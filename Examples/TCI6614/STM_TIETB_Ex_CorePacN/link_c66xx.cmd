/*---------------------------------------------------------------------------------*/
/* Description   : Contains memory mapping for C66xx*/
/* History       :                                                                 */
/****************************************************/

-x
-c
-heap 8192
-stack 8192
/* -e int00 */

MEMORY
{
  INVALID:     origin = 0x00000000 length = 0x00002000
  BOOT_MEM:    origin = 0x00800000 length = 0x00001000 /* This address is used for no boot mode */

  L1P_MEM:     origin = 0x00E00000 length = 0x00004000 /* This is the non-cache SRAM */

  L1D_MEM:     origin = 0x00F00000 length = 0x00004000 /* This is the non-cache SRAM */

  MAIL_BOX_MEM: origin =0x00801000 length = 0x00000100 /* L2 RAM dedicated for mail_box section so that the host testcase knows what address to refer to. */
  L2_MEM:      origin = 0x00801100 length = 0x00096f00 /* This is for local SRAM, could be used as cache. */
  MSMC_MEM:      origin = 0x0C1FF000 length = 0x00001000 /* MSMC SRAM. */
}

SECTIONS
{
/* common section */
    .vectors        >   BOOT_MEM
    .cinit			>	L2_MEM
    .text			>	L2_MEM
    .stack			>	L2_MEM
    .bss			>	L2_MEM
    .const			>	L2_MEM
    .data			>	L2_MEM
    .far			>	L2_MEM
    .switch			>	L2_MEM
    .tables			>	L2_MEM
    .cio			> 	L2_MEM
    .sysmem         >   L2_MEM
    .fardata        >   L2_MEM
    .rodata         >   L2_MEM
    .neardata       >   L2_MEM

/* custom section used by on_trace_1.c, cross_trig.c and sl2_swbp.c */
    .mail_box			>   	MAIL_BOX_MEM
	
	ETBLib_ExtMem > MSMC_MEM

 
}                             
