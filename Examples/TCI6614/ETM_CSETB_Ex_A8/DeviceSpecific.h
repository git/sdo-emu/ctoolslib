/*
 * ETM Device Specific Definitions  
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*******************************************************************************
 * Base Address Definitions for C6A816X Target board
 ******************************************************************************/
#define ETM_BASE_ADDRESS                    0x02580000

/* PSC MMR address */
#define PSC_BASE            (0x02350000) 
#define PSC_PDCTL(n)        (PSC_BASE + 0x300 + (4*n)) /* Power Domain Control Register */     
#define PSC_MDCTL(n)        (PSC_BASE + 0xA00 + (4*n)) /* Module Control Register  */
#define PSC_MDSTAT(n)       (PSC_BASE + 0x800 + (4*n)) /* Module Status Register */ 
#define PSC_PTCMD           (PSC_BASE + 0x120)  /* transition command register */
#define PSC_PTSTAT          (PSC_BASE + 0x128)  /* transition command status register */ 

void psc_config(void)
{
    register uint32_t retry = 1000;
    
    /* If the ARM is in user mode, access to these registers are not allowed,
     *  exit the application
     */
    if((_get_CPSR() & 0x1F) == 0x10)
    {
        exit(1);
    }

    /* Check to see if the Debug Subsystem and Tracers module status register
     *  STATE value is enabled, if not, proceed to enable.
     *  STATE = 0x0 - SwRstDisable state
     *  STATE = 0x3 - Enable state
     */
    /* !!! THIS CODE IS LEFT COMMENTED OUT:
     *      - The status shows that the DebugSS is enabled, but the ETM access
     *         will cause a data abort. The following code must get executed
     *         to keep from getting a data abort while running through the ETM
     *         example.
     */
//    if((*((volatile uint32_t*)PSC_MDSTAT(5)) & 0x3) == 0)
//    {
        /* Power up DebugSS */     
        *((volatile uint32_t*)PSC_PDCTL(1)) = 1; 
        
        // Issue GO for the DebugSS power domain
        *((volatile uint32_t*)PSC_PTCMD) = 1;
                
        //Wait for the domain transition to complete
        do 
        {
           retry--;
        } while((((*(volatile uint32_t*)PSC_PTSTAT) & 1) != 0) && (retry != 0));
        if ( retry == 0 ) return;
               
        /* Enable DEBUGSS subsystem */
        *((volatile uint32_t*)PSC_MDCTL(5)) = 0x3;
//    }

    return;
}

void etm_config_for_etb(void)
{
    /* No ETB trace funnel configuration required for TCI6614/12 */
}


