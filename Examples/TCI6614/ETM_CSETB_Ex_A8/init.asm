;******************************************************************************
;
; init.asm - Init code routines
;
; Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/
; All rights reserved.
;
;******************************************************************************
;****************************** Global Symbols*******************************
    .global Entry
    .global _c_int00
    .global psc_config
	.global	__STACK_SIZE

        .ref __stack

;**************************** Code Seection ***********************************
    .text

;
; This code is assembled for ARM instructions
;
    .state32

;******************************************************************************
;
;
Entry:
    ;*------------------------------------------------------
    ;* INITIALIZE THE STACK
    ;*------------------------------------------------------
	LDR     sp, c_stack
    LDR     r0, c_STACK_SIZE
	ADD	sp, sp, r0

	;*-----------------------------------------------------
	;* ALIGN THE STACK TO 64-BITS IF EABI.
	;*-----------------------------------------------------
	.if __TI_EABI_ASSEMBLER
	BIC     sp, sp, #0x07  ; Clear upper 3 bits for 64-bit alignment.
	.endif

    ;
    ; Call function to configure Power Sleep registers to enable DebugSS
    ;
    BL psc_config
        
    ;
    ; Call standard entry
    ;
    BL _c_int00

c_stack:
    .word __stack
c_STACK_SIZE:
  	.word __STACK_SIZE

