
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x10000
-heap   0x10000                             /* HEAP AREA SIZE                */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
	DATA     : org = 0x40300000   len = 0x00040000
    RAM      : org = 0x80000000   len = 0x00080000
    EDMA_RAM : org = 0x80080000   len = 0x00080000
}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > RAM               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > DATA                /* INITIALIZATION TABLES             */
    .const   : {} > DATA               /* CONSTANT DATA                     */

    .bss     : {} > DATA                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > DATA                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > DATA                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > DATA                /* INITIALIZATION                    */

    .data    : {} > DATA                /* DATA                              */
    .mysect  : {} > DATA                /* User defined section to make large file for benchmarks */

    ETBLib_ExtMem > RAM

 }
