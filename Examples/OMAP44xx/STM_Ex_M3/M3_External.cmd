
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x1000
-heap   0x1000                             /* HEAP AREA SIZE                */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{
    VECS     : org = 0x80000000   len = 0x00001000  /* VECTOR TABLE  */
    RAM      : org = 0x80001000   len = 0x00008000  
    RAMBASE  : org = 0x80009000	  len = 0x00002000  
/*
    RAM      : org = 0x10004000   len = 0x0000ffff  
    RAMBASE  : org = 0x10060000	  len = 0x00004000  
*/
}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > VECS               /* INTERRUPT VECTORS                 */

    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > RAM                /* INITIALIZATION TABLES             */
    .const   : {} > RAM                /* CONSTANT DATA                     */

    .bss     : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > RAM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > RAM                /* INITIALIZATION                    */

    .data    : {} > RAM                /* DATA                              */
    .mysect  : {} > RAM                /* User defined section to make large file for benchmarks */
    .ramtab  : {} > RAMBASE            /* TRANSLATION TABLE - this vale is set for use with MMUarm926.asm */
 }
