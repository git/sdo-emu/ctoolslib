
-c                                         /* LINK USING C CONVENTIONS      */
-stack  0x800
-heap   0x1000                             /* HEAP AREA SIZE                */

STM_BaseAddress = 0x54000000;     /* 0x5C400000 for 4K channel boandaries */
STM_ChannelResolution = 0x1000;    /* 0x1000 for 4K */

/* SPECIFY THE SYSTEM MEMORY MAP */

MEMORY
{   /* Additonal OMAP5 RAM:
			 0x80000000 SDRAM Bank 1
			 0xC0000000 SDRAM Bank 2
             0x40300000 Internal Ram 0xE000 bytes (56K)
     */
    VECS     : org = 0x80000000   len = 0x00001000  /* VECTOR TABLE  */
    RAM      : org = 0x80001000   len = 0x0001D000    
    RAMBASE  : org = 0x8001E000	  len = 0x00002000   

}

/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY */

SECTIONS
{

    .intvecs : {} > VECS               /* INTERRUPT VECTORS                 */
    .text    : {} > RAM                /* CODE                              */
    .cinit   : {} > RAM                /* INITIALIZATION TABLES             */
    .const   : {} > RAM                /* CONSTANT DATA                     */

    .bss     : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem  : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
    .stack   : {} > RAM                /* SOFTWARE SYSTEM STACK             */
    .pinit   : {} > RAM                /* INITIALIZATION                    */

    .data    : {} > RAM                /* DATA                              */
    .mysect  : {} > RAM                /* User defined section to make large file for benchmarks */
    .ramtab  : {} > RAMBASE            /* TRANSLATION TABLE - this vale is set for use with MMUarm926.asm */
    
	.dataImageErrStrings:PCMILib : {} > RAM 
	.dataImageMsgStrings:PMILib : {} > RAM
	.dataImageMsgStrings:CMILib : {} > RAM
    .dataImageMsgStrings:PCMILib : {} > RAM
    
 }
