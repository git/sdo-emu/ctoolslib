To build STM driver, you need complete kernel source code tree. 

For user and kernel mode API documentation see pmi_html/index.html and cmi_html/index.html

The following steps provide an example of how to add the PMI and CMI Linux drivers in the BUILD scheme:
1. Change folder to point to the character-driver folder in the kernel source code hierarchy - e.g. ��/kernel/drivers/char� 

2. Extract the PMI and CMI driver source & header files from Archive_pmicmi_driver_x.x.tgz 
   Note: This module also includes a pmicmi_tst module that can be used to access the drivers through the kernel API.

3. Modify �Kconfig� file here to add the following lines:

config PMI
        tristate "PMI (Power Management Instrumentation) Driver support"
        default n
        help
          This driver allows control of hardware generated power STM messages.
          Need to document PMI load parameters.

config CMI
        tristate "CMI (Clock Management Instrumentation) Driver support"
        default n
        help
          This driver allows control of hardware generated clock STM message.

Optionally add the following:

config PMICMI_TST
        tristate "PMI CMI API Test Module"
        default n
        help
          This module is for PMI/CMI driver kernel level API testing only
          and is not meant for distribution.


Modify the �Makefile� here to include the following line:
pmi_mod-objs                    := pmi.o PCMI_Common.o PMILib.o
obj-$(CONFIG_PMI)               += pmi_mod.o

cmi_mod-objs                    := cmi.o PCMI_Common.o CMILib.o
obj-$(CONFIG_CMI)               += cmi_mod.o

If you are including the test module add:
obj-$(CONFIG_PMICMI_TST)        += pmicmi_tst.o

Change directory to kernel folder (e.g. cd �/kernel) & modify kernel configuration to select PMI an CMI drivers as module:
Run: make menuconfig ARCH=arm
Navigate to  PMI driver & select this as a module
Navigate to  CMI driver & select this as a module
Optionally navigate to PMICMI_TST driver & select this as a module
Save & exit.
Run: make ARCH=arm modules
Modules (pmi_mod.ko, cmi_mod.ko, pmicmi_tst.ko) are built in the directory of the driver source files.
