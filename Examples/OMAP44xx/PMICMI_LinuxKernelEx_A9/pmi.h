/*
 * pmi.h
 *
 * Power Management Instrumentation Linux Kernel Module Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file pmi.h
    pmi kernel module 
*/
/*! \mainpage
    The pmi kernel module provides control over the PMI HW at module load time,
    provides user mode access through a standard char driver interface,
    and it exports a PMI Module programming API for kernel access.
    
    The module utilizes a lock to allow only a single client (user or kernel) 
    to open the device at any one time. There is no prioritization of one
    client type over another. The lock is provided on a purley first come
    first serve basis.
     
    Use the following command to load the module:
    
    # insmod pmi_mod.ko
    
    The following parameters can also be set at load time:
    
    \n uint clk_div - Clock divider selection (1-16), default is 16 
    \n unit smp_win - Sample window counter (1-256), default is 256
    \n bool lvd - Logic Voltage Domain OPP Change enable, default is enabled
    \n bool mvd - Memory Voltage Domain OPP Change enable, default is enabled
    \n bool lpd - Logic Power Domain State Change enable, default is enabled
    \n bool mpd - Memory Power Domain State Change enable, default is enabled

    
    Sample window clocks is clk_div * smp_win( maximum value is 4096).
    
    Default at load time is all domains enabled for moinitoring and the sample window size is set to the maximum.
    
    Example to change defaults:
    
    # insmod pmni_mod.ko clk_div=8 smp_win=128 lpd=0 mpd=0
    
    This changes the sample window size in clocks to 2048 and leave the lvd and mvd enables set.
    
    If any domins are enabled at module load time, activity monitoring is enabled and STM message generation is
    started immediatly. Setting all domain enables to 0 will cause the module to not enable activity monitoring.
    In this case either a user mode or kernel level call to the driver must be used to enable activity
    monitoring. 

    Module parameters can not be changed once the module is loaded.
    
    Use the following commands to register the device for user mode access:
    
    To load the pmi module with monitoring disabled
    \n # insmod pmi_mod.ko lvd=0, mvd=0, lpd=0, mpd=0 
    \n Get the pmi major device number (also provided by insmod pmi_mod.ko)
    \n # cat /proc/devices
    \n Make a device node
    \n # mknod /dev/pmi c major 0
    \n Check that the device node is valid
    \n # ls -l /dev/pmi

    The following user mode commands are supported through standard Linux open and ioctl functions.
 
    \par int open(char * dev, int flags)

    This is the standard Linux open command.
    
    \param[in] dev            const char pointer to device name (like "/dev/pmi")
    \param[in] flags          standard access modes (O_WRONLY)
    \return                   If no error the pmi device handle is returned, if error -1 returned and errno set to error code 
    
    This function calls pmi_kopen() so see it for functional details. 
    
    Example: int pmi_fd = open("/dev/pmi", O_WRONLY);
         
    \par int ioctl( int pmi_dev_hdl, int request, ...) 
    
    Where the type of the third parameter is dependent on request. This is the standard Linux ioctl command.
        
    \param[in] pmi_dev_hdl      pmi device handle returned from open
    \param[in] request          see ... for valid requests

    \param[in] ...              The third parameter type is dependent on the request
                                \li PMI_IOC_ENABLE  Third parameter not defined - use 0        
                                \li PMI_IOC_DISABLE Third parameter not defined - use 0    
                                \li PMI_IOC_CFG     Third parameter is an uint whose bits are defined by ::pmi_cfg_t   
                                \li PMI_IOC_GETSTATE Third parameter is an int pointer whose return value is 0 (disabled) or 1 (enabled)   
    `                           \li PMI_IOC_GETCFG   Third parameter is a uint pointer whose return value is defined by ::pmi_cfg_t
    \return                    If 0 no error, on error -1 returned and errno set to the error code.       
         
    \par PMI Kernel Module Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.1 </TD> <TD> 8/1/2010 </TD> <TD> pmi kernel module alpha release  </TD>
<TR><TD> 0.2 </TD> <TD> 9/14/2010 </TD> <TD> added user mode interface  </TD>
</TABLE>
   
*/

#include <linux/ioctl.h>

//////////////////////////////////////////////////////////////////////////////
// Definitions                                                              //
//////////////////////////////////////////////////////////////////////////////

#ifndef _DOXYGEN_IGNORE
//  Using 0xE0 as the majic number
#define PMI_IOC_MAGIC 0xE0
#endif

#define PMI_IOC_ENABLE      _IO(PMI_IOC_MAGIC, 0)           /*!< Enable PMI Module - Input command with no parameters */
#define PMI_IOC_DISABLE     _IO(PMI_IOC_MAGIC, 1)           /*!< Disable PMI Module - Input command with no parameters */
#define PMI_IOC_CFG         _IOW(PMI_IOC_MAGIC, 2, int)     /*!< Configure PMI Module - Input command that requires a uint parameter whose bits are defined by pmi_cfg_t */
#define PMI_IOC_GETSTATE    _IOR(PMI_IOC_MAGIC, 3, int)     /*!< Get PMI Module operating state - Output command that requires a int pointer parameter whose return value is 
                                                                 0 if the PMI module is diabled, 1 if the module is enabled */
#define PMI_IOC_GETCFG      _IOR(PMI_IOC_MAGIC, 4, int)     /*!< Get CMI Module configuration - Output command that requires a uint pointer whose return value is defined by pmi_cfg_t */



//////////////////////////////////////////////////////////////////////////////
// Public Enumerations                                                      //
//////////////////////////////////////////////////////////////////////////////

/*! \par pmi_errs_t
    
   PMI Module Errors - These errors extend the generic linux error codes
*/
typedef enum {  // The following are module errors                 
                EPMI_LIBERR = 200,                     /*!< PMILib error */
                // The following are API errors
                EPMI_HNDL,                           /*!< Handle error detected */
                EPMI_STATE                           /*!< Incorrect state for function */
                
             } pmi_err_t;

/*! \par pmi_id_t
    PMI module id. This parameter is defined to support expansion for multiple PMI modules.
    (OMAP430 has two CMI modules) 
*/

typedef enum {  PM_ID,                           /*!< PM module id */
                PMI_MODULE_CNT
              } pmi_id_t;
                                                    
/*! \par pmi_cfg_t
    
    PMI module cofiguration defines the domains that are enabled. Default at module load
    time is to enable all domains.
*/ 
typedef enum {  PMI_LVD_EN = 1 << 0,            /*!< PMI Module is enabled for LVD STM message generation */
                PMI_MVD_EN = 1 << 1,            /*!< PMI Module is enabled for MVD STM message generation */
                PMI_LPD_EN = 1 << 2,            /*!< PMI Module is enabled for LPD STM message generation */
                PMI_MPD_EN = 1 << 3,            /*!< PMI Module is enabled for MPD STM message generation */
             } pmi_cfg_t;

#ifndef _DOXYGEN_IGNORE
//////////////////////////////////////////////////////////////////////////////
// Private definitions                                                      //
//////////////////////////////////////////////////////////////////////////////
#ifdef PMI_DEBUG
#define dbg_print(fmt, args...) printk(KERN_DEBUG fmt, ## args)
#else
#define dbg_print(fmt, args...) /* Nothing */
#endif 

typedef struct { struct pmi_dev_it * pmi_dev;  //pmi_dev_it is an incomplete struct type (it)
                    
                } pmi_kdev_t;

#endif
//////////////////////////////////////////////////////////////////////////////
// Valid User mode functions                                                //
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
// Kernel function prototypes                                               //
//////////////////////////////////////////////////////////////////////////////

/*! \par pmi_kopen         
    
    Open the pmi device kernel API. Aquire ownership of the device's PMI module. 
    
    \param[in] pmi_id           ::pmi_id_t enumerated module intentifier.
    \param[out] err             Pointer to a ::pmi_err_t used to return the error value.
    \return pmi_kdev_t          pmi device handle
    

    \par Details:
    \details   
    
    This function will acquire ownership of the pmi kernel module for the client
    (assumed to be another kernel module). If the API is already opened (by another
    kernel module or user application), or if the module was not loaded properly
    (PMILib open failed), or if pmi_id is not a valid id pmi_kopen will return NULL.
    Check the printk log for module errors.

    This function must be called and return a valid pmi device handle pointer (not NULL)
    prior to calling any other pmi module functions.
    The module state is not chnged by this function. Use pmi_kgetstate() to retrive the
    current state. The state must be disabled to modify the domian enables.    

    If pmi_kopen is successfull, API's are avaiable to easily save the current operating state, modify
    that state for your use and then restore the original operating state when your use
    of the module is complete per the following example:
    
    \verbatim
    {
        uint32_t pmi_cfg_org, pmi_cfg_new;
        bool pmi_state_org;
  
        pmi_kdev_t pmi_hndl = pmi_kopen(PM_ID);
        if ( NULL == pmi_hndl) {
            ... invoke your error handling
        }
    
        //get current state
        pmi_kgetstate(pmi_hndl, &pmi_state_org, &pmi_cfg_org);
        
        //modify pmi state true PMI module is enabled
        if ( pmi_state_org ) {
           pmi_kdisable(pmi_hndl);
        }
        pmi_cfg_new = PMI_LVD_EN | PMI_MVD_EN;
        pmi_kcfg(pmi_hndl, pmi_cfg_new);
        pmi_kenable(pmi_hndl);
         
        // Your code
        ....
        // Note: to limit the amount of STM data generated
        // by the PMI HW module you may want to enable and
        // disable the unit appropriatly. 
         
        //All done - restore original state
        pmi_kdisable(pmi_hndl);
        pmi_kcfg(pmi_hndl, pmi_cfg_org);
        if ( pmi_state_org ) {
           pmi_kenable(pmi_hndl);
        }
        pmi_krelease(pmi_hndl);
    }
    \endverbatim   

*/
pmi_kdev_t * pmi_kopen( pmi_id_t pmi_id, pmi_err_t * err );

/*! \par pmi_krelease         
    
    Close the pmi device kernel API. Release ownership of the device's PMI module. 
    
    \param[in] pmi_hndl           ::pmi_kdev_t returned by pmi_kopen().
    \return pmi_errs_t            ::pmi_errs_t defines the potential errors. 

    \par Details:
    \details   
    
    This function will release ownership of the pmi kernel module. 
    The structure pointed to by pmi_hndl is destroyed.    
    If an invalid handle is detected (element out of range or pointer is NULL) pmi_krelease
    exits with -EHNDL. 
    This function will not modify the pmi module's operating state or change the state of the physical 
    module. 
*/
pmi_err_t pmi_krelease( pmi_kdev_t * pmi_hndl);

/*! \par pmi_kgetstate         
    
    Get the current state.
    
    \param[in] pmi_hndl           ::pmi_kdev_t returned by pmi_kopen().
    \param[out] pmi_state         pointer to bool client variable where pmi state 
                                  (true if enabled, false if disabled) is stored.
    \param[out] pmi_cfg           pointer to unit32_t client variable where pmi config is stored. 
                                  pmi_cfg is a bit field of domain enables definded by ::pmi_cfg_t.
    \return pmi_errs_t            ::pmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the pmi_hndl is valid provide the current state and configuration (doamin enables), then return ENOERR,
    otherwise return -EHNDL.
*/    
pmi_err_t pmi_kgetstate(pmi_kdev_t * pmi_hndl, bool * pmi_state, uint32_t * pmi_cfg);

/*! \par pmi_kenable         
    
    Enable export of PMI STM messages based on last call to ::pmi_kcfg().
    
    \param[in] pmi_hndl           ::pmi_kdev_t returned by pmi_kopen().
    \return pmi_errs_t            ::pmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the pmi_hndl is valid enable the export of PMI STM messages and return ENOERR, otherwise return -EHNDL.
    If an error occurs while attempting to enable the PMI HW module a -EPMI_LIBERR is returned.
*/    
pmi_err_t pmi_kenable(pmi_kdev_t * pmi_hndl);

/*! \par pmi_kenable         
    
    Disable export of PMI STM messages.
    
    \param[in] pmi_hndl           ::pmi_kdev_t returned by pmi_kopen().
    \return pmi_errs_t            ::pmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the pmi_hndl is valid disable the export of PMI STM messages and return ENOERR, otherwise return -EHNDL. 
    If an error occurs while attempting to disable the PMI HW module a -EPMI_LIBERR is returned. 
*/    

pmi_err_t pmi_kdisable(pmi_kdev_t * pmi_hndl);

/*! \par pmi_kcfg         
    
    Configure PMI module (enable domains).
    
    \param[in] pmi_hndl           ::pmi_kdev_t returned by pmi_kopen().
    \param[in] pmi_cfg            pmi_state is a bit field definded by ::pmi_cfg_t.
    \return pmi_errs_t            ::pmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    The pmi_state must be false (PMI module disabled) to call this function. 
    If the pmi_state is not false this function with exit and return -ESTATE. 
    If the pmi_hndl is valid set the state and return ENOERR, otherwise return -EHNDL. 
    If an error occurs while attempting to change the PMI HW module's configuration a -EPMI_LIBERR is returned. 
*/    

pmi_err_t pmi_kcfg(pmi_kdev_t * pmi_hndl, uint32_t pmi_cfg);


