/*
 * cmi.h
 *
 * Clock Management Instrumentation Linux Kernel Module Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file cmi.h
    cmi kernel module 
*/
/*! \mainpage
    The cmi kernel module provides control over the CMI HW at module load time (cab set the initial state)
    and provides a kernel xported CMI Module programming API. 
    Use the following command to load the module:
    
    insmod cmi_mod.ko
    
    The following parameters can also be set at load time:
    
    \n uint cm1_clk_div - Clock divider selection (1-16), default is 16 
    \n unit cm1_smp_win - Sample window counter (1-256), default is 256
    \n uint cm1_mode - see ::cmi_mode_t for valid values 
    \n uint cm1_cfg - cm1_cfg is a bit field definded by ::cmi_cfg_t. Valid values are dependent on
                  the the value of cmi_mode and the CM unit. When cmi_mode is set for event monitoring only
                  CM_EVT and CM1_EVT values are valid. When cmi_mode is set for activity monitoring
                  only CM_ACT values are valid.
    \n uint cm2_clk_div - Clock divider selection (1-16), default is 16 
    \n unit cm2_smp_win - Sample window counter (1-256), default is 256
    \n uint cm2_mode - see ::cmi_mode_t for valid values 
    \n uint cm2_cfg - cm2_cfg is a bit field definded by ::cmi_cfg_t. Valid values are dependent on
                  the the value of cmi_mode. When cmi_mode is set for event monitoring only
                  CM_EVT and CM2_EVT values are valid. When cmi_mode is set for activity monitoring
                  only CM_ACT values are valid.
 
    Sample window clocks is clk_div * smp_win( maximum value is 4096).
    
    Default at load time is both CMI modules enabled for event moinitoring and the sample window size is set to the maximum.
    
    Example to change defaults:
    
    insmod pmni_mod.ko cm1_clk_div=8 cm1_smp_win=128 cm1_mode=0 cm1_cfg = 0x1
    
    This would change the sample window size in clocks to 2048 and enable CM1 for clock state events.
    
    If you set cmn_cfg the CMn HW module is enabled for the mode selected (Event is the default), 
    and STM message generation is
    started immediatly at module load time. Setting cmn_cfg to 0 will cause the module
    to be disabled. In this case another kernel level module must enable event or activity
    monitoring through the exported API. 

    Module parameters can not be changed once the module is loaded.
    
    Use the following commands to register the device for user mode access:
    
    To load the pmi module with monitoring disabled:
    \n # insmod cmi_mod.ko cm1_cfg=0 cm2_cfg=0 
    \n Get the pmi major device number (also provided by insmod cmi_mod.ko)
    \n # cat /proc/devices  
    \n Make a device node
    \n # mknod /dev/cmi1 c major 0
    \n # mknod /dev/cmi2 c major 1
    \n Check that the device nodes are valid
    \n # ls -l /dev/cmi*

    The following user mode commands are supported through standard Linux open and ioctl functions.
 
    \par int open(char * dev, int flags)

    This is the standard Linux open command.
    
    \param[in] dev            const char pointer to device name (like "/dev/pmi")
    \param[in] flags          standard access modes (O_WRONLY)
    \return                   If no error the cmi device handle is returned, if error -1 returned and errno set to error code 
    
    This function calls cmi_kopen() so see it for functional details. 
    
    Example: int cmi1_fd = open("/dev/cmi1", O_WRONLY);
             int cmi2_fd = open("/dev/cmi2", O_WRONLY);
         
    \par int ioctl( int pmi_dev_hdl, int request, ...) 
    
    Where the type of the third parameter is dependent on request. This is the standard Linux ioctl command.
        
    \param[in] cmi_dev_hdl      cmi device handle returned from open
    \param[in] request          see ... for valid requests

    \param[in] ...              The third parameter type is dependent on the request
                                \li CMI_IOC_ENABLE  Third parameter not defined - use 0        
                                \li CMI_IOC_DISABLE Third parameter not defined - use 0    
                                \li CMI_IOC_CFG     Third parameter is a pointer to a struct ::cmi_ioc_cfg_t. The contents of struct pointed to are copied to the driver.   
                                \li CMI_IOC_GETSTATE Third parameter is an int pointer whose return value is 0 (disabled) or 1 (enabled)   
    `                           \li CMI_IOC_GETCFG   Third parameter is a pointer to a struct ::cmi_ioc_cfg_t. The contents of the struct pointed to are copied from the driver.
    \return                    If 0 no error, on error -1 returned and errno set to the error code.       
         

    \par CMI Kernel Module Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.1 </TD> <TD> 8/1/2010 </TD> <TD> cmi kernel module alpha release  </TD>
<TR><TD> 0.2 </TD> <TD> 9/14/2010 </TD> <TD> added user mode interface  </TD>
</TABLE>
   
*/

#include <linux/ioctl.h>


//////////////////////////////////////////////////////////////////////////////
// Public Enumerations                                                      //
//////////////////////////////////////////////////////////////////////////////

/*! \par cmi_errs_t
    
   CMI Module Errors - These errors extend the generic linux error codes
*/
typedef enum {  // The following are module specific errors                 
                ECMI_LIBERR = 200,                     /*!< PMILib error */
                // The following are API errors
                ECMI_HNDL,                           /*!< Handle error detected */
                ECMI_STATE                           /*!< Incorrect state for function */
                
             } cmi_err_t;

/*! \par cmi_id_t
    CMI module id. This parameter is defined to support expansion for multiple CMI modules.
    (OMAP430 has two CMI modules) 
*/
typedef enum { CM1_ID,                         /*!< Clock Module 0 ID */ 
               CM2_ID,                         /*!< Clock Module 1 ID */
               CMI_MODULE_CNT
             } cmi_id_t;
                                                    
/*! \par cmi_mode_t
    CMI module mode (Event or Activity collection) 
*/
typedef enum { CMI_EVENT,              /*!< Enable the module for Event collection */
               CMI_ACTIVITY            /*!< Enable the module for Activity collection */
             } cmi_mode_t;

/*! \par cmi_state_t
    
    CMI module state (enabled for message generation or disabled). This state can be
    set at module load time and is dependent on the module parameters. Default at module load
    time is to enable.
*/ 
typedef enum {  CM_ACT_TARGET_EN = 3,                   /*!< CMI Module activity enable for targets */
                CM_ACT_INITIATOR_EN = 0xc,              /*!< CMI Module activity enable for initiators */
                CM_EVT_STATE_EN = 1 << 0,               /*!< CMI Module event enable for Clock State STM message generation */
                CM_EVT_FRQDIV_EN = 1 << 2,              /*!< CMI Module event enable for Frequency Divider STM message generation */
                CM_EVT_SRCSEL_EN = 1 << 3,              /*!< CMI Module event enable for Clock Source Slection STM message generation */
                CM1_EVT_CORE_DPLL_EN = 1 << 16,         /*!< CMI Module event enable for CM1 Core DPLL STM message generation */
                CM1_EVT_MPU_DPLL_EN = 1 << 17,          /*!< CMI Module event enable for CM1 MPU DPLL STM message generation */
                CM1_EVT_IVA_DPLL_EN = 1 << 18,          /*!< CMI Module event enable for CM1 IVA DPLL STM message generation */
                CM1_EVT_ABE_DPLL_EN = 1 << 19,          /*!< CMI Module event enable for CM1 ABE DPLL STM message generation */
                CM1_EVT_DDRPHY_DPLL_EN = 1 << 20,       /*!< CMI Module event enable for CM1 DDRPHY DPLL STM message generation */
                CM2_EVT_PER_DPLL_EN = 1 << 16,          /*!< CMI Module event enable for CM2 PER DPLL STM message generation */
                CM2_EVT_USB_DPLL_EN = 1 << 17,          /*!< CMI Module event enable for CM2 USB DPLL STM message generation */
                CM2_EVT_UNIPRO_DPLL_EN = 1 << 18        /*!< CMI Module event enable for CM2 Unipro DPLL STM message generation */       
             } cmi_cfg_t;

//////////////////////////////////////////////////////////////////////////////
// Definitions                                                              //
//////////////////////////////////////////////////////////////////////////////

// ioctl definitions 
/*! \par cmi_ioc_cfg_t
    
    Structure used to pass paramters between user mode ioctl call and the device driver.
    
*/
struct cmi_ioc_cfg_t {
            cmi_mode_t  mode;               /*!< mode is definded by ::cmi_mode_t */
            uint32_t cfg;                   /*!< bit field defined by ::cmi_cfg_t */
};

#ifndef _DOXYGEN_IGNORE
//  Using 0xE1 as the magic number
#define CMI_IOC_MAGIC 0xE1
#endif

#define CMI_IOC_ENABLE      _IO(CMI_IOC_MAGIC, 0)                           /*!< Enable CMI Module - Input command with no parameters */
#define CMI_IOC_DISABLE     _IO(CMI_IOC_MAGIC, 1)                           /*!< Disable CMI Module - Input command with no parameters */
#define CMI_IOC_CFG         _IOW(CMI_IOC_MAGIC, 2, struct cmi_ioc_cfg_t)    /*!< Configure CMI Module - Input command that requires a
                                                                                 pointer to a ::cmi_ioc_cfg_t for input configuration parameters  */
#define CMI_IOC_GETSTATE    _IOR(CMI_IOC_MAGIC, 4, int)                     /*!< Get CMI Module operating state - Output command that requires a int pointer parameter that returns 
                                                                                 0 if the CMI module is diabled, 1 if the module is enabled */ 
#define CMI_IOC_GETCFG      _IOR(CMI_IOC_MAGIC, 5, struct cmi_ioc_cfg_t)    /*!< Get CMI Module configuration - Output command that requires a 
                                                                                 pointer to a ::cmi_ioc_cfg_t that returns current configuration parameters */

//////////////////////////////////////////////////////////////////////////////
// Private definitions                                                         //
//////////////////////////////////////////////////////////////////////////////
#ifndef _DOXYGEN_IGNORE

#ifdef CMI_DEBUG
#define dbg_print(fmt, args...) printk(KERN_DEBUG fmt, ## args)
#else
#define dbg_print(fmt, args...) /* Nothing */
#endif 

typedef struct { struct cmi_dev_it * cmi_dev;  //cmi_dev_it is an incomplete struct type (it)
                    
                } cmi_kdev_t; 

#endif
//////////////////////////////////////////////////////////////////////////////
// Kernel function prototypes                                               //
//////////////////////////////////////////////////////////////////////////////

/*! \par cmi_kopen         
    
    Open the cmi device kernel API. Aquire ownership of the device's CMI module. 
    
    \param[in] cmi_id           ::cmi_id_t enumerated module intentifier.
    \param[out] err             Pointer to a ::cmi_err_t used to return the error value.
    \return cmi_kdev_t           cmi device handle
    

    \par Details:
    \details   
    
    This function will acquire ownership of the cmi kernel module for the client
    (assumed to be another kernel module). If the API is already opened (by another
    kernel module or user application), or if the module was not loaded properly
    (CMILib open failed), or if cmi_id is not a valid id cmi_kopen will return NULL.
    Check the printk log for module errors.

    This function must be called and return a valid cmi device handle pointer (not NULL)
    prior to calling any other cmi module functions.
    The module state is not chnged by this function. Use cmi_kgetstate() to retrive the
    current state. The state must be disabled to modify the domian enables.    

    If cmi_kopen is successfull, API's are avaiable to easily save the current operating state, modify
    that state for your use and then restore the original operating state when your use
    of the module is complete per the following example:
    
    \verbatim
    {
        uint32_t cmi_cfg_org, cmi_cfg_new;
        uint32_t cmi_mode_org, cmi_mode_new;
        bool cmi_state_org;
  
        cmi_kdev_t cmi_hndl = cmi_kopen(CM0_ID);
        if ( NULL == cmi_hndl) {
            ... invoke your error handling
        }
    
        //get current state
        cmi_kgetstate(cmi_hndl, &cmi_state_org, &cmi_mode_org, &cmi_cfg_org);
        
        //modify cmi state. If true CMI module needs to be disabled
        if ( cmi_state_org ) {
           cmi_kdisable(cmi_hndl);
        }
        cmi_cfg_new = CM_EVT_STATE_EN;
        cmi_mode_new = CMI_EVENT;
        cmi_kcfg(cmi_hndl, cmi_mode_new, cmi_cfg_new);
        cmi_kenable(cmi_hndl);
         
        // Your code
        ....
        // Note: to limit the amount of STM data generated
        // by the CMI HW module you may want to enable and
        // disable the unit appropriatly. 
         
        //All done - restore original state
        cmi_kdisable(cmi_hndl);
        cmi_kcfg(cmi_hndl, cmi_mode_org, cmi_cfg_org);
        if ( cmi_state_org ) {
           cmi_kenable(cmi_hndl);
        }
        cmi_krelease(cmi_hndl);
    }
    \endverbatim   

*/
cmi_kdev_t * cmi_kopen( cmi_id_t cmi_id, cmi_err_t * err );

/*! \par cmi_krelease         
    
    Close the cmi device kernel API. Release ownership of the device's CMI module. 
    
    \param[in] cmi_hndl           ::cmi_kdev_t returned by cmi_kopen().
    \return cmi_errs_t            ::cmi_errs_t defines the potential errors. 

    \par Details:
    \details   
    
    This function will release ownership of the cmi kernel module. 
    The structure pointed to by cmi_hndl is destroyed.    
    If an invalid handle is detected (element out of range or pointer is NULL) cmi_krelease
    exits with -EHNDL. 
    This function will not modify the cmi module's operating state or change the state of the physical 
    module. 
*/
cmi_err_t cmi_krelease( cmi_kdev_t * cmi_hndl);

/*! \par cmi_kgetstate         
    
    Get the current state.
    
    \param[in] cmi_hndl           ::cmi_kdev_t returned by cmi_kopen().
    \param[out] cmi_state         pointer to bool client variable where cmi state 
                                  (true if enabled, false if disabled) is stored.
    \param[out] cmi_mode          pointer to ::cmi_mode_t client variable where cmi mode is stored.
    \param[out] cmi_cfg           pointer to unit32_t client variable where cmi config is stored. 
                                  cmi_cfg is a bit field of domain enables definded by ::cmi_cfg_t.
    \return cmi_errs_t            ::cmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the cmi_hndl is valid provide the current state and configuration (doamin enables), then return ENOERR,
    otherwise return -EHNDL.
*/    
cmi_err_t cmi_kgetstate(cmi_kdev_t * cmi_hndl, bool * cmi_state, cmi_mode_t  * cmi_mode, uint32_t * cmi_cfg);

/*! \par cmi_kenable         
    
    Enable export of CMI STM messages based on last call to ::cmi_kcfg().
    
    \param[in] cmi_hndl           ::cmi_kdev_t returned by cmi_kopen().
    \return cmi_errs_t            ::cmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the cmi_hndl is valid enable the export of CMI STM messages and return ENOERR, otherwise return -EHNDL.
    If an error occurs while attempting to enable the CMI HW module a -ECMI_LIBERR is returned.
*/    
cmi_err_t cmi_kenable(cmi_kdev_t * cmi_hndl);

/*! \par cmi_kenable         
    
    Disable export of CMI STM messages.
    
    \param[in] cmi_hndl           ::cmi_kdev_t returned by cmi_kopen().
    \return cmi_errs_t            ::cmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    If the cmi_hndl is valid disable the export of CMI STM messages and return ENOERR, otherwise return -EHNDL. 
    If an error occurs while attempting to disable the CMI HW module a -ECMI_LIBERR is returned. 
*/    

cmi_err_t cmi_kdisable(cmi_kdev_t * cmi_hndl);

/*! \par cmi_kcfg         
    
    Configure CMI module (enable domains).
    
    \param[in] cmi_hndl           ::cmi_kdev_t returned by cmi_kopen().
    \param [in] cmi_mode          ::cmi_mode_t defines the valid values to select event or activity monitoring
    \param[in] cmi_cfg            cmi_cfg is a bit field definded by ::cmi_cfg_t. Valid values are dependent on
                                  the the value of cmi_mode. When cmi_mode is set for event monitoring only
                                  ::cmi_cfg EVT values are valid. When cmi_mode is set for activity monitoring
                                  only ACT values are valid.  
    \return cmi_errs_t            ::cmi_errs_t defines the potential errors. 

    \par Details:
    \details
    
    The cmi_state must be false (CMI module disabled) to call this function. 
    If the cmi_state is not false this function with exit and return -ESTATE. 
    If the cmi_hndl is valid set the state and return ENOERR, otherwise return -EHNDL. 
    If an error occurs while attempting to change the CMI HW module's configuration a -ECMI_LIBERR is returned. 
*/    

cmi_err_t cmi_kcfg(cmi_kdev_t * cmi_hndl, cmi_mode_t cmi_mode, uint32_t cmi_cfg);


