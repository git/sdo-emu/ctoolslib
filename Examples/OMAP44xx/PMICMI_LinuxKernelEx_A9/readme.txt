This directory includes a tar file that contains the pmi and cmi driver
source and a kernel test module. See readme_dvr.txt for instructions on 
building the pmi, cmi and optional test module.

This directory also include pmi_html and pmi_html directories that contain Doxygen 
API documentation. Simply load the index.html files from each directory with your
favorite browser to explore.

This directory also contains a user mode pmi and cmi test project and a build script.
To build a debug version in the Debug directory simply run:
# ./build Debug 

Before runing the user mode code you will need to load the driver modules and
make device nodes per the instructions on the pmi_html and cmi_html main pages.