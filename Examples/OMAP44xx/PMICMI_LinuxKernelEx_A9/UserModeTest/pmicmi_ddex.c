
//#include "cmi.h"
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h> 
#include <stdbool.h>
#include <unistd.h>
#include "pmi.h"
#include "cmi.h"

main ( int argc, char * argv[]) 
{    
    int ret;
    ///////////////////////////////////////////////////////////////////////////    
    //PMI Test    
    ////////////////////////////////////////////////////////////////////////////    
    {
        int pmi_fd;
        int pmi_state;
        
        uint pmi_cfg = PMI_LVD_EN | PMI_MVD_EN | PMI_LPD_EN | PMI_MPD_EN;
        
        pmi_fd = open("/dev/pmi", O_WRONLY);
        if ( -1 == pmi_fd ) {
            int err = errno;
            printf ("PMI open failed with error %d\n", err);
            exit(-1);
        } 
        else {
            printf ("PMI open succes\n");
        }
        fflush(stdout);
        
        ret = ioctl(pmi_fd, PMI_IOC_GETSTATE, &pmi_state);
        if ( ret ) {
            int err = errno;
            printf ("PMI_IOC_GETSTATE ioctl 1st call failed with error %d\n", err);
            exit(-1);
        }
        printf ("PMI_IOC_GETSTATE ioctl succes\n");
        fflush(stdout);
        
        if (pmi_state) {
            printf ("Detected PMI enabled - switching to disabled state\n");
            
            ret = ioctl(pmi_fd, PMI_IOC_DISABLE, 0);
            if ( ret ) {
                int err = errno;
                printf ("PMI_IOC_DISABLE ioctl failed with error %d\n", err);
                exit(-1);
            }       
        
            ret = ioctl(pmi_fd, PMI_IOC_GETSTATE, &pmi_state);
            if ( ret ) {
                int err = errno;
                printf ("PMI_IOC_GETSTATE ioctl 2nd call failed with error %d\n", err);
                exit(-1);
            }
            
            if (pmi_state) {
                printf ("Error - pmi_state enabled\n ");
                exit(-1);
            }
            printf ("Success - pmi_state disabled\n");
        }
            
        ret = ioctl(pmi_fd, PMI_IOC_CFG, pmi_cfg);
        if ( ret ) {
            int err = errno;
            printf ("PPMI_IOC_CFG ioctl failed with error %d\n", err);
            exit(-1);
        }    
        printf ("PMI_IOC_CFG ioctl succes\n");
        fflush(stdout);
        
        ret = ioctl(pmi_fd, PMI_IOC_ENABLE, 0);
        if ( ret ) {
            int err = errno;
            printf ("PMI_IOC_ENABLE ioctl failed with error %d\n", err);
            exit(-1);
        }    
        printf ("PMI_IOC_ENABLE ioctl succes\n");    
        fflush(stdout);    
    
        //Test how we terminat
        // main has no arguments then exit
        // 1 argument that's 0 spin forever
        // 1 argument that's non-zero is a sleep (in seconds) to delay before exiting
        {
            if ( 1 < argc ) {// no arguments skip
                int delay = atoi(argv[1]);
                if ( 0 == delay ){
                    printf ("Endless loop\n");
                    while (1);
                }
                else {
                    printf("Sleep for %d seconds\n", delay);
                    sleep( delay );
                }
            }
            printf("Exit PMI Test\n");
        }
    }
    ///////////////////////////////////////////////////////////////////////////    
    //CMI Test - Event Mode   
    ////////////////////////////////////////////////////////////////////////////
    {
        int cmi_fd[CMI_MODULE_CNT];
        int cmi_state;
        struct cmi_ioc_cfg_t cmi_cfg;
        int id;
        
        for (id = CM1_ID; id < CMI_MODULE_CNT; id++) {

            if ( CM1_ID == id ) {
                cmi_fd[id] = open("/dev/cmi1", O_WRONLY);
                cmi_cfg.cfg =   CM_EVT_STATE_EN 
                              | CM_EVT_FRQDIV_EN              
                              | CM_EVT_SRCSEL_EN              
                              | CM1_EVT_CORE_DPLL_EN         
                              | CM1_EVT_MPU_DPLL_EN          
                              | CM1_EVT_IVA_DPLL_EN          
                              | CM1_EVT_ABE_DPLL_EN          
                              | CM1_EVT_DDRPHY_DPLL_EN;
            }
            else {
                cmi_fd[id] = open("/dev/cmi2", O_WRONLY);
                cmi_cfg.cfg =   CM_EVT_STATE_EN 
                              | CM_EVT_FRQDIV_EN 
                              | CM_EVT_SRCSEL_EN
                              | CM2_EVT_PER_DPLL_EN 
                              | CM2_EVT_USB_DPLL_EN 
                              | CM2_EVT_UNIPRO_DPLL_EN;
            }
            cmi_cfg.mode = CMI_EVENT;
            
            
            if ( -1 == cmi_fd[id] ) {
                int err = errno;
                printf ("CMI%d open failed with error %d\n", id+1, err);
                exit(-1);
            } 
            else {
                printf ("CMI%d open succes\n", id+1 );
            }
            fflush(stdout);
            
            ret = ioctl(cmi_fd[id], CMI_IOC_GETSTATE, &cmi_state);
            if ( ret ) {
                int err = errno;
                printf ("CMI%d CMI_IOC_GETSTATE ioctl 1st call failed with error %d\n", id+1, err);
                exit(-1);
            }
            printf ("CMI%d CMI_IOC_GETSTATE ioctl succes\n", id+1);
            fflush(stdout);
            
            if (cmi_state) {
                printf ("Detected CMI&d enabled - switching to disabled state\n", id+1);
                
                ret = ioctl(cmi_fd[id], CMI_IOC_DISABLE, 0);
                if ( ret ) {
                    int err = errno;
                    printf ("CMI%d CMI_IOC_DISABLE ioctl failed with error %d\n", id+1, err);
                    exit(-1);
                }       
            
                ret = ioctl(cmi_fd[id], CMI_IOC_GETSTATE, &cmi_state);
                if ( ret ) {
                    int err = errno;
                    printf ("CMI%d CMI_IOC_GETSTATE ioctl 2nd call failed with error %d\n", id+1, err);
                    exit(-1);
                }
                
                if (cmi_state) {
                    printf ("CMI%d Error - cmi_state enabled\n ", id+1);
                    exit(-1);
                }
                printf ("CMI%d Success - cmi_state disabled\n", id+1);
            }
                
            ret = ioctl(cmi_fd[id], CMI_IOC_CFG, &cmi_cfg);
            if ( ret ) {
                int err = errno;
                printf ("CMI%d CMI_IOC_CFG ioctl failed with error %d\n", id+1, err);
                exit(-1);
            }    
            printf ("CMI%d CMI_IOC_CFG ioctl succes\n", id+1 );
            fflush(stdout);
            
            ret = ioctl(cmi_fd[id], CMI_IOC_ENABLE, 0);
            if ( ret ) {
                int err = errno;
                printf ("CMI%d CMI_IOC_ENABLE ioctl failed with error %d\n", id+1, err);
                exit(-1);
            }    
            printf ("CMI%d CMI_IOC_ENABLE ioctl succes\n", id+1);    
            fflush(stdout);    
        
            //Test how we terminat
            // main has no arguments then exit
            // 1 argument that's 0 spin forever
            // 1 argument that's non-zero is a sleep (in seconds) to delay before exiting
            {
                if ( 1 < argc ) {// no arguments skip
                    int delay = atoi(argv[1]);
                    if ( 0 == delay ){
                        printf ("Endless loop\n");
                        while (1);
                    }
                    else {
                        printf("Sleep for %d seconds\n", delay);
                        sleep( delay );
                    }
                }
                printf("Exit CMI Event Test\n");
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////    
        //CMI Test - Activity mode    
        //////////////////////////////////////////////////////////////////////////// 
        
            usleep(10); // Sleep for 10 milliseconds
            cmi_cfg.mode = CMI_ACTIVITY;  
            cmi_cfg.cfg = CM_ACT_TARGET_EN | CM_ACT_INITIATOR_EN;
            
            for (id = CM1_ID; id < CMI_MODULE_CNT; id++) {
                if (    ( ret = ioctl(cmi_fd[id], CMI_IOC_DISABLE, 0) )
                     || ( ret = ioctl(cmi_fd[id], CMI_IOC_CFG, &cmi_cfg) )
                     || ( ret = ioctl(cmi_fd[id], CMI_IOC_ENABLE, 0) ) ) {
                        printf("Switch to Activity mode failed\n");
                 }
                 printf("Switch to Activity mode succes\n");
            }           

    }
    
}

