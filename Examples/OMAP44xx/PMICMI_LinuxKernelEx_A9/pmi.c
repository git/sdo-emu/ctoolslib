/*
 * pmi.c
 *
 * Power Management Instrumentation Linux Kernel Module Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/ioport.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/uaccess.h> 
#include "PMILib.h"
#include "pmi.h"

#ifndef __linux__
#define KERN_ALERT
#define printk printf
#define kmalloc(t,f) malloc(t)
#define kfree free
#endif


//////////////////////////////////////////////////////////////////////////////
// Macro declarations                                                       //
//////////////////////////////////////////////////////////////////////////////
MODULE_LICENSE("Dual BSD/GPL");

//////////////////////////////////////////////////////////////////////////////
// Static module parameters                                                 //
//////////////////////////////////////////////////////////////////////////////

static uint clk_div = 16;   //Set default PMI Clock Divider
static uint smp_win = 256;  //Set default sample window size
static bool lvd = 1;        //Logic voltage domain opp enable
static bool mvd = 1;        //Memory voltage domain opp enable
static bool lpd = 1;        //Logic power doamin state change enable
static bool mpd = 1;        //Memory power domain state change enable

//////////////////////////////////////////////////////////////////////////////
// Private definitions                                                      //
//////////////////////////////////////////////////////////////////////////////
//pmi device structure
struct pmi_dev_t {    //The following elements are pmi specific
                      bool state;                     // Enabled/Disabled 
                      pmi_id_t id;                    // physical pmi id 
                      uint32_t * mod_mapadr;          // io mapped module address
                      uint32_t * clk_mapadr;          // io mapped clk address 
                      PMI_Handle_Pntr pmi_lib_hndl;   // pmi library handle 
                      bool lvd;                       // lvd enable state 
                      bool mvd;                       // mvd enable state 
                      bool lpd;                       // lpd enable state 
                      bool mpd;                       // mpd enable state 
                      //The following elements are driver specific
                      struct cdev cdev;               // char device structure
};

//Clean-up levels
typedef enum {  PMI_CLEANUP_0_0, //No cleanup required
                PMI_CLEANUP_1_0, //Free pmi_dev struct allocated memory
                PMI_CLEANUP_1_1, //Release pmi device allocation + All previous
                PMI_CLEANUP_1_2, //Release pmi device registration + All previous
                PMI_CLEANUP_2_0, //Release pmi module physical memory region + All previous
                PMI_CLEANUP_3_0, //Release pmi clock control physical memory region + All previous
                PMI_CLEANUP_4_0  //Disable and close the pmi module + All previous
}pmi_cleanup_t;

//////////////////////////////////////////////////////////////////////////////
// Static (private)variables                                                //
//////////////////////////////////////////////////////////////////////////////

static atomic_t pmi_lock;                       //device lock - avoid a race condition by not including
                                                // in pmi_dev_hndl
static dev_t dev_num;
static pmi_cleanup_t pmi_clean_level = PMI_CLEANUP_0_0;   //clean-up level
static struct pmi_dev_t * pmi_dev_hndl = NULL;         //Note: keeping this around as a static is non-standard
                                                // (in terms of device driver implementation) 
                                                // but in this case since there will ever only be a single
                                                // device and a kernel level device API must be exported
                                                // it's an acceptable compromise.
static uint32_t pmi_mod_phyadr = 0x4A307F00;
static uint32_t pmi_clk_phyadr = 0x4A306040;

//////////////////////////////////////////////////////////////////////////////
// Public user mode functions                                               //
//////////////////////////////////////////////////////////////////////////////
int pmi_open (struct inode * inode, struct file * filp)
{
    struct pmi_dev_t *dev;
    pmi_kdev_t * pmi_kdev;
    pmi_err_t err;
    
    nonseekable_open( inode, filp);

    dev = container_of(inode->i_cdev, struct pmi_dev_t, cdev);
    
    pmi_kdev = pmi_kopen(dev->id, &err);
    if ( pmi_kdev ) {
        filp->private_data = pmi_kdev;
    }
    return err;
}
int pmi_release (struct inode * inode, struct file * filp)
{
    pmi_err_t err;
    pmi_kdev_t * pmi_kdev = filp->private_data;
    
    err = pmi_krelease(pmi_kdev);
    filp->private_data = NULL; 
    return err;
}
int pmi_ioctl (struct inode * inode, struct file * filp, unsigned int cmd, unsigned long arg)
{
    pmi_err_t err =0;
    pmi_kdev_t * pmi_kdev = filp->private_data;
    
    bool pmi_state;
    uint32_t pmi_cfg;
    
    switch (cmd) {
        case PMI_IOC_ENABLE:
            err = pmi_kenable(pmi_kdev);
            break;
        case PMI_IOC_DISABLE:
            err = pmi_kdisable(pmi_kdev);
            break;
        case PMI_IOC_CFG:
            err = pmi_kcfg(pmi_kdev, arg);
            break;
        case PMI_IOC_GETSTATE:       
            err = pmi_kgetstate(pmi_kdev, &pmi_state, &pmi_cfg);
            if (err) break;
            err = put_user(pmi_state, (uint32_t __user *)arg);
            break;
        case PMI_IOC_GETCFG:
            err = pmi_kgetstate(pmi_kdev, &pmi_state, &pmi_cfg);
            if (err) break;
            err = put_user(pmi_cfg, (uint32_t __user *)arg);
            break;
    }
    
    return err;
}

static struct file_operations pmi_fops = {     //Any structure element not explicitly assign is initialized to NULL by gcc
                .owner = THIS_MODULE,
                .llseek = no_llseek,           //from linux/fs.h
                .ioctl = pmi_ioctl,
                .open = pmi_open,
                .release = pmi_release
};

//////////////////////////////////////////////////////////////////////////////
// Static (private) functions                                               //
//////////////////////////////////////////////////////////////////////////////
static void pmi_callback(const char * funcName, ePMI_Error errCode)
{
    const char *  errStr = PMI_GetErrorMsg(errCode);
    printk( KERN_ALERT "%s:: PMI Library Error function %s: %s\n", __FUNCTION__, funcName, errStr);
}

static void __exit pmi_cleanup(pmi_cleanup_t level)
{
    //Cases are in reverse order so we fall through on purpose to lower cases
    switch (level) {
           
        case PMI_CLEANUP_4_0:
        {
            ePCMI_Error retErr;
            
            if (pmi_dev_hndl->state) {
                int32_t retain = (int32_t)false;
                retErr = PMI_ModuleActivityDisable(pmi_dev_hndl->pmi_lib_hndl, retain);
                if ( ePCMI_Success != retErr ) {
                        printk( KERN_ALERT "%s:: Error while disabling PMI event activity\n", __FUNCTION__);
                }
                else {
                    printk( KERN_ALERT "%s:: PMI Activity Monitoring disabled\n", __FUNCTION__);
                }  
            }
                    
            retErr = PMI_CloseModule(pmi_dev_hndl->pmi_lib_hndl);
            if ( ePCMI_Success != retErr ) {
                    printk( KERN_ALERT "%s:: Error while closing PMI Library\n", __FUNCTION__);
            }
            else {
                printk( KERN_ALERT "%s:: PMI Library closed\n", __FUNCTION__);
            } 
        }
         case PMI_CLEANUP_3_0:   
            release_mem_region( pmi_clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE);
            iounmap(pmi_dev_hndl->clk_mapadr);
        case PMI_CLEANUP_2_0:
            release_mem_region( pmi_mod_phyadr, PCMI_RegBankSize);
            iounmap(pmi_dev_hndl->mod_mapadr);
        case PMI_CLEANUP_1_2:
            cdev_del(&pmi_dev_hndl->cdev);
        case PMI_CLEANUP_1_1:   
            unregister_chrdev_region(dev_num, PMI_MODULE_CNT);
        case PMI_CLEANUP_1_0:
            kfree(pmi_dev_hndl);
            pmi_dev_hndl = NULL;
        case PMI_CLEANUP_0_0:
        default:
            break;  
    }
}

static int __init pmi_init(void)
{
    //////////////////////////////////////////////////////////////////////////////
    // Declare variables                                                        //
    //////////////////////////////////////////////////////////////////////////////
    ePCMI_Error retErr;
    PMI_Handle_Pntr pmi_lib_hndl = 0;
    
    PMI_CfgParams pmi_config  = {   0x4A307F00,     // PMI Base address
                                    0x4A306040,     // PMI Clock control base address
                                    pmi_callback,   // PMI call back function
#ifdef _STMLogging
                                    NULL,           // pSTMHandle

                                    0,              // STM Channel for logging
#endif
                                    clk_div,        // Sample wundow divide by factor
                                    smp_win         // Sample window size
                                    };
                                                                        
    //////////////////////////////////////////////////////////////////////////////
    // Lock and create the pmi dev struct                                                //
    //////////////////////////////////////////////////////////////////////////////
    
    //Initialize to lock state
    atomic_set(&pmi_lock, 0);                
    
    //Malloc device handle space 
    pmi_dev_hndl = (struct pmi_dev_t *)kmalloc(sizeof(struct pmi_dev_t), GFP_KERNEL);
    if ( NULL == pmi_dev_hndl ) {
        pmi_clean_level = PMI_CLEANUP_0_0;
        printk(KERN_ALERT "%s:: Kernel memory allocation error - exiting\n", __FUNCTION__);
        return -ENOMEM;
    }
    
    pmi_dev_hndl->pmi_lib_hndl = NULL;
    pmi_dev_hndl->state = false;
    pmi_dev_hndl->id = PM_ID; 
    
    dbg_print( "%s:: PMI Driver entry\n", __FUNCTION__);

    //////////////////////////////////////////////////////////////////////////////
    // Initialize the device driver                                             //
    //////////////////////////////////////////////////////////////////////////////
    {
        int err, pmi_major, pmi_minor;

        
        // Allocate a device                             
        err = alloc_chrdev_region(&dev_num, 0, PMI_MODULE_CNT, "pmi");
        pmi_major = MAJOR(dev_num);
        if ( err ) {
            pmi_clean_level = PMI_CLEANUP_1_0;
            printk(KERN_ALERT "%s:: Error %d on device allocation for major %d - exiting\n", __FUNCTION__, err, pmi_major);
            return err;
        }
        
        // Initalize the device
        cdev_init(&pmi_dev_hndl->cdev, &pmi_fops);
        pmi_dev_hndl->cdev.owner = THIS_MODULE;
        pmi_dev_hndl->cdev.ops = &pmi_fops;
        
        // Add the device to the system
        // Note - Normally we would use MKDEV(major,minor) to generate the dev_num but since there is only a single physical 
        //        pmi module we can use the first device number provided by alloc_chrdev_region
        // Note - On return from this function the device is live (pmi_open uses pmi_lock to gaurd aginst premature access) 
        err = cdev_add( &pmi_dev_hndl->cdev, dev_num, 1 );  // Add 1 device to the kernel
        if ( err ) {
            pmi_clean_level = PMI_CLEANUP_1_1;
            printk(KERN_ALERT "%s:: Error %d on adding device to the system  - exiting\n", __FUNCTION__, err);
            return err;
        }        
        
        pmi_minor = MINOR(dev_num);
        printk(KERN_ALERT "%s:: Added pmi device to the system, major %d minor %d\n", __FUNCTION__, pmi_major, pmi_minor);
        
    }
    //////////////////////////////////////////////////////////////////////////////
    // Map the physical addresses                                               //
    //////////////////////////////////////////////////////////////////////////////
    {    
        struct resource * mod_alloc;
        struct resource * clk_alloc;
    
        mod_alloc = request_mem_region( pmi_mod_phyadr, PCMI_RegBankSize, "PMI Mod Base");
        if ( NULL == mod_alloc ) {
            pmi_clean_level = PMI_CLEANUP_1_2;
            printk(KERN_ALERT "%s:: PMI base address allocation request failed - exiting\n", __FUNCTION__);
            return -EADDRNOTAVAIL;   
        }
        
        // Replace the base address with kernel mapped addresses 
        pmi_dev_hndl->mod_mapadr = ioremap_nocache( pmi_mod_phyadr, PCMI_RegBankSize);
        pmi_config.PMI_Module_BaseAddr = (uint32_t)pmi_dev_hndl->mod_mapadr;
        
        clk_alloc = request_mem_region( pmi_clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE, "PMI ClkCtl Base");
        if ( NULL == clk_alloc ) {
            pmi_clean_level = PMI_CLEANUP_2_0;
            printk(KERN_ALERT "%s:: PMI Clock Control address allocation request failed - exiting\n", __FUNCTION__);
            return -EADDRNOTAVAIL;             
        }
        
        // Replace the clock control address with kernel mapped address
        pmi_dev_hndl->clk_mapadr = ioremap_nocache( pmi_clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE );
        pmi_config.PMI_ClockCtl_BaseAddr = (uint32_t)pmi_dev_hndl->clk_mapadr;
    
    }    
    //////////////////////////////////////////////////////////////////////////////
    // Open the PMI Library                                                     //
    //////////////////////////////////////////////////////////////////////////////
    
    retErr = PMI_OpenModule(&pmi_lib_hndl, ePMI_PM, &pmi_config );
    if ( ePCMI_Success != retErr ) {
        pmi_clean_level = PMI_CLEANUP_3_0;
        printk(KERN_ALERT "%s:: PMILib open error - exiting\n", __FUNCTION__);
        return -EPMI_LIBERR;
    }
        
    pmi_dev_hndl->pmi_lib_hndl = pmi_lib_hndl;

    //////////////////////////////////////////////////////////////////////////////
    // Report the version of the PMI Library                                    //
    //////////////////////////////////////////////////////////////////////////////
    {
        uint32_t lib_maj_ver, lib_min_ver, lib_func_id, mod_func_id;
        
        retErr = PMI_GetVersion(pmi_lib_hndl, &lib_maj_ver, &lib_min_ver, &lib_func_id, &mod_func_id);
        printk( KERN_ALERT "%s:: PMILib version %d.%d\n", __FUNCTION__, lib_maj_ver, lib_min_ver);      
    }
    
    //////////////////////////////////////////////////////////////////////////////
    // Update the configuration parameters                                      //
    //////////////////////////////////////////////////////////////////////////////   
    {
        ePMI_EventEnables evt_enables = 0;
       
        if ( lvd ) evt_enables |= ePMI_ENABLE_LOGIC_VOLTAGE_DOMAIN_OPP_CHANGE;
        if ( mvd ) evt_enables |= ePMI_ENABLE_MEMORY_VOLTAGE_DOMAIN_OPP_CHANGE;
        if ( lpd ) evt_enables |= ePMI_ENABLE_LOGIC_POWER_DOMAIN_STATE_CHANGE;
        if ( mpd ) evt_enables |= ePMI_ENABLE_MEMORY_POWER_DOMAIN_STATE_CHANGE;
        
        retErr = PMI_ConfigModule(pmi_lib_hndl, ePCMI_TRIGGER_NONE, evt_enables);
        if ( ePCMI_Success != retErr ) {
            pmi_clean_level = PMI_CLEANUP_3_0;
            printk( KERN_ALERT "%s:: Error while setting PMI event enables\n", __FUNCTION__);
            return -EPMI_LIBERR;
        }
        
        printk( KERN_ALERT "%s:: Initial parameters \n", __FUNCTION__);
        printk( KERN_ALERT "%s:: Set sample window size to %d\n", __FUNCTION__, clk_div * smp_win );

        printk( KERN_ALERT "%s:: Logic Voltage Domain OPP Change %d\n", __FUNCTION__, lvd );
        printk( KERN_ALERT "%s:: Memory Voltage Domain OPP Change %d\n", __FUNCTION__, mvd );
        printk( KERN_ALERT "%s:: Logic Power Domain State Change %d\n", __FUNCTION__, lpd );
        printk( KERN_ALERT "%s:: Memory Power Domain State Change %d\n", __FUNCTION__, mpd );
        
        pmi_dev_hndl->lvd = lvd;
        pmi_dev_hndl->mvd = mvd;
        pmi_dev_hndl->lpd = lpd;
        pmi_dev_hndl->mpd = mpd;
        
    }
    //////////////////////////////////////////////////////////////////////////////
    // Enable PMI Messages                                                      //
    //////////////////////////////////////////////////////////////////////////////   
    if ( lvd || mvd || lpd || mpd ) {
        retErr = PMI_ModuleActivityEnable(pmi_lib_hndl);
        if ( ePCMI_Success != retErr ) {
                pmi_clean_level = PMI_CLEANUP_3_0;
                printk( KERN_ALERT "%s:: Error while enabling PMI event activity\n", __FUNCTION__);
                return -EPMI_LIBERR;
        }
        else {
            printk( KERN_ALERT "%s:: PMI Activity Monitoring enabled\n", __FUNCTION__);
        }
        
        pmi_dev_hndl->state = true;
    }
    else { 
        printk( KERN_ALERT "%s:: PMI Activity Monitoring not enabled\n", __FUNCTION__);
    }
    
    // Clean-up all
    pmi_clean_level = PMI_CLEANUP_4_0;
    
    //Release lock
    atomic_inc(&pmi_lock);
    return 0;
}
module_init(pmi_init);

static void __exit pmi_exit(void)
{
    pmi_cleanup(pmi_clean_level);
    printk(KERN_ALERT "pmi_exit::PMI Driver exit - Hasta la vista\n");
}
module_exit(pmi_exit);

//////////////////////////////////////////////////////////////////////////////
// Functions required by the PMI Library                                    //
//////////////////////////////////////////////////////////////////////////////
void * STM_memAlloc(size_t sizeInBytes)
{
    return((void *)kmalloc (sizeInBytes, GFP_KERNEL));
}

void STM_memFree(void *p)
{
    kfree(p);
}

// Since the pmi_init() already mapped the physical locations required
// This function simply returns the address.
void * STM_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes) 
{    
    return (void *)phyAddr;
}

void STM_memUnMap(void * vAddr, unsigned int mapSizeInBytes)
{
    return;
}

//////////////////////////////////////////////////////////////////////////////
// Functions exported for kernel module support                             //
//////////////////////////////////////////////////////////////////////////////

pmi_kdev_t * pmi_kopen(pmi_id_t id, pmi_err_t * err)
{
    pmi_kdev_t * pmi_kdev;
    *err = 0;
    
    // Test that we got far enough through pmi_init that the lock is valid and input id is valid
    if (   (NULL == pmi_dev_hndl)
        || (NULL == pmi_dev_hndl->pmi_lib_hndl)  
        || (pmi_dev_hndl->id != id ) ) {
        *err = -EPMI_HNDL;
        return NULL;
    }
    
    if ( ! atomic_dec_and_test(&pmi_lock) ) {
        atomic_inc(&pmi_lock);        
        *err = -EBUSY;
        return NULL;
    }
    
    //Create a pmi_kdev_t so it's contents (a pmi_dev pointer) can be 
    // destroyed on a release so it can't be abused
    pmi_kdev = (pmi_kdev_t *)kmalloc(sizeof(pmi_kdev_t), GFP_KERNEL);
    if ( NULL == pmi_kdev ) {
        *err = -ENOMEM;
        atomic_inc(&pmi_lock);
    }
    else {
        pmi_kdev->pmi_dev = (struct pmi_dev_it *)pmi_dev_hndl;
    }  
      
    return pmi_kdev;    
}
EXPORT_SYMBOL(pmi_kopen);

pmi_err_t pmi_krelease(pmi_kdev_t * pmi_hndl)
{   
    pmi_hndl->pmi_dev = NULL; 
    kfree(pmi_hndl);           
    atomic_inc(&pmi_lock);
    return 0;
    
}
EXPORT_SYMBOL(pmi_krelease);

pmi_err_t pmi_kgetstate(pmi_kdev_t * pmi_hndl, bool * pmi_state, uint32_t * pmi_cfg)
{   
    struct pmi_dev_t * pmi_dev;
    
    if (  (NULL == pmi_hndl)
       || (NULL == pmi_hndl->pmi_dev) )
       return -EPMI_HNDL;    
     
    pmi_dev = (struct pmi_dev_t *)(pmi_hndl->pmi_dev);
      
    * pmi_state = pmi_dev->state;
    * pmi_cfg = (pmi_dev->lvd) ? PMI_LVD_EN : 0;
    * pmi_cfg |= (pmi_dev->mvd) ? PMI_MVD_EN : 0;
    * pmi_cfg |= (pmi_dev->lpd) ? PMI_LPD_EN : 0;
    * pmi_cfg |= (pmi_dev->mpd) ? PMI_MPD_EN : 0;
         
    return 0;
}
EXPORT_SYMBOL(pmi_kgetstate);

pmi_err_t pmi_kenable(pmi_kdev_t * pmi_hndl)
{
    ePCMI_Error retErr;
    struct pmi_dev_t * pmi_dev;
    
    if (  (NULL == pmi_hndl)
       || (NULL == pmi_hndl->pmi_dev) )
       return -EPMI_HNDL;
    
    pmi_dev = (struct pmi_dev_t *)(pmi_hndl->pmi_dev);
    
    //if disabled then enable
    if (!pmi_dev->state) {
        retErr = PMI_ModuleActivityEnable(pmi_dev->pmi_lib_hndl);
        if ( ePCMI_Success != retErr ) {
                printk( KERN_ALERT "%s:: Error while enabling PMI event activity\n", __FUNCTION__);
                return -EPMI_LIBERR;
        }
        else {
            dbg_print( "%s:: PMI Activity Monitoring enabled\n", __FUNCTION__);
        }
        
        pmi_dev->state = true;   
    }
    
    return 0;    
}
EXPORT_SYMBOL(pmi_kenable);

pmi_err_t pmi_kdisable(pmi_kdev_t * pmi_hndl)
{
    ePCMI_Error retErr;
    struct pmi_dev_t * pmi_dev;
    
    if (  (NULL == pmi_hndl)
       || (NULL == pmi_hndl->pmi_dev) )
       return -EPMI_HNDL;
    
    pmi_dev = (struct pmi_dev_t *)(pmi_hndl->pmi_dev);
    
    //if enabled then disable
    if (pmi_dev->state) {
        int32_t retain = (int32_t)true;
        retErr = PMI_ModuleActivityDisable(pmi_dev->pmi_lib_hndl, retain);
        if ( ePCMI_Success != retErr ) {
                printk( KERN_ALERT "%s:: Error while disabling PMI event activity\n", __FUNCTION__);
                return -EPMI_LIBERR;
        }
        else {
            dbg_print( "%s:: PMI Activity Monitoring disabled\n", __FUNCTION__);
        }
        
        pmi_dev->state = false;   
    }
    
    return 0;        
}
EXPORT_SYMBOL(pmi_kdisable);

pmi_err_t pmi_kcfg(pmi_kdev_t * pmi_hndl, uint32_t pmi_cfg)
{
    ePCMI_Error retErr;
    struct pmi_dev_t * pmi_dev;
    
     if (  (NULL == pmi_hndl)
       || (NULL == pmi_hndl->pmi_dev) )
       return -EPMI_HNDL;
     
    pmi_dev = (struct pmi_dev_t *)(pmi_hndl->pmi_dev); 
      
    if (pmi_dev->state) return -EPMI_STATE;
    
    //if state disable can modify
    {
        ePMI_EventEnables evt_enables = 0;
       
        if ( pmi_cfg & PMI_LVD_EN ) evt_enables |= ePMI_ENABLE_LOGIC_VOLTAGE_DOMAIN_OPP_CHANGE;
        if ( pmi_cfg & PMI_MVD_EN ) evt_enables |= ePMI_ENABLE_MEMORY_VOLTAGE_DOMAIN_OPP_CHANGE;
        if ( pmi_cfg & PMI_LPD_EN ) evt_enables |= ePMI_ENABLE_LOGIC_POWER_DOMAIN_STATE_CHANGE;
        if ( pmi_cfg & PMI_MPD_EN ) evt_enables |= ePMI_ENABLE_MEMORY_POWER_DOMAIN_STATE_CHANGE;
        
        retErr = PMI_ConfigModule(pmi_dev->pmi_lib_hndl, ePCMI_TRIGGER_NONE, evt_enables);
        if ( ePCMI_Success != retErr ) {
            printk( KERN_ALERT "%s:: Error while setting PMI event enables\n", __FUNCTION__);
            return -EPMI_LIBERR;
        }
    }
    
    pmi_dev->lvd = (pmi_cfg & PMI_LVD_EN) ? true : false;
    pmi_dev->mvd = (pmi_cfg & PMI_MVD_EN) ? true : false;
    pmi_dev->lpd = (pmi_cfg & PMI_LPD_EN) ? true : false;
    pmi_dev->mpd = (pmi_cfg & PMI_MPD_EN) ? true : false;
    
    return 0;     
}
EXPORT_SYMBOL(pmi_kcfg);


//////////////////////////////////////////////////////////////////////////////
// Required Kernel Globals                                                  //
//////////////////////////////////////////////////////////////////////////////
module_param(clk_div, uint, S_IRUGO);     // Clock divider (1-16) No protection
module_param(smp_win, uint, S_IRUGO);     // Sample Window size ( 1-256) No protection 
module_param(lvd, bool, S_IRUGO);         //Logic voltage domain opp enable
module_param(mvd, bool, S_IRUGO);         //Memory voltage domain opp enable
module_param(lpd, bool, S_IRUGO);          //Logic power doamin state change enable
module_param(mpd, bool, S_IRUGO);         //Memory power domain state change enable
