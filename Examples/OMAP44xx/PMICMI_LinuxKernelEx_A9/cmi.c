/*
 * cmi.c
 *
 * Clock Management Instrumentation Linux Kernel Module Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/ioport.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/uaccess.h> 
#include "CMILib.h"
#include "cmi.h"

//////////////////////////////////////////////////////////////////////////////
// Macro declarations                                                       //
//////////////////////////////////////////////////////////////////////////////
MODULE_LICENSE("Dual BSD/GPL");

//////////////////////////////////////////////////////////////////////////////
// Static module parameters                                                 //
//////////////////////////////////////////////////////////////////////////////

static uint cm1_clk_div = 16;   //Set CM1 default CMI Clock Divider
static uint cm1_smp_win = 256;  //Set CM1 default sample window size
static uint cm1_mode = 0;       //Set CM1 default mode to event monitoring
static uint cm1_cfg = 0x1f000f; //Set CM1 default cfg for all events enabled
static uint cm2_clk_div = 16;   //Set CM1 default CMI Clock Divider
static uint cm2_smp_win = 256;  //Set CM1 default sample window size
static uint cm2_mode = 0;       //Set CM1 default mode to event monitoring
static uint cm2_cfg = 0x7000f;  //Set CM1 default cfg for all events enabled


//////////////////////////////////////////////////////////////////////////////
// Private definitions                                                      //
//////////////////////////////////////////////////////////////////////////////

struct  cmi_dev_t {   //The following elements are cmi specific
                      bool state;                     /* Enabled/Disabled */
                      cmi_id_t id;                    /* physical cmi id */
                      uint32_t mod_phyadr;            /* physical module address */
                      uint32_t clk_phyadr;            /* physical clock control address */
                      uint32_t * mod_mapadr;          /* io mapped module address */
                      uint32_t * clk_mapadr;          /* io mapped clk address */
                      CMI_Handle_Pntr cmi_lib_hndl;   /* cmi library handle */
                      cmi_mode_t  mode;               /* mode is definded by ::cmi_mode_t */
                      uint32_t cfg;                   /* bit field defined by ::cmi_cfg_t */
                      //The following elements are driver specific
                      struct cdev cdev;               // char device structure
                    };

//Clean-up levels
typedef enum {  CMI_CLEANUP_0_0, //No cleanup required
                CMI_CLEANUP_1_0, //Release cmi device allocation
                CMI_CLEANUP_1_1, //Free cmi_dev struct allocated memory + All previous
                CMI_CLEANUP_1_2, //Release cmi device registration + All previous
                CMI_CLEANUP_2_0, //Release cmi module physical memory region + All previous
                CMI_CLEANUP_3_0, //Release cmi clock control physical memory region + All previous
                CMI_CLEANUP_4_0  //Disable and close the cmi module + All previous
}cmi_cleanup_t;
//////////////////////////////////////////////////////////////////////////////
// Private static variables                                                 //
//////////////////////////////////////////////////////////////////////////////

static atomic_t cmi_lock[CMI_MODULE_CNT];                               //device lock - avoid a race condition by not including
                                                                        // in cmi_dev_hndl
static dev_t dev_num;                                                   // Device number
static cmi_cleanup_t cmi_clean_level[CMI_MODULE_CNT] = {0,0};           //clean-up level
static struct cmi_dev_t * cmi_dev_hndl[CMI_MODULE_CNT] = {NULL, NULL};  //Note: keeping this around as a static is non-standard
                                                                        // (in terms of device driver implementation) 
                                                                        // but in this case since there will ever only be a single
                                                                        // device and a kernel level device API must be exported
                                                                        // it's an acceptable compromise.

//////////////////////////////////////////////////////////////////////////////
// Public user mode functions                                               //
//////////////////////////////////////////////////////////////////////////////
int cmi_open (struct inode * inode, struct file * filp)
{
    struct cmi_dev_t *dev;
    cmi_kdev_t * cmi_kdev;
    cmi_err_t err;
        
    nonseekable_open( inode, filp);

    dev = container_of(inode->i_cdev, struct cmi_dev_t, cdev);
    
    cmi_kdev = cmi_kopen(dev->id, &err);
    if ( cmi_kdev ) {
        filp->private_data = cmi_kdev;
    }
    return err;
}
int cmi_release (struct inode * inode, struct file * filp)
{
    cmi_err_t err;
    cmi_kdev_t * cmi_kdev = filp->private_data;
    
    err = cmi_krelease(cmi_kdev);
    filp->private_data = NULL; 
    return err;
}
int cmi_ioctl (struct inode * inode, struct file * filp, unsigned int cmd, unsigned long arg)
{
    cmi_err_t err =0;
    cmi_kdev_t * cmi_kdev = filp->private_data;
    
    bool cmi_state;
    uint32_t cmi_cfg;
    cmi_mode_t cmi_mode;
    
    //If it's not a CMI IOC enum then return with not valid
    if ( _IOC_TYPE(cmd) != CMI_IOC_MAGIC ) return -ENOTTY;
    
    switch (cmd) {
        case CMI_IOC_ENABLE:
            err = cmi_kenable(cmi_kdev);
            break;
        case CMI_IOC_DISABLE:
            err = cmi_kdisable(cmi_kdev);
            break;
        case CMI_IOC_CFG:
            {
                unsigned long cnt;
                struct cmi_ioc_cfg_t cmi_ioc_cfg;                
                cnt = copy_from_user( (void *)&cmi_ioc_cfg,  (const void __user *)arg, sizeof(struct cmi_ioc_cfg_t));
                if ( 0 != cnt) {
                    printk( KERN_ALERT "%s:: attempted to copy from user %ld bytes but only %ld moved so -EFAULT\n", __FUNCTION__, (unsigned long)sizeof(struct cmi_ioc_cfg_t), cnt);                    
                    return -EFAULT;
                }
                dbg_print( "%s:: mode is %d cfg is 0x%x\n", __FUNCTION__, cmi_ioc_cfg.mode, cmi_ioc_cfg.cfg);
                err = cmi_kcfg(cmi_kdev, cmi_ioc_cfg.mode, cmi_ioc_cfg.cfg);
                break;
            }
        case CMI_IOC_GETSTATE:       
            err = cmi_kgetstate(cmi_kdev, &cmi_state, &cmi_mode, &cmi_cfg);
            if (err) break;
            err = put_user(cmi_state, (bool __user *)arg);
            break;
        case CMI_IOC_GETCFG:
             {
                unsigned long cnt;
                struct cmi_ioc_cfg_t cmi_ioc_cfg;  
                err = cmi_kgetstate(cmi_kdev, &cmi_state, &cmi_ioc_cfg.mode, &cmi_ioc_cfg.cfg); 
                if ( !err ) {             
                    cnt = copy_to_user( (void __user *)arg, (const void *)&cmi_ioc_cfg, sizeof(struct cmi_ioc_cfg_t));
                    if ( 0 != cnt) {
                        err = -EFAULT;
                    }
                }
                break;
            }
    }
    
    return err;
}

static struct file_operations cmi_fops = {     //Any structure element not explicitly assign is initialized to NULL by gcc
                .owner = THIS_MODULE,
                .llseek = no_llseek,           //from linux/fs.h
                .ioctl = cmi_ioctl,
                .open = cmi_open,
                .release = cmi_release
};


//////////////////////////////////////////////////////////////////////////////
// Static (private) functions                                               //
//////////////////////////////////////////////////////////////////////////////
static void cm1_callback(const char * funcName, eCMI_Error errCode)
{
    const char *  errStr = CMI_GetErrorMsg(errCode);
    printk( KERN_ALERT "%s:: CMI Library Error function %s: %s\n", __FUNCTION__, funcName, errStr);
}

static void cm2_callback(const char * funcName, eCMI_Error errCode)
{
    const char *  errStr = CMI_GetErrorMsg(errCode);
    printk( KERN_ALERT "%s:: CMI Library Error function %s: %s\n", __FUNCTION__, funcName, errStr);
}


static void __exit cmi_cleanup(cmi_id_t id)
{
    int level = cmi_clean_level[id];
    
    //Cases arein reverse order so we fall through on purpose to lower cases
    switch (level) {            
        case CMI_CLEANUP_4_0:
        {
            ePCMI_Error retErr;
            
            if (cmi_dev_hndl[id]->state) {
                int32_t retain = (int32_t)false;
                retErr = CMI_ModuleDisable(cmi_dev_hndl[id]->cmi_lib_hndl, retain);
                if ( ePCMI_Success != retErr ) {
                        printk( KERN_ALERT "%s:: Error while disabling CM%d event activity\n", __FUNCTION__, id+1);
                }
                else {
                    printk( KERN_ALERT "%s:: CM%d Activity Monitoring disabled\n", __FUNCTION__, id+1);
                }  
            }
                    
            retErr = CMI_CloseModule(cmi_dev_hndl[id]->cmi_lib_hndl);
            if ( ePCMI_Success != retErr ) {
                    printk( KERN_ALERT "%s:: Error while closing CM%d Library\n", __FUNCTION__, id+1);
            }
            else {
                printk( KERN_ALERT "%s:: CM%d Library closed\n", __FUNCTION__, id+1);
            } 
        }
        case CMI_CLEANUP_3_0:   
            release_mem_region( cmi_dev_hndl[id]->clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE);
            iounmap(cmi_dev_hndl[id]->clk_mapadr);
        case CMI_CLEANUP_2_0:
            release_mem_region( cmi_dev_hndl[id]->mod_phyadr, PCMI_RegBankSize);
            iounmap(cmi_dev_hndl[id]->mod_mapadr);
        case CMI_CLEANUP_1_2:
            cdev_del(&cmi_dev_hndl[id]->cdev);
        case CMI_CLEANUP_1_1:
            kfree(cmi_dev_hndl[id]);
        case CMI_CLEANUP_1_0:              // This is the unregister_chrdev_region case but we 
                                           // only want to do this on cleanup for the last device id
            if ( CMI_MODULE_CNT-1 == id){
                unregister_chrdev_region(dev_num, CMI_MODULE_CNT);
            }                            
        case CMI_CLEANUP_0_0:
        default:
            break;
    }

}

static int __init cmi_init(void)
{
    //////////////////////////////////////////////////////////////////////////////
    // Declare variables                                                        //
    //////////////////////////////////////////////////////////////////////////////
    ePCMI_Error retErr;
    CMI_Handle_Pntr cmi_lib_hndl = 0;
    int id;
    CMI_CfgParams cmi_config[CMI_MODULE_CNT]  = {   //CM1 Configuration parameters
                                                    {  0x4A004F00,      // CMI Base address
                                                       0x4A004040,      // CMI Clock control base address
                                                       cm1_callback,    // CMI call back function
#ifdef _STMLogging
                                                       NULL,            // pSTMHandle

                                                       0,               // STM Channel for logging
#endif
                                                        cm1_clk_div,    // Sample wundow divide by factor
                                                        cm1_smp_win,    // Sample window size
                                                    },
                                                    //CM2 Configuration parameters
                                                    {
                                                        0x4A009F00,     // CMI Base address
                                                        0x4A008040,     // CMI Clock control base address
                                                        cm2_callback,   // CMI call back function
#ifdef _STMLogging
                                                        NULL,           // pSTMHandle

                                                        0,              // STM Channel for logging
#endif
                                                        cm2_clk_div,        // Sample wundow divide by factor
                                                        cm2_smp_win         // Sample window size                                                    
                                                    }
                                                };
                                                                        
    //////////////////////////////////////////////////////////////////////////////
    // Lock and create the cmi dev struct                                       //
    //////////////////////////////////////////////////////////////////////////////
    int err, cmi_major, cmi_minor;

    // Allocate two cmi (0,1) devices                             
    err = alloc_chrdev_region(&dev_num, 0, CMI_MODULE_CNT, "cmi");
    cmi_major = MAJOR(dev_num);
    if ( err ) {
        for ( id = CM1_ID; id < CMI_MODULE_CNT; id++ ) {
            cmi_clean_level[id] = CMI_CLEANUP_0_0;
        }
        printk(KERN_ALERT "%s:: Error %d on device allocation for major %d - exiting\n", __FUNCTION__, err, cmi_major);
        return err;
    }
    
    for ( id = CM1_ID; id < CMI_MODULE_CNT; id++ ) {

        //Initialize to lock state
        atomic_set(&cmi_lock[id], 0);                
        
        //Malloc device handle space 
        cmi_dev_hndl[id] = (struct cmi_dev_t *)kmalloc(sizeof(struct cmi_dev_t), GFP_KERNEL);
        if ( NULL == cmi_dev_hndl[id] ) {
            cmi_clean_level[id] = CMI_CLEANUP_1_0;
            printk(KERN_ALERT "%s:: Kernel memory allocation error - exiting\n", __FUNCTION__);
            return -ENOMEM;
        }
        
        cmi_dev_hndl[id]->cmi_lib_hndl = NULL;
        cmi_dev_hndl[id]->state = false;
        cmi_dev_hndl[id]->id = id;
        cmi_dev_hndl[id]->mod_phyadr = cmi_config[id].CMI_Module_BaseAddr;
        cmi_dev_hndl[id]->clk_phyadr = cmi_config[id].CMI_ClockCtl_BaseAddr;
        
        dbg_print( "%s:: CMI Driver entry\n", __FUNCTION__);
        
        //////////////////////////////////////////////////////////////////////////////
        // Initialize the device driver                                             //
        //////////////////////////////////////////////////////////////////////////////
        {                         
            // Initalize the device
            cdev_init(&cmi_dev_hndl[id]->cdev, &cmi_fops);
            cmi_dev_hndl[id]->cdev.owner = THIS_MODULE;
            cmi_dev_hndl[id]->cdev.ops = &cmi_fops;
            
            // Add the device to the system
            dev_num = MKDEV(cmi_major, id);
            // Note - On return from this function the device is live. cmi_open uses cmi_lock
            // to gaurd aginst premature access. 
            err = cdev_add( &cmi_dev_hndl[id]->cdev, dev_num, 1 );  // Add 1 device to the kernel
            if ( err ) {
                cmi_clean_level[id] = CMI_CLEANUP_1_1;
                printk(KERN_ALERT "%s:: Error %d on adding device to the system  - exiting\n", __FUNCTION__, err);
                return err;
            }        
            
            cmi_minor = MINOR(dev_num);
            printk(KERN_ALERT "%s:: Added cmi device to the system, major %d minor %d\n", __FUNCTION__, cmi_major, cmi_minor);
            
        }               
        //////////////////////////////////////////////////////////////////////////////
        // Map the physical addresses                                               //
        //////////////////////////////////////////////////////////////////////////////
        {    
            struct resource * mod_alloc;
            struct resource * clk_alloc;
            char * name_msg = ( id == CM1_ID ) ? "CM1 Mod Base" : "CM2 Mod Base";
        
            mod_alloc = request_mem_region( cmi_dev_hndl[id]->mod_phyadr, PCMI_RegBankSize, name_msg);
            if ( NULL == mod_alloc ) {
                cmi_clean_level[id] = CMI_CLEANUP_1_2;
                printk(KERN_ALERT "%s:: CM%d base address allocation request failed - exiting\n", __FUNCTION__, id+1);
                return -EADDRNOTAVAIL;   
            }

            // Replace the base address with kernel mapped addresses 
            cmi_dev_hndl[id]->mod_mapadr = ioremap_nocache( cmi_dev_hndl[id]->mod_phyadr, PCMI_RegBankSize);
            cmi_config[id].CMI_Module_BaseAddr = (uint32_t)cmi_dev_hndl[id]->mod_mapadr;
            
            name_msg = ( id == CM1_ID ) ? "CM1 Clk Base" : "CM2 Clk Base";
            
            clk_alloc = request_mem_region( cmi_dev_hndl[id]->clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE, name_msg);
            if ( NULL == clk_alloc ) {
                cmi_clean_level[id] = CMI_CLEANUP_2_0;
                printk(KERN_ALERT "%s:: CM%d Clock Control address allocation request failed - exiting\n", __FUNCTION__, id+1);
                return -EADDRNOTAVAIL;             
            }
            
            // Replace the clock control address with kernel mapped address
            cmi_dev_hndl[id]->clk_mapadr = ioremap_nocache( cmi_dev_hndl[id]->clk_phyadr, PCMI_MODULE_CLKCTRL_SIZE );
            cmi_config[id].CMI_ClockCtl_BaseAddr = (uint32_t)cmi_dev_hndl[id]->clk_mapadr;
        
        }    
        //////////////////////////////////////////////////////////////////////////////
        // Open the CMI Library                                                     //
        //////////////////////////////////////////////////////////////////////////////
        
        retErr = CMI_OpenModule(&cmi_lib_hndl, id+1, &cmi_config[id] );
        if ( ePCMI_Success != retErr ) {
            cmi_clean_level[id] = CMI_CLEANUP_3_0;
            printk(KERN_ALERT "%s:: CMILib open error - exiting\n", __FUNCTION__);
            return -ECMI_LIBERR;
        }
            
        cmi_dev_hndl[id]->cmi_lib_hndl = cmi_lib_hndl;
    
        //////////////////////////////////////////////////////////////////////////////
        // Report the version of the CMI Library                                    //
        //////////////////////////////////////////////////////////////////////////////
        {
            uint32_t lib_maj_ver, lib_min_ver, lib_func_id, mod_func_id;
            
            retErr = CMI_GetVersion(cmi_lib_hndl, &lib_maj_ver, &lib_min_ver, &lib_func_id, &mod_func_id);
            printk( KERN_ALERT "%s:: CMILib version %d.%d\n", __FUNCTION__, lib_maj_ver, lib_min_ver);      
        }
        
        //////////////////////////////////////////////////////////////////////////////
        // Update the configuration parameters                                      //
        //////////////////////////////////////////////////////////////////////////////   
        {
 
            uint mode = ( id == CM1_ID ) ? cm1_mode : cm2_mode;
            uint cfg =  ( id == CM1_ID ) ? cm1_cfg  : cm2_cfg;
            uint clk_div = ( id == CM1_ID ) ? cm1_clk_div : cm2_clk_div;
            uint smp_win = ( id == CM1_ID ) ? cm1_smp_win : cm2_smp_win;
            
            eCMI_EventEnables evt_enables = ( mode == CMI_EVENT ) ? (eCMI_EventEnables)cfg : eCMI_ENABLE_EVENT_NONE; 
            eCMI_ActivityEnables act_enables = ( mode == CMI_ACTIVITY ) ? (eCMI_ActivityEnables)cfg : eCMI_ENABLE_ACTIVITY_NONE;
            char * mode_msg = ( mode == CMI_EVENT ) ? "Event" : "Activity";
            
            retErr = CMI_ConfigModule(cmi_lib_hndl, ePCMI_TRIGGER_NONE, evt_enables, act_enables);
            if ( ePCMI_Success != retErr ) {
                cmi_clean_level[id] = CMI_CLEANUP_3_0;
                printk( KERN_ALERT "%s:: Error while setting CM%d doamin enables\n", __FUNCTION__, id+1);
                return -ECMI_LIBERR;
            }                
            
            cmi_dev_hndl[id]->mode = mode;
            cmi_dev_hndl[id]->cfg = cfg;
            
            printk( KERN_ALERT "%s:: CM%d Initial parameters \n", __FUNCTION__, id+1);
            printk( KERN_ALERT "%s:: CM%d Set sample window size to %d\n", __FUNCTION__, id+1, clk_div * smp_win );
            printk( KERN_ALERT "%s:: CM%d Set mode to %s doamin enables to 0x%x\n", __FUNCTION__, id+1, mode_msg, cfg );
                
       
        }
        //////////////////////////////////////////////////////////////////////////////
        // Enable CMI Messages                                                      //
        //////////////////////////////////////////////////////////////////////////////   

        if ( cmi_dev_hndl[id]->cfg ) {
            retErr = CMI_ModuleEnable(cmi_lib_hndl, (eCMI_ModEnableType)cmi_dev_hndl[id]->mode);
            if ( ePCMI_Success != retErr ) {
                    cmi_clean_level[id] = CMI_CLEANUP_3_0;
                    printk( KERN_ALERT "%s:: Error while enabling CM%d\n", __FUNCTION__, id+1);
                    return -ECMI_LIBERR;
            }
            else {
                printk( KERN_ALERT "%s:: CM%d Monitoring enabled\n", __FUNCTION__, id+1);
            }
            
            cmi_dev_hndl[id]->state = true;
        }
        else { 
            printk( KERN_ALERT "%s:: CM%d Monitoring not enabled\n", __FUNCTION__, id+1);
        }
        
        // Clean-up all
        cmi_clean_level[id] = CMI_CLEANUP_4_0;
        
        //Release lock
        atomic_inc(&cmi_lock[id]);
    } // End of for loop
    
    return 0;
}
module_init(cmi_init);

static void __exit cmi_exit(void)
{
    int id;
    
    for ( id = CM1_ID; id < CMI_MODULE_CNT; id++ ) {
        cmi_cleanup( id );
    }
    
    printk(KERN_ALERT "%s:: CMI Driver exit - Hasta la vista\n",__FUNCTION__);
}
module_exit(cmi_exit);

//////////////////////////////////////////////////////////////////////////////
// Functions required by the CMI Library                                    //
//////////////////////////////////////////////////////////////////////////////
void * STM_memAlloc(size_t sizeInBytes)
{
    return((void *)kmalloc (sizeInBytes, GFP_KERNEL));
}

void STM_memFree(void *p)
{
    kfree(p);
}

// Since the cmi_init() already mapped the physical locations required
// This function simply returns the address.
void * STM_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes) 
{    
    return (void *)phyAddr;
}

void STM_memUnMap(void * vAddr, unsigned int mapSizeInBytes)
{
    return;
}

//////////////////////////////////////////////////////////////////////////////
// Functions exported for kernel module support                             //
//////////////////////////////////////////////////////////////////////////////

cmi_kdev_t * cmi_kopen(cmi_id_t id, cmi_err_t * err)
{
    cmi_kdev_t * cmi_kdev;
    *err = 0;
    
    // Test that we got far enough through cmi_init that the lock is valid and input id is valid
    if (   (NULL == cmi_dev_hndl[id])
        || (NULL == cmi_dev_hndl[id]->cmi_lib_hndl)  
        || (cmi_dev_hndl[id]->id != id ) ) {
        *err = -ECMI_HNDL;
        return NULL;
    }
    
    if ( ! atomic_dec_and_test(&cmi_lock[id]) ) {
        atomic_inc(&cmi_lock[id]);        
        *err = -EBUSY;
        return NULL;
    }
    
    //Create a cmi_kdev_t so it's contents (a cmi_dev pointer) can be 
    // destroyed on a release so it can't be abused
    cmi_kdev = (cmi_kdev_t *)kmalloc(sizeof(cmi_kdev_t), GFP_KERNEL);
    if ( NULL == cmi_kdev ) {
        *err = -ENOMEM;
        atomic_inc(&cmi_lock[id]);
    }
    else {
        cmi_kdev->cmi_dev = (struct cmi_dev_it *)cmi_dev_hndl[id];
    }  
        
    return cmi_kdev;    
}
EXPORT_SYMBOL(cmi_kopen);

cmi_err_t cmi_krelease(cmi_kdev_t * cmi_hndl)
{
    struct cmi_dev_t * cmi_dev;  
    
    if (  (NULL == cmi_hndl)
       || (NULL == cmi_hndl->cmi_dev) )
       return -ECMI_HNDL;    
    
    cmi_dev = (struct cmi_dev_t *)cmi_hndl->cmi_dev;
 
    cmi_hndl->cmi_dev = NULL; 
    kfree(cmi_hndl);           
    atomic_inc(&cmi_lock[cmi_dev->id]);
    return 0;
    
}
EXPORT_SYMBOL(cmi_krelease);

cmi_err_t cmi_kgetstate(cmi_kdev_t * cmi_hndl, bool * cmi_state, cmi_mode_t  * cmi_mode, uint32_t * cmi_cfg)
{   
    struct cmi_dev_t * cmi_dev;  
    
    if (  (NULL == cmi_hndl)
       || (NULL == cmi_hndl->cmi_dev) )
       return -ECMI_HNDL;    
    
    cmi_dev = (struct cmi_dev_t *)cmi_hndl->cmi_dev;
      
    * cmi_state = cmi_dev->state;
    * cmi_mode = cmi_dev->mode;
    * cmi_cfg = cmi_dev->cfg;
         
    return 0;
}
EXPORT_SYMBOL(cmi_kgetstate);

cmi_err_t cmi_kenable(cmi_kdev_t * cmi_hndl)
{
    ePCMI_Error retErr;
    struct cmi_dev_t * cmi_dev;
    
    if (  (NULL == cmi_hndl)
       || (NULL == cmi_hndl->cmi_dev) )
       return -ECMI_HNDL;
    
    cmi_dev = (struct cmi_dev_t *)cmi_hndl->cmi_dev;
    
    //if disabled then enable
    if (!cmi_dev->state) {
        retErr = CMI_ModuleEnable(cmi_dev->cmi_lib_hndl, (eCMI_ModEnableType)cmi_dev->mode);
        if ( ePCMI_Success != retErr ) {
                printk( KERN_ALERT "%s:: Error while enabling CM%d event activity\n", __FUNCTION__, cmi_dev->id+1);
                return -ECMI_LIBERR;
        }
        else {
            dbg_print("%s:: CM%d Activity Monitoring enabled\n", __FUNCTION__, cmi_dev->id+1);
        }
        
        cmi_dev->state = true;   
    }
    
    return 0;    
}
EXPORT_SYMBOL(cmi_kenable);

cmi_err_t cmi_kdisable(cmi_kdev_t * cmi_hndl)
{
    ePCMI_Error retErr;
    struct cmi_dev_t * cmi_dev;
    
    if (  (NULL == cmi_hndl)
       || (NULL == cmi_hndl->cmi_dev) )
       return -ECMI_HNDL;
    
    cmi_dev = (struct cmi_dev_t *)cmi_hndl->cmi_dev;
    
    //if enabled then disable
    if (cmi_dev->state) {
        int32_t retain = (int32_t)true;
        retErr = CMI_ModuleDisable(cmi_dev->cmi_lib_hndl, retain);
        if ( ePCMI_Success != retErr ) {
                printk( KERN_ALERT "%s:: Error while disabling CM%d monitoring\n", __FUNCTION__, cmi_dev->id+1);
                return -ECMI_LIBERR;
        }
        else {
            dbg_print( "%s:: CM%d monitoring disabled\n", __FUNCTION__, cmi_dev->id+1);
        }
        
        cmi_dev->state = false;   
    }
    
    return 0;        
}
EXPORT_SYMBOL(cmi_kdisable);

cmi_err_t cmi_kcfg(cmi_kdev_t * cmi_hndl, cmi_mode_t cmi_mode, uint32_t cmi_cfg)
{
    ePCMI_Error retErr;
    struct cmi_dev_t * cmi_dev;    
    eCMI_EventEnables evt_enables;
    eCMI_ActivityEnables act_enables;
    
     if (  (NULL == cmi_hndl)
       || (NULL == cmi_hndl->cmi_dev) )
       return -ECMI_HNDL;
    
    cmi_dev = (struct cmi_dev_t *)cmi_hndl->cmi_dev;
      
    if (cmi_dev->state) return -ECMI_STATE;

    evt_enables =  ( cmi_mode == CMI_EVENT ) ? cmi_cfg : eCMI_ENABLE_EVENT_NONE;
    act_enables =   ( cmi_mode == CMI_ACTIVITY ) ? cmi_cfg : eCMI_ENABLE_ACTIVITY_NONE;

    retErr = CMI_ConfigModule(cmi_dev->cmi_lib_hndl, ePCMI_TRIGGER_NONE, evt_enables, act_enables);
    if ( ePCMI_Success != retErr ) {
        printk( KERN_ALERT "%s:: Error while setting CM%d domain enables\n", __FUNCTION__, cmi_dev->id+1);
        return -ECMI_LIBERR;
    }
    cmi_dev->cfg = cmi_cfg;
    cmi_dev->mode = cmi_mode;
        
    return 0;     
}
EXPORT_SYMBOL(cmi_kcfg);


//////////////////////////////////////////////////////////////////////////////
// Required Kernel Globals                                                  //
//////////////////////////////////////////////////////////////////////////////
module_param(cm1_clk_div, uint, S_IRUGO);     // CM1 Clock divider (1-16) No protection
module_param(cm2_clk_div, uint, S_IRUGO);     // CM2 Clock divider (1-16) No protection
module_param(cm1_smp_win, uint, S_IRUGO);     // CM1 Sample Window size ( 1-256) No protection 
module_param(cm2_smp_win, uint, S_IRUGO);     // CM2 Sample Window size ( 1-256) No protection
module_param(cm1_mode, uint, S_IRUGO);        // CM1 mode (Event or Activity)
module_param(cm2_mode, uint, S_IRUGO);        // CM2 mode (Event or Activity)
module_param(cm1_cfg, uint, S_IRUGO);         // CM1 domain enables
module_param(cm2_cfg, uint, S_IRUGO);         // CM2 domain enables
