/*
 * pmi.c
 *
 * Power Management Instrumentation Linux Kernel Module Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <linux/init.h>
#include <linux/module.h>
#include "PMILib.h"
#include "CMILib.h"
#include "pmi.h"
#include "cmi.h"

//////////////////////////////////////////////////////////////////////////////
// Macro declarations                                                       //
//////////////////////////////////////////////////////////////////////////////
MODULE_LICENSE("Dual BSD/GPL");

//////////////////////////////////////////////////////////////////////////////
// Static module parameters                                                 //
//////////////////////////////////////////////////////////////////////////////
static bool lvd = 1;        //Logic voltage domain opp enable
static bool mvd = 1;        //Memory voltage domain opp enable
static bool lpd = 1;        //Logic power doamin state change enable
static bool mpd = 1;        //Memory power domain state change enable

//////////////////////////////////////////////////////////////////////////////
// Public functions                                                         //
//////////////////////////////////////////////////////////////////////////////
static int __init pmicmi_tst_init(void)
{
        uint32_t pmi_cfg_org, pmi_cfg_new;
        uint32_t cmi_mode_org[CMI_MODULE_CNT], cmi_mode_new[CMI_MODULE_CNT];
        uint32_t cmi_cfg_org[CMI_MODULE_CNT], cmi_cfg_new[CMI_MODULE_CNT];
        bool pmi_state_org;
        bool cmi_state_org[CMI_MODULE_CNT];
        pmi_err_t pmi_err;
        cmi_err_t cmi_err;
        
        pmi_kdev_t * pmi_hndl;
        cmi_kdev_t * cmi_hndl[CMI_MODULE_CNT];
        
        printk(KERN_ALERT "%s:: PMI/CMI Test Entry\n", __FUNCTION__);
  
        ////////////////////////////////////////////////////////////////
        // Open PM devices
        ///////////////////////////////////////////////////////////////
        pmi_hndl = pmi_kopen(PM_ID, &pmi_err);
        if ( NULL == pmi_hndl) {
                printk(KERN_ALERT "%s:: pmi_kopen failed with err %d- exiting\n", __FUNCTION__, pmi_err);
                return -1;
        }
    
        ///////////////////////////////////////////////////////////////
        //get current PM state and switch to new state
        ///////////////////////////////////////////////////////////////
        pmi_err = pmi_kgetstate(pmi_hndl, &pmi_state_org, &pmi_cfg_org);
        if ( pmi_err ) {
                printk(KERN_ALERT "%s:: pmi_kgetstate returned err %d\n", __FUNCTION__, pmi_err);
                return -1;
        }
        else {
                printk(KERN_ALERT "%s:: pmi_kgetstate no err - current state %d, current config 0x%x\n", __FUNCTION__, pmi_state_org, pmi_cfg_org);
        }
        
        //modify pmi state true PMI module is enabled
        if ( pmi_state_org ) {
                pmi_err = pmi_kdisable(pmi_hndl);
                if ( pmi_err ) {
                    printk(KERN_ALERT "%s:: pmi_kdisable returned err %d\n", __FUNCTION__, pmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: pmi_kdisable no err\n", __FUNCTION__);
                }  
        }
        
        pmi_cfg_new = 0;
        if ( lvd ) pmi_cfg_new |= PMI_LVD_EN;
        if ( mvd ) pmi_cfg_new |= PMI_MVD_EN;
        if ( lpd ) pmi_cfg_new |= PMI_LPD_EN;
        if ( mpd ) pmi_cfg_new |= PMI_MPD_EN;
        
        pmi_err = pmi_kcfg(pmi_hndl, pmi_cfg_new);
                
        if ( pmi_err ) {
            printk(KERN_ALERT "%s:: pmi_kcfg returned err %d\n", __FUNCTION__, pmi_err);
            return -1;
        }
        else {
            printk(KERN_ALERT "%s:: pmi_kcfg no err - set config to 0x%x\n", __FUNCTION__, pmi_cfg_new);
        }
        
        if ( pmi_cfg_new ) {
            
                pmi_err = pmi_kenable(pmi_hndl);
                 
                if ( pmi_err ) {
                    printk(KERN_ALERT "%s:: pmi_kenable returned err %d\n", __FUNCTION__, pmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: pmi_kenable no err\n", __FUNCTION__);
                }
        }
        
        ///////////////////////////////////////////////////////////////
        //get current CM state and switch to new state
        ///////////////////////////////////////////////////////////////
        {
            int id;
            
            for (id=CM1_ID; id < CMI_MODULE_CNT; id++) { 
        
                cmi_hndl[id] = cmi_kopen(id, &cmi_err);
                if ( NULL == cmi_hndl[id]) {
                        printk(KERN_ALERT "%s:: cmi_kopen on CM%d failed with err %d- exiting\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                }
        
                cmi_err = cmi_kgetstate(cmi_hndl[id], &cmi_state_org[id], &cmi_mode_org[id], &cmi_cfg_org[id]);
                if ( cmi_err ) {
                        printk(KERN_ALERT "%s:: cmi_kgetstate for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                }
                else {
                        printk(KERN_ALERT "%s:: cmi_kgetstate for CM%d no err - current state %d, current mode %d, current config 0x%x\n", __FUNCTION__, id+1, cmi_state_org[id], cmi_mode_org[id], cmi_cfg_org[id]);
                }       
        
        
                //modify cmi state, true PMI module is enabled
                if ( cmi_state_org[id] ) {
                        cmi_err = cmi_kdisable(cmi_hndl[id]);
                        if ( cmi_err ) {
                            printk(KERN_ALERT "%s:: cmi_kdisable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                            return -1;
                        }
                        else {
                            printk(KERN_ALERT "%s:: cmi_kdisable for CM%d no err\n", __FUNCTION__, id+1);
                        }  
                }
                
                if ( CM1_ID == id ) {
                    cmi_cfg_new[id] =   CM_EVT_STATE_EN 
                                      | CM_EVT_FRQDIV_EN              
                                      | CM_EVT_SRCSEL_EN              
                                      | CM1_EVT_CORE_DPLL_EN         
                                      | CM1_EVT_MPU_DPLL_EN          
                                      | CM1_EVT_IVA_DPLL_EN          
                                      | CM1_EVT_ABE_DPLL_EN          
                                      | CM1_EVT_DDRPHY_DPLL_EN;
                }
                else {
                    cmi_cfg_new[id] =   CM_EVT_STATE_EN 
                                      | CM_EVT_FRQDIV_EN 
                                      | CM_EVT_SRCSEL_EN
                                      | CM2_EVT_PER_DPLL_EN 
                                      | CM2_EVT_USB_DPLL_EN 
                                      | CM2_EVT_UNIPRO_DPLL_EN;
                }
                
                cmi_mode_new[id] = CMI_EVENT;
                
                cmi_err = cmi_kcfg(cmi_hndl[id], cmi_mode_new[id], cmi_cfg_new[id]);
                
                if ( cmi_err ) {
                    printk(KERN_ALERT "%s:: cmi_kcfg for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: cmi_kcfg for CM%d no err - set config to 0x%x\n", __FUNCTION__, id+1, cmi_cfg_new[id]);
                }
                    
                cmi_err = cmi_kenable(cmi_hndl[id]);
                 
                if ( cmi_err ) {
                    printk(KERN_ALERT "%s:: cmi_kenable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: cmi_kenable for CM%d no err\n", __FUNCTION__, id+1);
                }                  
            } // End of fist for loop
            ///////////////////////////////////////////////////////////////
            //Wait for a while
            ///////////////////////////////////////////////////////////////
            {
                    uint i = 250000;
                    do {
                        i--;
                    }
                    while (i>0);
            }        
         
            ///////////////////////////////////////////////////////////////
            //Switch CMs to activity mode
            ///////////////////////////////////////////////////////////////
            {
                int id;
                
                for (id=CM1_ID; id < CMI_MODULE_CNT; id++) {     
                
                    cmi_err = cmi_kdisable(cmi_hndl[id]);
                    if ( cmi_err ) {
                        printk(KERN_ALERT "%s:: cmi_kdisable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                    }
                    else {
                        printk(KERN_ALERT "%s:: cmi_kdisable for CM%d no err\n", __FUNCTION__, id+1);
                    }  
                    
                    // Switch to target and initiator activity monitoring
                    cmi_cfg_new[id] =   CM_ACT_TARGET_EN
                                      | CM_ACT_INITIATOR_EN;
                    
                    cmi_mode_new[id] = CMI_ACTIVITY;
                    
                    cmi_err = cmi_kcfg(cmi_hndl[id], cmi_mode_new[id], cmi_cfg_new[id]);
                    
                    if ( cmi_err ) {
                        printk(KERN_ALERT "%s:: cmi_kcfg for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                    }
                    else {
                        printk(KERN_ALERT "%s:: cmi_kcfg for CM%d no err - set config to 0x%x\n", __FUNCTION__, id+1, cmi_cfg_new[id]);
                    }
                        
                    cmi_err = cmi_kenable(cmi_hndl[id]);
                     
                    if ( cmi_err ) {
                        printk(KERN_ALERT "%s:: cmi_kenable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                    }
                    else {
                        printk(KERN_ALERT "%s:: cmi_kenable for CM%d no err\n", __FUNCTION__, id+1);
                    }                  
                }
            }
            ///////////////////////////////////////////////////////////////
            //Wait for a while again
            ///////////////////////////////////////////////////////////////
            {
                    uint i = 250000;
                    do {
                        i--;
                    }
                    while (i>0);
            }        
         
        }
        
        ///////////////////////////////////////////////////////////////
        //All done - restore original PM state
        ///////////////////////////////////////////////////////////////
        printk(KERN_ALERT "%s:: Restore the original state\n", __FUNCTION__ );
        
        
        pmi_err = pmi_kdisable(pmi_hndl);
 
        if ( pmi_err ) {
            printk(KERN_ALERT "%s:: pmi_kdisable returned err %d\n", __FUNCTION__, pmi_err);
            return -1;
        }
        else {
            printk(KERN_ALERT "%s:: pmi_kdisable no err\n", __FUNCTION__);
        }       
        
        pmi_err = pmi_kcfg(pmi_hndl, pmi_cfg_org);
        
        if ( pmi_err ) {
            printk(KERN_ALERT "%s:: pmi_kcfg returned err %d\n", __FUNCTION__, pmi_err);
            return -1;
        }
        else {
            printk(KERN_ALERT "%s:: pmi_kcfg no err - restored config 0x%x\n", __FUNCTION__, pmi_cfg_org);
        } 
        
        if ( pmi_state_org ) {
            
                printk(KERN_ALERT "%s:: Re-enable \n", __FUNCTION__);
                pmi_err = pmi_kenable(pmi_hndl);
                
                if ( pmi_err ) {
                    printk(KERN_ALERT "%s:: pmi_kenable returned err %d\n", __FUNCTION__, pmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: pmi_kenable no err\n", __FUNCTION__);
                }                 
                
        }
           
        pmi_err = pmi_krelease(pmi_hndl);
        
        if ( pmi_err ) {
            printk(KERN_ALERT "%s:: pmi_krelease returned err %d\n", __FUNCTION__, pmi_err);
            return -1;
        }
        else {
            printk(KERN_ALERT "%s:: pmi_krelease no err\n", __FUNCTION__);
        }
        
        ///////////////////////////////////////////////////////////////
        //All done - restore original CM state
        ///////////////////////////////////////////////////////////////
        {
            int id;
            
            for (id=CM1_ID; id < CMI_MODULE_CNT; id++) {  
                
                cmi_err = cmi_kdisable(cmi_hndl[id]);
                if ( cmi_err ) {
                    printk(KERN_ALERT "%s:: cmi_kdisable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: cmi_kdisable for CM%d no err\n", __FUNCTION__, id+1);
                }       
        
                cmi_err = cmi_kcfg(cmi_hndl[id], cmi_mode_org[id], cmi_cfg_org[id]);
                if ( cmi_err ) {
                    printk(KERN_ALERT "%s:: cmi_kcfg for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: cmi_kcfg for CM%d no err - restored config 0x%x\n", __FUNCTION__, id+1, cmi_cfg_org[id]);
                } 
                
                if ( cmi_state_org[id] ) {
                    
                    cmi_err = cmi_kenable(cmi_hndl[id]);    
                    if ( pmi_err ) {
                        printk(KERN_ALERT "%s:: cmi_kenable for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                        return -1;
                    }
                    else {
                        printk(KERN_ALERT "%s:: cmi_kenable for CM%d no err\n", __FUNCTION__, id+1);
                    }      
                        
                } 
                
                cmi_err = cmi_krelease(cmi_hndl[id]);
                if ( cmi_err ) {
                    printk(KERN_ALERT "%s:: cmi_krelease for CM%d returned err %d\n", __FUNCTION__, id+1, cmi_err);
                    return -1;
                }
                else {
                    printk(KERN_ALERT "%s:: cmi_krelease for CM%d no err\n", __FUNCTION__, id+1);
                }
                
                printk(KERN_ALERT "%s:: CM%d exit loop\n", __FUNCTION__, id+1);             
            }
        }
        
        printk(KERN_ALERT "%s:: Exit - don't forget to rmmod pmicmi_tst.ko\n",__FUNCTION__);
        
        return 0;   
}   
module_init(pmicmi_tst_init);

static void __exit pmicmi_tst_exit(void)
{
    printk(KERN_ALERT "%s:: PMI/CMI Test exit - Hasta la vista\n", __FUNCTION__);
}
module_exit(pmicmi_tst_exit);

//////////////////////////////////////////////////////////////////////////////
// Required Kernel Globals                                                  //
//////////////////////////////////////////////////////////////////////////////
module_param(lvd, bool, S_IRUGO);         //Logic voltage domain opp enable
module_param(mvd, bool, S_IRUGO);         //Memory voltage domain opp enable
module_param(lpd, bool, S_IRUGO);          //Logic power doamin state change enable
module_param(mpd, bool, S_IRUGO);         //Memory power domain state change enable
