#ifndef __PMILIBRARY_H
#define __PMILIBRARY_H
/*
 * PMILib.h
 *
 * Power Management Instrumentation API Definitions
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


/*! \file PMILib.h
    PMI Library Function Prototypes
*/
/*! \mainpage
    The PMI Library provides a PMI Module programming API. This library also provides PMI specific STM logging capabilities.

    \par PMI Library Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.1 </TD> <TD> 2/7/2010 </TD> <TD> PMI Library Alpha Release - no support for PMI_Mark functions.  </TD>
<TR><TD> 0.2 </TD> <TD> 2/18/2010 </TD> <TD> PMI Library Beta Release - All functions work per this documentation.  </TD>
<TR><TD> 0.3 </TD> <TD> 5/21/2010 </TD> <TD> Combined PMI/CMI Library Release </TD>
<TR><TD> 0.4 </TD> <TD> 6/07/2010 </TD> <TD> Fixed PCMI_ModuleClockEnable() to poll module status and added _STMLogging build option  </TD>
<TR><TD> 1.0 </TD> <TD> 8/10/2010 </TD> <TD> Modified:
                                                 \li PMI_OpenModule to use a config structure
                                                 \li Eliminated PMI_SetSampleWindow from API
                                                 \li PMI_GetSampleWindow modified to return both components of the sampel window  </TD>
</TABLE>
   Note: The PMI and CMI Libraries share version numbers since there are shared elements between the libraries.

    \par Conventions
    The following public naming conventions are used by the API
    \li PMI_ - Prefix for public PMI Library functions
    \li ePMI - Prefix for public PMI enumerations 
    \li ePCMI - Prefix for public shared enumerations used by both PMI and CMI Libraries.
    
    \par Build Notes
    The PMI Library supports the following pre-defined symbols that if defined
    enable the functionality at compile time.  
    \li _OMAP4430 - Include device specific parameters.
    \li _PMI - Compile for PMI specific instance of shared components (verses a CMI build).
    \li _STMLogging - If defined the following APIs, that require the library be opened with a valid STM Library handle,
        are included in the build:
        - PMI_LogMsg()
    \li _NoMark - May be defined to exclude the mark functions. 
         These functions require _STMLogging and _OMAP4430 to also be defined. 
        - PMI_MarkLogicVoltageOPPChange()
        - PMI_MarkMemoryVoltageOPPChange()
        - PMI_MarkLogicPowerStateChange()
        - PMI_MarkMemoryPowerStateChange()
    \li _UsingTITools - Includes TI specific pragmas that may not be compatible with other build tools such as gcc.
        Not defining this symbol will simply exclude the use of TI specific pragmas in the library. 
        The following sections are created with pragmas:
        - Code                           => .text:PCMILibrary
        - Error strings                  => .dataImageErrStrings:PCMILib
        - Internal STM message strings   => .dataImageMsgStrings:PCMILib  
        \n Note: The Internal STM messages strings can be placed in non-existent memory. 
        Only pointers to these strings are transferred over the STM. 
        
    \par PMI Export Notes
    If you are using CCS to capture STM data (with either a trace enabled emulator or ETB)
    you should enable the PMI module's STM messages through the CSS 4.x Breakpoint view by
    creating a Trace job and settings the following Trace Properties:
    \li Trace Type: System
    \li STM Trace Type: Trace Export Configuration
    \li Enable HW Message Sources:
    \li PM Event Profiling: True
    Note that PMI Event Profiling by default is enabled. CMI Event profiling is not.
    
    If you are working remotely from CCS and using the ETB to capture STM data 
    then you must provide your own application code to enable the CMI module's STM messages. 
 
*/

#ifdef _CIO
#include <stdio.h>
#endif

#ifndef __linux__        //If linux don't need
#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 
#else
#include <linux/kernel.h>
#include <stddef.h>
#endif

//Using forward slashes in the relative search path for linux compatibility

#ifdef _OMAP4430                 // must be OMAP4430 build
#include "../../PMICMILib/src/PCMI_Omap4430.h"
#endif

#include "../../STMLib/include/StmLibrary.h"
#include "../../STMLib/src/StmSupport.h"
#include "../../PMICMILib/src/PCMI_Common.h"



#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public PMI Specific Enumerations
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par ePMI_EventEnables
    PMI Event Enable selections
*/
typedef enum { ePMI_ENABLE_NONE =0,                              /*!< Clear all event enable bits*/
               ePMI_ENABLE_LOGIC_VOLTAGE_DOMAIN_OPP_CHANGE = 1,  /*!< Set Logic Voltage Domain OPP change bit */
               ePMI_ENABLE_MEMORY_VOLTAGE_DOMAIN_OPP_CHANGE = 2, /*!< Set Memory Voltage Domain OPP change bit*/
               ePMI_ENABLE_LOGIC_POWER_DOMAIN_STATE_CHANGE = 4,  /*!< Set Logic power domain state change bit */
               ePMI_ENABLE_MEMORY_POWER_DOMAIN_STATE_CHANGE = 8, /*!< Set Memory power domain state change bit */
               ePMI_ENABLES_NOT_MODIFIED = -1                  /*!< Don't modify the event enable bits */
               } ePMI_EventEnables;

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs
//
////////////////////////////////////////////////////////////////////////////////////////////////
/*! \par ePMI_ModID
    Power Module ID list
*/
typedef enum { ePMI_PM,                /*!< Power Module ID */
               ePMI_PM_ModuleCnt       /*!< Number of power modules in the System */ 
             } ePMI_ModID;
             
/*! PMI_Handle 
    PMI Handle object. Note that PMI_Handle is an incomplete struct hiding
    the actual PMI_Handle_t implementation from clients.
*/
typedef struct PMI_Handle_t PMI_Handle;

/*! PMI_Handle_Pntr
    Pointer to a PMI Handle object
*/
typedef PMI_Handle * PMI_Handle_Pntr;

/*! \par PMI_CfgParams
    PMI Configuration Parameters 
*/ 
typedef struct _PMI_CfgParams {
                uint32_t PMI_Module_BaseAddr;           /*!< Base address of PMI unit */
                uint32_t PMI_ClockCtl_BaseAddr;         /*!< Base address of PMI Clock Control unit. If a specific port does not include a 
                                                             clock control unit then set to NULL */
                PCMI_CallBack pPMI_CallBack;             /*!< ::CPT_CallBack is called by any CPT Library function prior to returning an error.
                                                             May be set to NULL to disable this feature. Default value is NULL.*/ 
#ifdef _STMLogging
                STMHandle * pSTMHandle;                 /*!< A pointer to a STM Handle for logging and providing meta data (possibly required by host tools).
                                                             If NULL STM logging is disabled. Default value is NULL.*/

                uint8_t STMMessageCh;                   /*!< A channel to use for CPT Library STM Messages (if pSTMHandle is not NULL) generated by:
                                                            \li PMI_LogMsg() 
                                                            \li PMI_ModuleActivityEnable() 
                                                            \li PMI_ModuleActivityDisable()
                                                        */
#endif
                uint32_t FuncClockDivider;              /*!< This value sets the sample window counter's sys clock divider. Must be in the range of 1 to 16. */
                                                                    
                uint32_t SampleWindowSize;              /*!< This value multiplied by the FuncClockDivider determines the sample window size in functional clocks. 
                                                             Must be in the range of 1 to 256. */ 
             }PMI_CfgParams;

/*! \par ePMI_Error
    PMI Error codes 
*/ 
typedef ePCMI_Error ePMI_Error;                     /*!< PMI specific version of ePCMI_Error type */  

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Function Definitions
//
////////////////////////////////////////////////////////////////////////////////////////////////


/*! \par PMI_OpenModule         
    
    Open the PMI Library API. Aquire ownership of the device's PMI module and allocate PMI Handle storage. 
    
    \param[out] pPMI_Handle     Pointer to a NULL ::PMI_Handle_Pntr. If PMI_OpenModule exits
                                with the return value of ::ePCMI_Success, the location pointed to by ::pPMI_Handle
                                is set to a valid PMI_Handle pointer.
    \param[in] PMI_ModId        ::ePMI_ModID enumerated module intentifier.
    \param[in] pPMI_CfgParams   Pointer to a ::PMI_CfgParams structure.
    
    \return ::ePMI_Error 

    \par Details:
    \details   
    
    This function must be called and return ::ePCMI_Success prior to any
    other PMI Library function that requires a ::PMI_Handle_Pntr paramter.
    Initially this function will check that the PMI module is functional.
    If the module is not in the functional state, this function will attempt
    to change the module's state to functional. 
    If the module's state can not be changed to functional this function will
    exit and return ::ePCMI_Error_ModuleClkStateModifyFailed. 
    This function will then confirm the PMI module is compatible with this version of the library. 
    If not compatible the function will exit and return ::ePCMI_Error_Not_Compatible.
    The function will attempt to claim ownership of the PMI module.
    If ownership is not achieved this function will exit with the return value set to ::ePMI_Error_Ownership_Not_Granted.
    If ownership successful a soft reset will be applied to the PMI module.
    If ResetDone is not detected, ownership will be released and this
    function exited with ::ePCMI_Error_ResetDone_Not_Detected.
    If function exits with ::ePCMI_Success storage for PMI_Handle is allocated
    through a call to the client provided external function STM_memAlloc().
    STM_memMap(), also a client provided function, is called by PMI_OpenModule to map
    the physical PMI_BaseAddress provided to a virtual address space.
     
    Prototype for client provided functions:
    \n void * STM_memAlloc(size_t sizeInBytes) return NULL if memory not allocated
    \n void * STM_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes) return phyAddr if virtual mapping not required. 	
 	
 	If the function exits with ::ePCMI_Success the device's PMI module default values are set to:
        \li Sample window size set to 4096 sysclks
        \li All events enabled for capture

    If pPMI_CfgParams->pSTMHandle is not NULL the PMI Library will:
        \li Generating STM messages for meta parameters needed by data analysis tools
            (such as the Trace Anaylzer)
        \li Enables library provided logging messages that reflect important state changes to the user.
        \li Enables user generated messages through PMI_LogMsg().  

*/
ePMI_Error PMI_OpenModule(PMI_Handle_Pntr * pPMI_Handle, ePMI_ModID PMI_ModId, PMI_CfgParams * pPMI_CfgParams);

/*! \par PMI_CloseModule

    Close the PMI Library. Disable the device's PMI module and release ownership. Free the PMI_Handle storage.

    \param[in] pPMI_Handle PMI Handle Pointer
    \return ::ePMI_Error
    
    \par Details:
    \details 
    
     Confirm PMI module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed
     if application does not own the module (debugger can override ownership).
 	 If ownership confirmed attempt to release the module. If ownership is not released exit and return
 	 ::ePCMI_Error_Ownership_Not_Released.
 	 The PMI_Handle is destroyed through a call to the client provided external function STM_memFree.
     The PMI_BaseAddress is unmapped through a call to the client provided external function STM_memUnMap.
 	 In the event a PMI module error is detected, the PMI_Handle is still destroyed.   
 	 No PMI Library functions can be called with PMI_Handle after PMI_CloseModule is called.

    \n Prototype for client provided functions:
    \n void STM_memFree(void *p) 
    \n void STM_memUnMap(void * vAddr, unsigned int mapSizeInBytes) if vitual address mapping not required simply return.
 
*/
ePMI_Error PMI_CloseModule(PMI_Handle_Pntr pPMI_Handle );

/*! \par PMI_GetVersion

    Get PMI Library version, software functional ID and hardware functional ID.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[out] pLibMajorVersion PMI Library Major Version (API modifications not backward compatible)  
    \param[out] pLibMinorVersion PMI Library Minor Version (Backward compatible modifications)
    \param[out] pSWFuncID The deivce's PMI Module Functional ID the library is compatible with
    \param[out] pHwFuncID The actual device's PMI Module Functional ID
    \return ::ePMI_Error
    
    \par Details:
    \details 
    
    This function provides the major and minor version numbers of the library's build. 
    These values can be compared with the following macro values in PMI_CMI_Common.h to confirm that the
    version of the library matches that of the header file.

    \n PMICMILIB_MAJOR_VERSION
    \n PMICMILIB_MINOR_VERSION

    Major version releases of the same level will be backward compatible with lower minor release versions.
    Any change in the API will be reflected in a change to the major release version. The minor release
    version is typically for bug fixes.

    The pPMISWFuncID value must match the pPMIHwFuncID value for a call to PMI_OpenModule not to
     return ::ePCMI_Error_Not_Compatible. 

*/

ePMI_Error PMI_GetVersion(PMI_Handle_Pntr pPMI_Handle, uint32_t * pLibMajorVersion, uint32_t * pLibMinorVersion, uint32_t * pSWFuncID, uint32_t * pHwFuncID);

/*! \par PMI_ModuleActivityEnable

    Enable the PMI module's activity collection and STM message export.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \return ::ePMI_Error
    
    \par Details:
    \details 
    
     Confirm PMI module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed if application does not
     own the module (debugger can override our ownership).
     The PMI module is then enabled and the enable state confirmed. If the enable state cannot be confirmed 
     ::ePCMI_Error_ModuleEnableFailed is returned. 
     
     If PMI_OpenModule was called with a non-NULL STMHandle, the PMI meta data STM messages are sent
     and a PMI Library logging message indicating the PMI HW module has started monitoring power activity.
     
 	 The module is then enabled to start the device's PMI STM message generation. 
*/

ePMI_Error  PMI_ModuleActivityEnable (PMI_Handle_Pntr pPMI_Handle );

/*! \par PMI_ModuleActivityDisable

     Disable the PMI module's activity collection and the device's PMI STM message export.
    
     \param[in] pPMI_Handle PMI Handle Pointer 
     \param[in] retain if true (non-zero) do not reset the module, if false perform soft reset of the PMI module
     \return ::ePMI_Error
    
     \par Details:
     \details  
    
     Confirm PMI module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed if application does not
     own the module (debugger can override our ownership). 
     If not already Disabled, Disable the PMI module for activity collection. 
     If PMI module ownership state Enabled, ownership state changed to Claimed.
     If retain is true, the current programming is retained, else a Softreset asserted and reset done confirmed.
     If reset done not confirmed exit and return ::ePCMI_Error_ResetDone_Not_Detected. 
    
     If PMI_OpenModule was called with a non-NULL STMHandle, the PMI meta data STM messages are sent
     and a PMI Library logging message indicating the PMI HW module has ended monitoring power activity.
    
     Note: The meta data is provided, in addition to being provided by PMI_ModuleActivityEnable(), incase the buffer
     wrapped thus allowing the sample window size to be applied to the previous data by analysis tools (such as the Trace Analyzer).

     Note: PMI_ModuleActivityDisable may be called with retain set true, and then called a second
     time with retain false prior to reprogramming the module.

*/
ePMI_Error  PMI_ModuleActivityDisable(PMI_Handle_Pntr pPMI_Handle, int32_t retain);

/*! \par PMI_ConfigModule

    Configure the PMI module's triggers or select events for activity collection. If the module has
    been previously enabled for activity collection, PMI_ModuleActivityDisable() must be called prior to 
    calling this function. The PMI module triggers and selected events are set to default
    values by PMI_OpenModule().

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] triggerEnables ::ePCMI_Triggers enumerated optional module start and stop triggers (default after calling PMI_OpenModule(), no triggers enabled)   
    \param[in] eventEnables ::ePMI_EventEnables enumerated event enables (default after calling PMI_OpenModule(), all events enabled)
    \return ::ePMI_Error
    
    \par Details:
    \details  
        
    Confirm PMI module ownership and return ::ePCMI_Error_Ownership_Not_Confirmed if application does not
    own the module (debugger can override our ownership).
    If this function is attempted while module activity is enabled this function will exit and return 
    ::ePCMI_Error_Module_Enabled.

    The start and stop trigger enable values can be ored together to enable both
    start and stop triggers at the same time. Triggers not explicitly set are disabled.
    Any event enable values can be ored with any other event enable to set multiple
    event classes. Any events not explicitly enabled will be disabled.
    
    Note: There is no requirement that PMI_ConfigModule be called prior to PM_ModuleActivityEnable().

*/
ePMI_Error PMI_ConfigModule(PMI_Handle_Pntr pPMI_Handle, ePCMI_Triggers triggerEnables, ePMI_EventEnables eventEnables);

/*! \par PMI_GetSampleWindow

    Retrieve the actual PMI module's sample window time frame in sysclocks.

    \param[in] pPMI_Handle PMI Handle Pointer
    \param[out] pFuncClockDivider Functional clock divider parameter (1-16 are valid values). 
    \param[out] pSampleWindowSize Sample window size (1-256 are valid values).   
    \return ePMI_Error
    
    \par Details:
    \details  
    
    This function provides the components to compute the actual size of the sample window in
    functional clocks ( FuncClockDivider * SampleWindowSize).

*/

ePMI_Error PMI_GetSampleWindow(PMI_Handle_Pntr pPMI_Handle, uint32_t * pFuncClockDivider, uint32_t * pSampleWindowSize );

/*! \par PMI_GetErrorMsg

    Retrieve a pointer to errorCode's text string.

    \param[in] errorCode ::ePMI_Error enumerated error code.   
    \return char * Pointer to the error string.
    
    \par Details:
    \details  
    
   It's expected this function will only be used when a client implements
   their own error handling process.
   Error message strings are centralized in the .dataImageErrStrings:PCMILib data section
   so the user can choose to link this section to real memory (to access the error strings
   from their application) or link them in non-existent memory space if this function is not used.

*/
const char *  PMI_GetErrorMsg(ePMI_Error errorCode); 

/*! \par PMI_LogMsg

    Issue a client provided Power Management relevant STM message.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] FmtString Format string (like printf format string) limited to a single int or unsigned int conversion character.
    \param[in] pValue Pointer to formatted value. NULL if not used in format string.
    \return ::ePMI_Error
    
    \par Details:
    \details   
    
    The following information will be provided in the PMI STM message:
    \li Module - Pointer to the module's name string, typically "PM". Derived from PMI_ModId (set by PMI_OpenModule()). 
    \li Data Domain  - Pointer to the doamin name, in this case it's "SW".
    \li Data Class   - Pointer to the class name, in this case it's "PMI User Message".
    \li Data Type    - Pointer to the data type, in this case it's "Msg".
    \li Data Message - Pointer to the FmtString.
    \li value - If pValue is not NULL, value is provided.

 	If PMI_OpenModule() is opened with a NULL STMHandle, this function exits
 	and returns ::ePCMI_Error_NULL_STMHandle.
    
*/
ePMI_Error  PMI_LogMsg(PMI_Handle_Pntr pPMI_Handle, const char * FmtString, int32_t  * pValue);

/*! \par PMI_MarkLogicVoltageOPPChange

    Issue a standard Logic Voltage OPP Change STM message. 
    This function provides the client a mechansim to mark when a logic voltage change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the voltage setting, and when the device responds to the request.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] pwrDomain ::ePMI_LVD_OPP specifies the logic vlotage domain that changed.
    \param[in] pwrValue Is the 8-bit SMPS Output Voltage Selection Code (Standard Mode) used for voltage level selection.
    Valid values are 0 to 0x3E. 
    \return ::ePMI_Error
    
    \par Details:
    \details   
    
    The following information will be provided in the PMI STM message:
    \li Module - Pointer to the module's name string, typically "PM". Derived from PMI_ModId (set by PMI_OpenModule()). 
    \li Data Domain  - Pointer to the logic voltage domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Logic Voltage OPP Change".
    \li Data Type    - Pointer to the data type, in this case it's "Value".
    \li Data Message - Pointer to the pwrValue converted to Vout(mV) string ("612.5 mV").
 	
 	If PMI_OpenModule() is opened with a NULL STMHandle, this function exits
 	and returns ::ePCMI_Error_NULL_STMHandle. If pwrValue value is not in the range
 	of 0 to 3E the function exits with a ::ePCMI_Error_Invalid_Parameter error. 

    All text for these mesages are provided in the .dataImageMsgStrings:PMILib section and at link time
    may be placed in non-existant memory.
    
*/
#ifndef _NoMark
ePMI_Error  PMI_MarkLogicVoltageOPPChange(PMI_Handle_Pntr pPMI_Handle, ePMI_LVD_OPP pwrDomain, uint8_t pwrValue);

/*! \par PMI_MarkMemoryVoltageOPPChange

    Issue a standard Memory Voltage OPP Change STM message. 
    This function provides the client a mechansim to mark when a memory voltage change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the voltage setting, and when the device responds to the request.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] pwrDomain ::ePMI_MVD_OPP specifies the memory voltage domain that changed.
    \param[in] pwrState ::ePMI_MVD_PwrState specifies the memory voltage domain state. 
    \return ::ePMI_Error
    
    \par Details:
    \details  
    
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "PM". Derived from PMI_ModId (set by PMI_OpenModule()). 
    \li Data Domain  - Pointer to the memory voltage domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Memory Voltage OPP Change".
    \li Data Type    - Pointer to the data type, in this case it's "State".
    \li Data Message - Pointer to the voltage state. Derived from the pwrState parameter.
 	
 	If PMI_OpenModule() is opened with a NULL STMHandle, this function exits
 	and returns ::ePCMI_Error_NULL_STMHandle.  
    
    All text for these mesages are provided in the .dataImageMsgStrings:PMILib section and at link time
    may be placed in non-existant memory.
    
*/
ePMI_Error  PMI_MarkMemoryVoltageOPPChange(PMI_Handle_Pntr pPMI_Handle, ePMI_MVD_OPP pwrDomain, ePMI_MVD_PwrState pwrState);

/*! \par PMI_MarkLogicPowerStateChange

    Issue a standard Logic Power State Change STM message. 
    This function provides the client a mechansim to mark when a logic power state change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the power setting, and when the device responds to the request. 

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] pwrDomain ::ePMI_LPD_StateChange specifies the logic power domain that changed.
    \param[in] pwrState ::ePMI_LPD_PwrState specifies the logic power domain state. 
    \return ePMI_Error
    
    \par Details:
    \details  
    
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "PM". Derived from PMI_ModId (set by PMI_OpenModule()). 
    \li Data Domain  - Pointer to the memory voltage domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Logic Power State Change".
    \li Data Type    - Pointer to the data type, in this case it's "State".
    \li Data Message - Pointer to the logic power state. Derived from the pwrState parameter.
 	
 	If PMI_OpenModule() is opened with a NULL STMHandle, this function exits
 	and returns ::ePCMI_Error_NULL_STMHandle.  

     All text for these mesages are provided in the .dataImageMsgStrings:PMILib section and at link time
     may be placed in non-existant memory.
    
*/
ePMI_Error  PMI_MarkLogicPowerStateChange(PMI_Handle_Pntr pPMI_Handle, ePMI_LPD_StateChange pwrDomain, ePMI_LPD_PwrState pwrState);

/*! \par PMI_MarkMemoryPowerStateChange

    Issue a standard Memory Power State Change STM message. 
    This function provides the client a mechansim to mark when a memory power state change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the power setting, and when the device responds to the request.

    \param[in] pPMI_Handle PMI Handle Pointer 
    \param[in] pwrDomain ::ePMI_MPD_StateChange specifies the memory power domain that changed.
    \param[in] pwrState ::ePMI_MPD_PwrState specifies the memory power domain state. 
    \return ePMI_Error
    
    \par Details:
    \details  
        
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "PM". Derived from PMI_ModId (set by PMI_OpenModule()). 
    \li Data Domain  - Pointer to the memory power domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Memory Power State Change".
    \li Data Type    - Pointer to the data type, in this case it's "State".
    \li Data Message - Pointer to the memory power state. Derived from the pwrState parameter.
 	
 	If PMI_OpenModule() is opened with a NULL STMHandle, this function exits
 	and returns ::ePCMI_Error_NULL_STMHandle.  
 
     All text for these mesages are provided in the .dataImageMsgStrings:PMILib section and at link time
     may be placed in non-existant memory.   
*/
ePMI_Error  PMI_MarkMemoryPowerStateChange(PMI_Handle_Pntr pPMI_Handle, ePMI_MPD_StateChange pwrDomain, ePMI_MPD_PwrState pwrState);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __PMILIBARY_H */
