#ifndef __CMILIBRARY_H
#define __CMILIBRARY_H
/*
 * CMILib.h
 *
 * Clock Management Instrumentation API Definitions
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file CMILib.h
    CMI Library Function Prototypes
*/
/*! \mainpage
    The CMI Library provides a CMI Module programming API. This library also provides CMI specific STM logging capabilities.

    \par CMI Library Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.3 </TD> <TD> 5/21/2009 </TD> <TD> Combined PMI/CMI Library Release  </TD>
<TR><TD> 0.4 </TD> <TD> 6/07/2009 </TD> <TD> Fixed PCMI_ModuleClockEnable() to poll module status and added _STMLogging build option  </TD>
<TR><TD> 1.0 </TD> <TD> 8/10/2010 </TD> <TD> Modified:
                                                 \li CMI_OpenModule to use a config structure
                                                 \li Eliminated CMI_SetSampleWindow from API
                                                 \li CMI_GetSampleWindow modified to return both components of the sampel window  </TD>
<TR><TD> 1.2 </TD> <TD> 9/26/2013 </TD> <TD> Support for DRA7xx  </TD>
<TR><TD> 1.3 </TD> <TD> 9/29/2014 </TD> <TD> Added missing CCS project files  </TD>
</TABLE>

    Note: The PMI and CMI Libraries share version numbers since there are shared elements between the libraries.
    
    \par Conventions
    The following public naming conventions are used by the API
    \li CMI_ - Prefix for public CMI Library functions
    \li eCMI - Prefix for public CMI enumerations 
    \li ePCMI - Prefix for public shared enumerations used by both PMI and CMI Libraries.
    
    \par Build Notes
    The CMI Library supports the following pre-defined symbols that if defined
    enable the functionality at compile time.  
    \li _OMAP4430 - Include device specific parameters.
    \li _CMI - Compile for CMI specific instance of shared components (verses a PMI build).
    \li _STMLogging - If defined the following APIs, that require the library be opened with a valid STM Library handle,
        are included in the build:
        - CMI_LogMsg()
    \li _NoMark - May be defined to exclude the mark functions. 
         These functions require _STMLogging and _OMAP4430 to also be defined. 
        - CMI_MarkClockDomainStateChange()
        - CMI_MarkClockDividerRatioChange()
        - CMI_MarkClockSourceSelectionChange()
        - CMI_MarkDPLLSettingChange()
    \li _UsingTITools - Includes TI specific pragmas that may not be compatible with other build tools such as gcc. 
         Not defining this symbol will simply exclude the use of TI specific pragmas in the library.
         The following sections are created with pragmas:
        - Code                           => .text:PCMILibrary
        - Error strings                  => .dataImageErrStrings:PCMILib
        - Internal STM message strings   => .dataImageMsgStrings:PCMILib  
        \n Note: The Internal STM messages strings can be placed in non-existent memory. 
        Only pointers to these strings are transferred over the STM.  

    \par CMI Export Notes
    If you are using CCS to capture STM data (with either a trace enabled emulator or ETB)
    you should enable the CMI module's STM messages through the CSS 4.x Breakpoint view by
    creating a Trace job and settings the following Trace Properties:
    \li Trace Type: System
    \li STM Trace Type: Trace Export Configuration
    \li Enable HW Message Sources:
    \li CM1 Event Profiling: True
    \li CM2 Event Profiling: True
    Note that CM1 and CM2 Event Profiling by default are disabled. PMI Event Profiling is enabled by default.
    
    If you are working remotely from CCS and using the ETB to capture STM data 
    then you must provide your own application code to enable the CMI module's STM messages. 

*/

#ifdef _CIO
#include <stdio.h>
#endif

#ifndef __linux__        //If linux don't need
#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 
#else
#include <linux/kernel.h>
#include <stddef.h>
#endif

//Using forward slashes in the relative search path for linux compatibility

#ifdef _OMAP4430                 // must be OMAP4430 build
#include "../../PMICMILib/src/PCMI_Omap4430.h"
#endif

#include "../../STMLib/include/StmLibrary.h"
#include "../../STMLib/src/StmSupport.h"
#include "../../PMICMILib/src/PCMI_Common.h"

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public CMI Specific Enumerations
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCMI_ModEnableType
    CMI module enable type
*/
typedef enum { eCMI_Event,              /*!< Enable the module for Event collection */
               eCMI_Activity            /*!< Enable the module for Activity collection */
             } eCMI_ModEnableType;

/*! \par eCMI_EventEnables;
    CMI module event enables
*/
typedef enum { eCMI_ENABLE_EVENT_NONE = 0,                                    /*!< Clear all enabled events */
               eCMI_ENABLE_EVENT_CLOCK_DOMAIN_STATE_UPDATE = 0x000001,        /*!< Enable Event Clock Domain State Updates */ 
               eCMI_ENABLE_EVENT_CLOCK_FREQDIVIDER_8BIT_UPDATE = 0x000002,    /*!< Enable Event Clock Frequency Divider Ratio Updates that require 8-bit messages*/
               eCMI_ENABLE_EVENT_CLOCK_FREQDIVIDER_4BIT_UPDATE = 0x000004,    /*!< Enable Event Clock Frequency Divider Ratio Updates that require 4-bit messages*/
               eCMI_ENABLE_EVENT_CLOCK_SOURCE_SELECTION_UPDATE = 0x000008,    /*!< Enable Event Clock Source Selection Updates */
               
               eCMI_ENABLE_EVENT_CM1_CORE_DPLL_UPDATE = 0x010000,             /*!< Enable CM1 Core DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM1_MPU_DPLL_UPDATE = 0x020000,              /*!< Enable CM1 MPU DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM1_IVA_DPLL_UPDATE = 0x040000,              /*!< Enable CM1 IVA DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM1_ABE_DPLL_UPDATE = 0x080000,              /*!< Enable CM1 ABE DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM1_DDRPHY_DPLL_UPDATE =  0x100000,          /*!< Enable CM1 DDRPHY DPLL Event Updates */

               eCMI_ENABLE_EVENT_CM2_PER_DPLL_UPDATE   = 0x010000,             /*!< Enable CM2 PER DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM2_USB_DPLL_UPDATE = 0x020000,               /*!< Enable CM2 USB DPLL Event Updates */
               eCMI_ENABLE_EVENT_CM2_UNIPRO_DPLL_UPDATE  =  0x040000,          /*!< Enable CM2 UNIPRO DPLL Event Updates */
               eCMI_ENABLES_EVENT_NOT_MODIFIED = -1                            /*!< Do not modify enabled events */
             } eCMI_EventEnables;
             
/*! \par eCMI_ActivityEnables;
    CMI module activity enables
*/
typedef enum { eCMI_ENABLE_ACTIVITY_NONE = 0,                                 /*!< Clear all enabled activity monitoring */
               eCMI_ENABLE_ACTIVITY_TARGET = 0x3,                             /*!< Enable target activity monitoring */ 
               eCMI_ENABLE_ACTIVITY_INITIATOR = 0xc,                          /*!< Enable initiator activity monorting */
               eCMI_ENABLES_ACTIVITY_NOT_MODIFIED = -1                        /*!< Do not modify activity enables */
             } eCMI_ActivityEnables;
                          
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////
/*! \par eCMI_ModID
    Clock Module ID list
*/
typedef enum { eCMI_CM1,                         /*!< Clock Module 0 ID */ 
#ifndef _DRA7xx
               eCMI_CM2,                         /*!< Clock Module 1 ID (not valid for DRA7xx) */
#endif
               eCMI_CM_ModuleCnt                 /*!< Number of clock modules in the System */
             } eCMI_ModID;

/*! CMI_Handle 
    CMI Handle object. Note that CMI_Handle is an incomplete struct hiding
    the actual PMI_Handle_t implementation from clients.
*/
typedef struct CMI_Handle_t CMI_Handle;

/*! \par CMI_Hande_Pntr
    Pointer to a CMI Handle object
*/
typedef CMI_Handle * CMI_Handle_Pntr;

/*! \par CMI_CfgParams
    CMI Configuration Parameters 
*/ 
typedef struct _CMI_CfgParams {
                uint32_t CMI_Module_BaseAddr;           /*!< Base address of PMI unit */
                uint32_t CMI_ClockCtl_BaseAddr;         /*!< Base address of PMI Clock Control unit. If a specific port does not include a 
                                                             clock control unit then set to NULL */
                PCMI_CallBack pCMI_CallBack;            /*!< ::CMI_CallBack is called by any CPT Library function prior to returning an error.
                                                             May be set to NULL to disable this feature. Default value is NULL.*/ 
#ifdef _STMLogging
                STMHandle * pSTMHandle;                 /*!< A pointer to a STM Handle for logging and providing meta data (possibly required by host tools).
                                                             If NULL STM logging is disabled. Default value is NULL.*/
                uint8_t STMMessageCh;                   /*!< A channel to use for CPT Library STM Messages (if pSTMHandle is not NULL) generated by:
                                                            \li PMI_LogMsg() 
                                                            \li PMI_ModuleActivityEnable() 
                                                            \li PMI_ModuleActivityDisable()
                                                        */
#endif
                uint32_t FuncClockDivider;              /*!< This value sets the sample window counter's sys clock divider. Must be in the range of 1 to 16. */
                                                                    
                uint32_t SampleWindowSize;              /*!< This value multiplied by the FuncClockDivider determines the sample window size in functional clocks. 
                                                             Must be in the range of 1 to 256. */ 
             }CMI_CfgParams;

/*! \par eCMI_Error
    CMI Error codes 
*/ 
typedef ePCMI_Error eCMI_Error;                     /*!< CMI specific version of ePCMI_Error type */ 

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Function Definitions
//
////////////////////////////////////////////////////////////////////////////////////////////////


/*! \par CMI_OpenModule         
    
    Open the CMI Library API, aquire ownership of the device's CMI module and allocate CMI Handle storage. 
    
    \param[out] pCMI_Handle     A pointer to a NULL CMI_Handle_Pntr. If CMI_OpenModule exits
                                with the return value of ::ePCMI_Success, the location pointed to by ::pCMI_Handle
                                is set to a valid CMI_Handle pointer.
    \param[in] CMI_ModId        ::eCMI_ModID enumerated module intentifier.
    \param[in] pCMI_CfgParams   Pointer to a ::CMI_CfgParams structure.
    
    \return ::eCMI_Error 

    \par Details:
    \details   
    
    This function must be called and return ::ePCMI_Success prior to any
    other CMI Library function that requires a ::CMI_Handle_Pntr paramter.
    Initially this function will check that the CMI module is functional. If the module not in the functional state,
    this function will attempt to change the modules state to functional. 
    If the module's state can not be changed to functional this function will exit and return ::ePCMI_Error_ModuleClkStateModifyFailed. 
    This function will then confirm the device's CMI module is compatible with this version of the library. 
    If not compatible the function will exit and return ::ePCMI_Error_Not_Compatible.
    The function will attempt to claim ownership of the device's CMI module.
    If ownership is not achieved this function exit with the return value set to ::ePCMI_Error_Ownership_Not_Granted.
    If ownership successful a soft reset will be applied to the device's CMI module.
    If ResetDone is not detected, ownership will be released and this
    function exited with ::ePCMI_Error_ResetDone_Not_Detected.
    If function exits with ::ePCMI_Success storage for CMI_Handle is allocated
    through a call to the client provided external function STM_memAlloc().
    STM_memMap(), also a client provided function, is called by CMI_OpenModule to map
    the physical CMI_BaseAddress provided to a virtual address space.
     
    Prototype for client provided functions:
    \n void * STM_memAlloc(size_t sizeInBytes) return NULL if memory not allocated
    \n void * STM_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes) return phyAddr if virtual mapping not required.     
    
    If the function exits with ::ePCMI_Success the device's CMI module default values are set to:
        \li Sample window size set to 4096 sysclks
        \li All events enabled for capture

    If pCMI_CfgParams->pSTMHandle is not NULL the PMI Library will:
        \li Generating STM messages for meta parameters needed by data analysis tools
            (such as the Trace Anaylzer)
        \li Enables library provided logging messages that reflect important state changes to the user.
        \li Enables user generated messages through CMI_LogMsg().  

*/

eCMI_Error CMI_OpenModule(CMI_Handle_Pntr * pCMI_Handle, eCMI_ModID CMI_ModId, CMI_CfgParams * pCMI_CfgParams);

/*! \par CMI_CloseModule

    Close the CMI Library API. Disable the device's CMI module and release ownership. Free the CMI_Handle storage.

    \param[in] pCMI_Handle CMI Handle Pointer
    \return ::eCMI_Error
    
    \par Details:
    \details 
    
     Confirm CMI module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed if application
     does not own the module (debugger can override ownership).
     If ownership confirmed attempt to release the module. If ownership is not released exit and return
     ::ePCMI_Error_Ownership_Not_Released.
     The CMI_Handle is destroyed through a call to the client provided external function STM_memFree.
     The CMI_BaseAddress is unmapped through a call to the client provided external function STM_memUnMap.
     In the event a CMI error is detected, the CMI_Handle is still destroyed.   
     No CMI Library functions can be called with CMI_Handle after CMI_CloseModule is called.

    \n Prototype for client provided functions:
    \n void STM_memFree(void *p) 
    \n void STM_memUnMap(void * vAddr, unsigned int mapSizeInBytes) if vitual address mapping not required simply return.
 
*/
eCMI_Error CMI_CloseModule(CMI_Handle_Pntr pCMI_Handle );

/*! \par CMI_GetVersion

    Get CMI Library version, software functional ID and hardware functional ID.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[out] pLibMajorVersion CMI Library Major Version (API modifications not backward compatible)  
    \param[out] pLibMinorVersion CMI Library Minor Version (Backward compatible modifications)
    \param[out] pSWFuncID The deivce's CMI Module Functional ID the library is compatible with
    \param[out] pHwFuncID The actual device's CMI Module Functional ID
    \return ::eCMI_Error
    
    \par Details:
    \details 
    
    This function provides the major and minor version numbers of the library's build. 
    These values can be compared with the following macro values in PMI_CMI_Common.h to confirm that the
    version of the library matches that of the header file.

    \n PMICMILIB_MAJOR_VERSION
    \n PMICMILIB_MINOR_VERSION

    Major version releases of the same level will be backward compatible with lower minor release versions.
    Any change in the API will be reflected in a change to the major release version. The minor release
    version is typically for bug fixes.

    The pCMISWFuncID value must match the pCMIHwFuncID value for a call to CMI_OpenModule not to
     return ::ePCMI_Error_Not_Compatible. 

*/

eCMI_Error CMI_GetVersion(CMI_Handle_Pntr pCMI_Handle, uint32_t * pLibMajorVersion, uint32_t * pLibMinorVersion, uint32_t * pSWFuncID, uint32_t * pHwFuncID);

/*! \par CMI_ModuleEnable

    Enable the CMI module's activity collection and STM message export.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] modEnableType  ::eCMI_ModEnableType enumerated value used to select the monitor type, activity or events.
    \return ::eCMI_Error
    
    \par Details:
    \details 
    
     Confirm CMI module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed if application does not
     own the module (debugger can override our ownership).
     If the module is already enabled, this function will exit with either ::eCMI_Error_InUseForEventCapture
     or ::eCMI_Error_InUseForModuleActivity.
     The device's CMI module is then enable and the enable state confirmed. If the enable state cannot be confirmed 
     ::ePCMI_Error_ModuleEnableFailed is returned.      
     If enabled successfully the module is then set for either activity or event monitoring.
     
     If CMI_OpenModule was called with a non-NULL STMHandle, the PMI meta data STM messages are sent
     and a PMI Library logging message indicating the PMI HW module has started monitoring clock activity.

    The module is then set for either activity or event monitoring to start device's CMI STM message generation. 
*/

eCMI_Error  CMI_ModuleEnable (CMI_Handle_Pntr pCMI_Handle, eCMI_ModEnableType modEnableType);

/*! \par CMI_ModuleDisable

     Disable the CMI module's activity or event collection and the device's CMI STM message export.

     \param[in] pCMI_Handle CMI Handle Pointer 
     \param[in] retain if true (non-zero) do not reset the module, if false perform soft reset of the CMI module
     \return ::eCMI_Error
    
     \par Details:
     \details  
    
     Confirm STM module ownership and exit with return value set to ::ePCMI_Error_Ownership_Not_Confirmed if application does not
     own the module (debugger can override our ownership).
 
     If not already Disabled, Disable the CMI module for both activity and event monitoring. 
     If CMI module ownership state is Enabled, ownership state is changed to Claimed.
     If retain is true, the current programming is retained, else a soft reset asserted and reset done confirmed.
     If reset done not confirmed exit and return ::ePCMI_Error_ResetDone_Not_Detected.
      
     If CMI_OpenModule was called with a non-NULL STMHandle, the PMI meta data STM messages are sent
     and a PMI Library logging message indicating the PMI HW module has ended monitoring clock activity.
             
     Note: The meta data is provided, in addition to being provided by CMI_ModuleEnable(), incase the buffer
     wrapped thus allowing the sample window size to be applied to the previous data by analysis tools (such as the Trace Analyzer).

     Note: CMI_ModuleDisable may be called with retain set true, and then called a second
     time with retain false prior to reprogramming the module.

*/

eCMI_Error  CMI_ModuleDisable(CMI_Handle_Pntr pCMI_Handle, int32_t retain);

/*! \par CMI_ConfigModule

    Configure the CMI module's triggers, select activity monitoring enables, or select event snapshot capture enables .
    If the module has been previously enabled for activity collection, this function must be called before any CMI module
    configuration parameters (triggers, event enables, activity enables, or sample window) may be modified.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] triggerEnables ::ePCMI_Triggers enumerated optional module start and stop triggers (default set by CMI_OpenModule(), no triggers enabled)   
    \param[in] eventEnables ::eCMI_EventEnables enumerated event snapshot enables (default set by CMI_OpenModule(), all events enabled)
    \param[in] activityEnables ::eCMI_ActivityEnables enumerated activity monitor enables (default set by CMI_OpenModule(), all activity enabled)
    \return ::eCMI_Error
    
    Note: Since eventEnables and activityEnables share control register bits (they are mode dependent),
    either eventEnables must be set to ::eCMI_ENABLE_EVENT_NONE or activityEnables must be set to ::eCMI_ENABLE_ACTIVITY_NONE. 
    Trying to set both eventEnables and activityEnables at the same time will result in a ::ePCMI_Error_Invalid_Parameter. 
    To clear all event and activity configuration bits the eventEnables parameter must be set to ::eCMI_ENABLE_EVENT_NONE
    and activityEnables must be set to ::eCMI_ENABLE_ACTIVITY_NONE.
    
    \par Details:
    \details  
        
    Confirm CMI module ownership and return ::ePCMI_Error_Ownership_Not_Confirmed if application does not
    own the module (debugger can override our ownership).
    If this function is attempted while module activity or event capture is enabled this function will exit and return 
    ::ePCMI_Error_Module_Enabled.

    The start and stop trigger enable values can be ored together to enable both
    start and stop triggers at the same time. Triggers not explicitly set are disabled.
    Any event enable can be ored with any other event enable to set multiple
    event classes. Any events not explicitly enabled will be disabled.
    Any activity enables can be ored with any other activity enable to set multiple
    activity classes. Any activity classes not explicitly enabled will be disabled.

*/
eCMI_Error CMI_ConfigModule(CMI_Handle_Pntr pCMI_Handle, ePCMI_Triggers triggerEnables, eCMI_EventEnables eventEnables, eCMI_ActivityEnables activityEnables);

/*! \par CMI_GetSampleWindow

    Retrieve the actual CMI module's sample window time frame in sysclocks.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[out] pFuncClockDivider Functional clock divider parameter (1-16 are valid values). 
    \param[out] pSampleWindowSize Sample window size (1-256 are valid values).     
    \return ::eCMI_Error
    
    \par Details:
    \details  
    
    This function provides the actual size of the sample window in functional clocks. 

*/
eCMI_Error CMI_GetSampleWindow(CMI_Handle_Pntr pCMI_Handle, uint32_t * pFuncClockDivider, uint32_t * pSampleWindowSize );

/*! \par CMI_GetErrorMsg

    Retrieve a pointer to errorCode's text string.

    \param[in] errorCode  :eCMI_Error enumerated error code.   
    \return char * Pointer to the error string.
    
    \par Details:
    \details  
    
   Note: It's expected this function will only be used when a client implements
   their own error handling processes.
   Error message strings are centralized in the .dataImageErrStrings:PCMILib data section
   so the user can choose to link this section to real memory (to access the error strings
   from their application) or link them to non-existent memory space.

*/
const char *  CMI_GetErrorMsg(eCMI_Error errorCode); 

/*! \par CMI_LogMsg

    Issue a client provided Clock Management relevant STM message.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] FmtString Format string (like printf format string) limited to a single int or unsigned int conversion character.
    \param[in] pValue Pointer to formatted value. NULL if not used in format string.
    \return ::eCMI_Error
    
    \par Details:
    \details   
    
    The following information will be provided in the CMI STM message:
    \li Module - Pointer to the module's name string, typically "CM1" or "CM2". Derived from CMI_ModId (set by CMI_OpenModule()). 
    \li Data Domain  - Pointer to the doamin name, in this case it's "SW".
    \li Data Class   - Pointer to the class name, in this case it's "CMI Message".
    \li Data Type    - Pointer to the data type, in this case it's "Msg".
    \li Data Message - Pointer to the FmtString.
    \li value - If pValue is not NULL, value is provided.

    If CMI_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::ePCMI_Error_NULL_STMHandle.
    
*/

eCMI_Error  CMI_LogMsg(CMI_Handle_Pntr pCMI_Handle, const char * FmtString, int32_t  * pValue);

#ifndef _NoMark
/*! \par CMI_MarkClockDomainStateChange

    Issue a standard Clock Doamin State Change STM message. 
    This function provides the client a mechansim to mark when a clock domain state change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the clock setting, and when the device responds to the request.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] clkDomain ::eCMI_ClockDomainStateChange specifies the enumerated clock domain that changed. If you attempt to select
               a clock domain that is not associated with the CM module the library is opened for,
               a Mark STM Message is not generated and the function exits with ::ePCMI_Error_Invalid_Parameter error. 
    \param[in] clkState ::eCMI_clkState specifies the enumerated clock domain state
    \return ::eCMI_Error
    
    \par Details:
    \details  
        
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "CM0" or "CM1". Derived from CMI_ModId (set by CMI_OpenModule()). 
    \li Data Domain  - Pointer to the clock domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Clock Doamin State Change".
    \li Data Type    - Pointer to the data type, in this case it's "State".
    \li Data Message - Pointer to the clock doamin state. Derived from the clkState parameter, "Clock gated" or "Clock running".
    
    If CMI_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::ePCMI_Error_NULL_STMHandle.  
 
    All text for these mesages are provided in the .dataImageMsgStrings:PCMILib section and at link time
    may be placed in non-existant memory.   
*/
eCMI_Error  CMI_MarkClockDomainStateChange(CMI_Handle_Pntr pCMI_Handle, eCMI_ClockDomainStateChange clkDomain, eCMI_clkState clkState);

/*! \par CMI_MarkClockDividerRatioChange

    Issue a standard Clock Divider Ratio Change STM message. 
    This function provides the client a mechansim to mark when a clock divider ratio change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the divider setting, and when the device responds to the request.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] divDomain ::eCMI_ClockDividerRatioChange specifies the emunerated clock divider that changed. If you attempt to select
               a clock divider that is not associated with the CM module the library is opened for,
               a Mark STM Message is not generated and the function exits with ::ePCMI_Error_Invalid_Parameter error. 
    \param[in] divState ::eCMI_divState specifies the emunerated clock divider value (eCMI_DivBy1, eCMI_DivBy2, eCMI_DivBy4) 
    \return ::eCMI_Error
    
    \par Details:
    \details  
        
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "CM0" or "CM1". Derived from CMI_ModId (set by CMI_OpenModule()). 
    \li Data Domain  - Pointer to the clock domain name string. Dirived from enumerated pwrDomain parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Clock Divider Ratio Change".
    \li Data Type    - Pointer to the data type, in this case it's "State".
    \li Data Message - Pointer to the clock doamin state. Derived from the divState parameter ("Divide by 1", "Divide by 2" or "Divide by 4").
    
    If CMI_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::ePCMI_Error_NULL_STMHandle.  
        
    All text for these mesages are provided in the .dataImageMsgStrings:PCMILib section and at link time
    may be placed in non-existant memory.   
*/
eCMI_Error  CMI_MarkClockDividerRatioChange(CMI_Handle_Pntr pCMI_Handle, eCMI_ClockDividerRatioChange divDomain, eCMI_divState divState);

/*! \par CMI_MarkClockSourceSelectionChange

    Issue a standard Clock Source Selection Change STM message. 
    This function provides the client a mechansim to mark when a clock source selection change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change the clock source, and when the device responds to the request.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] clkSource ::eCMI_ClockSource specifies the emunerated clock source that changed. If you attempt to select
               a clock source that is not associated with the CM module the library is opened for,
               a Mark STM Message is not generated and the function exits with a  ::ePCMI_Error_Invalid_Parameter error. 
    \return ::eCMI_Error
    
    \par Details:
    \details  
        
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "CM0" or "CM1". Derived from CMI_ModId (set by CMI_OpenModule()). 
    \li Data Domain  - Pointer to the clock domain name string. Dirived from enumerated clkSource parameter.
    \li Data Class   - Pointer to the class name, in this case it's "Clock Divider Ratio Change".
    \li Data Type    - Empty
    \li Data Message - Empty
    
    If CMI_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::ePCMI_Error_NULL_STMHandle.  
        
    All text for these mesages are provided in the .dataImageMsgStrings:PCMILib section and at link time
    may be placed in non-existant memory.   
*/
eCMI_Error  CMI_MarkClockSourceSelectionChange(CMI_Handle_Pntr pCMI_Handle, eCMI_ClockSource clkSource);

/*! \par CMI_MarkDPLLSettingChange

    Issue a standard DPLL Setting Change STM message. 
    This function provides the client a mechansim to mark when a DPLL Setting Change has occured in
    the STM data output, thus allowing latency measurements between when the application attempts to
    change a DLPP setting, and when the device responds to the request.

    \param[in] pCMI_Handle CMI Handle Pointer 
    \param[in] dlppSource ::eCMI_DPLLSrc specifies the emunerated DLPP source whose settings were changed. If you attempt to select
               a DPLL Source that is not associated with the CM module the library is opened for,
               a Mark STM Message is not generated and the function exits with a  ::ePCMI_Error_Invalid_Parameter error. 
    \return ::eCMI_Error
    
    \par Details:
    \details  
        
    The following information will be provided in the STM message:
    \li Module - Pointer to the module's name string, typically "CM0" or "CM1". Derived from CMI_ModId (set by CMI_OpenModule()). 
    \li Data Domain  - Pointer to the DPLL domain name string. Dirived from enumerated dlppSource parameter.
    \li Data Class   - Pointer to the class name, in this case it's "DLPP Setting Change".
    \li Data Type    - Empty
    \li Data Message - Empty
    
    If CMI_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::ePCMI_Error_NULL_STMHandle.  
 
    All text for these mesages are provided in the .dataImageMsgStrings:PCMILib section and at link time
    may be placed in non-existant memory.   
*/
eCMI_Error  CMI_MarkDPLLSettingChange(CMI_Handle_Pntr pCMI_Handle, eCMI_DPLLSrc dlppSource);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __CMILIBARY_H */
