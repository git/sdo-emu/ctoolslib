#ifndef __PMI_CMI_COMMON_H
#define __PMI_CMI_COMMON_H
/*
 * PCMI_Common.h
 *
 * Common Power & Clock Management Instrumentation Common API Definitions
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file PCMI_Common.h
       Common PMI and CMI Library Typedefs and definitions




*/
#ifdef __cplusplus
extern "C" {
#endif

//Using forward slashes in the relative serach path for linux compatibility

#ifdef _OMAP4430
#include "../../PMICMILib/src/PCMI_Omap4430.h"
#endif

#ifdef _STMLogging
#include "../../STMLib/include/StmLibrary.h"
#endif

#ifdef __KERNEL__        /* If being used for a kernel build, C99 def's not available */
#include <linux/types.h>
#else
#include <stdint.h>  // needed for uintN_t/intN_t (N=8,16,32,64) C99 standard types
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Common Naming Conventions
//
// PCMI_  Prefix for shared macros and functions (typically private) 
// ePCMI  Prefix for shared enumerations used by both PMI and CMI Libraries.  
//
////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////
//
// Common Typedefs
//
////////////////////////////////////////////////////////////////////////////////////////////////
/*! \par ePCMI_Error codes
    PMI/CMI Common error codes 
*/ 
typedef enum { 
    ePCMI_Success = 0,                              /*!< Function completed successfully, handle allocated */
    ePCMI_Error_Not_Compatible = -1,                /*!< Library and device module not compatible */
    ePCMI_Error_Ownership_Not_Confirmed =-2 ,       /*!< Device module ownership not confirmed, potential conflict with Debugger*/
    ePCMI_Error_Ownership_Not_Granted =-3,          /*!< Device module ownership not granted, potential conflict with Debugger */  
    ePCMI_Error_Ownership_Not_Released =-4,         /*!< Device module ownership not released */
    ePCMI_Error_ResetDone_Not_Detected =-5,         /*!< Device module reset done not detected */ 
    ePCMI_Error_Memory_Allocation =-6,              /*!< Memory allocation error */
    ePCMI_Error_Module_Enabled = -7,                /*!< Can't change module state while module enabled */
    ePCMI_Error_Invalid_Parameter = -8,             /*!< Invalid function parameter */
    ePCMI_Error_NULL_STMHandle = -9,                /*!< Invalid function call because library opened with a NULL STM Handle */
    ePCMI_Error_AlreadyOpen = -10,                  /*!< Invalid function call because library is already opened, library must be closed first */
    ePCMI_Error_MappingError = -11,                 /*!< Attempt to map module's base address failed */
    ePCMI_Error_ModuleEnableFailed = -12,           /*!< Module Enable Failed */
    ePCMI_Error_ModuleDisableFailed = -13,          /*!< Module Disable Failed */
    ePCMI_Error_InvalidHandlePointer = -14,         /*!< Invalid handle Pointer */
    ePCMI_Error_VerboseDisabled = -15,              /*!< Verbose Disabled */
    ePCMI_Error_ModuleClkStateModifyFailed = -16,   /*!< Attempt to modify the module's clock state failed */
                                                    /* ePMI_Error_STM must be the last PMI error*/
    ePMI_Error_STM = -16,                           /*!< PMI Library specific error, STM Library returned an error */
    eCMI_Error_InUseForModuleActivity = -17,        /*!< CMI Library specific error, Attempted an operation that's not legal when the module is enabled for module activity */
    eCMI_Error_InUseForEventCapture = -18,          /*!< CMI Library specific error, Attempted an operation that's not legal when the module is enabled for event capture */
                                                    /* eCMI_Error_STM must be the last CMI error*/
    eCMI_Error_STM = -18                            /*!< CMI Library specific error, STM Library returned an error */
}ePCMI_Error;


/*! \par PCMI_CallBack
    \param[in] funcName   Constant char pointer to the function name. Normally provided by the compiler using the __FUNCTION__ macro.
    \param[in]  ePCMI_Error Error returned by calling routine
*/
typedef void(*PCMI_CallBack)(const char * funcName, ePCMI_Error);
    

#ifndef _DOXYGEN_IGNORE
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
////////////////////////////////////////////////////////////////////////////////////////////////
// Since PMI and CMI libraries share source these libraries will
// also share revision numbers.
// Note - the version definitions must have the end of line immediately after the value (packaging script requirement)
#define PMICMILIB_MAJOR_VERSION        (0x1)
                                               /*!< PMI & CMI Libraries major version number - 
                                                     typically only incremented for API changes */ 
#define PMICMILIB_MINOR_VERSION        (0x3)
                                               /*!< PMI & CMI Libraries major version number -
                                                     typically only incremented for bug fixes and
                                                     inhancements that don't require API modifications */

#ifndef __linux__ 
#define NULL 0
#endif

#define PCMI_ATTR_BUFSIZE 256  // PMI/CMI MetaData buffer size

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Register Maps
//
////////////////////////////////////////////////////////////////////////////////////////////////


#define PCMI_ID_RegOff                   0x00
#define PCMI_SysConfig_RegOff            0x10
#define PCMI_Status_RegOff               0x14
#define PCMI_Configuration_RegOff        0x24
#define PCMI_EventClassFilter_RegOff     0x28
#define PCMI_TriggerControl_RegOff       0x2c
#define PCMI_SamplingWindow_RegOff       0x30
#define PCMI_RegBankSize                 0x50

typedef uint32_t REG32_TYPE;
typedef volatile uint32_t * pREG32_TYPE;

// PMI ID Register - common between PMI and CMI
/************************************************************************************
|    31:30    |   29:28   |   27:16  |   15:11   |    10:8   |     7:6    |   5:0   |
|Scheme       | reserved  |   Func   |    RTL    |   Major   |   Custom   | Minor   |
*************************************************************************************/
#define PCMI_ID_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_ID_RegOff )
#define PCMI_SCHEME_MASK     0xC0000000
#define PCMI_SCHEME_OFFSET   30
#define PCMI_GetSchemeValue(regData) ((regData & PCMI_SCHEME_MASK) >> PCMI_SCHEME_OFFSET )

#define PCMI_FUNC_MASK       0x0FFF0000
#define PCMI_FUNC_OFFSET     16
#define PCMI_GetFuncValue(regData) ((regData & PCMI_FUNC_MASK) >> PCMI_FUNC_OFFSET )

#define PCMI_RTL_MASK        0x0000F800
#define PCMI_RTK_OFFSET      11
#define PCMI_GetRTLValue(regData) ((regData & PCMI_RTL_MASK) >> PCMI_RTL_OFFSET )

#define PCMI_MAJOR_MASK      0x00000700
#define PCMI_MAJOR_OFFSET    8
#define PCMI_GetMajorValue(regData) ((regData & PCMI_MAJOR_MASK) >> PCMI_MAJOR_OFFSET )

#define PCMI_CUSTOM_MASK     0x000000C0
#define PCMI_CUSTOM_OFFSET   6
#define PCMI_GetCustomValue(regData) ((regData & PCMI_CUSTOM_MASK) >> PCMI_CUSTOM_OFFSET )

#define PCMI_MINOR_MASK      0x0000003F
#define PCMI_MINOR_OFFSET    0
#define PCMI_GetMinorValue(regData) ((regData & PCMI_MINOR_MASK) >> PCMI_MINOR_OFFSET )

// PMI System Configuration - common between PMI and CMI
/************************************************************************************
|                        31:4                  |    3:2    |    1      |     0      |
|                      Reserved                | Idle Mode | Reserved  | Soft Reset |
*************************************************************************************/
#define PCMI_SYSCNFG_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_SysConfig_RegOff )

#define PCMI_SOFTRESET_MASK     0x1
#define PCMI_SOFTRESET_OFFSET   0
#define PCMI_SOFTRESET          0x1
#define PCMI_SOFTRESET_DONE     0
#define PCMI_GetSoftResetValue(regData)  (regData & PCMI_SOFTRESET_MASK )

#define PCMI_SMARTIDLE          ( 0x2 << 2 )
#define PCMI_SMARTIDLE_WAKEUP   ( 0x3 << 2 ) 

// PMI Status Register - common between PMI and CMI
/************************************************************************************
|                        31:9                            |    8        |    7:0     |
|                    Module Status                       | Fifo Empty  | OCP Status |
*************************************************************************************/
#define PCMI_STATUS_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_Status_RegOff )

//TODO:: Need values for PMI Status Register bits

//Configuration Register - common between PMI and CMI
/************************************************************************************
|      31:30    |       29      |      28       |    27:8   |     7     |   6:0     |
|Claim Field    |Debug Override |Current Owner  | Reserved  | Event     | Reserved  |
|Read:          |               | Debug owns 0  |           | Capture   |           |
|  Avaialble 0  |               | App owns   1  |           | Enable    |           |
|  Claimed   1  |               |               |           |           |           |
|  Enabled   2  |               |               |           |           |           |
|Write:         |               |               |           |           |           |
|  Release 0    |               |               |           |           |           |
|  Claim   1    |               |               |           |           |           |
|  Enable  2    |               |               |           |           |           |
*************************************************************************************/
#define PCMI_CONFIG_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_Configuration_RegOff )
// Claim field read
#define PCMI_CLAIM_MASK    0xC0000000
#define PCMI_CLAIM_OFFSET  30
#define PCMI_GetClaimValue(regData)  (regData & PCMI_CLAIM_MASK)   
#define PCMI_AVAILABLE           (REG32_TYPE)(0x0 << PCMI_CLAIM_OFFSET)
#define PCMI_CLAIMED             (REG32_TYPE)(0x1 << PCMI_CLAIM_OFFSET)
#define PCMI_ENABLED             (REG32_TYPE)(0x2 << PCMI_CLAIM_OFFSET)

//Claim field write
#define PCMI_RELEASE_OWNERSHIP   (REG32_TYPE)(0x0 << PCMI_CLAIM_OFFSET)
#define PCMI_CLAIM_OWNERSHIP     (REG32_TYPE)(0x1 << PCMI_CLAIM_OFFSET)
#define PCMI_ENABLE_UNIT         (REG32_TYPE)(0x2 << PCMI_CLAIM_OFFSET)
#define PCMI_NOP                 (REG32_TYPE)(0x3 << PCMI_CLAIM_OFFSET) 

// Current Owner field if PCMI_CLAIMED
#define PCMI_OWNERSHIP_MASK      0x10000000
#define PCMI_OWNERSHIP_OFFSET    28
#define PCMI_APP_OWNERSHIP       (REG32_TYPE)(0x1 << PCMI_OWNERSHIP_OFFSET)
#define PCMI_DBG_OWNERSHIP       (REG32_TYPE)(0x0 << PCMI_OWNERSHIP_OFFSET)
#define PCMI_GetClaimValue(regData)  (regData & PCMI_CLAIM_MASK)
#define PCMI_GetOwnership(regData)   (regData & PCMI_OWNERSHIP_MASK)

// Event Capture Enable
#define PCMI_EVENTCAPTURE_ENABLE (0x80)
#define PCMI_EVENTCAPTURE_DISABLE (0x00)
#ifdef _CMI
#define CMI_MODULEACTVITYCOLLECTION_ENABLE (0x8000)
#endif

// Trigger Control Register - common between PMI and CMI
/************************************************************************************
|                      31:4                          |        1     |       0       |
|                    Reserved                        | Stop Capture | Start Capture |
*************************************************************************************/
#define PCMI_TRIGGERCONTROL_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_TriggerControl_RegOff )

#define PCMI_STARTCAPTURE_TRIGGER 0x1
#define PCMI_STOPCAPTURE_TRIGGER  0x2

//Sample Window Register - common between PMI and CMI
/************************************************************************************
|               31:20               |      19:16     |     15:8     |     7:0       |
|             Reserved              | Func Divide By |   Reserved   | Sample Window |
*************************************************************************************/
#define PCMI_SAMPLEWINDOW_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_SamplingWindow_RegOff )

#define PCMI_MAX_SAMPLEWINDOW    0xFF
#define PCMI_FUNCDIVIDEBY_OFFSET 16
#define PCMI_FUNCDIVIDEBY_MASK   0x000F0000
#define PCMI_SAMPLEWINDOW_MASK   0x000000FF
#define PCMI_MAX_DIVIDEBYFACTOR  ( 0xF << PCMI_FUNCDIVIDEBY_OFFSET )


// PMI/CMI Module Clock Control - common
/************************************************************************************
|               31:18               |      17:16     |     15:2     |     1:0       |
|             Reserved              | IDLEST         |   Reserved   | ModuleMode    |
|                                   | Read 0x0 - Functional         | R/W 0 - Disabled by SW
|                                   | Read 0x1 - Transition         | R/W 1 - Doamin on
|                                   | Read 0x2 - Idle               |               |
|                                   | Read 0x3 - Disabled           |               |
*************************************************************************************/
#define PCMI_MODULE_CLKCTRL_FUNC     ( 0x0<<16 )
#define PCMI_MODULE_CLKCTRL_TRAN     ( 0x1<<16 )
#define PCMI_MODULE_CLKCTRL_IDLE     ( 0x2<<16 )
#define PCMI_MODULE_CLKCTRL_DISABLED ( 0x3<<16 )
#define PCMI_MODULE_CLKCTRL_MASK     ( 0x3<<16 )

#define PCMI_MODULE_CLKCTRL_ON        (0x1)
#define PCMI_MODULE_CLKCTRL_OFF       (0x0)
#define PCMI_MODULE_CLKCTRL_MAXRETRY  (1000)
#define PCMI_MODULE_CLKCTRL_SIZE      (4)

//#ifdef _PMI
// PMI Event Class Filter Register
/************************************************************************************
|        31:4        |      3       |      2       |        1       |       0       |
|      Reserved      | Memory Power | Logic Power  | Memory Voltage | Logic Voltage |
|                    | State Change | State Change | OPP Change     | OPP Change    |
*************************************************************************************/
#define PMI_EVENTCLASSFILTER_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_EventClassFilter_RegOff )

#define PMI_LOGICVOLTAGEOPPCHANGE   0x1
#define PMI_MEMORYVOLTAGEOPPCHANGE  0x2
#define PMI_LOGICPOWERSTATECHANGE   0x4
#define PMI_MEMORYPOWERSATTECHANGE  0x8

//#endif

//#ifdef _CMI
// Event Class Filter Register (CM Events Mode)
/***********************************************************************************
|        15:4        |      3       |      2       |        1      |       0       |
|     Reserved       | Clock Source | Clock Freq   | Clock Freq    | Clock Domain  |
|                    | Selection    | Divider Ratio| Divider Ratio | State         |
|                    | Update       | 4-bit        | 8-bit         |               |
************************************************************************************/
/****************************************************************************************************
|      31:21      |      20         |     19       |     18         |       17      |       16      |
|    Reserved     | DDRPHY DPLL-CM1 | ABE DPLL-CM1 | IVA DPLL-CM1   | MPU DPLL-CM1  | Core DPLL-CM1 |
|                 | Reserverd-CM2   | Reserved CM2 | UNIPRO DPLL-CM2| USB DPLL -CM2 | PER DPLL-CM2  |
|                 | Update          | Update       | Update         | Update        | Update        |
|                 |                 |              |                |               |               |
****************************************************************************************************/
#define CMI_EVENTCLASSFILTER_REG_ADDR(ba) (uint32_t *)((uint32_t)(ba) + PCMI_EventClassFilter_RegOff )

#define CMI_EVENT_CLOCK_DOMAIN_STATE_UPDATE       0x000001
#define CMI_EVENT_CLOCK_FREQDIVIDER_8BIT_UPDATE   0x000002 //Not used in OMAP4
#define CMI_EVENT_CLOCK_FREQDIVIDER_4BIT_UPDATE   0x000004
#define CMI_EVENT_CLOCK_SOURCE_SELECTION_UPDATE   0x000008
#define CMI_EVENT_CM1_CORE_DPLL_UPDATE            0x010000
#define CMI_EVENT_CM1_MPU_DPLL_UPDATE             0x020000
#define CMI_EVENT_CM1_IVA_DPLL_UPDATE             0x040000
#define CMI_EVENT_CM1_ABE_DPLL_UPDATE             0x080000
#define CMI_EVENT_CM1_DDRPHY_DPLL_UPDATE          0x100000

#define CMI_EVENT_CM2_PER_DPLL_UPDATE             0x010000
#define CMI_EVENT_CM2_USB_DPLL_UPDATE             0x020000
#define CMI_EVENT_CM2_UNIPRO_DPLL_UPDATE          0x040000

// CMI Event Class Filter Register (Activity Mode)
/***********************************************************************************
|        31:4        |      3       |      2       |        1     |       0        |
|     Reserved       | Initiator    | Initiator    | Target       | Target         |
|                    | Activity     | Activity     | Activity     | Activity       |
|                    | 4-bit        | 8-bit        | 4-bit        | 8-bit          |
************************************************************************************/

// Note that the size of the sample window determines if 4-bit or 8-bit
// messages are generated.
#define CMI_TARGET_ACTVITY                          0x3
#define CMI_INITIATOR_ACTIVITY                      0xc

//#endif

typedef enum { ePCMI_TRIGGER_NONE = 0,                /*!< Clear the start and stop trigger bits */
               ePCMI_TRIGGER_START_ENABLE = 1,        /*!< Set start enable trigger bit */
               ePCMI_TRIGGER_STOP_ENABLE = 2,         /*!< Set stop enable trigger bit */
               ePCMI_TRIGGER_START_STOP_ENABLE = 3,
               ePCMI_TRIGGER_NOTMODIFIED = -1          /*!< Don't modify the start or stop trigger bits*/
               } ePCMI_Triggers;

/* 
    Strings and string pointers
*/

extern const char SWDomainStr[];
enum PCMI_DataType { MSG, STATE, VALUE, META};
extern const char * PCMI_DataTypeStr[];
extern const char SWMsg_WindowSizeStr[];
extern const char SWMsg_DivideByFactor[];

extern const char PCMI_ErrorStr_Success[];
extern const char PCMI_ErrorStr_Not_Compatible[];
extern const char PCMI_ErrorStr_Ownership_Not_Confirmed[];
extern const char PCMI_ErrorStr_Ownership_Not_Granted[];  
extern const char PCMI_ErrorStr_Ownership_Not_Released[];
extern const char PCMI_ErrorStr_ResetDone_Not_Detected[]; 
extern const char PCMI_ErrorStr_Memory_Allocation[];
extern const char PCMI_ErrorStr_Module_Enabled[];
extern const char PCMI_ErrorStr_Invalid_Parameter[];
extern const char PCMI_ErrorStr_NULL_STMHandle[];
extern const char PCMI_ErrorStr_AlreadyOpen[];
extern const char PCMI_ErrorStr_MappingError[];
extern const char PCMI_ErrorStr_ModuleEnableFailed[];
extern const char PCMI_ErrorStr_ModuleDisableFailed[];
extern const char PCMI_ErrorStr_InvalidHandlePointer[];
extern const char PCMI_ErrorStr_VerboseDisabled[];
extern const char PCMI_ErrorStr_ModuleClkModifyFailed[];
extern const char PCMI_ErrorStr_STM[]; 

/*
    Common Function prototypes
*/
#ifdef _STMLogging
eSTM_STATUS PCMI_STMlogMsg(STMHandle * pSTMHandle, uint8_t STMLoggingCh, int32_t STMLoggingEnable, const char * logStr);
eSTM_STATUS PCMI_STMlogMsg1(STMHandle * pSTMHandle, uint8_t STMLoggingCh, int32_t STMLoggingEnable, const char * logStr, int value);
#endif
int32_t PCMI_HWCompatibility(void * BaseAddress, int LibFuncValue);
int32_t PCMI_ClaimModule(void * BaseAddress);
extern int32_t PCMI_SoftResetModule( void * BaseAddress );
extern int32_t PCMI_ReleaseModule( void * BaseAddress );
extern int32_t PCMI_ConfirmOwnership( void * BaseAddress );
extern int32_t PCMI_EnableModule(void * BaseAddress);
extern int32_t PCMI_DisableModule(void * BaseAddress);
int32_t PCMI_ModuleClockEnable(pREG32_TYPE ModuleAddress);
int32_t PCMI_ModuleClockDisable( pREG32_TYPE ModuleAddress);
extern int32_t PCMI_Open(void * vPMI_BaseAddress, pREG32_TYPE vPMI_ClkCtrlRegAddr, const uint32_t PMI_LibFuncValue);
extern uint32_t PCMI_SetSampleWindow(void * BaseAddress, uint32_t FuncClockDivider, uint32_t SampleWindowSize );

#ifdef _UsingTITools
#ifdef _STMLogging
#pragma CODE_SECTION(PCMI_STMlogMsg,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_STMlogMsg1,  ".text:PCMILibrary");
#endif
#pragma CODE_SECTION(PCMI_HWCompatibility,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_ClaimModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_SoftResetModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_ReleaseModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_ConfirmOwnership,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_EnableModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_DisableModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_ModuleClockEnable,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_ModuleClockDisable,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_Open,  ".text:PCMILibrary");
#pragma CODE_SECTION(PCMI_SetSampleWindow,  ".text:PCMILibrary");
#endif

#endif //_DOXYGEN_IGNORE

#ifdef __cplusplus
}
#endif

#endif /* __PMI_CMI_COMMON__H */

