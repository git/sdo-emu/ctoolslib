/*
 * PMI_CMI_Omap4430.c
 *
 * Power & Clock Management Instrumentation Omap4430 Specific Implementation
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdint.h>

#ifdef _OMAP4430
#include "PCMI_Omap4430.h"
#endif
#include "StmLibrary.h"
#include "PCMI_Common.h"

#ifdef _PMI

////////////////////////////////////////////////////////////////////////////////////////////////
//
// OMAP4430 PMI Message Strings
//
// Note that all PMI Library message strings are all placed in the .dataImageMsgStrings:PMILib
// data section. This allows the user to link this data to non-existent memory if desired since
// STM transport only utilizes pointer to the data. 
//
////////////////////////////////////////////////////////////////////////////////////////////////

// Logic Voltage OPP Change Domains
#define PMI_STR_LVD_MPU         "MPU"
#define PMI_STR_LVD_IVA         "IVA"
#define PMI_STR_LVD_CORE        "CORE"

//Memory Voltage OPP Change Domains
#define PMI_STR_MVD_SRAM_MPU    "MPU SRAM"           
#define PMI_STR_MVD_SRAM_IVA    "IVA SRAM"           
#define PMI_STR_MVD_SRAM_CORE   "CORE SRAM"

//Logic Power State Change Doamins
#define PMI_STR_LPD_MPU         "MPU"             
#define PMI_STR_LPD_A9_C0       "Cortex-A9-C0"       
#define PMI_STR_LPD_A9_C1       "Cortex-A9-C1"      
#define PMI_STR_LPD_TESLA       "TESLA"       
#define PMI_STR_LPD_ABE         "ABE"      
#define PMI_STR_LPD_CORE        "CORE"      
#define PMI_STR_LPD_IVAHD       "IVAHD"       
#define PMI_STR_LPD_CAM         "CAM"      
#define PMI_STR_LPD_DSS         "DSS"      
#define PMI_STR_LPD_GFX         "GFX"       
#define PMI_STR_LPD_L3INT       "L3INT"
#define PMI_STR_LPD_LAPER       "LAPER"       
#define PMI_STR_LPD_CEFUSE      "CRFUSE"

//Memory Power State Change Doamins 
#define PMI_STR_MPD_MPU_L1          "MPU L1"
#define PMI_STR_MPD_MPU_L2          "MPU L2"
#define PMI_STR_MPD_MPU_RAM         "MPU RAM"
#define PMI_STR_MPD_MPU_CHIRON_M0   "MPU Chiron M0"
#define PMI_STR_MPD_MPU_CHIRON_M1   "MPU Chiron M1"
#define PMI_STR_MPD_TESLA_L1        "TESLA L1"    
#define PMI_STR_MPD_TESLA_L2        "TESLA L2"
#define PMI_STR_MPD_TESLA_EDMA      "TESLA EDMA"
#define PMI_STR_MPD_AUDIO_ENG       "Audio Engine"
#define PMI_STR_MPD_AUDIO_PER       "Audio PER"
#define PMI_STR_MPD_CORE_NON_RET    "Core Non Ret"
#define PMI_STR_MPD_OCMRAM          "OCMRAM"
#define PMI_STR_MPD_DUCATI_L2       "Ducati L2"
#define PMI_STR_MPD_DUCATI_UNICHACHE "Ducati Unicache"
#define PMI_STR_MPD_CORE_OTHER_BANKS "Core Other Banks"
#define PMI_STR_MPD_IVAHD_HWA        "IVA-HD HWA"
#define PMI_STR_MPD_IVAHD_SL2        "IVA-HD SL2"
#define PMI_STR_MPD_IVAHD_TCM1       "IVA-HD TCM1"
#define PMI_STR_MPD_IVAHD_TCM2       "IVA-HD TCM2"
#define PMI_STR_MPD_CAM              "CAM" 
#define PMI_STR_MPD_DSS              "DSS"
#define PMI_STR_MPD_GFX              "GFX"
#define PMI_STR_MPD_L3INT_BANK1      "L3INT Bank1"
#define PMI_STR_MPD_L4_PER1          "L4 PER1"
#define PMI_STR_MPD_L4_PER2          "L4 PER2" 

// Memory Voltage Domain (MVD) Power State Strings
#define PMI_STR_MVD_ON               "On"     
#define PMI_STR_MVD_SLEEP            "Sleep"

// Logic Power Domain (LPD) Power State Message
#define PMI_STR_LPD_OFF               "Off"
#define PMI_STR_LPD_RET               "Retention"
#define PMI_STR_LPD_INACTIVE          "Inactive"
#define PMI_STR_LPD_ON                "On"

//PMI Data Classes
#define PMI_STR_DC_LVC                 "Logic Voltage OPP Change"
#define PMI_STR_DC_MVC                 "Memory Voltage OPP Change"
#define PMI_STR_DC_LPS                 "Logic Power State Change"
#define PMI_STR_DC_MPS                 "Memory power State Change"

const struct  {
    // Logic Voltage OPP Change Domains
    const char pmi_LvdOpp_st1[sizeof(PMI_STR_LVD_MPU)];
    const char pmi_LvdOpp_st2[sizeof(PMI_STR_LVD_IVA)];
    const char pmi_LvdOPP_st3[sizeof(PMI_STR_LVD_CORE)];

    //Memory Voltage OPP Change Domains
    const char pmi_MvdOpp_st1[sizeof(PMI_STR_MVD_SRAM_MPU)];
    const char pmi_MvdOpp_st2[sizeof(PMI_STR_MVD_SRAM_IVA)];
    const char pmi_MvdOPP_st3[sizeof(PMI_STR_MVD_SRAM_CORE)];

    // Memory Voltage Domain (MVD) Power State Strings
    const char pmi_MvdOpp_st4[sizeof(PMI_STR_MVD_ON)];
    const char pmi_MvdOpp_st5[sizeof(PMI_STR_MVD_SLEEP)];

    //Logic Power State Change Doamins
    const char pmi_LpdOpp_st1[sizeof(PMI_STR_LPD_MPU)];
    const char pmi_LpdOpp_st2[sizeof(PMI_STR_LPD_A9_C0)];
    const char pmi_LpdOpp_st3[sizeof(PMI_STR_LPD_A9_C1)];
    const char pmi_LpdOpp_st4[sizeof(PMI_STR_LPD_TESLA)];
    const char pmi_LpdOpp_st5[sizeof(PMI_STR_LPD_ABE)];
    const char pmi_LpdOpp_st6[sizeof(PMI_STR_LPD_CORE)];
    const char pmi_LpdOpp_st7[sizeof(PMI_STR_LPD_IVAHD)];
    const char pmi_LpdOpp_st8[sizeof(PMI_STR_LPD_CAM)];
    const char pmi_LpdOpp_st9[sizeof(PMI_STR_LPD_DSS)];
    const char pmi_LpdOpp_st10[sizeof(PMI_STR_LPD_GFX)];
    const char pmi_LpdOpp_st11[sizeof(PMI_STR_LPD_L3INT)];
    const char pmi_LpdOpp_st12[sizeof(PMI_STR_LPD_LAPER)];
    const char pmi_LpdOpp_st13[sizeof(PMI_STR_LPD_CEFUSE)];
    
    // Logic Power Domain (LPD) Power State Message
    const char pmi_LpdOpp_st14[sizeof(PMI_STR_LPD_OFF)];
    const char pmi_LpdOpp_st15[sizeof(PMI_STR_LPD_RET)];
    const char pmi_LpdOpp_st16[sizeof(PMI_STR_LPD_INACTIVE)];
    const char pmi_LpdOpp_st17[sizeof(PMI_STR_LPD_ON)];

    //Memory Power State Change Doamins
    const char pmi_MpdOpp_st1[sizeof(PMI_STR_MPD_MPU_L1)];
    const char pmi_MpdOpp_st2[sizeof(PMI_STR_MPD_MPU_L2)];
    const char pmi_MpdOpp_st3[sizeof(PMI_STR_MPD_MPU_RAM)];
    const char pmi_MpdOpp_st4[sizeof(PMI_STR_MPD_MPU_CHIRON_M0)];
    const char pmi_MpdOpp_st5[sizeof(PMI_STR_MPD_MPU_CHIRON_M1)];
    const char pmi_MpdOpp_st6[sizeof(PMI_STR_MPD_TESLA_L1)];
    const char pmi_MpdOpp_st7[sizeof(PMI_STR_MPD_TESLA_L2)];
    const char pmi_MpdOpp_st8[sizeof(PMI_STR_MPD_TESLA_EDMA)];
    const char pmi_MpdOpp_st9[sizeof(PMI_STR_MPD_AUDIO_ENG)];
    const char pmi_MpdOpp_st10[sizeof(PMI_STR_MPD_AUDIO_PER)];
    const char pmi_MpdOpp_st11[sizeof(PMI_STR_MPD_CORE_NON_RET)];
    const char pmi_MpdOpp_st12[sizeof(PMI_STR_MPD_OCMRAM)];
    const char pmi_MpdOpp_st13[sizeof(PMI_STR_MPD_DUCATI_L2)];
    const char pmi_MpdOpp_st14[sizeof(PMI_STR_MPD_DUCATI_UNICHACHE)];
    const char pmi_MpdOpp_st15[sizeof(PMI_STR_MPD_CORE_OTHER_BANKS)];
    const char pmi_MpdOpp_st16[sizeof(PMI_STR_MPD_IVAHD_HWA)];
    const char pmi_MpdOpp_st17[sizeof(PMI_STR_MPD_IVAHD_SL2)];
    const char pmi_MpdOpp_st18[sizeof(PMI_STR_MPD_IVAHD_TCM1)];
    const char pmi_MpdOpp_st19[sizeof(PMI_STR_MPD_IVAHD_TCM2)];
    const char pmi_MpdOpp_st20[sizeof(PMI_STR_MPD_CAM)];
    const char pmi_MpdOpp_st21[sizeof(PMI_STR_MPD_DSS)];
    const char pmi_MpdOpp_st22[sizeof(PMI_STR_MPD_GFX)];
    const char pmi_MpdOpp_st23[sizeof(PMI_STR_MPD_L3INT_BANK1)];
    const char pmi_MpdOpp_st24[sizeof(PMI_STR_MPD_L4_PER1 )];
    const char pmi_MpdOpp_st25[sizeof(PMI_STR_MPD_L4_PER2)];

    //PMI Data Classes
    const char pmi_dataclass_st1[sizeof(PMI_STR_DC_LVC)];
    const char pmi_dataclass_st2[sizeof(PMI_STR_DC_MVC)];
    const char pmi_dataclass_st3[sizeof(PMI_STR_DC_LPS)];
    const char pmi_dataclass_st4[sizeof(PMI_STR_DC_MPS)]; 
 

}pmi_strings = { //Logic Voltage OPP Change Domains
                 PMI_STR_LVD_MPU,
                 PMI_STR_LVD_IVA,
                 PMI_STR_LVD_CORE,
                 //Memory Voltage OPP Change Domains
                 PMI_STR_MVD_SRAM_MPU,
                 PMI_STR_MVD_SRAM_IVA,
                 PMI_STR_MVD_SRAM_CORE,
                 // Memory Voltage Domain (MVD) Power State Strings
                 PMI_STR_MVD_ON,
                 PMI_STR_MVD_SLEEP,
                 //Logic Power State Change Doamins
                 PMI_STR_LPD_MPU,             
                 PMI_STR_LPD_A9_C0,             
                 PMI_STR_LPD_A9_C1,     
                 PMI_STR_LPD_TESLA,       
                 PMI_STR_LPD_ABE,      
                 PMI_STR_LPD_CORE,      
                 PMI_STR_LPD_IVAHD,       
                 PMI_STR_LPD_CAM,      
                 PMI_STR_LPD_DSS,      
                 PMI_STR_LPD_GFX,       
                 PMI_STR_LPD_L3INT,
                 PMI_STR_LPD_LAPER,       
                 PMI_STR_LPD_CEFUSE,
                 // Logic Power Domain (LPD) Power State Message
                 PMI_STR_LPD_OFF,
                 PMI_STR_LPD_RET,
                 PMI_STR_LPD_INACTIVE,
                 PMI_STR_LPD_ON,   
                 //Memory Power State Change Doamins
                 PMI_STR_MPD_MPU_L1,
                 PMI_STR_MPD_MPU_L2,
                 PMI_STR_MPD_MPU_RAM,
                 PMI_STR_MPD_MPU_CHIRON_M0,
                 PMI_STR_MPD_MPU_CHIRON_M1,
                 PMI_STR_MPD_TESLA_L1,
                 PMI_STR_MPD_TESLA_L2,
                 PMI_STR_MPD_TESLA_EDMA,
                 PMI_STR_MPD_AUDIO_ENG,
                 PMI_STR_MPD_AUDIO_PER,
                 PMI_STR_MPD_CORE_NON_RET,
                 PMI_STR_MPD_OCMRAM,
                 PMI_STR_MPD_DUCATI_L2,
                 PMI_STR_MPD_DUCATI_UNICHACHE,
                 PMI_STR_MPD_CORE_OTHER_BANKS,
                 PMI_STR_MPD_IVAHD_HWA,
                 PMI_STR_MPD_IVAHD_SL2,
                 PMI_STR_MPD_IVAHD_TCM1,
                 PMI_STR_MPD_IVAHD_TCM2,
                 PMI_STR_MPD_CAM,
                 PMI_STR_MPD_DSS,
                 PMI_STR_MPD_GFX,
                 PMI_STR_MPD_L3INT_BANK1,
                 PMI_STR_MPD_L4_PER1,
                 PMI_STR_MPD_L4_PER2,
                 //PMI Data Classes
                 PMI_STR_DC_LVC,
                 PMI_STR_DC_MVC,
                 PMI_STR_DC_LPS,
                 PMI_STR_DC_MPS 
                                 
                 };

const char * pPMI_LvdOppStr[] = { pmi_strings.pmi_LvdOpp_st1, pmi_strings.pmi_LvdOpp_st2, pmi_strings.pmi_LvdOPP_st3};
const char * pPMI_MvdOppStr[] = { pmi_strings.pmi_MvdOpp_st1, pmi_strings.pmi_MvdOpp_st2, pmi_strings.pmi_MvdOPP_st3};
const char * pPMI_MvdPwrStateStr[] = { pmi_strings.pmi_MvdOpp_st4, pmi_strings.pmi_MvdOpp_st5};
const char * pPMI_LpdOppStr[] = { pmi_strings.pmi_LpdOpp_st1,
                                  pmi_strings.pmi_LpdOpp_st2,
                                  pmi_strings.pmi_LpdOpp_st3,
                                  pmi_strings.pmi_LpdOpp_st4,
                                  pmi_strings.pmi_LpdOpp_st5,
                                  pmi_strings.pmi_LpdOpp_st6,
                                  pmi_strings.pmi_LpdOpp_st7,
                                  pmi_strings.pmi_LpdOpp_st8,
                                  pmi_strings.pmi_LpdOpp_st9,
                                  pmi_strings.pmi_LpdOpp_st10,
                                  pmi_strings.pmi_LpdOpp_st11,
                                  pmi_strings.pmi_LpdOpp_st12,
                                  pmi_strings.pmi_LpdOpp_st13
                                };
const char * pPMI_LpdPwrStateStr[] = {  pmi_strings.pmi_LpdOpp_st14,
                                        pmi_strings.pmi_LpdOpp_st15,
                                        pmi_strings.pmi_LpdOpp_st16,
                                        pmi_strings.pmi_LpdOpp_st17
                                  };
const char * pPMI_MpdOppStr[] = { pmi_strings.pmi_MpdOpp_st1,
                                  pmi_strings.pmi_MpdOpp_st2,
                                  pmi_strings.pmi_MpdOpp_st3,
                                  pmi_strings.pmi_MpdOpp_st4,
                                  pmi_strings.pmi_MpdOpp_st5,
                                  pmi_strings.pmi_MpdOpp_st6,
                                  pmi_strings.pmi_MpdOpp_st7,
                                  pmi_strings.pmi_MpdOpp_st8,
                                  pmi_strings.pmi_MpdOpp_st9,
                                  pmi_strings.pmi_MpdOpp_st10,
                                  pmi_strings.pmi_MpdOpp_st11,
                                  pmi_strings.pmi_MpdOpp_st12,
                                  pmi_strings.pmi_MpdOpp_st13,
                                  pmi_strings.pmi_MpdOpp_st14,
                                  pmi_strings.pmi_MpdOpp_st15,
                                  pmi_strings.pmi_MpdOpp_st16,
                                  pmi_strings.pmi_MpdOpp_st17,
                                  pmi_strings.pmi_MpdOpp_st18,
                                  pmi_strings.pmi_MpdOpp_st19,
                                  pmi_strings.pmi_MpdOpp_st20,
                                  pmi_strings.pmi_MpdOpp_st21,
                                  pmi_strings.pmi_MpdOpp_st22,
                                  pmi_strings.pmi_MpdOpp_st23,
                                  pmi_strings.pmi_MpdOpp_st24,
                                  pmi_strings.pmi_MpdOpp_st25
                                };
//Note that for OMAP4430 the Memory Power Domain State strings are
//identical to the Logic Power Domain State strings, so don't duplicate. 
const char * pPMI_MpdPwrStateStr[] = {  pmi_strings.pmi_LpdOpp_st14,
                                        pmi_strings.pmi_LpdOpp_st15,
                                        pmi_strings.pmi_LpdOpp_st16,
                                        pmi_strings.pmi_LpdOpp_st17
                                      };

const char * pPMI_DataClassStr[] = {  pmi_strings.pmi_dataclass_st1,
                                        pmi_strings.pmi_dataclass_st2,
                                        pmi_strings.pmi_dataclass_st3,
                                        pmi_strings.pmi_dataclass_st4
                                      };

const char PMI_DataValueFmtStr[] = "%d SMPS Code";
//uint32_t PMIisOpen[ePMI_PM_ModuleCnt] = {false};

#ifdef _UsingTITools
#pragma DATA_SECTION(pmi_strings, ".dataImageMsgStrings:PMILib");
#pragma DATA_SECTION(PMI_DataValueFmtStr, ".dataImageMsgStrings:PMILib");
#endif

#endif
#ifdef _CMI

// CMI Data Clases
#define CMI_STR_DC_CLKDOM_SC        "Clock Domain State Change"
#define CMI_STR_DC_CLKDIV_RC        "Clock Divider Ratio Change"
#define CMI_STR_DC_CLKSRC_SC        "Clock Source Selection Change"
#define CMI_STR_DC_DPLL_SC          "DPLL Setting Change"

//CM1 Clock Domains
#define CMI_STR_CD_MPU              "MPU DPLL"
#define CMI_STR_CD_TES              "TESLA ROOT CLK"
#define CMI_STR_CD_ABEGF            "ABE 24M GFCLK"
#define CMI_STR_CD_ABEAL            "ABE ALWON 32K"
#define CMI_STR_CD_ABESY            "ABE SYSCLK"
#define CMI_STR_CD_FUNC             "FUNC 24M GFCK"
#define CMI_STR_CD_ABEOP            "ABE OCP GICLK"
#define CMI_STR_CD_ABEX2            "ABE X2 CLK"
#define CMI_STR_CD_L4               "L4 ALWON ICLK"

//CM2 Clock Domains
#define CMI_STR_CD_L3_1             "L3 1"
#define CMI_STR_CD_L3_2             "L3 2"
#define CMI_STR_CD_DUCATI           "DUCATI"
#define CMI_STR_CD_L3_DMA           "L3 DMA"
#define CMI_STR_CD_ASYNC_PHY2       "ASYNC PHY2"
#define CMI_STR_CD_ASYNC_PHY1       "ASYNC PHY1"
#define CMI_STR_CD_ASYNC_DLL        "ASYNC DLL"
#define CMI_STR_CD_PHY_ROOT         "PHY ROOT"
#define CMI_STR_CD_DLL              "DLL"
#define CMI_STR_CD_L3_EMIF          "L3 EMIF"
#define CMI_STR_CD_L4_D2D           "L4 D2D"
#define CMI_STR_CD_L3_D2D           "L3 D2D"
#define CMI_STR_CD_L4_CONFIG        "L4 CONFIG"
#define CMI_STR_CD_IVAHD_ROOT       "IVAHD ROOT"
#define CMI_STR_CD_FDIF             "FDIF"
#define CMI_STR_CD_ISS              "ISS"
#define CMI_STR_CD_DSS_ALWON_SYS    "DSS ALWON SYS"
#define CMI_STR_CD_DSS              "DSS"
#define CMI_STR_CD_L3_DSS           "L3 DSS"
#define CMI_STR_CD_SGX              "SGX"
#define CMI_STR_CD_L3_GFX           "L3 GFX"
#define CMI_STR_CD_UTMI_ROOT        "UTMI ROOT"
#define CMI_STR_CD_INIT_HSMMC_6     "INIT_HSMMC_6"
#define CMI_STR_CD_INIT_HSMMC_2     "INIT_HSMMC_2"
#define CMI_STR_CD_INIT_HSMMC_1     "INIT_HSMMC_1"
#define CMI_STR_CD_INIT_HSI         "INIT_HSI"
#define CMI_STR_CD_USB_DPLL_HS      "USB_DPLL_HS"
#define CMI_STR_CD_USB_DPLL         "USB_DPLL"
#define CMI_STR_CD_INIT_48MC        "INIT 48MC"
#define CMI_STR_CD_INIT_48M         "INIT 48M"
#define CMI_STR_CD_INIT_96M         "INIT 96M"
#define CMI_STR_CD_EMAC_50MHZ       "EMAC 50MHZ"
#define CMI_STR_CD_INIT_L4          "INIT L4"
#define CMI_STR_CD_INIT_L3          "INIT L3"
#define CMI_STR_CD_PER_ABE_24M      "PER ABE 24M"
#define CMI_STR_CD_PER_SYS          "PER SYS"
#define CMI_STR_CD_PER_96M          "PER 96M"
#define CMI_STR_CD_PER_48M          "PER 48M"
#define CMI_STR_CD_FUNC_24MC        "FUNC 24MC"
#define CMI_STR_CD_FUNC_12M         "FUNC 12M"
#define CMI_STR_CD_PER_L4           "PER L4"
#define CMI_STR_CD_UNIPRO_DPLL      "UNIPRO DPLL"

//CMI Clock States
#define CMI_STR_CS_GATED            "Clock Gated"
#define CMI_STR_CS_ACTIVE           "Clock Active"

//CM1 Clock Divider Ratio Change
#define CMI_STR_CD_CLKSEL_OPP       "CLKSEL OPP"
#define CMI_STR_CD_BYPCLK_DPLL_MPU  "BYPCLK DPLL MPU"
#define CMI_STR_CD_BYPCLK_DPLL_IVA  "BYPCLK DPLL IVA"
#define CMI_STR_CD_ABE_AESS         "ABE AESS"

//CM2 Clock Divider Ratio Change
#define CMI_STR_CD_SCALE_FCLK       "SCALE FCLK"
#define CMI_STR_CD_CAM_FDIF         "CAM FDIF"
#define CMI_STR_CD_GFX_PER_192M     "GFX PER 192M"

//CMI Clock Divider Ration States
#define CMI_STR_CD_DIVBY            "Divide by %d"

//CM1 Clock Source Selection
#define CMI_STR_CSS_CORE_L4         "CORE L4"
#define CMI_STR_CSS_CORE_L3         "CORE L3"
#define CMI_STR_CSS_CORE            "CORE"
#define CMI_STR_CSS_DPLL_CORE       "DPLL CORE"

//CM2 Clock Source Selection
#define CMI_STR_CSS_DUCATI_ISS_ROOT "DUCATI ISS ROOT"
#define CMI_STR_CSS_USB_60MHZ       "USB 60MHZ"
#define CMI_STR_CSS_PER_DPLL_BYP    "PER DPLL BYP"
#define CMI_STR_CSS_USB_DPLL_BYP    "USB DPLL BYP"
#define CMI_STR_CSS_GFX             "GFX"
#define CMI_STR_CSS_MMC1            "MMC1"
#define CMI_STR_CSS_MMC2            "MMC2"
#define CMI_STR_CSS_HSI             "HSI"

//CM1 DPLL Settings Update
#define CMI_STR_DLPPUPDATE_CORE     "CORE DPLL"
#define CMI_STR_DLPPUPDATE_MPU      "MPU DPLL"
#define CMI_STR_DPLLUPDATE_IVA      "IVA DPLL"
#define CMI_STR_DPLLUPDATE_ABE      "ABE DPLL"
#define CMI_STR_DPLLUPDATE_DDRPHY   "DDRPHY DPLL"

//CM2 DLPP Settings Update
#define CMI_STR_DLPPUPDATE_PER      "PER DPLL"
#define CMI_STR_DLPPUPDATE_USB      "USB DPLL"
#define CMI_STR_DPLLUPDATE_UNIPRO   "UNIPRO DPLL"

const struct  {
    // CMI Data Classes
    const char cmi_dataclass_st1[sizeof(CMI_STR_DC_CLKDOM_SC)];
    const char cmi_dataclass_st2[sizeof(CMI_STR_DC_CLKDIV_RC)];
    const char cmi_dataclass_st3[sizeof(CMI_STR_DC_CLKSRC_SC)];
    const char cmi_dataclass_st4[sizeof(CMI_STR_DC_DPLL_SC)];
    
    // CMI CM1 Clock Domains 
    const char cmi_clkdomains_st1[sizeof(CMI_STR_CD_MPU)];
    const char cmi_clkdomains_st2[sizeof(CMI_STR_CD_TES)];
    const char cmi_clkdomains_st3[sizeof(CMI_STR_CD_ABEGF)];
    const char cmi_clkdomains_st4[sizeof(CMI_STR_CD_ABEAL)];
    const char cmi_clkdomains_st5[sizeof(CMI_STR_CD_ABESY)];
    const char cmi_clkdomains_st6[sizeof(CMI_STR_CD_FUNC)];
    const char cmi_clkdomains_st7[sizeof(CMI_STR_CD_ABEOP)];
    const char cmi_clkdomains_st8[sizeof(CMI_STR_CD_ABEX2)];
    const char cmi_clkdomains_st9[sizeof(CMI_STR_CD_L4)];

    // CMI CM2 Clock Domains 
    const char cmi_clkdomains_st10[sizeof(CMI_STR_CD_L3_1)];
    const char cmi_clkdomains_st11[sizeof(CMI_STR_CD_L3_2)];
    const char cmi_clkdomains_st12[sizeof(CMI_STR_CD_DUCATI)];
    const char cmi_clkdomains_st13[sizeof(CMI_STR_CD_L3_DMA)]; 
    const char cmi_clkdomains_st14[sizeof(CMI_STR_CD_ASYNC_PHY2)];
    const char cmi_clkdomains_st15[sizeof(CMI_STR_CD_ASYNC_PHY1)];
    const char cmi_clkdomains_st16[sizeof(CMI_STR_CD_ASYNC_DLL)];
    const char cmi_clkdomains_st17[sizeof(CMI_STR_CD_PHY_ROOT)];
    const char cmi_clkdomains_st18[sizeof(CMI_STR_CD_DLL)];
    const char cmi_clkdomains_st19[sizeof(CMI_STR_CD_L3_EMIF)];  
    const char cmi_clkdomains_st20[sizeof(CMI_STR_CD_L4_D2D)];
    const char cmi_clkdomains_st21[sizeof(CMI_STR_CD_L3_D2D)]; 
    const char cmi_clkdomains_st22[sizeof(CMI_STR_CD_L4_CONFIG)];
    const char cmi_clkdomains_st23[sizeof(CMI_STR_CD_IVAHD_ROOT)];
    const char cmi_clkdomains_st24[sizeof(CMI_STR_CD_FDIF)];
    const char cmi_clkdomains_st25[sizeof(CMI_STR_CD_ISS)];
    const char cmi_clkdomains_st26[sizeof(CMI_STR_CD_DSS_ALWON_SYS)];
    const char cmi_clkdomains_st27[sizeof(CMI_STR_CD_DSS)];
    const char cmi_clkdomains_st28[sizeof(CMI_STR_CD_L3_DSS)];
    const char cmi_clkdomains_st29[sizeof(CMI_STR_CD_SGX)];
    const char cmi_clkdomains_st30[sizeof(CMI_STR_CD_L3_GFX)];
    const char cmi_clkdomains_st31[sizeof(CMI_STR_CD_UTMI_ROOT)];
    const char cmi_clkdomains_st32[sizeof(CMI_STR_CD_INIT_HSMMC_6)];
    const char cmi_clkdomains_st33[sizeof(CMI_STR_CD_INIT_HSMMC_2)];
    const char cmi_clkdomains_st34[sizeof(CMI_STR_CD_INIT_HSMMC_1)];
    const char cmi_clkdomains_st35[sizeof(CMI_STR_CD_INIT_HSI)];
    const char cmi_clkdomains_st36[sizeof(CMI_STR_CD_USB_DPLL_HS)];
    const char cmi_clkdomains_st37[sizeof(CMI_STR_CD_USB_DPLL)];
    const char cmi_clkdomains_st38[sizeof(CMI_STR_CD_INIT_48MC)];
    const char cmi_clkdomains_st39[sizeof(CMI_STR_CD_INIT_48M)];
    const char cmi_clkdomains_st40[sizeof(CMI_STR_CD_INIT_96M)];
    const char cmi_clkdomains_st41[sizeof(CMI_STR_CD_EMAC_50MHZ)];
    const char cmi_clkdomains_st42[sizeof(CMI_STR_CD_INIT_L4)];
    const char cmi_clkdomains_st43[sizeof(CMI_STR_CD_INIT_L3)];
    const char cmi_clkdomains_st44[sizeof(CMI_STR_CD_PER_ABE_24M)];
    const char cmi_clkdomains_st45[sizeof(CMI_STR_CD_PER_SYS)];
    const char cmi_clkdomains_st46[sizeof(CMI_STR_CD_PER_96M)];
    const char cmi_clkdomains_st47[sizeof(CMI_STR_CD_PER_48M)];
    const char cmi_clkdomains_st48[sizeof(CMI_STR_CD_FUNC_24MC)];
    const char cmi_clkdomains_st49[sizeof(CMI_STR_CD_FUNC_12M)];
    const char cmi_clkdomains_st50[sizeof(CMI_STR_CD_PER_L4)];
    const char cmi_clkdomains_st51[sizeof(CMI_STR_CD_UNIPRO_DPLL)];

    // CMI Clock Domain States
    const char cmi_clkstates_st1[sizeof(CMI_STR_CS_GATED)];
    const char cmi_clkstates_st2[sizeof(CMI_STR_CS_ACTIVE)];

    //CM1 Clock Divider Ratio Change
    const char cmi_divratio_st1[sizeof(CMI_STR_CD_CLKSEL_OPP)];
    const char cmi_divratio_st2[sizeof(CMI_STR_CD_BYPCLK_DPLL_MPU)];
    const char cmi_divratio_st3[sizeof(CMI_STR_CD_BYPCLK_DPLL_IVA)];
    const char cmi_divratio_st4[sizeof(CMI_STR_CD_ABE_AESS)];

    //CM2 Clock Divider Ratio Change
    const char cmi_divratio_st5[sizeof(CMI_STR_CD_SCALE_FCLK)];
    const char cmi_divratio_st6[sizeof(CMI_STR_CD_CAM_FDIF)];
    const char cmi_divratio_st7[sizeof(CMI_STR_CD_GFX_PER_192M)];
    
    //Clock Divider Ratio States
    const char cmi_divratio_st8[sizeof(CMI_STR_CD_DIVBY)];
    
    //CM1 Clock Source Selection
    const char cmi_clksrcsel_st1[sizeof(CMI_STR_CSS_CORE_L4)];
    const char cmi_clksrcsel_st2[sizeof(CMI_STR_CSS_CORE_L3)];
    const char cmi_clksrcsel_st3[sizeof(CMI_STR_CSS_CORE)];
    const char cmi_clksrcsel_st4[sizeof(CMI_STR_CSS_DPLL_CORE)];

    //CM2 Clock Source Selection
    const char cmi_clksrcsel_st5[sizeof(CMI_STR_CSS_DUCATI_ISS_ROOT)];
    const char cmi_clksrcsel_st6[sizeof(CMI_STR_CSS_USB_60MHZ)];
    const char cmi_clksrcsel_st7[sizeof(CMI_STR_CSS_PER_DPLL_BYP)];
    const char cmi_clksrcsel_st8[sizeof(CMI_STR_CSS_USB_DPLL_BYP)];
    const char cmi_clksrcsel_st9[sizeof(CMI_STR_CSS_GFX)];
    const char cmi_clksrcsel_st10[sizeof(CMI_STR_CSS_MMC1)];
    const char cmi_clksrcsel_st11[sizeof(CMI_STR_CSS_MMC2)];
    const char cmi_clksrcsel_st12[sizeof(CMI_STR_CSS_HSI)];
    
    //CM1 DPLL Settings Update
    const char cmi_dlppupdate_st1[sizeof(CMI_STR_DLPPUPDATE_CORE)];
    const char cmi_dlppupdate_st2[sizeof(CMI_STR_DLPPUPDATE_MPU )];
    const char cmi_dlppupdate_st3[sizeof(CMI_STR_DPLLUPDATE_IVA )];
    const char cmi_dlppupdate_st4[sizeof(CMI_STR_DPLLUPDATE_ABE)];
    const char cmi_dlppupdate_st5[sizeof(CMI_STR_DPLLUPDATE_DDRPHY)];

    //CM2 DLPP Settings Update
    const char cmi_dlppupdate_st6[sizeof(CMI_STR_DLPPUPDATE_PER)];
    const char cmi_dlppupdate_st7[sizeof(CMI_STR_DLPPUPDATE_USB)];
    const char cmi_dlppupdate_st8[sizeof(CMI_STR_DPLLUPDATE_UNIPRO)];
    
}cmi_strings = { //Data Classes
                 CMI_STR_DC_CLKDOM_SC,
                 CMI_STR_DC_CLKDIV_RC,
                 CMI_STR_DC_CLKSRC_SC,
                 CMI_STR_DC_DPLL_SC,
                 
                 // CM1 Clock Domain Changes
                 CMI_STR_CD_MPU,
                 CMI_STR_CD_TES,
                 CMI_STR_CD_ABEGF,
                 CMI_STR_CD_ABEAL,
                 CMI_STR_CD_ABESY,
                 CMI_STR_CD_FUNC,
                 CMI_STR_CD_ABEOP,
                 CMI_STR_CD_ABEX2,
                 CMI_STR_CD_L4,
                 
                 // CM2 Clock Doamin Changes
                 CMI_STR_CD_L3_1,
                 CMI_STR_CD_L3_2,
                 CMI_STR_CD_DUCATI,
                 CMI_STR_CD_L3_DMA,
                 CMI_STR_CD_ASYNC_PHY2,
                 CMI_STR_CD_ASYNC_PHY1,
                 CMI_STR_CD_ASYNC_DLL,
                 CMI_STR_CD_PHY_ROOT,
                 CMI_STR_CD_DLL,
                 CMI_STR_CD_L3_EMIF,
                 CMI_STR_CD_L4_D2D,
                 CMI_STR_CD_L3_D2D,
                 CMI_STR_CD_L4_CONFIG,
                 CMI_STR_CD_IVAHD_ROOT,
                 CMI_STR_CD_FDIF,
                 CMI_STR_CD_ISS,
                 CMI_STR_CD_DSS_ALWON_SYS,
                 CMI_STR_CD_DSS,
                 CMI_STR_CD_L3_DSS,
                 CMI_STR_CD_SGX,
                 CMI_STR_CD_L3_GFX,
                 CMI_STR_CD_UTMI_ROOT,
                 CMI_STR_CD_INIT_HSMMC_6,
                 CMI_STR_CD_INIT_HSMMC_2,
                 CMI_STR_CD_INIT_HSMMC_1,
                 CMI_STR_CD_INIT_HSI,
                 CMI_STR_CD_USB_DPLL_HS,
                 CMI_STR_CD_USB_DPLL,
                 CMI_STR_CD_INIT_48MC,
                 CMI_STR_CD_INIT_48M,
                 CMI_STR_CD_INIT_96M,
                 CMI_STR_CD_EMAC_50MHZ,
                 CMI_STR_CD_INIT_L4,
                 CMI_STR_CD_INIT_L3,
                 CMI_STR_CD_PER_ABE_24M,
                 CMI_STR_CD_PER_SYS,
                 CMI_STR_CD_PER_96M,
                 CMI_STR_CD_PER_48M,
                 CMI_STR_CD_FUNC_24MC,
                 CMI_STR_CD_FUNC_12M,
                 CMI_STR_CD_PER_L4,
                 CMI_STR_CD_UNIPRO_DPLL,
                 
                 //Clock Domain States
                 CMI_STR_CS_GATED,
                 CMI_STR_CS_ACTIVE,

                 //CM1 Clock Divider Ratio Change
                 CMI_STR_CD_CLKSEL_OPP,
                 CMI_STR_CD_BYPCLK_DPLL_MPU,
                 CMI_STR_CD_BYPCLK_DPLL_IVA,
                 CMI_STR_CD_ABE_AESS,

                 //CM2 Clock Divider Ratio Change
                 CMI_STR_CD_SCALE_FCLK,
                 CMI_STR_CD_CAM_FDIF,
                 CMI_STR_CD_GFX_PER_192M,
                 
                 //CMI Clock Divider Ratio change state
                 CMI_STR_CD_DIVBY,  
                 
                //CM1 Clock Source Selection
                CMI_STR_CSS_CORE_L4,
                CMI_STR_CSS_CORE_L3,
                CMI_STR_CSS_CORE,
                CMI_STR_CSS_DPLL_CORE,

                //CM2 Clock Source Selection
                CMI_STR_CSS_DUCATI_ISS_ROOT,
                CMI_STR_CSS_USB_60MHZ,
                CMI_STR_CSS_PER_DPLL_BYP,
                CMI_STR_CSS_USB_DPLL_BYP,
                CMI_STR_CSS_GFX,
                CMI_STR_CSS_MMC1,
                CMI_STR_CSS_MMC2,
                CMI_STR_CSS_HSI,
                
                //CM1 DPLL Settings Update
                CMI_STR_DLPPUPDATE_CORE,
                CMI_STR_DLPPUPDATE_MPU,
                CMI_STR_DPLLUPDATE_IVA,
                CMI_STR_DPLLUPDATE_ABE,
                CMI_STR_DPLLUPDATE_DDRPHY,

                //CM2 DLPP Settings Update
                CMI_STR_DLPPUPDATE_PER,
                CMI_STR_DLPPUPDATE_USB,
                CMI_STR_DPLLUPDATE_UNIPRO
                               
                };

const char * pCMI_DataClassStr[] = {cmi_strings.cmi_dataclass_st1,
                                    cmi_strings.cmi_dataclass_st2,
                                    cmi_strings.cmi_dataclass_st3,
                                    cmi_strings.cmi_dataclass_st4 
                                    };
    
const char * pCMI_ClkDomainStr[] = { cmi_strings.cmi_clkdomains_st1,
                                     cmi_strings.cmi_clkdomains_st2,
                                     cmi_strings.cmi_clkdomains_st3,
                                     cmi_strings.cmi_clkdomains_st4,
                                     cmi_strings.cmi_clkdomains_st5,
                                     cmi_strings.cmi_clkdomains_st6,
                                     cmi_strings.cmi_clkdomains_st7,
                                     cmi_strings.cmi_clkdomains_st8,
                                     cmi_strings.cmi_clkdomains_st9,
                                     cmi_strings.cmi_clkdomains_st10,
                                     cmi_strings.cmi_clkdomains_st11,
                                     cmi_strings.cmi_clkdomains_st12,
                                     cmi_strings.cmi_clkdomains_st13,
                                     cmi_strings.cmi_clkdomains_st14,
                                     cmi_strings.cmi_clkdomains_st15,
                                     cmi_strings.cmi_clkdomains_st16,
                                     cmi_strings.cmi_clkdomains_st17,
                                     cmi_strings.cmi_clkdomains_st18,
                                     cmi_strings.cmi_clkdomains_st19,
                                     cmi_strings.cmi_clkdomains_st20,
                                     cmi_strings.cmi_clkdomains_st21,
                                     cmi_strings.cmi_clkdomains_st22,
                                     cmi_strings.cmi_clkdomains_st23,
                                     cmi_strings.cmi_clkdomains_st24,
                                     cmi_strings.cmi_clkdomains_st25,
                                     cmi_strings.cmi_clkdomains_st26,
                                     cmi_strings.cmi_clkdomains_st27,
                                     cmi_strings.cmi_clkdomains_st28,
                                     cmi_strings.cmi_clkdomains_st29,
                                     cmi_strings.cmi_clkdomains_st30,
                                     cmi_strings.cmi_clkdomains_st31,
                                     cmi_strings.cmi_clkdomains_st32,
                                     cmi_strings.cmi_clkdomains_st33,
                                     cmi_strings.cmi_clkdomains_st34,
                                     cmi_strings.cmi_clkdomains_st35,
                                     cmi_strings.cmi_clkdomains_st36,
                                     cmi_strings.cmi_clkdomains_st37,
                                     cmi_strings.cmi_clkdomains_st38,
                                     cmi_strings.cmi_clkdomains_st39,
                                     cmi_strings.cmi_clkdomains_st40,
                                     cmi_strings.cmi_clkdomains_st41,
                                     cmi_strings.cmi_clkdomains_st42,
                                     cmi_strings.cmi_clkdomains_st43,
                                     cmi_strings.cmi_clkdomains_st44,
                                     cmi_strings.cmi_clkdomains_st45,
                                     cmi_strings.cmi_clkdomains_st46,
                                     cmi_strings.cmi_clkdomains_st47,
                                     cmi_strings.cmi_clkdomains_st48,
                                     cmi_strings.cmi_clkdomains_st49,
                                     cmi_strings.cmi_clkdomains_st50,
                                     cmi_strings.cmi_clkdomains_st51
                                   
                                    };

const char * pCMI_ClkStateStr[] = { cmi_strings.cmi_clkstates_st1,
                                    cmi_strings.cmi_clkstates_st2 
                                    };

const char * pCMI_DivRatioStr[] = { cmi_strings.cmi_divratio_st1,
                                    cmi_strings.cmi_divratio_st2,
                                    cmi_strings.cmi_divratio_st3,
                                    cmi_strings.cmi_divratio_st4,
                                    cmi_strings.cmi_divratio_st5,
                                    cmi_strings.cmi_divratio_st6,
                                    cmi_strings.cmi_divratio_st7
                                    };

const char * pCMI_DivRatioStateStr[] = { cmi_strings.cmi_divratio_st8
                                    };
                                    
const char * pCMI_ClkSelectStr[] = { cmi_strings.cmi_clksrcsel_st1,
                                     cmi_strings.cmi_clksrcsel_st2,
                                     cmi_strings.cmi_clksrcsel_st3,
                                     cmi_strings.cmi_clksrcsel_st4,
                                     cmi_strings.cmi_clksrcsel_st5,
                                     cmi_strings.cmi_clksrcsel_st6,
                                     cmi_strings.cmi_clksrcsel_st7,
                                     cmi_strings.cmi_clksrcsel_st8,
                                     cmi_strings.cmi_clksrcsel_st9,
                                     cmi_strings.cmi_clksrcsel_st10,
                                     cmi_strings.cmi_clksrcsel_st11,
                                     cmi_strings.cmi_clksrcsel_st12
                                    };

const char * pCMI_DLPPSrcStr[] =  { cmi_strings.cmi_dlppupdate_st1,
                                    cmi_strings.cmi_dlppupdate_st2,
                                    cmi_strings.cmi_dlppupdate_st3,
                                    cmi_strings.cmi_dlppupdate_st4,
                                    cmi_strings.cmi_dlppupdate_st5,
                                    cmi_strings.cmi_dlppupdate_st6,
                                    cmi_strings.cmi_dlppupdate_st7,
                                    cmi_strings.cmi_dlppupdate_st8
                                  };
    
//int32_t CMIisOpen[eCMI_CM_ModuleCnt] = {false, false};

#ifdef _UsingTITools
#pragma DATA_SECTION(cmi_strings, ".dataImageMsgStrings:CMILib");
#endif

#endif


