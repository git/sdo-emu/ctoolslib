/*
 * CMILib.c
 *
 * Power Management Instrumentation Specific API Implementation
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include "CMILib.h"
#ifdef _STMLogging
    #include <stdio.h>
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
////////////////////////////////////////////////////////////////////////////////////////////////

static const uint32_t CMI_LibFuncValue = 0x5;   // This is the function level of the SW that
                                                // must match the func field of the CMI HW Module


static const uint32_t CMI_ValidHandlePattern = 0xBBBBBBBB;

#ifdef _STMLogging
static const char SWLibClassStr[] = "CMILib Message";
static const char SWUsrClassStr[] = "CMI User Message";

#ifdef _UsingTITools
#pragma DATA_SECTION(SWLibClassStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWUsrClassStr, ".dataImageMsgStrings:PCMILib");
#endif
#endif
const char CM1_ModID[] = "CM1";
const char CM2_ModID[] = "CM2";
const char * strCMI_ModID[] = {CM1_ModID, CM2_ModID};

const char SWMsg_StartActivityStr[] ="Start monitoring clock activity";
const char SWMsg_StartEventStr[] ="Start monitoring clock events";
const char SWMsg_EndActivityStr[] ="End monitoring clock activity";
const char SWMsg_EndEventStr[] ="End monitoring clock events";
const char CMI_ErrorStr_InUseForModuleActivity[] = "Module already in use for module activity monitoring";
const char CMI_ErrorStr_InUseForEventCapture[] = "Module already in use for event capture"; 

const char * CMI_ErrorStr[] = { PCMI_ErrorStr_Success,
                                PCMI_ErrorStr_Not_Compatible,
                                PCMI_ErrorStr_Ownership_Not_Confirmed,
                                PCMI_ErrorStr_Ownership_Not_Granted,
                                PCMI_ErrorStr_Ownership_Not_Released,
                                PCMI_ErrorStr_ResetDone_Not_Detected,
                                PCMI_ErrorStr_Memory_Allocation,
                                PCMI_ErrorStr_Module_Enabled,
                                PCMI_ErrorStr_Invalid_Parameter,
                                PCMI_ErrorStr_NULL_STMHandle,
                                PCMI_ErrorStr_AlreadyOpen,
                                PCMI_ErrorStr_MappingError,
                                PCMI_ErrorStr_ModuleEnableFailed,
                                PCMI_ErrorStr_ModuleDisableFailed,
                                PCMI_ErrorStr_InvalidHandlePointer,
                                PCMI_ErrorStr_VerboseDisabled,
                                PCMI_ErrorStr_ModuleClkModifyFailed,
                                CMI_ErrorStr_InUseForModuleActivity,
                                CMI_ErrorStr_InUseForEventCapture,
                                PCMI_ErrorStr_STM                        //Always last
                                };
                                
#ifdef _UsingTITools
#pragma DATA_SECTION(CM1_ModID, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(CM2_ModID, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWMsg_StartActivityStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWMsg_EndActivityStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWMsg_StartEventStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWMsg_EndEventStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(CMI_ErrorStr_InUseForModuleActivity, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(CMI_ErrorStr_InUseForEventCapture, ".dataImageErrStrings:PCMILib");
#endif

static int32_t CMIisOpen[eCMI_CM_ModuleCnt] = {false, false};

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Function's typedefs
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _STMLogging
static eCMI_Error  CMI_ExportMsg( CMI_Handle * pCMI_Handle, 
                           const char * pModuleName, const char * pDomainName,
                           const char * pDataClass, const char * pDataType,
                           const char * pDataMsg, uint32_t *  pData);
#endif

typedef struct _CMIHandle {
    void * BaseAddress;
    PCMI_CallBack pCallBack;
    eCMI_ModID CMI_ModId;
#ifdef _STMLogging
    STMHandle *pSTMHandle;
    uint8_t     STMMessageCh;
    char      *pCMI_MetaData;
    uint32_t    CMI_MetaDataByteCnt;
#endif
    pREG32_TYPE ClkCtrlRegAddr;
    uint32_t validHandle;
} CMI_Handle_t;


////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
eCMI_Error CMI_OpenModule(CMI_Handle_Pntr * ppCMI_Handle, eCMI_ModID CMI_ModId, CMI_CfgParams * pCMI_CfgParams)
{
    // This function returns a eCMI_Error type, but it calls functions that
    // return ints with the following definitions:
    // 0 - Operation complete
    // 1 - Operation not needed
    // -1 - Operation error (true 0/false -1) 
    // eSTM_STATUS types are also retunrned from some calls. So the convention is
    // use the right type, but convert to a eCMI_Error type immediatly.

    eCMI_Error retVal_CMI = ePCMI_Success;

    // Just in case this function returns with an error, initialize the handle pointer to NULL.
    * ppCMI_Handle = NULL;

    //If interface already open return error
    if ( true == CMIisOpen[CMI_ModId] )
    {
        retVal_CMI = ePCMI_Error_AlreadyOpen;
    }
    else
    {
        pREG32_TYPE vCMI_ClkCtrlRegAddr = cTools_memMap(pCMI_CfgParams->CMI_ClockCtl_BaseAddr, sizeof(REG32_TYPE));
        void * vCMI_BaseAddress = cTools_memMap( pCMI_CfgParams->CMI_Module_BaseAddr, PCMI_RegBankSize);
        if ( ( NULL == vCMI_ClkCtrlRegAddr ) || ( NULL ==  vCMI_BaseAddress ) )
        {
             // Mapping error 
             retVal_CMI = ePCMI_Error_MappingError;
        }
        else
        {
        // Call the CMI/CMI common open command       
            retVal_CMI = (eCMI_Error)PCMI_Open(vCMI_BaseAddress, vCMI_ClkCtrlRegAddr, CMI_LibFuncValue);
            if ( ePCMI_Success == retVal_CMI )
            {
                // Set the samplw window
                retVal_CMI = (eCMI_Error)PCMI_SetSampleWindow(vCMI_BaseAddress, pCMI_CfgParams->FuncClockDivider, pCMI_CfgParams->SampleWindowSize );
                if ( ePCMI_Success == retVal_CMI )
                { 
                    * ppCMI_Handle = (CMI_Handle *) cTools_memAlloc(sizeof(CMI_Handle_t));
                    if (NULL == * ppCMI_Handle) 
                    {
                        // Memory allocation failed.
                        retVal_CMI = ePCMI_Error_Memory_Allocation;
                    }                
                    else
                    {
                        CMI_Handle_t * pCMI_Hdl = (CMI_Handle_t *)* ppCMI_Handle;
                        pCMI_Hdl->BaseAddress =  vCMI_BaseAddress;
                        pCMI_Hdl->pCallBack = pCMI_CfgParams->pCMI_CallBack;
                        pCMI_Hdl->CMI_ModId = CMI_ModId;
#ifdef _STMLogging
                        pCMI_Hdl->pSTMHandle = pCMI_CfgParams->pSTMHandle;
                        pCMI_Hdl->STMMessageCh = pCMI_CfgParams->STMMessageCh;
#endif
                        pCMI_Hdl->ClkCtrlRegAddr = vCMI_ClkCtrlRegAddr;
                        pCMI_Hdl->validHandle = CMI_ValidHandlePattern;
                        
                        CMIisOpen[CMI_ModId] = true;
            
                        //Set defualt values
                        {
                            pREG32_TYPE  pCMI_EventClassFilter_Reg = CMI_EVENTCLASSFILTER_REG_ADDR(vCMI_BaseAddress);
    
                            if ( eCMI_CM1 == CMI_ModId )
                            {
                                
                                * pCMI_EventClassFilter_Reg =   CMI_EVENT_CLOCK_DOMAIN_STATE_UPDATE
                                                              | CMI_EVENT_CLOCK_FREQDIVIDER_8BIT_UPDATE 
                                                              | CMI_EVENT_CLOCK_FREQDIVIDER_4BIT_UPDATE
                                                              | CMI_EVENT_CLOCK_SOURCE_SELECTION_UPDATE
                                                              | CMI_EVENT_CM1_CORE_DPLL_UPDATE
                                                              | CMI_EVENT_CM1_MPU_DPLL_UPDATE
                                                              | CMI_EVENT_CM1_IVA_DPLL_UPDATE
                                                              | CMI_EVENT_CM1_ABE_DPLL_UPDATE
                                                              | CMI_EVENT_CM1_DDRPHY_DPLL_UPDATE;
                            }
                            else
                            {
                                * pCMI_EventClassFilter_Reg =   CMI_EVENT_CLOCK_DOMAIN_STATE_UPDATE
                                                              | CMI_EVENT_CLOCK_FREQDIVIDER_8BIT_UPDATE
                                                              | CMI_EVENT_CLOCK_FREQDIVIDER_4BIT_UPDATE
                                                              | CMI_EVENT_CLOCK_SOURCE_SELECTION_UPDATE
                                                              | CMI_EVENT_CM2_PER_DPLL_UPDATE       
                                                              | CMI_EVENT_CM2_USB_DPLL_UPDATE
                                                              | CMI_EVENT_CM2_UNIPRO_DPLL_UPDATE;
                              
                            }
                        }
#ifdef _STMLogging
                        {
                            int32_t chrCnt;
                            
                            // Malloc space for the attribute string
                            pCMI_Hdl->pCMI_MetaData = (char *)cTools_memAlloc(PCMI_ATTR_BUFSIZE);
                            if(NULL == pCMI_Hdl->pCMI_MetaData)
                            {
                                PCMI_ReleaseModule(pCMI_Hdl->BaseAddress);
                                cTools_memUnMap((void *)pCMI_Hdl->BaseAddress, PCMI_RegBankSize);
                                cTools_memUnMap((void *)pCMI_Hdl->ClkCtrlRegAddr, sizeof(REG32_TYPE));
                                CMIisOpen[CMI_ModId] = false;
                                cTools_memFree(pCMI_Hdl);
                                return ePCMI_Error_Memory_Allocation;
                            }
                            
                            //
                            // The snprintf function null terminates the string, but it is not included in the
                            //  returned length, add 1 to the chrCnt to send the \0 as part of the STM msg.
                            //
                            chrCnt = snprintf(pCMI_Hdl->pCMI_MetaData, PCMI_ATTR_BUFSIZE,
                                              "{type=CMI,mid=%d,sw=%d,df=%d}",
                                                CMI_ModId,
                                                pCMI_CfgParams->SampleWindowSize,
                                                pCMI_CfgParams->FuncClockDivider);
                            pCMI_Hdl->CMI_MetaDataByteCnt = chrCnt + 1;
                            
                            if(PCMI_ATTR_BUFSIZE < chrCnt)
                            {
                                cTools_memFree(pCMI_Hdl->pCMI_MetaData);
                                PCMI_ReleaseModule(pCMI_Hdl->BaseAddress);
                                cTools_memUnMap((void *)pCMI_Hdl->BaseAddress, PCMI_RegBankSize);
                                cTools_memUnMap((void *)pCMI_Hdl->ClkCtrlRegAddr, sizeof(REG32_TYPE));
                                CMIisOpen[CMI_ModId] = false;
                                cTools_memFree(pCMI_Hdl);
                                return eCMI_Error_STM;
                            }
                        }
#endif
                    }
                }
            }
        }
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_CfgParams->pCMI_CallBack ) )
    { 
        pCMI_CfgParams->pCMI_CallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;
}

eCMI_Error CMI_CloseModule( CMI_Handle_Pntr CMI_Handle )
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    int32_t retVal = 0;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    // Validate the handle
    if ( CMI_ValidHandlePattern != pCMI_Handle->validHandle )
    {
        retVal_CMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        
        // Confirm ownership
        retVal = PCMI_ConfirmOwnership(pCMI_Handle->BaseAddress);
        if ( 0 != retVal ) 
        {
            retVal_CMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else 
        {
            // Release the module
            retVal = PCMI_ReleaseModule(pCMI_Handle->BaseAddress);
            if ( 0 != retVal ) {
                retVal_CMI = ePCMI_Error_Ownership_Not_Released;
            }
// Normally we would wan to disable the unit when closing but this makes the 
// uinit completly inaccessible.
#if 0
            else 
            {
                retVal = PCMI_ModuleClockDisable( pCMI_Handle->vCMI_ClkCtrlRegAddr );
                if ( 0 != retVal )
                {
                    retVal_CMI = eCMI_Error_ModuleClkEnableFailed;
                }
            }
#endif      
        }
        
        // Even if releasing the module or checking ownership failed we still
        // want to clean up after ourselves.

        //Unmap the CMI Base Address
        cTools_memUnMap((void *)pCMI_Handle->BaseAddress, PCMI_RegBankSize);
        
        //Unmap the CMI Clock Control Address
        cTools_memUnMap((void *)pCMI_Handle->ClkCtrlRegAddr, sizeof(REG32_TYPE));

        CMIisOpen[pCMI_Handle->CMI_ModId] = false;

#ifdef _STM_Logging
        if(NULL != pCMI_Handle->pCMI_MetaData)
        {
            cTools_memFree(pCMI_Handle->pCMI_MetaData);
        }
#endif
        //Free the CMI Handle memory
        cTools_memFree(pCMI_Handle);
    
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;

}

eCMI_Error CMI_GetVersion(CMI_Handle_Pntr CMI_Handle, uint32_t * pLibMajorVersion, uint32_t * pLibMinorVersion, uint32_t * pSWFuncID, uint32_t * pHwFuncID)
{

    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;
    pREG32_TYPE  pCMI_ID_Reg = PCMI_ID_REG_ADDR(pCMI_Handle->BaseAddress);
    * pLibMajorVersion = PMICMILIB_MAJOR_VERSION;
    * pLibMinorVersion = PMICMILIB_MINOR_VERSION;
    * pSWFuncID = CMI_LibFuncValue;
    * pHwFuncID = PCMI_GetFuncValue(* pCMI_ID_Reg);
    return ePCMI_Success;
}

eCMI_Error  CMI_ModuleEnable (CMI_Handle_Pntr CMI_Handle, eCMI_ModEnableType modEnableType)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    int32_t retVal = 0;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;
    
    pREG32_TYPE pCMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pCMI_Handle->BaseAddress);

    if ( CMI_ValidHandlePattern != pCMI_Handle->validHandle )
    {
        retVal_CMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        // Confirm ownership of the module
        retVal = PCMI_ConfirmOwnership( pCMI_Handle->BaseAddress );
        if ( 0 != retVal )
        {
            retVal_CMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else
        {
            // Read the register and confirm the activity enable bit is not already set
            REG32_TYPE CMI_Config_Reg = * pCMI_Config_Reg;
            if (    ( ( eCMI_Event == modEnableType) && ( CMI_MODULEACTVITYCOLLECTION_ENABLE & CMI_Config_Reg ) )
                 || ( ( eCMI_Activity == modEnableType) && ( PCMI_EVENTCAPTURE_ENABLE & CMI_Config_Reg ) ) )
            {
                if ( eCMI_Event == modEnableType)
                {
                    // If enabling for event capture, but already in use for module activity
                    retVal_CMI = eCMI_Error_InUseForModuleActivity;
                }
                else
                {
                    // If enabling for module activity, but already in use for event capture
                    retVal_CMI = eCMI_Error_InUseForEventCapture;
                }
            }
            else
            {
                retVal = PCMI_EnableModule(pCMI_Handle->BaseAddress);
                if ( 1 == retVal )
                {
                    // Already enabled so just return.
                    return ePCMI_Success;
                }
                
                if ( 0 > retVal ) 
                {
                    retVal_CMI = ePCMI_Error_ModuleEnableFailed;
                }
                else
                {   
#ifdef _STMLogging
                    eSTM_STATUS retVal_STM = eSTM_SUCCESS;
                    
                    // Ready to start event capture or module activity monitoring
                    if ( NULL != pCMI_Handle->pSTMHandle ) 
                    {
                        const char * SWMsg_Start;
                        if ( eCMI_Event == modEnableType)
                        {
                            SWMsg_Start = SWMsg_StartEventStr;
                        }
                        else
                        {
                            SWMsg_Start = SWMsg_StartActivityStr;
                        }
                    
                        retVal_STM = STMExport_IntMsg(pCMI_Handle->pSTMHandle, pCMI_Handle->STMMessageCh,
                                strCMI_ModID[pCMI_Handle->CMI_ModId],
                                SWDomainStr, SWLibClassStr,
                                PCMI_DataTypeStr[MSG], SWMsg_Start, NULL);
    
                        if ( eSTM_SUCCESS == retVal_STM ) 
                        {
                            retVal_STM = STMExport_putMeta(pCMI_Handle->pSTMHandle,
                                                           pCMI_Handle->pCMI_MetaData,
                                                           pCMI_Handle->CMI_MetaDataByteCnt);
                            if(eSTM_SUCCESS != retVal_STM)
                            {
                                retVal_CMI = eCMI_Error_STM;
                            }
                        }
                    } 
#endif
                    if ( eCMI_Event == modEnableType)
                    {
                        * pCMI_Config_Reg = PCMI_NOP | PCMI_EVENTCAPTURE_ENABLE;
                    }
                    else
                    {
                        * pCMI_Config_Reg = PCMI_NOP | CMI_MODULEACTVITYCOLLECTION_ENABLE;
                    }
                }                       
            }           
        }
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;

}

eCMI_Error  CMI_ModuleDisable(CMI_Handle_Pntr CMI_Handle, int32_t retain)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    int32_t retVal = 0;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    pREG32_TYPE pCMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pCMI_Handle->BaseAddress);

    if ( CMI_ValidHandlePattern != pCMI_Handle->validHandle )
    {
        retVal_CMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        // Confirm ownership and enable the module
        retVal = PCMI_ConfirmOwnership( pCMI_Handle->BaseAddress );
        if ( 0 != retVal ) 
        {
            retVal_CMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else
        {
#ifdef _STMLogging
            // Read the configuration register to get the right message 
            REG32_TYPE CMI_Config_Reg = * pCMI_Config_Reg;
#endif
            
            //Note that this write will take care of disabling both Activity and Event messages 
            * pCMI_Config_Reg = PCMI_NOP | PCMI_EVENTCAPTURE_DISABLE;
                       
            retVal = PCMI_DisableModule(pCMI_Handle->BaseAddress);
            if ( 0 != retVal )
            {
                retVal_CMI = ePCMI_Error_ModuleDisableFailed;
            }
            else
            {
#ifdef _STMLogging
                eSTM_STATUS retVal_STM = eSTM_SUCCESS;
                
                if ( NULL != pCMI_Handle->pSTMHandle )
                {
                    const char * SWMsg_End;
                    if ( CMI_MODULEACTVITYCOLLECTION_ENABLE & CMI_Config_Reg )
                    {
                        SWMsg_End = SWMsg_EndActivityStr;
                    }
                    else
                    {
                        SWMsg_End = SWMsg_EndEventStr;
                    }

                    retVal_STM = STMExport_putMeta(pCMI_Handle->pSTMHandle,
                                                   pCMI_Handle->pCMI_MetaData,
                                                   pCMI_Handle->CMI_MetaDataByteCnt);
                    if(eSTM_SUCCESS != retVal_STM)
                    {
                        retVal_CMI = eCMI_Error_STM;
                    }

                    if ( eSTM_SUCCESS == retVal_STM ) {
                        retVal_STM = STMExport_IntMsg(pCMI_Handle->pSTMHandle, pCMI_Handle->STMMessageCh,
                                strCMI_ModID[pCMI_Handle->CMI_ModId],
                                SWDomainStr, SWLibClassStr,
                                PCMI_DataTypeStr[MSG], SWMsg_End, NULL);
                    }

                }
#endif                
 
                // If retain false then soft reset the module
                if ( false == retain ) 
                {
                    retVal = PCMI_SoftResetModule( pCMI_Handle->BaseAddress );
                    if ( 0 != retVal ) 
                    {
                        retVal_CMI = ePCMI_Error_ResetDone_Not_Detected;
                    } 
                }             
            }
        }
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;
}


eCMI_Error CMI_ConfigModule(CMI_Handle_Pntr CMI_Handle, ePCMI_Triggers triggerEnables, eCMI_EventEnables eventEnables, eCMI_ActivityEnables activityEnables)
{

    eCMI_Error retVal_CMI = ePCMI_Success;
    int32_t retVal = 0;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    pREG32_TYPE pCMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pCMI_Handle->BaseAddress);
        
    if ( ( eCMI_ENABLE_ACTIVITY_NONE != activityEnables ) && ( eCMI_ENABLE_EVENT_NONE != eventEnables ) )
    {
        retVal_CMI = ePCMI_Error_Invalid_Parameter;
    }
    else
    {
        if ( CMI_ValidHandlePattern != pCMI_Handle->validHandle )
        {
            retVal_CMI = ePCMI_Error_InvalidHandlePointer;
        }
        else
        {
            // Confirm ownership and enable the module
            retVal = PCMI_ConfirmOwnership( pCMI_Handle->BaseAddress );
            if ( 0 != retVal ) 
            {
                retVal_CMI = ePCMI_Error_Ownership_Not_Confirmed;
            }
            else
            { 
                // Check if the CMI module is enabled
                if ( PCMI_ENABLED == ( * pCMI_Config_Reg & PCMI_CLAIM_MASK ) ) 
                {
                    retVal_CMI = ePCMI_Error_Module_Enabled;
                }
                else
                {
                    pREG32_TYPE pCMI_TrigCntrl_Reg = PCMI_TRIGGERCONTROL_REG_ADDR(pCMI_Handle->BaseAddress);
                    pREG32_TYPE pCMI_EventClassFltr_Reg = CMI_EVENTCLASSFILTER_REG_ADDR(pCMI_Handle->BaseAddress);
    
                    switch ( triggerEnables ) 
                    {
                        case ePCMI_TRIGGER_NOTMODIFIED:
                            break;
                        case ePCMI_TRIGGER_NONE:
                            * pCMI_TrigCntrl_Reg = 0;
                            break;
                        case ePCMI_TRIGGER_START_ENABLE:
                            * pCMI_TrigCntrl_Reg = PCMI_STARTCAPTURE_TRIGGER;
                            break;
                        case ePCMI_TRIGGER_STOP_ENABLE:
                            * pCMI_TrigCntrl_Reg = PCMI_STOPCAPTURE_TRIGGER;
                            break;
                        case ePCMI_TRIGGER_START_ENABLE | ePCMI_TRIGGER_STOP_ENABLE:
                            * pCMI_TrigCntrl_Reg = PCMI_STOPCAPTURE_TRIGGER | PCMI_STARTCAPTURE_TRIGGER;
                            break;
                    }
                   
                    if ( ( eCMI_ENABLE_ACTIVITY_NONE == activityEnables ) && ( eCMI_ENABLE_EVENT_NONE != eventEnables ) )
                    {
                        switch ( eventEnables ) 
                        {
                            case eCMI_ENABLES_EVENT_NOT_MODIFIED:
                                break;
                            default: 
                            {
                                * pCMI_EventClassFltr_Reg = eventEnables;
                            }  
                        }
                    }
                    
                    if ( ( eCMI_ENABLE_ACTIVITY_NONE != activityEnables ) && ( eCMI_ENABLE_EVENT_NONE == eventEnables ) )
                    {
                        switch ( activityEnables ) 
                        {
                            case eCMI_ENABLES_ACTIVITY_NOT_MODIFIED:
                                break;
                            default: 
                            {
                                * pCMI_EventClassFltr_Reg = activityEnables;
                            }  
                        }
                    }
                    
                    if ( ( eCMI_ENABLE_ACTIVITY_NONE == activityEnables ) && ( eCMI_ENABLE_EVENT_NONE == eventEnables ) )
                    {
                        * pCMI_EventClassFltr_Reg = 0;                   
                    }
                    
                }
            } 
        }  
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;

}

eCMI_Error CMI_GetSampleWindow(CMI_Handle_Pntr CMI_Handle, uint32_t * pFuncClockDivider, uint32_t * pSampleWindowSize )
{
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;
    pREG32_TYPE pCMI_SampleWin_Reg = PCMI_SAMPLEWINDOW_REG_ADDR(pCMI_Handle->BaseAddress );

    REG32_TYPE CMI_SampleWin_Reg = * pCMI_SampleWin_Reg;

    * pFuncClockDivider = (((CMI_SampleWin_Reg & PCMI_FUNCDIVIDEBY_MASK ) >>  PCMI_FUNCDIVIDEBY_OFFSET )+1);
    
    * pSampleWindowSize = ( ( CMI_SampleWin_Reg & PCMI_SAMPLEWINDOW_MASK )+1 );

    return ePCMI_Success;
}   

const char *  CMI_GetErrorMsg(eCMI_Error errorCode)
{   
    // Since our convention is STM error codes simply extend CMI error codes from eCMI_Error_STM
    // simply return eCMI_Error_STM if the code is greater than eCMI_Error_STM.
    if ( errorCode < eCMI_Error_STM ) {
        errorCode = eCMI_Error_STM;
    }

    return CMI_ErrorStr[abs(errorCode)];

}

#ifdef _STMLogging
eCMI_Error  CMI_LogMsg(CMI_Handle_Pntr CMI_Handle, const char * FmtString, int32_t  * pValue)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    retVal_CMI = CMI_ExportMsg( CMI_Handle, 
                               strCMI_ModID[pCMI_Handle->CMI_ModId], SWDomainStr,
                               SWUsrClassStr, PCMI_DataTypeStr[MSG],
                               FmtString, (uint32_t *) pValue);

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;

}
#ifndef _NoMark
eCMI_Error  CMI_MarkClockDomainStateChange(CMI_Handle_Pntr CMI_Handle, eCMI_ClockDomainStateChange clkDomain, eCMI_clkState clkState)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    if ( eCMI_CM1 == pCMI_Handle->CMI_ModId )
    {
        // If NOT in the valid range of parameters for CM1 exit with an error
        if ( !( ( eCM1_CDSC_firstElement <= clkDomain ) && ( eCM1_CDSC_lastElement >= clkDomain) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        }
    }
    else 
    {
        // If NOT in the valid range of parameters for CM2 exit with an error
        if ( !( ( eCM2_CDSC_firstElement <= clkDomain ) && ( eCM2_CDSC_lastElement >= clkDomain) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        } 
    }

    if ( ePCMI_Success == retVal_CMI )
    {

        retVal_CMI = CMI_ExportMsg( CMI_Handle, 
                                   strCMI_ModID[pCMI_Handle->CMI_ModId],
                                   // pCMI_ClkDomainStr[clkDomain],
                                   pCMI_ClkDomainStr[clkDomain],
                                   pCMI_DataClassStr[eCMI_CLOCK_DOMAIN_STATE_CHANGE], 
                                   PCMI_DataTypeStr[STATE], 
                                   pCMI_ClkStateStr[clkState], NULL);  
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;
}

eCMI_Error  CMI_MarkClockDividerRatioChange(CMI_Handle_Pntr CMI_Handle, eCMI_ClockDividerRatioChange divDomain, eCMI_divState divState)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    if ( eCMI_CM1 == pCMI_Handle->CMI_ModId )
    {
        if ( !(( eCM1_CDRC_firstElement <= divDomain ) && ( eCM1_CDRC_lastElement >= divDomain) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        }
    }
    else 
    {
        if ( !( ( eCM2_CDRC_firstElement <= divDomain ) && ( eCM2_CDRC_lastElement >= divDomain) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        } 
    } 

    if ( ePCMI_Success == retVal_CMI )
    {

        retVal_CMI = CMI_ExportMsg( CMI_Handle, 
                                   strCMI_ModID[pCMI_Handle->CMI_ModId],
                                   pCMI_DivRatioStr[divDomain],
                                   pCMI_DataClassStr[eCMI_CLOCK_DIVIDER_RATIO_CHANGE], 
                                   PCMI_DataTypeStr[STATE], 
                                   pCMI_DivRatioStateStr[0], (uint32_t *)&divState);  
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;
}

eCMI_Error  CMI_MarkClockSourceSelectionChange(CMI_Handle_Pntr CMI_Handle, eCMI_ClockSource clkSource)
{
    eCMI_Error retVal_CMI = ePCMI_Success;
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    if ( eCMI_CM1 == pCMI_Handle->CMI_ModId )
    {
        if ( !( ( eCM1_CSSC_firstElement <= clkSource ) && ( eCM1_CSSC_lastElement >= clkSource) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        }
    }
    else 
    {
        if ( !( ( eCM2_CSSC_firstElement <= clkSource ) && ( eCM2_CSSC_lastElement >= clkSource) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        } 
    }

    if ( ePCMI_Success == retVal_CMI )
    {
        retVal_CMI = CMI_ExportMsg( CMI_Handle, 
                                   strCMI_ModID[pCMI_Handle->CMI_ModId],
                                   pCMI_ClkSelectStr[clkSource],
                                   pCMI_DataClassStr[eCMI_CLOCK_SOURCE_SELECTION_CHANGE], 
                                   PCMI_DataTypeStr[STATE], 
                                   pCMI_ClkSelectStr[clkSource], NULL);  
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;
   
}

eCMI_Error  CMI_MarkDPLLSettingChange(CMI_Handle_Pntr CMI_Handle, eCMI_DPLLSrc dpllSource)
{
   eCMI_Error retVal_CMI = ePCMI_Success;
   CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;

    if ( eCMI_CM1 == pCMI_Handle->CMI_ModId )
    {
        if ( !( ( eCM1_DLPPSRC_firstElement <= dpllSource ) && ( eCM1_DLPPSRC_lastElement >= dpllSource) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        }
    }
    else 
    {
        if ( !( ( eCM2_DLPPSRC_firstElement <= dpllSource) && ( eCM2_DLPPSRC_lastElement >= dpllSource) ) )
        {
            retVal_CMI = ePCMI_Error_Invalid_Parameter;
        } 
    }

    if ( ePCMI_Success == retVal_CMI )
    {
        retVal_CMI = CMI_ExportMsg( CMI_Handle, 
                                   strCMI_ModID[pCMI_Handle->CMI_ModId],
                                   pCMI_DLPPSrcStr[dpllSource],
                                   pCMI_DataClassStr[eCMI_DLPP_SETTINGS_CHANGE], 
                                   PCMI_DataTypeStr[STATE], 
                                   pCMI_DLPPSrcStr[dpllSource], NULL);  
    }

    if ( ( ePCMI_Success != retVal_CMI ) && ( NULL != pCMI_Handle->pCallBack ) )
    { 
        pCMI_Handle->pCallBack(__FUNCTION__,retVal_CMI);
    }

    return retVal_CMI;    
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////

static eCMI_Error  CMI_ExportMsg( CMI_Handle * CMI_Handle, 
                           const char * pModuleName, const char * pDomainName,
                           const char * pDataClass, const char * pDataType,
                           const char * pDataMsg, uint32_t *  pData)
{
    CMI_Handle_t * pCMI_Handle = (CMI_Handle_t *)CMI_Handle;
    eCMI_Error retVal_CMI = ePCMI_Success;

    if ( CMI_ValidHandlePattern != pCMI_Handle->validHandle ) 
    { 
        retVal_CMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        if ( NULL == pCMI_Handle->pSTMHandle ) 
        {
            retVal_CMI = ePCMI_Error_NULL_STMHandle;
        }
        else 
        {

            eSTM_STATUS retVal_STM = STMExport_IntMsg(pCMI_Handle->pSTMHandle, pCMI_Handle->STMMessageCh,
                                                      pModuleName, pDomainName,
                                                      pDataClass, pDataType, 
                                                      pDataMsg, pData);
            if ( eSTM_SUCCESS == retVal_STM ) 
            {
                return ePCMI_Success;
            }
            else {
                retVal_CMI = (eCMI_Error)((int32_t)eCMI_Error_STM + (int32_t)retVal_STM);
            }
        }
    }  

    return retVal_CMI;

}
#endif //#ifdef _STMLogging

#ifdef _UsingTITools
#pragma CODE_SECTION(CMI_OpenModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_CloseModule, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_GetVersion,  ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_ModuleEnable, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_ModuleDisable, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_ConfigModule, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_GetSampleWindow, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_GetErrorMsg, ".text:PCMILibrary");
#ifdef _STMLogging
#pragma CODE_SECTION(CMI_LogMsg, ".text:PCMILibrary");
#ifndef _NoMark
#pragma CODE_SECTION(CMI_MarkClockDomainStateChange, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_MarkClockDividerRatioChange, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_MarkClockSourceSelectionChange, ".text:PCMILibrary");
#pragma CODE_SECTION(CMI_MarkDPLLSettingChange, ".text:PCMILibrary");
#endif
#pragma CODE_SECTION(CMI_ExportMsg, ".text:PCMILibrary");
#endif
#endif
