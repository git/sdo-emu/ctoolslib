/*
 * PMI_CMI_Common.c
 *
 * Common Power & Clock Management Instrumentation Common API Implementation
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


////////////////////////////////////////////////////////////////////////////////////////////////
//
// Commmon PMI/CMI Error Strings
//
// Note that all PMI Library error strings are all placed in the .dataImageErrStrings:PCMILib
// data section. This allows the user to link this data to non-existent memory if desired since
// STM transport only utilizes pointer to the data. 
//
// If your application uses the PMI_GetErrorMsg() function then the .dataImageErrStrings:PMILib
// dta section must be linked to real memory.
//
////////////////////////////////////////////////////////////////////////////////////////////////

#include "PCMI_Common.h"

const char PCMI_ErrorStr_Success[] = "Function completed successfully";
const char PCMI_ErrorStr_Not_Compatible[] = "This Library and MPI module not compatible";
const char PCMI_ErrorStr_Ownership_Not_Confirmed[] = "HW module ownership not confirmed, potential conflict with Debugger";
const char PCMI_ErrorStr_Ownership_Not_Granted[] = "HW ownership not granted, potential conflict with Debugger";  
const char PCMI_ErrorStr_Ownership_Not_Released[] = "HW ownership not released";
const char PCMI_ErrorStr_ResetDone_Not_Detected[] = "HW reset done not detected"; 
const char PCMI_ErrorStr_Memory_Allocation[] = "Memory allocation error ";
const char PCMI_ErrorStr_Module_Enabled[] = "Can't change hw state while module enabled";
const char PCMI_ErrorStr_Invalid_Parameter[] = "Invalid function parameter";
const char PCMI_ErrorStr_NULL_STMHandle[] = "Invalid function call because library opened with a NULL STM Handle";
const char PCMI_ErrorStr_AlreadyOpen[] = "PCMI_OpenModule() can not be called if already opened, PCMI_CloseModule() must be called first";
const char PCMI_ErrorStr_MappingError[] = "Attempt to map module base address failed";
const char PCMI_ErrorStr_ModuleEnableFailed[] = "Module Enable Failed";
const char PCMI_ErrorStr_ModuleDisableFailed[] = "Module Disable Failed";
const char PCMI_ErrorStr_InvalidHandlePointer[] = "Invalid handle pointer";
const char PCMI_ErrorStr_VerboseDisabled[] = "Verbose disabled, need to build with _VERBOSE defined";
const char PCMI_ErrorStr_ModuleClkModifyFailed[] = "Attempt to enable module's clock failed";
const char PCMI_ErrorStr_STM[] = "STMLibrary error";

#ifdef _UsingTITools
#pragma DATA_SECTION(PCMI_ErrorStr_Success, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Not_Compatible, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Ownership_Not_Confirmed, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Ownership_Not_Granted, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Ownership_Not_Released, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_ResetDone_Not_Detected, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Memory_Allocation, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Module_Enabled, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_Invalid_Parameter, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_NULL_STMHandle, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_AlreadyOpen, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_MappingError, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_ModuleEnableFailed, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_ModuleDisableFailed, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_InvalidHandlePointer, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_VerboseDisabled, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_ModuleClkModifyFailed, ".dataImageErrStrings:PCMILib");
#pragma DATA_SECTION(PCMI_ErrorStr_STM, ".dataImageErrStrings:PCMILib");
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Commmon Message Strings
//
// Note that all PMI and CMI Library message strings are all placed in the .dataImageMsgStrings:PCMILib
// data section. This allows the user to link this data to non-existent memory if desired since
// STM transport only utilizes a pointer to the data.
//
////////////////////////////////////////////////////////////////////////////////////////////////
const char SWDomainStr[]= "SW";
const char PCMI_DataTypeStr_Msg[] = "Message";
const char PCMI_DataTypeStr_State[] = "State";
const char PCMI_DataTypeStr_Value[] = "Value";
const char PCMI_DataTypeStr_Meta[] = "Meta";

const char * PCMI_DataTypeStr[] = { PCMI_DataTypeStr_Msg,
                                    PCMI_DataTypeStr_State,
                                    PCMI_DataTypeStr_Value,
                                    PCMI_DataTypeStr_Meta
                                  };

const char SWMsg_WindowSizeStr[] = "Sample window size";
const char SWMsg_DivideByFactor[] = "Sample window divide by factor";

#ifdef _UsingTITools
#pragma DATA_SECTION(SWDomainStr, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(PCMI_DataTypeStr_Msg, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(PCMI_DataTypeStr_State, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(PCMI_DataTypeStr_Value, ".dataImageMsgStrings:PCMILib");
#pragma DATA_SECTION(SWMsg_WindowSizeStr, ".dataImageMsgStrings:PCMILib");
#endif
 
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Commmon Internal Function 
//
// Functions include:
//  - STM Logging functions
//  - Hardware support functions 
//
////////////////////////////////////////////////////////////////////////////////////////////////

int32_t PCMI_ModuleClockEnable( pREG32_TYPE ModuleAddress)
{
    REG32_TYPE  Module_ClkCtrl_Reg = *ModuleAddress;
    uint32_t waitCnt = 0;
    
    //Check if module already functional
    if ( ( PCMI_MODULE_CLKCTRL_FUNC | PCMI_MODULE_CLKCTRL_ON ) == Module_ClkCtrl_Reg )
    {
        return 0;
    }

    //If the module is in transition wait for it to complete
    while ( ( PCMI_MODULE_CLKCTRL_MASK  & Module_ClkCtrl_Reg ) == PCMI_MODULE_CLKCTRL_TRAN )
    {
        Module_ClkCtrl_Reg = *ModuleAddress;
        
        //Could have been transitioning to enable so check if functional
        if ( ( PCMI_MODULE_CLKCTRL_FUNC | PCMI_MODULE_CLKCTRL_ON ) == Module_ClkCtrl_Reg )
        {
            return 0;
        }
        
        // Don't want to wait forever
        if ( ( waitCnt++ ) > PCMI_MODULE_CLKCTRL_MAXRETRY ) 
        {
            return -1;
        }   
    }
    
    //Turn Domain on
    *ModuleAddress = PCMI_MODULE_CLKCTRL_ON;
    
    //Last check for func
    {
        int retryCnt = PCMI_MODULE_CLKCTRL_MAXRETRY;
        
        Module_ClkCtrl_Reg = * ModuleAddress;
        while ( !(( PCMI_MODULE_CLKCTRL_FUNC | PCMI_MODULE_CLKCTRL_ON ) == Module_ClkCtrl_Reg ))
        {
            if ( 0 == retryCnt--)
            {                
                return -1;
            }
            Module_ClkCtrl_Reg = * ModuleAddress;
        }
    }

    return 0;

}

int32_t PCMI_ModuleClockDisable( pREG32_TYPE ModuleAddress)
{
    REG32_TYPE  Module_ClkCtrl_Reg = *ModuleAddress;
    
    //Check if module not functional then exit
    if ( ( PCMI_MODULE_CLKCTRL_FUNC | PCMI_MODULE_CLKCTRL_ON ) != Module_ClkCtrl_Reg )
    {
        return 0;
    }
    
    //Turn Domain off
    *ModuleAddress = PCMI_MODULE_CLKCTRL_OFF;
    
    //Check domain is off
    Module_ClkCtrl_Reg = * ModuleAddress;
    if ( PCMI_MODULE_CLKCTRL_DISABLED == Module_ClkCtrl_Reg )
    {
        return 0;
    }

    return  -1;

}

int32_t PCMI_HWCompatibility(void * BaseAddress, int32_t LibFuncValue)
{
    pREG32_TYPE  pPMI_ID_Reg = PCMI_ID_REG_ADDR(BaseAddress);

    if ( PCMI_GetFuncValue(* pPMI_ID_Reg) != LibFuncValue ) {
        return -1;
    }
    return 0;
}

int32_t PCMI_ClaimModule(void * BaseAddress)
{
    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);
    REG32_TYPE PMI_Config_Reg = * pPMI_Config_Reg & (PCMI_CLAIM_MASK | PCMI_OWNERSHIP_MASK);

    // If already claimed and owned by the application then just exit
    if ( ( PCMI_CLAIMED | PCMI_APP_OWNERSHIP )  == PMI_Config_Reg ) {
        return 0;
    }

    //If the application is still enabled (problem from the last time we ran) then simply release
    if ( ( PCMI_ENABLED | PCMI_APP_OWNERSHIP )  == PMI_Config_Reg ) {
       * pPMI_Config_Reg = PCMI_RELEASE_OWNERSHIP;   
    }

    if ( PCMI_AVAILABLE  == ( PCMI_GetClaimValue(* pPMI_Config_Reg) & PCMI_CLAIM_MASK ) ) {

        //Set claim , clear enable
        * pPMI_Config_Reg = PCMI_CLAIM_OWNERSHIP;

        //Check Claim & Application ownership granted
        //Note: that only way PMI_CLAIMED can be set and Application ownership not granted
        // is if the Debugger requested ownership at the same time and it won the race.
        // In that case treat it just like the Application was not granted ownership.
        if ( ( PCMI_CLAIMED | PCMI_APP_OWNERSHIP )  == (* pPMI_Config_Reg & (PCMI_CLAIM_MASK | PCMI_OWNERSHIP_MASK) ) ) {
            return 0;
        }
    }
    return -1;
}

int32_t PCMI_EnableModule(void * BaseAddress)
{
    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);

    uint32_t ClaimValue = PCMI_GetClaimValue(* pPMI_Config_Reg) & (PCMI_CLAIM_MASK | PCMI_OWNERSHIP_MASK);

    //If already enabled then return
    if ( PCMI_ENABLED == ClaimValue ) {
        return 1;
    }

    if ( PCMI_CLAIMED  == ClaimValue ) {

        //Enable unit
        * pPMI_Config_Reg = PCMI_ENABLE_UNIT;

        //Check Enable state granted
        if ( ( PCMI_ENABLED )  == (* pPMI_Config_Reg & PCMI_CLAIM_MASK ) ) {
            return 0;
        }
    }
    return -1;
}

int32_t PCMI_DisableModule(void * BaseAddress)
{
    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);

    uint32_t ClaimValue = * pPMI_Config_Reg & (PCMI_CLAIM_MASK | PCMI_OWNERSHIP_MASK);

    //If already disabled then return
    if ( ( PCMI_CLAIMED | PCMI_APP_OWNERSHIP)  == ClaimValue ) {
        return 0;
    }

    if ( ( PCMI_ENABLED | PCMI_APP_OWNERSHIP )  == ClaimValue ) {

        //Disable unit by requesting claim state
        * pPMI_Config_Reg = PCMI_CLAIM_OWNERSHIP;

        //Check Enable state granted
        if ( ( PCMI_CLAIMED )  == (* pPMI_Config_Reg & PCMI_CLAIM_MASK ) ) {
            return 0;
        }
    }
    return -1;
}

int32_t PCMI_SoftResetModule( void * BaseAddress )
{
    pREG32_TYPE pPMI_SysConfig_Reg = PCMI_SYSCNFG_REG_ADDR(BaseAddress);

    //Set soft reset, but leave module in Smart-idle mode (reset state)
    * pPMI_SysConfig_Reg = PCMI_SMARTIDLE | PCMI_SOFTRESET;

    //If hw module does not acknowledge the reset is done then exit with error 
    {
        int retryCnt = 10;
        
        while ( PCMI_SOFTRESET_DONE != PCMI_GetSoftResetValue(* pPMI_SysConfig_Reg) ) {
            if ( 0 == retryCnt--) {
                return -1;
            }
        }
    }

    return 0;
}

int32_t PCMI_ReleaseModule( void * BaseAddress )
{

    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);
 
    //Release ownership
    * pPMI_Config_Reg = PCMI_RELEASE_OWNERSHIP;
   
    //Check that ownership released and all is well
    if ( PCMI_AVAILABLE  == PCMI_GetClaimValue(* pPMI_Config_Reg) ) {
        return 0;
    } 

    return -1;
}


int32_t PCMI_ConfirmOwnership( void * BaseAddress )
{

    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);

    //Check for ownership
    if ( PCMI_APP_OWNERSHIP  == ( PCMI_GetOwnership(* pPMI_Config_Reg) & PCMI_OWNERSHIP_MASK ) ) {  
        return 0;
    }

    return -1;

}

int32_t PCMI_Open(void * vPMI_BaseAddress, pREG32_TYPE vPMI_ClkCtrlRegAddr, const uint32_t PMI_LibFuncValue)
{
    int32_t retVal = 0;

    retVal = PCMI_ModuleClockEnable(vPMI_ClkCtrlRegAddr);
    if ( -1 == retVal )
    {
        retVal =  ePCMI_Error_ModuleClkStateModifyFailed;   
    }
    else
    {
        //Check for Hardware Compatibility
        retVal = PCMI_HWCompatibility(vPMI_BaseAddress, PMI_LibFuncValue);
        if ( 0 != retVal )
        {
            // Functional compatibility test failed
            retVal = ePCMI_Error_Not_Compatible;
        }
        else 
        {
            // Attempt to claim the PMI hw module
            retVal = PCMI_ClaimModule( vPMI_BaseAddress );
            if ( 0 != retVal ) 
            {
                // Claiming ownership failed
                retVal = ePCMI_Error_Ownership_Not_Granted;
            }
            else 
            {
                //Attempt to soft reset the PMI hw module
                retVal = PCMI_SoftResetModule( vPMI_BaseAddress );
                if ( 0 != retVal ) 
                {
                    // Soft reset failed.
                    // If ownership still ok then release the module
                    retVal = PCMI_ConfirmOwnership( vPMI_BaseAddress );
                    if ( 0 == retVal ) {
                        PCMI_ReleaseModule( vPMI_BaseAddress );
                    }
                    retVal = ePCMI_Error_ResetDone_Not_Detected;
    
                }
            }
        }    
    }          
   
    return retVal;
}

uint32_t PCMI_SetSampleWindow(void * BaseAddress, uint32_t FuncClockDivider, uint32_t SampleWindowSize )
{
    int32_t retVal = 0;
    pREG32_TYPE Config_Reg = PCMI_CONFIG_REG_ADDR(BaseAddress);
    
    // Confirm ownership and enable the module
    retVal = PCMI_ConfirmOwnership( BaseAddress );
    if ( 0 != retVal )
    {
        retVal = ePCMI_Error_Ownership_Not_Confirmed;
    }
    else
    {
        // Check if the PMI module is enabled
        if ( PCMI_ENABLED == ( * Config_Reg & PCMI_CLAIM_MASK ) ) 
        {
            retVal = ePCMI_Error_Module_Enabled;
        }
        else
        {
            if (   ( FuncClockDivider < 1 ) || ( FuncClockDivider > 16)
                || ( SampleWindowSize < 1 ) || ( SampleWindowSize > 256 ) )
            {
                retVal = ePCMI_Error_Invalid_Parameter;
            }
            else
            {
                pREG32_TYPE pPMI_SampleWin_Reg = PCMI_SAMPLEWINDOW_REG_ADDR(BaseAddress );
                * pPMI_SampleWin_Reg = ((FuncClockDivider-1) << PCMI_FUNCDIVIDEBY_OFFSET) |  (SampleWindowSize-1);
            } 
        }
    } 
    
    return retVal;
}
