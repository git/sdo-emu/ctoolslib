/*
 * PMILib.c
 *
 * Power Management Instrumentation Specific API Implementation
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include "PMILib.h"
#ifdef _STMLogging
    #include <stdio.h>
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
////////////////////////////////////////////////////////////////////////////////////////////////

static const uint32_t PMI_LibFuncValue = 0x5;   // This is the function level of the SW that
                                                // must match the func field of the PMI HW Module

static const uint32_t PMI_ValidHandlePattern = 0xAAAAAAAA;

#ifdef _STMLogging
    static const char SWLibClassStr[] = "PMILib Message";
    static const char SWUsrClassStr[] = "PMI User Message";

    #ifdef _UsingTITools
        #pragma DATA_SECTION(SWLibClassStr, ".dataImageMsgStrings:PCMILib");
        #pragma DATA_SECTION(SWUsrClassStr, ".dataImageMsgStrings:PCMILib");
    #endif
#endif

static uint32_t PMIisOpen[ePMI_PM_ModuleCnt] = {false};

#ifdef _STMLogging
    static const char PMI_Name[] = "PM";
    static const char * strPMI_ModID[] = {PMI_Name};
    #ifdef _UsingTITools
        #pragma DATA_SECTION(PMI_Name, ".dataImageMsgStrings:PCMILib");
    #endif
#endif

const char SWMsg_StartMonitoringStr[] = "Start monitoring module activity";
const char SWMsg_EndMonitoringStr[] = "End monitoring module activity";

#ifdef _UsingTITools
    #pragma DATA_SECTION(SWMsg_StartMonitoringStr, ".dataImageMsgStrings:PCMILib");
    #pragma DATA_SECTION(SWMsg_EndMonitoringStr, ".dataImageMsgStrings:PCMILib");
#endif

const char * PMI_ErrorStr[] = { PCMI_ErrorStr_Success,
                                PCMI_ErrorStr_Not_Compatible,
                                PCMI_ErrorStr_Ownership_Not_Confirmed,
                                PCMI_ErrorStr_Ownership_Not_Granted,
                                PCMI_ErrorStr_Ownership_Not_Released,
                                PCMI_ErrorStr_ResetDone_Not_Detected,
                                PCMI_ErrorStr_Memory_Allocation,
                                PCMI_ErrorStr_Module_Enabled,
                                PCMI_ErrorStr_Invalid_Parameter,
                                PCMI_ErrorStr_NULL_STMHandle,
                                PCMI_ErrorStr_AlreadyOpen,
                                PCMI_ErrorStr_MappingError,
                                PCMI_ErrorStr_ModuleEnableFailed,
                                PCMI_ErrorStr_ModuleDisableFailed,
                                PCMI_ErrorStr_InvalidHandlePointer,
                                PCMI_ErrorStr_VerboseDisabled,
                                PCMI_ErrorStr_ModuleClkModifyFailed,
                                PCMI_ErrorStr_STM                        //Always last
                                };             
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Function's typedefs
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _STMLogging
static ePMI_Error  PMI_ExportMsg( PMI_Handle * pPMCMI_Handle, 
                           const char * pModuleName, const char * pDomainName,
                           const char * pDataClass, const char * pDataType,
                           const char * pDataMsg, uint32_t *  pData);
#endif

typedef struct _PMI_Handle {
    void * BaseAddress;
    PCMI_CallBack pCallBack; 
    ePMI_ModID PMI_ModId;
#ifdef _STMLogging
    STMHandle *pSTMHandle;
    uint8_t     STMMessageCh;
    char      *pPMI_MetaData;
    uint32_t    PMI_MetaDataByteCnt;
#endif
    pREG32_TYPE ClkCtrlRegAddr;
    uint32_t validHandle;
}PMI_Handle_t;

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
ePMI_Error PMI_OpenModule(PMI_Handle_Pntr * ppPMI_Handle, ePMI_ModID PMI_ModId, PMI_CfgParams * pPMI_CfgParams)
{
    // This function returns a ePMI_Error type, but it calls functions that
    // return ints (true 0/false -1) and eSTM_STATUS types. So the rule is
    // use the right type, but convert to a ePMI_Error type immediatly.

    ePMI_Error retVal_PMI = ePCMI_Success;

    // Just in case this function returns with an error, initialize the handle pointer to NULL.
    * ppPMI_Handle = NULL;

    //If interface already open return error
    if ( true == PMIisOpen[PMI_ModId] )
    {
        retVal_PMI = ePCMI_Error_AlreadyOpen;
    }
    else
    {
        pREG32_TYPE vPMI_ClkCtrlRegAddr = cTools_memMap(pPMI_CfgParams->PMI_ClockCtl_BaseAddr, sizeof(REG32_TYPE));
        void * vPMI_BaseAddress = cTools_memMap( pPMI_CfgParams->PMI_Module_BaseAddr, PCMI_RegBankSize);
        if ( ( NULL == vPMI_ClkCtrlRegAddr ) || ( NULL ==  vPMI_BaseAddress ) )
        {
             // Mapping error 
             retVal_PMI = ePCMI_Error_MappingError;
        }
        else
        {
            // Call the PMI/CMI common open command       
            retVal_PMI = (ePMI_Error)PCMI_Open(vPMI_BaseAddress, vPMI_ClkCtrlRegAddr, PMI_LibFuncValue);
            if ( ePCMI_Success == retVal_PMI )
            {
                // Set the samplw window
                retVal_PMI = (ePMI_Error)PCMI_SetSampleWindow(vPMI_BaseAddress, pPMI_CfgParams->FuncClockDivider, pPMI_CfgParams->SampleWindowSize );
                if ( ePCMI_Success == retVal_PMI )
                {                  
                    * ppPMI_Handle = (PMI_Handle_Pntr) cTools_memAlloc(sizeof(PMI_Handle_t));
                    if (NULL == * ppPMI_Handle) 
                    {
                        // Memory allocation failed.
                        retVal_PMI = ePCMI_Error_Memory_Allocation;
                    }                
                    else
                    {
                        PMI_Handle_t * pPMI_Hdl = (PMI_Handle_t *)* ppPMI_Handle;
                        pPMI_Hdl->BaseAddress =  vPMI_BaseAddress;
                        pPMI_Hdl->pCallBack = pPMI_CfgParams->pPMI_CallBack;
                        pPMI_Hdl->PMI_ModId = PMI_ModId;
#ifdef _STMLogging
                        pPMI_Hdl->pSTMHandle = pPMI_CfgParams->pSTMHandle;
                        pPMI_Hdl->STMMessageCh = pPMI_CfgParams->STMMessageCh;
#endif
                        pPMI_Hdl->ClkCtrlRegAddr = vPMI_ClkCtrlRegAddr;
                        pPMI_Hdl->validHandle = PMI_ValidHandlePattern;
                        
                        PMIisOpen[PMI_ModId] = true;
            
                       
            
                        //Set defualt values
                        {   
                            pREG32_TYPE  pPMI_EventClassFilter_Reg = PMI_EVENTCLASSFILTER_REG_ADDR(vPMI_BaseAddress);
                            * pPMI_EventClassFilter_Reg =   PMI_LOGICVOLTAGEOPPCHANGE 
                                                          | PMI_MEMORYVOLTAGEOPPCHANGE
                                                          | PMI_LOGICPOWERSTATECHANGE
                                                          | PMI_MEMORYPOWERSATTECHANGE; 
                        }
#ifdef _STMLogging
                        {
                            int32_t chrCnt;
                            
                            // Malloc space for the attribute string
                            pPMI_Hdl->pPMI_MetaData = (char *)cTools_memAlloc(PCMI_ATTR_BUFSIZE);
                            if(NULL == pPMI_Hdl->pPMI_MetaData)
                            {
                                PCMI_ReleaseModule(pPMI_Hdl->BaseAddress);
                                cTools_memUnMap((void *)pPMI_Hdl->BaseAddress, PCMI_RegBankSize);
                                cTools_memUnMap((void *)pPMI_Hdl->ClkCtrlRegAddr, sizeof(REG32_TYPE));
                                PMIisOpen[PMI_ModId] = false;
                                cTools_memFree(pPMI_Hdl);
                                return ePCMI_Error_Memory_Allocation;
                            }
                            
                            //
                            // The snprintf function null terminates the string, but it is not included in the
                            //  returned length, add 1 to the chrCnt to send the \0 as part of the STM msg.
                            //
                            chrCnt = snprintf(pPMI_Hdl->pPMI_MetaData, PCMI_ATTR_BUFSIZE,
                                              "{type=PMI,mid=%d,sw=%d,df=%d}",
                                                PMI_ModId,
                                                pPMI_CfgParams->SampleWindowSize,
                                                pPMI_CfgParams->FuncClockDivider);
                            pPMI_Hdl->PMI_MetaDataByteCnt = chrCnt + 1;
                            
                            if(PCMI_ATTR_BUFSIZE < chrCnt)
                            {
                                cTools_memFree(pPMI_Hdl->pPMI_MetaData);
                                PCMI_ReleaseModule(pPMI_Hdl->BaseAddress);
                                cTools_memUnMap((void *)pPMI_Hdl->BaseAddress, PCMI_RegBankSize);
                                cTools_memUnMap((void *)pPMI_Hdl->ClkCtrlRegAddr, sizeof(REG32_TYPE));
                                PMIisOpen[PMI_ModId] = false;
                                cTools_memFree(pPMI_Hdl);
                                return ePMI_Error_STM;
                            }
                        }
#endif
                    }
                }
            }
        }
    }

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_CfgParams->pPMI_CallBack ) )
    { 
        pPMI_CfgParams->pPMI_CallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;
}


ePMI_Error PMI_CloseModule(PMI_Handle_Pntr PMI_Handle )
{
    ePMI_Error retVal_PMI = ePCMI_Success;
    int32_t retVal = 0;
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;

    // Validate the handle
    if ( PMI_ValidHandlePattern != pPMI_Handle->validHandle )
    {
        retVal_PMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        
        // Confirm ownership
        retVal = PCMI_ConfirmOwnership(pPMI_Handle->BaseAddress);
        if ( 0 != retVal ) 
        {
            retVal_PMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else 
        {
            // Release the module
            retVal = PCMI_ReleaseModule(pPMI_Handle->BaseAddress);
            if ( 0 != retVal ) {
                retVal_PMI = ePCMI_Error_Ownership_Not_Released;
            }
// Normally we would want to disable the unit when closing but this makes the 
// uinit completly inaccessible.
#if 0
            else 
            {
                retVal = PCMI_ModuleClockDisable( pPMI_Handle->vPMI_ClkCtrlRegAddr );
                if ( 0 != retVal )
                {
                    retVal_PMI = ePMI_Error_ModuleClkEnableFailed;
                }
            }
#endif      
        }
        
        // Even if releasing the module or checking ownership failed we still
        // want to clean up after ourselves.

        //Unmap the PMI Base Address
        cTools_memUnMap((void *)pPMI_Handle->BaseAddress, PCMI_RegBankSize);
        
        //Unmap the PMI Clock Control Address
        cTools_memUnMap((void *)pPMI_Handle->ClkCtrlRegAddr, sizeof(REG32_TYPE));

        PMIisOpen[pPMI_Handle->PMI_ModId] = false;

#ifdef _STM_Logging
        if(NULL != pPMI_Handle->pPMI_MetaData)
        {
            cTools_memFree(pPMI_Handle->pPMI_MetaData);
        }
#endif
        //Free the PMI Handle memory
		cTools_memFree(pPMI_Handle);
           
    }

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;

}

ePMI_Error PMI_GetVersion(PMI_Handle_Pntr PMI_Handle, uint32_t * pLibMajorVersion, uint32_t * pLibMinorVersion, uint32_t * pSWFuncID, uint32_t * pHwFuncID)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    pREG32_TYPE  pPMI_ID_Reg = PCMI_ID_REG_ADDR(pPMI_Handle->BaseAddress);
    * pLibMajorVersion = PMICMILIB_MAJOR_VERSION;
    * pLibMinorVersion = PMICMILIB_MINOR_VERSION;
    * pSWFuncID = PMI_LibFuncValue;
    * pHwFuncID = PCMI_GetFuncValue(* pPMI_ID_Reg);
    return ePCMI_Success;
}

ePMI_Error  PMI_ModuleActivityEnable (PMI_Handle_Pntr PMI_Handle )
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;
    int32_t retVal = 0;
    
    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pPMI_Handle->BaseAddress);

    if ( PMI_ValidHandlePattern != pPMI_Handle->validHandle )
    {
        retVal_PMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        // Confirm ownership and enable the module
        retVal = PCMI_ConfirmOwnership( pPMI_Handle->BaseAddress );
        if ( 0 != retVal )
        {
            retVal_PMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else
        {

            retVal = PCMI_EnableModule(pPMI_Handle->BaseAddress);
            if ( 1 == retVal )
            {
                // Already enabled so just return.
                return ePCMI_Success;
            }
            
            if ( 0 > retVal ) 
            {
                retVal_PMI = ePCMI_Error_ModuleEnableFailed;
            }
            else
            {
#ifdef _STMLogging
                eSTM_STATUS retVal_STM = eSTM_SUCCESS;
                       
                if ( NULL != pPMI_Handle->pSTMHandle ) 
                {
                
                    retVal_STM = STMExport_IntMsg(pPMI_Handle->pSTMHandle, pPMI_Handle->STMMessageCh,
                            strPMI_ModID[pPMI_Handle->PMI_ModId],
                            SWDomainStr, SWLibClassStr,
                            PCMI_DataTypeStr[MSG], SWMsg_StartMonitoringStr, NULL);

                    if ( eSTM_SUCCESS == retVal_STM ) 
                    {
                        retVal_STM = STMExport_putMeta(pPMI_Handle->pSTMHandle,
                                                       pPMI_Handle->pPMI_MetaData,
                                                       pPMI_Handle->PMI_MetaDataByteCnt);
                        if(eSTM_SUCCESS != retVal_STM)
                        {
                            retVal_PMI = eCMI_Error_STM;
                        }
                    }
                } 
#endif
                * pPMI_Config_Reg = PCMI_NOP | PCMI_EVENTCAPTURE_ENABLE;

            }            
        }
    }

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;

}

ePMI_Error  PMI_ModuleActivityDisable(PMI_Handle_Pntr PMI_Handle, int32_t retain)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;
    int32_t retVal = 0;
    
    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pPMI_Handle->BaseAddress);

    if ( PMI_ValidHandlePattern != pPMI_Handle->validHandle )
    {
        retVal_PMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        // Confirm ownership and enable the module
        retVal = PCMI_ConfirmOwnership( pPMI_Handle->BaseAddress );
        if ( 0 != retVal ) 
        {
            retVal_PMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else
        {
            * pPMI_Config_Reg = PCMI_NOP | PCMI_EVENTCAPTURE_DISABLE;
            
            
            retVal = PCMI_DisableModule(pPMI_Handle->BaseAddress);
            if ( 0 != retVal )
            {
                retVal_PMI = ePCMI_Error_ModuleDisableFailed;
            }
            else
            {
#ifdef _STMLogging
                eSTM_STATUS retVal_STM = eSTM_SUCCESS;
                
                if ( NULL != pPMI_Handle->pSTMHandle )
                {
                    retVal_STM = STMExport_putMeta(pPMI_Handle->pSTMHandle,
                                                   pPMI_Handle->pPMI_MetaData,
                                                   pPMI_Handle->PMI_MetaDataByteCnt);
                    if(eSTM_SUCCESS != retVal_STM)
                    {
                        retVal_PMI = ePMI_Error_STM;
                    }

                    if ( eSTM_SUCCESS == retVal_STM ) {
                        retVal_STM = STMExport_IntMsg(pPMI_Handle->pSTMHandle, pPMI_Handle->STMMessageCh,
                                strPMI_ModID[pPMI_Handle->PMI_ModId],
                                SWDomainStr, SWLibClassStr,
                                PCMI_DataTypeStr[MSG], SWMsg_EndMonitoringStr, NULL);
                    }
                }
#endif
                // If retain false then soft reset the module
                if ( false == retain ) 
                {
                    retVal = PCMI_SoftResetModule( pPMI_Handle->BaseAddress );
                    if ( 0 != retVal ) 
                    {
                        retVal_PMI = ePCMI_Error_ResetDone_Not_Detected;
                    } 
                }             
            }
        }
    }

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;
}

ePMI_Error PMI_ConfigModule(PMI_Handle_Pntr PMI_Handle,  ePCMI_Triggers triggerEnables, ePMI_EventEnables eventEnables)
{

    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;
    int32_t retVal = 0;

    pREG32_TYPE pPMI_Config_Reg = PCMI_CONFIG_REG_ADDR(pPMI_Handle->BaseAddress);
        
    if ( PMI_ValidHandlePattern != pPMI_Handle->validHandle )
    {
        retVal_PMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {

        // Confirm ownership and enable the module
        retVal = PCMI_ConfirmOwnership( pPMI_Handle->BaseAddress );
        if ( 0 != retVal ) 
        {
            retVal_PMI = ePCMI_Error_Ownership_Not_Confirmed;
        }
        else
        { 
            // Check if the PMI module is enabled
            if ( PCMI_ENABLED == ( * pPMI_Config_Reg & PCMI_CLAIM_MASK ) ) 
            {
                retVal_PMI = ePCMI_Error_Module_Enabled;
            }
            else
            {
            
                pREG32_TYPE pPMI_TrigCntrl_Reg = PCMI_TRIGGERCONTROL_REG_ADDR(pPMI_Handle->BaseAddress);
                pREG32_TYPE pPMI_EventClassFltr_Reg = PMI_EVENTCLASSFILTER_REG_ADDR(pPMI_Handle->BaseAddress);

                switch ( triggerEnables ) 
                {
                    case ePCMI_TRIGGER_NOTMODIFIED:
                        break;
                    case ePCMI_TRIGGER_NONE:
                        * pPMI_TrigCntrl_Reg = 0;
                        break;
                    case ePCMI_TRIGGER_START_ENABLE:
                        * pPMI_TrigCntrl_Reg = PCMI_STARTCAPTURE_TRIGGER;
                        break;
                    case ePCMI_TRIGGER_STOP_ENABLE:
                        * pPMI_TrigCntrl_Reg = PCMI_STOPCAPTURE_TRIGGER;
                        break;
                    case ePCMI_TRIGGER_START_ENABLE | ePCMI_TRIGGER_STOP_ENABLE:
                        * pPMI_TrigCntrl_Reg = PCMI_STOPCAPTURE_TRIGGER | PCMI_STARTCAPTURE_TRIGGER;
                        break;
                }
               
                switch ( eventEnables ) 
                {
                    case ePMI_ENABLES_NOT_MODIFIED:
                        break;
                    case ePMI_ENABLE_NONE:
                      * pPMI_EventClassFltr_Reg = 0;
                      break;
                    default: 
                    {
                        * pPMI_EventClassFltr_Reg = eventEnables;
                    }  
                    
                }
            }
        } 
    }  

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;

}

ePMI_Error PMI_GetSampleWindow(PMI_Handle_Pntr PMI_Handle, uint32_t * pFuncClockDivider, uint32_t * pSampleWindowSize)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    pREG32_TYPE pPMI_SampleWin_Reg = PCMI_SAMPLEWINDOW_REG_ADDR(pPMI_Handle->BaseAddress );

    REG32_TYPE PMI_SampleWin_Reg = * pPMI_SampleWin_Reg;

    * pFuncClockDivider = (((PMI_SampleWin_Reg & PCMI_FUNCDIVIDEBY_MASK ) >>  PCMI_FUNCDIVIDEBY_OFFSET )+1);
    
    * pSampleWindowSize = ( ( PMI_SampleWin_Reg & PCMI_SAMPLEWINDOW_MASK )+1 );

    return ePCMI_Success;
} 	

const char *  PMI_GetErrorMsg(ePMI_Error errorCode)
{   
    // Since our convention is STM error codes simply extend PMI error codes from ePMI_Error_STM
    // simply return ePMI_Error_STM if the code is greater than ePMI_Error_STM.
    if ( errorCode < ePMI_Error_STM ) {
        errorCode = ePMI_Error_STM;
    }

    return PMI_ErrorStr[abs(errorCode)];

}

#ifdef _STMLogging
ePMI_Error  PMI_LogMsg(PMI_Handle_Pntr PMI_Handle, const char * FmtString, int32_t  * pValue)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    retVal_PMI = PMI_ExportMsg( PMI_Handle, 
                               strPMI_ModID[pPMI_Handle->PMI_ModId], SWDomainStr,
                               SWUsrClassStr, PCMI_DataTypeStr[MSG],
                               FmtString, (uint32_t *) pValue);

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;

}

#ifndef _NoMark
ePMI_Error  PMI_MarkLogicVoltageOPPChange(PMI_Handle_Pntr PMI_Handle, ePMI_LVD_OPP pwrDomain, uint8_t pwrValue)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    uint32_t pwrWide = (uint32_t)pwrValue;
    retVal_PMI = PMI_ExportMsg( PMI_Handle, 
                                strPMI_ModID[pPMI_Handle->PMI_ModId], pPMI_LvdOppStr[pwrDomain],
                                pPMI_DataClassStr[ePMI_LOGIC_VOLTAGE_OPP_CHANGE], PCMI_DataTypeStr[VALUE], 
                                PMI_DataValueFmtStr, &pwrWide);  
  
    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;
}

ePMI_Error  PMI_MarkMemoryVoltageOPPChange(PMI_Handle_Pntr PMI_Handle, ePMI_MVD_OPP pwrDomain, ePMI_MVD_PwrState pwrState)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    retVal_PMI = PMI_ExportMsg( PMI_Handle, 
                               strPMI_ModID[pPMI_Handle->PMI_ModId], pPMI_MvdOppStr[pwrDomain], 
                               pPMI_DataClassStr[ePMI_MEMORY_VOLTAGE_OPP_CHANGE], PCMI_DataTypeStr[STATE], 
                               pPMI_MvdPwrStateStr[pwrState], NULL);  

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;
}

ePMI_Error  PMI_MarkLogicPowerStateChange(PMI_Handle_Pntr PMI_Handle, ePMI_LPD_StateChange pwrDomain, ePMI_LPD_PwrState pwrState)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    retVal_PMI = PMI_ExportMsg( PMI_Handle, 
                               strPMI_ModID[pPMI_Handle->PMI_ModId],pPMI_LpdOppStr[pwrDomain],
                               pPMI_DataClassStr[ePMI_LOGIC_POWER_STATE_CHANGE], PCMI_DataTypeStr[STATE], 
                               pPMI_LpdPwrStateStr[pwrState], NULL);  

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;
}

ePMI_Error  PMI_MarkMemoryPowerStateChange(PMI_Handle_Pntr PMI_Handle, ePMI_MPD_StateChange pwrDomain, ePMI_MPD_PwrState pwrState)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    retVal_PMI = PMI_ExportMsg( PMI_Handle, 
                               strPMI_ModID[pPMI_Handle->PMI_ModId], pPMI_MpdOppStr[pwrDomain],
                               pPMI_DataClassStr[ePMI_MEMORY_POWER_STATE_CHANGE],PCMI_DataTypeStr[STATE],
                               pPMI_MpdPwrStateStr[pwrState], NULL);

    if ( ( ePCMI_Success != retVal_PMI ) && ( NULL != pPMI_Handle->pCallBack ) )
    { 
        pPMI_Handle->pCallBack(__FUNCTION__,retVal_PMI);
    }

    return retVal_PMI;

}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
static ePMI_Error  PMI_ExportMsg( PMI_Handle * PMI_Handle, 
                           const char * pModuleName, const char * pDomainName,
                           const char * pDataClass, const char * pDataType,
                           const char * pDataMsg, uint32_t *  pData)
{
    PMI_Handle_t * pPMI_Handle = (PMI_Handle_t *)PMI_Handle;
    ePMI_Error retVal_PMI = ePCMI_Success;

    if ( PMI_ValidHandlePattern != pPMI_Handle->validHandle ) 
    { 
        retVal_PMI = ePCMI_Error_InvalidHandlePointer;
    }
    else
    {
        if ( NULL == pPMI_Handle->pSTMHandle ) 
        {
            retVal_PMI = ePCMI_Error_NULL_STMHandle;
        }
        else 
        {

            eSTM_STATUS retVal_STM = STMExport_IntMsg(pPMI_Handle->pSTMHandle, pPMI_Handle->STMMessageCh,
                                                      pModuleName, pDomainName,
                                                      pDataClass, pDataType, 
                                                      pDataMsg, pData);
            if ( eSTM_SUCCESS == retVal_STM ) 
            {
                return ePCMI_Success;
            }
            else {
                retVal_PMI = (ePMI_Error)((int32_t)ePMI_Error_STM + (int32_t)retVal_STM);
            }
        }
    }  

    return retVal_PMI;

}
#endif //#ifdef _STMLogging

#ifdef _UsingTITools
#pragma CODE_SECTION(PMI_OpenModule,  ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_CloseModule, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_GetVersion,  ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_ModuleActivityEnable, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_ModuleActivityDisable, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_ConfigModule, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_GetSampleWindow, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_GetErrorMsg, ".text:PCMILibrary");
#ifdef _STMLogging
#pragma CODE_SECTION(PMI_LogMsg, ".text:PCMILibrary");
#ifndef _NoMark
#pragma CODE_SECTION(PMI_MarkLogicVoltageOPPChange, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_MarkMemoryVoltageOPPChange, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_MarkLogicPowerStateChange, ".text:PCMILibrary");
#pragma CODE_SECTION(PMI_MarkMemoryPowerStateChange, ".text:PCMILibrary");
#endif
#pragma CODE_SECTION(PMI_ExportMsg, ".text:PCMILibrary");
#endif
#endif

