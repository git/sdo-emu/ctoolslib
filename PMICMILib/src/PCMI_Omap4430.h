#ifndef __PMI_CMI_OMAP4430_H
#define __PMI_CMI_OMAP4430_H
/*
 * PCMI_Omap4430.h
 *
 * Common Power & Clock Management Instrumentation OMAP4430 Specific Definitions
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file PCMI_Omap4430.h

    PMI OMAP4430 specific definitions

*/


#ifdef _PMI

/*! \par ePMI_DataClass
    PMI_ExportMsg data class parameter
*/             
typedef enum {
               ePMI_LOGIC_VOLTAGE_OPP_CHANGE,    /*!< Logic Voltage OPP Change Data Class*/
               ePMI_MEMORY_VOLTAGE_OPP_CHANGE,   /*!< Memory Voltage OPP Change Data Class*/
               ePMI_LOGIC_POWER_STATE_CHANGE,    /*!< Logic Power State Change Data Class*/
               ePMI_MEMORY_POWER_STATE_CHANGE       /*!< Memory Power State Change Data Class*/
             } ePMI_DataClass;

/*! \par ePMI_DataClass
    Logic voltage domain selections
*/  
typedef enum { eLVD_MPU,                /*!< MPU Logic Voltage Domain */
               eLVD_IVA,                /*!< IVA Logic Voltage Domain */
               eLVD_CORE,               /*!< CORE Logic Voltage Domain */
               eLVD_EndEl
             } ePMI_LVD_OPP;

/*! \par ePMI_MVD_OPP
    Memory voltage domain selections
*/  
typedef enum { eMVD_SRAM_MPU,           /*!< MPU SRAM Memory Voltage Domain */ 
               eMVD_SRAM_IVA,           /*!< IVA SRAM Memory Voltage Domain */
               eMVD_SRAM_CORE,          /*!< CORE SRAM Memory Voltage Domain */
               eMVD_EndEl
             } ePMI_MVD_OPP;

/*! \par ePMI_MVD_OPP
    Logic Power State Change doamins selections
*/  
typedef enum { eLPD_MPU,                /*!< MPU Logic Power Domain */
               eLPD_A9_C0,              /*!< A9 Core 0 Logic Power Domain */
               eLPD_A9_C1,              /*!< A9 Core 1 Logic Power Domain */
               eLPD_TESLA,              /*!< Tesla Logic Power Domain */
               eLPD_ABE,                /*!< ABE Logic Power Domain */
               eLPD_CORE,               /*!< CORE Logic Power Domain */
               eLPD_IVAHD,              /*!< IVAHD Logic Power Domain */
               eLPD_CAM,                /*!< CAM Logic Power Domain */
               eLPD_DSS,                /*!< DSS Logic Power Domain */
               eLPD_GFX,                /*!< GFX Logic Power Domain */
               eLPD_L3INT,              /*!< L3INT Logic Power Domain */
               eLPD_LAPER,              /*!< LAPER Logic Power Domain */
               eLPD_CEFUSE,              /*!< CEFUSE Logic Power Domain */
               eLPD_EndEl
             } ePMI_LPD_StateChange;

/*! \par ePMI_MVD_OPP
    Memory Power State Change doamins selections
*/ 
typedef enum { eMPD_MPU_L1,             /*!< MPU L1 Memory Power Domain */
               eMPD_MPU_L2,             /*!< MPU L2 Memory Power Domain */
               eMPD_MPU_RAM,            /*!< MPU RAM Memory Power Domain */
               eMPD_MPU_CHIRON_M0,      /*!< MPU Chiron M0 Memory Power Domain */
               eMPD_MPU_CHIRON_M1,      /*!< MPU Chiron M1 Memory Power Domain */
               eMPD_TESLA_L1,           /*!< Tesla L1 Memory Power Domain */    
               eMPD_TESLA_L2,           /*!< Tesla L2 Memory Power Domain */
               eMPD_TESLA_EDMA,         /*!< Tesla EDMA Memory Power Domain */
               eMPD_AUDIO_ENG,          /*!< Audio Engine Memory Power Domain */
               eMPD_AUDIO_PER,          /*!< Audio PER Memory Power Domain */
               eMPD_CORE_NON_RET,       /*!< Core Non Ret Memory Power Domain */
               eMPD_OCMRAM,             /*!< OCMRAM Memory Power Domain */
               eMPD_DUCATI_L2,          /*!< Ducati L2 Memory Power Domain */
               eMPD_DUCATI_UNICHACHE,   /*!< Ducati Unicache Memory Power Domain */
               eMPD_CORE_OTHER_BANKS,   /*!< Core Other Banks Memory Power Domain */
               eMPD_IVAHD_HWA,          /*!< IVAHD HWA Memory Power Domain */
               eMPD_IVAHD_SL2,          /*!< IVAHD SL2 Memory Power Domain */
               eMPD_IVAHD_TCM1,         /*!< IVAHD TCM1 Memory Power Domain */
               eMPD_IVAHD_TCM2,         /*!< IVAHD TCM2 Memory Power Domain */
               eMPD_CAM,                /*!< CAM Memory Power Domain */
               eMPD_DSS,                /*!< DSS Memory Power Domain */
               eMPD_GFX,                /*!< GFX Memory Power Domain */
               eMPD_L3INT_BANK1,        /*!< L3INT Bank1 Memory Power Domain */
               eMPD_L4_PER1,            /*!< L4 PER1 Memory Power Domain */
               eMPD_L4_PER2,            /*!< L4 PER2 Memory Power Domain */
               eMPD_EndEl
             } ePMI_MPD_StateChange;

/*! \par ePMI_MVD_OPP
    Memory Voltage Domain (MVD) Power State message selections
*/
typedef enum { eMVD_ON,                 /*!< Memory Voltage Domain "on" state*/
               eMVD_SLEEP,              /*!< Memory Voltage Domain "sleep" state*/
               eMVD_PwrState_EndEl
             } ePMI_MVD_PwrState;

/*! \par ePMI_MVD_OPP
    Logic Power Domain (LPD) Power State message selections
*/
typedef enum { eLPD_OFF,                /*!< Logic Power Domain "off" state*/
               eLPD_RET,                /*!< Logic Power Domain "retention" state*/
               eLPD_INACTIVE,           /*!< Logic Power Domain "inactive" state*/
               eLPD_ON,                 /*!< Logic Power Domain "on" state*/
               eLPD_PwrState_EndEl
             } ePMI_LPD_PwrState;

/*! \par ePMI_MVD_OPP
    Memory Power Domain (MPD) Data message selections
*/
typedef enum { eMPD_OFF,                /*!< Memory Power Domain "off" state*/
               eMPD_RET,                /*!< Memory Power Domain "retention" state*/
               eMPD_INACTIVE,           /*!< Memory Power Domain "inactive" state*/
               eMPD_ON,                 /*!< Memory Power Domain "on" state*/
               eMPD_PwrState_EndEl
             } ePMI_MPD_PwrState;

#ifndef _DOXYGEN_IGNORE
extern const char * pPMI_LpdPwrStateStr[];
extern const char * pPMI_MvdPwrStateStr[];
extern const char * pPMI_MpdOppStr[];
extern const char * pPMI_LpdOppStr[];
extern const char * pPMI_LpdOppStr[];
extern const char * pPMI_MvdOppStr[];
extern const char * pPMI_DataClassStr[];
extern const char * pPMI_LvdOppStr[];
extern const char * pPMI_MpdPwrStateStr[];

extern const char PMI_DataValueFmtStr[];
#endif
#endif
#ifdef _CMI

/*! \par eCMI_DataClasses
    CMI_ExportMsg data class parameter
*/
typedef enum {  eCMI_CLOCK_DOMAIN_STATE_CHANGE,     /*!< Clock domain state change class */
                eCMI_CLOCK_DIVIDER_RATIO_CHANGE,    /*!< Clock divider ratio change class */
                eCMI_CLOCK_SOURCE_SELECTION_CHANGE, /*!< Clock source selection chnage class */
                eCMI_DLPP_SETTINGS_CHANGE           /*!< DLPP settings change class */
                } eCMI_DataClasses;

/*! \par eCMI_ClockDomainStateChange
    CM1 & CM2 clock domain state change members
*/
typedef enum { //CM1 Clock Domain State Change
               eCMI_CDSC_MPU_DPLL,                /*!< MPU DPLL CM1 Clock Domain State Change ID */ 
               eCMI_CDSC_TESALA_ROOT,             /*!< TESLA ROOT CM1 Clock Domain State Change ID */
               eCMI_CDSC_ABE_24M_GFCLK,           /*!< ABE 24M GFCLK CM1 Clock Domain State Change ID */
               eCMI_CDSC_ABE_ALWON_32K,           /*!< BE ALWON 32K CM1 Clock Domain State Change ID */
               eCMI_CDSC_ABE_SYSCLK,              /*!< ABE SYSCLK CM1 Clock Domain State Change ID */
               eCMI_CDSC_FUNC_24M_GFCK,           /*!< FUNC 24M GFCK CM1 Clock Domain State Change ID */
               eCMI_CDSC_ABE_OCP_GICLK,           /*!< ABE OCP GICLK CM1 Clock Domain State Change ID */
               eCMI_CDSC_ABE_X2_CLK,              /*!< ABE X2 CLK CM1 Clock Domain State Change ID */
               eCMI_CDSC_L4_ALWON_ICLK,           /*!< L4 ALWON ICLK CM1 Clock Domain State Change ID */               
                
               //CM2 Clock Domain State Change
               eCMI_CDSC_L3_1,                  /*!< L3 1 CM2 Clock Domain State Change ID */
               eCMI_CDSC_L3_2,                  /*!< L3 2 CM2 Clock Domain State Change ID */
               eCMI_CDSC_DUCATI,                /*!< DUCATI CM2 Clock Domain State Change ID */
               eCMI_CDSC_L3_DMA,                /*!< L3 DMA CM2 Clock Domain State Change ID */
               eCMI_CDSC_ASYNC_PHY2,            /*!< ASYNC PHY2 CM2 Clock Domain State Change ID */
               eCMI_CDSC_ASYNC_PHY1,            /*!< ASYNC PHY1 CM2 Clock Domain State Change ID */
               eCMI_CDSC_ASYNC_DLL,             /*!< ASYNC DLL CM2 Clock Domain State Change ID */
               eCMI_CDSC_PHY_ROOT,              /*!< PHY ROOT CM2 Clock Domain State Change ID */
               eCMI_CDSC_DLL,                   /*!< DLL CM2 Clock Domain State Change ID */
               eCMI_CDSCL3_EMIF,                /*!< EMIF CM2 Clock Domain State Change ID */
               eCMI_CDSC_L4_D2D,                /*!< L4 D2D CM2 Clock Domain State Change ID */
               eCMI_CDSC_L3_D2D,                /*!< L3 D2D CM2 Clock Domain State Change ID */
               eCMI_CDSC_L4_CONFIG,             /*!< L4 CONFIG CM2 Clock Domain State Change ID */
               eCMI_CDSC_IVAHD_ROOT,            /*!< IVAHD ROOT CM2 Clock Domain State Change ID */
               eCMI_CDSC_FDIF,                  /*!< FDIF CM2 Clock Domain State Change ID */
               eCMI_CDSC_ISS,                   /*!< ISS CM2 Clock Domain State Change ID */
               eCMI_CDSC_DSS_ALWON_SYS,         /*!< DSS ALWON SYS CM2 Clock Domain State Change ID */
               eCMI_CDSC_DSS,                   /*!< DSS CM2 Clock Domain State Change ID */
               eCMI_CDSC_L3_DSS,                /*!< L3 DSS CM2 Clock Domain State Change ID */
               eCMI_CDSC_SGX,                   /*!< SGX CM2 Clock Domain State Change ID */
               eCMI_CDSC_L3_GFX,                /*!< L3 GFX CM2 Clock Domain State Change ID */
               eCMI_CDSC_UTMI_ROOT,             /*!< UTMI ROOT CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_HSMMC_6,          /*!< INIT HSMMC 6 CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_HSMMC_2,          /*!< INIT HSMMC 2 CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_HSMMC_1,          /*!< INIT HSMMC 1 CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_HSI,              /*!< INIT HSI CM2 Clock Domain State Change ID */
               eCMI_CDSC_USB_DPLL_HS,           /*!< USB DPLL HS CM2 Clock Domain State Change ID */
               eCMI_CDSC_USB_DPLL,              /*!< USB DPLL CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_48MC,             /*!< INIT 48MC CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_48M,              /*!< INIT 48M CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_96M,              /*!< INIT 96M CM2 Clock Domain State Change ID */
               eCMI_CDSC_EMAC_50MHZ,            /*!< EMAC 50MHZ CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_L4,               /*!< INIT L4 CM2 Clock Domain State Change ID */
               eCMI_CDSC_INIT_L3,               /*!< INIT L3 CM2 Clock Domain State Change ID */
               eCMI_CDSC_PER_ABE_24M,           /*!< PER ABE 24M CM2 Clock Domain State Change ID */
               eCMI_CDSC_PER_SYS,               /*!< PER SYS CM2 Clock Domain State Change ID */
               eCMI_CDSC_PER_96M,               /*!< PER 96M CM2 Clock Domain State Change ID */
               eCMI_CDSC_PER_48M,               /*!< PER 48M CM2 Clock Domain State Change ID */
               eCMI_CDSC_FUNC_24MC,             /*!< FUNC 24MC CM2 Clock Domain State Change ID */
               eCMI_CDSC_FUNC_12M,              /*!< FUNC 12M CM2 Clock Domain State Change ID */
               eCMI_CDSC_PER_L4,                /*!< PER L4 CM2 Clock Domain State Change ID */
               eCMI_CDSC_UNIPRO_DPLL,           /*!< UNIPRO DPLL CM2 Clock Domain State Change ID */
               eCMI_CDSC_EndEl
                             
             }eCMI_ClockDomainStateChange;

/*! \par eCMI_clkState
    CM1 & CM2 clock domain state parameters
*/             
typedef enum { eCMI_ClkDomainGated,          /*!< Domain clock gated - off */ 
               eCMI_ClkDomainActive          /*!< Domain clock active - on */
             }eCMI_clkState;

/*! \par eCMI_clkState
    CM1 & CM2 divide ratio change members
*/ 
typedef enum { //CM1 Clock Divider Ratio Change
                 eCMI_CDRC_CLKSEL_OPP,          /*!< CLKSEL OPP CM1 Clock Divider Ratio Change */
                 eCMI_CDRC_BYPCLK_DPLL_MPU,     /*!< BYPCLK DPLL MPU CM1 Clock Divider Ratio Change */
                 eCMI_CDRC_BYPCLK_DPLL_IVA,     /*!< BYPCLK DPLL IVA CM1 Clock Divider Ratio Change */
                 eCMI_CDRC_ABE_AESS,            /*!< ABE AESS CM1 Clock Divider Ratio Change */

                 //CM2 Clock Divider Ratio Change
                 eCMI_CDRC_SCALE_FCLK,          /*!< SCALE FCLK CM2 Clock Divider Ratio Change */
                 eCMI_CDRC_CAM_FDIF,            /*!< CAM FDIF CM2 Clock Divider Ratio Change */
                 eCMI_CDRC_GFX_PER_192M         /*!< GFX PER 192M CM2 Clock Divider Ratio Change */
                   
                 
             } eCMI_ClockDividerRatioChange;

/*! \par eCMI_divState
    CM1 & CM2 divide ratio change parameters
*/ 
typedef enum { eCMI_DivideBy_dummy,            // Just a placeholder for 0 
               eCMI_DivideBy_1,                /*!< Clock Divider Ratio Change State  - divide by 1 */ 
               eCMI_DivideBy_2,                /*!< Clock Divider Ratio Change State  - divide by 2 */
               eCMI_DivideBy_3,                /*!< Clock Divider Ratio Change State  - divide by 3 */
               eCMI_DivideBy_4,                 /*!< Clock Divider Ratio Change State  - divide by 4 */
               eCMI_DivideBy_EndEl 
             } eCMI_divState;

/*! \par eCMI_divState
    CM1 & CM2 clock source selection change members
*/
typedef enum {  //CM1 Clock Source Selection Change
                eCMI_CSS_CORE_L4,               /*!< Core L4 CM1 Clock Source Selection Changed */
                eCMI_CSS_CORE_L3,               /*!< Core L3 CM1 Clock Source Selection Changed */
                eCMI_CSS_CORE,                  /*!< Core CM1 Clock Source Selection Changed */
                eCMI_CSS_DPLL_CORE,             /*!< DPLL Core CM1 Clock Source Selection Changed */

                //CM2 Clock Source Selection
                eCMI_CSS_DUCATI_ISS_ROOT,       /*!< DUCATI ISS ROOT CM2 Clock Source Selection Changed */
                eCMI_CSS_USB_60MHZ,             /*!< USB 60MHZ CM2 Clock Source Selection Changed */
                eCMI_CSS_PER_DPLL_BYP,          /*!< PER DPLL BYP CM2 Clock Source Selection Changed */
                eCMI_CSS_USB_DPLL_BYP,          /*!< USB DPLL BYP CM2 Clock Source Selection Changed */
                eCMI_CSS_GFX,                   /*!< GFX CM2 Clock Source Selection Changed */
                eCMI_CSS_MMC1,                  /*!< MMC1 CM2 Clock Source Selection Changed */
                eCMI_CSS_MMC2,                  /*!< MMC2 CM2 Clock Source Selection Changed */
                eCMI_CSS_HSI                    /*!< HSI CM2 Clock Source Selection Changed */
             } eCMI_ClockSource;

/*! \par eCMI_DPLLSrc
    CM1 & CM2 DLPP settings update members
*/
typedef enum {  //CM1 DPLL Settings Update
                eCMI_DLPP_UPDATE_CORE,           /*!< CM1 CORE DPLL setting update */
                eCMI_DLPP_UPDATE_MPU,            /*!< CM1 MPU DPLL setting update */
                eCMI_DLPP_UPDATE_IVA,            /*!< CM1 IVA DPLL setting update */
                eCMI_DLPP_UPDATE_ABE,            /*!< CM1 ABE DPLL setting update */
                eCMI_DLPP_UPDATE_DDRPHY,         /*!< CM1 DDRPHY DPLL setting update */

                //CM2 DLPP Settings Update
                eCMI_DLPP_UPDATE_PER,            /*!< CM2 PER DPLL setting update */
                eCMI_DLPP_UPDATE_USB,            /*!< CM2 USB DPLLsetting update */
                eCMI_DLPP_UPDATE_UNIPRO          /*!< CM2 UNIPRO DPLLsetting update */
 
             }eCMI_DPLLSrc;
 
#ifndef _DOXYGEN_IGNORE
extern const char * pCMI_DataClassStr[];

#define eCM1_CDSC_firstElement eCMI_CDSC_MPU_DPLL
#define eCM1_CDSC_lastElement eCMI_CDSC_L4_ALWON_ICLK
#define eCM2_CDSC_firstElement eCMI_CDSC_L3_1
#define eCM2_CDSC_lastElement eCMI_CDSC_UNIPRO_DPLL

extern const char * pCMI_ClkDomainStr[];
 
#define eCM1_DLPPSRC_firstElement eCMI_DLPP_UPDATE_CORE
#define eCM1_DLPPSRC_lastElement eCMI_DLPP_UPDATE_DDRPHY
#define eCM2_DLPPSRC_firstElement eCMI_DLPP_UPDATE_PER
#define eCM2_DLPPSRC_lastElement eCMI_DLPP_UPDATE_UNIPRO

extern const char * pCMI_DLPPSrcStr[];

#define eCM1_CDRC_firstElement eCMI_CDRC_CLKSEL_OPP
#define eCM1_CDRC_lastElement eCMI_CDRC_ABE_AESS
#define eCM2_CDRC_firstElement eCMI_CDRC_SCALE_FCLK
#define eCM2_CDRC_lastElement eCMI_CDRC_GFX_PER_192M

extern const char * pCMI_DivRatioStr[];

#define eCM1_CSSC_firstElement eCMI_CSS_CORE_L4
#define eCM1_CSSC_lastElement eCMI_CSS_DPLL_CORE
#define eCM2_CSSC_firstElement eCMI_CSS_DUCATI_ISS_ROOT
#define eCM2_CSSC_lastElement eCMI_CSS_HSI

extern const char * pCMI_ClkSelectStr[];

extern const char * pCMI_ClkStateStr[];
extern const char * pCMI_DivRatioStateStr[];
#endif
            
#endif // End of #ifdef _CMI
#endif // End of #ifndef __PMI_CMI_OMAP4430_H
