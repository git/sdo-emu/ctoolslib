/*
 * StmLibrary.h
 *
 * STM Library public API Definitions. 
 *
 * Copyright (C) 2009-2012 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef __STMLLIBRARY_H
#define __STMLLIBRARY_H

#ifdef _CIO
#include <stdio.h>
#endif
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


/*! \file STMLibrary.h

*/
/*! \mainpage

    \section intro Introduction

    The STM Library provides a STM programming API for software instrumentation. The library provides
    methods for transporting instrumentation messages and application data. Message transport support includes
    facilities for both variable length formatted strings (STMXport_printf), fixed length formatted strings (STMXport_logMsgN) and 
    user formatted strings (STMXport_putMsg). The fixed and variable length strings provide the most efficient 
    transport packet sizes since only pointers to the actual strings are transported. STMXport_logMsgN()
    methods are significantly more efficient than STMXport_printf because the string does not have to be parsed
    to determine the number of variables to transport. Data transport can be performed on buffers of data 
    (STMXport_putBuf) or on individual data values (STMXport_putWord, STMXport_putShort, STMXport_putByte).  
       
    The STM Target Library exports a "C" interface to the customer's application. This is a single
    threaded API that is non-atomic (interrupts are not disabled).  

    The physical STM unit provides multiple channels, each channel providing thread safe data transport, thus
    messages from different channels may be interleaved. STM messages can also be interleaved between masters
    (different processors). Channel distribution by the client may also be used to provide classification of
    different data types, like distinguishing between logging levels, warning and error messages. When using 
    STM Channels to classify data you can filter the data in the Trace Display based solely on channel numbers,
    or in conjunction with other parameters. For these reasons the STM library does not provide any channel
    management, currently leaving that to the client's library to manage.      

    If a client utilizes a single channel, the clients is responsible for wrapping it with appropriate
    semaphores to ensure thread safety or if a client wants to use this API from an ISR (without using a
    unique channel). The API client is also responsible for disabling interrupts to create atomic operations.
    Atomic operations may be required if the client is relying on the accuracy of timestamps applied
    to each STM message (since these timestamps are applied at transport time when the last data element
    is transported).

    STM SW messages generated using these APIs will be displayed in a human readable format in the XDS560 Trace
    Display and Analyzer tool. For the most part it is not needed nor recommended to put or impose any
    protocols on the top of the messages. In case there is a strong need for adding additional protocols on
    the top of the messages, a paired change in STM SW decoder framework (part of host tooling) may be needed
    to propagate knowledge of the protocols so the information could be defined in a human readable or desired
    format. Such decoder changes are made possible by the STM tooling SDK. 

    \par Helper Functions
    
    Helper functions allow the library to be ported easily to different operating environments without modifications
    to the library directly. Within the library these functions are declared extern so the library will build but
    can not be linked without implementations for the following helper functions:
    
    \li void * cTools_memAlloc(size_t sizeInBytes); \n\n
    If allocation successful returns pointer to allocated memory, else if not successful return NULL.
    
    \li void * cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes); \n\n
    Map a physical address to a virtual address space. Return the virtual pointer if successful, else return NULL.
    Return phyAddr if virtual mapping not required.   
    
    \li void cTools_memFree(void * ptr); \n\n
    Free the memory at the ptr (returned from cTools_memAlloc()) call. 
    
    \li void cTools_memUnmap(void * vAddr, unsigned int mapSizeInBytes); \n\n
    Unmap virtual address space starting at vAddr (returned from cTools_memMap())of mapSizeInBytes length.
    If virtual address mapping not required simply return.    
    
    \par Directory Structure
    
    STM is a component of cTools so it resides within the cTools directory structure.
    
    @code
    
    |--cTools
       |
       |-- STMLib
       |   |
       |   |-- doc
       |   |   |
       |   |   |--STM_html
       |   |      |
       |   |      |-index.html (Doxygen API documentation)
       |   |     
       |   |-- include  (Public API include file)
       |   |-- src      (Private .c and .h files )
       |   |-- projects (Target specific library builds)
       |-- Examples
           |
           |-- 'Specific Device' (such as 'C667x')
           |   |
           |   |--STM_...        (Target specific stand-alone example projects)
    
     
    @endcode
    
    \par Library Builds

    There are two library types available for STM export and two for CIO(file or stdout) export. The library
    type is controlled by defining the following preprocessor names:
    \li	_STM
    \li	_COMPACT
    \li	_CIO 
    \li	_CIO & _COMPACT

    The _STM build provides all defined API functions and features. The _COMPACT build reduces the capabilities
    of the library to fit into ~1K of memory. The _CIO build provides a path for the STM library APIs to be
    utilized in the simulator environment. All user data is exported to either a file or stdout through CIO,
    while remaining completely portable to either the _STM or _COMPACT build by simply changing out the library
    build used(with the exception of how the library is opened). For the compact build all functions are
    available to maintain compatibility with code implemented for the normal build, but some functions will
    return errors if their use is invalid. For _STM and _COMPACT builds both release and debug versions are
    supported. Only debug builds are provided for the _CIO version of the library. The debug version of the
    libraries enables function parameter checking. 


    Important Notes: 
    \li	If the data cache is enabled the MIPI_STM memory space must be protected from caching.
    \li	If the interface is opened for blocking calls, and your system has an MMU, your performance may be
        improved by enabling the MIPI_STM memory space for buffered writes.
    \li	Opening the library for buffered IO operation is only supported for OMAP devices. 
    \li	The following code section names are provided to allow splitting the library between different
        memory sections:
    \n	- .text:STMLibraryFast used for code that interfaces to the STM module. The library may benefit from a performance improvement if this section is included in fast internal device memory.
    \n	- .text:STMLibrarySlow used for code that can be placed in external memory that will be cached. This is typically the API interface routines.
    \n	- .text:STMLibraryISR used for code that may be called from an ISR or SWI.
    \n  These section names are only provided as a convenience. The STM library has no requirement that any section be placed in a specific internal or external memory space.

    \par STM Library Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 1.0 </TD> <TD> 10/20/2008 </TD> <TD> Initial Release </TD>
<TR><TD> 1.1 </TD> <TD> 11/19/2008 </TD> <TD> Updated callback to work in both buffered and blocking modes.  </TD>
<TR><TD> 1.2 </TD> <TD> 1/14/2009 </TD> <TD> Added APIs to improve performance of individual value transfers
                                             and created a compact  version of the Library.  </TD>
<TR><TD> 1.3 </TD> <TD> 2/11/2009 </TD> <TD> Added APIs to log text messages </TD>
<TR><TD> 1.4 </TD> <TD> 2/16/2009 </TD> <TD> Removed RTS dependency for the compact build </TD> 
<TR><TD> 1.5 </TD> <TD> 5/19/2009 </TD> <TD> Added CIO build for Simulation support </TD> 
<TR><TD> 1.6 </TD> <TD> 7/6/2009 </TD> <TD> Abstracted memory allocation and mapping routines to external functions
                                             to be provided by the client. </TD>                                            
<TR><TD> 2.0 </TD> <TD> 3/1/2010   </TD> <TD> Added configuration structure for printf optimization parameter  </TD>
<TR><TD> 3.0 </TD> <TD> 10/22/2010 </TD> <TD> \li Updated API parameters to fixed-width types. 
                                             \li Moved the STM Base Address and Channel Resolution STMXport_open
                                             parameters to the configuration structure.
                                             \li Changed the names of client provided external support functions
                                             from STM_* to cTools_*.
                                             \li Made the callback a configuration parameter rather than a separate
                                             argument for each API call. </TD>
<TR><TD> 3.1 </TD> <TD> 12/1/2010 </TD> <TD> Added support for internal function STMExport_putMeta().
                                             This function will normally be used by other TI provided libraries
                                             to include meta data (context sensitive information) in the STM data
                                             stream. </TD>
<TR><TD> 4.0 </TD> <TD> 12/1/2010 </TD> <TD> Added  STMXport_config_CS() for ARM Coresight STM 2.0 module support. </TD>
<TR><TD> 4.1 </TD> <TD> 12/1/2010 </TD> <TD> Updated projects to CCSv5 type</TD>
<TR><TD> 5.0 </TD> <TD> 12/1/2010 </TD> <TD> Removed STMBufObj * from STMXport_open (to eliminate message buffering support.
                                             Added xmit_printf_mode. Removed OMAP SDMA Support. </TD>
<TR><TD> 5.1 </TD> <TD> 12/6/2013 </TD> <TD> Fix software messages for big endian targets. </TD>
<TR><TD> 5.2 </TD> <TD> 9/29/2014 </TD> <TD> Added missing CCS project files. </TD>											 
</TABLE>

*/

// Note - the version definitions must have the end of line immediately after the value (packaging script requirement)
#define STMLIB_MAJOR_VERSION    (0x5)
                                        /*!< Major version number - Incremented for API changes*/
#define STMLIB_MINOR_VERSION    (0x2)
                                        /*!< Minor version number - Incremented for bug fixes  */


/*! \par bool

    bool typedef provided for compilers that are not C99 compliant.

*/
#ifndef __bool_true_false_are_defined
typedef enum
{ 
	false = 0,
    true = 1
	
}bool;
#endif

/*! \par eSTM_STATUS

    STM Library API function return codes.

*/
typedef enum{
	
	eSTM_SUCCESS = 0, 						/*!< No error, message completed */
	eSTM_PENDING = 1,						/*!< No error, message buffered */ 
	eSTM_ERROR_PARM = -1,					/*!< Error, method parameter error */
	eSTM_ERROR_STRING_FORMAT = -2,			/*!< Error, Printf string format error */
	eSTM_ERROR_HEAP_ALLOCATION = -3,		/*!< Error , Can not allocate heap error */
	eSTM_ERROR_BUF_ALLOCATION = -4,			/*!< Error, message to large for remaining buffer space */ 
	eSTM_ERROR_INVALID_FUNC = -5,			/*!< Error, Function not supported by _COMPACT build */
	eSTM_ERROR_CIO_ERROR = -6,				/*!< Error, CIO error occurred */
	eSTM_ERROR_OWNERSHIP_NOT_GRANTED = -7,  /*!< Error, Module ownership not granted */
	eSTM_ERROR_FIFO_NOTEMPTY = -8

}eSTM_STATUS;

/*! \par eSTMElementSize

    STM Library API element size codes.

*/
typedef enum
{
	eByte = 1,                              /*!< Byte element size */ 
	eShort = 2,                             /*!< Short element size */
	eWord = 4                               /*!< Word element size */
}eSTMElementSize;

/*! \par STMBufObj

    Configuration parameters for non-blocking buffered IO.

*/
#ifdef _CIO
typedef struct _STMBufObj 
{
	char        * pFileName;					// Used in eBlockingCIO case only! CIO filename for STM destination.
	                                            //  if NULL stdout used
	bool          fileAppend;                   // Preserve contents of file and append 
	char        * pMasterId;                    // Used in eBlockingCIO case only! Pointer to client provided master name  

}STMBufObj;
#else
typedef struct _STMBufObj 
{
    bool    notUsed;                             // Not used in normal mode but we need to keep the API backward compatible
}STMBufObj;
#endif

/*! \par STMXport_callback         
    
    The callback function is called with the function's exit status.  Note that this function is not exported
    by the interface, but is used as a parameter for STM data transport calls. 
    
    \param[in] eSTM_STATUS The function's exit status  
   
    \return void 

    \par Details:
    
    This is a user provided callback normally used to centralize error handling. When operating the STM Library
    in Buffered Mode, if there is no error, the status indicates if the message is pending (eSTM_PENDING) or
    in the process of being transported (eSTM_SUCCESS).  
    
*/
typedef void(*STMXport_callback)(const char * funcName, eSTM_STATUS);

/*! \par eSTM_PrintfMode
 *
 *    Set the mode for STMXport_printf, TMXport_logMsg, and STMXport_logMsgN functions.
 */

typedef enum
{
    eSend_char_stream,                        /*!< Send the printf format string and arguments as a resolved char stream */
    eSend_optimized                           /*!< Send the printf format string as a pointer and arguments as binary values */
} eSTM_XmitPrintfMode;

/*! \par eSTM_XportModuleType
 *
 *    Set the transport module type to MIPI STM or CoreSight STM.
 */


/*! \par STMConfigObj
 
    This struct is used to communicate configuration parameters to the library.
*/
typedef struct _STMConfigObj 
{
    uint32_t            STM_XportBaseAddr;          /*!< The STM module's Transport base address. */
    uint32_t            STM_ChannelResolution;      /*!< The STM units channel resolution in bytes. Valid values for MIPI is either
                                                    1024 or 4096, ARM is 256 */
    uint32_t			STM_CntlBaseAddr;           /*!< MIPI or CoreSight STM 2.0 Control register mapping base address.*/
    STMXport_callback   pCallBack;                  /*!< This is a pointer to a callback function that if not NULL is
                                                     called on completion of most API functions */
    eSTM_XmitPrintfMode xmit_printf_mode;            /*!< STM transmit mode (optimized or character) for STMXport_printf()
                                                          and STMXport_logMsg functions. */
    bool                optimize_strings;            /*!< If xmit_printf_mode is eSend_optimized, then optimize_strings determines
                                                     if string values are transported as a pointer (optimize_strings true)
                                                     or by value (optimize_strings false).
                                                     Optimize_strings only affects the %%s case of a STMXport_printf()
                                                     or STMXport_logMsg format string.
                                                     If optimize_strings is true only a pointer to the %%s constant char
                                                     string is transported. 
                                                     If optimize_strings is false, the complete "C" string (null terminated)
                                                     byte buffer pointed to by %%s is transported.  */
}STMConfigObj;


typedef enum
{
    eBUFSIZE_0,                         /*!< STM data will be drained into a fixed buffer */
	eBUFSIZE_4K,                        /*!< STM data will be drained into a 4K circular buffer */
	eBUFSIZE_8K,                        /*!< STM data will be drained into a 8K circular buffer */
	eBUFSIZE_16K,                       /*!< STM data will be drained into a 16K circular buffer */
	eBUFSIZE_32K,                       /*!< STM data will be drained into a 32K circular buffer */
	eBUFSIZE_64K,                       /*!< STM data will be drained into a 64K circular buffer */
	eBUFSIZE_128K,                      /*!< STM data will be drained into a 128K circular buffer */
	eBUFSIZE_256K                       /*!< STM data will be drained into a 256K circular buffer */
} eSTM_ExportBufSize;

#ifdef _CS_STM
typedef struct _CS_ConfigObj
{
	uint32_t            TraceBufSize;           /*!< Size in bytes of the trace capture buffer. Needed to set ATB parameters properly */

}STM_CS_ConfigObj;
#endif
#ifdef _MIPI_STM
typedef struct _MIPI_ConfigObj
{
	uint32_t            TraceBufSize;           /*!< Size in bytes of the trace capture buffer. Needed to set ATB parameters properly */
	uint32_t            SW_MasterMapping;        /*!< Software Master enable register - device specific,
	                                                  see /Example/device for notes on how to set */
	uint32_t            SW_MasterMask;           /*!< Software Master mask register - device specific,
	                                                  see /Example/device for notes on how to set */
	uint32_t            HW_MasterMapping;		/*!< Hardware Master enable register - device specific,
	                                                  see /Example/device for notes on how to set */

}STM_MIPI_ConfigObj;
#endif
typedef struct _DCM_InfoObj
{
	uint32_t  stm_data_flip;
	uint32_t  stpVersion;
	bool      atb_head_required;
	uint32_t  atb_head_present_0;
	uint32_t  atb_head_pointer_0;
	uint32_t  atb_head_present_1;
	uint32_t  atb_head_pointer_1;

}STM_DCM_InfoObj;

/*! \par STMHandle
 
    STMHandle is an incomplete structure pointer provide to the client by STMXport_open().
*/
//This is an incomplete structure used to make the contents of the handle private to the library
typedef struct _STMHandle STMHandle;


// Public Functions Interfaces

/*! \par STMXport_open         
    
    Open the STM Library API. 
    
    \param[in] pSTMBufObj               A pointer to a STMBufObj. To enable blocking IO pSTMBufObj must be NULL.
                                        To enable non-blocking buffered IO pSTMBufObj must not be NULL and the
                                        pSTMBufObj parameters must be valid.In the case of non-blocking buffered
                                        IO the client must provide a maximum amount of heap space that may be allocated
                                        by the STM Library and two DMA channels, defined within pSTMBufObj.
    \param[in] pSTMConfigObj            A pointer to a STM Configuration object. 
                                        Currently used to provide the STM_BaseAddress, STM_ChannelResolution, an optional 
                                        CallBack function pointer and an enable for STMXport_printf STM message optimization.
                                        If STMXport_printf optimization is enabled in the case of %%s, only the pointer to the const char is transported. 
                                        If STMXport_printf optimization is not enabled the entire null terminated string pointed by %%s is transported.
                                        If pSTMConfigObj is NULL then the default configuration is used, which is all configuration parameters are false.
    \return STMHandle pointer 

    \par Details:   
    
    This API opens a physical STM hw module for SW instrumentation data export. The STM Library can be
    opened for either non-blocking buffered IO or blocking IO. In the case of blocking IO the CPU
    is utilized to facilitate STM transfers. In case of non-blocking IO, DMA services are used. When
    the interface is opened for blocking IO, the CPU can be stalled. CPU stalls are avoided if the
    interface is opened for non-blocking, buffered IO. 

    To change the interfaces IO type (between blocking and non-blocking/buffered) the interface must
    be closed and then re-opened.

    Return handle is NULL if open calls failed. Only a single STMHandle can be opened on a processor.

    If STMXport_open exits with a non-NULL STMHandle memory is allocated
    through a call to the client provided external function cTools_memAlloc().
    cTools_memMap(), also a client provided function, is called by STMXport_open to map
    the physical pSTMConfigObj->STM_BaseAddress to a virtual address space.
     
    Prototype for client provided functions:
    \n void * cTools_memAlloc(size_t sizeInBytes) return NULL if memory not allocated
    \n void * cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes) return phyAddr if virtual mapping not required. 	


    \par Build Option Notes:

    Compact Build:

    The compact build only supports enabling the API for blocking mode. If STMXport_open is called with
    pSTMBufObj set to a non-NULL value, it will return NULL rather than a valid pSTMHdl. The compact
    build makes no calls to the RTS Library.

    CIO Build:

    When using the CIO build, STMXport_open must be called with a pSTMBufObj that utilizes the CIO
    definition.
 	
*/

STMHandle* STMXport_open( STMBufObj * pSTMBufObj, STMConfigObj * pSTM_ConfigObj);

#ifdef _CS_STM
/*! \par STMXport_config_CS

    Use this function to configure a CoreSight STM module.

    \param[in] pSTMHandle        A pointer to an STM Handle returned by STMXport_open().
    \param[in] pCSSTMConfigObj   A pointer to a CoreSight STM Configuration object (see :: CS_ConfigObj)

    \par Details:

     This function is used to configure the STM module for remote cases (not using an XDS)
     when you are capturing STM messages with an Embedded Trace Buffer(ETB). Execution
     of this function is optional in cases where you are using an XDS. In that case the Debugger
     performs the STM module configuration for collecting data with the ETB or exporting STM data
     out the pins for capture with the XDS.

     In a multi-core system, where instances of this library may be running on different cores,
     this function should only be called once, from a single core (thus the reason it is not
     integrated in to the STMXport_Open function which is used to setup common transport parameters).
     Regardless of which module type the library is opened for (see ::eSTM_Module) this function may be
     called to configure a CS STM module.

*/
eSTM_STATUS STMXport_config_CS(STMHandle* pSTMHandle, STM_CS_ConfigObj * pCS_configObj);
#endif
#ifdef _MIPI_STM
/*! \par STMXport_config_MIPI

    Use this function to configure a MIPI STM module.

    \param[in] pSTMHandle        A pointer to an STM Handle returned by STMXport_open().
    \param[in] pMIPI_ConfigObj   A pointer to a MIPI Configuration object (see :: MIPI_ConfigObj).

    \par Details:

     This function is used to configure the STM module for remote cases (not using an XDS)
     when you are capturing STM messages with an Embedded Trace Buffer(ETB). Execution
     of this function is optional in cases where you are using an XDS. In that case the Debugger
     performs the STM module configuration for collecting data with the ETB or exporting STM data
     out the pins for capture with the XDS.

     In a multi-core system, where instances of this library may be running on different cores,
     this function should only be called once, from a single core (thus the reason it is not
     integrated in to the STMXport_Open function which is used to setup common transport parameters).
     Regardless of which module type the library is opened for (see ::eSTM_Module) this function may be
     called to configure a MIPI STM module.

*/
eSTM_STATUS STMXport_config_MIPI(STMHandle* pSTMHandle, STM_MIPI_ConfigObj * pMIPI_configObj);
#endif
/*! \par STMXport_getDCMInfo

    Use this function to extract DCM data required to provide back to the decoder utility
    (normally within a file).

    \param[in] pSTMHandle        A pointer to an STM Handle returned by STMXport_open().
    \param[out] pDCM_InfoObj     A pointer to a DCM_InfoObj (see :: MIPI_ConfigObj).

    \par Details:

     This function is used to provide DCM parameters, extracted form the STM module, after
     data collection has been terminated.

*/

eSTM_STATUS STMXport_getDCMInfo(STMHandle* pSTMHandle, STM_DCM_InfoObj  * pDCM_InfoObj);

/*! \par STMXport_printf         
    
    Transport over STM a formatted string pointer and variables  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification). The format
                           string must be defined as a global static.
    \param[in] ...         List of variables corresponding to conversion characters in the
                           format string.
    
    \return eSTM_STATUS    Function's exit status.

    \par Details:   
    
    This function is utilized to transport a pointer to a static formatted string and the values
    defined by the conversion characters within the formatted string. This is similar to a C printf
    call with some additional STM specific parameters. All elements of the transported data are
    transported using 32-bit STM transfers. 

    If data has been buffered or queued previously for any channel,
    that data is transported first.

    If STMXport_printf STM message optimization is enabled (see STMXport_open()), in the case of %%s only its const char pointer is transported. 
    If STMXport_printf STM message optimization is not enabled, in the case of %%s the entire null terminated string pointed by the %%s parameter is transported.
    In this case STM byte transfers are used to eliminate endian conversion issues.
    
    The %%s STMXport_printf STM message optimization is provided for cases where the client is using const char pointers for all %%s cases
    and is not forming strings dynamically. If your application forms strings for use with %%s dynamically 
    you must disable STMXport_printf message optimization (default). 
    
    \par Limitations
    
    The following "C" conversion characters are not supported:
    \li %%p
    \li %%n
    \li %% 

    \par Build Option Notes:

    Compact Build:

    This function is not valid and will return eSTM_ERROR_INVALID_FUNC.
 	
*/

eSTM_STATUS STMXport_printf(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, ... );

/*! \par STMXport_printfV         
    
    Transport over STM a formatted string pointer and variables. This function is identical
    in operation to STMXport_printf(), except that instead of a variable argument list the variables
    are passed through va_list.  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification). The format
                           string must be defined as a global static.
    \param[in] arg_addr    A va_list of variables corresponding to conversion characters in the
                           format string.
    
    \return eSTM_STATUS    Function's exit status.

    \par Details:
    
    See STMXport_printf().
    
    Note that this function is provided in cases where existing client logging functions require the
    use of a va_list. 
    
    This function is also effected by the STMXport_printf optimization configuration parameter.
    See STMXport_printf() for details.
*/

eSTM_STATUS STMXport_printfV(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, va_list arg_addr);

/*! \par STMXport_putMsg         
    
    Transport over STM a user formatted string  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  Pointer to a byte array containing the message to be transported.
    \param[in] iMsgByteCount Number of bytes to be transported from the pMsgString pointer.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details: 
    
    This function is utilized to transport non-static data, starting on any byte address in the most
    efficient manner possible. In the blocking case the data transfer starts with byte and short transfers
    until the data is aligned to a word address, then making as many word transfers as possible, followed
    up with any trailing short and byte transfers on the back-end if required. In the non-blocking buffered
    case data is buffered, aligned to a word address and zero padded. All elements are transported using
    32-bit STM transfers. 

    If data has been buffered or queued previously for any channel,
    that data is transported first.

    \par Build Option Notes:

    Compact Build:

    This function is not valid and will return eSTM_ERROR_INVALID_FUNC.
 	
*/

eSTM_STATUS STMXport_putMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, int32_t iMsgByteCount);

/*! \par STMXport_putBuf         
    
    Transport over STM an element array  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pDataBuf    Pointer to the data buffer array to be transported.
    \param[in] elementSize Size of each element represented in the buffer.
    \param[in] elementCount Total number of elements (of elementSize) in the buffer being transported.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details:  
    
    This function is utilized to transport non-formatted data values. All the data pointed to by
    pDataBuf is transported based on elementSize. 

    If data has been buffered or queued previously for any channel,
    that data is transported first.

    \par Build Option Notes:

    Compact Build:

    This function is valid but will only work in blocking mode.
 	
*/

eSTM_STATUS STMXport_putBuf(STMHandle* pSTMHandle, int32_t chNum, 
                            void* pDataBuf, eSTMElementSize elementSize, int32_t elementCount);

/*! \par STMXport_putTwoBufs         
    
    Transport two element arrays  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pDataBuf1     Pointer to the first data buffer array to be transported.
    \param[in] elementSize1  Size of each element represented in the buffer.
    \param[in] elementCount1 Total number of elements (of elementSize) in the buffer being transported.
    \param[in] pDataBuf2     Pointer to the second data buffer array to be transported.
    \param[in] elementSize2  Size of each element represented in the buffer.
    \param[in] elementCount2 Total number of elements (of elementSize) in the buffer being transported.    
    \return eSTM_STATUS    Function's exit status. 

    \par Details:  
    
    This function is utilized to transport non-formatted data values. All the data pointed to by
    pDataBuf is transported based on elementSize. 

    \par Build Option Notes:

    Compact Build:

    This function is valid but will only work in blocking mode.
*/

eSTM_STATUS STMXport_putTwoBufs(STMHandle* pSTMHandle, int32_t chNum,  
                                void* pDataBuf1, eSTMElementSize elementSize1, int32_t elementCount1,
                                void* pDataBuf2, eSTMElementSize elementSize2, int32_t elementCount2); 

/*! \par STMXport_getBufInfo         
    
    Get Buffered Message Information  
    
    \param[in] pSTMHandle       A pointer to an STM Handle returned by STMXport_open().
    \param[out] msgCnt          Returns the number of outstanding messages.
    \param[out] curMsgBufSize   Returns the number of currently utilized bytes of the space provided
                                by STMBufObj->MaxMsgBufSize currently allocated.

    \return eSTM_STATUS    Function's exit status. 

    \par Details: 
    
    When the interface is opened for non-blocking buffered IO, periodically the user may need to check
    the number of messages queued and flush the buffer to guarantee there are no eSTM_ERROR_BUF_ALLOCATION
    errors.

    \par Important Notes:
    \li When the interface is opened for buffered IO, any errors detected by the STMXport_DMAIntService
        routine will be reported by this function.

    \par Build Option Notes:

    Compact and CIO Build:

    This function is not valid and will return eSTM_ERROR_INVALID_FUNC.
 	
*/

eSTM_STATUS STMXport_getBufInfo(STMHandle* pSTMHandle, uint32_t * msgCnt, uint32_t * curMsgBufSize);

/*! \par STMXport_flush         
    
    Flush Buffered Messages  
    
    \param[in] pSTMHandle       A pointer to an STM Handle returned by STMXport_open().
 
    \return eSTM_STATUS    Function's exit status. 

    \par Details: 
    
    Flush all data buffered or pending.

    \par Build Option Notes:

    Compact Build:

    This function is valid but it performs no useful purpose since the API is in blocking mode. Will always
    return eSTM_SUCCESS.
 	
*/
eSTM_STATUS STMXport_flush(STMHandle* pSTMHandle);

/*! \par STMXport_close         
    
    Close the instance of the STM Library pointed to by pSTMHandle.  
    
    \param[in] pSTMHandle       A pointer to an STM Handle returned by STMXport_open().
 
    \return eSTM_STATUS    Function's exit status. 

    \par Details:  
    
    In the case the library is open for buffered IO, if the client has not flushed the channel,
    the channel will still be closed although any partial transfers already in progress will be completed. 
 	
*/

eSTM_STATUS STMXport_close(STMHandle* pSTMHandle);

/*! \par STMXport_getVersion         
    
    Get the version of the STM Library    
    
    \param[in] pSTMHandle               A pointer to an STM Handle returned by STMXport_open().
    \param[out] pSTMLibMajorVersion     The return pointer for the library's major version.
    \param[out] pSTMLibMinorVersion     The return pointer for the library's minor version. 
 
    \return eSTM_STATUS                 Function's exit status. 

    \par Details:  
    
    This function provides the major and minor version numbers of the libraries build.
    These values can be compared with the following macro values in StmLibrary.h to
    confirm that the version of the library matches that of the header file.

    \li STMLIB_MAJOR_VERSION
    \li STMLIB_MINOR_VERSION

    Major version releases of the same level will be backward compatible with lower minor
    release versions. Any change in the API will be reflected in a change to the major release
    version. The minor release version is typically for bug fixes. 
	
*/

eSTM_STATUS STMXport_getVersion(STMHandle* pSTMHandle, uint32_t * pSTMLibMajorVersion, uint32_t * pSTMLibMinorVersion);

/*! \par STMXport_putWord         
    
    Transport over STM a 32-bit word.  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] data        32-bit value to transport.

    \return eSTM_STATUS    Function's exit status. 

    \par Details:  
    
    This function is utilized to transport a single 32-bit value. This function is a blocking only call
    regardless of which mode the API is opened for.
 	
*/

eSTM_STATUS STMXport_putWord(STMHandle* pSTMHandle, int32_t chNum, uint32_t data);

/*! \par STMXport_putShort         
    
    Transport over STM a 16-bit word.  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] data        16-bit value to transport.

    \return eSTM_STATUS    Function's exit status. 

    \par Details:  
    
    This function is utilized to transport a single 16-bit value. This function is a blocking only call
    regardless of which mode the API is opened for.
 	
*/

eSTM_STATUS STMXport_putShort(STMHandle* pSTMHandle, int32_t chNum, uint16_t data);

/*! \par STMXport_putByte         
    
    Transport over STM an 8-bit word.  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] data        8-bit value to transport.

    \return eSTM_STATUS    Function's exit status. 

    \par Details:
    
    This function is utilized to transport a single 8-bit value. This function is a blocking only call
    regardless of which mode the API is opened for.
 	
*/

eSTM_STATUS STMXport_putByte(STMHandle* pSTMHandle, int32_t chNum, uint8_t data);

/*! \par STMXport_logMsg         
    
    Transport over STM a formatted string and a list of variables whose conversion character types are limited to ints.  
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification) limited to int
                           type conversion characters. The format string must be defined
                           as a global static.
    \param[in] ...         List of int variables corresponding to conversion characters in the
                           format string.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details: 
    
    This function is utilized to transport the pointer to a static formatted string and the
    values defined by the conversion characters within the formatted string with the limitation
    that all conversion characters are ints. All elements 
    are transported using 32-bit STM transfers. This function is a blocking only call regardless
    of which mode the API is opened for. 

    \par Build Option Notes:

    Compact Build:

    This function is not valid and will return eSTM_ERROR_INVALID_FUNC.
 	
*/
eSTM_STATUS STMXport_logMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, ...);

/*! \par STMXport_logMsg0         
    
    Transport over STM a string. Format string contains no conversion characters.   
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification) with no
                           conversion characters. The format string must be defined
                           as a global static.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details:
    
    This function is utilized to transport the pointer to a static formatted string with no
    conversion characters within the formatted string. This is an optimized version of STMXport_logMsg.
    All elements are transported using 32-bit STM transfers. This function is
    a blocking only call regardless of which mode the API is opened for.
 	
*/
eSTM_STATUS STMXport_logMsg0(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString);

/*! \par STMXport_logMsg1         
    
    Transport over STM a formatted string and a single variable whose conversion characters type is limited an int.   
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification) limited to a single int
                           type conversion characters. The format string must be defined
                           as a global static.
    \param[in] parm1       32-bit value associated with the int conversion character to be transferred.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details:
    
    This function is utilized to transport the pointer to a static formatted string and a
    single 32-bit value whose format is defined by a single int conversion character within
    the formatted string. This is an optimized version of STMXport_logMsg. All elements are
    transported using 32-bit STM transfers. This function is a blocking
    only call regardless of which mode the API is opened for.

 	
*/

eSTM_STATUS STMXport_logMsg1(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, uint32_t parm1);

/*! \par STMXport_logMsg2         
    
    Transport over STM a formatted string and two variables whose conversion character types are limited to ints.   
    
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to STM_MAX_CHANNEL(StmSupport.h).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pMsgString  ANSI C style format string (printf specification) limited to two int
                           type conversion characters. The format string must be defined
                           as a global static.
    \param[in] parm1       32-bit value associated with the first int conversion character to be transferred.
    \param[in] parm2       32-bit value associated with the second int conversion character to be transferred.
    
    \return eSTM_STATUS    Function's exit status. 

    \par Details:
    
    This function is utilized to transport the pointer to a static formatted string and a two 32-bit values
    whose format are defined by int conversion characters within the formatted string. This is an optimized
    version of STMXport_logMsg. All elements are transported using 32-bit STM transfers. This function is
    a blocking only call regardless of which mode the API is opened for.
	
*/

eSTM_STATUS STMXport_logMsg2(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, uint32_t parm1, uint32_t parm2);

/*! \par STMXport_setMetaState
 *  
 *  Enable/Disable transport of meta data on demand.
 * 
 *  \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
 *  \param[in] onDemand    Set to "true" to enable Meta On Demand, Set to "false" to disable.
 * 
 *  \return eSTM_STATUS    Function's exit status. 
 * 
 *  \par Details:
 *  
 *  If "true" meta data is only transported by call to STMXport_sendMetaOnDemand (a public function).
 *  In this case the STMXport_putMeta (a private function) is used by other libraries to register
 *  their meta data. If "false" meta data sent to STMXport_putMeta is transported immediately
 *  and STMXport_sendMetaOnDemand is disabled (does nothing). False is the default state.
 */

eSTM_STATUS STMXport_setMetaState(STMHandle* pSTMHandle, bool onDemand);

/*! \par STMXport_getMetaState
 *  
 *  Enable/Disable state of meta data on demand.
 * 
 *  \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
 *  \param[in] onDemand    Set to "true" if Meta On Demand enabled, Set to "false" if disable.
 * 
 *  \return eSTM_STATUS    Function's exit status. 
 * 
 *  \par Details:
 *  
 */

eSTM_STATUS STMXport_getMetaState(STMHandle* pSTMHandle, bool * onDemand);

/*! \par STMXport_sendMetaOnDemand
 *  
 *  Send all meta data.
 * 
 *  \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
 * 
 *  \return eSTM_STATUS    Function's exit status. 
 * 
 *  \par Details:
 * 
 *  If meta onDemand state has been changed to "true" meta data all registered meta data
 *  is transported. When the Meta onDemand state is true, meta data is registered by other
 *  libraries calling STMXport_putMeta (a private function). If meta onDemand state is 
 *  "false" meta data sent to STMXport_putMeta is transported immediately
 *  and STMXport_sendMetaOnDemand is disabled (does nothing). Meta onDemand's default state
 *  is false and must be explicitly set by a call to STMXport_setMetaState.
 *  
 */

eSTM_STATUS STMXport_sendMetaOnDemand(STMHandle* pSTMHandle);



#ifdef __cplusplus
}
#endif

#endif /* __STMLLIBARY_H */
