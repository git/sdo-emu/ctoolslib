/*
 * STMHelper.h
 *
 * STM Helper public API Definitions. 
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

extern void STMLog_ErrHandler( eSTM_STATUS stmStatus );
extern void STMLog_SendInfo(STMHandle * phandle, int iChannel, unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution);
extern void STMLog_CallBack(const char * funcName, eSTM_STATUS stmStatus);
extern STMHandle * STMLog_EnableBufMode(unsigned int maxHeapAllowed, unsigned int dmaBase_Address, int dmaCh_Msg, int dmaCh_Ts, unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution, STMXport_callback arg_pCalBack);
extern STMHandle* STMLog_EnableBlkMode( unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution, int OptPrintf, STMXport_callback arg_pCalBack);
extern void STMLog_EnableCIOMode();
extern void STMLog_checkFlush(STMHandle *phandle);
extern void STMLog_exit(STMHandle *phandle);

