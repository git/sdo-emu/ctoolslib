/*
 * StmSupport.h
 *
 * STM Library support header file. This file is intended for internal use only. 
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef __STMSUPPORT_H
#define __STMSUPPORT_H

#ifdef __cplusplus
#define restrict __restrict__
#endif

#define STM_WORD_SIZE			(4)
#define OST_VERSION				(1 << 24)						//Restrictions: value must be between 1 and 15
#define OST_ENTITY_ID			(1 << 16)
#define OST_PROTOCOL_PRINTF		(0x80 << 8)						//STMXport_printf (STM messages pass %s pointer)
#define OST_PROTOCOL_BYTESTREAM	(0x81 << 8)	                    //STMXport_putMsg
#define OST_PROTOCOL_MSGSTREAM	(0x82 << 8)	                    //STMXport_putBuf
#define OST_PROTOCOL_INTERNALMSG (0x83 << 8)                    //STMXport_IntMsg (Internal Message)
#define OST_PROTOCOL_PRINTF_UNOPTIMIZED (0x84 << 8)             //STMXport_printf (STM messages pass %s byte stream)
#define OST_SHORT_HEADER_LENGTH_LIMIT	(255)

#ifdef C55XP_STM
//C55p is big endiam
	#define L0			(3)
	#define L1			(2)
	#define L2			(1)
	#define L3			(0)
#else
//ARM assumption is little
	#define L0			(0)
	#define L1			(1)
	#define L2			(2)
	#define L3			(3)
#endif

#define MAX_DMA_CHANNEL (31)
#define MAX_DMA_IRQ     (3)
#define STM_MAX_CHANNEL (240)
#define OST_SMALL_MSG   (255)

typedef enum
{
	eByteAlign = 1,
	eShortAlign = 2,
	eShortAndByteAlign = 3,
	eWordAlign = 4
}eAddrAlignment;

//ARM CoreSight STM 2.0 Module Offsets
#define CSSTM_PORT_GUARANTEED_DATA     0x18		//Guaranteed Data port
#define CSSTM_PORT_GUARANTEED_DATA_TS  0x10  	//Guaranteed Data port with timestamp

#define VALIDATE_NULL_VALUE(value) if (NULL == value) return eSTM_ERROR_PARM

#ifndef _CIO
#define RETURN_CALLBACK(pCallBack, retValue) 		if (( NULL != pCallBack ) && ( retValue < 0))  \
	                                                    pCallBack(__FUNCTION__, retValue);         \
	                                                return retValue
#else
#define RETURN_CALLBACK(pCallBack, retValue)        return retValue
#endif

typedef enum
{
	eRegular = 1,
	eTiming = 2
}eAddressType;

typedef struct _STMMsgObj 
{
    void *              pMsg;                   // Pointer to message
    uint32_t            elementCnt;             // number of elements
    eSTMElementSize     elementSize;            // element size
    int32_t             chNum;                  // STM channel id
    bool                DMA_SingleAccessMode;   // Use Burst or Single Access modes
    STMXport_callback   pCallBack;              // Callback function
    bool                DMA_posted;             // message posted to DMA
    struct _STMMsgObj * pNextMsg;               // next message in the link list
    struct _STMMsgObj * pPrevMsg;               // prev message in the link list
}STMMsgObj;

// Internal Functions
#ifndef _CIO
inline uint32_t Compose_Regular_Address(STMHandle * pSTMHandle, int32_t chNum);
inline uint32_t Compose_Timing_Address(STMHandle * pSTMHandle, int32_t chNum);
#endif
void Compose_OST_MSG(const char *pInputBuf, int32_t iInputBufSize, const char *pOSTHeader, int32_t iHeaderSizeInByte, char *pReturnBuf);
uint32_t getSyncCount(uint32_t traceBufSize );
void Build_OST_Header(uint32_t protocolID, uint32_t numberOfBytes, uint32_t *pReturnBuf, int32_t *pBufSizeInBytes);
inline uint32_t Build_CompactOST_Header(uint32_t protocolID, uint32_t numberOfBytes );
inline void Build_ExtendedOST_Header(uint32_t protocolID, uint32_t numberOfBytes, uint32_t *pReturnBuf);
eAddrAlignment AddressAlignmentChecker(uint32_t address);
eSTM_STATUS STM_putCharMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsg, int32_t iMsgByteCount);
eSTM_STATUS  SendPrintf_CharStream(STMHandle * pSTMHandle, int32_t chNum, const char* pFmtString, ... );
eSTM_STATUS SendVprintf_CharStream(STMHandle * pSTMHandle, int32_t chNum, const char* pFmtString, va_list arg_list );

//External Functions
extern void * cTools_memAlloc(size_t sizeInBytes);
extern void   cTools_memFree(void *);
extern void * cTools_memMap(uint32_t phyAddress, size_t mapSizeInBytes);
extern void   cTools_memUnMap(void * vAddress, size_t mapSizeInBytes);
#ifdef RUNTIME_DEVICE_SELECT
extern uint32_t cTools_reg_rd32(uint32_t addr);
extern void cTools_reg_wr32(uint32_t addr, uint32_t value);
#endif

//Exported Functions meant to be used by other Libraries
/*! \par STMExport_IntMsg         
    
    Internal Message: This method is meant to be used by other STM Libraries to provide meta data or
    logging messages. It will transport over STM all the elements required to form a complete message
    sample. This function does not require any knowledge by the decoder as to which library was the source
    of the data.  
   
    \param[in] pSTMHandle  A pointer to an STM Handle returned by STMXport_open().
    \param[in] chNum       A STM Channel number in the range of 0 to 254 (255 reserved).
                           The most common usage is to identify messages coming from
                           different OS tasks or processes. It's expected the application
                           will manage it's channel usage.
    \param[in] pModuleName Pointer to a global static Module Name string  
    \param[in] pDomainName Pointer to a global static Domain Name string
    \param[in] pDataClass  Pointer to a global static Data Class string
    \param[in] pDataType   Pointer to a global static Data Type string
    \param[in] pDataMsg    Pointer to a global static Data Message string
    \param[in] pData       Pointer to a unsigned int Data value

    \return eSTM_STATUS    Function's exit status. 

    \par Details:
    \details   
    
    This function is utilized to transport the pointer to a static strings and a 32-bit data value.
    All elements are transported using 32-bit STM transfers. 
	
*/
eSTM_STATUS STMExport_IntMsg (STMHandle * pSTMHandle, int32_t chNum, 
                              const char * pModuleName, const char * pDomainName,
                              const char * pDataClass, const char * pDataType,
                              const char * pDataMsg, uint32_t *  pData);

eSTM_STATUS STMExport_putMeta(STMHandle* pSTMHandle, const char* pMsgString, int32_t iMsgByteCount);

#pragma CODE_SECTION(STMExport_IntMsg,      ".text:STMLibraryFast")
#pragma CODE_SECTION(STMExport_putMeta,     ".text:STMLibraryFast")

// Msg handling routines
eSTM_STATUS STM_putBufCPU(STMHandle * pHandle, int32_t iChNum, void * pHdrBuf, int32_t iHdrWrdCnt, void * pMsgBuf, uint32_t iElementCnt, eSTMElementSize iElementSize);
eSTM_STATUS STM_putMsgCPU(STMHandle * pHandle, int32_t iChNum, void * pHdrBuf, int32_t iHdrWrdCnt, void * pMsgBuf, uint32_t iMsgByteCnt);
eSTM_STATUS STM_putDMA(STMHandle* pHandle, int32_t iChNum, void* pMsgBuf, uint32_t iElementCnt, eSTMElementSize iElementSize, bool DMASingleAccessMode, STMXport_callback pCallBack);
eSTM_STATUS STM_printfV(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, va_list arg_addr);

//DMA Helper routines
eSTM_STATUS DMA_checkStatus(STMHandle* pHandle);
void DMA_serviceNextMsg(STMHandle* pHandle);
void DMA_postMsg(STMHandle* pHandle, STMMsgObj * msgObj);

//Common MIPI and ARM definitions
#ifdef RUNTIME_DEVICE_SELECT
#define reg_rd32(addr) cTools_reg_rd32((uint32_t)addr)
#define reg_wr32(addr, value) cTools_reg_wr32((uint32_t)addr, value)
#else
#define reg_rd32(addr) (*addr)
#define reg_wr32(addr,value) (*addr = value)
#endif

#define STM_CNTLREG_BLKSIZE          4096
#define STM_UNLOCK_VALUE             0xC5ACCE55

#define STM_CS_REGOFF_Lock           0xFB0
#define STM_CS_REGOFF_LockStatus     0xFB4

#define STM_CS_LOCK(ba)              ((volatile uint32_t *)((uint32_t)(ba) + STM_CS_REGOFF_Lock))
#define STM_CS_LOCKSTATUS(ba)        ((volatile uint32_t *)((uint32_t)(ba) + STM_CS_REGOFF_LockStatus))

#define STM_FLUSH_RETRY       1024
#define STM_CLAIM_RETRY       100

//STM ATB IDs
#define STM_ATB_ID_MIPI              0x40
#define STM_ATB_ID_ARM2_0            0x41

// MIPI STM Control registers offsets
#define STM_MIPI_REGOFF_Id           0x000
#define STM_MIPI_REGOFF_SysConfig    0x010
#define STM_MIPI_REGOFF_SysStatus    0x014
#define STM_MIPI_REGOFF_SWMstCntl_0  0x024
#define STM_MIPI_REGOFF_SWMstCntl_1  0x028
#define STM_MIPI_REGOFF_SWMstCntl_2  0x02C
#define STM_MIPI_REGOFF_SWMstCntl_3  0x030
#define STM_MIPI_REGOFF_SWMstCntl_4  0x034
#define STM_MIPI_REGOFF_HWMstCntl    0x038
#define STM_MIPI_REGOFF_PTIConfig    0x03C
#define STM_MIPI_REGOFF_PTICntDown   0x040
#define STM_MIPI_REGOFF_ATBConfig    0x044
#define STM_MIPI_REGOFF_ATBSyncCnt   0x048
#define STM_MIPI_REGOFF_ATBHeadPtr   0x048     //Same offset as ATBSyncPtr so we must know the revision of the STM unit.
#define STM_MIPI_REGOFF_ATBid        0x04C


#define STM_MIPI_ID_REG(ba)          ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_Id))
#define STM_MIPI_SYSCNFG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SysConfig))
#define STM_MIPI_SYSSTAT(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SysStatus))
#define STM_MIPI_SWMSTCNTL0(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SWMstCntl_0))
#define STM_MIPI_SWMSTCNTL1(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SWMstCntl_1))
#define STM_MIPI_SWMSTCNTL2(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SWMstCntl_2))
#define STM_MIPI_SWMSTCNTL3(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SWMstCntl_3))
#define STM_MIPI_SWMSTCNTL4(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_SWMstCntl_4))
#define STM_MIPI_HWMSTCNTL(ba)       ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_HWMstCntl))
#define STM_MIPI_PTICNFG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_PTIConfig))
#define STM_MIPI_PTICNTDOWN(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_PTICntDown))
#define STM_MIPI_ATBCNFG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_ATBConfig))
#define STM_MIPI_ATBHEADPTR(ba)	     ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_ATBHeadPtr))
#define STM_MIPI_ATBSYNCCNT(ba)	     ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_ATBSyncCnt))
#define STM_MIPI_ATBID(ba)	         ((volatile uint32_t *)((uint32_t)(ba) + STM_MIPI_REGOFF_ATBid))

//MIPI STM specific definitions
#define STM_MIPI_NUM_CHANNELS 256
#define STM_FIFO_EMPTY        (1<<8)
#define STM_MAJOR_MASK        (7<<8)
#define STM_MAJOR_STM2_0      0x400

//STM_MIPI_SWMSTCNTL0 definitions
#define OWNERSHIP_MASK     (3<<30)
#define MOD_AVAILABLE      (0<<30)
#define MOD_CLAIMED        (1<<30)
#define MOD_ENABLED        (2<<30)
#define DEBUG_OVERRIDE     (1<<29)
#define CURRENT_OWNER_MASK (1<<28)
#define CURRENT_DEBUG_OWNS (0<<28)
#define CURRENT_APP_OWNS   (1<<28)
#define STM_TRACE_EN       (1<<16)

//ARM STM Control registers
#define STM_ARM_REGOFF_StimPortEn        0xE00
#define STM_ARM_REGOFF_StimPortTrgEn     0xE20
#define STM_ARM_REGOFF_StimPortSelCfg    0xE60
#define STM_ARM_REGOFF_StimPortMstSelCfg 0xE64
#define STM_ARM_REGOFF_CntlStat          0xE80
#define STM_ARM_REGOFF_SyncCntl          0xE90
#define STM_ARM_REGOFF_DevCfg            0xFC8

#define STM_ARM_PORTEN(ba)           ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_StimPortEn))
#define STM_ARM_PORTTRGEN(ba)        ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_StimPortTrgEn))
#define STM_ARM_PORTSELCFG(ba)       ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_StimPortSelCfg))
#define STM_ARM_PORTMSTSELCFG(ba)    ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_StimPortMstSelCfg))
#define STM_ARM_CNTLSTAT(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_CntlStat))
#define STM_ARM_SYNCCNTL(ba)         ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_SyncCntl))
#define STM_ARM_NUMSTMPORTS(ba)      ((volatile uint32_t *)((uint32_t)(ba) + STM_ARM_REGOFF_DevCfg))

//ARM STM specific definitions
#define STM_ARM_CHN_RESOLUTION 256

//ARM STM CNTL STAT Register Definitions (Trace Control and Status Register - STMTCSR)
#define CSSTM_CTRL_STAT_BUSY       (1 << 23)       // STM unit is busy (STM FIFO full)
#define CSSTM_CTRL_STAT_TRACEID(x) (x << 16)       // Trace id (ATB ID)
#define CSSTM_CTRL_STAT_COMPEN     (1 << 5)        // Compression enable - this should normally be disabled for TI sw compatibility
#define CSSTM_CTRL_STAT_SYNCEN     (1 << 2)        // Enable sync packets
#define CSSTM_CTRL_STAT_TSEN       (1 << 1)        // Timestamp Enable
#define CSSTM_CTRL_STAT_EN         (1 << 0)        // Global STM Enable



#ifndef _CIO
//#pragma CODE_SECTION(Compose_Address, 			".text:STMLibraryFast")
#endif
#pragma CODE_SECTION(Compose_OST_MSG, 			".text:STMLibraryFast")
#pragma CODE_SECTION(Build_OST_Header, 			".text:STMLibraryFast")
#pragma CODE_SECTION(AddressAlignmentChecker, 	".text:STMLibraryFast")
#pragma CODE_SECTION(STM_putBufCPU, 			".text:STMLibraryFast")
#pragma CODE_SECTION(STM_putMsgCPU, 			".text:STMLibraryFast")

#endif /* __STMSUPPORT_H */
