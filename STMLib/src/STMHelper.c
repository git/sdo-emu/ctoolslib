/*
 * STMHelper.c
 *
 * STM helper functions. These functions provide higher level support for 
 * opening the STM Library in different modes (Blocking, Bufferred and CIO), 
 * error handling, implementations of required external functions for mapping
 * the STM unit to a virtual address space and memory allocation, version
 * logging, and error callback support.   
 *
 * Copyright (C) 2009,2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  Include Header Files  */
#include "StmLibrary.h" 
#include <stdio.h>
#include <stdlib.h>

#if _LINUX
#include <sys/mman.h>
#include <fcntl.h>
#endif

// The following helper code is optional

void STMLog_ErrHandler( const char * funcName, eSTM_STATUS stmStatus )
{

#ifdef _DEBUG
	char * errMsg = 0;

	switch ( stmStatus ) {
		case eSTM_SUCCESS:
			errMsg = "stmStatus is eSTM_SUCCESS - nothing wrong";
			break;
		case eSTM_PENDING:
			errMsg = "stmStatus is eSTM_PENDING - pending waiting for the dma channel";
			break;
		case eSTM_ERROR_PARM:
			errMsg = "stmStatus is eSTM_ERROR_PARM - API parameter error ";
			break;
		case eSTM_ERROR_STRING_FORMAT:
			errMsg = "stmStatus is eSTM_ERROR_STRING_FORMAT - Format string error detected";
			break;
		case eSTM_ERROR_HEAP_ALLOCATION:
			errMsg = "stmStatus is eSTM_ERROR_HEAP_ALLOCATION - Malloc allocation returned null";
			break;
		case eSTM_ERROR_BUF_ALLOCATION:
			errMsg = "stmStatus is eSTM_ERROR_BUF_ALLOCATION - Requested buffer allocation will exceed max provided STMXport_open";
			break;
		case eSTM_ERROR_INVALID_FUNC:
			errMsg = "stmStatus is eSTM_ERROR_INVALID_FUNC - API call not support for this build";
			break;
		case eSTM_ERROR_CIO_ERROR:
			errMsg = "stmStatus is eSTM_ERROR_CIO_ERROR - CIO Error";
			break;
		default:
			errMsg = "stmStatus is unknown";
			break;	
	}

	printf("STMLog_ErrHandler:: Error in function %s(), %s\n", funcName, errMsg);

#endif

	if (   ( eSTM_SUCCESS == stmStatus )
		|| ( eSTM_ERROR_INVALID_FUNC == stmStatus ) ) {
		return;
	}

#ifdef _DEBUG
	printf("STMLog_ErrHandler: Execution Aborted\n");
#endif

}

void STMLog_SendInfo(STMHandle * phandle, int iChannel, unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution)
{
	unsigned int major_version = (unsigned int)-1, minor_version = (unsigned int)-1;
	eSTM_STATUS stmStatus;

#ifndef _CIO
    stmStatus = STMXport_printf( phandle, iChannel, "Demo::initLog base address 0x%x channel resolution 0x%x\0", STM_BaseAddress, STM_ChannelResolution);
	if (stmStatus < eSTM_SUCCESS )	STMLog_ErrHandler("STMXport_printf", stmStatus );
#endif
	stmStatus = STMXport_getVersion( phandle, &major_version, &minor_version );
	if (stmStatus < eSTM_SUCCESS )	STMLog_ErrHandler("STMXport_getVersion", stmStatus );
	
	stmStatus = STMXport_printf( phandle, iChannel, "Demo::initLog STM Library revision is %d.%d\0", major_version, minor_version);
	if (stmStatus < eSTM_SUCCESS )	STMLog_ErrHandler( "STMXport_printf", stmStatus );

}

void STMLog_CallBack(const char * funcName, eSTM_STATUS stmStatus)
{
    
	if (stmStatus < eSTM_SUCCESS ) {
        STMLog_ErrHandler( funcName, stmStatus );
	}
	
}

#if 0
STMHandle * STMLog_EnableBufMode(unsigned int maxHeapAllowed, unsigned int dmaBase_Address, int dmaCh_Msg, int dmaCh_Ts, unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution, STMXport_callback arg_pSTMCallBack )
{
#if defined(_STM) || (defined(_COMPACT) && !defined(_CIO))
    STMHandle * phandle;
	STMBufObj * pSTMBufInfo;
    STMBufObj STMBufInfo;
    STMConfigObj STMConfigInfo; 
    
    STMConfigInfo.optimize_strings = true;
    STMConfigInfo.STM_BaseAddress = STM_BaseAddress;
    STMConfigInfo.STM_ChannelResolution = STM_ChannelResolution;
    STMConfigInfo.pCallBack = arg_pSTMCallBack;

	STMBufInfo.maxBufSize = maxHeapAllowed;
    STMBufInfo.DMA4_BaseAddress = dmaBase_Address;
	STMBufInfo.DMAChan_0 = dmaCh_Msg;
	STMBufInfo.DMAChan_1 = dmaCh_Ts;
	STMBufInfo.DMAIrq = 0;
	STMBufInfo.usingDMAInterrupt = false;

	//Using the callback allows us to check status and respond to errors without
	//adding the explicit status test after every API call

	pSTMBufInfo = & STMBufInfo;

	phandle = STMXport_open(pSTMBufInfo, &STMConfigInfo);
	if ( phandle == 0 ) exit(EXIT_FAILURE);
#ifdef _DEBUG
    {
        int iChannel = 0;    
	   
        STMXport_printf( phandle, iChannel, "Demo::STMLog_EnableBufMode API initialized for buffered non-blocking calls\0");
       
	    STMLog_SendInfo(phandle, iChannel, STM_BaseAddress, STM_ChannelResolution);
    }
#endif
    return phandle;
#endif
#ifdef _CIO
	printf ("In CIO mode STMLog_EnableBufMode() call is not valid\n");
    return NULL; 
#endif
}
#endif


STMHandle * STMLog_EnableBlkMode( unsigned int STM_BaseAddress, unsigned int STM_ChannelResolution, int OptPrintf, STMXport_callback arg_pSTMCallBack)
{
#if defined(_STM) || (defined(_COMPACT) && !defined(_CIO))
#ifdef _DEBUG
	eSTM_STATUS stmStatus;
#endif
    STMHandle * phandle;
	STMBufObj * pSTMBufInfo = NULL; //Required to enable block mode
    STMConfigObj STMConfigInfo; 

    STMConfigInfo.optimize_strings = ( 0 == OptPrintf ) ? false : true;
    STMConfigInfo.STM_XportBaseAddr = STM_BaseAddress;
    STMConfigInfo.STM_ChannelResolution = STM_ChannelResolution;
    STMConfigInfo.pCallBack = arg_pSTMCallBack;
 
	phandle = STMXport_open( pSTMBufInfo, &STMConfigInfo);
	if ( phandle == 0 ) exit(EXIT_FAILURE);
#ifdef _DEBUG
    {
       int iChannel = 0;
       
	   //This is an error handling example with callbacks diabled - the remainder of this example app assumes callbacks are enabled
       stmStatus = STMXport_printf( phandle, iChannel, "Demo::STMLog_EnableBlkMode API initialized for blocking calls\0");
	   if (stmStatus < eSTM_SUCCESS )	STMLog_ErrHandler("STMXport_printf", stmStatus );

	   STMLog_SendInfo(phandle, iChannel, STM_BaseAddress, STM_ChannelResolution);
    }
#endif
    return phandle;
#endif
#ifdef _CIO
	printf ("In CIO mode STMLog_EnableBlkMode() call is not valid\n");
    return NULL; 
#endif

}

void STMLog_EnableCIOMode()
{
#ifdef _CIO
	eSTM_STATUS stmStatus;
	STMBufObj STMBufInfo;
	STMBufObj * pSTMBufInfo;

//	pSTMCallBack = 0; //Lets live dangerously and disable external callbacks

#ifdef _DEBUG
// The following code is intended as a test of mmap and munmap.
// To port this to your system change the phyAddr and phySize
// to a valid memory area in your system. There is no attempt to 
// actually access the location. 
	{
		unsigned int phyAddr = 0xA0000; //PC's Video Ram Address
		unsigned int phySize = 1024; 
		void * newVAddr = cTools_memMap(phyAddr, phySize);
		printf ("STMLog_EnableCIOMode:: cTools_memMap Test newVAddr is 0x%X\n", newVAddr);
		cTools_memUnMap(newVAddr, phySize);

	}
#endif
	
	STMBufInfo.fileAppend = false;
	STMBufInfo.pFileName = NULL; //"C:\\CCStudio_v3.3\\MyProjects\\STMLog.csv"; // NULL for stdout
	STMBufInfo.pMasterId = "myARM";
 	pSTMBufInfo = & STMBufInfo;

	phandle = STMXport_open(pSTMBufInfo);
	if ( phandle == 0 ) exit(EXIT_FAILURE);
#ifdef _DEBUG
	//This is an error handling example with callbacks diabled - the remainder of this example app assumes callbacks are enabled
    stmStatus = STMXport_printf( phandle, iChannel, "Demo::STMLog_EnableCIOMode API initialized for CIO\0");
	if (stmStatus < eSTM_SUCCESS )	STMLog_ErrHandler( stmStatus );


	STMLog_SendInfo();

#endif
#endif
}

void STMLog_checkFlush(STMHandle *phandle)
{
#if defined(_STM) || defined (_COMPACT)
	eSTM_STATUS stmStatus;
	uint32_t msgCnt, heapUsed;

	stmStatus = STMXport_getBufInfo( phandle, &msgCnt, &heapUsed );
	if (stmStatus < eSTM_SUCCESS ) STMLog_ErrHandler( "STMXport_getBufInfo", stmStatus );

#ifdef _DEBUG    
    printf ( "Demo::STMLog_checkFlush before flush - outstanding messages %d heap used %d\0", msgCnt, heapUsed);
#endif

	//If there are any outstanding messages or any heap still allocated the flush will clean all this up
	if ( ( msgCnt > 0 ) || (heapUsed != 0) ) {
		STMXport_flush(phandle);
	}
#ifdef _DEBUG
	//This is for test purposes to confirm the flush worked as expected
	stmStatus = STMXport_getBufInfo( phandle, &msgCnt, &heapUsed );
	if (stmStatus < eSTM_SUCCESS ) STMLog_ErrHandler( "STMXport_getBufInfo", stmStatus );
    
	printf("Demo::STMLog_checkFlush after flush - outstanding messages %d heap used %d\n", msgCnt, heapUsed);
#endif
#endif
#ifdef _CIO
	//DO the CIO flush
	STMXport_flush(phandle);
#endif


}



void STMLog_exit(STMHandle * phandle)
{

	STMLog_checkFlush(phandle);
	STMXport_close (phandle);	

}

