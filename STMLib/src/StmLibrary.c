/*
 * StmLibrary.C
 *
 * STM Library API Implementation 
 *
 * Copyright (C) 2009-2012 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>              // The library uses C99 exact-width integer types
#include "StmLibrary.h"
#include "StmSupport.h"

#if defined(_MIPI_STM) && !defined(_CS_STM)
#define STM_TYPE eMIPI_STM
#endif
#if !defined(_MIPI_STM) && defined(_CS_STM)
#define STM_TYPE eCS_STM_20
#endif


//Note: the contents of the handle is private to the library and is subject to change with revision

#ifdef _CIO
struct _STMHandle
{
    FILE *       pLogFileHdl;
    STMBufObj    BufIOInfo;

};
#else

typedef struct _MetaObj {
    const char * pMsg;
    int32_t msgByteCnt;
    struct _MetaObj * next;
    struct _MetaObj * prev;
} MetaObj;    

struct _STMHandle
{
	uint32_t vCntlBaseAddr;
	uint32_t vXportBaseAddr;
    uint32_t chResolution;
    uint32_t numChannels;
    uint32_t stpVersion;
    uint32_t regularPortOffset;
    uint32_t timingPortOffset;
    STMXport_callback pCallBack;
    bool optimize_strings;
    eSTM_XmitPrintfMode xmit_printf_mode;
    bool * pUnOptPrintfMap;
    bool metaOnDemand;
    MetaObj * pMetaObj;
    STMMsgObj * pHeadMsgObj;
    bool bigEndian;
};
#endif

//STPv2 SYNC COUNT table
static uint32_t sync_count_table[] = {
    0,     // eBUFSIZE_0
	245,   //eBUFSIZE_4K
	355,   //eBUFSIZE_8K
	510,   //eBUFSIZE_16K
	730,   //eBUFSIZE_32K
	1040,  //eBUFSIZE_64K
	1480,  //eBUFSIZE_128K
	2090,  //eBUFSIZE_256K
};


static bool STMisOpen = false;
#ifdef _CIO

static char * STM_FrmtStringTop; 
static char * STM_FrmtStringHdr;
static char * STM_FrmtStringMsg1; 
static char * STM_FrmtStringMsg;

static char * STM_FrmtStringTop_STDIO = "Master ID  Chan Num  Status                  Message/Data\n";
static char * STM_FrmtStringHdr_STDIO = "%-10.10s   %-3.3d     %-22.22s  ";
static char * STM_FrmtStringMsg1_STDIO = "0x%8.8X\n";
static char * STM_FrmtStringMsg_STDIO = "                                             0x%8.8X\n";

static char * STM_FrmtStringTop_FILEIO = "Master ID,Chan Num,Status,Message,Data\n";
static char * STM_FrmtStringHdr_FILEIO = "%s,%d,%s,";
static char * STM_FrmtStringMsg1_FILEIO = ",0x%8.8X\n";
static char * STM_FrmtStringMsg_FILEIO = ",,,,0x%8.8X\n";
#endif

#if defined(_COMPACT) || defined(_CIO)
// Compact and CIO builds uses a pre-allocated handle object to avoid malloc 
STMHandle STMHandleObj;
#endif

#ifndef _COMPACT
#define SIZE_OF_TEMPBUF 32
static char tempBuf[SIZE_OF_TEMPBUF];
#endif

#define ENDIAN_SWAP(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )
#define ENDIAN_SWAP_SHORT(n) ((n>>8) | (n<<8))

eSTM_STATUS STMXport_getVersion(STMHandle* pSTMHandle, uint32_t * pSTMLibMajorVersion, uint32_t * pSTMLibMinorVersion)
{
#ifdef _DEBUG
	VALIDATE_NULL_VALUE(pSTMHandle);
	VALIDATE_NULL_VALUE(pSTMLibMajorVersion);
	VALIDATE_NULL_VALUE(pSTMLibMinorVersion);	 
#endif

	*pSTMLibMajorVersion = STMLIB_MAJOR_VERSION;
	*pSTMLibMinorVersion = STMLIB_MINOR_VERSION;	

	return eSTM_SUCCESS;
}

STMHandle* STMXport_open( STMBufObj * pSTMBufObj, STMConfigObj * pSTMConfigObj)
{
	STMHandle *pSTMHandle = NULL;
	uint32_t numChannels;
	uint32_t chResolution;
	uint32_t stpVersion;
	uint32_t regularPortOffset;
	uint32_t timingPortOffset;

#ifdef _CIO
	if ( NULL == pSTMBufObj) {
	    //Error - CIO version requires buffer object to define filename and master id, return NULL
		return pSTMHandle;
	}
#endif


#ifndef _CIO
	if ( NULL == pSTMConfigObj ) {
        //Error - Must call STMXport_open with a valid pSTMConfigObj pointer.
        return pSTMHandle;
    }
#endif
    //If interface already open return NULL handle
	if ( false == STMisOpen )  
	{
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO) )
		//Map the physical STM control register base address to a virtual address
		void * vSTM_CntlBaseAddr = cTools_memMap(  pSTMConfigObj->STM_CntlBaseAddr, STM_CNTLREG_BLKSIZE);
		if ( NULL == vSTM_CntlBaseAddr ){
             return pSTMHandle;	//Return NULL handle on mapping error
		}

		//Determine the number of STM channels and the channel resolution
#ifdef _MIPI_STM
			numChannels = STM_MIPI_NUM_CHANNELS;
			chResolution = pSTMConfigObj->STM_ChannelResolution;
			stpVersion = ( STM_MAJOR_STM2_0 > ( reg_rd32(STM_MIPI_ID_REG(vSTM_CntlBaseAddr)) & STM_MAJOR_MASK ) ) ? 1 : 2;
        	regularPortOffset = 0;
            timingPortOffset = pSTMConfigObj->STM_ChannelResolution/2;
#endif
#ifdef _CS_STM
			numChannels = reg_rd32(STM_ARM_NUMSTMPORTS(vSTM_CntlBaseAddr));
			chResolution = STM_ARM_CHN_RESOLUTION;                   //ARM channel resolution is fixed
			stpVersion = 2;
           	regularPortOffset = CSSTM_PORT_GUARANTEED_DATA;;
            timingPortOffset = CSSTM_PORT_GUARANTEED_DATA_TS;
#endif

		//Map the physical STM transport base address to a virtual address
        void * vSTM_XportBaseAddr = cTools_memMap(  pSTMConfigObj->STM_XportBaseAddr, numChannels*chResolution);
        if ( NULL == vSTM_XportBaseAddr )  {
        	cTools_memUnMap((void *)vSTM_CntlBaseAddr, STM_CNTLREG_BLKSIZE);
			return pSTMHandle;	//Return NULL handle on mapping error
		}

		//Allocate space for interface handle
#ifdef _COMPACT
		pSTMHandle = &STMHandleObj;
#else
		pSTMHandle = (STMHandle *) cTools_memAlloc(sizeof(STMHandle));
		if (NULL != pSTMHandle)
#endif
		{
			// Save parameters to interface handle
			pSTMHandle->vCntlBaseAddr =(uint32_t)vSTM_CntlBaseAddr;
			pSTMHandle->vXportBaseAddr =(uint32_t)vSTM_XportBaseAddr;
            pSTMHandle->numChannels =  numChannels;
            pSTMHandle->stpVersion = stpVersion;
            pSTMHandle->chResolution = chResolution;
            pSTMHandle->metaOnDemand = false;
            pSTMHandle->pMetaObj = NULL;

            pSTMHandle->pCallBack = pSTMConfigObj->pCallBack;
            pSTMHandle->optimize_strings =  pSTMConfigObj->optimize_strings;
            pSTMHandle->xmit_printf_mode = pSTMConfigObj->xmit_printf_mode;
            pSTMHandle->pUnOptPrintfMap = NULL;

        	pSTMHandle->regularPortOffset = regularPortOffset;
            pSTMHandle->timingPortOffset = timingPortOffset;

			STMisOpen = true;

			//Check device endianness.  This is done at runtime to avoid
			//discrepancies with pre-defined macros accross TI and gcc compilers.
			pSTMHandle->bigEndian = false;
			uint32_t i = 1;
			char *le = (char *) &i;
			if(le[0] != 1)
				pSTMHandle->bigEndian = true;

		} //End of - if (NULL != pSTMHandle)
#endif
#ifdef _CIO
		//Utilize pre-allocated handle object
		pSTMHandle = &STMHandleObj;

		STMHandleObj.BufIOInfo = * pSTMBufObj;
		if ( NULL == pSTMBufObj->pFileName )
		{
			STMHandleObj.pLogFileHdl = stdout;
			
			STM_FrmtStringTop = STM_FrmtStringTop_STDIO; 
			STM_FrmtStringHdr = STM_FrmtStringHdr_STDIO;
			STM_FrmtStringMsg1 = STM_FrmtStringMsg1_STDIO; 
			STM_FrmtStringMsg = STM_FrmtStringMsg_STDIO;	
		}
		else {		     
			char * fileMode = ( false == pSTMBufObj->fileAppend ) ? "w" : "a";
			STMHandleObj.pLogFileHdl = fopen( pSTMBufObj->pFileName, fileMode );
			if ( NULL == STMHandleObj.pLogFileHdl )
			{
				//Could not open file so return NULL
				return NULL;
			}

			STM_FrmtStringTop = STM_FrmtStringTop_FILEIO; 
			STM_FrmtStringHdr = STM_FrmtStringHdr_FILEIO;
			STM_FrmtStringMsg1 = STM_FrmtStringMsg1_FILEIO; 
			STM_FrmtStringMsg = STM_FrmtStringMsg_FILEIO;
		}
			
		if ( 0 > fprintf(STMHandleObj.pLogFileHdl, STM_FrmtStringTop) )
		{
			//Could not write to file so return NULL
			return NULL;
		}
		
		STMisOpen = true;
#endif
					
	} //End of - if ( false == STMisOpen )

	return pSTMHandle;
}

#ifdef _MIPI_STM
eSTM_STATUS STMXport_config_MIPI(STMHandle* pSTMHandle, STM_MIPI_ConfigObj * pMIPI_ConfigObj)
{

	int claimRetry = STM_CLAIM_RETRY;

	// If the Debugger already owns the STM unit then exit (can rely on Debugger to configure properly).
	uint32_t swmctrl0_reg = reg_rd32(STM_MIPI_SWMSTCNTL0(pSTMHandle->vCntlBaseAddr));
	if (   (swmctrl0_reg & (OWNERSHIP_MASK | CURRENT_OWNER_MASK)) == (MOD_ENABLED | CURRENT_DEBUG_OWNS)
		|| (swmctrl0_reg & (OWNERSHIP_MASK | CURRENT_OWNER_MASK)) == (MOD_CLAIMED | CURRENT_DEBUG_OWNS))
	{
		return eSTM_SUCCESS;
	}

	// Unlock STM module
	reg_wr32(STM_CS_LOCK(pSTMHandle->vCntlBaseAddr), STM_UNLOCK_VALUE);

	// Claim it for the Application but leave it in Debug override
	// mode allowing the Debugger to take control of it at any time.
	reg_wr32(STM_MIPI_SWMSTCNTL0(pSTMHandle->vCntlBaseAddr), 0);
	reg_wr32(STM_MIPI_SWMSTCNTL0(pSTMHandle->vCntlBaseAddr), MOD_CLAIMED | DEBUG_OVERRIDE);

	// Check the claim bit or already enabled and application owns the unit
	do {
		swmctrl0_reg = reg_rd32(STM_MIPI_SWMSTCNTL0(pSTMHandle->vCntlBaseAddr));
	} while (  ((swmctrl0_reg & (OWNERSHIP_MASK | CURRENT_OWNER_MASK)) != (MOD_CLAIMED | CURRENT_APP_OWNS))
			&& ((swmctrl0_reg & (OWNERSHIP_MASK | CURRENT_OWNER_MASK)) != (MOD_ENABLED | CURRENT_APP_OWNS))
			&& ( 0 != claimRetry--) );

	if ( 0 >= claimRetry ) {
		return eSTM_ERROR_OWNERSHIP_NOT_GRANTED;
	}

	// Enable Masters
	reg_wr32(STM_MIPI_SWMSTCNTL1(pSTMHandle->vCntlBaseAddr), pMIPI_ConfigObj->SW_MasterMapping);
	reg_wr32(STM_MIPI_SWMSTCNTL2(pSTMHandle->vCntlBaseAddr), pMIPI_ConfigObj->SW_MasterMask);
	reg_wr32(STM_MIPI_SWMSTCNTL3(pSTMHandle->vCntlBaseAddr), 0x07070707);
	reg_wr32(STM_MIPI_SWMSTCNTL4(pSTMHandle->vCntlBaseAddr), 0x07070707);
	reg_wr32(STM_MIPI_HWMSTCNTL(pSTMHandle->vCntlBaseAddr), pMIPI_ConfigObj->HW_MasterMapping);

	//Set STM PTI output to gated mode, 4 bit data and dual edge clocking
	reg_wr32(STM_MIPI_PTICNFG(pSTMHandle->vCntlBaseAddr), 0x000000A0);
	//Set STM master ID generation frequency for HW messages - not used for any SW messages
	reg_wr32(STM_MIPI_PTICNTDOWN(pSTMHandle->vCntlBaseAddr), 0xFC);


	if ( STM_MAJOR_STM2_0 > ( reg_rd32(STM_MIPI_ID_REG(pSTMHandle->vCntlBaseAddr)) & STM_MAJOR_MASK ) )
	{
		// MIPI STM 1.0

		// Enable ATB interface for ETB. Repeat master ID every 8 x 8 instrumentation access and
		// repeat channel ID after F x 8 instrumentation access from master.
		// This is needed to optimize ETB buffer in circular mode.
		reg_wr32(STM_MIPI_ATBCNFG(pSTMHandle->vCntlBaseAddr), 0x0000F800);
		reg_wr32(STM_MIPI_ATBCNFG(pSTMHandle->vCntlBaseAddr), 0x0001F800);
	}
	else {
		// MIPI STM 2.0

		// Enable ATB interface for ETB. Since STP 2.0 ASYNC being used, the Master
		// and Channel repeats can be set to 0. The Async packet will provide new
		// masters and channel numbers.
		reg_wr32(STM_MIPI_ATBCNFG(pSTMHandle->vCntlBaseAddr), 0x0);

		//Set the ATB ID
		reg_wr32(STM_MIPI_ATBID(pSTMHandle->vCntlBaseAddr), STM_ATB_ID_MIPI);

		// Setup the Async counter
		reg_wr32(STM_MIPI_ATBSYNCCNT(pSTMHandle->vCntlBaseAddr), getSyncCount(pMIPI_ConfigObj->TraceBufSize));

		//Enable the ATB port
		reg_wr32(STM_MIPI_ATBCNFG(pSTMHandle->vCntlBaseAddr), 0x00010000);
	}

	// Enable the STM module to export data
	reg_wr32(STM_MIPI_SWMSTCNTL0(pSTMHandle->vCntlBaseAddr), MOD_ENABLED | STM_TRACE_EN);

	return eSTM_SUCCESS;
}
#endif

#ifdef _CS_STM
eSTM_STATUS STMXport_config_CS(STMHandle* pSTMHandle, STM_CS_ConfigObj * pCS_ConfigObj)
{
	// Set defaults
	// Check for parameters set from CSSTM_ConfigObj struct
	if (NULL == pCS_ConfigObj) {
		return eSTM_ERROR_PARM;
	}

	// Unlock STM module
	reg_wr32(STM_CS_LOCK(pSTMHandle->vCntlBaseAddr), STM_UNLOCK_VALUE);

	// Make sure the STM unit is disabled before any changes are made
	reg_wr32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr), reg_rd32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr)) & ~CSSTM_CTRL_STAT_EN);

	//Enable all stimulus ports & for triggers, Port selection and set master filtering to not used */
	reg_wr32(STM_ARM_PORTEN(pSTMHandle->vCntlBaseAddr), 0xFFFFFFFF);   // Enable all 32 stimulus ports (with PORTSELCFG set to zero this applies to
	                                                          // every group of 32 stimulus ports.
	reg_wr32(STM_ARM_PORTTRGEN(pSTMHandle->vCntlBaseAddr), 0);        // STMLib provide no APIs for generating trigger messages so disable.
	reg_wr32(STM_ARM_PORTSELCFG(pSTMHandle->vCntlBaseAddr), 0);      // Disable port selection so PORTEN reg applies to every group of 32 stimulus ports.

	reg_wr32(STM_ARM_PORTMSTSELCFG(pSTMHandle->vCntlBaseAddr), 0);      // Enable PORTSELCFG to apply to all masters

	reg_wr32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr), CSSTM_CTRL_STAT_TRACEID(STM_ATB_ID_ARM2_0) |  //Set the ATB ID
                                                   CSSTM_CTRL_STAT_SYNCEN |                       //Set Sync Enable
                                                   CSSTM_CTRL_STAT_TSEN);                         //Set Time stamp Enable

	// Set the sync rate
	reg_wr32(STM_ARM_SYNCCNTL(pSTMHandle->vCntlBaseAddr), getSyncCount(pCS_ConfigObj->TraceBufSize));

	//Enable the STM Unit
	reg_wr32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr), reg_rd32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr)) | CSSTM_CTRL_STAT_EN);

	return eSTM_SUCCESS;
}
#endif

eSTM_STATUS STMXport_getDCMInfo(STMHandle* pSTMHandle, STM_DCM_InfoObj  * pDCM_Info)
{
    if (NULL == pDCM_Info) {
        return eSTM_ERROR_PARM;
	}

    pDCM_Info->stpVersion = pSTMHandle->stpVersion;
#ifdef _MIPI_STM
    if ( 1 == pSTMHandle->stpVersion){

    	pDCM_Info->stm_data_flip = 1;
    	uint32_t value = reg_rd32(STM_MIPI_ATBHEADPTR(pSTMHandle->vCntlBaseAddr));

        pDCM_Info->atb_head_present_0 = (value & 0x8) >> 3;
        pDCM_Info->atb_head_pointer_0 = (value & 0x7);
        pDCM_Info->atb_head_present_1 = (value & 0x80) >> 7;
        pDCM_Info->atb_head_pointer_1 = (value & 0x70) >> 4;

    	pDCM_Info->atb_head_required = true;
    }
    else {
    	pDCM_Info->stm_data_flip = 0;
        pDCM_Info->atb_head_present_0 = 0;
        pDCM_Info->atb_head_pointer_0 = 0;
        pDCM_Info->atb_head_present_1 = 0;
        pDCM_Info->atb_head_pointer_1 = 0;
    	pDCM_Info->atb_head_required = false;
    }

#endif
#ifdef _CS_STM
    pDCM_Info->stm_data_flip = 0;
    pDCM_Info->atb_head_required = false;
    pDCM_Info->atb_head_present_0 = 0;
    pDCM_Info->atb_head_pointer_0 = 0;
    pDCM_Info->atb_head_present_1 = 0;
    pDCM_Info->atb_head_pointer_1 = 0;
#endif

    return eSTM_SUCCESS;
}

eSTM_STATUS STMXport_printfV(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, va_list arg_addr)
{
    eSTM_STATUS retStatus = eSTM_SUCCESS;
    retStatus = STM_printfV(pSTMHandle, chNum, pMsgString, arg_addr);
    return retStatus;

}

eSTM_STATUS STMXport_printf(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, ... )
{
#if ( defined(_CIO) || defined(_STM) ) && !defined(_COMPACT)    
    va_list arg_addr;
    eSTM_STATUS retStatus = eSTM_SUCCESS;
    
    va_start(arg_addr, pMsgString);
    retStatus = STM_printfV(pSTMHandle, chNum, pMsgString, arg_addr);
    va_end(arg_addr);
    return retStatus;
#endif
#ifdef _COMPACT
    //This code included in Compact Build
    RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_INVALID_FUNC);
#endif      
}

eSTM_STATUS STM_printfV(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, va_list arg_addr)
{
	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) && !defined(_COMPACT)
	int32_t iHdrWordCnt = 1;					//Minimum is one word 
	int32_t iBufWordCnt = 1;        			//Minimum is one word for formatted string pointer
    int32_t iBufByteCnt = 0;                    //Only used for un-optimized %s case
    int32_t iBufWordCntAdj = 0;                 //Only used for un-optimized %s case
	uint32_t *pOutBuffer;                       //Pointer to STM output buffer
    bool * pMapBuffer;                          //Pointer to a Map Buffer for the un-optimized printf case
	const char* pTemp;

	//////////////////////////////////////////////////////////////////
	// Check all function parameters and return error if necessary  //
	//////////////////////////////////////////////////////////////////
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
	if ( ( NULL == pMsgString ) || ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);	
	} 

	//////////////////////////////////////////////////////////////////
	// Figure out the buffer size required and allocate it          //
	//  on the heap.											    //
	//////////////////////////////////////////////////////////////////
    if ( eSend_char_stream == pSTMHandle->xmit_printf_mode )
    {
    	returnStatus =  SendVprintf_CharStream(pSTMHandle, chNum, pMsgString, arg_addr );
    	RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
	}
	else
	{
		const char * pConvChar = pMsgString;
		const char * pPrevConvChar = NULL;	
		
		//Scan pMsgString for the number of conversion characters (%)
		while (1) {
			pConvChar = strchr(pConvChar, '%');
			if ( NULL != pConvChar ) 
			{
				bool bExitFlag = true;

				// For the single "%" case - we know there is at least 1 word to save off
				pPrevConvChar = pConvChar;
				iBufWordCnt++;

				//Search for the conversion character
				pTemp = pConvChar;
				bExitFlag = true;
				while (bExitFlag)
				{
					pTemp++;
					if (('\0' != *pTemp) && (NULL != pTemp))
					{
						char formatCode = *pTemp;
						bExitFlag = false;
						switch (formatCode)
						{
							case 'h': case 'd': case'u': case'i': case 'c': case 'o': case 'x': case 'X':
                            case 's':
									bExitFlag = false;
									break;
							case 'l':
							case 'L':
									if (!( 'l' == (char)(*(pTemp+1))
										|| 'f' == (char)(*(pTemp+1)) 
										|| 'e' == (char)(*(pTemp+1))
										|| 'E' == (char)(*(pTemp+1))
										|| 'g' == (char)(*(pTemp+1))
										|| 'G' == (char)(*(pTemp+1)) ) )
									{
										//Not a leagal case so during the second pass we will error out
										//Don't need to repeat the code here.
										bExitFlag = false;
										break;
									}
									// All valid double and uint32_t uint32_t cases fall through
							case 'e': case 'f': case 'g': case 'E': case 'F': case 'G': 
									/* Float will be treated as double (64 bits) in va_arg macro */
									iBufWordCnt++;
									bExitFlag = false;
									break;
							case '%':
									if (( pTemp - pPrevConvChar ) == 1 )
									{
										//Double '%' case
										pPrevConvChar = NULL; 
										iBufWordCnt--;	
									}
									bExitFlag = false;
									break;		
						}/* end of switch */
					}/* if  */
					else break; //Reached the end of the conversion string
				}/* while */

				pConvChar = pTemp++;
			}
			else break;		
		}
		 
		//Add room for the header. If number of words for message > OST_SMALL_MSG then 2 words needed, else 1 word needed 
		iHdrWordCnt += ( iBufWordCnt > OST_SMALL_MSG ) ? 1 : 0;
		iBufWordCnt += iHdrWordCnt;  

		//Allocate enough memory for both the header and the message
		pOutBuffer = (uint32_t*) cTools_memAlloc(iBufWordCnt * sizeof(uint32_t));
		if (NULL == pOutBuffer)
		{
			RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_HEAP_ALLOCATION);
		}
  
        //If printf optimization is disabled we must create a map to
        //indicate where the %s pointers are in the message list 
        if ( false == pSTMHandle->optimize_strings )
        {
            pMapBuffer = (bool *) cTools_memAlloc(iBufWordCnt * sizeof(bool));
            if (NULL == pMapBuffer)
            {
                cTools_memFree(pOutBuffer);
                RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_HEAP_ALLOCATION);
            }
            
            pSTMHandle->pUnOptPrintfMap = pMapBuffer;
            
        }

	}
	//////////////////////////////////////////////////////////////////
	// Put format string values in msg buffer                       //
	//////////////////////////////////////////////////////////////////
	{
		const char* pTemp;
		uint32_t  *pOutBufTemp, *pBufPtr, *tempPtr;	
		int32_t  tempArgValueInt;
		uint32_t  tempArgValueUint;
		uint64_t tempArgValue64;
		double tempArgValueDouble;
		char bExitFlag = 1;
        bool * pMapBufferTmp = pMapBuffer;
        
        if ( false == pSTMHandle->optimize_strings )
        {
             *pMapBufferTmp++ = false;          // OST header is not %s 
             if ( 1 < iHdrWordCnt )
                *pMapBufferTmp++ = false;       // And the count word if present
             *pMapBufferTmp++ = false;          // Format string is not %s 
        }
         
		//Offset the header
		pOutBufTemp = pOutBuffer + iHdrWordCnt; 

		pBufPtr = pOutBufTemp;		
		pTemp = pMsgString;

		/* store the address of string pointer as the first element in output buffer */
		*pBufPtr = (uint32_t)pMsgString; 
		pBufPtr++;

		/* now walk though the remaining string. The end of string should be a " symbol as well */
		while (('\0' != *pTemp) && (NULL != pTemp))
		{
			if ('%' == *pTemp)
			{
				bExitFlag = 1;
				returnStatus = eSTM_ERROR_STRING_FORMAT;
				while (bExitFlag)
				{
					pTemp++;
					if (NULL != pTemp)
					{
						char formatCode = (char)(*pTemp);
						bExitFlag = 0;
						switch (formatCode)
						{
							//int (32-bits) cases
							case 'h':
									//The format conversion promotes shorts to ints
									//Fall through to the next case for processing
									if ( !( 'd' == (char)(*(pTemp+1))
										 || 'i' == (char)(*(pTemp+1))
										 || 'o' == (char)(*(pTemp+1))
									     || 'u' == (char)(*(pTemp+1))
									     || 'x' == (char)(*(pTemp+1))
									     || 'X' == (char)(*(pTemp+1))  ) ) 							
									{
										// Must be an un-supported format so error out
										break;
									}
									//Valid int case so fall through
							case 'd': case'u': case'i': case 'c': case 'o': case 'x': case 'X':
                            case 's':
									tempArgValueInt = va_arg(arg_addr,int);
									*pBufPtr = (uint32_t)tempArgValueInt;

                                    //For the un-optimized case we need to fill in the %s map
                                    //and figure out how many bytes are in the %s string so we
                                    // can adjust the OST header byte count.
                                    if ( false == pSTMHandle->optimize_strings )
                                    {
                                        if ( 's' == formatCode ) { 
                                            * pMapBufferTmp++ = true;
                                            iBufWordCntAdj++;
                                            iBufByteCnt += strlen((char *)*pBufPtr)+1;
                                        }   
                                        else
                                        {
                                            * pMapBufferTmp++ = false;
                                        }
                                    }                                    

									pBufPtr++;								
									returnStatus = eSTM_SUCCESS;

									break;
							//uint32_t (32-bit) & uint32_t uint32_t cases
							case 'l':
						
									/* this is the uint32_t data type (32 bits) */
									if (    'd' == (char)(*(pTemp+1))
										 || 'i' == (char)(*(pTemp+1))
										 || 'o' == (char)(*(pTemp+1))
									     || 'u' == (char)(*(pTemp+1))
									     || 'x' == (char)(*(pTemp+1))
									     || 'X' == (char)(*(pTemp+1))  )							
									{
										tempArgValueUint = va_arg(arg_addr,uint32_t);
										*pBufPtr = tempArgValueUint;
										pBufPtr++;
										returnStatus = eSTM_SUCCESS;
                                        
                                        //for the un-optimized case we need to fill in the %s map
                                        if ( false == pSTMHandle->optimize_strings )
                                        {
                                            *pMapBufferTmp++ = false;   
                                        }  
                                        
									}
									if ( 'l' == (char)(*(pTemp+1)) )
									{
										tempArgValue64 = va_arg(arg_addr,uint64_t);
										tempPtr = (uint32_t*)&tempArgValue64;
										*pBufPtr++ = *tempPtr++;
#ifdef C55XP_STM
										//for DSPs the uint32_t uint32_t type is 40 bits
										*pBufPtr++ = 0x000000FF & *tempPtr;
#else 
										*pBufPtr++ = *tempPtr;
#endif
										returnStatus = eSTM_SUCCESS;
                                        
                                        //for the un-optimized case we need to fill in the %s map
                                        if ( false == pSTMHandle->optimize_strings )
                                        {
                                            *pMapBufferTmp++ = false;
                                            *pMapBufferTmp++ = false;   
                                        }                                          
                                        											
									} 
									//Fall through and test %lf ... cases which are questionable for ANSI C
							//double ( 64-bits only) cases
							case 'L':
									/* this is the double data type (64 bits)*/
									if ( !( 'f' == (char)(*(pTemp+1))
									     || 'e' == (char)(*(pTemp+1))
									     || 'g' == (char)(*(pTemp+1))
									     || 'E' == (char)(*(pTemp+1))
									     || 'G' == (char)(*(pTemp+1))) )

									{
										// Must be an un-supported format so error out
										break;
									}
									//Valid double case so fall through
							case 'e': case 'f': case 'g': case 'E': case 'G': 
									/* Float will be treated as double (64 bits) in va_arg macro */
									tempArgValueDouble = va_arg(arg_addr,double);
									tempPtr = (uint32_t*)&tempArgValueDouble;
									
									//TI Compiler pointing to MS word first
									tempArgValueUint = *tempPtr++;
									*pBufPtr++ = *tempPtr;
									*pBufPtr++ = tempArgValueUint;

									returnStatus = eSTM_SUCCESS;
                                    
                                    //For the un-optimized case we need to fill in the %s map
                                    if ( false == pSTMHandle->optimize_strings )
                                    {
                                        *pMapBufferTmp++ = false;
                                        *pMapBufferTmp++ = false;   
                                    }   

									break;

							//Cases we skip
							case '.': case '+': case '-': case ' ': case '#': case '*':
							case '0': case '1': case '2': case '3': case '4': 
							case '5': case '6': case '7': case '8': case '9': 
									 /* skip these formatting characters. */
									 bExitFlag = 1;
									 returnStatus = eSTM_SUCCESS;
									 break;
							case '%':
									 bExitFlag = 1;
									 break;		
						}/* end of switch */
					}/* if  */
				}/* while */
				/* return failure immediately if the number of variables do not match */
				if (returnStatus != eSTM_SUCCESS)
				{

                    if ( false == pSTMHandle->optimize_strings )
                    {
                        cTools_memFree(pMapBuffer);   
                    } 
					cTools_memFree(pOutBuffer);
					RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);
				}
			}
			pTemp++;
		}/* end of while loop */
	}

	//////////////////////////////////////////////////////////////////
	// Add the header and dump to STM module                        //
	//////////////////////////////////////////////////////////////////
	if (returnStatus == eSTM_SUCCESS)
	{
		int32_t bytesInHeader;
		uint32_t msgByteCount = (iBufWordCnt - iHdrWordCnt) * STM_WORD_SIZE; 	
        uint32_t protocol = ( false == pSTMHandle->optimize_strings ) ? OST_PROTOCOL_PRINTF_UNOPTIMIZED: OST_PROTOCOL_PRINTF;

        if ( false == pSTMHandle->optimize_strings )
        {
            //Adjust the msgByteCount by the number of bytes in each %s string.
            //Note: We still pass the original iBufWordCnt to STM_putBufCPU() because
            //      this number is used to walk the %s map buffer (pMapBuffer)
            msgByteCount -= (iBufWordCntAdj * STM_WORD_SIZE);
            msgByteCount += iBufByteCnt;
        }

        Build_OST_Header(protocol, msgByteCount, (uint32_t*)pOutBuffer, &bytesInHeader);

		{
			//Blocking case, ok to use the CPU
			returnStatus = STM_putBufCPU(pSTMHandle, chNum, NULL, 0, pOutBuffer, iBufWordCnt, eWord);
			cTools_memFree(pOutBuffer);
            
            if ( false == pSTMHandle->optimize_strings )
            {
                cTools_memFree(pMapBuffer);
                pSTMHandle->pUnOptPrintfMap = NULL;
            }
		}					
	}
	
#endif
#ifdef _COMPACT
	//This code included in Compact Build
	returnStatus =  eSTM_ERROR_INVALID_FUNC;
#endif
#if defined(_CIO) && !defined(_COMPACT)

	if (   ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_printf(...)" ) )
	    || ( 0 > vfprintf(pSTMHandle->pLogFileHdl, pMsgString, arg_addr) )
		|| ( 0 > fprintf (pSTMHandle->pLogFileHdl, "\n") ) )
	{
	 	returnStatus = eSTM_ERROR_CIO_ERROR;
	}
       
#endif

    RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);
}

eSTM_STATUS STM_putCharMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsg, int32_t iMsgByteCount)
{ 
	eSTM_STATUS returnStatus = eSTM_SUCCESS;

#if defined(_STM) && !defined(_COMPACT)

	{
		int32_t bytesInHeader = 0;
		uint32_t  OST_Header[2]={0};

		Build_OST_Header((uint32_t)OST_PROTOCOL_BYTESTREAM, iMsgByteCount, (uint32_t*)OST_Header, &bytesInHeader);
		returnStatus = STM_putMsgCPU(pSTMHandle, chNum, (void *)OST_Header, bytesInHeader, (void *)pMsg, iMsgByteCount);

	}

#endif
#ifdef _COMPACT 
	//This code included in Compact Build
	returnStatus =  eSTM_ERROR_INVALID_FUNC;	
#endif

#if defined(_CIO) && !defined(_COMPACT)
	{
		int32_t fprintfErr = 0;
		uint32_t data;
		bool firstTime = true;
		const char * pMsgTmp = pMsg;

		if ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_putMsg(...)") )
		{
			returnStatus = eSTM_ERROR_CIO_ERROR;
		}

        do 
		{  
			data = (0 < iMsgByteCount--) ? *pMsgTmp++ : 0;
			data |= (0 < iMsgByteCount--) ? *pMsgTmp++ << 8 : 0;
			data |= (0 < iMsgByteCount--) ? *pMsgTmp++ << 16: 0;
			data |= (0 < iMsgByteCount--) ? *pMsgTmp++ << 24: 0;

			if ( true == firstTime )
			{
				fprintfErr = fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg1, data );
			}
			else
			{
				fprintfErr = fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg, data );
			}
			if ( 0 > fprintfErr )
			{
				returnStatus = eSTM_ERROR_CIO_ERROR;
				break;
			}
			firstTime = false;
		}
		while ( 0 < iMsgByteCount); 

	}

#endif
    return returnStatus;
}

eSTM_STATUS STMXport_putMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsg, int32_t iMsgByteCount)
{ 
    eSTM_STATUS returnStatus = eSTM_SUCCESS;

    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
    if ( ( NULL == pMsg ) || ( 0 == iMsgByteCount ) 
         || ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
    {
        RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);    
    }

    returnStatus = STM_putCharMsg(pSTMHandle, chNum, pMsg, iMsgByteCount);
    RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
}

#ifdef _STM
eSTM_STATUS STMXport_setMetaState(STMHandle* pSTMHandle, bool onDemand)
{
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
    pSTMHandle->metaOnDemand = onDemand;
    return eSTM_SUCCESS;        
}

eSTM_STATUS STMXport_getMetaState(STMHandle* pSTMHandle, bool * onDemand)
{
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
    *onDemand = pSTMHandle->metaOnDemand;
    return eSTM_SUCCESS;        
}


eSTM_STATUS STMXport_sendMetaOnDemand(STMHandle* pSTMHandle)
{
    eSTM_STATUS returnStatus = eSTM_SUCCESS;
    int32_t chNum = pSTMHandle->numChannels-1;
    
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
    if ( NULL == pSTMHandle->pMetaObj ) 
    {
        //No data to send
        return eSTM_SUCCESS;
    }
    else
    {
        MetaObj * pCurObj = pSTMHandle->pMetaObj;
        while ( NULL != pCurObj )
        {
            returnStatus = STM_putCharMsg(pSTMHandle, chNum, pCurObj->pMsg, pCurObj->msgByteCnt);
            if (eSTM_SUCCESS != returnStatus)
            {
                break;
            }
            pCurObj = pCurObj->next;
        }
        
    } 
      
    RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
}

eSTM_STATUS STMExport_putMeta(STMHandle* pSTMHandle, const char* pMsg, int32_t iMsgByteCount)
{     
    eSTM_STATUS returnStatus = eSTM_SUCCESS;
    int32_t chNum = pSTMHandle->numChannels-1;
    
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
 
    if (  false == pSTMHandle->metaOnDemand ) 
    {     
	    //Send the meta data immediately
        if ( ( NULL == pMsg ) || ( 0 == iMsgByteCount ) )
	    {
	        RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);    
	    }
	
	    returnStatus = STM_putCharMsg(pSTMHandle, chNum, pMsg, iMsgByteCount);
    }
    else
    {
        //Check that meta data not already in list
        // If so simply update curent MetaObj
        MetaObj * pCurObj = pSTMHandle->pMetaObj;
        MetaObj * pLastValid = NULL;
        while ( NULL != pCurObj) {
            
            if ( pCurObj->pMsg == pMsg )
            {
                pCurObj->msgByteCnt = iMsgByteCount;
                break;
            }
            pLastValid = pCurObj;
            pCurObj = pCurObj->next;
        }         

        // If the previous check did not update an existing member 
        // then add a MetaObj to the end of the link list.
        if ( NULL == pCurObj)
        {
	        //Register meta data at end of list
	        MetaObj * pMetaObj = (MetaObj *)cTools_memAlloc(sizeof(MetaObj));
	        if ( NULL == pMetaObj )
	        {
	            RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_HEAP_ALLOCATION);
	        }
	        
	        pMetaObj->pMsg = pMsg;
	        pMetaObj->msgByteCnt = iMsgByteCount;
	        pMetaObj->next = NULL;
	        pMetaObj->prev = NULL;
	        
	        if ( NULL == pSTMHandle->pMetaObj ) 
	        {
	            pSTMHandle->pMetaObj = pMetaObj;
	        }
	        else
	        {               
	            pLastValid->next =  pMetaObj;
	            pMetaObj->prev = pCurObj; 
	        }  
        } 
        
    }
    RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
}
#endif

eSTM_STATUS STMXport_putBuf(STMHandle* pSTMHandle, int32_t chNum, void* pDataBuf, eSTMElementSize elementSize, int32_t elementCount)
{
	uint32_t bufAlignment;
	eSTM_STATUS returnStatus = eSTM_SUCCESS;

    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

	if ( ( NULL == pDataBuf ) || ( 0 == elementCount ) 
	     || ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

	//Check address alignment
	bufAlignment = AddressAlignmentChecker((uint32_t)pDataBuf);
	switch (elementSize)
	{
		case eShort:
			if ( ( eByteAlign == bufAlignment) || ( eShortAndByteAlign ==  bufAlignment) )
			{
				RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
			}
			break;
		case eWord:
			if ( ( eByteAlign == bufAlignment) || ( eShortAndByteAlign ==  bufAlignment) || ( eShortAlign == bufAlignment ) )
			{
				RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
			}
			break;
	} 

#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO) )
	{

		register uint32_t msgByteCount = elementSize * elementCount;
		register int32_t bytesInHeader = 0;
		register uint32_t OST_Header1 = 0;
		uint32_t  OST_Header2[2]={0,0};

		if ( msgByteCount < OST_SHORT_HEADER_LENGTH_LIMIT )
		{
			OST_Header1 = Build_CompactOST_Header((uint32_t)OST_PROTOCOL_MSGSTREAM, msgByteCount );
			bytesInHeader = 4;
			returnStatus = STM_putBufCPU(pSTMHandle, chNum, (void *) OST_Header1, bytesInHeader, pDataBuf, elementCount, elementSize);		

		}
		else {
			Build_ExtendedOST_Header((uint32_t)OST_PROTOCOL_MSGSTREAM, msgByteCount, OST_Header2);
			bytesInHeader = 8;
			returnStatus = STM_putBufCPU(pSTMHandle, chNum, (void *) OST_Header2, bytesInHeader, pDataBuf, elementCount, elementSize);		
		}
	}
#endif
#ifdef _CIO
	{
		int32_t fprintfErr = 0;
		int32_t iElementIndex;
		uint32_t data=0;
		bool firstTime = true;
		unsigned char * pBufByte = pDataBuf;
		uint16_t * pBufShort = pDataBuf;
		uint32_t * pBufInt = pDataBuf;

		if ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_putBuf(...)") )
		{
			returnStatus = eSTM_ERROR_CIO_ERROR;
		}

		for (iElementIndex = 0; iElementIndex < elementCount; iElementIndex++)
		{
			switch (elementSize)
			{

				case eByte:
					data = *pBufByte++;
					break;

				case eShort:
					data = *pBufShort++;
					break;

				case eWord:
					data = *pBufInt++;
					break;	
			} //End of switch

			if ( true == firstTime )
			{
				fprintfErr = fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg1, data );
			}
			else
			{
				fprintfErr = fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg, data );
			}
			if ( 0 > fprintfErr )
			{
				returnStatus = eSTM_ERROR_CIO_ERROR;
				break;
			}
			firstTime = false;


		} //End of for
	}
#endif
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}
 
 eSTM_STATUS STMXport_putTwoBufs(STMHandle* pSTMHandle, int32_t chNum,  
                                void* pDataBuf1, eSTMElementSize elementSize1, int32_t elementCount1,
                                void* pDataBuf2, eSTMElementSize elementSize2, int32_t elementCount2)
{
#ifdef _STM
    eSTM_STATUS returnStatus = eSTM_SUCCESS;
    register volatile uint32_t msgByteCount = (elementCount1 * elementSize1) + (elementCount2 * elementSize2);
    register volatile uint32_t ulMsgAddress = 0;
    register volatile uint32_t ulEndAddress = 0;
    register unsigned int i = 0;
    
    if (( elementCount2 < 1 ) || ( NULL == pDataBuf1) || (NULL == pDataBuf2))
    {
    	RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
    }

	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

    //Export the header
    if ( msgByteCount < OST_SHORT_HEADER_LENGTH_LIMIT )
    {
        *((uint32_t *) ulMsgAddress) = (uint32_t)(OST_VERSION | OST_ENTITY_ID | OST_PROTOCOL_MSGSTREAM | msgByteCount);
    }
    else {    
        *((uint32_t *) ulMsgAddress) = (uint32_t)(OST_VERSION | OST_ENTITY_ID | OST_PROTOCOL_MSGSTREAM | OST_SHORT_HEADER_LENGTH_LIMIT);
        *((uint32_t *) ulMsgAddress) = msgByteCount;                
    }     
	
	while (i++ < 2) {
	    
		register unsigned int j = (i==1) ? elementCount1 : elementCount2-1;
	    register eSTMElementSize elementSize = (i==1) ? elementSize1 : elementSize2;
	    
	    switch (elementSize)
	    {
	        case eByte:
	        {
	            register uint8_t * restrict pTempBytePtr = (i==1) ? (uint8_t *)pDataBuf1 : (uint8_t *)pDataBuf2;
	            register volatile uint8_t * restrict stmMsgPtr = (uint8_t *) ulMsgAddress;
				while (j--)
	            {    
					*stmMsgPtr = *pTempBytePtr++;
	            }
	
	            if (i == 2) 
	            {
	                *((uint8_t *) ulEndAddress) = *pTempBytePtr;
	            }	

	                break;
	            }
	
	            case eShort:
	            {
	                register uint16_t * restrict pTempShortPtr = (i==1) ? (uint16_t *)pDataBuf1 : (uint16_t *)pDataBuf2;
					register volatile uint16_t * restrict stmMsgPtr = (uint16_t *) ulMsgAddress;
					while (j--)
	                {    
	                    *stmMsgPtr = *pTempShortPtr++;
	                }
	                
	                if (i == 2)
	                {
	                    *((uint16_t *) ulEndAddress) = *pTempShortPtr;
	                }

	            break;
	        }
	
	        case eWord:
	        {
	            register uint32_t * restrict pTempWordPtr = (i==1) ? (uint32_t *)pDataBuf1 : (uint32_t *)pDataBuf2;
	            register volatile uint32_t * restrict stmMsgPtr = (uint32_t *) ulMsgAddress;
				while(j--)
	            {    
	                *stmMsgPtr = *pTempWordPtr++;
	            }
	            
	            if (i == 2)
	            {
	                *((uint32_t *) ulEndAddress) = *pTempWordPtr;
	            }

	            break;
	        }
	    } //End of switch
	
	}//End of while
 
    RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);
#endif
#ifdef _CIO
    return eSTM_ERROR_INVALID_FUNC;
#endif

}


// Warning - Blocking function only, No callback support
// Optimized to send a single word msg (OST Header + 32-bit data) as fast as possible
eSTM_STATUS STMXport_putWord(STMHandle* pSTMHandle, int32_t chNum, uint32_t data)
{

	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_MSGSTREAM | 4;

	//Check for errors
	if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    
    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;

	//Xport the Data
	*((uint32_t *) ulEndAddress) = data;
#endif
#ifdef _CIO
		if (  ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_putWord(...)") )
			||( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg1, (uint32_t)data ) ) )
		{
			returnStatus = eSTM_ERROR_CIO_ERROR;
		}	
#endif
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}

eSTM_STATUS STMXport_putShort(STMHandle* pSTMHandle, int32_t chNum, uint16_t data)
{
	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_MSGSTREAM | 4;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }
    	
    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}
    
	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;

	//Xport the Data
	*((unsigned short *) ulEndAddress) = data;
#endif
#ifdef _CIO
		if (  ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_putShort(...)") )
			||( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg1, (unsigned short)data ) ) )
		{
			returnStatus = eSTM_ERROR_CIO_ERROR;
		}	
#endif
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);


}

eSTM_STATUS STMXport_putByte(STMHandle* pSTMHandle, int32_t chNum, uint8_t data)
{

	eSTM_STATUS returnStatus = eSTM_SUCCESS;

#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_MSGSTREAM | 4;


	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;

	//Xport the Data
	*((unsigned char *) ulEndAddress) = data;
#endif
#ifdef _CIO
		if (  ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_putByte(...)") )
			||( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringMsg1, (unsigned char)data ) ) )
		{
			returnStatus = eSTM_ERROR_CIO_ERROR;
		}	
#endif
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}

eSTM_STATUS STMXport_logMsg(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, ...)
{
	eSTM_STATUS returnStatus = eSTM_SUCCESS;
//#ifdef _STM
#if defined(_STM) && !defined(_COMPACT)
	register volatile uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_PRINTF;

	char firstTime = '0';
	char * pLastConvChar = &firstTime;
	const char * pConvChar = pMsgString;
	int iBufWordCnt = 0;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

    if ( eSend_char_stream == pSTMHandle->xmit_printf_mode )
    {
        va_list arg_list;

        va_start(arg_list, pMsgString);
        returnStatus =  SendVprintf_CharStream(pSTMHandle, chNum, pMsgString, arg_list );
        va_end(arg_list);

    	RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
	}



	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Figure out the number of variables in the format string
	while ( (pConvChar = strchr(pConvChar, '%')) != NULL) {

		if ( pLastConvChar+1 != pConvChar ) {
			pLastConvChar = (char *)pConvChar;		
			iBufWordCnt++;

		}
		else {
			//Duplicate % so start over
			pLastConvChar = &firstTime;
			//Must remove first % since there is a duplicate
			iBufWordCnt--;
		}
		pConvChar++;

	}

	//Add one for the format string
	iBufWordCnt++;

	//Check that the format string plus the number of variables do not exceed the OST header limit
	if ( iBufWordCnt < OST_SHORT_HEADER_LENGTH_LIMIT) {
		 OST_Header |= iBufWordCnt;
	} 
	else {
		returnStatus = eSTM_ERROR_PARM;
	}

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;	


	if ( 1 == iBufWordCnt ) {

		//Xport the Data
		*((uint32_t *) ulEndAddress) = (uint32_t)pMsgString;
	}
	else {
		va_list arg_addr;		
		int argValue;
		 
		va_start(arg_addr, pMsgString);
		
		//Export the pointer to the format string and adjust iBufWordCnt
		*((uint32_t *) ulMsgAddress) = (uint32_t)pMsgString;
		iBufWordCnt--;

		if ( iBufWordCnt > 1 ) {
			do {
				argValue = va_arg(arg_addr,int);
				*((uint32_t *) ulMsgAddress) = argValue;
			} while (--iBufWordCnt != 1);
		}
		
		//One last word to transfer to the end address
		argValue = va_arg(arg_addr,int);
		*((uint32_t *) ulEndAddress) = argValue;

		va_end(arg_addr);
	}
	
#endif
#ifdef _COMPACT
	//This code included in Compact Build
	returnStatus =  eSTM_ERROR_INVALID_FUNC;	
#endif

#if defined(_CIO) && !defined(_COMPACT)
	va_list args;

	va_start(args, pMsgString);
	if (   ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_logMsg(...)") )
	    || ( 0 > vfprintf(pSTMHandle->pLogFileHdl, pMsgString, args) )
	    || ( 0 > fprintf (pSTMHandle->pLogFileHdl, "\n") ) )
	{
		returnStatus = eSTM_ERROR_CIO_ERROR;
	}
	va_end(args);

#endif
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);
}

eSTM_STATUS STMXport_logMsg0(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString)
{

	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_PRINTF | 1;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

    if ( eSend_char_stream == pSTMHandle->xmit_printf_mode )
    {
        returnStatus =  SendPrintf_CharStream(pSTMHandle, chNum, pMsgString);
    	RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;	

	//Xport the Data
	*((uint32_t *) ulEndAddress) = (uint32_t)pMsgString;
#endif
#ifdef _CIO
	fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_logMsg0(...)");
	fprintf (pSTMHandle->pLogFileHdl, pMsgString );
	fprintf (pSTMHandle->pLogFileHdl, "\n");
#endif	
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}

eSTM_STATUS STMXport_logMsg1(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, uint32_t data)
{
	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register volatile uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_PRINTF | 2;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

    if ( eSend_char_stream == pSTMHandle->xmit_printf_mode )
    {
        returnStatus =  SendPrintf_CharStream(pSTMHandle, chNum, pMsgString, data );
    	RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;	

	//Xport the Data
	*((uint32_t *) ulMsgAddress) = (uint32_t)pMsgString;

	*((uint32_t *) ulEndAddress) = (uint32_t)data;
#endif
#ifdef _CIO
	if (   ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_logMsg1(...)") )
		|| ( 0 > fprintf (pSTMHandle->pLogFileHdl, pMsgString, data ) )
		|| ( 0 > fprintf (pSTMHandle->pLogFileHdl, "\n") ) )
	{
		returnStatus = eSTM_ERROR_CIO_ERROR;
	}
#endif	
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}

eSTM_STATUS STMXport_logMsg2(STMHandle* pSTMHandle, int32_t chNum, const char* pMsgString, uint32_t data0, uint32_t data1)
{
	eSTM_STATUS returnStatus = eSTM_SUCCESS;
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
	register volatile uint32_t ulMsgAddress = 0;
	register uint32_t ulEndAddress = 0;
	register uint32_t OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_PRINTF | 3;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

    if ( ( chNum < 0 ) || ( chNum > STM_MAX_CHANNEL ) )
	{
		RETURN_CALLBACK(pSTMHandle->pCallBack, eSTM_ERROR_PARM);
	}

    if ( eSend_char_stream == pSTMHandle->xmit_printf_mode )
    {
        returnStatus =  SendPrintf_CharStream(pSTMHandle, chNum, pMsgString, data0, data1 );
    	RETURN_CALLBACK(pSTMHandle->pCallBack,returnStatus);
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;	

	//Xport the Data
	*((uint32_t *) ulMsgAddress) = (uint32_t)pMsgString;

	*((uint32_t *) ulMsgAddress) = (uint32_t)data0;

	*((uint32_t *) ulEndAddress) = (uint32_t)data1;
#endif
#ifdef _CIO
	if (   ( 0 > fprintf (pSTMHandle->pLogFileHdl, STM_FrmtStringHdr, pSTMHandle->BufIOInfo.pMasterId, chNum, "STMXport_logMsg2(...)") )
		|| ( 0 > fprintf (pSTMHandle->pLogFileHdl, pMsgString, data0, data1 ) )
		|| ( 0 > fprintf (pSTMHandle->pLogFileHdl, "\n") ) )
	{
	 	returnStatus = eSTM_ERROR_CIO_ERROR;
	}
#endif	
	RETURN_CALLBACK(pSTMHandle->pCallBack, returnStatus);

}

eSTM_STATUS STMXport_getBufInfo(STMHandle* pSTMHandle, uint32_t * msgCnt, uint32_t * curMsgBufSize)
{
#if defined(_CIO) || !defined(_DMA)
	return eSTM_ERROR_INVALID_FUNC;
#endif
}

eSTM_STATUS STMXport_flush(STMHandle* pSTMHandle)
{
	eSTM_STATUS retValue = eSTM_SUCCESS;
	int retry = STM_FLUSH_RETRY;
	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

#ifdef _CIO
	if ( 0 != fflush(pSTMHandle->pLogFileHdl) )
	{
		retValue = eSTM_ERROR_CIO_ERROR;
	}
#else
#ifdef _MIPI_STM
	// Wait for FIFO Empty bit to be set
	while ( ( reg_rd32(STM_MIPI_SYSSTAT(pSTMHandle->vCntlBaseAddr)) & STM_FIFO_EMPTY )  != STM_FIFO_EMPTY  ){
		if (0 == --retry) break;
	}
#endif
#ifdef _CS_STM

	// Disable the STM module
	reg_wr32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr), reg_rd32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr)) & ~CSSTM_CTRL_STAT_EN);

	// Polling on CS STM BUSY bit does not seem to be a reliable method for testing if the FIFO is empty
	while ((reg_rd32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr)) & CSSTM_CTRL_STAT_BUSY) == CSSTM_CTRL_STAT_BUSY) {
		if (0 == --retry) break;
	}

	// Re-enable the STM module
	reg_wr32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr), reg_rd32(STM_ARM_CNTLSTAT(pSTMHandle->vCntlBaseAddr)) | CSSTM_CTRL_STAT_EN);

#endif
	if (retry == 0) retValue = eSTM_ERROR_FIFO_NOTEMPTY;
#endif

	RETURN_CALLBACK(pSTMHandle->pCallBack, retValue);
}

eSTM_STATUS STMXport_close(STMHandle* pSTMHandle)
{
	eSTM_STATUS retValue = eSTM_SUCCESS;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
    cTools_memUnMap((void *)pSTMHandle->vXportBaseAddr, 256*pSTMHandle->chResolution);
    cTools_memUnMap((void *)pSTMHandle->vCntlBaseAddr, STM_CNTLREG_BLKSIZE);
#endif

#ifdef _CIO
	if ( 0 != fclose(pSTMHandle->pLogFileHdl) )
	{
		retValue = eSTM_ERROR_CIO_ERROR;
	}

#endif

#ifdef _STM
	if (NULL != pSTMHandle)
	{
        //Free any meta objects 
        MetaObj * pCurObj = pSTMHandle->pMetaObj;
        MetaObj * pPrevObj = NULL;
        if ( NULL != pCurObj )
        {
            //Find end of list
            while ( NULL != pCurObj->next) pCurObj = pCurObj->next;
            //Work backwords
            do {
                pPrevObj = pCurObj->prev;
                cTools_memFree(pCurObj);
                pCurObj = pPrevObj;
            }
            while ( NULL != pPrevObj);
            
        }
        
        cTools_memFree(pSTMHandle);
	}
#endif

	STMisOpen = false;

	return retValue;
}

#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))

inline uint32_t Compose_Regular_Address(STMHandle * pSTMHandle, int32_t chNum)
{
	return pSTMHandle->vXportBaseAddr + (pSTMHandle->chResolution * (uint32_t)chNum) + pSTMHandle->regularPortOffset;
}
inline uint32_t Compose_Timing_Address(STMHandle * pSTMHandle, int32_t chNum)
{
	return pSTMHandle->vXportBaseAddr + (pSTMHandle->chResolution * (uint32_t)chNum) + pSTMHandle->timingPortOffset;
}

eSTM_STATUS STM_putBufCPU(STMHandle * pSTMHandle, int32_t chNum, void * pHdrBuf, int32_t iHdrByteCnt, void * pMsgBuf, uint32_t iElementCnt, eSTMElementSize iElementSize)
{
	volatile uint32_t ulMsgAddress = 0;
	uint32_t ulEndAddress = 0;
	uint32_t iElementIndex;
	uint32_t * pTempWordPtr;

	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

#ifdef _STM
    if (( false == pSTMHandle->optimize_strings ) && ( NULL != pSTMHandle->pUnOptPrintfMap))
    {
        // Special case for un-optimized printf
        bool * pStrMap = pSTMHandle->pUnOptPrintfMap;
        
        pTempWordPtr = (uint32_t *)pMsgBuf;
        for (iElementIndex = 0; iElementIndex < iElementCnt; iElementIndex++)
        {   
            if ( true == *pStrMap++ )
            {
                char * pStr = (char *)* pTempWordPtr;
                char data;
                do {
                    data = *pStr++;
                    if ( ( iElementIndex == iElementCnt-1 ) && ('\0' == data) )
                    {
                        *((unsigned char *) ulEndAddress) = data;
                    }
                    else
                    {
                        *((unsigned char *) ulMsgAddress) = data;
                    }
                }while( '\0' != data);
            }
            else
            {

                if ( iElementIndex == iElementCnt-1 )
                {
                    *((uint32_t *) ulEndAddress) = * pTempWordPtr;
                }
                else
                {
                    *((uint32_t *) ulMsgAddress) = * pTempWordPtr;
                }
            }
            pTempWordPtr++;                       
        }  
    }
    else
#endif
    {
        // Normal case for this function
        register unsigned int j = iElementCnt-1;     
        // If the header pointer is not null and the header
        //  word count is greater than 0, send the header.
        // Else (header word count 0 or header pointer null)
        //  then it's assumed the header is contained in the message.
     
        if (4 == iHdrByteCnt  ) 
        {
            *((uint32_t *) ulMsgAddress) = (uint32_t)pHdrBuf;  
        
        }
        if ( 8 == iHdrByteCnt  ) 
        {
            *((uint32_t *) ulMsgAddress) = * (uint32_t *)pHdrBuf;
            *((uint32_t *) ulMsgAddress) = * (((uint32_t *)pHdrBuf)+1);
        }   

    	
        switch (iElementSize)
    	{            
    		case eByte:
            {
                register uint8_t * restrict pTempBytePtr = (uint8_t *)pMsgBuf;
                register volatile uint8_t * restrict stmMsgPtr = (uint8_t *) ulMsgAddress;
                while(j--)
    			{	 
                    *stmMsgPtr = *pTempBytePtr++;
    			}
    
    			*((uint8_t *) ulEndAddress) = * pTempBytePtr;
    			break;
            }
    		case eShort:
            {
                register uint16_t * restrict pTempShortPtr = (uint16_t *)pMsgBuf;
                register volatile uint16_t * restrict stmMsgPtr = (uint16_t *) ulMsgAddress;
                while(j--)
    			{	 
                    *stmMsgPtr = * pTempShortPtr++;
    			}
    
    			*((uint16_t *) ulEndAddress) = * pTempShortPtr;
    			break;
            }
    		case eWord:
            {
                register uint32_t * restrict pTempWordPtr = (uint32_t *)pMsgBuf;
                register volatile uint32_t * restrict stmMsgPtr = (uint32_t *) ulMsgAddress;
                while(j--)
    			{	 
                    *stmMsgPtr = * pTempWordPtr++;
    			}
    
    			*((uint32_t *) ulEndAddress) = * pTempWordPtr;
    			break;
            }	
    	}
    }
	return eSTM_SUCCESS;
}
#endif

#ifdef _STM
eSTM_STATUS STM_putMsgCPU(STMHandle * pSTMHandle, int32_t chNum, void * pHdrBuf, int32_t iHdrByteCnt, void * pMsgBuf, uint32_t iMsgByteCnt)
{
	volatile uint32_t ulMsgAddress = 0;
	uint32_t ulEndAddress = 0;
	uint32_t iElementIndex;
	uint32_t * pTempWordPtr;
	uint16_t * pTempShortPtr;
	uint8_t * pTempBytePtr;
	void * pTempBuf;
	uint32_t bufByteCnt;
	uint32_t bufAlignment;
	 
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);


	// If the header pointer is not null and the header
	//  word count is greater than 0, send the header.
	// Else (header word count 0 or header pointer null)
	//  then it's assumed the header is contained in the message.
	if ( ( 0 != iHdrByteCnt ) && ( NULL != pHdrBuf ) )
	{
		uint32_t iHdrWrdCnt = iHdrByteCnt/eWord;
		for (iElementIndex = 0; iElementIndex < iHdrWrdCnt; iElementIndex++)
		{ 
			*((uint32_t *) ulMsgAddress) = * (uint32_t *)pHdrBuf;
		}
	}

	// Process the front end of the message
	bufAlignment = AddressAlignmentChecker((uint32_t)pMsgBuf);
	pTempBuf = pMsgBuf;
	bufByteCnt = iMsgByteCnt;
	switch (bufAlignment)
	{
		//Note on the fron end the alignment enums are reversed from backend
		// eShortAndByte - need to transfer 1 byte to achive word alignment
		// eByte - need to transfer 1 byte and then a short to achive word alignment
		// eShort - same as backend
		// eWord - same as backend
		case eShortAndByteAlign:
			//Fall through to the byte case 
		case eByteAlign:
			if ( bufByteCnt > 1 )
			{
				pTempBytePtr = (uint8_t *)pMsgBuf;
		 
				*((uint8_t *) ulMsgAddress) = * pTempBytePtr;
				pTempBytePtr++;

				pTempBuf = (void *)pTempBytePtr;

				bufByteCnt -= eByte;
			}

			if (eShortAndByteAlign == bufAlignment) break;

			//If eByte case fall through to eShort case
		case eShortAlign:
			if ( bufByteCnt > 2 ) 
			{
				pTempShortPtr = (uint16_t *)pTempBuf;

				if (pSTMHandle->bigEndian)
					*((uint16_t *) ulMsgAddress) = ENDIAN_SWAP_SHORT(* pTempShortPtr);
				else
					*((uint16_t *) ulMsgAddress) = * pTempShortPtr;
				pTempShortPtr++;

				pTempBuf = (void *)pTempShortPtr;

				bufByteCnt -=eShort;
			}

			break;
	}

	// Process the whole number of eWords
	{
		uint32_t wordCnt;
		uint32_t remBytes = bufByteCnt % eWord;
		if ( 0 == remBytes )
		{
			//Must leave one word to terminate the STM message 
			wordCnt = (bufByteCnt/eWord) - 1;
		}
		else
		{
			//Since there are some byte remainder
			wordCnt = (bufByteCnt/eWord);
		}
		
		pTempWordPtr = (uint32_t *)pTempBuf;
		if (pSTMHandle->bigEndian)
		{
			for (iElementIndex = 0; iElementIndex < wordCnt; iElementIndex++)
			{
				*((uint32_t *) ulMsgAddress) = ENDIAN_SWAP(* pTempWordPtr);
				pTempWordPtr++;
				bufByteCnt -= eWord;
			}
		}
		else
		{
			for (iElementIndex = 0; iElementIndex < wordCnt; iElementIndex++)
			{
				*((uint32_t *) ulMsgAddress) = * pTempWordPtr;
				pTempWordPtr++;
				bufByteCnt -= eWord;
			}
		}

		pTempBuf = (void *)pTempWordPtr;
		  
		//Check for the case where there are 3 bytes left on a word boundary
		if ( bufByteCnt == 3 )
		{
				pTempShortPtr = (uint16_t *)pTempBuf;
				if (pSTMHandle->bigEndian)
					*((uint16_t *) ulMsgAddress) = ENDIAN_SWAP_SHORT(* pTempShortPtr);
				else
					*((uint16_t *) ulMsgAddress) = * pTempShortPtr;
				pTempShortPtr++;
				pTempBuf = (void *)pTempShortPtr;
				bufByteCnt -= eShort;	
		}
	}

	//Process the end of the message

	// Byte count must be 1, 2 or 4 at this point
	// and aligned to the proper address.
	switch (bufByteCnt)
	{
		case eByte:
			pTempBytePtr = (uint8_t *)pTempBuf;
 
			*((uint8_t *) ulEndAddress) = * pTempBytePtr;
			break;

		case eShort:
			pTempShortPtr = (uint16_t *)pTempBuf;
			if (pSTMHandle->bigEndian)
				*((uint16_t *) ulEndAddress) = ENDIAN_SWAP_SHORT(* pTempShortPtr);
			else
				*((uint16_t *) ulEndAddress) = * pTempShortPtr;
			break;

		case eWord:
			pTempWordPtr = (uint32_t *)pTempBuf;
			if (pSTMHandle->bigEndian)
				*((uint32_t *) ulEndAddress) = ENDIAN_SWAP(* pTempWordPtr);
			else
				*((uint32_t *) ulEndAddress) = * pTempWordPtr;
			break;	
	}

	return eSTM_SUCCESS;
}
#endif

#ifdef _STM

eSTM_STATUS  SendPrintf_CharStream(STMHandle * pSTMHandle, int32_t chNum, const char* pFmtString, ... )
{
    va_list arg_list;
    eSTM_STATUS retVal = eSTM_SUCCESS;

    va_start(arg_list, pFmtString);
    retVal =  SendVprintf_CharStream(pSTMHandle, chNum, pFmtString, arg_list );
    va_end(arg_list);

	return retVal;
}
eSTM_STATUS  SendVprintf_CharStream(STMHandle * pSTMHandle, int32_t chNum, const char* pFmtString, va_list arg_list )
{
	eSTM_STATUS retVal = eSTM_SUCCESS;
    int byteCnt;
    char * printfBuf = tempBuf;

    byteCnt = vsnprintf(tempBuf, sizeof tempBuf, pFmtString, arg_list);

    if (byteCnt >= sizeof tempBuf)
    {
        printfBuf = (char *)cTools_memAlloc(byteCnt+1);
        if ( NULL == printfBuf )
        {
            return eSTM_ERROR_HEAP_ALLOCATION;
        }

        vsnprintf(printfBuf, byteCnt+1, pFmtString, arg_list);
    }

    retVal = STM_putCharMsg(pSTMHandle, chNum, printfBuf, byteCnt);

    if (printfBuf != tempBuf )
        cTools_memFree(printfBuf);

    return retVal;
}


void Build_OST_Header(uint32_t protocolID, uint32_t numberOfBytes, uint32_t *pReturnBuf, int32_t *pBufSizeInBytes)
{
		
	if (numberOfBytes >= OST_SHORT_HEADER_LENGTH_LIMIT)
	{
		*pReturnBuf = OST_VERSION | OST_ENTITY_ID | protocolID | OST_SHORT_HEADER_LENGTH_LIMIT;
		pReturnBuf++;
		*(pReturnBuf) = (numberOfBytes);
		*pBufSizeInBytes = 8; /* 8 bytes for the extended length header */
	}
	else
	{
		*pReturnBuf = OST_VERSION | OST_ENTITY_ID | protocolID | numberOfBytes;
		*pBufSizeInBytes = 4; /* 4 bytes for the normal header */
	}

	return;
}
#endif

#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO))
inline uint32_t Build_CompactOST_Header(uint32_t protocolID, uint32_t numberOfBytes )
{
	
	return OST_VERSION | OST_ENTITY_ID | protocolID | numberOfBytes;
	
}

inline void Build_ExtendedOST_Header(uint32_t protocolID, uint32_t numberOfBytes, uint32_t *pReturnBuf)
{

	*pReturnBuf = (uint32_t)OST_VERSION | OST_ENTITY_ID | protocolID | OST_SHORT_HEADER_LENGTH_LIMIT;
	pReturnBuf++;
	*(pReturnBuf) = numberOfBytes;
	return;
}


void Compose_OST_MSG(const char *pInputBuf, int32_t iInputBufSize, const char *pOSTHeader, int32_t iHeaderSizeInByte, char *pReturnBuf)
{
	if ( 0 != iHeaderSizeInByte ) {
		memcpy(pReturnBuf, pOSTHeader, iHeaderSizeInByte);
		pReturnBuf += iHeaderSizeInByte;  /* we know that OST header is word aligned */
	}
	memcpy(pReturnBuf, pInputBuf, iInputBufSize); 
}

uint32_t getSyncCount(uint32_t traceBufSize )
{
	eSTM_ExportBufSize asyncIndex = eBUFSIZE_0;

	if (( traceBufSize > 0 ) && ( traceBufSize <= 4096 )) asyncIndex = eBUFSIZE_4K;
	else if (( traceBufSize > 4096 ) && ( traceBufSize <= 8192 )) asyncIndex = eBUFSIZE_8K;
	else if (( traceBufSize > 8192 ) && ( traceBufSize <= 16384 )) asyncIndex = eBUFSIZE_16K;
	else if (( traceBufSize > 16384 ) && ( traceBufSize <= 32768 )) asyncIndex = eBUFSIZE_32K;
	else if (( traceBufSize > 32768 ) && ( traceBufSize <= 65536 )) asyncIndex = eBUFSIZE_64K;
	else if (( traceBufSize > 65536 ) && ( traceBufSize <= 131072 )) asyncIndex = eBUFSIZE_128K;
	else if (( traceBufSize > 131072 ) && ( traceBufSize <= 262144 )) asyncIndex = eBUFSIZE_256K;

	return sync_count_table[asyncIndex];

}
#endif
eAddrAlignment AddressAlignmentChecker(uint32_t addressValue)
{
	eAddrAlignment addressAlignment;

	if (addressValue % STM_WORD_SIZE == 0)
	{
		addressAlignment = eWordAlign;
	}
	else if (addressValue % STM_WORD_SIZE == eShortAndByteAlign)
	{
		addressAlignment = eShortAndByteAlign;
	}
	else if (addressValue % STM_WORD_SIZE == eShortAlign)
	{
		addressAlignment = eShortAlign;
	}
	else
	{	
		addressAlignment = eByteAlign;
	}

	return addressAlignment;
}

eSTM_STATUS STMExport_IntMsg (STMHandle * pSTMHandle, int32_t chNum, 
                              const char * pModuleName, const char * pDomainName,
                              const char * pDataClass, const char * pDataType,
                              const char * pDataMsg, uint32_t * pData)

{
#if defined(_STM) || ( defined(_COMPACT) && !defined(_CIO)) 	
	register volatile uint32_t ulMsgAddress = 0;
	register volatile uint32_t ulEndAddress = 0;
    register uint32_t OST_Header;

	//Check for errors
    if ( NULL == pSTMHandle )
    {
        return eSTM_ERROR_PARM;
    }

	if ( NULL != pData)
	{
		OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_INTERNALMSG | 24;
	}
	else
	{
		OST_Header = OST_VERSION | OST_ENTITY_ID | (uint32_t)OST_PROTOCOL_INTERNALMSG | 20;
	}

	//Calculate STM Addresses
	ulMsgAddress = Compose_Regular_Address(pSTMHandle, chNum);
	ulEndAddress = Compose_Timing_Address(pSTMHandle, chNum);

	//Xport the OST Header
	*((uint32_t *) ulMsgAddress) = OST_Header;

	//Xport the Data
	*((uint32_t *) ulMsgAddress) = (uint32_t)pModuleName;
	*((uint32_t *) ulMsgAddress) = (uint32_t)pDomainName;
	*((uint32_t *) ulMsgAddress) = (uint32_t)pDataClass;
	*((uint32_t *) ulMsgAddress) = (uint32_t)pDataType;
	
	if ( NULL != pData) 
	{
        *((uint32_t *) ulMsgAddress) = (uint32_t)pDataMsg;
		*((uint32_t *) ulEndAddress) = *pData;
	}
    else
    {
        *((uint32_t *) ulEndAddress) = (uint32_t)pDataMsg;
    }
	
    return eSTM_SUCCESS;
#endif
#if defined(_CIO)
    return eSTM_ERROR_INVALID_FUNC;
#endif
}

#pragma CODE_SECTION(STMXport_open, 		".text:STMLibrarySlow")
#pragma CODE_SECTION(STMXport_printf, 		".text:STMLibrarySlow")
#pragma CODE_SECTION(STMXport_putMsg, 		".text:STMLibrarySlow")
#pragma CODE_SECTION(STMXport_putBuf, 		".text:STMLibrarySlow")
#pragma CODE_SECTION(STMXport_putWord, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_putShort, 	".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_logMsg, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_logMsg0, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_logMsg1, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_logMsg2, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_putByte, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_getBufInfo,	".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_flush, 		".text:STMLibraryFast")
#pragma CODE_SECTION(STMXport_close, 		".text:STMLibrarySlow")
