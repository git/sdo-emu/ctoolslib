/****************************************************************************
CToolsLib - DSPTraceExport Library  

Copyright (c) 2009-2010 Texas Instruments Inc. (www.ti.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****************************************************************************/
#ifndef __DSPTRACEEXPORT_ADDR_H
#define __DSPTRACEEXPORT_ADDR_H

#ifdef __cplusplus
extern "C" {
#endif

/*! \file DSPTraceAddr.h

    This file contains the DSP Trace Export addresses.
*/

/* PIN manager MMR address. */
#define PIN_MGR0   (0x1BC0130)
#define PIN_MGR1   (0x1BC0134)
#define PIN_MGR2   (0x1BC0138) 
#define PIN_MGR3   (0x1BC0164)


#if defined(TCI6486)  //Tomahawk
    #define PLLDIV  (0x029A0178)
    #define PLLCMD  (0x029A0138)
    #define PLLSTAT (0x029A013C)
   
#elif defined(TCI6488) //Faraday
    #define PLLDIV  (0x029A0184)
    #define PLLCMD  (0x029A0138)
    #define PLLSTAT (0x029A013C)

#elif defined(TCI6484) //Curie
    #define PLLDIV  (0x029A0120)
    #define PLLCMD  (0x029A0138)
    #define PLLSTAT (0x029A013C)

#elif defined(_DRA7xx) //DRA7xx
	#define PLLDIV  (0x0231011C)
	#define PLLCMD  (0x02310138)
	#define PLLSTAT (0x0231013C)

#elif defined(C66xx) //C66x devices
    #define PLLDIV2 (0x0231011C)
    #define PLLDIV3 (0x02310120)
    #define PLLCMD  (0x02310138)
    #define PLLSTAT (0x0231013C)
    #define ALNCTL  (0x02310140)

	/* JTAG ID MMR address */
	#define JTAG_ID_REG          (0x02620018)
	#define JTAG_ID_MASK         (0x0FFFFFFF)

#else
    #error No device type preprocessor defined for the DSPTraceExport library

#endif

/* Trace control register MMR address. */
#define TR_CNTL    (0x1BC013C)
#define TF_CNTL    (0x1BC014C)
#define TR_SYNC    (0x1BC0140)
#define TR_IAR_ADD (0x1BC0020)

/* PIN manager MMR address. */
#define PIN_MGR0   (0x1BC0130)
#define PIN_MGR1   (0x1BC0134)
#define PIN_MGR2   (0x1BC0138) 
#define PIN_MGR3   (0x1BC0164) 

/* DTF registers */
#define DTF_CNTL(n)				(DTF_BASE(n) + 0x000) /* Funnel control register */
#define TAGSET(n)				(DTF_BASE(n) + 0xFA0) /* Claim Tag Set register  */
#define TAGCLR(n)				(DTF_BASE(n) + 0xFA4) /* Claim Tag Clear register */
#define DTF_LOCK(n)				(DTF_BASE(n) + 0xFB0) /* Lock Access - WO */
#define DTF_LOCK_STATUS(n)		(DTF_BASE(n) + 0xFB4) /* Lock Status - RO */
#define ID(n)              		(DTF_BASE(n) + 0xFC8) /* Device ID */
/* DTF unlock value */
#define DTF_UNLOCK_VAL			0xC5ACCE55
/* DTF bit fields */
#define DTF_ENABLE_BIT			(1<<3)
#define LOCK_STATUS_IMP_BIT	 	(1<<0)
#define LOCK_STATUS_STAT_BIT	(1<<1)
#define DTF_ID_MAJOR_MASK	    (0xF<<4)
#define DTF_ID_MAJOR_VER1	    (0x1<<4)
#define DTF_ID_MAJOR_VER2	    (0x2<<4)
#define DTF_VER2_ENABLE_BIT		(1<<0)
#define DTF_VER2_NOP_BIT  		(1<<2)
#define DTF_VER2_OVERFLOW_BIT   (1<<4)
#define DTF_VER2_FLUSH_BIT  	(1<<7)
/* Device specific DTF MMR addresses. */
#if defined(TCI6484) //Curie
	#define DTF_BASE(n)			(0x02AD1000)
	#define NUM_DTF_INSTANCES 1

#elif defined(TCI6486) //Tomahawk
	#define DTF_BASE(n)			(0x02A80100 + (n << 4))
	#define NUM_DTF_INSTANCES 6

#elif defined(TCI6488) //Faraday
	#define DTF_BASE(n)			(0x02880400 + (n << 2))
	#define NUM_DTF_INSTANCES 3

#elif defined(TI816x) //Netra
    #define DTF_BASE(n)			(0x4B166000 + (n << 16))
	#define NUM_DTF_INSTANCES   1

#elif defined(TDA3x_C66x) //ADAS-low
	#define DTF_BASE(n)			(0x01D0F000)
	#define DTF_ATBID(n)		(DTF_BASE(n) + 0x400)
	#define DTF_SUPPORTS_ATBID
	#define NUM_DTF_INSTANCES   2

#elif defined(_DRA7xx) //Vayu
	#define DTF_BASE(n)			(0x01D0F000) //Local for each DSP
	#define DTF_ATBID(n)		(DTF_BASE(n) + 0x400)
	#define DTF_SUPPORTS_ATBID
	#define NUM_DTF_INSTANCES   2

#elif defined(C66xx) // C66x
	#ifdef __linux
	#define DTF_BaseAddress(n)		(0x02440000 + (n << 16))
	#define DTF_BASE(n)			virtual_DTF_BaseAddress[n]
	#define SIZEOF_DTF_SPACE	4096
	#else
	#define DTF_BASE(n)			(0x02440000 + (n << 16))
	#endif
	#define NUM_DTF_INSTANCES   8
#else
    #error No DTF base address defined in DSPTraceLib
#endif

#define VALID_DSPTRACE_HANDLE    0xbeef

#ifdef __cplusplus
}
#endif

#endif //__DSPTRACEEXPORT_ADDR_H
