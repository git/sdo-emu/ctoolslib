#ifndef __DSPTRACEEXPORT_INTERFACE_H
#define __DSPTRACEEXPORT_INTERFACE_H

#include <stdint.h> /*ANSI C99 specific type definitions */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * TI DSP Trace Export API 
 *
 * Copyright (C) 2009-2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \par ETBLIB_MAJOR_VERSION
    ETBLib major revision. This number will be changed for API modifications. 
*/
#define DSPTRACELIB_MAJOR_VERSION    (0x1)

/*! \par ETBLIB_MINOR_VERSION
    ETBLib minor revision. This number will be changed for bug fixes. 
*/
#define DSPTRACELIB_MINOR_VERSION    (0xE)

/*! \file DSPTraceExport.h
    \version 1.14
    Application Access to DSP Trace Export.

    This module allows users to program DSP Trace Export hardware.
*/


/*! \par DSPTRACE_STATE_TEND
    Used with DSPTrace_getState() result to test for DSPTRACE_STATE_TEND.
*/
#define DSPTRACE_STATE_TEND (1<<30)

/*! \par DSPTRACE_SET_TEND
    Used with DSPTrace_setState() to set DSPTRACE_STATE_TEND.
*/
#define DSPTRACE_SET_TEND (1<<22)

/*! \par DSPTRACE_CLR_TEND
    Used with DSPTrace_setState() to clear DSPTRACE_STATE_TEND.
*/
#define DSPTRACE_CLR_TEND (1<<30)

/*! \par eDSPTrace_Error
    Common function return error enumeration. 
*/
typedef enum _eDSPTrace_Error 
{
    eDSPTrace_Success           = 0,                  /*!< Function completed successfully */
    eDSPTrace_Error_Bad_Param   = -1,         /*!< Error, method parameter error */
    eDSPTrace_Error_Program     = -2,           /*!< Error, Error programming hardware */
    eDSPTrace_Error_Cannot_Own  = -3,        /*!< Error, Error programming hardware, ownership cannot be taken */
    eDSPTrace_Error_Program_PLL_Pre  = -4,        /*!< Error, Error programming PLL for trace export (PLL GO operation was already ongoing) */
    eDSPTrace_Error_Program_PLL_Post  = -5,        /*!< Error, Error programming PLL for trace export  (PLL GO operation did not finish expectedly) */
    eDSPTrace_Error_TCU = -6,                     /*!< Error, Error programming TCU */
    eDSPTrace_Error_DTF = -7                      /*!< Error, Error programming ADTF/DTF */
} eDSPTrace_Error;

/*! \par DSPTrace_error_callback
    The callback function is called with the function's exit status.  Note that this function is not exported
    by the interface, but is used as a parameter for calls. 
    
    \param[in] eDSPTrace_Error error returned by calling routine.
    \return void 

    \par Details:
    \details This is a user provided callback normally used to centralize error handling and error is desired to be handled in a
    callback routine provided by the caller instead a return error code. 
*/
typedef void(*DSPTrace_errorCallback)(eDSPTrace_Error);

/*! \par DSPTraceHandle
    DSP trace context handle structure definition.
*/
typedef struct _DSPTraceHandle
{
    uint32_t ulContext;                     /*!< DSP Trace export context handle*/
    DSPTrace_errorCallback pErrCallBack;    /*!< Error Callback*/
}DSPTraceHandle;


/*! \par DSPTrace_open
    Open and acquire DSP Trace.

    \param[in] pErrCallBack is called if not NULL and this function returns any eDSPTrace_Error value other than eDSPTrace_Success.
    \param[out] ppHandle is pointer to a DSPTracehandle pointer, the pointer is allocated by the DSpTraceLib so caller should not allocate the pointer. This should be passed back to "DSPTrace_close()" call, once done.
    \return eDSPTrace_Error. 
    
    \par Details:
    \details This function must be called as the very first call to initialize DSp Trace module.
    The return value is NULL, if failed. 
    An allocated handle pointer is returned, if success.
*/
eDSPTrace_Error  DSPTrace_open(DSPTrace_errorCallback pErrCallBack, DSPTraceHandle** ppHandle);

/*! \par DSPTrace_setClock
    Setup the trace export clock. 

    \param[in] pHandle DSPTrace Handle pointer.
    \param[in] pllDiv is number representing the Trace PLL div ratio (1 - 32) --> Clock/pllDiv.
    \return eDSPTrace_Error. 
    
    \par Details:
    \details This function sets up DSP trace clock. This divides Trace PLL clock frequency.
    This function must be called before enabling the DSPTrace to setup appropriate trace frequency.
*/
eDSPTrace_Error  DSPTrace_setClock(DSPTraceHandle* pHandle, uint32_t pllDiv);

/*! \par DSPTrace_enable
    Enables DSPTrace to export trace data. 

    \param[in] pHandle DSPTrace Handle pointer.
    \param[in] stallEnable (value 0/1) controls trace hardware can generate a stall to the CPU to prevent trace FIFOs from overflowing
    \param[in] pasueMode flag (value 0/1) to control trace clock mode (pause --> 1, free run --> 0)
    \return eDSPTrace_Error. 
    
    \par Details:
    \details This function enabled DSPTrace to export trace data. As soon as DSPTrace is enabled, it can export data to a receiver such as ETB.

*/
eDSPTrace_Error  DSPTrace_enable(DSPTraceHandle* pHandle, uint8_t stallEnable, uint8_t pasueMode);

/*! \par DSPTrace_disable
    Disables DSPTrace to export trace data. 

    \param[in] pHandle DSPTrace Handle pointer.
    \return eDSPTrace_Error. 
    
    \par Details:
    \details This function disables DSPTrace to export trace data. As soon as DSPTrace is disabled, it stops sending data to a receiver such as ETB.
*/
eDSPTrace_Error  DSPTrace_disable(DSPTraceHandle* pHandle);

/*! \par DSPTrace_close
    Function to close the DSPTrace and relese DSPTrace handle pointer.

    \param[in] pHandle DSPTrace Handle pointer.
    \return eDSPTrace_Error 
    
    \par Details:
    \details This function should be the last call made once your are done with the DSPTrace.
    After closing th DSPTrace, DSPTrace_open call is requuired before DSPTrace can be used again.
*/
eDSPTrace_Error  DSPTrace_close(DSPTraceHandle* pHandle);

/*! \par DSPTrace_getState
    Function to get the state of the DSPTrace system.

    \param[in] pHandle DSPTrace Handle pointer.
    \param[out] traceState Trace trigger state (DSPTRACE_STATE_TEND or not)
    \return eDSPTrace_Error 
    
    \par Details:
    \details This function returns the current state of the trace system.
    If trace state is DSPTRACE_STATE_TEND a trace end all trigger (see AETLib) or call to DSPTrace_setState() has 
    terminated the export of trace data.

    \note
    Note:This function will not provide the correct state information if AET is not initialized and claimed.
    See AETLib's AET_init() and AET_claim() functions.
*/

eDSPTrace_Error  DSPTrace_getState(DSPTraceHandle* pHandle, uint32_t * traceState);

/*! \par DSPTrace_setState
    Function to set the state of the DSPTrace system.

    \param[in] pHandle DSPTrace Handle pointer.
    \param[in] traceState Set or clear the TEND state.
    \return eDSPTrace_Error 
    
    \par Details:
    \details This function allows the Trace End stste to be set or cleared. Setting TEND
    will terminate trace export. TEND must be cleared to re-arm trace.

    \note
    Note:This function will not change the TEND state if AET is not initialized and claimed.
    See AETLib's AET_init() and AET_claim() functions.

*/

eDSPTrace_Error  DSPTrace_setState(DSPTraceHandle* pHandle, uint32_t traceState);

#ifdef __cplusplus
}
#endif

#endif //__DSPTrace_INTERFACE_H


