/****************************************************************************
CToolsLib - DSPTraceExport Library 

Copyright (c) 2009-2010 Texas Instruments Inc. (www.ti.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****************************************************************************/

/*! \file DSPTraceExport.c
    \version 1.13
*/

#include "DSPTraceExport.h"
#include "DSPTraceAddr.h"
#include "c6x.h"

#define CONCAT(A, B) CONCAT_(A,B)
#define CONCAT_(A, B) A#B
#define DSPTRACELIB_VERSION CONCAT("DSPTRACELIB_VER_",\
            CONCAT(DSPTRACELIB_MAJOR_VERSION,\
            CONCAT(".",DSPTRACELIB_MINOR_VERSION)))

#pragma SET_DATA_SECTION(".text")
#pragma RETAIN(version_string)
static char* version_string = DSPTRACELIB_VERSION;
#pragma SET_DATA_SECTION()

/* DSPTrace module access handle - virtual for this library */
static DSPTraceHandle stHandle;
#ifdef __linux
static uint32_t virtual_DTF_BaseAddress[NUM_DTF_INSTANCES] = {0};
#endif

/*! RETURN_DSP_CALLBACK
    ImplementaMacro to return API error.
*/
#define RETURN_DSP_CALLBACK(retValue)        if ( 0 != stHandle.pErrCallBack ) stHandle.pErrCallBack(retValue); return retValue

#if defined(C66xx)

static const uint32_t dsptracelib_k2_jtagId_table[] = {
		0x0b98102f, /* TCI6638K2K, TCI6636K2H, 66AK2Hxx */
		0x0b9a702f, /* TCI6630K2L */
		0x0b9a602f, /* 66AK2Exx, AM5K2Exx */
		0x0bb0602f  /* 66AK2Gxx */
};

const uint32_t dsptracelib_k2_jtagId_table_elements = sizeof(dsptracelib_k2_jtagId_table)/sizeof(uint32_t);

#endif

/**
* DSPTrace_open - open DSPTrace programming module interface
*/
eDSPTrace_Error  DSPTrace_open(DSPTrace_errorCallback pErrCallBack, DSPTraceHandle** ppHandle)
{
    if(ppHandle == 0 )
    {
        return eDSPTrace_Error_Bad_Param;
    }
    stHandle.ulContext = VALID_DSPTRACE_HANDLE;
    stHandle.pErrCallBack = pErrCallBack;

    (void)version_string;

    *ppHandle = &stHandle;
    return eDSPTrace_Success;
}

/**
* DSPTrace_setClock- Set DSPTrace export clock 
*/
eDSPTrace_Error  DSPTrace_setClock(DSPTraceHandle* pHandle, uint32_t pllDiv)
{

    uint32_t  pllcmd, val, waitCount=1000;
    
    if(pHandle->ulContext != VALID_DSPTRACE_HANDLE)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* PLLDIV register value: 
	bit 4:0 -- ratio = 00011b (/4), 11111b (/32)
	bit 15 -- DnEn = 1 */
	/* PLL divider value could be from 1 to 32. */
	if (pllDiv > 32)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Bad_Param);
    }
    
    /* Check that the GOSTAT bit in PLLSTAT is cleared to show that no GO operation is currently in progress */
    do
	{
    val = *(volatile uint32_t*)(PLLSTAT);
        waitCount--;
	} while (((val & 0x1) != 0x0) && (waitCount > 0));
    
    if(waitCount == 0)
    {
    	        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program_PLL_Pre);
    }

	waitCount = 1000;
	val = (1<<15) | (pllDiv -1);

#if defined(C66xx)

	uint32_t jtag_id, dev_index;

    /* Get the JTAG ID and get the device id */
    jtag_id = *(volatile uint32_t *)JTAG_ID_REG & JTAG_ID_MASK;

    for (dev_index = 0; dev_index <  dsptracelib_k2_jtagId_table_elements; dev_index++ )
    {
    	if (jtag_id == dsptracelib_k2_jtagId_table[dev_index])
    	{
    		//This is a Keystone2 device
    		*((volatile uint32_t*)PLLDIV3) = val;

    		/* Align DSP trace export clock(SYSCLK3) to PLLCTL SYSCLK */
    		*(volatile uint32_t*)(ALNCTL) |= 0x4;

    		break;
    	}
    }
    if (dev_index == dsptracelib_k2_jtagId_table_elements)
    {
    	//Not a Keystone2 device: Hence this device is a Keystone1 device
		*((volatile uint32_t*)PLLDIV2) = val;

		/* Align DSP trace export clock(SYSCLK2) to PLLCTL SYSCLK */
		*(volatile uint32_t*)(ALNCTL) |= 0x2;
    }

#else

	*((volatile uint32_t*)PLLDIV) = val;

#endif

	/* PLLCMD register value:
	bit 1 -- GOSET */
	pllcmd = 0x1;
	*((volatile uint32_t*)PLLCMD) = pllcmd;

    /* Read the GOSTAT bit in PLLSTAT to make sure the bit returns to 0 to indicate that the GO operation has completed */
    do
	{
		val = *(volatile uint32_t*)(PLLSTAT);
        waitCount--;
	} while (((val & 0x1) != 0x0) && (waitCount > 0));
    
    if(waitCount == 0)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program_PLL_Post);
    }

    return eDSPTrace_Success;
}

#if defined(TCI6484)

/**
* DSPTrace_DTF_NOPS - Set/clear Discard NOPs bit for DSP trace formatter (valid only for TCI6484).
* Used only for TCI6484 ADTF defect work around to flush a stuck packet.
*/
static eDSPTrace_Error DSPTrace_DTF_NOPS(uint8_t coreID, uint8_t discard)
{
    uint32_t value = 0;

    value = *(volatile uint32_t*)(DTF_CNTL(coreID));

    if (discard == 1)
    {
        value |= DTF_VER2_NOP_BIT;
    }
    else
    {
        value &= ~DTF_VER2_NOP_BIT;
    }
    value |= DTF_VER2_OVERFLOW_BIT;

    *(volatile uint32_t*)(DTF_CNTL(coreID)) = value;

    return eDSPTrace_Success;
}

/**
* DSPTrace_DTF_Flush - Manually flush DSP trace formatter (valid only for TCI6484).
* Used only for TCI6484 ADTF defect work around to flush a stuck packet.
*/
static eDSPTrace_Error DSPTrace_DTF_flush(uint8_t coreID)
{
    /* Only ADTF v2 supports software flush. Check the version ID. */
    if ((*(volatile uint32_t*)(ID(coreID)) & DTF_ID_MAJOR_MASK) == DTF_ID_MAJOR_VER2)
    {
    	/* ADTF is looking for a rising edge.  Set the flush bit. It will be cleared in DSPTrace_DTF_NOPS(). */
        *(volatile uint32_t*)(DTF_CNTL(coreID)) = *(volatile uint32_t*)(DTF_CNTL(coreID)) | DTF_VER2_FLUSH_BIT;
        *(volatile uint32_t*)(DTF_CNTL(coreID)) = *(volatile uint32_t*)(DTF_CNTL(coreID)) & ~DTF_VER2_FLUSH_BIT;
    }
    DSPTrace_DTF_NOPS(coreID, 0);
    DSPTrace_DTF_NOPS(coreID, 1);

    return eDSPTrace_Success;
}

#endif

/**
* DSPTrace_DTFDisable - Disable DSP trace formatter.
*/
static eDSPTrace_Error DSPTrace_DTFDisable(uint8_t coreID)
{


#ifdef TCI6484
    /* In the TCI6484 case the DTF flush is not integrated so we must do a manual flush */
	uint32_t value = 0;
    value = DSPTrace_DTF_flush(coreID);
    if (value != eDSPTrace_Success)
    	return eDSPTrace_Error_DTF;
#endif

    /* Disable the DTF */
#if defined(TCI6486) || defined(TCI6488)
    *(volatile uint32_t*)(DTF_CNTL(coreID)) &= ~DTF_ENABLE_BIT;
#endif

    /* Disable the ADTFv2 */
#if defined(C66xx) || defined(_DRA7xx) || defined(_TDA3x) || defined(TCI6484)
    *(volatile uint32_t*)(TAGCLR(coreID)) = 0x1;
	*(volatile uint32_t*)(DTF_CNTL(coreID)) &= ~DTF_VER2_ENABLE_BIT;

	/* Verify that DTF_CNTL is disabled. */
	if ((*((volatile uint32_t*)DTF_CNTL(coreID)) & DTF_VER2_ENABLE_BIT) != 0)
	{
		return eDSPTrace_Error_DTF;
    }
#endif


#ifdef __linux
	cTools_memUnMap(virtual_DTF_BaseAddress((coreID)), SIZEOF_DTF_SPACE);
	virtual_DTF_BaseAddress(coreID) = 0;
#endif

	return eDSPTrace_Success;
}

/**
* DSPTrace_DTFEnable - Enable DSP trace formatter.
*/
static eDSPTrace_Error DSPTrace_DTFEnable(uint8_t coreID)
{
	uint32_t value = 0;

#ifdef __linux
    virtual_DTF_BaseAddress[n] = cTools_memMap(DTF_BaseAddress(coreID), SIZEOF_DTF_SPACE);
#endif

#if defined(C66xx) || defined(_DRA7xx) || defined(_TDA3x) || defined(TI816x) || defined(TCI6484)
    /* Configure DTF/ADTF */
	value = *(volatile uint32_t*)(DTF_LOCK_STATUS(coreID));
	if (value & LOCK_STATUS_IMP_BIT)
	{
		/* If this bit is set, it device access is locked, we need to unlock the device*/
		if (value & LOCK_STATUS_STAT_BIT)
		{
			*(volatile uint32_t*)(DTF_LOCK(coreID)) = DTF_UNLOCK_VAL;
		}
	}

	/* Check ADTF lock status again to confirm that it is unlocked */
	value = *(volatile uint32_t*)(DTF_LOCK_STATUS(coreID));
	if (value & LOCK_STATUS_STAT_BIT)
	{
		RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
	}

	/* Claim Tag Set */
	*(volatile uint32_t*)(TAGSET(coreID)) = 0x1;

	/* Enable ADTF */
	*(volatile uint32_t*)(DTF_CNTL(coreID)) = *(volatile uint32_t*)(DTF_CNTL(coreID)) | 0x1;

#ifdef DTF_SUPPORTS_ATBID
	/* Set the ATB ID */
    *(volatile uint32_t*)(DTF_ATBID(coreID)) = coreID + 1;
#endif

	/* Verify that DTF_CNTL is enabled. */
	if ((*((volatile uint32_t*)DTF_CNTL(coreID)) & 0x1) != 0x1)
	{
		RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
	}

#endif

#if defined(TCI6486) || defined(TCI6488)
	/* Claim DTF ownership, DTF_CNTL:AOWN=0, DTFFLUSH=0, DTFENABLE=0, SPMDISABLE=0, DTFOWN_ST=1 */
	*(volatile uint32_t*)(DTF_CNTL(coreID)) = 0x1;

	/* check AOWN bit to see if DTF ownership is now owned by application  code. */
	value = *(volatile uint32_t*)(DTF_CNTL(coreID));
	if ((value & 0x80000000) != 0x80000000)
	{
		RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
	}

	/* enable DTF ownership, DTF_CNTL:AOWN=0, DTFFLUSH=0, DTFENABLE=0, SPMDISABLE=0, DTFOWN_ST=2 */
	*(volatile uint32_t*)(DTF_CNTL(coreID)) = 2;

	/* enable DTF and disable SPM input DTF_CNTL: AOWN=0, DTFFLUSH=0, DTFENABLE=1, SPMDISABLE=1, DTFOWN_ST=10b */
	*(volatile uint32_t*)(DTF_CNTL(coreID)) = 0xE;
#endif


	return eDSPTrace_Success;
}

/**
* DSPTrace_enable- Enable DSPTrace to start exporting trace data
*/
eDSPTrace_Error  DSPTrace_enable(DSPTraceHandle* pHandle, uint8_t stallEnable, uint8_t pauseMode)
{
    uint32_t flow=2;
    eDSPTrace_Error ret = eDSPTrace_Success;

    if(pHandle->ulContext != VALID_DSPTRACE_HANDLE)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    if (pauseMode == 1)
        flow = 0;

    /* Disable Trace Unit before enabling*/
    *((volatile uint32_t*)TR_CNTL) = 0;

    /* Verify TR_CNTL write before accessing TCU_CNTL. */
    if ((*((volatile uint32_t*)TR_CNTL) & 0x3) != 0x0)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /*Check if TCU owned by Debugger */
    if ( !(   ( (TCU_CNTL & 0x8000003) == 0x80000000)
    	   || ( (TCU_CNTL & 0x8000000) == 0x0) ))
    {
    	RETURN_DSP_CALLBACK(eDSPTrace_Error_TCU);
    }

    /* Claim ownership for trace export block */
    if ( (TCU_CNTL & 0x3) == 0) TCU_CNTL = 0x1;  //Can't change state from enabled to claim or Trace will not work!

    /* Program Pin manager to have EMU pin allocation as follows to conform to DTF spec 3.2.1 requirements */
    /* EMU0=trace_data[19], EMU1=trace_data[18], ..., EMU19=trace_data[0],  */
    /* EMU20=clock0, EMU21=clock1                                           */
    *((volatile uint32_t*)PIN_MGR0) = 0xCCDDEEFF;
    *((volatile uint32_t*)PIN_MGR1) = 0x8899AABB;
    *((volatile uint32_t*)PIN_MGR2) = 0x00556677;
    *((volatile uint32_t*)PIN_MGR3) = 0x00000000;

    /* Verify PIN_MGR3 write. */
    if (*((volatile uint32_t*)PIN_MGR3) != 0x0)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* Enable ownership for trace export block to start exporting trace. */
    TCU_CNTL = 0x2;

    /* Disable ADTF */
    ret = DSPTrace_DTFDisable(DNUM);
    if(ret != eDSPTrace_Success)
    {
    	RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
    }

    /* Program trace control */
    *((volatile uint32_t*)TR_CNTL) = (0x4003d) | (stallEnable << 15); /* claim and enable all the streams */

    /* Verify TR_CNTL write. */
    if ((*((volatile uint32_t*)TR_CNTL) & 0x80000003) != 0x80000001)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* TF_CNTL: setup trace formatter control */
    *((volatile uint32_t*)TF_CNTL) = (0x2c1400) | (flow);

    /* Verify TF_CNTL write. */
    if ((*((volatile uint32_t*)TF_CNTL) & 0xF) != flow)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* Enable DTF */
    ret = DSPTrace_DTFEnable(DNUM);
    if(ret != eDSPTrace_Success)
    {
    	RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
    }
    
    /* TF_CNTL: setup trace formatter control */
    *((volatile uint32_t*)TF_CNTL) = (0x2c1420) | (flow);

    /* Verify TF_CNTL write. */
    if ((*((volatile uint32_t*)TF_CNTL) & 0xF) != flow)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }
    
	/* TR_SYNC: sync count = 3*/
    *((volatile uint32_t*)TR_SYNC) = 0x3; 
    
    /* Verify TR_SYNC write. */
    if ((*((volatile uint32_t*)TR_SYNC) & 0x7) != 0x3)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* TF_CNTL: go trace formatter control */
    *((volatile uint32_t*)TF_CNTL) = (0x2c1430) | (flow);

    /* Verify TF_CNTL write. */
    if ((*((volatile uint32_t*)TF_CNTL) & 0xF) != flow)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* Program trace control */
    *((volatile uint32_t*)TR_CNTL) = (0x4003e) | (stallEnable << 15); /* claim and enable all the streams */

    /* Verify TR_CNTL write before exiting. */
    if ((*((volatile uint32_t*)TR_CNTL) & 0x80000003) != 0x80000002)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    return eDSPTrace_Success;
}

/**
* DSPTrace_disable - Disable DSPTrace to stop exporting trace data
*/
eDSPTrace_Error  DSPTrace_disable(DSPTraceHandle* pHandle)
{
    uint32_t value=0;
    eDSPTrace_Error ret = eDSPTrace_Success;

    if(pHandle->ulContext != VALID_DSPTRACE_HANDLE)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    /* Disable DTF */
    ret = DSPTrace_DTFDisable(DNUM);
    if(ret != eDSPTrace_Success)
    {
    	RETURN_DSP_CALLBACK(eDSPTrace_Error_DTF);
    }

	value = *((volatile uint32_t*)TF_CNTL);

    /* Stop  trace */
    value = value & 0xffffff00;
    *((volatile uint32_t*)TF_CNTL) = value;//(0x1400);

    /* Verify TF_CNTL write before accessing TCU_CNTL. */
    if ((*((volatile uint32_t*)TF_CNTL) & 0xFF) != 0x0)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

   /* Stop exporting trace. */
    TCU_CNTL = 0x1;

    return eDSPTrace_Success;
}

/**
* DSPTrace_close- Close DSPTrace programming module interface
*/
eDSPTrace_Error  DSPTrace_close(DSPTraceHandle* pHandle)
{
    if(pHandle->ulContext == VALID_DSPTRACE_HANDLE)
    {
        pHandle->ulContext = 0;
    }

     /* Release ownership */
    *((volatile uint32_t*)TR_CNTL) = 0x0000C03C;

    /* Verify TR_CNTL write before accessing TCU_CNTL. */
    if ((*((volatile uint32_t*)TR_CNTL) & 0x3) != 0x0)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

     /* Release ownership for trace export block */
     TCU_CNTL = 0;
    
    return eDSPTrace_Success;
}

eDSPTrace_Error  DSPTrace_getState(DSPTraceHandle* pHandle, uint32_t * traceState)
{
    if(pHandle->ulContext != VALID_DSPTRACE_HANDLE)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    *traceState = *((volatile uint32_t*)TR_IAR_ADD);
    
    return eDSPTrace_Success;   
}

eDSPTrace_Error  DSPTrace_setState(DSPTraceHandle* pHandle, uint32_t traceState)
{
    if(pHandle->ulContext != VALID_DSPTRACE_HANDLE)
    {
        RETURN_DSP_CALLBACK(eDSPTrace_Error_Program);
    }

    *((volatile uint32_t*)TR_IAR_ADD) = traceState;
    
    return eDSPTrace_Success;
}
