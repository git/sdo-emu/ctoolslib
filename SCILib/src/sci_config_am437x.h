/*
 * sci_config_am437x.h
 *
 * Statistic Collector Instrumentation Library 
 * - Statistic Collector module specific definitions
 * - Device specific configurations
 *
 * Copyright (C) 20131 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef SCI_CONFIG_AM437X_H
#define SCI_CONFIG_AM437X_H

#define SC_VER 116
#include "sc_reg.h"
#define SC_INTERFACE_VERSION 1.0

#define SC_LAT0_BASE 0x44002000
#define SC_LAT0_NUM_CNTRS 4
#define SC_LAT0_NUM_PROBES 5
	
#define SC_LAT1_BASE 0x44003000
#define SC_LAT1_NUM_CNTRS 6
#define SC_LAT1_NUM_PROBES 6
	
#define SC_LAT2_BASE 0x44004000
#define SC_LAT2_NUM_CNTRS 4
#define SC_LAT2_NUM_PROBES 5
	
#define SC_LAT3_BASE 0x44804000
#define SC_LAT3_NUM_CNTRS 4
#define SC_LAT3_NUM_PROBES 8
	

/* Statistic Collector counter element definition*/
struct sc_cnt_elements {
    int num_filters;
    uint32_t cnt_offset;   
};

/* lat0 Statistic Collector counter register configurations */
struct sc_lat_0_counters {
	    struct sc_lat_cnt_filter1 cnt[4];
    };


/* lat0 Statistic Collector register configuration */
struct sc_lat_0_reg_map {
sc_lat_regs regs;
struct sc_lat_0_counters counters;  
};

/* lat1 Statistic Collector counter register configurations */
struct sc_lat_1_counters {
	    struct sc_lat_cnt_filter1 cnt[6];
    };


/* lat1 Statistic Collector register configuration */
struct sc_lat_1_reg_map {
sc_lat_regs regs;
struct sc_lat_1_counters counters;  
};

/* lat2 Statistic Collector counter register configurations */
struct sc_lat_2_counters {
	    struct sc_lat_cnt_filter1 cnt[4];
    };


/* lat2 Statistic Collector register configuration */
struct sc_lat_2_reg_map {
sc_lat_regs regs;
struct sc_lat_2_counters counters;  
};

/* lat3 Statistic Collector counter register configurations */
struct sc_lat_3_counters {
	    struct sc_lat_cnt_filter1 cnt[4];
    };


/* lat3 Statistic Collector register configuration */
struct sc_lat_3_reg_map {
sc_lat_regs regs;
struct sc_lat_3_counters counters;  
};

/* Statistic Collector counter maps */

struct sc_cnt_elements sc_lat_0_cnt_map[] = {
	1, (uint32_t)(&((struct sc_lat_0_reg_map *)0)->counters.cnt[0]),
	1, (uint32_t)(&((struct sc_lat_0_reg_map *)0)->counters.cnt[1]),
	1, (uint32_t)(&((struct sc_lat_0_reg_map *)0)->counters.cnt[2]),
	1, (uint32_t)(&((struct sc_lat_0_reg_map *)0)->counters.cnt[3])
};

struct sc_cnt_elements sc_lat_1_cnt_map[] = {
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[0]),
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[1]),
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[2]),
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[3]),
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[4]),
	1, (uint32_t)(&((struct sc_lat_1_reg_map *)0)->counters.cnt[5])
};

struct sc_cnt_elements sc_lat_2_cnt_map[] = {
	1, (uint32_t)(&((struct sc_lat_2_reg_map *)0)->counters.cnt[0]),
	1, (uint32_t)(&((struct sc_lat_2_reg_map *)0)->counters.cnt[1]),
	1, (uint32_t)(&((struct sc_lat_2_reg_map *)0)->counters.cnt[2]),
	1, (uint32_t)(&((struct sc_lat_2_reg_map *)0)->counters.cnt[3])
};

struct sc_cnt_elements sc_lat_3_cnt_map[] = {
	1, (uint32_t)(&((struct sc_lat_3_reg_map *)0)->counters.cnt[0]),
	1, (uint32_t)(&((struct sc_lat_3_reg_map *)0)->counters.cnt[1]),
	1, (uint32_t)(&((struct sc_lat_3_reg_map *)0)->counters.cnt[2]),
	1, (uint32_t)(&((struct sc_lat_3_reg_map *)0)->counters.cnt[3])
};

/* Statistic Collector probe element definition */
struct sc_probe_element {
	uint32_t probe_id;
	int32_t req_port_num;
	int32_t rsp_port_num;
};

enum sc_probe_valid {
	SCI_INVALID_PROBE,
 	SCI_VALID_PROBE
};

/* Statistic Collector probe element maps */
struct sc_probe_element sc_lat_0_probe_map[] = { 
{SCI_MPU_128, 0, 1},
{SCI_MPU_64, 2, 3},
{SCI_GFX, 4, 5},
{SCI_DSS, 6, 7},
{SCI_EMIF, 8, 9}
};

struct sc_probe_element sc_lat_1_probe_map[] = { 
{SCI_EDMA3TC0_RD, 0, 1},
{SCI_EDMA3TC1_RD, 2, 3},
{SCI_EDMA3TC2_RD, 4, 5},
{SCI_EDMA3TC0_WR, 6, 7},
{SCI_EDMA3TC1_WR, 8, 9},
{SCI_EDMA3TC2_WR, 10, 11}
};

struct sc_probe_element sc_lat_2_probe_map[] = { 
{SCI_PRU_ICSS1, 0, 1},
{SCI_CRYPTODMA_WR, 2, 3},
{SCI_CPSW, 4, 5},
{SCI_PRU_ICSS0, 6, 7},
{SCI_CRYPTODMA_RD, 8, 9}
};

struct sc_probe_element sc_lat_3_probe_map[] = { 
{SCI_0_USB0, 0, 1},
{SCI_1_USB0, 2, 3},
{SCI_GPMC, 4, 5},
{SCI_MMCSD2, 6, 7},
{SCI_MCASP0_DATA, 8, 9},
{SCI_MCASP1_DATA, 10, 11},
{SCI_0_USB1, 12, 13},
{SCI_1_USB1, 14, 15}
};




/* Statistic Collector element definition */
enum sc_module_type {SDRAM, MSTR};
struct sc_element_map {
        enum sc_module_type mod_type;
        uint32_t base_addr;
        int mod_size;                   /* In bytes */ 
        int num_counters;
        struct sc_cnt_elements * cnt_map;
        int num_probes;
        struct sc_probe_element * probe_map;
        enum sc_probe_valid * sc_no_filter_valid_probe_map;
        uint32_t cnt_overflow_load; /*Counter saturation value */
        uint32_t cnt_overflow_lat; /*Latency counter saturation value */
};    

/* Statistic Collector element maps */
struct sc_element_map sc_lat_0_map = { 
		MSTR,
		SC_LAT0_BASE,
		sizeof(struct sc_lat_0_reg_map),
		SC_LAT0_NUM_CNTRS,
		sc_lat_0_cnt_map,
		SC_LAT0_NUM_PROBES,
		sc_lat_0_probe_map,
		NULL,
		SC_LAT_LOAD_CNT_OVERFLOW,
		SC_LAT_LATENCY_CNT_OVERFLOW
};

struct sc_element_map sc_lat_1_map = { 
		MSTR,
		SC_LAT1_BASE,
		sizeof(struct sc_lat_1_reg_map),
		SC_LAT1_NUM_CNTRS,
		sc_lat_1_cnt_map,
		SC_LAT1_NUM_PROBES,
		sc_lat_1_probe_map,
		NULL,
		SC_LAT_LOAD_CNT_OVERFLOW,
		SC_LAT_LATENCY_CNT_OVERFLOW
};

struct sc_element_map sc_lat_2_map = { 
		MSTR,
		SC_LAT2_BASE,
		sizeof(struct sc_lat_2_reg_map),
		SC_LAT2_NUM_CNTRS,
		sc_lat_2_cnt_map,
		SC_LAT2_NUM_PROBES,
		sc_lat_2_probe_map,
		NULL,
		SC_LAT_LOAD_CNT_OVERFLOW,
		SC_LAT_LATENCY_CNT_OVERFLOW
};

struct sc_element_map sc_lat_3_map = { 
		MSTR,
		SC_LAT3_BASE,
		sizeof(struct sc_lat_3_reg_map),
		SC_LAT3_NUM_CNTRS,
		sc_lat_3_cnt_map,
		SC_LAT3_NUM_PROBES,
		sc_lat_3_probe_map,
		NULL,
		SC_LAT_LOAD_CNT_OVERFLOW,
		SC_LAT_LATENCY_CNT_OVERFLOW
};


struct sc_element_map * sc_map[] = { 
&sc_lat_0_map,
&sc_lat_1_map,
&sc_lat_2_map,
&sc_lat_3_map
 };
 
 /* Statistic Collector names are in sc_map order */
const char * sci_unit_name_table[] = {
    "LAT0",
    "LAT1",
    "LAT2",
    "LAT3"
};

/* Statistic Collector probe name tables */
const char * sci_sdram_probe_name_table[];
const char * sci_mstr_probe_name_table[] = { 
"MPU_128",
"MPU_64",
"GFX",
"DSS",
"EMIF",
"EDMA3TC0_RD",
"EDMA3TC1_RD",
"EDMA3TC2_RD",
"EDMA3TC0_WR",
"EDMA3TC1_WR",
"EDMA3TC2_WR",
"PRU_ICSS1",
"CRYPTODMA_WR",
"CPSW",
"PRU_ICSS0",
"CRYPTODMA_RD",
"USB0",
"USB0",
"GPMC",
"MMCSD2",
"MCASP0_DATA",
"MCASP1_DATA",
"USB1",
"USB1"
};

/* Statistic Collector master address table */
enum sci_master_addr sci_master_addr_table[] = { 
		SCI_MSTID_MPU_128,
		SCI_MSTID_MPU_64,
		SCI_MSTID_DAP,
		SCI_MSTID_IEEE1500,
		SCI_MSTID_PRU_ICSS0,
		SCI_MSTID_PRU_ICSS1,
		SCI_MSTID_EDMA3TC0_RD,
		SCI_MSTID_EDMA3TC0_WR,
		SCI_MSTID_EDMA3TC1_RD,
		SCI_MSTID_EDMA3TC1_WR,
		SCI_MSTID_EDMA3TC2_RD,
		SCI_MSTID_EDMA3TC2_WR,
		SCI_MSTID_GFX,
		SCI_MSTID_DSS,
		SCI_MSTID_CRYPTODMA_RD,
		SCI_MSTID_CRYPTODMA_WR,
		SCI_MSTID_VPFE0,
		SCI_MSTID_VPFE1,
		SCI_MSTID_CPSW,
		SCI_MSTID_USB0,
		SCI_MSTID_USB1,
		SCI_MSTID_STATCOL0,
		SCI_MSTID_STATCOL1,
		SCI_MSTID_STATCOL2,
 		SCI_MSTID_STATCOL3
};

/* Statistic Collector master name table. 
        In sci_master_addr_table order.
       */
const char * sci_master_name_table[] = { 
		"MPU_128",
		"MPU_64",
		"DAP",
		"IEEE1500",
		"PRU_ICSS0",
		"PRU_ICSS1",
		"EDMA3TC0_RD",
		"EDMA3TC0_WR",
		"EDMA3TC1_RD",
		"EDMA3TC1_WR",
		"EDMA3TC2_RD",
		"EDMA3TC2_WR",
		"GFX",
		"DSS",
		"CRYPTODMA_RD",
		"CRYPTODMA_WR",
		"VPFE0",
		"VPFE1",
		"CPSW",
		"USB0",
		"USB1",
		"STATCOL0",
		"STATCOL1",
		"STATCOL2",
 		"STATCOL3"
};

/* Statistic Collector slave address table */
enum sci_slave_addr sci_slave_addr_table[] = {
		SCI_SLVID_EMIF,
		SCI_SLVID_OCMCRAM,
		SCI_SLVID_EDMA3TC0_CFG,
		SCI_SLVID_EDMA3TC1_CFG,
		SCI_SLVID_EDMA3TC2_CFG,
		SCI_SLVID_EDMA3CC,
		SCI_SLVID_DEBUGSS,
		SCI_SLVID_ADC1,
		SCI_SLVID_L4_PER_0,
		SCI_SLVID_L4_PER_1,
		SCI_SLVID_L4_PER_2,
		SCI_SLVID_L4_PER_3,
		SCI_SLVID_MCASP0_DATA,
		SCI_SLVID_MCASP1_DATA,
		SCI_SLVID_GPMC,
		SCI_SLVID_L4_FW,
		SCI_SLVID_L3F_CFG,
		SCI_SLVID_L3S_CFG,
		SCI_SLVID_AES,
		SCI_SLVID_SHA,
		SCI_SLVID_MMCSD2,
		SCI_SLVID_L4_WKUP,
		SCI_SLVID_GFX,
		SCI_SLVID_ADC0,
		SCI_SLVID_L4_FAST,
		SCI_SLVID_QSPI,
		SCI_SLVID_MPU_L2_CACHE,
		SCI_SLVID_DES,
 		SCI_SLVID_PRU_ICSS1
};

/* Statistic Collector slave name table.
       In sci_slave_addr_table order.
       */
const char * sci_slave_name_table[] = { 
		"EMIF",
		"OCMCRAM",
		"EDMA3TC0_CFG",
		"EDMA3TC1_CFG",
		"EDMA3TC2_CFG",
		"EDMA3CC",
		"DEBUGSS",
		"ADC1",
		"L4_PER_0",
		"L4_PER_1",
		"L4_PER_2",
		"L4_PER_3",
		"MCASP0_DATA",
		"MCASP1_DATA",
		"GPMC",
		"L4_FW",
		"L3F_CFG",
		"L3S_CFG",
		"AES",
		"SHA",
		"MMCSD2",
		"L4_WKUP",
		"GFX",
		"ADC0",
		"L4_FAST",
		"QSPI",
		"MPU_L2_CACHE",
		"DES",
 		"PRU_ICSS1"
};

#endif
