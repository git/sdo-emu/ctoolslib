/*
 * sci_config_omap5430.h
 *
 * Statistic Collector Instrumentation Library 
 * - OMAP5430 Statistic Collector register definitions
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef SCI_CONFIG_OMAP5430_H
#define SCI_CONFIG_OMAP5430_H

#define SC_VER 116
#include "sc_reg.h"

#define SC_INTERFACE_VERSION 001

#define SC_SDRAM_BASE 0x45001000 
#define SC_LAT_0_BASE 0x45002000
#define SC_LAT_1_BASE 0x45003000

#define SC_SDRAM_NUM_CNTRS 8 
#define SC_SDRAM_NUM_PROBES 5

#define SC_LAT_NUM_CNTRS 4
#define SC_LAT_0_NUM_PROBES 8
#define SC_LAT_1_NUM_PROBES 6

/* SDRAM Statistic Collector counter register configurations */
struct sc_sdram_counters {
    struct sc_sdram_cnt_filter2 cnt0;
    struct sc_sdram_cnt_filter1 cnt1;
    struct sc_sdram_cnt_filter2 cnt2;
    struct sc_sdram_cnt_filter1 cnt3;
    struct sc_sdram_cnt_filter1 cnt4;
    struct sc_sdram_cnt_filter1 cnt5;
    struct sc_sdram_cnt_filter0 cnt6;
    struct sc_sdram_cnt_filter0 cnt7;
};

/* LAT Statistic Collector counter register configurations */
struct sc_lat_counters {
	struct sc_lat_cnt_filter1 cnt[4];
};

/* SDRAM Statistic Collector register configuration */
struct sc_sdram_reg_map {
		sc_sdram_regs regs;
		struct sc_sdram_counters counters;
};

/* LAT Statistic Collector register configuration */
struct sc_lat_reg_map {
		sc_lat_regs regs;
		struct sc_lat_counters counters;
};

/* Statistic Collector counter element definition*/
struct sc_cnt_elements {
    int num_filters;
    uint32_t cnt_offset;   
};

/* Statistic Collector counter maps */
struct sc_cnt_elements sdram_cnt_map[] = {
    2, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt0),
    1, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt1),
    2, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt2),
    1, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt3),
    1, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt4),
    1, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt5),
    0, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt6),
    0, (uint32_t)(&((struct sc_sdram_reg_map *)0)->counters.cnt7)
};

struct sc_cnt_elements lat_cnt_map[] = {
    1, (uint32_t)(&((struct sc_lat_reg_map *)0)->counters.cnt[0]),
    1, (uint32_t)(&((struct sc_lat_reg_map *)0)->counters.cnt[1]),
    1, (uint32_t)(&((struct sc_lat_reg_map *)0)->counters.cnt[2]),
    1, (uint32_t)(&((struct sc_lat_reg_map *)0)->counters.cnt[3])
};

/* Statistic Collector probe element definition */
struct sc_probe_element {
    uint32_t probe_id;
    int32_t req_port_num;
    int32_t rsp_port_num;
};

struct sc_probe_element sc_sdram_probe_map[] = {
    {SCI_EMIF1, 0, 1},
    {SCI_EMIF2, 2, 3},
    {SCI_MA_MPU_P1, 4, 5},
    {SCI_MA_MPU_P2, 6, 7}, 
    {SCI_EMIF_LL, 8, 9}
};

enum sc_probe_valid {
	SCI_INVALID_PROBE,
 	SCI_VALID_PROBE
};


enum sc_probe_valid sc_sdram_no_filter_valid_probe_map[] = {
		SCI_INVALID_PROBE, /*SCI_EMIF1 restricted - can not use */
		SCI_INVALID_PROBE, /*SCI_EMIF2 restricted - can not use */
		SCI_VALID_PROBE, /*SCI_MA_MPU_P1 ok to use */
		SCI_VALID_PROBE, /*SCI_MA_MPU_P2 ok to use */
		SCI_VALID_PROBE  /*SCI_EMIF_LL ok to use */
};


struct sc_probe_element sc_lat_0_probe_map[] = {
    {SCI_MPU, 0, 1},
    {SCI_DSP, 2, 3},
    {SCI_SDMA_RD, 4, 5},
    {SCI_SDMA_WR, 6, 7},
    {SCI_DSS, 8, 9},
    {SCI_ISS, 10, 11},
    {SCI_GPU_P1, 12, 13},
    {SCI_BB2D_P1, 14,15}
};
                
struct sc_probe_element sc_lat_1_probe_map[] = {
    {SCI_IVAHD, 0, 1},
    {SCI_MPU, 2, 3},
    {SCI_SDMA_WR, 4, 5},
    {SCI_GPU_P2, 6, 7},
    {SCI_IPU, 8, 9},
    {SCI_BB2D_P2, 10,11}
};

/* Statistic Collector element definition */
enum sc_module_type {SDRAM, MSTR};

struct sc_element_map {
    enum sc_module_type mod_type;
    uint32_t base_addr;
    int mod_size;                   /* In bytes */
    int num_counters;
    struct sc_cnt_elements * cnt_map;
    int num_probes;
    struct sc_probe_element * probe_map;
    enum sc_probe_valid * sc_no_filter_valid_probe_map;
    uint32_t cnt_overflow_load;
    uint32_t cnt_overflow_lat;
};    

/* Statistic Collector element maps */
struct sc_element_map sc_sdram_map = {
    SDRAM,
    SC_SDRAM_BASE,
    sizeof(struct sc_sdram_reg_map),
    SC_SDRAM_NUM_CNTRS,
    sdram_cnt_map,
    SC_SDRAM_NUM_PROBES,
    sc_sdram_probe_map,
    sc_sdram_no_filter_valid_probe_map,
    SC_SDRAM_LOAD_CNT_OVERFLOW,
    SC_SDRAM_LATENCY_CNT_OVERFLOW
};

struct sc_element_map sc_lat_0_map = {
    MSTR,
    SC_LAT_0_BASE,
    sizeof(struct sc_lat_reg_map),
    SC_LAT_NUM_CNTRS,
    lat_cnt_map,
    SC_LAT_0_NUM_PROBES,
    sc_lat_0_probe_map,
    NULL,
    SC_LAT_LOAD_CNT_OVERFLOW,
    SC_LAT_LATENCY_CNT_OVERFLOW
};

struct sc_element_map sc_lat_1_map = {
    MSTR,
    SC_LAT_1_BASE,
    sizeof(struct sc_lat_reg_map),
    SC_LAT_NUM_CNTRS,
    lat_cnt_map,
    SC_LAT_1_NUM_PROBES,
    sc_lat_1_probe_map,
    NULL,
    SC_LAT_LOAD_CNT_OVERFLOW,
    SC_LAT_LATENCY_CNT_OVERFLOW
};

struct sc_element_map * sc_map[] = {
    &sc_sdram_map,
    &sc_lat_0_map,
    &sc_lat_1_map
};

/* Statistic Collector names are in sc_map order */
const char * sci_unit_name_table[] = {
    "SDRAM",
    "LAT0",
    "LAT1"
};

/* Statistic Collector probe name tables */
const char * sci_sdram_probe_name_table[] = {
    "EMIF1",
    "EMIF2",
    "MA_MPU_P1",
    "MA_MPU_P2",
    "EMIF1_LL"
};

const char * sci_mstr_probe_name_table[] = {
    "MPU",
    "DSP",
    "SDMA_RD",
    "SDMA_WR",
    "DSS",
    "ISS",
    "IVAHD",
    "GPU_P1",
    "GPU_P2",
    "IPU"
};

/* Statistic Collector master address table */
enum sci_master_addr sci_master_addr_table[] = {
    SCI_MSTID_MPU,
    SCI_MSTID_DAP,
    SCI_MSTID_DSP,
    SCI_MSTID_IVAHD,
    SCI_MSTID_ISS,
    SCI_MSTID_IPU,
    SCI_MSTID_FDIF,
    SCI_MSTID_CAL,
    SCI_MSTID_SDMA_RD,
    SCI_MSTID_SDMA_WR,
    SCI_MSTID_GPU_P1,
    SCI_MSTID_GPU_P2,
    SCI_MSTID_BB2D_P1,
    SCI_MSTID_BB2D_P2,
    SCI_MSTID_DSS,
    SCI_MSTID_C2C,
    SCI_MSTID_LLI,
    SCI_MSTID_HSI,
    SCI_MSTID_UNIPRO1,
    SCI_MSTID_UNIPRO2,
    SCI_MSTID_MMC1,
    SCI_MSTID_MMC2,
    SCI_MSTID_SATA,
    SCI_MSTID_USB_HOST_HS,
    SCI_MSTID_USB_OTG_HS,
    SCI_MASTID_ALL
};

/* Statistic Collector master name table.
 * In sci_master_addr_table order.
 */
const char * sci_master_name_table[] = {
    "MPU",
    "DAP",
    "DSP",
    "IVA",
    "ISS",
    "IPU",
    "FDIF",
    "CAL",
    "DMA_SYSTEM_RD",
    "DMA_SYSTEM_WR",
    "GPU_P1",
    "GPU_P2",
    "BB2D_P1"
    "BB2D_P2"
    "DSS",
    "C2C",
    "LLI",
    "HSI",
    "UNIPRO1",
    "UNIPRO2",
    "MMC1",
    "MMC2",
    "SATA",
    "USB_HOST_HS",
    "USB_OTG_HS"
    "ALL"
};

/* Statistic Collector slave address table */
enum sci_slave_addr sci_slave_addr_table[] = {
    SCI_SLVID_HOST_CLK1,
    SCI_SLVID_DMM1,
    SCI_SLVID_DMM2,
    SCI_SLVID_ABE,
    SCI_SLVID_L4_CFG,
    SCI_SLVID_HOST_CLK2,
    SCI_SLVID_GPMC,
    SCI_SLVID_OCM_RAM,
    SCI_SLVID_DSS,
    SCI_SLVID_ISS,
    SCI_SLVID_IPU,
    SCI_SLVID_GPU,
    SCI_SLVID_IVA_CFG,
    SCI_SLVID_IVA_SL2,
    SCI_SLVID_L4_PER0,
    SCI_SLVID_L4_PER1,
    SCI_SLVID_L4_PER2,
    SCI_SLVID_L4_PER3,
    SCI_SLVID_C2C,
    SCI_SLVID_LLI,
    SCI_SLVID_CAL,
    SCI_SLVID_HOST_CLK3,
    SCI_SLVID_L3_INSTR,
    SCI_SLVID_DEBUGSS_CT_TBR,
	SCI_SLVID_ALL
};

/* Statistic Collector slave name table.
 * In sci_slave_addr_table order.
 */
const char * sci_slave_name_table[] = {
    "HOST_CLK1",
    "DMM1",
    "DMM2",
    "ABE",
    "L4_CFG",
    "HOST_CLK2",
    "GPMC",
    "OCM_RAM",
    "DSS",
    "ISS",
    "IPU",
    "GPU",
    "IVAHD",
    "SL2",
    "L4_PER_P0",
    "L4_PER_P1",
    "L4_PER_P2",
    "L4_PER_P3",
    "C2C",
    "LLI",
    "CAL",
    "HOST_CLK3",
    "L3_INSTR",
    "DEBUGSS_CT_TBR"
    "ALL"
};
#endif
