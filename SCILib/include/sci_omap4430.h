/*
 * sci_omap4430.h
 *
 * Statistic Collector Instrumentation Library 
 * - OMAP5430 Statistic Collector public definitions
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef SCI_OMAP4430_H
#define SCI_OMAP4430_H

enum sci_probeid_sdram {
    SCI_EMIF1,                  /*!< EMIF1 */
    SCI_EMIF2,                  /*!< EMIF2 */
    SCI_EMIF_MODEM              /*!< EMIF MODEM */
};

/*! \par probeid_mstr Probe selection for master statistic collectors. */
/* Note: Order for probe_name_table indexing - not probe ids.
 *  For probe_ids a probe_map table is used (see sci_dev.h).
 */

enum sci_probeid_mstr {
    SCI_A9_MPU,
    SCI_DSP,
    SCI_SDMA_RD,
    SCI_SDMA_WR,
    SCI_DSS,
    SCI_ISS,
    SCI_IVAHD,
    SCI_SGX,
    SCI_M3_MPU
};

/*! \par sci_master_addr Master address enumerations. */
enum sci_master_addr {
    SCI_MSTID_MPU = 0x0,
    SCI_MSTID_DAP = 0x10,
    SCI_MSTID_IEEE1500 = 0x14,
    SCI_MSTID_DSP = 0x20,
    SCI_MSTID_IVAHD = 0x30,
    SCI_MSTID_ISS = 0x40,
    SCI_MSTID_M3_MPU = 0x44,
    SCI_MSTID_FACE_DETECT_SMP = 0x48,
    SCI_MSTID_SDMA_RD = 0x50,
    SCI_MSTID_SDMA_WR = 0x54,
    SCI_MSTID_CRYPTO_DMA_RD = 0x58,
    SCI_MSTID_CRYPTO_DMA_WR = 0x5C,
    SCI_MSTID_SGX = 0x60,
    SCI_MSTID_DSS = 0x70,
    SCI_MSTID_SAD2D = 0x80,
    SCI_MSTID_HSI = 0x90,
    SCI_MSTID_MMC1 = 0xA0,
    SCI_MSTID_MMC2 = 0xA4,
    SCI_MSTID_MMC6_MID = 0xA8,
    SCI_MSTID_UNIPRO1 = 0xB0,
    SCI_MSTID_USB_HOST_HS_SMP = 0xC0,
    SCI_MSTID_USB_OTG_HS = 0xC4,
    SCI_MSTID_USB_HOST_FS_SMP = 0xC8,
    SCI_MASTID_ALL
};

/*! \par sci_slave_addr Slave address enumerations. */
enum sci_slave_addr {
    SCI_SLVID_HOST_CLK1 = 0x0,
    SCI_SLVID_DMM1 = 0x1,
    SCI_SLVID_DMM2 = 0x2,
    SCI_SLVID_ABE = 0x3,
    SCI_SLVID_L4CFG = 0x4,
    SCI_SLVID_HOST_CLK2 = 0x6,
    SCI_SLVID_GPMC = 0x7,
    SCI_SLVID_OCMRAM = 0x8,
    SCI_SLVID_DSS = 0x9,
    SCI_SLVID_ISS = 0xA,
    SCI_SLVID_M3_MPU = 0xB,
    SCI_SLVID_SGX = 0xC,
    SCI_SLVID_IVAHD = 0xD,
    SCI_SLVID_SL2 = 0xE,
    SCI_SLVID_L4PER0 = 0xF,
    SCI_SLVID_L4PER1 = 0x10,
    SCI_SLVID_L4PER2 = 0x11,
    SCI_SLVID_L4PER3 = 0x12,
    SCI_SLVID_AES1 = 0x13,
    SCI_SLVID_AES2 = 0x14,
    SCI_SLVID_SHA1 = 0x15,
    SCI_SLVID_MODEM_MID = 0x16,
    SCI_SLVID_HOST_CLK3 = 0x18,
    SCI_SLVID_DEBUGSS = 0x19,
    SCI_SLVID_ALL
};
#endif
