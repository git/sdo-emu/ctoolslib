/*
 * sci_am437x.h
 *
 * Statistic Collector Instrumentation Library 
 * - Statistic Collector module specific definitions
 * - Device specific configurations
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef SCI_AM437X_H
#define SCI_AM437X_H

/*! \file sci_am437x.h
    AM437x device specific CPTLib definitions
*/

/*! \enum sci_probeid_sdram
    Probe selection for sdram statistic collectors.
    Note: In the case of AM437x there are no sdram statistic collectors.
*/
enum sci_probeid_sdram { 
	SCI_NOT_IMPLEMENTED				/*!< Not used for am437x devices but the structure must exist. */
};

/*! \enum sci_probeid_mstr
     Probe selection for master statistic collectors.
     Note: In the case of AM437x not all probes in this list are masters (EMIF).
*/
enum sci_probeid_mstr { 
     		SCI_MPU_128, 			/*!< MPU 128 */
 	 		SCI_MPU_64,				/*!< MPU 64 */
 	 		SCI_GFX,				/*!< GFX */
 	 		SCI_DSS,				/*!< DSS */
 	 		SCI_EMIF,				/*!< EMIF */
 	 		SCI_EDMA3TC0_RD,		/*!< EDMA3 TC0 RD */
 	 		SCI_EDMA3TC1_RD,		/*!< EDMA3 TC1 RD */
 	 		SCI_EDMA3TC2_RD,		/*!< EDMA3 TC2 RD */
 	 		SCI_EDMA3TC0_WR,		/*!< EDMA3 TC0 WR */
 	 		SCI_EDMA3TC1_WR,		/*!< EDMA3 TC1 WR */
 	 		SCI_EDMA3TC2_WR,		/*!< EDMA3 TC2 WR */
 	 		SCI_PRU_ICSS1,			/*!< PRU ICSS1 */
 	 		SCI_CRYPTODMA_WR,		/*!< CRYPTO DMA WR */
 	 		SCI_CPSW,				/*!< CPSW */
 	 		SCI_PRU_ICSS0,			/*!< PRU ICSS0 */
 	 		SCI_CRYPTODMA_RD,		/*!< CRYPTO DMA RD */
 	 		SCI_0_USB0,				/*!< USB0 0 */
 	 		SCI_1_USB0,				/*!< USB0 1 */
 	 		SCI_GPMC,				/*!< GPMC */
 	 		SCI_MMCSD2,				/*!< MMCSD2 */
 	 		SCI_MCASP0_DATA,		/*!< MCASP0 DATA */
 	 		SCI_MCASP1_DATA,		/*!< MCASP1 DATA */
 	 		SCI_0_USB1,				/*!< USB1 0 */
 	 		SCI_1_USB1				/*!< USB1 1 */
 	};

/*! \enum sci_master_addr
    Master address enumerations.
*/
enum sci_master_addr { 
     		SCI_MSTID_MPU_128=0x00,			/*!< MPU 128 */
 	 		SCI_MSTID_MPU_64=0x01,			/*!< MPU 64 */
 	 		SCI_MSTID_DAP=0x04,				/*!< DAP */
 	 		SCI_MSTID_IEEE1500=0x05,		/*!< IEEE1500 */
 	 		SCI_MSTID_PRU_ICSS0=0x0C,		/*!< PRU ICSS0 */
 	 		SCI_MSTID_PRU_ICSS1=0x0D,		/*!< PRU ICSS1 */
 	 		SCI_MSTID_EDMA3TC0_RD=0x18, 	/*!< EDMA3 TC0 RD */
 	 		SCI_MSTID_EDMA3TC0_WR=0x19, 	/*!< EDMA3 TC0 WR */
 	 		SCI_MSTID_EDMA3TC1_RD=0x1A, 	/*!< EDMA3 TC1 RD */
 	 		SCI_MSTID_EDMA3TC1_WR=0x1B, 	/*!< EDMA3 TC1 WR */
 	 		SCI_MSTID_EDMA3TC2_RD=0x1C, 	/*!< EDMA3 TC2 RD */
 	 		SCI_MSTID_EDMA3TC2_WR=0x1D, 	/*!< EDMA3 TC2 WR */
 	 		SCI_MSTID_GFX=0x20,				/*!< GFX */
 	 		SCI_MSTID_DSS=0x25,				/*!< DSS */
 	 		SCI_MSTID_CRYPTODMA_RD=0x28,	/*!< CRYPTO DMA RD */
 	 		SCI_MSTID_CRYPTODMA_WR=0x29,	/*!< CRYPTO DMA WR */
 	 		SCI_MSTID_VPFE0=0x2C,			/*!< VPFE0 */
 	 		SCI_MSTID_VPFE1=0x2D,			/*!< VPFE1 */
 	 		SCI_MSTID_CPSW=0x30,			/*!< CPSW */
 	 		SCI_MSTID_USB0=0x34,			/*!< USB0 */
 	 		SCI_MSTID_USB1=0x36,			/*!< USB1 */
 	 		SCI_MSTID_STATCOL0=0x3C,		/*!< STATCOL0 */
 	 		SCI_MSTID_STATCOL1=0x3D,		/*!< STATCOL1 */
 	 		SCI_MSTID_STATCOL2=0x3E,		/*!< STATCOL2 */
 	 		SCI_MSTID_STATCOL3=0x3F,		/*!< STATCOL3 */
 	 		SCI_MASTID_ALL					/*!< Select all masters */
};

/*! \enum sci_slave_addr
     Slave address enumerations.
*/
enum sci_slave_addr { 
     		SCI_SLVID_EMIF=0x1,				/*!< EMIF */
 	 		SCI_SLVID_OCMCRAM=0x10,			/*!< OCMCRAM */
 	 		SCI_SLVID_EDMA3TC0_CFG=0x7,		/*!< EDMA3 TC0 CFG */
 	 		SCI_SLVID_EDMA3TC1_CFG=0x8,		/*!< EDMA3 TC1 CFG */
 	 		SCI_SLVID_EDMA3TC2_CFG=0x9,		/*!< EDMA3 TC2 CFG */
 	 		SCI_SLVID_EDMA3CC=0xB,			/*!< EDMA3CC */
 	 		SCI_SLVID_DEBUGSS=0x1F,			/*!< DEBUGSS */
 	 		SCI_SLVID_ADC1=0xF,				/*!< ADC1 */
 	 		SCI_SLVID_L4_PER_0=0x11,		/*!< L4 PER 0 */
 	 		SCI_SLVID_L4_PER_1=0x12,		/*!< L4 PER 1 */
 	 		SCI_SLVID_L4_PER_2=0x13,		/*!< L4 PER 2 */
 	 		SCI_SLVID_L4_PER_3=0x14,		/*!< L4 PER 3 */
 	 		SCI_SLVID_MCASP0_DATA=0x20,		/*!< MCASP0 DATA */
 	 		SCI_SLVID_MCASP1_DATA=0x21,		/*!< MCASP1 DATA */
 	 		SCI_SLVID_GPMC=0x1E,			/*!< GPMC */
 	 		SCI_SLVID_L4_FW=0x1B,			/*!< L4 FW */
 	 		SCI_SLVID_L3F_CFG=0x0,			/*!< L3F CFG */
 	 		SCI_SLVID_L3S_CFG=0xC,			/*!< L3S CFG */
 	 		SCI_SLVID_AES=0x4,				/*!< AES */
 	 		SCI_SLVID_SHA=0x2,				/*!< SHA */
 	 		SCI_SLVID_MMCSD2=0x26,			/*!< MMCSD2 */
 	 		SCI_SLVID_L4_WKUP=0xD,			/*!< L4 WKUP */
 	 		SCI_SLVID_GFX=0xE,				/*!< GFX */
 	 		SCI_SLVID_ADC0=0xA,				/*!< ADC0 */
 	 		SCI_SLVID_L4_FAST=0x5,			/*!< L4 FAST */
 	 		SCI_SLVID_QSPI=0x1C,			/*!< QSPI */
 	 		SCI_SLVID_MPU_L2_CACHE=0x3,		/*!< MPU L2 CACHE */
 	 		SCI_SLVID_DES=0x15,				/*!< DES */
 	 		SCI_SLVID_PRU_ICSS1=0x1A,		/*!< ICSS1 */
 	 		SCI_SLVID_ALL					/*!< Select all slaves */
};

#endif
