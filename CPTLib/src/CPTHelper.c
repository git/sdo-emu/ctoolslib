/*
 * CPTHelper.c
 *
 * cTools Helper public functions implementation. 
 *
 * Copyright (C) 2010,2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "CPTLib.h"
#include "CPTHelper.h"

#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID,
                                CPT_Qualifiers const * const pCPT_TPCntQual, CPT_TrigQualifiers * pCPT_TrigQual)
#else
eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID,
                                CPT_Qualifiers const * const pCPT_TPCntQual)
#endif
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
#ifndef RUNTIME_DEVICE_SELECT
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
#endif
    error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID, eCPT_Throughput_Cnt0, eCPT_Mstr_Enable_Grp);
    if ( eCPT_Success == error ) 
    {  
        error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID, eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    if ( eCPT_Success == error )
    { 
        error = CPT_ModuleEnable (pCPT_Handle, eCPT_MsgSelect_Statistics, eCPT_CntSelect_WaitAndGrant );
    }
    return error;

}

#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_TotalProfileMult( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID[],
                                    int CPT_MasterIDCnt, CPT_Qualifiers const * const pCPT_TPCntQual, CPT_TrigQualifiers * pCPT_TrigQual)
#else
eCPT_Error CPTCfg_TotalProfileMult( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[], 
                                    int CPT_MasterIDCnt, CPT_Qualifiers const * const pCPT_TPCntQual)
#endif
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
#ifndef RUNTIME_DEVICE_SELECT
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
#endif
    {
        int i;
        for ( i = 0; i < CPT_MasterIDCnt; i++ )
        {
            error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[i], eCPT_Throughput_Cnt0, eCPT_Mstr_Enable_Grp);
            if ( eCPT_Success != error )
            {
                break;
            }
        }
    }

    if ( eCPT_Success == error ) 
    {  
        error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[0], eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    if ( eCPT_Success == error )
    { 
        error = CPT_ModuleEnable (pCPT_Handle, eCPT_MsgSelect_Statistics, eCPT_CntSelect_WaitAndGrant );
    }
    return error;

}
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle,
                                 const int CPT_MasterID0, CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                 const int CPT_MasterID1, CPT_Qualifiers const * const pCPT_TPCnt1Qual,
                                 CPT_TrigQualifiers * pCPT_TrigQual)
#else
eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle, 
                                 const eCPT_MasterID CPT_MasterID0, CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                 const eCPT_MasterID CPT_MasterID1, CPT_Qualifiers const * const pCPT_TPCnt1Qual)
#endif
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
#ifndef RUNTIME_DEVICE_SELECT
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
#endif
    
    error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID0, eCPT_Throughput_Cnt0, eCPT_Mstr_Enable_Grp);
    if ( eCPT_Success == error ) 
    {  
        error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID1, eCPT_Throughput_Cnt1, eCPT_Mstr_Enable_Grp);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCnt0Qual, pCPT_TPCnt1Qual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    if ( eCPT_Success == error )
    { 
        error = CPT_ModuleEnable (pCPT_Handle, eCPT_MsgSelect_Statistics, eCPT_CntSelect_WaitAndGrant );
    }
    return error;
}

#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID[], int CPT_MasterIDCnt,
                          const eCPT_MsgSelects CPT_MsgEnables, CPT_Qualifiers const * const pCPT_TPEventQual,
                          CPT_TrigQualifiers * pCPT_TrigQual)
#else
eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[], int CPT_MasterIDCnt, 
                          const eCPT_MsgSelects CPT_MsgEnables, CPT_Qualifiers const * const pCPT_TPEventQual)
#endif
{    
    eCPT_Error error = eCPT_Success;
    CPT_Qualifiers * pCPT_TPCntQual = NULL;
#ifndef RUNTIME_DEVICE_SELECT
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;    
#endif
    
    if (    ( ( eCPT_MsgSelect_Event_NewReq & CPT_MsgEnables ) != eCPT_MsgSelect_Event_NewReq )
         && ( ( eCPT_MsgSelect_Event_LastWrt & CPT_MsgEnables ) != eCPT_MsgSelect_Event_LastWrt )
         && ( ( eCPT_MsgSelect_Event_LastRd & CPT_MsgEnables ) != eCPT_MsgSelect_Event_LastRd ) )
    {
        error = eCPT_Error_Invalid_Parameter; 
    }
    
    if ( eCPT_Success == error )
    {
    	int i;

    	if (CPT_MasterIDCnt == 0)
    	{
    		error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[0], eCPT_Throughput_Cnt0, eCPT_Mstr_EnableAll);
    	}

        for ( i = 0; i < CPT_MasterIDCnt; i++ )
        {
            error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID[i], eCPT_Throughput_Cnt0, eCPT_Mstr_Enable_Grp);
            if ( eCPT_Success != error )
            {
                break;
            }
        }
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    if ( eCPT_Success == error )
    { 
        error = CPT_ModuleEnable (pCPT_Handle, CPT_MsgEnables, eCPT_CntSelect_None );
    }
    return error;
}            

#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle,
                                 CPT_Qualifiers const * const pCPT_TPCntQual,
                                 CPT_TrigQualifiers * pCPT_TrigQual)
#else
eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle, 
                                 CPT_Qualifiers const * const pCPT_TPCntQual)
#endif
{
    eCPT_Error error;
    CPT_Qualifiers * pCPT_TPEventQual = NULL;
#ifndef RUNTIME_DEVICE_SELECT
    CPT_TrigQualifiers * pCPT_TrigQual = NULL;
#endif

    //Note that CPT_MasterID value is ignored because eCPT_Mstr_DisableALL overrides
#ifdef RUNTIME_DEVICE_SELECT
    int CPT_MasterID = 0;
#else
    eCPT_MasterID CPT_MasterID = eCPT_MID_GEM0;
#endif
    error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID, eCPT_Throughput_Cnt0, eCPT_Mstr_DisableALL);
    if ( eCPT_Success == error ) 
    {  
        //Note that eCPT_MID_GEM0 is ignored because eCPT_Mstr_EnableAll overrides
        error = CPT_CfgMaster( pCPT_Handle, CPT_MasterID, eCPT_Throughput_Cnt1, eCPT_Mstr_EnableAll);
    }
    if ( eCPT_Success == error )
    {
        error = CPT_CfgQualifiers( pCPT_Handle,  pCPT_TPCntQual, pCPT_TPCntQual, pCPT_TPEventQual, pCPT_TrigQual);
    }
    
    return error;

}

