/*
 * CPTLib.c
 *
 * Common Platform (CP) Tracer Library Implementation
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include "CPTLib.h"
#include "CPT_Common.h"
#ifdef _STM_Logging
#include "StmSupport.h"
#endif
#include <stdio.h>
#include <string.h>
#ifdef _TMS320C6X /*Always defined by the compiler*/
#include <c6x.h>
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public external function prototypes
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _DOXYGEN_IGNORE
extern void * cTools_memAlloc(size_t sizeInBytes);
extern void   cTools_memFree(void * ptr);
extern void * cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes);
extern void   cTools_memUnMap(void * vAddr, unsigned int mapSizeInBytes);
extern int cTools_mutexInit(uint32_t mutexId);
extern int cTools_mutexTrylock(uint32_t mutexId);
extern int cTools_mutexUnlock(uint32_t mutexId);
#ifdef RUNTIME_DEVICE_SELECT
extern uint32_t cTools_reg_rd32(uint32_t addr);
extern void cTools_reg_wr32(uint32_t addr, uint32_t value);
#endif
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private structs
//
////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum {eCPT_ModDisabled, eCPT_ModEnabled } eModState; 

struct _CPT_Handle {
#ifndef RUNTIME_DEVICE_SELECT
    eCPT_ModID CPT_ModId;
#else
    int CPT_ModId;
#endif
    eModState ModuleState;
    uint32_t * BaseAddr;
    CPT_CallBack pCPT_CallBack;
    uint32_t SampleWindowSize;
    uint32_t AddrExportMask;
#ifdef _STM_Logging
    bool STM_LogMsgEnable;
    STMHandle *  pSTMHandle;
    int32_t      STMMessageCh;
#endif
#if defined(_STM_Logging) || defined(RUNTIME_DEVICE_SELECT)
    char * pCPT_MetaData;
    uint32_t CPT_MetaDataByteCnt; 
#endif
#ifdef RUNTIME_DEVICE_SELECT
    uint32_t ArmingAddr;
    uint32_t ArmingValue;
    uint32_t DisarmingValue;
#endif
};

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Static constants
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _STM_Logging
static const char CPT_LibClassStr[] = "CPTLib message";
static const char CPT_UsrClassStr[] = "Traget function: CPT_LogMsg()";
static const char CPT_ModuleStr[]= "CPT";
static const char CPT_DataTypeStr_Msg[] = "Message";

static const char CPT_Log_Msg1[] = "Statistic counters msg gen enabled";
static const char CPT_Log_Msg2[] = "New Request event msg gen enabled";
static const char CPT_Log_Msg3[] = "Last Write event msg gen enabled";
static const char CPT_Log_Msg4[] = "Last Read event msg gen enabled";
static const char CPT_Log_Msg5[] = "Access Status msg gen enabled";

static const char * CPT_Log_MsgEnables[] = { CPT_Log_Msg1,
                                             CPT_Log_Msg2,
                                             CPT_Log_Msg3,
                                             CPT_Log_Msg4,
                                             CPT_Log_Msg5
                                            };
static const char CPT_Log_CntSelects1[] = "Wait counter enabled";
static const char CPT_Log_CntSelects2[] = "Grant counter enabled";

static const char * CPT_Log_CntSelects[] = { CPT_Log_CntSelects1,
                                             CPT_Log_CntSelects2
                                            };

static const char CPT_Log_MsgDisables[] = "Msg generation disabled";

#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Library support functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
inline eCPT_Error CPT_utlTestPwr() 
{
#ifndef RUNTIME_DEVICE_SELECT
    if  (  ( 0 == ( CPT_PDSTAT_ON & reg_rd32(PSC_PDSTAT(CPT_POWER_DOMAIN)) ) )
        || ( CPT_MDSTAT_ENABLED != ( CPT_MDSTAT_ENABLED & reg_rd32(PSC_MDSTAT(CPT_LPCS_NUM))) ) )
    {
        return eCPT_Error_PowerOrModuleDisabled; 
    }
#endif
    return eCPT_Success;
       
}
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Library functions
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef RUNTIME_DEVICE_SELECT
static int CPT_ModName_max;
static int CPT_MasterIdTable_max;
const char ** pCPT_ModNames;
const char ** pCPT_MasterNames;
const uint32_t * CPT_BaseAddressTable;
const uint8_t * CPT_ModDivByFactors;
const CPT_MultiMID_t * CPT_MultiMID;
static int CPT_MultiMIDTable_max;
const struct _CPT_MasterIdTable * pCPT_MasterIDTable;

eCPT_Error CPT_runtimeConfig(eCPT_DeviceID CPT_deviceID)
{
    switch (CPT_deviceID) {
    case DEVID_TCI6612:
    	CPT_ModName_max = eCPT_TCI6612_ModID_Last;
    	CPT_MasterIdTable_max = sizeof(CPT_TCI6612_MasterIDTable)/sizeof(struct _CPT_MasterIdTable);
    	pCPT_ModNames = pCPT_TCI6612_ModNames;
    	CPT_BaseAddressTable = CPT_TCI6612_BaseAddressTable;
    	CPT_MultiMID = CPT_TCI6612_MultiMID;
    	CPT_MultiMIDTable_max =  sizeof(CPT_TCI6612_MultiMID)/sizeof(CPT_MultiMID_t);
    	pCPT_MasterIDTable = CPT_TCI6612_MasterIDTable;
    	pCPT_MasterNames = CPT_TCI6612_MasterNames;
    	CPT_ModDivByFactors = CPT_TCI6612_ModDivByFactors;
    	break;
    case DEVID_TCI6614:
    	CPT_ModName_max = eCPT_TCI6614_ModID_Last;
    	CPT_MasterIdTable_max = sizeof(CPT_TCI6614_MasterIDTable)/sizeof(struct _CPT_MasterIdTable);
    	pCPT_ModNames = pCPT_TCI6614_ModNames;
    	CPT_BaseAddressTable = CPT_TCI6614_BaseAddressTable;
    	CPT_MultiMID = CPT_TCI6614_MultiMID;
    	CPT_MultiMIDTable_max =  sizeof(CPT_TCI6614_MultiMID)/sizeof(CPT_MultiMID_t);
    	pCPT_MasterIDTable = CPT_TCI6614_MasterIDTable;
    	pCPT_MasterNames = CPT_TCI6614_MasterNames;
    	CPT_ModDivByFactors = CPT_TCI6614_ModDivByFactors;
    	break;
    default:
    	return eCPT_Error_Invalid_Parameter;
    }

    return eCPT_Success;
}

eCPT_Error CPT_getModID(char * modName, int * CPT_ModId)
{
	int i = 0;
	for (i = 0; i < CPT_ModName_max; i++)
	{
		if (!strcmp(pCPT_ModNames[i],  modName)) {
			*CPT_ModId = i;
			break;
		}
	}

	if (i == CPT_ModName_max)
	{
		return eCPT_Error_Invalid_Parameter;
	}

	return eCPT_Success;
}

eCPT_Error CPT_getDivideByFactor(int CPT_ModId, uint8_t * CPT_DivideByFactor)
{

	if ((CPT_ModId < 0) || ( CPT_ModId >= CPT_ModName_max )) {
		return eCPT_Error_Invalid_Parameter;
	}

	*CPT_DivideByFactor = CPT_ModDivByFactors[CPT_ModId];

	return eCPT_Success;
}

eCPT_Error CPT_getModList(const char *** CPT_modList, int * CPT_listCnt)
{
	 *CPT_modList = pCPT_ModNames;
	 *CPT_listCnt = CPT_ModName_max;
	 return eCPT_Success;
}

eCPT_Error CPT_getMasterList(const char *** CPT_masterList, int * CPT_listCnt)
{
	 *CPT_masterList = pCPT_MasterNames;
	 *CPT_listCnt = CPT_MasterIdTable_max;
	 return eCPT_Success;
}


eCPT_Error CPT_getMasterID(char * masterName, int * CPT_MasterId, int * CPT_MasterGroupCnt)
{
	int i = 0;
	for (i = 0; i < CPT_MasterIdTable_max; i++)
	{
		if (!strcmp(pCPT_MasterNames[i],  masterName)) {
			* CPT_MasterId = pCPT_MasterIDTable[i].master_id;
			if (CPT_MasterGroupCnt != NULL)
			{
				* CPT_MasterGroupCnt = pCPT_MasterIDTable[i].group_cnt;
			}
			break;
		}
	}

	if (i == CPT_MasterIdTable_max)
	{
		return eCPT_Error_Invalid_Parameter;
	}

	return eCPT_Success;
}

eCPT_Error CPT_getArmingInfo(CPT_Handle_Pntr const pCPT_Handle, struct CPT_ArmingInfo * pCPT_ArmingInfo )
{

	pCPT_ArmingInfo->arming_addr = pCPT_Handle->ArmingAddr;
	pCPT_ArmingInfo->arming_value = pCPT_Handle->ArmingValue;
	pCPT_ArmingInfo->disarm_value = pCPT_Handle->DisarmingValue;

	return eCPT_Success;
}


eCPT_Error CPT_getMetaDataSize(CPT_Handle_Pntr const pCPT_Handle, int * num_bytes)
{
	* num_bytes = pCPT_Handle->CPT_MetaDataByteCnt;

	return eCPT_Success;
}


eCPT_Error CPT_getMetaData(CPT_Handle_Pntr const pCPT_Handle, char * buf, int buf_size, int * num_bytes_returned)
{
	if (( (int)pCPT_Handle->CPT_MetaDataByteCnt > buf_size ) || ( buf == NULL ))
	{
		return eCPT_Error_Invalid_Parameter;
	}

	memcpy( buf, pCPT_Handle->pCPT_MetaData, pCPT_Handle->CPT_MetaDataByteCnt );

	* num_bytes_returned = pCPT_Handle->CPT_MetaDataByteCnt;

	return eCPT_Success;
}
#endif


#ifndef RUNTIME_DEVICE_SELECT
eCPT_Error CPT_OpenModule(CPT_Handle_Pntr * const pCPT_Handle, const eCPT_ModID CPT_ModId,  
                                                               CPT_CfgOptions const * const pCPT_CfgOptions )
#else
eCPT_Error CPT_OpenModule(CPT_Handle_Pntr * const pCPT_Handle, int CPT_ModId,
                                                               CPT_CfgOptions const * const pCPT_CfgOptions )
#endif
{
    CPT_Handle_t * pCPT_hdl;
    uint32_t * CPT_vBaseAddr;           //Mapped Base Address
    uint32_t HwFuncID;                  //Physical module's func ID - used to test software compatability with the specific module
    
    //If pCPT_Handle is not NULL then return error
    if ( NULL != * pCPT_Handle ) 
    {
        return eCPT_Error_InvalidHandlePointer;
    }

#ifdef RUNTIME_DEVICE_SELECT
    if ((CPT_ModId >= CPT_ModName_max) || (CPT_ModId < 0))
	{
    	return eCPT_Error_Invalid_Parameter;
	}
#endif
    //If config parameters are provided test they are valid
    if ( NULL != pCPT_CfgOptions )
    {
        //All usecases ecept the raw case must have a non-zero SampleWindowSize
        if (   ( 0 == pCPT_CfgOptions->SampleWindowSize )  
            && ( eCPT_UseCase_Raw != pCPT_CfgOptions->CPT_UseCaseId ))
        {
            return eCPT_Error_Invalid_Parameter; 
        } 
    }
 
    //Attempt to open and acquire the lock for this CP Tracer
    if ( cTools_mutexInit(CPT_MUTEXID(CPT_ModId)) 
    ||  cTools_mutexTrylock(CPT_MUTEXID(CPT_ModId)) ) 
    {
        return eCPT_Error_Busy;
    }

    //Attempt to map the CP Tracer base address
    CPT_vBaseAddr = (uint32_t *)cTools_memMap(CPT_BaseAddressTable[CPT_ModId], CPT_REGSPACE_BYTES);
    if (NULL == CPT_vBaseAddr)
    {
        cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
        return eCPT_Error_MappingError;
    }

#ifndef RUNTIME_DEVICE_SELECT
    //Before we touch a CP Tracer we have to test that the DebugSS+CP_Tracer domain is 
    //powered up and enabled.
    {

        // If the power domain is not already on, turn it on
        if ( 0 == ( CPT_PDSTAT_ON & reg_rd32(PSC_PDSTAT(CPT_POWER_DOMAIN)) ) )
        {
            int retry = CPT_TIMEOUT_CNT;
            
            //Check for power transition. Don't know if transition on or off.
            while ( CPT_POWER_TRANSITION == (reg_rd32(PSC_PTSTAT) & CPT_POWER_TRANSITION) )
            {
                if ( --retry == 0 ) break;
            }
            if ( 0 == retry )
            {
                cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
                return eCPT_Error_DomainPowerUp;
            }

            //Enable the domain
            reg_wr32(PSC_PDCTL( CPT_POWER_DOMAIN ), 0x1);    //Set the power domain's NEXT state to ON.
            reg_wr32(PSC_PTCMD, 1<< CPT_POWER_DOMAIN);         //Issue GO for the CP Tracer power domain
                                                             //to modify the domain's power state
            //Reset the timeout count
            retry = CPT_TIMEOUT_CNT;
            
            //Wait for the domain transition to complete
            while( CPT_POWER_TRANSITION == (reg_rd32(PSC_PTSTAT) & CPT_POWER_TRANSITION ) )
            {
                if ( --retry == 0 ) break;
            }  
            if ( 0 == retry )
            {
                cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
                return eCPT_Error_DomainPowerUp;
            }
        }
        
        // If the module is not enabled, then enable it
        if ( CPT_MDSTAT_ENABLED != ( CPT_MDSTAT_ENABLED & reg_rd32(PSC_MDSTAT(CPT_LPCS_NUM))) )
        {
            int retry = CPT_TIMEOUT_CNT;
            reg_wr32(PSC_MDCTL(CPT_LPCS_NUM), (reg_rd32(PSC_MDCTL(CPT_LPCS_NUM)) & 0xFFFFFFE0) | 0x0003);  //Set the LPCS Module NEXT state to ON.
            reg_wr32(PSC_PTCMD, (0x1<<CPT_POWER_DOMAIN));                               // Start transition
            
            // Wait for transition to complete
            while( ((reg_rd32(PSC_PTSTAT) & (0x1<<CPT_POWER_DOMAIN) ) != 0 ) && ( 0 < retry-- ) );
            
            // Wait for transition to complete     
            while( ((reg_rd32(PSC_MDSTAT(CPT_LPCS_NUM)) & 0x1F)!= 0x3) && ( 0 < retry-- ) );
            if ( 0 >= retry )
            {
                cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
                return eCPT_Error_DomainEnable;
            }
        } 
        
    }
#endif
    //Test module version is compatible with the library
    {
        reg32_t id_reg = reg_rd32(CPT_ID_REG(CPT_vBaseAddr));
        if (   ( CPTLIB_FUNC != CPT_GET_IDREG_FUNC(id_reg) 
            || ( CPT_FUNC_ID_SCHEME != CPT_GET_IDREG_IDSCH(id_reg) ) ) )
        {
            cTools_memUnMap(CPT_vBaseAddr, CPT_REGSPACE_BYTES);
            cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
            return eCPT_Error_Not_Compatible;
        }
        // Store the module's func id 
        HwFuncID = CPT_GET_IDREG_FUNC(id_reg);
        
    }
    
    //Test for module ownership
    //Notes: The CP Tracer modules do not have an ownership mechanism
    // that allows protected access by either the IDE (through the JTAG port) or
    // from application code (this library). By convention the IDE must read the
    // CP Tracer Ownership bit, if clear it will then  confirm it won the ownership
    // race (with the library) by reading the Destination register. If the Destination
    // register is zero then the IDE can safely write the Ownership bit and use the unit. 
    //
    // The Combination of the Ownership bit and the Destination register defines
    // ownership:
    //
    //      Ownership 0 - Library owns (reset value 0)
    //                1 - IDE owns
    //      Destination non-zero - owned by Library or IDE (reset value 0) 
    //
    // Note: The library uses the DBGM bit to disable IDE accesses while ownership is
    // determined by the library. If the CP tracer Ownership bit is clear the library
    // will write the Destination register while the DBGM bit is set. Thus if there is
    // a race between the Library and IDE both writing the CP Tracer Destination register,
    // the Library will win the race. To minimize the amount of time DBGM is set for,
    // this process is atomic. If implemented the mutex provides ownership protection
    // from other processes running on this processor and from other processors.
    {
        reg32_t cpt_owner;
        uint32_t restore_value;
        bool forceOwnership = false;

        if ( NULL != pCPT_CfgOptions )
            forceOwnership = pCPT_CfgOptions->ForceOwnership;

#ifdef _TMS320C6X /*Always defined by the compiler*/
        // disable interrupts
        restore_value = _disable_interrupts();

        //Set DBGM bit to hold off accesses from the JTAG port (generated by CCS).
        //Note: _set() is a compiler intrinsic function. TSR is defined in c6x.h
        TSR = _set(TSR,4,4); // set DBGM bit (bit 4 of TSR)
#endif
        //Read the CPT Ownership bit and test it. If set then debugger owns
        //the resource, if not set and destination address is set then another
        //core or thread on this core owns. In the latter case ownership may be forced.
        cpt_owner = CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(CPT_vBaseAddr)));
        if (   ( cpt_owner )
        	|| ( ( 0 == cpt_owner) && (0 != reg_rd32(CPT_DSTADDR(CPT_vBaseAddr)))  && !forceOwnership))
        {
#ifdef _TMS320C6X /*Always defined by the compiler*/
            TSR = _clr(TSR,4,4);    // clear DBGM bit (bit 4 of TSR)
            _restore_interrupts(restore_value);
#endif
            cTools_memUnMap(CPT_vBaseAddr, CPT_REGSPACE_BYTES);
            cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
            return eCPT_Error_Busy; 
        }

        //Leave the Ownership bit clear (Library owns) 
        //and set the Destination register.
        reg_wr32(CPT_DSTADDR(CPT_vBaseAddr), STM_Ch0MsgEnd);
        
        //Clear DBGM bit to allow accesses through the JTAG port
#ifdef _TMS320C6X /*Always defined by the compiler*/
        TSR = _clr(TSR,4,4);    // clear DBGM bit (bit 4 of TSR)
        _restore_interrupts(restore_value);
#endif

    }

    //Malloc handle
    {
        pCPT_hdl = (CPT_Handle_t *)cTools_memAlloc(sizeof(CPT_Handle_t));
        if ( NULL == pCPT_hdl ) {

#ifdef _TMS320C6X /*Always defined by the compiler*/
//            reg32_t cpt_owner;
            uint32_t restore_value;
            
            //Clear the ownership
            restore_value = _disable_interrupts();
            TSR = _set(TSR,4,4); // set DBGM bit (bit 4 of TSR)
#endif
            reg_wr32(CPT_DSTADDR(CPT_vBaseAddr), 0);

#ifdef _TMS320C6X /*Always defined by the compiler*/
            TSR = _clr(TSR,4,4);    // clear DBGM bit (bit 4 of TSR)
            _restore_interrupts(restore_value);
#endif
            
            cTools_memUnMap(CPT_vBaseAddr, CPT_REGSPACE_BYTES);
            cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
            return eCPT_Error_Memory_Allocation;
        } 
        * pCPT_Handle = (CPT_Handle_Pntr)pCPT_hdl;
    }


    //Fill in handle
    pCPT_hdl->CPT_ModId = CPT_ModId;
    pCPT_hdl->BaseAddr = CPT_vBaseAddr;
    pCPT_hdl->ModuleState = eCPT_ModDisabled;
    if ( NULL != pCPT_CfgOptions )
    {
        pCPT_hdl->pCPT_CallBack = pCPT_CfgOptions->pCPT_CallBack;
        pCPT_hdl->SampleWindowSize = pCPT_CfgOptions->SampleWindowSize;
        pCPT_hdl->AddrExportMask = pCPT_CfgOptions->AddrExportMask;
#ifdef _STM_Logging
        pCPT_hdl->STM_LogMsgEnable = pCPT_CfgOptions->STM_LogMsgEnable;
        pCPT_hdl->pSTMHandle =  pCPT_CfgOptions->pSTMHandle;
        pCPT_hdl->STMMessageCh =  pCPT_CfgOptions->STMMessageCh;
#endif 
    }
    else
    {
        pCPT_hdl->pCPT_CallBack = NULL;
        pCPT_hdl->SampleWindowSize = 65536;
        pCPT_hdl->AddrExportMask = 0;
#ifdef _STM_Logging
        pCPT_hdl->pSTMHandle =  NULL;
        pCPT_hdl->STMMessageCh =  0;
#endif         
    } 
        
    //Set CP tracer register defaults
    reg_wr32(CPT_STWINDOW_REG(CPT_vBaseAddr), 0); //Disable sample window counter, this also disables message generation
    
    reg_wr32(CPT_ADDRMASK(CPT_vBaseAddr), CPT_SET_ADDRESSMASK(pCPT_hdl->AddrExportMask));
    
    reg_wr32(CPT_ENDADDR(CPT_vBaseAddr), 0); //Disable address comparator
    
    reg_wr32(CPT_MSGPRI(CPT_vBaseAddr), CPT_SET_MSGPRI_STAT(eCPT_MsgPri_7)
                                	  | CPT_SET_MSGPRI_EVTE(eCPT_MsgPri_7)
                                      | CPT_SET_MSGPRI_EVTC(eCPT_MsgPri_7)
                                      | CPT_SET_MSGPRI_EVTB(eCPT_MsgPri_7)
                                      | CPT_SET_MSGPRI_ACC(eCPT_MsgPri_7));
     
    reg_wr32(CPT_QUAL_REG(CPT_vBaseAddr), CPT_SET_QUAL_ALLDISABLED);
     
    reg_wr32(CPT_MODCTL_REG(CPT_vBaseAddr), CPT_SET_MODCTL_ALLDISABLED);
    
    reg_wr32(CPT_INTCLR(CPT_vBaseAddr), CPT_SET_MASKINTDISABLE);
    reg_wr32(CPT_INTSTATUS(CPT_vBaseAddr), CPT_CLR_MASKINTSTATE);
    
    reg_wr32(CPT_MSTSELA_TP0_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELB_TP0_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELC_TP0_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELD_TP0_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELA_TP1_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELB_TP1_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELC_TP1_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    reg_wr32(CPT_MSTSELD_TP1_REG(CPT_vBaseAddr), CPT_SET_MASTER_SEL_NONE);
    
#if defined(_STM_Logging) || defined(RUNTIME_DEVICE_SELECT)
    //malloc space
    {
        
        int32_t chrCnt = 0;
        reg32_t modCtl_reg = reg_rd32(CPT_MODCTL_REG(CPT_vBaseAddr));
        int32_t sid = CPT_GET_MODCTL_SID(modCtl_reg);
        uint32_t libVer = ( CPTLIB_MAJOR_VERSION << 16 ) | CPTLIB_MINOR_VERSION;
        
        //Malloc enough space for the attribute string
        pCPT_hdl->pCPT_MetaData = (char *)cTools_memAlloc(CPT_ATTR_BUFSIZE);
        if ( NULL == pCPT_hdl->pCPT_MetaData )
        {
            cTools_memUnMap(CPT_vBaseAddr, CPT_REGSPACE_BYTES);
            cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
            cTools_memFree(pCPT_hdl);
            return eCPT_Error_Memory_Allocation;    
        }
        
        chrCnt += snprintf ( pCPT_hdl->pCPT_MetaData+chrCnt, CPT_ATTR_BUFSIZE, "{type=CPT,jobid=0,sid=%d,name=%s,mode=%d,sw=%d,cr=%d,df=%d,do=%d, mv=%d, lv=0x%x",
                                                                                sid,
                                                                                pCPT_ModNames[CPT_ModId],
                                                                                pCPT_CfgOptions->CPT_UseCaseId,
                                                                                pCPT_hdl->SampleWindowSize,
                                                                                pCPT_CfgOptions->CPUClockRateMhz,
                                                                                CPT_ModDivByFactors[CPT_ModId],
                                                                                pCPT_CfgOptions->DataOptions,
                                                                                HwFuncID,
                                                                                libVer );
        
        pCPT_hdl->CPT_MetaDataByteCnt = chrCnt;
        
        if ( CPT_ATTR_BUFSIZE < chrCnt )
        {
            cTools_memUnMap(CPT_vBaseAddr, CPT_REGSPACE_BYTES);
            cTools_mutexUnlock(CPT_MUTEXID(CPT_ModId));
            cTools_memFree(pCPT_hdl->pCPT_MetaData);
            cTools_memFree(pCPT_hdl);            
            return eCPT_Error_STM;
        }
               
    }
#endif    
    
    return eCPT_Success;
}

eCPT_Error CPT_CloseModule(CPT_Handle_Pntr * const pCPT_Handle )
{
    eCPT_Error retVal_CPT = eCPT_Success;
    CPT_Handle_t * pCPT_hdl = (CPT_Handle_t *)* pCPT_Handle;
    CPT_CallBack callback = pCPT_hdl->pCPT_CallBack;
    
    
    // Check for an invalid handle
    if ( NULL == pCPT_hdl )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {

        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_hdl->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }

        // If the CP tracer is enabled, disable it.
        if ( eCPT_ModDisabled != pCPT_hdl->ModuleState ) 
        {
            retVal_CPT = CPT_ModuleDisable ( (CPT_Handle_Pntr)pCPT_hdl, eCPT_WaitDisable );
        }
        
        // Give up ownership of the module
        reg_wr32(CPT_DSTADDR(pCPT_hdl->BaseAddr), 0);
        
        
        //Unmap the CP Tracer Base Address
        cTools_memUnMap(pCPT_hdl->BaseAddr, CPT_REGSPACE_BYTES);
        //Unlock the CP Tracer's Mutex
        cTools_mutexUnlock(CPT_MUTEXID(pCPT_hdl->CPT_ModId));
#ifdef _STM_Logging
        if ( NULL != pCPT_hdl->pCPT_MetaData )
        {
            cTools_memFree(pCPT_hdl->pCPT_MetaData);
        }
#endif
        cTools_memFree(pCPT_hdl);
        
        // Set the user's handle to NULL
        * pCPT_Handle = NULL;
        

    }
     
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_hdl->pCPT_CallBack ) )
    { 
        callback(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

eCPT_Error CPT_GetVersion(CPT_Handle_Pntr const pCPT_Handle, uint32_t * const pLibMajorVersion, 
                                                             uint32_t * const pLibMinorVersion,
                                                             uint32_t * const pSWFuncID, 
                                                             uint32_t * const pHwFuncID )
{       
    eCPT_Error retVal_CPT = eCPT_Success;
    
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else
    {
        * pLibMajorVersion = CPTLIB_MAJOR_VERSION;
        * pLibMinorVersion = CPTLIB_MINOR_VERSION;
        * pSWFuncID = CPTLIB_FUNC;
        
        // Check the power and module enable
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {        
            reg32_t id_reg = reg_rd32(CPT_ID_REG(pCPT_Handle->BaseAddr));
        
            * pHwFuncID = CPT_GET_IDREG_FUNC(id_reg);
            
        }
    }
     
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;               
}

eCPT_Error  CPT_ModuleEnable (CPT_Handle_Pntr const pCPT_Handle, const eCPT_MsgSelects CPT_MsgEnables, 
                                                                 const eCPT_CntSelects CPT_CntEnables )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }
        
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {
            // Check the power and module enable
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {
            	reg32_t modCtl_reg;

#ifdef _STM_Logging
                // If _STM_Logging enabled for the build then send user
                // logging messages and meta data
//                if ( NULL != pCPT_Handle->pSTMHandle )
                {
                    eSTM_STATUS retVal_STM;
                    
                    if (( true == pCPT_Handle->STM_LogMsgEnable ) && ( pCPT_Handle->pSTMHandle != NULL ))
                    {
                        int i;
                        //Log message enables
                        for ( i = 0; i < eCPT_MsgSelect_Num; i++) 
                        {
                            if ( (1 << i) & CPT_MsgEnables )
                            {
                                retVal_STM = STMExport_IntMsg( pCPT_Handle->pSTMHandle,
                                              pCPT_Handle->STMMessageCh, 
                                              CPT_ModuleStr,
                                              pCPT_ModNames[pCPT_Handle->CPT_ModId],
                                              CPT_LibClassStr,
                                              CPT_DataTypeStr_Msg,
                                              CPT_Log_MsgEnables[i],
                                              NULL);
                                      
                                if ( eSTM_SUCCESS != retVal_STM )
                                {
                                    retVal_CPT = eCPT_Error_STM;
                                }
                            }  
                        }
                        
                        //Log counters enabled
                        for ( i = 0; i < eCPT_CntSelect_Num; i++) 
                        {
                            if ( (1 << i) & CPT_CntEnables )
                            {
                                retVal_STM = STMExport_IntMsg( pCPT_Handle->pSTMHandle,
                                              pCPT_Handle->STMMessageCh, 
                                              CPT_ModuleStr,
                                              pCPT_ModNames[pCPT_Handle->CPT_ModId],
                                              CPT_LibClassStr,
                                              CPT_DataTypeStr_Msg,
                                              CPT_Log_CntSelects[i],
                                              NULL);
                                      
                                if ( eSTM_SUCCESS != retVal_STM )
                                {
                                    retVal_CPT = eCPT_Error_STM;
                                }
                            }  
                        }
                    }                                                               
                }
#endif
#if defined(_STM_Logging) || defined(RUNTIME_DEVICE_SELECT)
                {

                    //Add master ID selection registers to meta data
                    {
                        int i, j;
                        for ( i = 0; i < 2; i++ )
                        {
                            for ( j = 0; j < 4; j++ )
                            {
                                reg32_t mstSelReg = reg_rd32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, j + (i*4)));
                                size_t msgbufsize = CPT_ATTR_BUFSIZE - pCPT_Handle->CPT_MetaDataByteCnt;
                                pCPT_Handle->CPT_MetaDataByteCnt += snprintf ( pCPT_Handle->pCPT_MetaData + pCPT_Handle->CPT_MetaDataByteCnt, msgbufsize, ",m_%d%d=0x%x", i,j,mstSelReg);
                                if (  CPT_ATTR_BUFSIZE < pCPT_Handle->CPT_MetaDataByteCnt) break;
                            
                            }
                        }
                        //Remember that sprintf does not include the null termination in the string byte count
                        if (  CPT_ATTR_BUFSIZE >= pCPT_Handle->CPT_MetaDataByteCnt+2)
                        {
                            // Wite the end of meta data marker over the sprintf's '\0' character
                            *(pCPT_Handle->pCPT_MetaData + pCPT_Handle->CPT_MetaDataByteCnt) = '}';
                            pCPT_Handle->CPT_MetaDataByteCnt++;
                            *(pCPT_Handle->pCPT_MetaData + pCPT_Handle->CPT_MetaDataByteCnt) = '\0';
                            pCPT_Handle->CPT_MetaDataByteCnt++;
#ifdef _STM_Logging
                            if ( pCPT_Handle->pSTMHandle != NULL )
                            {
                                eSTM_STATUS retVal_STM;
                                //Sendout the meta data
                                retVal_STM = STMExport_putMeta(pCPT_Handle->pSTMHandle,
                                                               pCPT_Handle->pCPT_MetaData,
                                                               pCPT_Handle->CPT_MetaDataByteCnt);
                                if ( eSTM_SUCCESS != retVal_STM )
                                {
                                    retVal_CPT = eCPT_Error_STM;
                                }
                            }
#endif
                        }
                        else
                        {
                           //Exceeded the buffer size - error out
                           retVal_CPT = eCPT_Error_STM; 
                        }
                     
                    }

                }                          
#endif                
                modCtl_reg = reg_rd32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr));

                //Clear the message select and counter enable fields
                modCtl_reg &= ~CPT_MODCTL_EXPSEL_MASK;
                modCtl_reg &= ~CPT_MODCTL_WAITCNTEN_MASK;
                modCtl_reg &= ~CPT_MODCTL_GRANTCNTEN_MASK;

#ifndef RUNTIME_DEVICE_SELECT
                //Set the new message select and counter enable state
                reg_wr32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr), modCtl_reg | CPT_SET_MODCTL_EXPSEL(CPT_MsgEnables) | CPT_SET_MODCTL_CNTEN(CPT_CntEnables));

                //Load the sliding time window (sample) counter
                reg_wr32(CPT_STWINDOW_REG(pCPT_Handle->BaseAddr), pCPT_Handle->SampleWindowSize);
#else
                if(CPT_MsgEnables & eCPT_MsgSelect_Statistics)
                {
                	/* When the usecas uses statistic counters, arm export using the sample window */
                    reg_wr32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr), modCtl_reg | CPT_SET_MODCTL_EXPSEL(CPT_MsgEnables) | CPT_SET_MODCTL_CNTEN(CPT_CntEnables));

                    pCPT_Handle->ArmingAddr =  (uint32_t)CPT_STWINDOW_REG(pCPT_Handle->BaseAddr);
                    pCPT_Handle->ArmingValue = pCPT_Handle->SampleWindowSize;
                    pCPT_Handle->DisarmingValue = 0;
                } else {
                    /* When the usecas does not use statistic counters (event loggign case), arm export using the module control register */
                    pCPT_Handle->ArmingAddr = (uint32_t)CPT_MODCTL_REG(pCPT_Handle->BaseAddr);
                    pCPT_Handle->ArmingValue =  modCtl_reg | CPT_SET_MODCTL_EXPSEL(CPT_MsgEnables) | CPT_SET_MODCTL_CNTEN(CPT_CntEnables);
                    pCPT_Handle->DisarmingValue = 0;
                }
                
#endif
                //Mark the unit enabled
                pCPT_Handle->ModuleState = eCPT_ModEnabled;  
            }          
            
        }
        else
        {
            retVal_CPT = eCPT_Error_Module_Enabled;
        }
        
    }
          
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}                                                               
                                                                 
eCPT_Error  CPT_ModuleDisable (CPT_Handle_Pntr const pCPT_Handle, eCPT_WaitSelect CPT_WaitSelect )
{
    
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
 
    //If ownership has changed to Debugger(IDE) exit with busy error
    if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
    {
        return eCPT_Error_Busy; 
    } 
 
    if ( eCPT_ModEnabled == pCPT_Handle->ModuleState )
    {
        // Check the power and module enable
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
            if ( eCPT_WaitEnable == CPT_WaitSelect ) 
            {
                //Wait for the next timing window to expire
                int retry = 0;
                reg_wr32(CPT_INTSTATUS(pCPT_Handle->BaseAddr), CPT_CLR_MASKINTSTATE);
                while ( !reg_rd32(CPT_INTRAW(pCPT_Handle->BaseAddr)) )
                {
                    if ( retry++ > (int)(pCPT_Handle->SampleWindowSize*CPT_ModDivByFactors[pCPT_Handle->CPT_ModId] ) )
                    {
                         break;
                    }
                }
            }          
            //Disable the CP Tracer Module
            reg_wr32(CPT_STWINDOW_REG(pCPT_Handle->BaseAddr), 0);
            
            //Mark the unit disabled
            pCPT_Handle->ModuleState = eCPT_ModDisabled;
        }
    }
   
#ifdef _STM_Logging
    if ( NULL != pCPT_Handle->pSTMHandle )
    {                
        eSTM_STATUS retVal_STM;
        
        if ( true == pCPT_Handle->STM_LogMsgEnable )
        {
            
            retVal_STM = STMExport_IntMsg( pCPT_Handle->pSTMHandle,
                          pCPT_Handle->STMMessageCh, 
                          CPT_ModuleStr,
                          pCPT_ModNames[pCPT_Handle->CPT_ModId],
                          CPT_LibClassStr,
                          CPT_DataTypeStr_Msg,
                          CPT_Log_MsgDisables,
                          NULL);
                              
            if ( 0 != retVal_STM )
            {
                retVal_CPT = eCPT_Error_STM;
            }
        }

		retVal_STM = STMExport_putMeta(pCPT_Handle->pSTMHandle,
                                       pCPT_Handle->pCPT_MetaData,
                                       pCPT_Handle->CPT_MetaDataByteCnt);
		if ( 0 != retVal_STM )
		{
			retVal_CPT = eCPT_Error_STM;
		}

  
    }
                                            
#endif                
        
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;   
}

#ifndef RUNTIME_DEVICE_SELECT
eCPT_Error CPT_CfgMaster( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterId, 
                                                             const eCPT_ThroughputCntID CPT_ThroughputCnt, 
                                                             const eCPT_MasterState CPT_MasterState )
#else
eCPT_Error CPT_CfgMaster( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterId,
                                                             const eCPT_ThroughputCntID CPT_ThroughputCnt,
                                                             const eCPT_MasterState CPT_MasterState )
#endif
{
    eCPT_Error retVal_CPT = eCPT_Success;

#ifndef RUNTIME_DEVICE_SELECT
	//Return error, if the input master ID is out of range
	if(CPT_MasterId >= eCPT_MID_Cnt)
	{
		return eCPT_Error_Invalid_Parameter;
	}
#endif

    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {

            	//Search the CPT_MultiMID[] array to determine how many bits must be set
                int i;
                int numElem = sizeof CPT_MultiMID / sizeof CPT_MultiMID[0];
                int numMstIds = 1;

                //Master ID can be a 8 bit value, however CP Tracers and STM modules can only support 7 bit master IDs
                //Hence, mask the MSB of the master id. Master ID = 128 will map to 0, 129 will map to 1,... 255 will map to 127
                int regIndex = (CPT_MasterId & 0x7F) >> 5;
                int mstIDBitIndex = CPT_MasterId & 0x1F;
                uint32_t mstGrpMask = 0;

#ifdef RUNTIME_DEVICE_SELECT
            	numElem = CPT_MultiMIDTable_max;
#endif
                
                // If disable or enable group figure out the number of master ids in the group. 
                // If the id provided is not a starting group id then the number of master ids
                // in the group is 1. 
                if (  ( eCPT_Mstr_Disable_Grp == CPT_MasterState )
                    ||( eCPT_Mstr_Enable_Grp == CPT_MasterState ) )
                {
                    for ( i=0; i < numElem; i++ ) 
                    {
                        if ( CPT_MasterId == CPT_MultiMID[i].Base_MID )
                        {
                            numMstIds = CPT_MultiMID[i].NumMasterIDs;
                        }    
                    }
                }
                
                //Figure out the master id bit mask
                for ( i = 0; i <  numMstIds; i++ )
                {  
                    mstGrpMask |= 1 << ( mstIDBitIndex + i );
                }
                
                //Set the registers
                switch (CPT_MasterState)
                {
                    case eCPT_Mstr_Disable_Grp:
                    case eCPT_Mstr_Disable:
                    {
                    	reg32_t mstrSelectReg = reg_rd32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, ( regIndex + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt ))));
                        reg_wr32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, ( regIndex + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt ))), mstrSelectReg & ~mstGrpMask);
                        break;
                    }
                    case eCPT_Mstr_Enable_Grp:
                    case eCPT_Mstr_Enable:
                    {
                    	reg32_t mstrSelectReg = reg_rd32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, ( regIndex + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt ))));
                        reg_wr32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, ( regIndex + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt ))), mstrSelectReg | mstGrpMask);
                        break;
                    }
                    case eCPT_Mstr_DisableALL:
                        for ( i = 0; i < CPT_NUM_MASTER_SEL_REGS; i++ ) 
                        {
                        	reg_wr32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, ( i + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt ))), 0);
                        }
                        break;
                    case eCPT_Mstr_EnableAll:
                        for ( i = 0; i < CPT_NUM_MASTER_SEL_REGS; i++ ) 
                        {
                             uint32_t index = i + ( CPT_NUM_MASTER_SEL_REGS * CPT_ThroughputCnt );
                             reg_wr32(CPT_MSTSELA_TP0_REGIndex(pCPT_Handle->BaseAddr, index ), (uint32_t)-1);
                        }
                        break;
                
                }
   
            }
        }
        else
        {
           retVal_CPT = eCPT_Error_Module_Enabled; 
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

eCPT_Error CPT_CfgQualifiers( CPT_Handle_Pntr const pCPT_Handle,  CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                                                  CPT_Qualifiers const * const pCPT_TPCnt1Qual, 
                                                                  CPT_Qualifiers const * const pCPT_TPEventQual,
                                                                  CPT_TrigQualifiers const * const pCPT_TrigQual )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }
                
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {
                //Setting all Transaction Qualifier register bits 
                //so don't have to worry about initial register state.         
         
                uint32_t qualifier_reg = reg_rd32(CPT_QUAL_REG(pCPT_Handle->BaseAddr));
                
                reg32_t trigQuals = 0;
                reg32_t evtBQual = 0;
                reg32_t tpCnt1Qual = 0;
                reg32_t tpCnt0Qual = 0;
                 
                trigQuals = (NULL != pCPT_TrigQual) ?  CPT_SET_QUAL_TRIGOUT(pCPT_TrigQual->TrigOutEnables)
                                                     | CPT_SET_QUAL_TRIGIN(pCPT_TrigQual->TrigInEnable)
                                                     | CPT_SET_QUAL_TRIGOUTDTYPE(pCPT_TrigQual->NewReqSrcQual)
                                                     | CPT_SET_QUAL_TRIGOUTRW(pCPT_TrigQual->RWQual) 
                                                    :  CPT_TRIG_QUAL_MASK & qualifier_reg;
                
                evtBQual = ( NULL != pCPT_TPEventQual ) ?  CPT_SET_QUAL_EVTBDTYPE(pCPT_TPEventQual->NewReqSrcQual)
                                                         | CPT_SET_QUAL_EVTBRW(pCPT_TPEventQual->RWQual)
                                                        :  CPT_EVTB_QUAL_MASK & qualifier_reg;

                tpCnt1Qual = ( NULL != pCPT_TPCnt1Qual ) ?  CPT_SET_QUAL_TH1DTYPE(pCPT_TPCnt1Qual->NewReqSrcQual)
                                                          | CPT_SET_QUAL_TH1RW(pCPT_TPCnt1Qual->RWQual)
                                                         :  CPT_TH1_QUAL_MASK & qualifier_reg;             
                tpCnt0Qual = ( NULL != pCPT_TPCnt0Qual ) ?  CPT_SET_QUAL_TH0DTYPE(pCPT_TPCnt0Qual->NewReqSrcQual)
                                                          | CPT_SET_QUAL_TH0RW(pCPT_TPCnt0Qual->RWQual)
                                                         : CPT_TH0_QUAL_MASK & qualifier_reg;
                
                reg_wr32(CPT_QUAL_REG(pCPT_Handle->BaseAddr), trigQuals | evtBQual| tpCnt1Qual | tpCnt0Qual);
            } 
        }
        else
        {
           retVal_CPT = eCPT_Error_Module_Enabled; 
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}                                                                 

eCPT_Error CPT_CfgAddrFilter( CPT_Handle_Pntr const pCPT_Handle, const uint32_t AddrFilterMSBs, 
                                                                 const uint32_t StartAddrFilterLSBs, 
                                                                 const uint32_t EndAddrFilterLSBs,
                                                                 const eCPT_FilterMode CPT_FilterMode )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
     else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {

            //Can only set Address MSBs that are valid for this CP Tracer
            reg32_t modCtl_reg = reg_rd32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr));
            
            switch CPT_GET_MODCTL_ADDRMODE(modCtl_reg)
            {
                case eCPT_AddrMode_32bits:
                    if ( CPT_MODCTL_ADDRMODE_32_TEST(AddrFilterMSBs) ) 
                    {
                        retVal_CPT = eCPT_Error_Invalid_Parameter;
                    }
                    break;
                case eCPT_AddrMode_36bits:
                    if ( 0 != CPT_MODCTL_ADDRMODE_36_TEST(AddrFilterMSBs) )
                    {
                         retVal_CPT = eCPT_Error_Invalid_Parameter;
                    }
                    break;
                case eCPT_AddrMode_40bits:
                    if ( 0 != CPT_MODCTL_ADDRMODE_40_TEST(AddrFilterMSBs) )
                    {
                         retVal_CPT = eCPT_Error_Invalid_Parameter;
                    }
                    break;                    
                case eCPT_AddrMode_44bits:
                    if ( 0 != CPT_MODCTL_ADDRMODE_44_TEST(AddrFilterMSBs) )
                    {
                         retVal_CPT = eCPT_Error_Invalid_Parameter;
                    }
                    break;
                case eCPT_AddrMode_48bits:
                    if ( 0 != CPT_MODCTL_ADDRMODE_48_TEST(AddrFilterMSBs) )
                    {
                         retVal_CPT = eCPT_Error_Invalid_Parameter;
                    }
                    break;
            }
            
            if ( eCPT_Success == retVal_CPT )
            { 
                if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
                {
                    //Clear the upper address bits and the filter mode
                    reg32_t modCtl_reg = reg_rd32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr));
            
                    //Clear the upper address bits and the filter mode fields
                    modCtl_reg &= ~CPT_MODCTL_UPPERADDRBITS_MASK;
                    modCtl_reg &= ~CPT_MODCTL_ADDRFLTEN_MASK;
                    
                    //Set the upper address bits and the filter mode
                    reg_wr32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr), modCtl_reg
                                                        | CPT_SET_MODCTL_UPPERADDRBITS(AddrFilterMSBs) 
                                                        | CPT_SET_MODCTL_ADDRFLTEN(CPT_FilterMode));
                    
                    //Set the starting and ending addres filters
                    reg_wr32(CPT_STARTADDR(pCPT_Handle->BaseAddr), StartAddrFilterLSBs);
                    reg_wr32(CPT_ENDADDR(pCPT_Handle->BaseAddr), EndAddrFilterLSBs);
                }
            }
        }
        else
        {
           retVal_CPT = eCPT_Error_Module_Enabled; 
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}                                                                

eCPT_Error CPT_CfgPacing ( CPT_Handle_Pntr const pCPT_Handle, const uint32_t PacingCntValue )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {         
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {
                reg_wr32(CPT_PACING(pCPT_Handle->BaseAddr), CPT_SET_PACING(PacingCntValue));
            }  
        }
        else
        {
           retVal_CPT = eCPT_Error_Module_Enabled; 
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

eCPT_Error CPT_CfgMsgPriority ( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MsgPri CPT_StatisticMsgsPri,
                                                                   const eCPT_MsgPri CPT_NewRequestEventMsgPri,
                                                                   const eCPT_MsgPri CPT_LastWriteEventMsgPri,
                                                                   const eCPT_MsgPri CPT_LastReadEventMsgPri,
                                                                   const eCPT_MsgPri CPT_AccessStatusMsgPri )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_ModDisabled == pCPT_Handle->ModuleState )
        {         
            
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {
                reg_wr32(CPT_MSGPRI(pCPT_Handle->BaseAddr), CPT_SET_MSGPRI_STAT(CPT_StatisticMsgsPri)
                                                 | CPT_SET_MSGPRI_EVTB(CPT_NewRequestEventMsgPri)
                                                 | CPT_SET_MSGPRI_EVTC(CPT_LastWriteEventMsgPri)
                                                 | CPT_SET_MSGPRI_EVTE(CPT_LastReadEventMsgPri)
                                                 | CPT_SET_MSGPRI_ACC(CPT_AccessStatusMsgPri));
            } 
        }
        else
        {
           retVal_CPT = eCPT_Error_Module_Enabled; 
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}
                                                                   
eCPT_Error CPT_GetAddrMode( CPT_Handle_Pntr const pCPT_Handle,  eCPT_AddrMode * const pAddrModeRange )
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
            reg32_t modCtl_reg = reg_rd32(CPT_MODCTL_REG(pCPT_Handle->BaseAddr));
            * pAddrModeRange = (eCPT_AddrMode)CPT_GET_MODCTL_ADDRMODE(modCtl_reg);
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

#ifndef RUNTIME_DEVICE_SELECT
/* Note that  RUNTIME_DEVICE_SELECT is normally done from a remote
 *  host where sampling registers atomically (required by this function)
 *  can't be done.
 */
eCPT_Error CPT_GetCurrentState ( CPT_Handle_Pntr const pCPT_Handle, uint32_t * const pCPT_ThroughPutCnt0,
                                                                   uint32_t * const pCPT_ThroughPutCnt1,
                                                                   uint32_t * const pCPT_WaitCnt,
                                                                   uint32_t * const pCPT_GrantCnt, 
                                                                   uint32_t * const pAccessStatus )
{   
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    { 
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
#ifdef _TMS320C6X /*Always defined by the compiler*/
            unsigned int restore_value;         
            restore_value = _disable_interrupts();
#endif
            * pCPT_ThroughPutCnt0 = CPT_GET_THROUGHPUTCNT0(reg_rd32(CPT_TP0(pCPT_Handle->BaseAddr)));
            * pCPT_ThroughPutCnt1 = CPT_GET_THROUGHPUTCNT0(reg_rd32(CPT_TP1(pCPT_Handle->BaseAddr)));
            * pCPT_WaitCnt = CPT_GET_WAITTIMECNT(reg_rd32(CPT_ACCUMWAIT(pCPT_Handle->BaseAddr)));
            * pCPT_GrantCnt = CPT_GET_GRANTCNT(reg_rd32(CPT_GRANTCNT(pCPT_Handle->BaseAddr)));
            * pAccessStatus = reg_rd32(CPT_ACCSTATUS(pCPT_Handle->BaseAddr));
#ifdef _TMS320C6X /*Always defined by the compiler*/
            _restore_interrupts(restore_value);
#endif
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}
#endif

eCPT_Error CPT_CfgInt  ( CPT_Handle_Pntr const pCPT_Handle, const eCPT_IntMask CPT_IntMask)
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        return eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }
                
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
            switch (CPT_IntMask)
            {
                case eCPT_IntMask_Enable:
                     reg_wr32(CPT_INTSET(pCPT_Handle->BaseAddr), CPT_SET_MASKINTENABLE);
                    break;
                case eCPT_IntMask_Disable:
                     reg_wr32(CPT_INTCLR(pCPT_Handle->BaseAddr), CPT_SET_MASKINTDISABLE);
                    break;
            }
        }
        
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

eCPT_Error CPT_GetIntStatus ( CPT_Handle_Pntr const pCPT_Handle, eCPT_IntStatus * const pCPT_IntStatus)
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        retVal_CPT = eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }        
        
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
            * pCPT_IntStatus = (eCPT_IntStatus)CPT_GET_MASKINTSTATE(reg_rd32(CPT_INTSTATUS(pCPT_Handle->BaseAddr)));
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

eCPT_Error CPT_ClrIntStatus ( CPT_Handle_Pntr const pCPT_Handle )
{
     eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        retVal_CPT = eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
        //If ownership has changed to Debugger(IDE) exit with busy error
        if ( CPT_GET_OWNERSHIP(reg_rd32(CPT_OWNERSHIP(pCPT_Handle->BaseAddr))) )
        {
            return eCPT_Error_Busy; 
        }            
            
        if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
        {
            reg_wr32(CPT_INTSTATUS(pCPT_Handle->BaseAddr), CPT_CLR_MASKINTSTATE);
        }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
}

#ifdef _STM_Logging
eCPT_Error  CPT_LogMsg(CPT_Handle_Pntr const pCPT_Handle, const char * FmtString, uint32_t * const pValue)
{
    eCPT_Error retVal_CPT = eCPT_Success;
        
    // Check for an invalid handle
    if ( NULL == pCPT_Handle )
    {
        retVal_CPT = eCPT_Error_InvalidHandlePointer;
    }
    else 
    {
            if ( eCPT_Success == ( retVal_CPT = CPT_utlTestPwr() ) )
            {
                if ( NULL == pCPT_Handle->pSTMHandle )
                {
                    retVal_CPT = eCPT_Error_NULL_STMHandle;
                }
                else
                {
                    eSTM_STATUS retVal_STM;
                    retVal_STM = STMExport_IntMsg( pCPT_Handle->pSTMHandle,
                                  pCPT_Handle->STMMessageCh, 
                                  CPT_ModuleStr,
                                  pCPT_ModNames[pCPT_Handle->CPT_ModId],
                                  CPT_UsrClassStr,
                                  CPT_DataTypeStr_Msg,
                                  FmtString,
                                  pValue);
                                  
                    if ( eSTM_SUCCESS != retVal_STM )
                    {
                        retVal_CPT += eCPT_Error_STM;
                    }
                }
            }
    }
    
    if ( ( eCPT_Success != retVal_CPT ) && ( NULL != pCPT_Handle->pCPT_CallBack ) )
    { 
        pCPT_Handle->pCPT_CallBack(__FUNCTION__,retVal_CPT);
    }
        
    return retVal_CPT;
} 

#endif
