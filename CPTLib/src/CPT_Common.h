#ifndef __CPT_COMMON_H
#define __CPT_COMMON_H
/*
 * CPT_Common.h
 *
 * Common Platform (CP) Tracer Library Common definitions
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names ofTC0
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*! \file CPT_Common.h
       Common CP Tracer Library typedefs and definitions
*/
#ifdef __cplusplus
extern "C" {
#endif

#ifndef _DOXYGEN_IGNORE

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Typedefs
//
////////////////////////////////////////////////////////////////////////////////////////////////

// CP Tracer register typedef
typedef volatile uint32_t reg32_t;

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Private Definitions
//
////////////////////////////////////////////////////////////////////////////////////////////////    

// Mutex ID definitions

#define CPT_MUTEXID_MAJOR 1
#define CPT_MUTEXID_MINOR_MASK 0xFFFF
#define CPT_MUTEXID(CPT_ModId) (CPT_MUTEXID_MAJOR | (CPT_MUTEXID_MINOR_MASK & CPT_ModId))

//////////////////////////////////////////////////////////////////////////////////////////
// Private device specific data
//////////////////////////////////////////////////////////////////////////////////////////

typedef struct _eCPT_MultiMID {
#ifndef RUNTIME_DEVICE_SELECT
	eCPT_MasterID Base_MID;
#else
    int Base_MID;
#endif
	int8_t    NumMasterIDs;             // Always 32 or less
}CPT_MultiMID_t;

// Meta data Attribute definitions

#define  CPT_ATTR_BUFSIZE  256

#if defined(_C6670)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_L2_2[] = "L2_2";
static const char CPT_ModName_L2_3[] = "L2_3";
static const char CPT_ModName_RAC[] = "RAC";
static const char CPT_ModName_RAC_CFG[] = "RAC_CFG";
static const char CPT_ModName_TAC[] = "TAC";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_L2_1,
									CPT_ModName_L2_2,
									CPT_ModName_L2_3,
									CPT_ModName_RAC,
									CPT_ModName_RAC_CFG,
									CPT_ModName_TAC
								};

#endif //_STM_Logging


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d58000, // eCPT_L2_2
											0x01d60000, // eCPT_L2_3
											0x01d68000, // eCPT_RAC
											0x01d70000, // eCPT_RAC_CFG
											0x01d78000  // eCPT_TAC
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_L2_2
											3, // eCPT_L2_3
											3, // eCPT_RAC
											3, // eCPT_RAC_CFG
											3  // eCPT_TAC
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
										2,
										eCPT_MID_QM_SS_Grp0,
										4,
										eCPT_MID_AIF_Grp0,
										8,
										eCPT_MID_QM_CDMA_Grp0,
										4,
										eCPT_MID_NETCP_Grp0,
										2
									};
#endif
#if defined(_C6671)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";


//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG,
									CPT_ModName_CFG,
									CPT_ModName_L2_0
								};

#endif


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000 // eCPT_L2_0
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3 // eCPT_L2_0
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
										2,
										eCPT_MID_QM_CDMA_Grp0,
										4,
										eCPT_MID_QM_second_Grp0,
										2,
										eCPT_MID_NETCP_Grp0,
										4
									};
#endif
#if defined(_C6672)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";


//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_L2_1
								};

#endif


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000 // eCPT_L2_1
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3 // eCPT_L2_1
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
										2,
										eCPT_MID_QM_CDMA_Grp0,
										4,
										eCPT_MID_QM_second_Grp0,
										2,
										eCPT_MID_NETCP_Grp0,
										4
									};
#endif
#if defined(_C6674)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_L2_2[] = "L2_2";
static const char CPT_ModName_L2_3[] = "L2_3";


//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_L2_1,
									CPT_ModName_L2_2,
									CPT_ModName_L2_3
								};

#endif


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d58000, // eCPT_L2_2
											0x01d60000 // eCPT_L2_3
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_L2_2
											3 // eCPT_L2_3
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
										2,
										eCPT_MID_QM_CDMA_Grp0,
										4,
										eCPT_MID_QM_second_Grp0,
										2,
										eCPT_MID_NETCP_Grp0,
										4
									};
#endif
#if defined(_C6678)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_L2_2[] = "L2_2";
static const char CPT_ModName_L2_3[] = "L2_3";
static const char CPT_ModName_L2_4[] = "L2_4";
static const char CPT_ModName_L2_5[] = "L2_5";
static const char CPT_ModName_L2_6[] = "L2_6";
static const char CPT_ModName_L2_7[] = "L2_7";


//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
                                    CPT_ModName_MSMC_1,
                                    CPT_ModName_MSMC_2,
                                    CPT_ModName_MSMC_3,
                                    CPT_ModName_QM_MST,
                                    CPT_ModName_DDR,
                                    CPT_ModName_SM,
                                    CPT_ModName_QM_CFG,
                                    CPT_ModName_CFG,
                                    CPT_ModName_L2_0,
                                    CPT_ModName_L2_1,
                                    CPT_ModName_L2_2,
                                    CPT_ModName_L2_3,
                                    CPT_ModName_L2_4,
                                    CPT_ModName_L2_5,
                                    CPT_ModName_L2_6,
                                    CPT_ModName_L2_7
                                };

#endif


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
                                            0x01d08000, // eCPT_MSMC_1
                                            0x01d10000, // eCPT_MSMC_2
                                            0x01d18000, // eCPT_MSMC_3
                                            0x01d20000, // eCPT_QM_MST
                                            0x01d28000, // eCPT_DDR
                                            0x01d30000, // eCPT_SM
                                            0x01d38000, // eCPT_QM_PRI
                                            0x01d40000, // eCPT_SCR3_CFG
                                            0x01d48000, // eCPT_L2_0
                                            0x01d50000, // eCPT_L2_1
                                            0x01d58000, // eCPT_L2_2
                                            0x01d60000, // eCPT_L2_3
                                            0x01d68000, // eCPT_L2_4
                                            0x01d70000, // eCPT_L2_5
                                            0x01d78000, // eCPT_L2_6
                                            0x01d80000  // eCPT_L2_7
                                         };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
                                            2, // eCPT_MSMC_1
                                            2, // eCPT_MSMC_2
                                            2, // eCPT_MSMC_3
                                            3, // eCPT_QM_MST
                                            2, // eCPT_DDR
                                            3, // eCPT_SM
                                            3, // eCPT_QM_PRI
                                            3, // eCPT_SCR3_CFG
                                            3, // eCPT_L2_0
                                            3, // eCPT_L2_1
                                            3, // eCPT_L2_2
                                            3, // eCPT_L2_3
                                            3, // eCPT_L2_4
                                            3, // eCPT_L2_5
                                            3, // eCPT_L2_6
                                            3  // eCPT_L2_7
                                         };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
                                        2,
                                        eCPT_MID_QM_CDMA_Grp0,
                                        4,
                                        eCPT_MID_QM_second_Grp0,
                                        2,
                                        eCPT_MID_NETCP_Grp0,
                                        4
                                    };

#endif
#if defined(_C6657)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR[] = "DDR";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG[] = "QM_P"; //for 6670 was called QM_CFG
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_SCR_6P_A[] = "SCR_6P_A";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
                                    CPT_ModName_MSMC_1,
                                    CPT_ModName_MSMC_2,
                                    CPT_ModName_MSMC_3,
                                    CPT_ModName_QM_MST,
                                    CPT_ModName_DDR,
                                    CPT_ModName_SM,
                                    CPT_ModName_QM_CFG,
                                    CPT_ModName_CFG,
                                    CPT_ModName_L2_0,
                                    CPT_ModName_L2_1,
                                    CPT_ModName_SCR_6P_A
                                };

#endif


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
                                            0x01d08000, // eCPT_MSMC_1
                                            0x01d10000, // eCPT_MSMC_2
                                            0x01d18000, // eCPT_MSMC_3
                                            0x01d20000, // eCPT_QM_MST
                                            0x01d28000, // eCPT_DDR
                                            0x01d30000, // eCPT_SM
                                            0x01d38000, // eCPT_QM_PRI
                                            0x01d40000, // eCPT_SCR3_CFG
                                            0x01d48000, // eCPT_L2_0
                                            0x01d50000, // eCPT_L2_1
                                            0x01d58000  //eCPT_SCR_6P_A
                                         };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     2, // eCPT_MSMC_0
                                            2, // eCPT_MSMC_1
                                            2, // eCPT_MSMC_2
                                            2, // eCPT_MSMC_3
                                            3, // eCPT_QM_MST
                                            2, // eCPT_DDR
                                            3, // eCPT_SM
                                            3, // eCPT_QM_PRI
                                            3, // eCPT_SCR3_CFG
                                            3, // eCPT_L2_0
                                            3, // eCPT_L2_1
                                            3  // eCPT_SCR_6P_A
                                         };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
                                        2,
                                        eCPT_MID_EMAC_0,
                                        4,
                                        eCPT_MID_QM_CDMA_Grp0,
                                        4,
                                        eCPT_MID_QM_second_Grp0,
                                        2
                                    };
#endif
#if defined(_TCI6614)

#if defined(_STM_Logging) || defined(RUNTIME_DEVICE_SELECT)
static const char CPT_TCI6614_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_TCI6614_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_TCI6614_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_TCI6614_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_TCI6614_ModName_QM_MST[] = "QM_M";
static const char CPT_TCI6614_ModName_DDR[] = "DDR";
static const char CPT_TCI6614_ModName_SM[] = "SM";
static const char CPT_TCI6614_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_TCI6614_ModName_CFG[] = "CFG";
static const char CPT_TCI6614_ModName_L2_0[] = "L2_0";
static const char CPT_TCI6614_ModName_L2_1[] = "L2_1";
static const char CPT_TCI6614_ModName_L2_2[] = "L2_2";
static const char CPT_TCI6614_ModName_L2_3[] = "L2_3";
static const char CPT_TCI6614_ModName_RAC[] = "RAC";
static const char CPT_TCI6614_ModName_RAC_CFG[] = "RAC_CFG";
static const char CPT_TCI6614_ModName_TAC[] = "TAC";
static const char CPT_TCI6614_ModName_SCR_6P_A[] = "SCR_6P_A";
static const char CPT_TCI6614_ModName_DDR_2[] = "DDR_2";

//CP Tracer module mame strings indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const char * pCPT_TCI6614_ModNames[] = {
#else
const char * pCPT_ModNames[] = {
#endif
									CPT_TCI6614_ModName_MSMC_0,
									CPT_TCI6614_ModName_MSMC_1,
									CPT_TCI6614_ModName_MSMC_2,
									CPT_TCI6614_ModName_MSMC_3,
									CPT_TCI6614_ModName_QM_MST,
									CPT_TCI6614_ModName_DDR,
									CPT_TCI6614_ModName_SM,
									CPT_TCI6614_ModName_QM_CFG,
									CPT_TCI6614_ModName_CFG,
									CPT_TCI6614_ModName_L2_0,
									CPT_TCI6614_ModName_L2_1,
									CPT_TCI6614_ModName_L2_2,
									CPT_TCI6614_ModName_L2_3,
									CPT_TCI6614_ModName_RAC,
									CPT_TCI6614_ModName_RAC_CFG,
									CPT_TCI6614_ModName_TAC,
									CPT_TCI6614_ModName_SCR_6P_A,
									CPT_TCI6614_ModName_DDR_2
								};

#endif //_STM_Logging


//CP Tracer Address Table indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const uint32_t CPT_TCI6614_BaseAddressTable[] = {
#else
const uint32_t CPT_BaseAddressTable[] = {
#endif
											0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d58000, // eCPT_L2_2
											0x01d60000, // eCPT_L2_3
											0x01d68000, // eCPT_RAC
											0x01d70000, // eCPT_RAC_CFG
											0x01d78000,  // eCPT_TAC
											0x01D80000, //eCPT_SCR_6P_A
											0x01D88000   //eCPT_DDR_2
										 };

//CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const uint8_t CPT_TCI6614_ModDivByFactors[] = {
#else
const uint8_t CPT_ModDivByFactors[] = {
#endif
											2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_L2_2
											3, // eCPT_L2_3
											3, // eCPT_RAC
											3, // eCPT_RAC_CFG
											3,  // eCPT_TAC
										    3,  // eCPT_SCR_6P_A
											2   // eCPT_DDR_2
										 };

#ifdef RUNTIME_DEVICE_SELECT
const CPT_MultiMID_t CPT_TCI6614_MultiMID[] = {
										{eCPT_TCI6614_MID_SRIO_PKTDMA_Grp0, 2},
										{eCPT_TCI6614_MID_QM_SS_Grp0, 4},
										{eCPT_TCI6614_MID_AIF_Grp0, 8},
										{eCPT_TCI6614_MID_QM_CDMA_Grp0, 4},
										{eCPT_TCI6614_MID_NETCP_Grp0, 2},
										{eCPT_TCI6614_MID_ARM_128_Grp0, 32}
									};
#else
const CPT_MultiMID_t CPT_MultiMID[] = {
										{eCPT_MID_SRIO_PKTDMA_Grp0, 2},
										{eCPT_MID_QM_SS_Grp0, 4},
										{eCPT_MID_AIF_Grp0, 8},
										{eCPT_MID_QM_CDMA_Grp0, 4},
										{eCPT_MID_NETCP_Grp0, 2},
										{eCPT_MID_ARM_128_Grp0, 32}
									};

#endif
#endif
#if defined(_TCI6612)

#if defined(_STM_Logging) || defined(RUNTIME_DEVICE_SELECT)
static const char CPT_TCI6612_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_TCI6612_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_TCI6612_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_TCI6612_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_TCI6612_ModName_QM_MST[] = "QM_M";
static const char CPT_TCI6612_ModName_DDR[] = "DDR";
static const char CPT_TCI6612_ModName_SM[] = "SM";
static const char CPT_TCI6612_ModName_QM_CFG[] = "QM_CFG";
static const char CPT_TCI6612_ModName_CFG[] = "CFG";
static const char CPT_TCI6612_ModName_L2_0[] = "L2_0";
static const char CPT_TCI6612_ModName_L2_1[] = "L2_1";
static const char CPT_TCI6612_ModName_RAC[] = "RAC";
static const char CPT_TCI6612_ModName_RAC_CFG[] = "RAC_CFG";
static const char CPT_TCI6612_ModName_TAC[] = "TAC";
static const char CPT_TCI6612_ModName_SCR_6P_A[] = "SCR_6P_A";
static const char CPT_TCI6612_ModName_DDR_2[] = "DDR_2";

//CP Tracer module mame strings indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const char * pCPT_TCI6612_ModNames[] = {
#else
const char * pCPT_ModNames[] = {
#endif
									CPT_TCI6612_ModName_MSMC_0,
									CPT_TCI6612_ModName_MSMC_1,
									CPT_TCI6612_ModName_MSMC_2,
									CPT_TCI6612_ModName_MSMC_3,
									CPT_TCI6612_ModName_QM_MST,
									CPT_TCI6612_ModName_DDR,
									CPT_TCI6612_ModName_SM,
									CPT_TCI6612_ModName_QM_CFG,
									CPT_TCI6612_ModName_CFG,
									CPT_TCI6612_ModName_L2_0,
									CPT_TCI6612_ModName_L2_1,
									CPT_TCI6612_ModName_RAC,
									CPT_TCI6612_ModName_RAC_CFG,
									CPT_TCI6612_ModName_TAC,
									CPT_TCI6612_ModName_SCR_6P_A,
									CPT_TCI6612_ModName_DDR_2
								};

#endif //_STM_Logging


//Nyquist CP Tracer Address Table indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const uint32_t CPT_TCI6612_BaseAddressTable[] = {
#else
const uint32_t CPT_BaseAddressTable[] = {
#endif
											0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d68000, // eCPT_RAC
											0x01d70000, // eCPT_RAC_CFG
											0x01d78000,  // eCPT_TAC
											0x01D80000, //eCPT_SCR_6P_A
											0x01D88000   //eCPT_DDR_2
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
#ifdef RUNTIME_DEVICE_SELECT
const uint8_t CPT_TCI6612_ModDivByFactors[] = {
#else
const uint8_t CPT_ModDivByFactors[] = {
#endif
											2, // eCPT_MSMC_0
											2, // eCPT_MSMC_1
											2, // eCPT_MSMC_2
											2, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											2, // eCPT_DDR
											3, // eCPT_SM
											3, // eCPT_QM_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_RAC
											3, // eCPT_RAC_CFG
											3,  // eCPT_TAC
											3,  // eCPT_SCR_6P_A
											2   // eCPT_DDR_2
										 };

#ifdef RUNTIME_DEVICE_SELECT
const CPT_MultiMID_t CPT_TCI6612_MultiMID[] = {
										{eCPT_TCI6612_MID_SRIO_PKTDMA_Grp0, 2},
										{eCPT_TCI6612_MID_QM_SS_Grp0, 4},
										{eCPT_TCI6612_MID_AIF_Grp0, 8},
										{eCPT_TCI6612_MID_QM_CDMA_Grp0, 4},
										{eCPT_TCI6612_MID_NETCP_Grp0, 2},
										{eCPT_TCI6612_MID_ARM_128_Grp0, 32}
									};
#else
const CPT_MultiMID_t CPT_MultiMID[] = {
										{eCPT_MID_SRIO_PKTDMA_Grp0, 2},
										{eCPT_MID_QM_SS_Grp0, 4},
										{eCPT_MID_AIF_Grp0, 8},
										{eCPT_MID_QM_CDMA_Grp0, 4},
										{eCPT_MID_NETCP_Grp0, 2},
										{eCPT_MID_ARM_128_Grp0, 32}
									};
#endif

#endif
#if defined(_C66AK2Hxx)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR3A[] = "DDR3A";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG1[] = "QM_CFG1";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_L2_2[] = "L2_2";
static const char CPT_ModName_L2_3[] = "L2_3";
static const char CPT_ModName_L2_4[] = "L2_4";
static const char CPT_ModName_L2_5[] = "L2_5";
static const char CPT_ModName_L2_6[] = "L2_6";
static const char CPT_ModName_L2_7[] = "L2_7";
static const char CPT_ModName_RAC[] = "RAC";
static const char CPT_ModName_RAC_CFG1[] = "RAC_CFG1";
static const char CPT_ModName_TAC[] = "TAC";
static const char CPT_ModName_QM_CFG2[] = "QM_CFG2";
static const char CPT_ModName_RAC_CFG2[] = "RAC_CFG2";
static const char CPT_ModName_DDR3B[] = "DDR3B";
static const char CPT_ModName_BCR_CFG[] = "BCR_CFG";
static const char CPT_ModName_TPCC_0_4[] = "TPCC_0_4";
static const char CPT_ModName_TPCC_1_2_3[] = "TPCC_1_2_3";
static const char CPT_ModName_INTC_CFG[] = "INTC_CFG";
static const char CPT_ModName_MSMC_4[] = "MSMC_4";
static const char CPT_ModName_MSMC_5[] = "MSMC_5";
static const char CPT_ModName_MSMC_6[] = "MSMC_6";
static const char CPT_ModName_MSMC_7[] = "MSMC_7";
static const char CPT_ModName_SPI_ROM_EMIF16[] = "SPI_ROM_EMIF16";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR3A,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG1,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_L2_1,
									CPT_ModName_L2_2,
									CPT_ModName_L2_3,
									CPT_ModName_L2_4,
									CPT_ModName_L2_5,
									CPT_ModName_L2_6,
									CPT_ModName_L2_7,
									CPT_ModName_RAC,
									CPT_ModName_RAC_CFG1,
									CPT_ModName_TAC,
									CPT_ModName_QM_CFG2,
									CPT_ModName_RAC_CFG2,
									CPT_ModName_DDR3B,
									CPT_ModName_BCR_CFG,
									CPT_ModName_TPCC_0_4,
									CPT_ModName_TPCC_1_2_3,
									CPT_ModName_INTC_CFG,
									CPT_ModName_MSMC_4,
									CPT_ModName_MSMC_5,
									CPT_ModName_MSMC_6,
									CPT_ModName_MSMC_7,
									CPT_ModName_SPI_ROM_EMIF16
								};

#endif //_STM_Logging

//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR3A
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM1_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d58000, // eCPT_L2_2
											0x01d60000, // eCPT_L2_3
											0x01D68000, // eCPT_L2_4
											0x01D70000, // eCPT_L2_5
											0x01D78000, // eCPT_L2_6
											0x01D80000, // eCPT_L2_7
											0x01D88000, // eCPT_RAC
											0x01D90000, // eCPT_RAC_CFG1
											0x01D98000, // eCPT_TAC
											0x01DA0000, // eCPT_QM_CFG2
											0x01DA8000, // eCPT_RAC_CFG2
											0x01DB0000, // eCPT_DDR3B
											0x01DB8000, // eCPT_BCR_CFG
											0x01DC0000, // eCPT_TPCC_0_4
											0x01DC8000, // eCPT_TPCC_1_2_3
											0x01DD0000, // eCPT_INTC_CFG
											0x01DD8000, // eCPT_MSMC_4
											0x01DE0000, // eCPT_MSMC_5
											0x01DE0400, // eCPT_MSMC_6
											0x01DE0800, // eCPT_MSMC_7
											0x01DE8000  // eCPT_SPI_ROM_EMIF16
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     1, // eCPT_MSMC_0
											1, // eCPT_MSMC_1
											1, // eCPT_MSMC_2
											1, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											1, // eCPT_DDR3A
											3, // eCPT_SM
											3, // eCPT_QM1_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_L2_2
											3, // eCPT_L2_3
											3, // eCPT_L2_4
											3, // eCPT_L2_5
											3, // eCPT_L2_6
											3, // eCPT_L2_7
											3, // eCPT_RAC
											3, // eCPT_RAC_CFG1
											3, // eCPT_TAC
											3, // eCPT_QM_CFG2
											3, // eCPT_RAC_CFG2
											3, // eCPT_DDR3B
											3, // eCPT_BCR_CFG
											3, // eCPT_TPCC_0_4
											3, // eCPT_TPCC_1_2_3
											3, // eCPT_INTC_CFG
											1, // eCPT_MSMC_4
											1, // eCPT_MSMC_5
											1, // eCPT_MSMC_6
											1, // eCPT_MSMC_7
											3  // eCPT_SPI_ROM_EMIF16
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_SRIO_PKTDMA_Grp0,
										2,
										eCPT_MID_QM_second_Grp0,
										4,
										eCPT_MID_AIF_Grp0,
										8,
										eCPT_MID_XGE_Grp0,
										4,
										eCPT_MID_QM2_CDMA_Grp0,
										4,
										eCPT_MID_QM1_CDMA_Grp0,
										4,
										eCPT_MID_NETCP_Grp0,
										4
									};

#endif

#if defined(_66AK2Exx)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR3A[] = "DDR3A";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG1[] = "QM_CFG1";
static const char CPT_ModName_CFG[] = "SCR3_CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_QM_CFG2[] = "QM_CFG2";
static const char CPT_ModName_TPCC_0_4[] = "TPCC_0_4";
static const char CPT_ModName_TPCC_1_2_3[] = "TPCC_1_2_3";
static const char CPT_ModName_INTC_CFG[] = "INTC_CFG";
static const char CPT_ModName_MSMC_4[] = "MSMC_4";
static const char CPT_ModName_MSMC_5[] = "MSMC_5";
static const char CPT_ModName_MSMC_6[] = "MSMC_6";
static const char CPT_ModName_MSMC_7[] = "MSMC_7";
static const char CPT_ModName_SPI_ROM_EMIF16[] = "SPI_ROM_EMIF16";
static const char CPT_ModName_NETCP_USB_CFG[] = "NETCP_USB_CFG";
static const char CPT_ModName_PCIE1_CFG[] = "PCIE1_CFG";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR3A,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG1,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_QM_CFG2,
									CPT_ModName_TPCC_0_4,
									CPT_ModName_TPCC_1_2_3,
									CPT_ModName_INTC_CFG,
									CPT_ModName_MSMC_4,
									CPT_ModName_MSMC_5,
									CPT_ModName_MSMC_6,
									CPT_ModName_MSMC_7,
									CPT_ModName_SPI_ROM_EMIF16,
									CPT_ModName_NETCP_USB_CFG,
									CPT_ModName_PCIE1_CFG
								};

#endif //_STM_Logging

//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR3A
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM1_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01DA0000, // eCPT_QM_CFG2
											0x01DC0000, // eCPT_TPCC_0_4
											0x01DC8000, // eCPT_TPCC_1_2_3
											0x01DD0000, // eCPT_INTC_CFG
											0x01DD8000, // eCPT_MSMC_4
											0x01DE0000, // eCPT_MSMC_5
											0x01DE0400, // eCPT_MSMC_6
											0x01DE0800, // eCPT_MSMC_7
											0x01DE8000, // eCPT_SPI_ROM_EMIF16
											0x021D0400, // NETCP_USB_CFG
											0x01d80000  // PCIE1_CFG
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     1, // eCPT_MSMC_0
											1, // eCPT_MSMC_1
											1, // eCPT_MSMC_2
											1, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											1, // eCPT_DDR3A
											3, // eCPT_SM
											3, // eCPT_QM1_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_QM_CFG2
											3, // eCPT_TPCC_0_4
											3, // eCPT_TPCC_1_2_3
											3, // eCPT_INTC_CFG
											1, // eCPT_MSMC_4
											1, // eCPT_MSMC_5
											1, // eCPT_MSMC_6
											1, // eCPT_MSMC_7
											3, // eCPT_SPI_ROM_EMIF16
											3, // eCPT_NETCP_USB_CFG
											3  // eCPT_PCIE1_CFG
										 };




const CPT_MultiMID_t CPT_MultiMID[] = {
										eCPT_MID_QM_second_Grp0,
										4,
										eCPT_MID_NETCP_GLOBAL1_Grp0,
										4,
										eCPT_MID_XGE_Grp0,
										4,
										eCPT_MID_QM1_CDMA_Grp0,
										4,
										eCPT_MID_NETCP_LOCAL_Grp0,
										8,
										eCPT_MID_NETCP_Grp0,
										4
									};

#endif

#if defined (_TCI6630K2L)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_MSMC_1[] = "MSMC_1";
static const char CPT_ModName_MSMC_2[] = "MSMC_2";
static const char CPT_ModName_MSMC_3[] = "MSMC_3";
static const char CPT_ModName_QM_MST[] = "QM_M";
static const char CPT_ModName_DDR3A[] = "DDR3A";
static const char CPT_ModName_SM[] = "SM";
static const char CPT_ModName_QM_CFG1[] = "QM_CFG1";
static const char CPT_ModName_CFG[] = "CFG";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_L2_1[] = "L2_1";
static const char CPT_ModName_L2_2[] = "L2_2";
static const char CPT_ModName_L2_3[] = "L2_3";
static const char CPT_ModName_RAC[] = "RAC";
static const char CPT_ModName_RAC_CFG1[] = "RAC_CFG1";
static const char CPT_ModName_TAC[] = "TAC";
static const char CPT_ModName_QM_CFG2[] = "QM_CFG2";
static const char CPT_ModName_OSR_PCIE1_CFG[] = "OSR_PCIE1_CFG";
static const char CPT_ModName_TPCC_0_4[] = "TPCC_0";
static const char CPT_ModName_TPCC_1_2_3[] = "TPCC_1_2";
static const char CPT_ModName_INTC_CFG[] = "INTC_CFG";
static const char CPT_ModName_MSMC_4[] = "MSMC_4";
static const char CPT_ModName_MSMC_5[] = "MSMC_5";
static const char CPT_ModName_MSMC_6[] = "MSMC_6";
static const char CPT_ModName_MSMC_7[] = "MSMC_7";
static const char CPT_ModName_SPI_ROM_EMIF16[] = "SPI_ROM_EMIF16";
static const char CPT_ModName_CPT_CFG_3P_U[] = "CFG_3P_U";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
									CPT_ModName_MSMC_1,
									CPT_ModName_MSMC_2,
									CPT_ModName_MSMC_3,
									CPT_ModName_QM_MST,
									CPT_ModName_DDR3A,
									CPT_ModName_SM,
									CPT_ModName_QM_CFG1,
									CPT_ModName_CFG,
									CPT_ModName_L2_0,
									CPT_ModName_L2_1,
									CPT_ModName_L2_2,
									CPT_ModName_L2_3,
									CPT_ModName_RAC,
									CPT_ModName_RAC_CFG1,
									CPT_ModName_TAC,
									CPT_ModName_QM_CFG2,
									CPT_ModName_OSR_PCIE1_CFG,
									CPT_ModName_TPCC_0_4,
									CPT_ModName_TPCC_1_2_3,
									CPT_ModName_INTC_CFG,
									CPT_ModName_MSMC_4,
									CPT_ModName_MSMC_5,
									CPT_ModName_MSMC_6,
									CPT_ModName_MSMC_7,
									CPT_ModName_SPI_ROM_EMIF16,
									CPT_ModName_CPT_CFG_3P_U
								};

#endif //_STM_Logging

//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
											0x01d08000, // eCPT_MSMC_1
											0x01d10000, // eCPT_MSMC_2
											0x01d18000, // eCPT_MSMC_3
											0x01d20000, // eCPT_QM_MST
											0x01d28000, // eCPT_DDR3A
											0x01d30000, // eCPT_SM
											0x01d38000, // eCPT_QM1_PRI
											0x01d40000, // eCPT_SCR3_CFG
											0x01d48000, // eCPT_L2_0
											0x01d50000, // eCPT_L2_1
											0x01d58000, // eCPT_L2_2
											0x01d60000, // eCPT_L2_3
											0x01D88000, // eCPT_RAC
											0x01D90000, // eCPT_RAC_CFG1
											0x01D98000, // eCPT_TAC
											0x01DA0000, // eCPT_QM_CFG2
											0x01DB0000, // eCPT_OSR_PCIE1_CFG
											0x01DC0000, // eCPT_TPCC_0
											0x01DC8000, // eCPT_TPCC_1_2
											0x01DD0000, // eCPT_INTC_CFG
											0x01DD8000, // eCPT_MSMC_4
											0x01DE0000, // eCPT_MSMC_5
											0x01DE0400, // eCPT_MSMC_6
											0x01DE0800, // eCPT_MSMC_7
											0x01DE8000, // eCPT_SPI_ROM_EMIF16
											0x02340800  // eCPT_CFG_3P_U
										 };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     1, // eCPT_MSMC_0
											1, // eCPT_MSMC_1
											1, // eCPT_MSMC_2
											1, // eCPT_MSMC_3
											3, // eCPT_QM_MST
											1, // eCPT_DDR3A
											3, // eCPT_SM
											3, // eCPT_QM1_PRI
											3, // eCPT_SCR3_CFG
											3, // eCPT_L2_0
											3, // eCPT_L2_1
											3, // eCPT_L2_2
											3, // eCPT_L2_3
											3, // eCPT_RAC
											3, // eCPT_RAC_CFG1
											3, // eCPT_TAC
											3, // eCPT_QM_CFG2
											3, // eCPT_OSR_PCIE1_CFG
											3, // eCPT_TPCC_0_4
											3, // eCPT_TPCC_1_2_3
											3, // eCPT_INTC_CFG
											1, // eCPT_MSMC_4
											1, // eCPT_MSMC_5
											1, // eCPT_MSMC_6
											1, // eCPT_MSMC_7
											3, // eCPT_SPI_ROM_EMIF16
											3 // eCPT_CFG_3P_U
										 };




const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_QM_second_Grp0,
										4,
										eCPT_MID_IQN_CDMA_Grp0,
										4,
										eCPT_MID_QM1_CDMA_Grp0,
										4,
										eCPT_MID_NETCP_LOCAL_Grp0,
										8,
										eCPT_MID_NETCP_GLOBAL_Grp0,
										4
									};

#endif
#if defined (_66AK2Gxx)

#ifdef _STM_Logging
static const char CPT_ModName_MSMC_0[] = "MSMC_0";
static const char CPT_ModName_DDR3[] = "DDR3";
static const char CPT_ModName_L2_0[] = "L2_0";
static const char CPT_ModName_PCIE[] = "PCIE";
static const char CPT_ModName_DXB[] = "DXB";
static const char CPT_ModName_MCASP_MCBSP[] = "MCASP_MCBSP";
static const char CPT_ModName_GPMC_MMC_QSPI[] = "GPMC_MMC_QSPI";
static const char CPT_ModName_TPCC[] = "TPCC0";
static const char CPT_ModName_ALWAYSON_CFG[] ="ALWAYSON_CFG";
static const char CPT_ModName_GIC[] ="GIC";
static const char CPT_ModName_CIC[] ="CIC";
static const char CPT_ModName_ROM_SPI[] ="ROM_SPI";
static const char CPT_ModName_ALWAYSON_MAIN[] ="ALWAYSON_MAIN";
static const char CPT_ModName_ICSS_ASRC[] ="ICSS_ASRC";
static const char CPT_ModName_CFG[] ="CFG";

//CP Tracer module mame strings indexed by eCPT_ModID
const char * pCPT_ModNames[] = {    CPT_ModName_MSMC_0,
                                    CPT_ModName_DDR3,
                                    CPT_ModName_L2_0,
                                    CPT_ModName_PCIE,
                                    CPT_ModName_DXB,
                                    CPT_ModName_MCASP_MCBSP,
                                    CPT_ModName_GPMC_MMC_QSPI,
                                    CPT_ModName_TPCC,
                                    CPT_ModName_CFG,
                                    CPT_ModName_ALWAYSON_CFG,
                                    CPT_ModName_GIC,
                                    CPT_ModName_CIC,
                                    CPT_ModName_ROM_SPI,
                                    CPT_ModName_ALWAYSON_MAIN,
                                    CPT_ModName_ICSS_ASRC
                                };

#endif //_STM_Logging

//Nyquist CP Tracer Address Table indexed by eCPT_ModID
const uint32_t CPT_BaseAddressTable[] = {   0x01d00000, // eCPT_MSMC_0
                                            0x01d08000, // eCPT_DDR
                                            0x01d10000, // eCPT_L2_0
                                            0x01d18000, // eCPT_PCIE
                                            0x01d20000, // eCPT_DXB
                                            0x01d28000, // eCPT_MCASP_MCBSP
                                            0x01d30000, // eCPT_GPMC_MMC_QSPI
                                            0x01d38000, // eCPT_TPCC
                                            0x01d40000, // eCPT_CFG
                                            0x01d48000, // eCPT_ALWAYSON_CFG
                                            0x01d50000, // eCPT_GIC
                                            0x01d58000, // eCPT_CIC
                                            0x01d60000, // eCPT_ROM_SPI
                                            0x01D68000, // eCPT_ALWAYSON_MAIN
                                            0x01D70000  // eCPT_ICSS_ASRC
                                         };

//Nyquist CP Tracer clock rate divide-by-factors indexed by eCPT_ModID
const uint8_t CPT_ModDivByFactors[] = {     1, // eCPT_MSMC_0
                                            1, // eCPT_DDR
                                            3, // eCPT_L2_0
                                            3, // eCPT_PCIE
                                            3, // eCPT_DXB
                                            3, // eCPT_MCASP_MCBSP
                                            3, // eCPT_GPMC_MMC_QSPI
                                            3, // eCPT_TPCC
                                            3, // eCPT_CFG
                                            3, // eCPT_ALWAYSON_CFG
                                            3, // eCPT_GIC
                                            3, // eCPT_CIC
                                            3, // eCPT_ROM_SPI
                                            3, // eCPT_ALWAYSON_MAIN
                                            3  // eCPT_ICSS_ASRC
                                         };

const CPT_MultiMID_t CPT_MultiMID[] = { eCPT_MID_NSSL_Grp0,
                                        4,
                                        eCPT_MID_DBX_Grp0,
                                        32,
                                        eCPT_MID_DSSUL_Grp0,
                                        16
                                    };

#endif
#ifdef RUNTIME_DEVICE_SELECT
//////////////////////////////////////////////////////////////////////////////////////////
// Master names in eCPT_MasterID id order
//////////////////////////////////////////////////////////////////////////////////////////
static const char CPT_MasterName_GEM0[] = "DSP0";
static const char CPT_MasterName_GEM1[] = "DSP1";
static const char CPT_MasterName_GEM2[] = "DSP2";
static const char CPT_MasterName_GEM3[] = "DSP3";
static const char CPT_MasterName_ARM_64[] = "ARM_64";
static const char CPT_MasterName_GEM0_CFG[] = "DSP0_CFG";
static const char CPT_MasterName_GEM1_CFG[] = "DSP1_CFG";
static const char CPT_MasterName_GEM2_CFG[] = "DSP2_CFG";
static const char CPT_MasterName_GEM3_CFG[] = "DSP3_CFG";
static const char CPT_MasterName_EDMA0_TC0_RD[] = "EDMA0_TC0_RD";
static const char CPT_MasterName_EDMA0_TC0_WR[] = "EDMA0_TC0_WR";
static const char CPT_MasterName_EDMA0_TC1_RD[] = "EDMA0_TC1_RD";
static const char CPT_MasterName_EDMA0_TC1_WR[] = "EDMA0_TC1_WR";
static const char CPT_MasterName_EDMA1_TC0_RD[] = "EDMA1_TC0_RD";
static const char CPT_MasterName_EDMA1_TC0_WR[] = "EDMA1_TC0_WR";
static const char CPT_MasterName_EDMA1_TC1_RD[] = "EDMA1_TC1_RD";
static const char CPT_MasterName_EDMA1_TC1_WR[] = "EDMA1_TC1_WR";
static const char CPT_MasterName_EDMA1_TC2_RD[] = "EDMA1_TC2_RD";
static const char CPT_MasterName_EDMA1_TC2_WR[] = "EDMA1_TC2_WR";
static const char CPT_MasterName_EDMA1_TC3_RD[] = "EDMA1_TC3_RD";
static const char CPT_MasterName_EDMA1_TC3_WR[] = "EDMA1_TC3_WR";
static const char CPT_MasterName_EDMA2_TC0_RD[] = "EDMA2_TC0_RD";
static const char CPT_MasterName_EDMA2_TC0_WR[] = "EDMA2_TC0_WR";
static const char CPT_MasterName_EDMA2_TC1_RD[] = "EDMA2_TC1_RD";
static const char CPT_MasterName_EDMA2_TC1_WR[] = "EDMA2_TC1_WR";
static const char CPT_MasterName_EDMA2_TC2_RD[] = "EDMA2_TC2_RD";
static const char CPT_MasterName_EDMA2_TC2_WR[] = "EDMA2_TC2_WR";
static const char CPT_MasterName_EDMA2_TC3_RD[] = "EDMA2_TC3_RD";
static const char CPT_MasterName_EDMA2_TC3_WR[] = "EDMA2_TC3_WR";
static const char CPT_MasterName_SRIO_PKTDMA_Grp0[] = "SRIO_PKTDMA_0";
static const char CPT_MasterName_SRIO_PKTDMA_Grp1[] = "SRIO_PKTDMA_1";
static const char CPT_MasterName_DAP[] = "DAP";
static const char CPT_MasterName_TPCC0[] = "TPCC0";
static const char CPT_MasterName_TPCC1[] = "TPCC1";
static const char CPT_MasterName_TPCC2[] = "TPCC2";
static const char CPT_MasterName_MSMC[] = "MSMC";
static const char CPT_MasterName_PCIe[] = "PCIE";
static const char CPT_MasterName_SRIO_M[] = "SRIO_M";
static const char CPT_MasterName_HyperBridge[] = "HYPERBRIDGE";
static const char CPT_MasterName_QM_SS_Grp0[] = "QM_SS_0";
static const char CPT_MasterName_QM_SS_Grp1[] = "QM_SS_1";
static const char CPT_MasterName_QM_SS_Grp2[] = "QM_SS_2";
static const char CPT_MasterName_QM_SS_Grp3[] = "QM_SS_3";
static const char CPT_MasterName_AIF_Grp0[] = "AIF_0";
static const char CPT_MasterName_AIF_Grp1[] = "AIF_1";
static const char CPT_MasterName_AIF_Grp2[] = "AIF_2";
static const char CPT_MasterName_AIF_Grp3[] = "AIF_3";
static const char CPT_MasterName_AIF_Grp4[] = "AIF_4";
static const char CPT_MasterName_AIF_Grp5[] = "AIF_5";
static const char CPT_MasterName_AIF_Grp6[] = "AIF_6";
static const char CPT_MasterName_AIF_Grp7[] = "AIF_7";
static const char CPT_MasterName_QM_CDMA_Grp0[] = "QM_CDMA_0";
static const char CPT_MasterName_QM_CDMA_Grp1[] = "QM_CDMA_1";
static const char CPT_MasterName_QM_CDMA_Grp2[] = "QM_CDMA_2";
static const char CPT_MasterName_QM_CDMA_Grp3[] = "QM_CDMA_3";
static const char CPT_MasterName_NETCP_Grp0[] = "NETCP_0";
static const char CPT_MasterName_NETCP_Grp1[] = "NETCP_1";
static const char CPT_MasterName_TAC[] = "TAC";

static const char CPT_MasterName_BCP_DIO1[] = "BCP_DIO1";
static const char CPT_MasterName_BCP_CDMA[] = "BCP_CDMA";
static const char CPT_MasterName_BCP_DIO0[] = "BCP_DIO0";

static const char CPT_MasterName_ARM_128_Grp0[] = "ARM_128_0";


const char * CPT_TCI6614_MasterNames[] = {
	CPT_MasterName_GEM0,
	CPT_MasterName_GEM1,
	CPT_MasterName_GEM2,
	CPT_MasterName_GEM3,
	CPT_MasterName_ARM_64,
	CPT_MasterName_GEM0_CFG,
	CPT_MasterName_GEM1_CFG,
	CPT_MasterName_GEM2_CFG,
	CPT_MasterName_GEM3_CFG,
	CPT_MasterName_EDMA0_TC0_RD,
	CPT_MasterName_EDMA0_TC0_WR,
	CPT_MasterName_EDMA0_TC1_RD,
	CPT_MasterName_EDMA0_TC1_WR,
	CPT_MasterName_EDMA1_TC0_RD,
	CPT_MasterName_EDMA1_TC0_WR,
	CPT_MasterName_EDMA1_TC1_RD,
	CPT_MasterName_EDMA1_TC1_WR,
	CPT_MasterName_EDMA1_TC2_RD,
	CPT_MasterName_EDMA1_TC2_WR,
	CPT_MasterName_EDMA1_TC3_RD,
	CPT_MasterName_EDMA1_TC3_WR,
	CPT_MasterName_EDMA2_TC0_RD,
	CPT_MasterName_EDMA2_TC0_WR,
	CPT_MasterName_EDMA2_TC1_RD,
	CPT_MasterName_EDMA2_TC1_WR,
	CPT_MasterName_EDMA2_TC2_RD,
	CPT_MasterName_EDMA2_TC2_WR,
	CPT_MasterName_EDMA2_TC3_RD,
	CPT_MasterName_EDMA2_TC3_WR,
	CPT_MasterName_SRIO_PKTDMA_Grp0,
	CPT_MasterName_SRIO_PKTDMA_Grp1,
	CPT_MasterName_DAP,
	CPT_MasterName_TPCC0,
	CPT_MasterName_TPCC1,
	CPT_MasterName_TPCC2,
	CPT_MasterName_MSMC,
	CPT_MasterName_PCIe,
	CPT_MasterName_SRIO_M,
	CPT_MasterName_HyperBridge,
	CPT_MasterName_QM_SS_Grp0,
	CPT_MasterName_QM_SS_Grp1,
	CPT_MasterName_QM_SS_Grp2,
	CPT_MasterName_QM_SS_Grp3,
	CPT_MasterName_AIF_Grp0,
	CPT_MasterName_AIF_Grp1,
	CPT_MasterName_AIF_Grp2,
	CPT_MasterName_AIF_Grp3,
	CPT_MasterName_AIF_Grp4,
	CPT_MasterName_AIF_Grp5,
	CPT_MasterName_AIF_Grp6,
	CPT_MasterName_AIF_Grp7,
	CPT_MasterName_QM_CDMA_Grp0,
	CPT_MasterName_QM_CDMA_Grp1,
	CPT_MasterName_QM_CDMA_Grp2,
	CPT_MasterName_QM_CDMA_Grp3,
	CPT_MasterName_NETCP_Grp0,
	CPT_MasterName_NETCP_Grp1,
	CPT_MasterName_TAC,
	CPT_MasterName_BCP_DIO1,
	CPT_MasterName_BCP_CDMA,
	CPT_MasterName_BCP_DIO0,
	CPT_MasterName_ARM_128_Grp0
};

const char * CPT_TCI6612_MasterNames[] = {
	CPT_MasterName_GEM0,
	CPT_MasterName_GEM1,
	CPT_MasterName_ARM_64,
	CPT_MasterName_GEM0_CFG,
	CPT_MasterName_GEM1_CFG,
	CPT_MasterName_EDMA0_TC0_RD,
	CPT_MasterName_EDMA0_TC0_WR,
	CPT_MasterName_EDMA0_TC1_RD,
	CPT_MasterName_EDMA0_TC1_WR,
	CPT_MasterName_EDMA1_TC0_RD,
	CPT_MasterName_EDMA1_TC0_WR,
	CPT_MasterName_EDMA1_TC1_RD,
	CPT_MasterName_EDMA1_TC1_WR,
	CPT_MasterName_EDMA1_TC2_RD,
	CPT_MasterName_EDMA1_TC2_WR,
	CPT_MasterName_EDMA1_TC3_RD,
	CPT_MasterName_EDMA1_TC3_WR,
	CPT_MasterName_EDMA2_TC0_RD,
	CPT_MasterName_EDMA2_TC0_WR,
	CPT_MasterName_EDMA2_TC1_RD,
	CPT_MasterName_EDMA2_TC1_WR,
	CPT_MasterName_EDMA2_TC2_RD,
	CPT_MasterName_EDMA2_TC2_WR,
	CPT_MasterName_EDMA2_TC3_RD,
	CPT_MasterName_EDMA2_TC3_WR,
	CPT_MasterName_SRIO_PKTDMA_Grp0,
	CPT_MasterName_SRIO_PKTDMA_Grp1,
	CPT_MasterName_DAP,
	CPT_MasterName_TPCC0,
	CPT_MasterName_TPCC1,
	CPT_MasterName_TPCC2,
	CPT_MasterName_MSMC,
	CPT_MasterName_PCIe,
	CPT_MasterName_SRIO_M,
	CPT_MasterName_HyperBridge,
	CPT_MasterName_QM_SS_Grp0,
	CPT_MasterName_QM_SS_Grp1,
	CPT_MasterName_QM_SS_Grp2,
	CPT_MasterName_QM_SS_Grp3,
	CPT_MasterName_AIF_Grp0,
	CPT_MasterName_AIF_Grp1,
	CPT_MasterName_AIF_Grp2,
	CPT_MasterName_AIF_Grp3,
	CPT_MasterName_AIF_Grp4,
	CPT_MasterName_AIF_Grp5,
	CPT_MasterName_AIF_Grp6,
	CPT_MasterName_AIF_Grp7,
	CPT_MasterName_QM_CDMA_Grp0,
	CPT_MasterName_QM_CDMA_Grp1,
	CPT_MasterName_QM_CDMA_Grp2,
	CPT_MasterName_QM_CDMA_Grp3,
	CPT_MasterName_NETCP_Grp0,
	CPT_MasterName_NETCP_Grp1,
	CPT_MasterName_TAC,
	CPT_MasterName_BCP_DIO1,
	CPT_MasterName_BCP_CDMA,
	CPT_MasterName_BCP_DIO0,
	CPT_MasterName_ARM_128_Grp0,
};

struct _CPT_MasterIdTable {
	int master_id;
	int group_cnt;
};

/* Note: The master id's in this table are indexed from the CPT_TCI6612_MasterNames tables */

struct _CPT_MasterIdTable CPT_TCI6614_MasterIDTable[] = {
	{eCPT_TCI6614_MID_GEM0, 0},
	{eCPT_TCI6614_MID_GEM1, 0},
	{eCPT_TCI6614_MID_GEM2, 0},
	{eCPT_TCI6614_MID_GEM3, 0},
	{eCPT_TCI6614_MID_ARM_64, 0},
	{eCPT_TCI6614_MID_GEM0_CFG, 0},
	{eCPT_TCI6614_MID_GEM1_CFG, 0},
	{eCPT_TCI6614_MID_GEM2_CFG, 0},
	{eCPT_TCI6614_MID_GEM3_CFG, 0},
	{eCPT_TCI6614_MID_EDMA0_TC0_RD, 0},
	{eCPT_TCI6614_MID_EDMA0_TC0_WR, 0},
	{eCPT_TCI6614_MID_EDMA0_TC1_RD, 0},
	{eCPT_TCI6614_MID_EDMA0_TC1_WR, 0},
	{eCPT_TCI6614_MID_EDMA1_TC0_RD, 0},
	{eCPT_TCI6614_MID_EDMA1_TC0_WR, 0},
	{eCPT_TCI6614_MID_EDMA1_TC1_RD, 0},
	{eCPT_TCI6614_MID_EDMA1_TC1_WR, 0},
	{eCPT_TCI6614_MID_EDMA1_TC2_RD, 0},
	{eCPT_TCI6614_MID_EDMA1_TC2_WR, 0},
	{eCPT_TCI6614_MID_EDMA1_TC3_RD, 0},
	{eCPT_TCI6614_MID_EDMA1_TC3_WR, 0},
	{eCPT_TCI6614_MID_EDMA2_TC0_RD, 0},
	{eCPT_TCI6614_MID_EDMA2_TC0_WR, 0},
	{eCPT_TCI6614_MID_EDMA2_TC1_RD, 0},
	{eCPT_TCI6614_MID_EDMA2_TC1_WR, 0},
	{eCPT_TCI6614_MID_EDMA2_TC2_RD, 0},
	{eCPT_TCI6614_MID_EDMA2_TC2_WR, 0},
	{eCPT_TCI6614_MID_EDMA2_TC3_RD, 0},
	{eCPT_TCI6614_MID_EDMA2_TC3_WR, 0},
	{eCPT_TCI6614_MID_SRIO_PKTDMA_Grp0, 2},
	{eCPT_TCI6614_MID_SRIO_PKTDMA_Grp1, 0},
	{eCPT_TCI6614_MID_DAP, 0},
	{eCPT_TCI6614_MID_TPCC0, 0},
	{eCPT_TCI6614_MID_TPCC1, 0},
	{eCPT_TCI6614_MID_TPCC2, 0},
	{eCPT_TCI6614_MID_MSMC, 0},
	{eCPT_TCI6614_MID_PCIe, 0},
	{eCPT_TCI6614_MID_SRIO_M, 0},
	{eCPT_TCI6614_MID_HyperBridge, 0},
	{eCPT_TCI6614_MID_QM_SS_Grp0, 0},
	{eCPT_TCI6614_MID_QM_SS_Grp1, 0},
	{eCPT_TCI6614_MID_QM_SS_Grp2, 0},
	{eCPT_TCI6614_MID_QM_SS_Grp3, 0},
	{eCPT_TCI6614_MID_AIF_Grp0, 8},
	{eCPT_TCI6614_MID_AIF_Grp1, 0},
	{eCPT_TCI6614_MID_AIF_Grp2, 0},
	{eCPT_TCI6614_MID_AIF_Grp3, 0},
	{eCPT_TCI6614_MID_AIF_Grp4, 0},
	{eCPT_TCI6614_MID_AIF_Grp5, 0},
	{eCPT_TCI6614_MID_AIF_Grp6, 0},
	{eCPT_TCI6614_MID_AIF_Grp7, 0},
	{eCPT_TCI6614_MID_QM_CDMA_Grp0, 4},
	{eCPT_TCI6614_MID_QM_CDMA_Grp1, 0},
	{eCPT_TCI6614_MID_QM_CDMA_Grp2, 0},
	{eCPT_TCI6614_MID_QM_CDMA_Grp3, 0},
	{eCPT_TCI6614_MID_NETCP_Grp0, 2},
	{eCPT_TCI6614_MID_NETCP_Grp1, 0},
	{eCPT_TCI6614_MID_TAC, 0},
	{eCPT_TCI6614_MID_BCP_DIO1, 0},
	{eCPT_TCI6614_MID_BCP_CDMA, 0},
	{eCPT_TCI6614_MID_BCP_DIO0, 0},
	{eCPT_TCI6614_MID_ARM_128_Grp0, 32}
};


struct _CPT_MasterIdTable CPT_TCI6612_MasterIDTable[] = {
	{eCPT_TCI6612_MID_GEM0, 0},
	{eCPT_TCI6612_MID_GEM1, 0},
	{eCPT_TCI6612_MID_ARM_64, 0},
	{eCPT_TCI6612_MID_GEM0_CFG, 0},
	{eCPT_TCI6612_MID_GEM1_CFG, 0},
	{eCPT_TCI6612_MID_EDMA0_TC0_RD, 0},
	{eCPT_TCI6612_MID_EDMA0_TC0_WR, 0},
	{eCPT_TCI6612_MID_EDMA0_TC1_RD, 0},
	{eCPT_TCI6612_MID_EDMA0_TC1_WR, 0},
	{eCPT_TCI6612_MID_EDMA1_TC0_RD, 0},
	{eCPT_TCI6612_MID_EDMA1_TC0_WR, 0},
	{eCPT_TCI6612_MID_EDMA1_TC1_RD, 0},
	{eCPT_TCI6612_MID_EDMA1_TC1_WR, 0},
	{eCPT_TCI6612_MID_EDMA1_TC2_RD, 0},
	{eCPT_TCI6612_MID_EDMA1_TC2_WR, 0},
	{eCPT_TCI6612_MID_EDMA1_TC3_RD, 0},
	{eCPT_TCI6612_MID_EDMA1_TC3_WR, 0},
	{eCPT_TCI6612_MID_EDMA2_TC0_RD, 0},
	{eCPT_TCI6612_MID_EDMA2_TC0_WR, 0},
	{eCPT_TCI6612_MID_EDMA2_TC1_RD, 0},
	{eCPT_TCI6612_MID_EDMA2_TC1_WR, 0},
	{eCPT_TCI6612_MID_EDMA2_TC2_RD, 0},
	{eCPT_TCI6612_MID_EDMA2_TC2_WR, 0},
	{eCPT_TCI6612_MID_EDMA2_TC3_RD, 0},
	{eCPT_TCI6612_MID_EDMA2_TC3_WR, 0},
	{eCPT_TCI6612_MID_SRIO_PKTDMA_Grp0, 2},
	{eCPT_TCI6612_MID_SRIO_PKTDMA_Grp1, 0},
	{eCPT_TCI6612_MID_DAP, 0},
	{eCPT_TCI6612_MID_TPCC0, 0},
	{eCPT_TCI6612_MID_TPCC1, 0},
	{eCPT_TCI6612_MID_TPCC2, 0},
	{eCPT_TCI6612_MID_MSMC, 0},
	{eCPT_TCI6612_MID_PCIe, 0},
	{eCPT_TCI6612_MID_SRIO_M, 0},
	{eCPT_TCI6612_MID_HyperBridge, 0},
	{eCPT_TCI6612_MID_QM_SS_Grp0, 0},
	{eCPT_TCI6612_MID_QM_SS_Grp1, 0},
	{eCPT_TCI6612_MID_QM_SS_Grp2, 0},
	{eCPT_TCI6612_MID_QM_SS_Grp3, 0},
	{eCPT_TCI6612_MID_AIF_Grp0, 8},
	{eCPT_TCI6612_MID_AIF_Grp1, 0},
	{eCPT_TCI6612_MID_AIF_Grp2, 0},
	{eCPT_TCI6612_MID_AIF_Grp3, 0},
	{eCPT_TCI6612_MID_AIF_Grp4, 0},
	{eCPT_TCI6612_MID_AIF_Grp5, 0},
	{eCPT_TCI6612_MID_AIF_Grp6, 0},
	{eCPT_TCI6612_MID_AIF_Grp7, 0},
	{eCPT_TCI6612_MID_QM_CDMA_Grp1, 0},
	{eCPT_TCI6612_MID_QM_CDMA_Grp3, 0},
	{eCPT_TCI6612_MID_NETCP_Grp0, 2},
	{eCPT_TCI6612_MID_NETCP_Grp1, 0},
	{eCPT_TCI6612_MID_TAC, 0},
	{eCPT_TCI6612_MID_BCP_DIO1, 0},
	{eCPT_TCI6612_MID_BCP_CDMA, 0},
	{eCPT_TCI6612_MID_BCP_DIO0, 0},
	{eCPT_TCI6612_MID_ARM_128_Grp0, 32}
};

#if 0
/* Master id table */
struct _CPT_MasterIdTable {
	const char * name;
	int master_id;
	int group_cnt;
};


struct _CPT_MasterIdTable CPT_TCI6614_MasterIDTable[] = {
											{CPT_MasterName_GEM0, eCPT_TCI6614_MID_GEM0, 0},
											{CPT_MasterName_GEM1, eCPT_TCI6614_MID_GEM1, 0},
											{CPT_MasterName_GEM2, eCPT_TCI6614_MID_GEM2, 0},
											{CPT_MasterName_GEM3, eCPT_TCI6614_MID_GEM3, 0},
											{CPT_MasterName_ARM_64, eCPT_TCI6614_MID_ARM_64, 0},
											{CPT_MasterName_GEM0_CFG, eCPT_TCI6614_MID_GEM0_CFG, 0},
											{CPT_MasterName_GEM1_CFG, eCPT_TCI6614_MID_GEM1_CFG, 0},
											{CPT_MasterName_GEM2_CFG, eCPT_TCI6614_MID_GEM2_CFG, 0},
											{CPT_MasterName_GEM3_CFG, eCPT_TCI6614_MID_GEM3_CFG, 0},
											{CPT_MasterName_EDMA0_TC0_RD, eCPT_TCI6614_MID_EDMA0_TC0_RD, 0},
											{CPT_MasterName_EDMA0_TC0_WR, eCPT_TCI6614_MID_EDMA0_TC0_WR, 0},
											{CPT_MasterName_EDMA0_TC1_RD, eCPT_TCI6614_MID_EDMA0_TC1_RD, 0},
											{CPT_MasterName_EDMA0_TC1_WR, eCPT_TCI6614_MID_EDMA0_TC1_WR, 0},
											{CPT_MasterName_EDMA1_TC0_RD, eCPT_TCI6614_MID_EDMA1_TC0_RD, 0},
											{CPT_MasterName_EDMA1_TC0_WR, eCPT_TCI6614_MID_EDMA1_TC0_WR, 0},
											{CPT_MasterName_EDMA1_TC1_RD, eCPT_TCI6614_MID_EDMA1_TC1_RD, 0},
											{CPT_MasterName_EDMA1_TC1_WR, eCPT_TCI6614_MID_EDMA1_TC1_WR, 0},
											{CPT_MasterName_EDMA1_TC2_RD, eCPT_TCI6614_MID_EDMA1_TC2_RD, 0},
											{CPT_MasterName_EDMA1_TC2_WR, eCPT_TCI6614_MID_EDMA1_TC2_WR, 0},
											{CPT_MasterName_EDMA1_TC3_RD, eCPT_TCI6614_MID_EDMA1_TC3_RD, 0},
											{CPT_MasterName_EDMA1_TC3_WR, eCPT_TCI6614_MID_EDMA1_TC3_WR, 0},
											{CPT_MasterName_EDMA2_TC0_RD, eCPT_TCI6614_MID_EDMA2_TC0_RD, 0},
											{CPT_MasterName_EDMA2_TC0_WR, eCPT_TCI6614_MID_EDMA2_TC0_WR, 0},
											{CPT_MasterName_EDMA2_TC1_RD, eCPT_TCI6614_MID_EDMA2_TC1_RD, 0},
											{CPT_MasterName_EDMA2_TC1_WR, eCPT_TCI6614_MID_EDMA2_TC1_WR, 0},
											{CPT_MasterName_EDMA2_TC2_RD, eCPT_TCI6614_MID_EDMA2_TC2_RD, 0},
											{CPT_MasterName_EDMA2_TC2_WR, eCPT_TCI6614_MID_EDMA2_TC2_WR, 0},
											{CPT_MasterName_EDMA2_TC3_RD, eCPT_TCI6614_MID_EDMA2_TC3_RD, 0},
											{CPT_MasterName_EDMA2_TC3_WR, eCPT_TCI6614_MID_EDMA2_TC3_WR, 0},
											{CPT_MasterName_SRIO_PKTDMA_Grp0, eCPT_TCI6614_MID_SRIO_PKTDMA_Grp0, 2},
											{CPT_MasterName_SRIO_PKTDMA_Grp1, eCPT_TCI6614_MID_SRIO_PKTDMA_Grp1, 0},
											{CPT_MasterName_DAP, eCPT_TCI6614_MID_DAP, 0},
											{CPT_MasterName_TPCC0, eCPT_TCI6614_MID_TPCC0, 0},
											{CPT_MasterName_TPCC1, eCPT_TCI6614_MID_TPCC1, 0},
											{CPT_MasterName_TPCC2, eCPT_TCI6614_MID_TPCC2, 0},
											{CPT_MasterName_MSMC,  eCPT_TCI6614_MID_MSMC, 0},
											{CPT_MasterName_PCIe, eCPT_TCI6614_MID_PCIe, 0},
											{CPT_MasterName_SRIO_M, eCPT_TCI6614_MID_SRIO_M, 0},
											{CPT_MasterName_HyperBridge, eCPT_TCI6614_MID_HyperBridge, 0},
											{CPT_MasterName_QM_SS_Grp0, eCPT_TCI6614_MID_QM_SS_Grp0, 0},
											{CPT_MasterName_QM_SS_Grp1, eCPT_TCI6614_MID_QM_SS_Grp1, 0},
											{CPT_MasterName_QM_SS_Grp2, eCPT_TCI6614_MID_QM_SS_Grp2, 0},
											{CPT_MasterName_QM_SS_Grp3, eCPT_TCI6614_MID_QM_SS_Grp3, 0},
											{CPT_MasterName_AIF_Grp0, eCPT_TCI6614_MID_AIF_Grp0, 8},
											{CPT_MasterName_AIF_Grp1, eCPT_TCI6614_MID_AIF_Grp1, 0},
											{CPT_MasterName_AIF_Grp2, eCPT_TCI6614_MID_AIF_Grp2, 0},
											{CPT_MasterName_AIF_Grp3, eCPT_TCI6614_MID_AIF_Grp3, 0},
											{CPT_MasterName_AIF_Grp4, eCPT_TCI6614_MID_AIF_Grp4, 0},
											{CPT_MasterName_AIF_Grp5, eCPT_TCI6614_MID_AIF_Grp5, 0},
											{CPT_MasterName_AIF_Grp6, eCPT_TCI6614_MID_AIF_Grp6, 0},
											{CPT_MasterName_AIF_Grp7, eCPT_TCI6614_MID_AIF_Grp7, 0},
											{CPT_MasterName_QM_CDMA_Grp0, eCPT_TCI6614_MID_QM_CDMA_Grp0, 4},
											{CPT_MasterName_QM_CDMA_Grp1, eCPT_TCI6614_MID_QM_CDMA_Grp1, 0},
											{CPT_MasterName_QM_CDMA_Grp2, eCPT_TCI6614_MID_QM_CDMA_Grp2, 0},
											{CPT_MasterName_QM_CDMA_Grp3, eCPT_TCI6614_MID_QM_CDMA_Grp3, 0},
											{CPT_MasterName_NETCP_Grp0, eCPT_TCI6614_MID_NETCP_Grp0, 2},
											{CPT_MasterName_NETCP_Grp1, eCPT_TCI6614_MID_NETCP_Grp1, 0},
											{CPT_MasterName_TAC, eCPT_TCI6614_MID_TAC, 0},
											{CPT_MasterName_BCP_DIO1, eCPT_TCI6614_MID_BCP_DIO1, 0},
											{CPT_MasterName_BCP_CDMA, eCPT_TCI6614_MID_BCP_CDMA, 0},
											{CPT_MasterName_BCP_DIO0, eCPT_TCI6614_MID_BCP_DIO0, 0},
											{CPT_MasterName_ARM_128_Grp0, eCPT_TCI6614_MID_ARM_128_Grp0, 32}
};


struct _CPT_MasterIdTable CPT_TCI6612_MasterIDTable[] = {
											{CPT_MasterName_GEM0, eCPT_TCI6612_MID_GEM0, 0},
											{CPT_MasterName_GEM1, eCPT_TCI6612_MID_GEM1, 0},
											{CPT_MasterName_ARM_64, eCPT_TCI6612_MID_ARM_64, 0},
											{CPT_MasterName_GEM0_CFG, eCPT_TCI6612_MID_GEM0_CFG, 0},
											{CPT_MasterName_GEM1_CFG, eCPT_TCI6612_MID_GEM1_CFG, 0},
											{CPT_MasterName_EDMA0_TC0_RD, eCPT_TCI6612_MID_EDMA0_TC0_RD, 0},
											{CPT_MasterName_EDMA0_TC0_WR, eCPT_TCI6612_MID_EDMA0_TC0_WR, 0},
											{CPT_MasterName_EDMA0_TC1_RD, eCPT_TCI6612_MID_EDMA0_TC1_RD, 0},
											{CPT_MasterName_EDMA0_TC1_WR, eCPT_TCI6612_MID_EDMA0_TC1_WR, 0},
											{CPT_MasterName_EDMA1_TC0_RD, eCPT_TCI6612_MID_EDMA1_TC0_RD, 0},
											{CPT_MasterName_EDMA1_TC0_WR, eCPT_TCI6612_MID_EDMA1_TC0_WR, 0},
											{CPT_MasterName_EDMA1_TC1_RD, eCPT_TCI6612_MID_EDMA1_TC1_RD, 0},
											{CPT_MasterName_EDMA1_TC1_WR, eCPT_TCI6612_MID_EDMA1_TC1_WR, 0},
											{CPT_MasterName_EDMA1_TC2_RD, eCPT_TCI6612_MID_EDMA1_TC2_RD, 0},
											{CPT_MasterName_EDMA1_TC2_WR, eCPT_TCI6612_MID_EDMA1_TC2_WR, 0},
											{CPT_MasterName_EDMA1_TC3_RD, eCPT_TCI6612_MID_EDMA1_TC3_RD, 0},
											{CPT_MasterName_EDMA1_TC3_WR, eCPT_TCI6612_MID_EDMA1_TC3_WR, 0},
											{CPT_MasterName_EDMA2_TC0_RD, eCPT_TCI6612_MID_EDMA2_TC0_RD, 0},
											{CPT_MasterName_EDMA2_TC0_WR, eCPT_TCI6612_MID_EDMA2_TC0_WR, 0},
											{CPT_MasterName_EDMA2_TC1_RD, eCPT_TCI6612_MID_EDMA2_TC1_RD, 0},
											{CPT_MasterName_EDMA2_TC1_WR, eCPT_TCI6612_MID_EDMA2_TC1_WR, 0},
											{CPT_MasterName_EDMA2_TC2_RD, eCPT_TCI6612_MID_EDMA2_TC2_RD, 0},
											{CPT_MasterName_EDMA2_TC2_WR, eCPT_TCI6612_MID_EDMA2_TC2_WR, 0},
											{CPT_MasterName_EDMA2_TC3_RD, eCPT_TCI6612_MID_EDMA2_TC3_RD, 0},
											{CPT_MasterName_EDMA2_TC3_WR, eCPT_TCI6612_MID_EDMA2_TC3_WR, 0},
											{CPT_MasterName_SRIO_PKTDMA_Grp0, eCPT_TCI6612_MID_SRIO_PKTDMA_Grp0, 2},
											{CPT_MasterName_SRIO_PKTDMA_Grp1, eCPT_TCI6612_MID_SRIO_PKTDMA_Grp1, 0},
											{CPT_MasterName_DAP, eCPT_TCI6612_MID_DAP, 0},
											{CPT_MasterName_TPCC0, eCPT_TCI6612_MID_TPCC0, 0},
											{CPT_MasterName_TPCC1, eCPT_TCI6612_MID_TPCC1, 0},
											{CPT_MasterName_TPCC2, eCPT_TCI6612_MID_TPCC2, 0},
											{CPT_MasterName_MSMC,  eCPT_TCI6612_MID_MSMC, 0},
											{CPT_MasterName_PCIe, eCPT_TCI6612_MID_PCIe, 0},
											{CPT_MasterName_SRIO_M, eCPT_TCI6612_MID_SRIO_M, 0},
											{CPT_MasterName_HyperBridge, eCPT_TCI6612_MID_HyperBridge, 0},
											{CPT_MasterName_QM_SS_Grp0, eCPT_TCI6612_MID_QM_SS_Grp0, 0},
											{CPT_MasterName_QM_SS_Grp1, eCPT_TCI6612_MID_QM_SS_Grp1, 0},
											{CPT_MasterName_QM_SS_Grp2, eCPT_TCI6612_MID_QM_SS_Grp2, 0},
											{CPT_MasterName_QM_SS_Grp3, eCPT_TCI6612_MID_QM_SS_Grp3, 0},
											{CPT_MasterName_AIF_Grp0, eCPT_TCI6612_MID_AIF_Grp0, 8},
											{CPT_MasterName_AIF_Grp1, eCPT_TCI6612_MID_AIF_Grp1, 0},
											{CPT_MasterName_AIF_Grp2, eCPT_TCI6612_MID_AIF_Grp2, 0},
											{CPT_MasterName_AIF_Grp3, eCPT_TCI6612_MID_AIF_Grp3, 0},
											{CPT_MasterName_AIF_Grp4, eCPT_TCI6612_MID_AIF_Grp4, 0},
											{CPT_MasterName_AIF_Grp5, eCPT_TCI6612_MID_AIF_Grp5, 0},
											{CPT_MasterName_AIF_Grp6, eCPT_TCI6612_MID_AIF_Grp6, 0},
											{CPT_MasterName_AIF_Grp7, eCPT_TCI6612_MID_AIF_Grp7, 0},
											{CPT_MasterName_QM_CDMA_Grp0, eCPT_TCI6612_MID_QM_CDMA_Grp0, 4},
											{CPT_MasterName_QM_CDMA_Grp1, eCPT_TCI6612_MID_QM_CDMA_Grp1, 0},
											{CPT_MasterName_QM_CDMA_Grp2, eCPT_TCI6612_MID_QM_CDMA_Grp2, 0},
											{CPT_MasterName_QM_CDMA_Grp3, eCPT_TCI6612_MID_QM_CDMA_Grp3, 0},
											{CPT_MasterName_NETCP_Grp0, eCPT_TCI6612_MID_NETCP_Grp0, 2},
											{CPT_MasterName_NETCP_Grp1, eCPT_TCI6612_MID_NETCP_Grp1, 0},
											{CPT_MasterName_TAC, eCPT_TCI6612_MID_TAC, 0},
											{CPT_MasterName_BCP_DIO1, eCPT_TCI6612_MID_BCP_DIO1, 0},
											{CPT_MasterName_BCP_CDMA, eCPT_TCI6612_MID_BCP_CDMA, 0},
											{CPT_MasterName_BCP_DIO0, eCPT_TCI6612_MID_BCP_DIO0, 0},
											{CPT_MasterName_ARM_128_Grp0, eCPT_TCI6612_MID_ARM_128_Grp0, 32}
};
#endif
#endif
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Common Register Maps
//
////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef RUNTIME_DEVICE_SELECT
#define reg_rd32(addr) cTools_reg_rd32((uint32_t)addr)
#define reg_wr32(addr, value) cTools_reg_wr32((uint32_t)addr, value)
#else
#define reg_rd32(addr) (*addr)
#define reg_wr32(addr,value) (*addr = value)
#endif



//CP Tracer must know address (Destination Register) of STM unit to forward messages
// This must be set to the channel zero timestamp address.
const uint32_t STM_BaseAddress  = 0x20000000;
const uint32_t STM_ChResolution = 0x1000;
const uint32_t STM_Ch0MsgEnd = 0x20000800;

//PSC_Base & register definitions
#define PSC_BASE 0x02350000

#define CPT_POWER_DOMAIN        1
#define CPT_POWER_TRANSITION    (1 << CPT_POWER_DOMAIN)
#define CPT_LPCS_NUM            5
#define CPT_TIMEOUT_CNT         0xFFFF
#define CPT_PDSTAT_ON           0x1
#define CPT_PDSTAT_POR_VALID    0x300
#define CPT_MDSTAT_ENABLED      0x3

//Common Power and Domain definitions
#define PSC_PTCMD      ((volatile uint32_t *) (PSC_BASE+ 0x120))
#define PSC_PTSTAT     ((volatile uint32_t *) (PSC_BASE+ 0x128))

#define PSC_PDSTAT(n)  ((volatile uint32_t *) (PSC_BASE+0x200+4*n))
#define PSC_PDCTL(n)   ((volatile uint32_t *) (PSC_BASE+0x300+4*n))
#define PSC_PDCFG(n)   ((volatile uint32_t *) (PSC_BASE+0x400+4*n))

#define PSC_MDCFG(n)   ((volatile uint32_t *) (PSC_BASE+0x600+4*n))
#define PSC_MDSTAT(n)  ((volatile uint32_t *) (PSC_BASE+0x800+4*n))
#define PSC_MDCTL(n)   ((volatile uint32_t *) (PSC_BASE+0xA00+4*n))

//CP Tracer register offsets
#define CPT_REGOFF_Identification           0x00  
#define CPT_REGOFF_TransactionQualifier     0x04
#define CPT_REGOFF_ModuleControlAndStatus   0x08
#define CPT_REGOFF_SlidingTimeWindow        0x0C
#define CPT_REGOFF_BusMasterSelect_TP0_A    0x10
#define CPT_REGOFF_BusMasterSelect_TP0_B    0x14
#define CPT_REGOFF_BusMasterSelect_TP0_C    0x18
#define CPT_REGOFF_BusMasterSelect_TP0_D    0x1C
#define CPT_REGOFF_BusMasterSelect_TP1_A    0x20
#define CPT_REGOFF_BusMasterSelect_TP1_B    0x24
#define CPT_REGOFF_BusMasterSelect_TP1_C    0x28
#define CPT_REGOFF_BusMasterSelect_TP1_D    0x2C
#define CPT_REGOFF_EndAddress               0x30
#define CPT_REGOFF_StartAddress             0x34
#define CPT_REGOFF_AccessStatus             0x38
#define CPT_REGOFF_AccessStatusPacing       0x3C
#define CPT_REGOFF_AddressMask              0x40
#define CPT_REGOFF_DestinationAddress       0x44
#define CPT_REGOFF_MessagePriority          0x48
#define CPT_REGOFF_Ownership                0x4c
#define CPT_REGOFF_ThroughPut0              0x50
#define CPT_REGOFF_ThroughPut1              0x54
#define CPT_REGOFF_AccumulatedWaitTime      0x58
#define CPT_REGOFF_NumberOfGrants           0x5c
#define CPT_REGOFF_InterruptRawStatus       0x60
#define CPT_REGOFF_InterruptMaskStatus      0x64
#define CPT_REGOFF_InterruptMaskSet         0x68
#define CPT_REGOFF_InterruptMaskClear       0x6C
#define CPT_REGOFF_InterruptEOI             0x70

#define CPT_REGSPACE_BYTES                  128


// CP Tracer register definitions
// Note that ba may be passed as an unsigned int * 
#define CPT_ID_REG(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_Identification ))
#define CPT_QUAL_REG(ba)                ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_TransactionQualifier ))
#define CPT_MODCTL_REG(ba)              ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_ModuleControlAndStatus ))
#define CPT_STWINDOW_REG(ba)            ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_SlidingTimeWindow))
#define CPT_MSTSELA_TP0_REGIndex(ba, r) ((volatile uint32_t *)((uint32_t)(ba) + ((r) * 4) + CPT_REGOFF_BusMasterSelect_TP0_A))
#define CPT_MSTSELA_TP0_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP0_A))
#define CPT_MSTSELB_TP0_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP0_B))
#define CPT_MSTSELC_TP0_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP0_C))
#define CPT_MSTSELD_TP0_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP0_D))
#define CPT_MSTSELA_TP1_REGIndex(ba, r) ((volatile uint32_t *)((uint32_t)(ba) + ((r) * 4) + CPT_REGOFF_BusMasterSelect_TP1_A))
#define CPT_MSTSELA_TP1_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP1_A))
#define CPT_MSTSELB_TP1_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP1_B))
#define CPT_MSTSELC_TP1_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP1_C))
#define CPT_MSTSELD_TP1_REG(ba)         ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_BusMasterSelect_TP1_D))
#define CPT_ENDADDR(ba)                 ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_EndAddress ))
#define CPT_STARTADDR(ba)               ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_StartAddress ))
#define CPT_ACCSTATUS(ba)               ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_AccessStatus ))
#define CPT_PACING(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_AccessStatusPacing ))
#define CPT_ADDRMASK(ba)                ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_AddressMask ))
#define CPT_DSTADDR(ba)                 ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_DestinationAddress ))
#define CPT_MSGPRI(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_MessagePriority ))
#define CPT_OWNERSHIP(ba)               ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_Ownership ))
#define CPT_TP0(ba)                     ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_ThroughPut0 ))
#define CPT_TP1(ba)                     ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_ThroughPut1 ))
#define CPT_ACCUMWAIT(ba)               ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_AccumulatedWaitTime ))
#define CPT_GRANTCNT(ba)                ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_NumberOfGrants ))
#define CPT_INTRAW(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_InterruptRawStatus ))
#define CPT_INTSTATUS(ba)               ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_InterruptMaskStatus ))
#define CPT_INTSET(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_InterruptMaskSet ))
#define CPT_INTCLR(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_InterruptMaskClear ))
#define CPT_INTEOI(ba)                  ((volatile uint32_t *)((uint32_t)(ba) + CPT_REGOFF_InterruptEOI ))

// Type Key
// R - read only
// W - write only
// R/W - read or write
// WC - Write-to-clear  (Write 1 to clear bit, write 0 has no effect)
// WS - Write-to-set    (Write 1 to set bit, write 0 has no effect)
// WD - Write-to-decrement field (Decrement register field by value written - reads have no effect)

//Identification and Version Register - Read only
//Bits      Type    Reset   Name                Description 
//31        R       1       ID Scheme           Id register format
//29:28     X       0       Reserved
//27:16     R       0xE89   ID
//15:11     R       0       RTL Version         Changes with Spec changes and bug fixes. Resets to 0 when
//                                              Major or Minor numbers updated.
//10:8      R       0x1     Major Version       Major feature addition, does not change for bug fixes or
//                                              feature scaling (ex: size of a RAM)
//7:6       R       0       Custom Version      Special version for a specific device
//5:0       R       0x2     Minor Version       Changes with feature scaling (ex: size of a RAM), does not
//                                              change for bug fixes 
// Note - one may be tempted to replace reg with  CPT_ID_REG(ba) but in the code this would not allow
//        reading the register once and then checking multiple bits 
#define CPT_GET_IDREG_MINOR(reg)             ( reg & 0x1F )
#define CPT_GET_IDREG_CUSTOM(reg)            ( ( reg & 0xC0 ) >> 6 )
#define CPT_GET_IDREG_MAJOR(reg)             ( ( reg & 0xE00 ) >> 8 )     
#define CPT_GET_IDREG_RTL(reg)               ( ( reg & 0xF800 ) >> 11 ) 
#define CPT_GET_IDREG_FUNC(reg)              ( ( reg & 0x0FFF0000 ) >> 16 )
#define CPT_GET_IDREG_IDSCH(reg)             ( ( reg & 0x80000000 ) >> 31 )

//Transaction Qualifier Register - All bits are read/write except Emu_status bit
//Bits      Type    Reset   Name                Description    
//31:28     X               Reserved
//27        R/W     0       Emu0_trig           1: emu0_out is asserted when event B has occurred
//                                              0: emu0_out is not asserted by CP_tracer
//26        R/W     0       Emu1_trig           1: emu1_out is asserted when qualified event B has occurred
//                                              0: emu1_out is not asserted by CP_tracer
//25        R       1       Emu_status          Set by the hardware and read only
//                                              1: CP_tracer is enabled to monitor CBA events
//                                              0: CP_tracer is not enabled to monitor CBA events.
//                                              When Qualif_EMU = 0, EMU_status is set to 1.
//                                              When Qualif_EMU= 1, EMU_status is set to 1 when EMU0 is asserted. EMU_status is set to 0 when EMU1 is asserted.
//24        R/W     0       Qualif_EMU          1: only the CBA events happening between assertion of EMU0 and assertion of EMU1 are sent to qualifier.
//                                              0: all CBA events sent to qualifier
//23:20     R/W     0       Qualif_dtype_trig   0bxxx0: Capture event Bs with dtype 0 (CPU Data Access) for emu0/1_out triggering
//                                              0bxxx1: Exclude event Bs with dtype 0 (CPU Data Access) from emu0/1_out triggering
//                                              0bxx0x: Capture event Bs with dtype 1 (CPU Instruction Access) for emu0/1_out triggering
//                                              0bxx1x: Exclude event Bs with dtype 1 (CPU Instruction Access) from emu0/1_out triggering
//                                              0bx0xx: Capture event Bs with dtype 2 (DMA  Access) for emu0/1_out triggering 
//                                              0bx1xx: Exclude event Bs with dtype 2 (DMA Access) from emu0/1_out triggering
//                                              0b0xxx: Capture event Bs with dtype 3 (RESERVED) for emu0/1_out triggering
//                                              0b1xxx: Exclude event Bs with dtype 3 (RESERVED) from emu0/1_out triggering
//19:16     R/W     0       Qualif_dtype_TH1    0bxxx0: Capture event Bs with dtype 0 (CPU Data Access) for throughput 1 calculation
//                                              0bxxx1: Exclude event Bs with dtype 0 (CPU Data Access) from throughput 1 calculation 
//                                              0bxx0x: Capture event Bs with dtype 1 (CPU Instruction Access) for throughput 1 calculation
//                                              0bxx1x: Exclude event Bs with dtype 1 (CPU Instruction Access) from throughput 1 calculation
//                                              0bx0xx: Capture event Bs with dtype 2 (DMA  Access) for throughput 1 calculation
//                                              0bx1xx: Exclude event Bs with dtype 2 (DMA Access) from throughput 1 calculation
//                                              0b0xxx: Capture event Bs with dtype 3 (RESERVED) for throughput 1 calculation
//                                              0b1xxx: Exclude event Bs with dtype 3 (RESERVED) from throughput 1 calculation
//15:12     R/W     0       Qualif_dtype_TH0    0bxxx0: Capture event Bs with dtype 0 (CPU Data Access) for throughput 0 calculation
//                                              0bxxx1: Exclude event Bs with dtype 0 (CPU Data Access) from throughput 0 calculation 
//                                              0bxx0x: Capture event Bs with dtype 1 (CPU Instruction Access) for throughput 0 calculation
//                                              0bxx1x: Exclude event Bs with dtype 1 (CPU Instruction Access) from throughput 0 calculation
//                                              0bx0xx: Capture event Bs with dtype 2 (DMA  Access) for throughput 0 calculation
//                                              0bx1xx: Exclude event Bs with dtype 2 (DMA Access) from throughput 0 calculation
//                                              0b0xxx: Capture event Bs with dtype 3 (RESERVED) for throughput 0 calculation
//                                              0b1xxx: Exclude event Bs with dtype 3 (RESERVED) from throughput 0 calculation
//11:8      R/W     0       Qualif_dtype_trace  0bxxx0: Capture event Bs with dtype 0 (CPU Data Access) for event B export
//                                              0bxxx1: Exclude event Bs with dtype 0 (CPU Data Access) from event B export
//                                              0bxx0x: Capture event Bs with dtype 1 (CPU Instruction Access) for event B export
//                                              0bxx1x: Exclude event Bs with dtype 1 (CPU Instruction Access) from event B export
//                                              0bx0xx: Capture event Bs with dtype 2 (DMA  Access) for event B export
//                                              0bx1xx: Exclude event Bs with dtype 2 (DMA Access) from event B export
//                                              0b0xxx: Capture event Bs with dtype 3 (RESERVED) for event B export
//                                              0b1xxx: Exclude event Bs with dtype 3 (RESERVED) from event B export
//7:6       R/W     0       Qualif_trig         0b11: Capture both read and write transactions for emu0/1_out trigger
//                                              0b01: Only capture read transactions for emu0/1_out trigger
//                                              0b10: Only capture write transactions for emu0/1_out trigger
//                                              0b00: Do not capture either read or write transaction for emu0/1_out trigger (Disables emu0/1_out triggering)
//5:4       R/W     0       Qualif_TH1          0b11: Capture both read and write transactions for  throughput 1
//                                              0b01: Only capture read transactions for  throughput 1
//                                              0b10: Only capture write transactions for  throughput 1
//                                              0b00: Do not capture either read or write transaction for throughput 1 (Disables throughput count 1)
//3:2       R/W     0       Qualif_TH0          0b11: Capture both read and write transactions for  throughput 0
//                                              0b01: Only capture read transactions for  throughput 0
//                                              0b10: Only capture write transactions for  throughput 0
//                                              0b00: Do not capture either read or write transaction for throughput 0 (Disables throughput count 0)
//1:0       R/W     0       Qualif_trace        0b11: Capture both read and write transactions for event B trace export
//                                              0b01: Only capture read transactions for event B trace export
//                                              0b10: Only capture write transactions for event B trace export
//                                              0b00: Do not capture either read or write transaction for event B trace export
#define CPT_GET_QUAL_TRIGOUT(reg)               ( ( reg & 0xC0000000 ) >> 26 )
#define CPT_GET_QUAL_TRIGSTAT(reg)              ( ( reg & 0x02000000 ) >> 25 )
#define CPT_GET_QUAL_TRIGIN(reg)                ( ( reg & 0x01000000 ) >> 24 )
#define CPT_GET_QUAL_TRIGOUTDTYPE(reg)          ( ( reg & 0x00F00000 ) >> 20 )
#define CPT_GET_QUAL_TH1DTYPE(reg)              ( ( reg & 0x000F0000 ) >> 16 )
#define CPT_GET_QUAL_TH0DTYPE(reg)              ( ( reg & 0x0000F000 ) >> 12 )
#define CPT_GET_QUAL_EVTBDTYPE(reg)             ( ( reg & 0x00000F00 ) >> 8 )
#define CPT_GET_QUAL_TRIGOUTRW(reg)             ( ( reg & 0x000000C0 ) >> 6 )
#define CPT_GET_QUAL_TH1RW(reg)                 ( ( reg & 0x00000030 ) >> 4 )
#define CPT_GET_QUAL_TH0RW(reg)                 ( ( reg & 0x0000000C ) >> 2 )
#define CPT_GET_QUAL_EVTBRW(reg)                ( reg & 0x3 )

#define CPT_SET_QUAL_TRIGOUT(qual)              ( ( qual & 0x3 ) << 26 )  
#define CPT_SET_QUAL_TRIGIN(qual)               ( ( qual & 1 ) << 24 )
#define CPT_SET_QUAL_TRIGOUTDTYPE(qual)         ( ( qual & 0xF ) << 20 )
#define CPT_SET_QUAL_TH1DTYPE(qual)             ( ( qual & 0xF ) << 16 )
#define CPT_SET_QUAL_TH0DTYPE(qual)             ( ( qual & 0xF ) << 12 ) 
#define CPT_SET_QUAL_EVTBDTYPE(qual)            ( ( qual & 0xF ) << 8 ) 
#define CPT_SET_QUAL_TRIGOUTRW(qual)            ( ( qual & 0x3 ) << 6 )
#define CPT_SET_QUAL_TH1RW(qual)                ( ( qual & 0x3 ) << 4 )
#define CPT_SET_QUAL_TH0RW(qual)                ( ( qual & 0x3 ) << 2 )
#define CPT_SET_QUAL_EVTBRW(qual)               ( qual & 0x3 )

#define CPT_QUAL_TRIGOUT_MASK                   ( 0x3  << 26 )  
#define CPT_QUAL_TRIGIN_MASK                    ( 0x1  << 24 )
#define CPT_QUAL_TRIGOUTDTYPE_MASK              ( 0xF  << 20 )
#define CPT_QUAL_TH1DTYPE_MASK                  ( 0xF  << 16 )
#define CPT_QUAL_TH0DTYPE_MASK                  ( 0xF  << 12 ) 
#define CPT_QUAL_EVTBDTYPE_MASK                 ( 0xF  << 8 ) 
#define CPT_QUAL_TRIGOUTRW_MASK                 ( 0x3  << 6 )
#define CPT_QUAL_TH1RW_MASK                     ( 0x3  << 4 )
#define CPT_QUAL_TH0RW_MASK                     ( 0x3  << 2 )
#define CPT_QUAL_EVTBRW_MASK                    ( 0x3 )

#define CPT_SET_QUAL_ALLDISABLED                0x000000FF
#define CPT_TRIG_QUAL_MASK                      0x0DF000C0
#define CPT_TH1_QUAL_MASK                       0x000F0030
#define CPT_TH0_QUAL_MASK                       0x0000F00C
#define CPT_EVTB_QUAL_MASK                      0x00000F03

//Module Control and Status Register 
//Bits      Type    Reset   Name                Description
//31:16     R/W     0       Upper_address_bits  These are the upper address bits used for all address filtering.
//                                              They function as the MSBs [47:32] of both start address and end
//                                              address.  See the definition of the address_mode field for which
//                                              bits of this address are valid.  Only the valid bits as indicated
//                                              by the address_mode field are writable, all other bits are
//                                              read-only 0.
//15        R/W     0       Excl_addr_filter_en 1: Enable exclusive address filtering
//                                              0: Disable exclusive address filtering
//14        R/W     0       Acc_wait_cnt_en     1: Enables accumulated wait counter
//                                              0: Disables accumulated wait counter
//13        R/W     0       Num_grant_Cnt_en    1: Enables num grant counter
//                                              0: Disables num grant counter
//12:8      R/W     0       Export_select       0b00000: no export capability
//                                              0bxxxx1: export statistics message when the sliding time window is expired.
//                                              0bxxx1x: export event trace based on filtering for event B
//                                              0bxx1xx: export event trace for event C
//                                              0bx1xxx: export event trace for event E
//                                              0b1xxxx: export access status message based on pacing
//7:5       R       Tie-Off Address_mode        This register reflects the value of the address_mode_mode tie off and is read-only.  The value indicates how many address bits on the event_<slv>_arb_address are valid.  For example, if it indicates 32 bits then bits 31:0 are used for address filtering and all others must be tied to 0.  36 bits means 35:0 are used etc...
//                                                 0'b000: 32 bit address
//                                                 0'b001: 36 bit address
//                                                 0'b010: 40 bit address
//                                                 0'b011: 44 bit address
//                                                 0'b100: 48 bit address
//                                              Other values reserved.
//                                              
//                                              Note: event_<slv>_arb_address bits that  are not used must be tied to zero.
//                                              If they are non-zero, address filtering will not work correctly.
//4:0       R       Tie-Off SID                 Read only. It is used to differentiate the CP_tracer modules if a
//                                              device has multiple CP_tracers on chip. It is a tie-off value.                                                             
#define CPT_GET_MODCTL_UPPERADDRBITS(reg)       ( ( reg & 0xFFFF0000 ) >> 16 )
#define CPT_GET_MODCTL_ADDRFLTEN(reg)           ( ( reg & 0x00008000 ) >> 15 )
#define CPT_GET_MODCTL_WAITCNTEN(reg)           ( ( reg & 0x00004000 ) >> 14 )
#define CPT_GET_MODCTL_GRANTCNTEN(reg)          ( ( reg & 0x00002000 ) >> 13 )
#define CPT_GET_MODCTL_EXPSEL(reg)              ( ( reg & 0x00001F00 ) >> 8 )
#define CPT_GET_MODCTL_ADDRMODE(reg)            ( ( reg & 0x000000E0 ) >> 5 )
#define CPT_GET_MODCTL_SID(reg)                 ( reg & 0x1F )

#define CPT_SET_MODCTL_UPPERADDRBITS(cntl)       ( ( cntl & 0xFFFF ) << 16 )
#define CPT_SET_MODCTL_ADDRFLTEN(cntl)           ( ( cntl & 0x1 ) << 15 )
#define CPT_SET_MODCTL_WAITCNTEN(cntl)           ( ( cntl & 0x1 ) << 14 )
#define CPT_SET_MODCTL_GRANTCNTEN(cntl)          ( ( cntl & 0x1 ) << 13 )
#define CPT_SET_MODCTL_EXPSEL(cntl)              ( ( cntl & 0x1F ) << 8 )

#define CPT_SET_MODCTL_CNTEN(cntl)               ( ( cntl & 0x3 ) << 13 )

#define CPT_MODCTL_UPPERADDRBITS_MASK            ( 0xFFFF << 16)
#define CPT_MODCTL_ADDRFLTEN_MASK                ( 0x1 << 15 )
#define CPT_MODCTL_WAITCNTEN_MASK                ( 0x1 << 14 )
#define CPT_MODCTL_GRANTCNTEN_MASK               ( 0x1 << 13 )
#define CPT_MODCTL_EXPSEL_MASK                   ( 0x1F << 8 )

#define CPT_MODCTL_ADDRMODE_32_TEST(value)       ( 0xFFFFFFFF & value )
#define CPT_MODCTL_ADDRMODE_36_TEST(value)       ( 0xFFFFFFF0 & value )
#define CPT_MODCTL_ADDRMODE_40_TEST(value)       ( 0xFFFFFF00 & value )
#define CPT_MODCTL_ADDRMODE_44_TEST(value)       ( 0xFFFFF000 & value )
#define CPT_MODCTL_ADDRMODE_48_TEST(value)       ( 0xFFFF0000 & value )




#define CPT_SET_MODCTL_ALLDISABLED                0x0


//Sliding Time Window 
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0xFFFFFFFF  Sliding Time Window     Specifies a sliding window in terms of the CP_TRACER clock
//                                                      over which the statistics are to be collected. An interrupt
//                                                      is generated (if enabled) when the sliding time window expires. 
//                                                      *   Value is in cp_tracer clocks
//                                                      *   Counter starts as soon as a non-zero value is written to
//                                                          this register
//                                                      *   Writing a 0x00000000 to this register will disable the
//                                                          sliding time window counter
//                                                      *   Counter is disabled at reset despite having a non-zero value

//Master ID Select Register A for Throughput0 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp A  Master ID select Group A contains the master ID select for master
//                                                      ID 0 to 31. Bit 0 enables mstid 0, bit 1 enables mstid 1 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput0
//                                                      calculation. The selected mstid is also used to filter event B
//                                                      and E for event trace export.

#define CPT_SET_MASTER_SEL_NONE 0
#define CPT_SET_MASTER_SEL_ALL  0xFFFFFFFF
#define CPT_NUM_MASTER_SEL_REGS 4

//Master ID Select Register B for Throughput0 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp B  Master ID select Group A contains the master ID select for master
//                                                      ID 32 to 63. Bit 0 enables mstid 32, bit 1 enables mstid 33 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput0
//                                                      calculation. The selected mstid is also used to filter event B
//                                                      and E for event trace export.

//Master ID Select Register C for Throughput0 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp C  Master ID select Group A contains the master ID select for master
//                                                      ID 64 to 95. Bit 0 enables mstid 64, bit 1 enables mstid 65 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput0
//                                                      calculation. The selected mstid is also used to filter event B
//                                                      and E for event trace export.

//Master ID Select Register D for Throughput0 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp D  Master ID select Group A contains the master ID select for master
//                                                      ID 96 to 127. Bit 0 enables mstid 96, bit 1 enables mstid 97 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput0
//                                                      calculation. The selected mstid is also used to filter event B
//                                                      and E for event trace export.

//Master ID Select Register A for Throughput1 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp A  Master ID select Group A contains the master ID select for master
//                                                      ID 0 to 31. Bit 0 enables mstid 0, bit 1 enables mstid 1 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput0
//                                                      calculation.

//Master ID Select Register B for Throughput1 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp B  Master ID select Group A contains the master ID select for master
//                                                      ID 32 to 63. Bit 0 enables mstid 32, bit 1 enables mstid 33 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput1
//                                                      calculation.

//Master ID Select Register C for Throughput1 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp C  Master ID select Group A contains the master ID select for master
//                                                      ID 64 to 95. Bit 0 enables mstid 64, bit 1 enables mstid 65 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput1
//                                                      calculation.

//Master ID Select Register D for Throughput1 and Event Trace
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Master ID Select Grp D  Master ID select Group A contains the master ID select for master
//                                                      ID 96 to 127. Bit 0 enables mstid 96, bit 1 enables mstid 97 etc. 
//                                                      If enabled, the corresponding mstid is selected for throughput1
//                                                      calculation.

//End Address Register
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           End Address             Memory address. Combined with the start address register, it
//                                                      provides the address range for filtering function for both
//                                                      throughput statistics and event B export. Only the transactions
//                                                      within the address range between and including [start address,
//                                                      end address] will be captured for throughput.  For event trace
//                                                      export, the event which overlaps [start address, end address]
//                                                      will be captured to export.  When the end address is set to 0x0,
//                                                      it disables the address filtering function.  When the address
//                                                      filtering is disabled, the transactions are captured regardless
//                                                      of the memory range.

//Start Address Register
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Start Address           Memory address for the low address range.

//Access Status Register
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Access Status           Each bit is correspondent to one master event I/F.
//                                                      1: means a transaction arrived in that master event I/F
//                                                         during the previous time sliding window
//                                                      0: means no transactions arrived in that master event I/F
//                                                         during the previous time sliding window
//                                                      The access status is set by the hardware when event A arrives
//                                                      at the master event I/F. The access status is reset to 0x0 when
//                                                      the sliding window has expired.

//Access Pacing Register
//Bits      Type    Reset       Name                    Description
//31:0      R/W     0           Pacing                  0x000000: If export_select[4] is set and an access has occurred
//                                                                since the previous access status message was sent, 
//                                                                then the access status message is sent out whenever
//                                                                the sliding time window expires.
//                                                      0xXXXXXX: If this field is non-zero, export_select[4] is set,
//                                                                and an access has occurred since the previous access
//                                                                status message was sent, then the access status message
//                                                                is sent out as long as X cycles have occurred since the
//                                                                last message where X is the value of this field.
//                                                                NOTE: if the sliding time window expires, then the access
//                                                                status message will be sent out regardless of pacing,
//                                                                and the pacing counter will be reset.  If this value is
//                                                                greater than the sliding time window then the access
//                                                                status message will only be sent out whenever the
//                                                                sliding time window expires.

#define CPT_GET_PACING(reg)                         ( reg & 0x00FFFFFF )
#define CPT_SET_PACING(reg)                         ( reg & 0x00FFFFFF )

//Address Mask Register
//Bits      Type    Reset       Name                    Description
//5:0      R/W     0           Address Bits Select      Event messages export 10 bits of the address associated with the
//                                                      event.  This field indicates which 10 contiguous bits of the
//                                                      possible 48 bits are exported.  The value indicates the lowest bit
//                                                      and the next 9 bits are exported also:
//                                                      *   0  Bits 09:0 are exported
//                                                      *   1  Bits 10:1 are exported
//                                                      *   2  Bits 11:2 are exported
//                                                      *   3-36 - ..
//                                                      *   37  Bits 46:37 are exported
//                                                      *   38  Bits 47:38 are exported
//                                                      *   63-38  Reserved.  Behavior undefined.
//
#define CPT_GET_ADDRESSMASK(reg)                    ( reg & 0x1F )
#define CPT_SET_ADDRESSMASK(mask)                   ( mask & 0x1F )

//Destination Address Register
//Bits      Type    Reset       Name                    Description
//31:2      R/W     0           Destination Address     Destination Address bits 31:2 for the outgoing VBUSP
//                                                      transactions from the master interface.  The address must be word
//                                                      aligned, so the 2 LSBS (1:0) are always 0            
//1:0       R       0           Reserved

//This register simply needs to be set to the address of the debug_SS_STM module 

//Message Priority Register
//Bits      Type    Reset       Name                    Description
//31:15     R       0           Reserved
//14:12     R/W     0x7         Stat_msg_prio           Vbusp priority and epriority associated with statistics messages
//11:9      R/W     0x7         Event_e_msg_prio        Vbusp priority and epriority associated with event e message export
//8:6       R/W     0x7         Event_c_msg_prio        Vbusp priority and epriority associated with event c message export
//5:3       R/W     0x7         Event_b_msg_prio        Vbusp priority and epriority associated with event b message export
//2:0       R/W     0x7         Access_msg_prio         Vbusp priority and epriority associated with access status message export
#define CPT_GET_MSGPRI_STAT(reg)                  ( ( reg & 0x00007000 ) >> 12 )
#define CPT_GET_MSGPRI_EVTE(reg)                  ( ( reg & 0x00000E00 ) >> 9 )
#define CPT_GET_MSGPRI_EVTB(reg)                  ( ( reg & 0x00000038 ) >> 3 )
#define CPT_GET_MSGPRI_ACC(reg)                   ( reg & 0x7 )
#define CPT_SET_MSGPRI_STAT(msgpri)               ( ( msgpri & 0x7 ) << 12 )
#define CPT_SET_MSGPRI_EVTE(msgpri)               ( ( msgpri & 7 ) << 9 )
#define CPT_SET_MSGPRI_EVTC(msgpri)               ( ( msgpri & 7 ) << 6 ) 
#define CPT_SET_MSGPRI_EVTB(msgpri)               ( ( msgpri & 7 ) << 3 )
#define CPT_SET_MSGPRI_ACC(msgpri)                ( msgpri & 0x7 ) 


//Ownership Register
//Bits      Type    Reset       Name                    Description
//31:1      R       0           Reserved
//0         R/W     0           Ownership               This bit can be used by software to indicate that this cp_tracer
//                                                      is owned by a process for configuration.  It is simply a
//                                                      read/write register that is read as the last value written.
//                                                      It provides no interlock capability so software is responsible
//                                                      for implementing a semaphore using this bit and handle any possible
//                                                      race conditions that may occur.
#define CPT_GET_OWNERSHIP(reg)                   ( reg & 0x1 )
#define CPT_SET_OWNERSHIP                        ( 0x1 )
#define CPT_CLR_OWNERSHIP                        ( 0x0 )

//Throughput0 Register
//Bits      Type    Reset       Name                    Description
//31:24     R       0           Reserved
//23:0      R       0           Throughput              The accumulated throughput during the last sliding time window
//                                                      based on all throughput0 qualifiers and filters.  
#define CPT_GET_THROUGHPUTCNT0(reg)             ( reg & 0xFFFFFF )

//Throughput1 Register
//Bits      Type    Reset       Name                    Description
//31:24     R       0           Reserved
//23:0      R       0           Throughput              The accumulated throughput during the last sliding time window
//                                                      based on all throughput1 qualifiers and filters.
#define CPT_GET_THROUGHPUTCNT1(reg)             ( reg & 0xFFFFFF )


//Accumulative Waiting Time Register
//Bits      Type    Reset       Name                    Description
//31:24     R       0           Reserved
//23:0      R       0           Accum Waiting Time      The accumulative time that arbiter is busy during a sliding
//                                                      time window. It is set when the sliding time window is expired.
#define CPT_GET_WAITTIMECNT(reg)                ( reg & 0xFFFFFF )


//Number of Grants Register
//Bits      Type    Reset       Name                    Description
//31:24     R       0           Reserved
//23:0      R       0           Number of Grants        The number of times that arbiter made grants during a sliding
//                                                      timing window. It is set when the sliding timing window is expired.
#define CPT_GET_GRANTCNT(reg)                   ( reg & 0xFFFFFF )

//5.25  Interrupt Raw Status Register
//Bits      Type    Reset       Name                    Description
//31:1      R       0           Reserved
//0         R/WS     0          Interrupt_Raw           This bit is set when the sliding time window expires regardless
//                                                      of the interrupt mask set bit in the Interrupt Mask Set register.
//                                                      This bit remains set until the host CPU clears it. Writing a 1 to
//                                                      this bit sets it and writing a 0 has no effect.
#define CPT_GET_RAWINTSTATE(reg)                 ( reg & 0x1 )
#define CPT_SET_RAWINTSTATE                     ( 0x1 )

//Interrupt Masked Status Register
//Bits      Type    Reset       Name                    Description
//31:1      R       0           Reserved
//0         R/WC     0          Interrupt_Masked        This bit is set when the sliding time window expires and the
//                                                      interrupt mask set bit in the Interrupt Mask Set register is set.
//                                                      This bit remains set until the host CPU clears the raw status bit.
//                                                      This bit also drives the module output port. Writing a 1 to this
//                                                      bit clears the raw status (and therefore this status) and writing
//                                                      a 0 has no effect.
#define CPT_GET_MASKINTSTATE(reg)                 ( reg & 0x1 )
#define CPT_CLR_MASKINTSTATE                      ( 0x1 ) 


//Interrupt Masked Set Register
//Bits      Type    Reset       Name                    Description
//31:1      R       0           Reserved
//0         R/WS     0          Interrupt Mask          When set to 1, this bit enables the Interrupt_Masked Status bit.
//                                                      Writing a 1 to this bit sets it and writing a 0 has no effect.
#define CPT_GET_MASKINTENABLE(reg)                ( reg & 0x1 )
#define CPT_SET_MASKINTENABLE                     ( 0x1 )

//Interrupt Masked Clear Register
//Bits      Type    Reset       Name                    Description
//31:1      R       0           Reserved
//0         R/WC     0          Interrupt Mask          Writing a 1 to this bit clears the Interrupt Mask and disables
//                                                      the Interrupt_Masked bit. Writing a 0 has no effect
#define CPT_GET_MASKINTDISABLE(reg)               ( reg & 0x1 )
#define CPT_SET_MASKINTDISABLE                    ( 0x1 )

#endif // _DOXYGEN_IGNORE
#endif //__CPT_COMMON_H

#ifdef __cplusplus
}
#endif
