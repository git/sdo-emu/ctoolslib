/*
 * CPTHelper.h
 *
 * cTools Helper public functions definitions. 
 *
 * Copyright (C) 2010,2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef _CPTHELPER_H
#define _CPTHELPER_H

//Using forward slashes in the relative serach path for linux compatibility

#ifdef RUNTIME_DEVICE_SELECT
#include "CPTLib.h"
#define eCPT_MasterID int
#else
#include "../../CPTLib/include/CPTLib.h"
#endif

/*  CPTCfg_TotalProfile

    Total Bandwidth Profile � Profile of a single master as a % of total slave activity

    Configure an opened CP Tracer for the Total Bandwidth Profiling. This configuration
    utilizes one throughput counter to be configured for a single master, while the
    other throughput counter is configured for all masters. Qualifiers are applied to both
    throughput counters. All statistic counters are enabled(throughput, wait and grant). The
    following calculations are automatically processed from the raw data on the host by td.exe 
    (if collecting the data with the on-chip Embedded Trace Buffer -ETB ) or by CCS's Tracer Analyzer:
    
    - % of total slave activity utilized by selected masters
    - Slave Bus Bandwidth utilized by selected master
    - Average Access Size of slave transactions
    - Bus Utilization
    - Contention Percentage
    - Minimum Average Latency

    CP Tracer statistic message generation is started. The CPTLib functions CPT_ModuleDisable()
    and CPT_ModuleEnable() may be used to suspend/restart CP Tracer data message generation.
    
    If address filtering is required for the Throughput counters, the client should call
    CPTLib's CPT_CfgAddrFilter() function prior to calling CPTHelp_TotalCfg().    

    param[in] pCPT_Handle       Pointer to a CPT_Handle provided by CPT_OpenModule().
    param[in] CPT_MasterID      ::eCPT_MasterID provides master id selection for Throughput Counter 0.
    param[in] pCPT_TPCntQual    Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides New Request Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for both Throughput Counters. 
    return ::ePMI_Error 
    
    Notes:
    - It's recommended that when you open the CP Tracer Library that you do so with 
    CPT_CfgOptions.CPUClockRateMhz set accurately.     
*/
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID,
                                CPT_Qualifiers const * const pCPT_TPCntQual, CPT_TrigQualifiers * pCPT_TrigQual);
#else
eCPT_Error CPTCfg_TotalProfile( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID,
                                CPT_Qualifiers const * const pCPT_TPCntQual);
#endif

/*  CPTCfg_TotalProfileMult

    Total Bandwidth Profile Multiple � Profile of multiple masters (summed) as a % of total slave activity
    
    Functionally this is the same as CPTCfg_TotalProfile() except that multiple masters can be selected.
    
    param[in] pCPT_Handle       Pointer to a CPT_Handle provided by CPT_OpenModule().
    param[in] CPT_MasterID[]    Array of ::eCPT_MasterID provides master id selection for Throughput Counter 0.
    param[in] pCPT_TPCntQual    Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides New Request Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for both Throughput Counters. 
    return ::ePMI_Error  
*/
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_TotalProfileMult( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID[],
                                    int CPT_MasterIDCnt, CPT_Qualifiers const * const pCPT_TPCntQual, CPT_TrigQualifiers * pCPT_TrigQual);
#else
eCPT_Error CPTCfg_TotalProfileMult( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[],
                                    int CPT_MasterIDCnt, CPT_Qualifiers const * const pCPT_TPCntQual);
#endif

/*  CPTCfg_MasterProfile

    Master Bandwidth Profile � Profile of two master(s)
    
    Configure an opened CP Tracer for Master Bandwidth Profiling. This configuration 
    allows each of the two throughput counter to be configured for a single master id. Master 
    dependent qualifiers are applied to the throughput counters. All statistic counters are
    enabled(throughput, wait and grant). The following calculations are automatically processed
    from the raw data on the host by td.exe (if collecting the data with the on-chip Embedded Trace
    Buffer -ETB ) or by CCS's Tracer Analyzer:
    
    - Slave Bus Bandwidth utilized by master group 0
    - Slave Bus Bandwidth utilized by master group 1 
    - Bus Utilization
    - Contention Percentage
    - Minimum Average Latency

    CP Tracer statistic message generation is started. The CPTLib functions CPT_ModuleDisable()
    and CPT_ModuleEnable() may be used to suspend/restart CP Tracer data message generation.
    
    If address filtering is required for the Throughput counters, the client should call
    CPTLib's CPT_CfgAddrFilter() function prior to calling CPTHelp_StdCfg().    

    param[in] pCPT_Handle       Pointer to a CPT_Handle provided by CPT_OpenModule().
    param[in] CPT_MasterID0      ::eCPT_MasterID provides master id selection for Throughput Counter 0.
    param[in] pCPT_TPCnt0Qual    Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides New Request Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for Throughput Counter 0. 
    param[in] CPT_MasterID1      ::eCPT_MasterID provides master id selection for Throughput Counter 1.
    param[in] pCPT_TPCnt1Qual    Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides New Request Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for Throughput Counter 1.     
    return ::ePMI_Error  
    
    Notes:
    - It's recommended that when you open the CP Tracer Library that you do so with 
    CPT_CfgOptions.CPUClockRateMhz set accurately.     
*/
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle,
                                 const int CPT_MasterID0, CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                 const int CPT_MasterID1, CPT_Qualifiers const * const pCPT_TPCnt1Qual,
                                 CPT_TrigQualifiers * pCPT_TrigQual);
#else
eCPT_Error CPTCfg_MasterProfile( CPT_Handle_Pntr const pCPT_Handle, 
                                 const eCPT_MasterID CPT_MasterID0, CPT_Qualifiers const * const pCPT_TPCnt0Qual, 
                                 const eCPT_MasterID CPT_MasterID1, CPT_Qualifiers const * const pCPT_TPCnt1Qual);
#endif
/*  CPTCfg_Events
    
    Configure an opened CP Tracer for event generation for a single master. Events are Event B (New Request),
    Event C (Last write events) and Event E (Last read events). The master selection and qualifier filters
    only apply to Event B and E, not C.   
   
    CP Tracer statistic message generation is started. The CPTLib functions CPT_ModuleDisable()
    and CPT_ModuleEnable() may be used to suspend/restart CP Tracer data message generation.
    
    If address filtering is required for Event B and E, the client should call
    CPTLib's CPT_CfgAddrFilter() function prior to calling CPTHelp_StdCfg().    

    param[in] pCPT_Handle       Pointer to a CPT_Handle provided by CPT_OpenModule().
    param[in] CPT_MasterID      ::eCPT_MasterID provides master id selection for Event B (New Request)
                                and Event E (Last Read).
    param[in] CPT_MasterState   ::eCPT_MasterState designates the CPT_MasterID type. For this
                                function only the following selections are valid:
                                    eCPT_Mstr_Enable (used to select and enable a single master out of a group)
                                    eCPT_Mstr_Enable_Grp (typical for selecting and enabling all masters of a group, and signle masters)
                                    eCPT_Mstr_EnableAll (used to select and enable all masters)
                                Any other eCPT_MasterState values will cause a eCPT_Error_Invalid_Parameter error to be returned.                                 
    param[in] CPT_MsgSelects    ::eCPT_MsgSelects designates the enabled event messages
                                enables. For this function the following selections are valid:
                                    eCPT_MsgSelect_Event_NewReq (New Request - Event B)
                                    eCPT_MsgSelect_Event_LastWrt (Last Write - Event C)
                                    eCPT_MsgSelect_Event_LastRd (Last Read - Event E)
                                Any others eCPT_MsgSelects values will cause a eCPT_Error_Invalid_Parameter error to be returned.
                                
    param[in] pCPT_TPEventQual   Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides Event B (New Request) Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for Events B and E. 
    return ::ePMI_Error   
    
*/
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID[], int CPT_MasterIDCnt,
                          const eCPT_MsgSelects CPT_MsgEnables, CPT_Qualifiers const * const pCPT_TPEventQual,
                          CPT_TrigQualifiers * pCPT_TrigQual);
#else
eCPT_Error CPTCfg_Events( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID[], int CPT_MasterIDCnt, 
                          const eCPT_MsgSelects CPT_MsgSelects, CPT_Qualifiers const * const pCPT_TPEventQual);
#endif
/*  CPTCfg_SystemProfile

    Total Bandwidth Profile Multiple � Profile of bandwidth or latency across all masters
    
    Configure an opened CP Tracer for the System Bandwidth Profiling. This configuration
    utilizes the second throughput counter to be configured for all masters. Qualifiers are applied to the
    throughput counter. The Slave Bus Bandwidth utilized or the Slave Bus Latentcy 
    are automatically processed from the raw data on the host by td.exe (if collecting the data with the
    on-chip Embedded Trace Buffer -ETB ) or by CCS's Tracer Analyzer:
    
    CP Tracer statistic message generation is NOT enabled, so that this function can be called multiple
    times in a loop to enable multiple CP Tracers.
    
    If address filtering is required for the Throughput counters, the client should call
    CPTLib's CPT_CfgAddrFilter() function prior to calling CPTHelp_TotalCfg().    
    
    param[in] pCPT_Handle       Pointer to a CPT_Handle provided by CPT_OpenModule().
    param[in] pCPT_TPCntQual    Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure
                                 provides New Request Data Qualifiers (CPU Data, CPU Inst,
                                 DMA) and read/write transaction qualifiers for both Throughput Counters. 
    return ::ePMI_Error  
*/
#ifdef RUNTIME_DEVICE_SELECT
eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle,
                                 CPT_Qualifiers const * const pCPT_TPCntQual,
                                 CPT_TrigQualifiers * pCPT_TrigQual);
#else
eCPT_Error CPTCfg_SystemProfile( CPT_Handle_Pntr const pCPT_Handle, CPT_Qualifiers const * const pCPT_TPCntQual);
#endif
#endif
