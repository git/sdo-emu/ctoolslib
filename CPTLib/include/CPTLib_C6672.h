#ifndef __CPTLIBRARY_C6672_H
#define __CPTLIBRARY_C6672_H
/*
 * CPTLib_C6672.h
 *
 * Common Platform (CP) Tracer Library C6672 device specific definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib_C6672.h
    C6672 specific CP Tracer modules definitions
*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCPT_ModID
    CP Tracer module ids
*/

// These are in SID order               
typedef enum {  eCPT_MSMC_0,        /*!< CP Tracer MSMC 0 module*/
                eCPT_MSMC_1,        /*!< CP Tracer MSMC 1 module*/
                eCPT_MSMC_2,        /*!< CP Tracer MSMC 2 module*/
                eCPT_MSMC_3,        /*!< CP Tracer MSMC 3 module*/
                eCPT_QM_MST,        /*!< CP Tracer Queue Manager Master module*/
                eCPT_DDR,           /*!< CP Tracer DDR module*/
                eCPT_SM,            /*!< CP Tracer Semaphore module*/
                eCPT_QM_CFG,        /*!< CP Tracer Queue Manager Priority module*/
                eCPT_SCR3_CFG,      /*!< CP Tracer SCR3 Configuration module*/
                eCPT_L2_0,          /*!< CP Tracer L2 0 Memory Controller module*/
                eCPT_L2_1,          /*!< CP Tracer L2 1 Memory Controller module*/
                eCPT_ModID_Last
               } eCPT_ModID;        

/*! \par eCPT_MasterID
    CP Tracer master ids
    
    The following table defines the list of masters that can be enabled for throughput counting and 
    New Request events.
    
    Note that some masters consist of a group of IDs designated with "_GrpN" suffix. For most situations 
    enabling the entire group rather than a single group is the typical use case. See CPT_CfgMaster()
    for details.
	
	Also note that master ids 128-144 correspond to the CP tracer masters. These 
	CP tracer masters are connected to the STM module via a private interconnect. We intentionally did 
	not define these master IDs, because the transactions from these masters have no significance from 
	an application SW point of view. Also, the transactions from these masters cannot be traced at any 
	available CP tracers.
                                                                                                                       
    \par <DQR> Data Qualifier Restriction for certain Masters
    The dtype qualifier for all non-MSMC CP Tracers is tied off to the DMA value. This means that for these CP Tracers, if you
    exclude DMA cycles (see ::eCPT_SrcQual) all data accesses are filtered.
    
*/

// Master IDs 
typedef enum { 
	            eCPT_MID_GEM0,                      /*!< GEM0 */
	            eCPT_MID_GEM1,                      /*!< GEM1 */
				eCPT_MID_reserved2,
				eCPT_MID_reserved3,
				eCPT_MID_reserved4,
				eCPT_MID_reserved5,
				eCPT_MID_reserved6,
				eCPT_MID_reserved7,
				eCPT_MID_GEM0_CFG,                  /*!< GEM0 CFG */
				eCPT_MID_GEM1_CFG,                  /*!< GEM1 CFG */
				eCPT_MID_reserved10,
				eCPT_MID_reserved11,
				eCPT_MID_reserved12,
				eCPT_MID_reserved13,
				eCPT_MID_reserved14,
				eCPT_MID_reserved15,
                eCPT_MID_EDMA0_TC0_RD,              /*!< EDMA0 TC0 Read */
                eCPT_MID_EDMA0_TC0_WR,              /*!< EDMA0 TC0 Write */
                eCPT_MID_EDMA0_TC1_RD,              /*!< EDMA0 TC1 Read */
                eCPT_MID_EDMA0_TC1_WR,              /*!< EDMA0 TC1 Write */
                eCPT_MID_EDMA1_TC0_RD,              /*!< EDMA1 TC0 Read */
                eCPT_MID_EDMA1_TC0_WR,              /*!< EDMA1 TC0 Write */
                eCPT_MID_EDMA1_TC1_RD,              /*!< EDMA1 TC1 Read */
                eCPT_MID_EDMA1_TC1_WR,              /*!< EDMA1 TC1 Write */                
                eCPT_MID_EDMA1_TC2_RD,              /*!< EDMA1 TC2 Read */
                eCPT_MID_EDMA1_TC2_WR,              /*!< EDMA1 TC2 Write */
                eCPT_MID_EDMA1_TC3_RD,              /*!< EDMA1 TC3 Read */
                eCPT_MID_EDMA1_TC3_WR,              /*!< EDMA1 TC3 Write */
                eCPT_MID_EDMA2_TC0_RD,              /*!< EDMA2 TC0 Read */
                eCPT_MID_EDMA2_TC0_WR,              /*!< EDMA2 TC0 Write */
                eCPT_MID_EDMA2_TC1_RD,              /*!< EDMA2 TC1 Read */
                eCPT_MID_EDMA2_TC1_WR,              /*!< EDMA2 TC1 Write */                
                eCPT_MID_EDMA2_TC2_RD,              /*!< EDMA2 TC2 Read */
                eCPT_MID_EDMA2_TC2_WR,              /*!< EDMA2 TC2 Write */
                eCPT_MID_EDMA2_TC3_RD,              /*!< EDMA2 TC3 Read */
                eCPT_MID_EDMA2_TC3_WR,              /*!< EDMA2 TC3 Write */
                eCPT_MID_reserved36,
                eCPT_MID_reserved37,
				/* SRIO PKTDMA master group */
				eCPT_MID_SRIO_PKTDMA_Grp0,          /*!< SRIO_PKTDMA - master 0*/
				eCPT_MID_SRIO_PKTDMA_Grp1,          /*!< SRIO_PKTDMA - master 1*/
                eCPT_MID_reserved40,
                eCPT_MID_reserved41,
                eCPT_MID_reserved42,
                eCPT_MID_reserved43,
                eCPT_MID_reserved44,
                eCPT_MID_reserved45,
                eCPT_MID_reserved46,
                eCPT_MID_reserved47,
                eCPT_MID_DAP,                       /*!< DAP  */
                eCPT_MID_TPCC0,                     /*!< TPCC0 */
                eCPT_MID_TPCC1,                     /*!< TPCC1 */
                eCPT_MID_TPCC2,                     /*!< TPCC2 */
                eCPT_MID_MSMC,                      /*!< MSMC (note- for transactions initiated by MSMC internally and sent to the DDR) */
                eCPT_MID_PCIe,                      /*!< PCIe */
                eCPT_MID_SRIO_M,                    /*!< SRIO Master */
                eCPT_MID_HyperBridge,               /*!< Hyperbridge */
                eCPT_MID_NETCP_Grp0,                /*!< Network Co-processor master 0 */
                eCPT_MID_NETCP_Grp1,                /*!< Network Co-processor master 1 */
                eCPT_MID_NETCP_Grp2,                /*!< Network Co-processor master 2 */
                eCPT_MID_NETCP_Grp3,                /*!< Network Co-processor master 3 */
                eCPT_MID_reserved60,
                eCPT_MID_reserved61,
                eCPT_MID_reserved62,
                eCPT_MID_reserved63,
                eCPT_MID_reserved64,
                eCPT_MID_reserved65,
                eCPT_MID_reserved66,
                eCPT_MID_reserved67,
                eCPT_MID_reserved68,
                eCPT_MID_reserved69,
                eCPT_MID_reserved70,
                eCPT_MID_reserved71,
                eCPT_MID_reserved72,
                eCPT_MID_reserved73,
                eCPT_MID_reserved74,
                eCPT_MID_reserved75,
                eCPT_MID_reserved76,
                eCPT_MID_reserved77,
                eCPT_MID_reserved78,
                eCPT_MID_reserved79,
                eCPT_MID_reserved80,
                eCPT_MID_reserved81,
                eCPT_MID_reserved82,
                eCPT_MID_reserved83,
                eCPT_MID_reserved84,
                eCPT_MID_reserved85,
                eCPT_MID_reserved86,
                eCPT_MID_reserved87, 
                /* QM_CDMA master group */
                eCPT_MID_QM_CDMA_Grp0,              /*!< QM_CDMA - master 0 */
                eCPT_MID_QM_CDMA_Grp1,              /*!< QM_CDMA - master 1 */
                eCPT_MID_QM_CDMA_Grp2,              /*!< QM_CDMA - master 2 */
                eCPT_MID_QM_CDMA_Grp3,              /*!< QM_CDMA - master 3 */
                /* QM_second master group */
                eCPT_MID_QM_second_Grp0,            /*!< QM_second - master 0..1*/
                eCPT_MID_QM_second_Grp1,            /*!< QM_second - master 0*/
                eCPT_MID_reserved94,
                eCPT_MID_reserved95,
                eCPT_MID_reserved96,
                eCPT_MID_Cnt = 128                     
            } eCPT_MasterID;

#ifdef __cplusplus
}
#endif

#endif /* __CPTLIBARY_C6672_H */
