#ifndef __CPTLIBRARY_TCI6630K2L_H
#define __CPTLIBRARY_TCI6630K2L_H
/*
 * CPTLib_TCI6630K2L.h
 *
 * Common Platform (CP) Tracer Library TCI6630K2L device specific definitions
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib_TCI6630K2L.h
    TCI6630K2L specific CP Tracer modules definitions
*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCPT_ModID
    CP Tracer module ids
*/

// These are in SID order               
typedef enum {  eCPT_MSMC_0,             /*!< CP Tracer MSMC 0 module*/
                eCPT_MSMC_1,             /*!< CP Tracer MSMC 1 module*/
                eCPT_MSMC_2,             /*!< CP Tracer MSMC 2 module*/
                eCPT_MSMC_3,             /*!< CP Tracer MSMC 3 module*/
                eCPT_QM_MST,             /*!< CP Tracer Queue Manager Master module*/
                eCPT_DDR,              /*!< CP Tracer DDR3A module*/
                eCPT_SM,                 /*!< CP Tracer Semaphore module*/
                eCPT_QM_CFG1,            /*!< CP Tracer Queue Manager 1 Priority module*/
                eCPT_SCR3_CFG,           /*!< CP Tracer SCR3 Configuration module*/
                eCPT_L2_0,               /*!< CP Tracer L2 0 Memory Controller module*/
                eCPT_L2_1,               /*!< CP Tracer L2 1 Memory Controller module*/
                eCPT_L2_2,               /*!< CP Tracer L2 2 Memory Controller module*/
                eCPT_L2_3,               /*!< CP Tracer L2 3 Memory Controller module*/
                eCPT_RAC,                /*!< CP Tracer Receiver Accelerator Coprocessor module*/
                eCPT_RAC_CFG1,           /*!< CP Tracer Receiver Accelerator Coprocessor Configuration 1 module*/
                eCPT_TAC,                /*!< CP Tracer Transmit Accelerator Coprocessor module*/
                eCPT_QM_CFG2,            /*!< CP Tracer Queue Manager 2 Priority module*/
                eCPT_OSR_PCIE1_CFG,       /*!< CP Tracer On-chip Standalone RAM and PCIE1 CFG port */
                eCPT_TPCC_0,             /*!< CP Tracer EDMA3 TPCC0 module*/
                eCPT_TPCC_1_2,           /*!< CP Tracer EDMA3 TPCC1 and EDMA3 TPCC2 module*/
                eCPT_INTC_CFG,           /*!< CP Tracer Interrupt Controller (INTC) configuration module*/
                eCPT_MSMC_4,             /*!< CP Tracer MSMC 4 module*/
				eCPT_MSMC_5,             /*!< CP Tracer MSMC 5 module*/
				eCPT_MSMC_6,             /*!< CP Tracer MSMC 6 module*/
				eCPT_MSMC_7,             /*!< CP Tracer MSMC 7 module*/
				eCPT_SPI_ROM_EMIF16,     /*!< CP Tracer SPI, ROM and EMIF16 modules*/
				eCPT_CFG_3P_U,           /*!< CP Tracer CFG 3P U */
                eCPT_ModID_Last
               } eCPT_ModID;        

/*! \par eCPT_MasterID
    CP Tracer master ids
    
    The following table defines the list of masters that can be enabled for throughput counting and 
    New Request events.
    
    Note that some masters consist of a group of IDs designated with "_GrpN" suffix. For most situations 
    enabling the entire group rather than a single group is the typical use case. See CPT_CfgMaster()
    for details.
	
	Also note that master ids 140-167 and 174-178 correspond to the CP tracer masters. These 
	CP tracer masters are connected to the STM module via a private interconnect. We intentionally did 
	not define these master IDs, because the transactions from these masters have no significance from 
	an application SW point of view. Also, the transactions from these masters cannot be traced at any 
	available CP tracers.
                                                                                                                       
    \par <DQR> Data Qualifier Restriction for certain Masters
    The dtype qualifier for all non-MSMC CP Tracers is tied off to the DMA value. This means that for these CP Tracers, if you
    exclude DMA cycles (see ::eCPT_SrcQual) all data accesses are filtered.
    
*/

// Master IDs 
typedef enum { 
	            eCPT_MID_GEM0,                      /*!< GEM0 */
	            eCPT_MID_GEM1,                      /*!< GEM1 */
	            eCPT_MID_GEM2,                      /*!< GEM2 */
	            eCPT_MID_GEM3,                      /*!< GEM3 */
	            eCPT_MID_reserved_4,
	            eCPT_MID_reserved_5,
	            eCPT_MID_reserved_6,
	            eCPT_MID_reserved_7,
	            eCPT_MID_A15_CPU0_0,                /*!< ARM A15 CPU0 */
				eCPT_MID_A15_CPU1_0,                /*!< ARM A15 CPU1 */
				eCPT_MID_reserved_10,
				eCPT_MID_reserved_11,
				eCPT_MID_reserved_12,
				eCPT_MID_reserved_13,
				eCPT_MID_reserved_14,
				eCPT_MID_reserved_15,
				eCPT_MID_GEM0_CFG,                  /*!< GEM0 CFG */
				eCPT_MID_GEM1_CFG,                  /*!< GEM1 CFG */
				eCPT_MID_GEM2_CFG,                  /*!< GEM2 CFG */
				eCPT_MID_GEM3_CFG,                  /*!< GEM3 CFG */
				eCPT_MID_reserved_20,
				eCPT_MID_reserved_21,
				eCPT_MID_reserved_22,
				eCPT_MID_reserved_23,
				eCPT_MID_reserved_24,
                eCPT_MID_EDMA0_TC0_RD,              /*!< EDMA0 TC0 Read */
                eCPT_MID_EDMA0_TC0_WR,              /*!< EDMA0 TC0 Write */
                eCPT_MID_EDMA0_TC1_RD,              /*!< EDMA0 TC1 Read */
                eCPT_MID_PCIE_1_MST,                /*!< PCIE 1 Master */
                eCPT_MID_reserved_29,
                eCPT_MID_reserved_30,
                eCPT_MID_PCIE_0_MST,                /*!< PCIE 0 Master */
                eCPT_MID_EDMA0_TC1_WR,              /*!< EDMA0 TC1 Write */
                eCPT_MID_EDMA1_TC0_RD,              /*!< EDMA1 TC0 Read */
                eCPT_MID_EDMA1_TC0_WR,              /*!< EDMA1 TC0 Write */
                eCPT_MID_EDMA1_TC1_RD,              /*!< EDMA1 TC1 Read */
                eCPT_MID_EDMA1_TC1_WR,              /*!< EDMA1 TC1 Write */                
                eCPT_MID_EDMA1_TC2_RD,              /*!< EDMA1 TC2 Read */
                eCPT_MID_EDMA1_TC2_WR,              /*!< EDMA1 TC2 Write */
                eCPT_MID_EDMA1_TC3_RD,              /*!< EDMA1 TC3 Read */
                eCPT_MID_EDMA1_TC3_WR,              /*!< EDMA1 TC3 Write */
                eCPT_MID_EDMA2_TC0_RD,              /*!< EDMA2 TC0 Read */
                eCPT_MID_EDMA2_TC0_WR,              /*!< EDMA2 TC0 Write */
                eCPT_MID_EDMA2_TC1_RD,              /*!< EDMA2 TC1 Read */
                eCPT_MID_EDMA2_TC1_WR,              /*!< EDMA2 TC1 Write */                
                eCPT_MID_EDMA2_TC2_RD,              /*!< EDMA2 TC2 Read */
                eCPT_MID_EDMA2_TC2_WR,              /*!< EDMA2 TC2 Write */
                eCPT_MID_EDMA2_TC3_RD,              /*!< EDMA2 TC3 Read */
                eCPT_MID_EDMA2_TC3_WR,              /*!< EDMA2 TC3 Write */
                eCPT_MID_reserved_49,
                eCPT_MID_reserved_50,
                eCPT_MID_reserved_51,
                eCPT_MID_reserved_52,
                eCPT_MID_reserved_53,              /*!< EDMA3 TC1 Write */
				/* NETCP Global master group */
				eCPT_MID_NETCP_GLOBAL0,            /*!< NETCP_GLOBAL0 */
				eCPT_MID_NETCP_GLOBAL1,            /*!< NETCP_GLOBAL1 */
				eCPT_USB_MST,                      /*!< USB Master  */
				eCPT_MID_FFTC_B,                    /*!< FFTC_B  */
				eCPT_MID_reserved_58,
				eCPT_MID_reserved_59,
				eCPT_MID_RAC_A_BE0,                 /*!< RAC_A_BE0 */
				eCPT_MID_RAC_A_BE1,                 /*!< RAC_A_BE1 */
				eCPT_MID_TPCC0,                     /*!< EDMA3 TPCC0 */
				eCPT_MID_TPCC1,                     /*!< EDMA3 TPCC1 */
				eCPT_MID_TPCC2,                     /*!< EDMA3 TPCC2 */
				eCPT_MID_reserved_65,
				eCPT_MID_reserved_66,
				eCPT_MID_reserved_67,
				/* QM Secondary CDMA master group */
				eCPT_MID_QM_second_Grp0,          /*!< QM Secondary CDMA - master 0 */
				eCPT_MID_QM_second_Grp1,          /*!< QM Secondary CDMA - master 1 */
				eCPT_MID_QM_second_Grp2,          /*!< QM Secondary CDMA - master 2 */
				eCPT_MID_QM_second_Grp3,          /*!< QM Secondary CDMA - master 3 */
				/* IQN CDMA master group  */
				eCPT_MID_IQN_CDMA_Grp0,           /*!< IQN CDMA - master 0 */
				eCPT_MID_IQN_CDMA_Grp1,           /*!< IQN CDMA - master 1 */
				eCPT_MID_IQN_CDMA_Grp2,           /*!< IQN CDMA - master 2* */
				eCPT_MID_IQN_CDMA_Grp3,           /*!< IQN CDMA - master 3* */
				eCPT_MID_reserved_76,
				eCPT_MID_reserved_77,
				eCPT_MID_reserved_78,
				eCPT_MID_reserved_79,
				eCPT_MID_reserved_80,
				eCPT_MID_BCP_DIO0,                  /*!< BCP Direct IO 0  */
				eCPT_MID_BCP_DIO1,                  /*!< BCP Direct IO 1  */
				eCPT_MID_reserved_83,
				eCPT_MID_reserved_84,
				eCPT_MID_reserved_85,
				eCPT_MID_reserved_86,
				eCPT_MID_reserved_87,
				eCPT_MID_reserved_88,
				eCPT_MID_reserved_89,
				eCPT_MID_reserved_90,
				eCPT_MID_reserved_91,
				eCPT_MID_reserved_92,
				eCPT_MID_reserved_93,
				eCPT_MID_reserved_94,
				eCPT_MID_reserved_95,
				/* QM1_CDMA master group */
				eCPT_MID_QM1_CDMA_Grp0,              /*!< QM1_CDMA - master 0 */
				eCPT_MID_QM1_CDMA_Grp1,              /*!< QM1_CDMA - master 1 */
				eCPT_MID_QM1_CDMA_Grp2,              /*!< QM1_CDMA - master 2 */
				eCPT_MID_QM1_CDMA_Grp3,              /*!< QM1_CDMA - master 3 */
                eCPT_MID_reserved_100,
                eCPT_MID_reserved_101,
                eCPT_MID_BCP_CDMA,                   /*!< BCP CDMA master */
                eCPT_MID_TAC_FEI0,                   /*!< TAC FEI0 master */
                eCPT_MID_reserved_104,
                eCPT_MID_reserved_105,
                eCPT_MID_FFTC_A,                     /*!< FFTC_A  */
                eCPT_MID_DAP,                        /*!< DAP  */
                eCPT_MID_reserved_108,
                eCPT_MID_reserved_109,
                eCPT_MID_reserved_110,
                eCPT_MID_reserved_111,
                eCPT_MID_NETCP_LOCAL_Grp0, 			/*!< NETCP Local - master 0 */
                eCPT_MID_NETCP_LOCAL_Grp1,			/*!< NETCP Local - master 1 */
                eCPT_MID_NETCP_LOCAL_Grp2,			/*!< NETCP Local - master 2 */
                eCPT_MID_NETCP_LOCAL_Grp3,			/*!< NETCP Local - master 3 */
                eCPT_MID_NETCP_LOCAL_Grp4,			/*!< NETCP Local - master 4 */
                eCPT_MID_NETCP_LOCAL_Grp5,			/*!< NETCP Local - master 5 */
                eCPT_MID_NETCP_LOCAL_Grp6,			/*!< NETCP Local - master 6 */
                eCPT_MID_NETCP_LOCAL_Grp7,			/*!< NETCP Local - master 7 */
                eCPT_MID_MSMC5 = 174,               /*!< MSMC5 master  */
                eCPT_MID_MSMC6,						/*!< MSMC6 master  */
                eCPT_MID_MSMC7,						/*!< MSMC7 master  */
                eCPT_MID_MSMC4,						/*!< MSMC4 master  */
                eCPT_MID_CFG_3P_U,                  /*!< CFG 3P U master  */
                eCPT_MID_reserved_179,
				/* NETCP master group */
				eCPT_MID_NETCP_GLOBAL_Grp0,         /*!< NETCP GLOBAL- master 0 */
				eCPT_MID_NETCP_GLOBAL_Grp1,         /*!< NETCP GLOBAL- master 1 */
				eCPT_MID_NETCP_GLOBAL_Grp2,         /*!< NETCP GLOBAL- master 2 */
				eCPT_MID_NETCP_GLOBAL_Grp3,         /*!< NETCP GLOBAL- master 3 */
                eCPT_MID_Cnt = 256
            } eCPT_MasterID;

#ifdef __cplusplus
}
#endif

#endif /* __CPTLIBARY_TCI6630K2L_H */
