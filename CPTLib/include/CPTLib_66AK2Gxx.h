#ifndef __CPTLIBRARY_66AK2Gxx_H
#define __CPTLIBRARY_66AK2Gxx_H
/*
 * CPTLib_66AK2Exx.h
 *
 * Common Platform (CP) Tracer Library 66AK2Exx device specific definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib_66AK2Gxx.h
    66AK2Exx specific CP Tracer modules definitions
*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCPT_ModID
    CP Tracer module ids
*/

typedef enum {  eCPT_MSMC_0,            /*!< CP Tracer MSMC 0 module*/
                eCPT_DDR,               /*!< CP Tracer DDR3 module*/
                eCPT_L2_0,              /*!< CP Tracer L2 0 Memory Controller module*/
                eCPT_PCIE,              /*!< CP Tracer PCIE module*/
                eCPT_DXB,               /*!< CP Tracer DXB module*/
                eCPT_MCASP_MCBSP,       /*!< CP Tracer MCASP and MCBSP0..2 modules*/
                eCPT_GPMC_MMC_QSPI,     /*!< CP Tracer GPMC and QAPI and MMC0..1 module*/
                eCPT_TPCC0_1,           /*!< CP Tracer TPCC_0 and TPCC_1 modules */
                eCPT_CFG,               /*!< CP Tracer CFG */
                eCPT_ALWAYSON_CFG,      /*!< CP Tracer Always on peripheral config ports */
                eCPT_GIC,               /*!< CP Tracer GIC module*/
                eCPT_CIC,               /*!< CP Tracer CIC module*/
                eCPT_ROM_SPI,           /*!< CP Tracer SPI, DSP and ARM Boot ROMs*/
                eCPT_ALWAYSON_MAIN,     /*!< CP Tracer Alyways On From DMA SCR */
                eCPT_ICSS_ASRC,         /*!< CP Tracer ICSS and ASRC modules*/
                eCPT_ModID_Last
               } eCPT_ModID;        

/*! \par eCPT_MasterID
    CP Tracer master ids
    
    The following table defines the list of masters that can be enabled for throughput counting and 
    New Request events.
    
    Note that some masters consist of a group of IDs designated with "_GrpN" suffix. For most situations 
    enabling the entire group rather than a single group is the typical use case. See CPT_CfgMaster()
    for details.
	
	Also note that master ids 140-154 correspond to the CP tracer masters. These
	CP tracer masters are connected to the STM module via a private interconnect. We intentionally did 
	not define these master IDs, because the transactions from these masters have no significance from 
	an application SW point of view. Also, the transactions from these masters cannot be traced at any 
	available CP tracers.
                                                                                                                       
    \par <DQR> Data Qualifier Restriction for certain Masters
    The dtype qualifier for all non-MSMC CP Tracers is tied off to the DMA value. This means that for these CP Tracers, if you
    exclude DMA cycles (see ::eCPT_SrcQual) all data accesses are filtered.
    
*/

// Master IDs 
typedef enum {
	            eCPT_MID_DSP0,                      /*!< C66x Corepac 0 */
	            eCPT_MID_reserved1,
	            eCPT_MID_reserved2,
	            eCPT_MID_reserved3,
	            eCPT_MID_reserved4,
	            eCPT_MID_reserved5,
	            eCPT_MID_reserved6,
	            eCPT_MID_reserved7,
	            eCPT_MID_A15_CPU0_0,                /*!< ARM A15 CPU0 */
	            eCPT_MID_reserved9,
	            eCPT_MID_reserved10,
	            eCPT_MID_reserved11,
				eCPT_MID_reserved12,
				eCPT_MID_reserved13,
				eCPT_MID_reserved14,
				eCPT_MID_reserved15,
				eCPT_MID_DSP0_CFG,                  /*!< C66x Corepac 0 CFG */
				eCPT_MID_reserved17,
				eCPT_MID_reserved18,
				eCPT_MID_reserved19,
				eCPT_MID_reserved20,
				eCPT_MID_reserved21,
				eCPT_MID_reserved22,
				eCPT_MID_reserved23,
				eCPT_MID_reserved24,
				eCPT_MID_reserved25,
                eCPT_MID_PCIE,                      /*!< PCIE */
                eCPT_MID_ICSS_0_MST0,               /*!< ICSS_0_MST0 */
                eCPT_MID_ICSS_0_MST1,               /*!< ICSS_0_MST1 */
                eCPT_MID_ICSS_1_MST0,               /*!< ICSS_1_MST0 */
                eCPT_MID_ICSS_1_MST1,               /*!< ICSS_1_MST1 */
                eCPT_MID_PMMC,                      /*!< PMMC */
                eCPT_MID_NSSL_Grp0,                 /*!< NSSL group master port 0 */
                eCPT_MID_NSSL_Grp1,                 /*!< NSSL group master port 1 */
                eCPT_MID_NSSL_Grp2,                 /*!< NSSL group master port 2 */
                eCPT_MID_NSSL_Grp3,                 /*!< NSSL group master port 3 */
                eCPT_MID_reserved36,
                eCPT_MID_reserved37,
                eCPT_MID_reserved38,
                eCPT_MID_EDMA0_CC_TR,               /*!< EDMA0 CC TR */
                eCPT_MID_EDMA1_CC_TR,               /*!< EDMA0 CC TR */
                eCPT_MID_EDMA0_TC0_RD,              /*!< EDMA2 TC0 Read */
                eCPT_MID_EDMA0_TC0_WR,              /*!< EDMA2 TC0 Write */
                eCPT_MID_EDMA0_TC1_RD,              /*!< EDMA2 TC1 Read */
                eCPT_MID_EDMA0_TC1_WR,              /*!< EDMA2 TC1 Write */
                eCPT_MID_EDMA1_TC0_RD,              /*!< EDMA2 TC2 Read */
                eCPT_MID_EDMA1_TC0_WR,              /*!< EDMA2 TC2 Write */
                eCPT_MID_EDMA1_TC1_RD,              /*!< EDMA2 TC3 Read */
                eCPT_MID_EDMA1_TC1_WR,              /*!< EDMA2 TC3 Write */
                eCPT_MID_reserved49,
                eCPT_MID_USB_0_MST,                 /*!< USB_0_MST  */
                eCPT_MID_USB_1_MST,                 /*!< USB_1_MST  */
				eCPT_MID_MSMC,                      /*!< MSMC (note- for transactions initiated by MSMC internally and sent to the DDR) */
				eCPT_MID_reserved53,
				eCPT_MID_reserved54,
				eCPT_MID_reserved55,
				eCPT_MID_reserved56,
				eCPT_MID_DAP,                       /*!< DAP Master */
				eCPT_MID_reserved58,
				eCPT_MID_MLB,                       /*!< MLB Master */
				eCPT_MID_reserved60,
				eCPT_MID_reserved61,
				eCPT_MID_MMCHS_0,                   /*!< MMCHS 0 Master */
				eCPT_MID_MMCHS_1,                   /*!< MMCHS 1 Master */
				eCPT_MID_DBX_Grp0,                  /*!< DBX group of 32 masters*/
				eCPT_MID_DSSUL_Grp0=96,             /*!< DSSUL group 0f 16 masters*/
                eCPT_MID_Cnt = 112
            } eCPT_MasterID;

#ifdef __cplusplus
}
#endif

/*! \par eCPT_MID_GEM0
 *      Provided for code that is ported from another Keystone device to 66AK2Gxx.
 */
#define eCPT_MID_GEM0 eCPT_MID_DSP0

#endif /* __CPTLIBARY_66AK2Gxx_H */
