#ifndef __CPTLIBRARY_66AK2Exx_H
#define __CPTLIBRARY_66AK2Exx_H
/*
 * CPTLib_66AK2Exx.h
 *
 * Common Platform (CP) Tracer Library 66AK2Exx device specific definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib_66AK2Exx.h
    66AK2Exx specific CP Tracer modules definitions
*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCPT_ModID
    CP Tracer module ids
*/

typedef enum {  eCPT_MSMC_0,             /*!< CP Tracer MSMC 0 module*/
                eCPT_MSMC_1,             /*!< CP Tracer MSMC 1 module*/
                eCPT_MSMC_2,             /*!< CP Tracer MSMC 2 module*/
                eCPT_MSMC_3,             /*!< CP Tracer MSMC 3 module*/
                eCPT_QM_MST,             /*!< CP Tracer Queue Manager Master module*/
                eCPT_DDR,                /*!< CP Tracer DDR3A module*/
                eCPT_SM,                 /*!< CP Tracer Semaphore module*/
                eCPT_QM_CFG1,            /*!< CP Tracer Queue Manager 1 Priority module*/
                eCPT_SCR3_CFG,           /*!< CP Tracer SCR3 Configuration module*/
                eCPT_L2_0,               /*!< CP Tracer L2 0 Memory Controller module*/
                eCPT_QM_CFG2,            /*!< CP Tracer Queue Manager 2 Priority module*/
                eCPT_TPCC_0_4,           /*!< CP Tracer EDMA3 TPCC0 and EDMA3 TPCC4 module*/
                eCPT_TPCC_1_2_3,         /*!< CP Tracer EDMA3 TPCC1, EDMA3 TPCC2 and EDMA3 TPCC3 module*/
                eCPT_INTC_CFG,           /*!< CP Tracer Interrupt Controller (INTC) configuration module*/
                eCPT_MSMC_4,             /*!< CP Tracer MSMC 4 module*/
				eCPT_MSMC_5,             /*!< CP Tracer MSMC 5 module*/
				eCPT_MSMC_6,             /*!< CP Tracer MSMC 6 module*/
				eCPT_MSMC_7,             /*!< CP Tracer MSMC 7 module*/
				eCPT_SPI_ROM_EMIF16,     /*!< CP Tracer SPI, ROM and EMIF16 modules*/
				eCPT_NETCP_USB_CFG,      /*!< CP Tracer NETCP USB Config */
				eCPT_PCIE1_CFG,          /*!< CP Tracer PCIE1_CFG */
                eCPT_ModID_Last
               } eCPT_ModID;        

/*! \par eCPT_MasterID
    CP Tracer master ids
    
    The following table defines the list of masters that can be enabled for throughput counting and 
    New Request events.
    
    Note that some masters consist of a group of IDs designated with "_GrpN" suffix. For most situations 
    enabling the entire group rather than a single group is the typical use case. See CPT_CfgMaster()
    for details.
	
	Also note that master ids 140-167 and 174-178 correspond to the CP tracer masters. These 
	CP tracer masters are connected to the STM module via a private interconnect. We intentionally did 
	not define these master IDs, because the transactions from these masters have no significance from 
	an application SW point of view. Also, the transactions from these masters cannot be traced at any 
	available CP tracers.
                                                                                                                       
    \par <DQR> Data Qualifier Restriction for certain Masters
    The dtype qualifier for all non-MSMC CP Tracers is tied off to the DMA value. This means that for these CP Tracers, if you
    exclude DMA cycles (see ::eCPT_SrcQual) all data accesses are filtered.
    
*/

// Master IDs 
typedef enum { 
	            eCPT_MID_GEM0,                      /*!< GEM0 */
	            eCPT_MID_reserved1,
	            eCPT_MID_reserved2,
	            eCPT_MID_reserved3,
	            eCPT_MID_reserved4,
	            eCPT_MID_reserved5,
	            eCPT_MID_reserved6,
	            eCPT_MID_reserved7,
	            eCPT_MID_A15_CPU0_0,                /*!< ARM A15 CPU0 */
				eCPT_MID_A15_CPU1_0,                /*!< ARM A15 CPU1 */
				eCPT_MID_A15_CPU2_0,                /*!< ARM A15 CPU2 */
				eCPT_MID_A15_CPU3_0,                /*!< ARM A15 CPU3 */
				eCPT_MID_reserved12,
				eCPT_MID_reserved13,
				eCPT_MID_reserved14,
				eCPT_MID_reserved15,
				eCPT_MID_GEM0_CFG,                  /*!< GEM0 CFG */
				eCPT_MID_reserved17,
				eCPT_MID_reserved18,
				eCPT_MID_reserved19,
				eCPT_MID_reserved20,
				eCPT_MID_reserved21,
				eCPT_MID_reserved22,
				eCPT_MID_reserved23,
				eCPT_MID_reserved24,
                eCPT_MID_EDMA0_TC0_RD,              /*!< EDMA0 TC0 Read */
                eCPT_MID_EDMA0_TC0_WR,              /*!< EDMA0 TC0 Write */
                eCPT_MID_EDMA0_TC1_RD,              /*!< EDMA0 TC1 Read */
                eCPT_MID_VUSR0_MST,                 /*!< VSR0_MST */
                eCPT_MID_USB_1_MST,                 /*!< USB_1_MST */
                eCPT_MID_reseerved30,
                eCPT_MID_PCIe,                      /*!< PCIe Master */
                eCPT_MID_EDMA0_TC1_WR,              /*!< EDMA0 TC1 Write */
                eCPT_MID_EDMA1_TC0_RD,              /*!< EDMA1 TC0 Read */
                eCPT_MID_EDMA1_TC0_WR,              /*!< EDMA1 TC0 Write */
                eCPT_MID_EDMA1_TC1_RD,              /*!< EDMA1 TC1 Read */
                eCPT_MID_EDMA1_TC1_WR,              /*!< EDMA1 TC1 Write */                
                eCPT_MID_EDMA1_TC2_RD,              /*!< EDMA1 TC2 Read */
                eCPT_MID_EDMA1_TC2_WR,              /*!< EDMA1 TC2 Write */
                eCPT_MID_EDMA1_TC3_RD,              /*!< EDMA1 TC3 Read */
                eCPT_MID_EDMA1_TC3_WR,              /*!< EDMA1 TC3 Write */
                eCPT_MID_EDMA2_TC0_RD,              /*!< EDMA2 TC0 Read */
                eCPT_MID_EDMA2_TC0_WR,              /*!< EDMA2 TC0 Write */
                eCPT_MID_EDMA2_TC1_RD,              /*!< EDMA2 TC1 Read */
                eCPT_MID_EDMA2_TC1_WR,              /*!< EDMA2 TC1 Write */                
                eCPT_MID_EDMA2_TC2_RD,              /*!< EDMA2 TC2 Read */
                eCPT_MID_EDMA2_TC2_WR,              /*!< EDMA2 TC2 Write */
                eCPT_MID_EDMA2_TC3_RD,              /*!< EDMA2 TC3 Read */
                eCPT_MID_EDMA2_TC3_WR,              /*!< EDMA2 TC3 Write */
                eCPT_MID_EDMA3_TC0_RD,              /*!< EDMA3 TC0 Read */
				eCPT_MID_EDMA3_TC0_WR,              /*!< EDMA3 TC0 Write */
				eCPT_MID_EDMA3_TC1_RD,              /*!< EDMA3 TC1 Read */
				eCPT_MID_MSMC,                      /*!< MSMC (note- for transactions initiated by MSMC internally and sent to the DDR) */
				eCPT_MID_EDMA3_TC1_WR,              /*!< EDMA3 TC1 Write */
				eCPT_MID_reserved54,
				eCPT_MID_reserved55,
				eCPT_MID_USB_0_MST,                 /*!< USB_0_MST  */
				eCPT_MID_reserved57,
				eCPT_MID_reserved58,
				eCPT_MID_reserved59,
				eCPT_MID_reserved60,
				eCPT_MID_reserved61,
				eCPT_MID_TPCC0,                     /*!< EDMA0 CC TR */
				eCPT_MID_TPCC1,                     /*!< EDMA1 CC TR */
				eCPT_MID_TPCC2,                     /*!< EDMA2 CC TR */
				eCPT_MID_reserved65,
				eCPT_MID_reserved66,
				eCPT_MID_reserved67,
				/* QM Secondary CDMA master group */
				eCPT_MID_QM_second_Grp0,            /*!< QM Secondary CDMA - master 0 */
				eCPT_MID_QM_second_Grp1,            /*!< QM Secondary CDMA - master 1 */
				eCPT_MID_QM_second_Grp2,            /*!< QM Secondary CDMA - master 2 */
				eCPT_MID_QM_second_Grp3,            /*!< QM Secondary CDMA - master 3 */
				/* NETCP_GLOBAL1  */
				eCPT_MID_NETCP_GLOBAL1_Grp0,        /*!< NETCP_GLOBAL1 - master 0 */
				eCPT_MID_NETCP_GLOBAL1_Grp1,        /*!< NETCP_GLOBAL1 - master 1 */
				eCPT_MID_NETCP_GLOBAL1_Grp2,        /*!< NETCP_GLOBAL1 - master 2* */
				eCPT_MID_NETCP_GLOBAL1_Grp3,        /*!< NETCP_GLOBAL1 - master 3* */
				eCPT_MID_reserved76,
				eCPT_MID_reserved77,
				eCPT_MID_reserved78,
				eCPT_MID_reserved79,
				eCPT_MID_TSIP1,                     /*!< TSIP DMA 1  */
				eCPT_MID_reserved81,
				eCPT_MID_reserved82,
				eCPT_MID_TPCC3,                     /*!< EDMA3 CC TR */
				/* XGE (10 GIG Ethernet) master group */
				eCPT_MID_XGE_Grp0,                  /*!< XGE (10 GIG Ethernet) master 0 */
				eCPT_MID_XGE_Grp1,                  /*!< XGE (10 GIG Ethernet) master 1 */
				eCPT_MID_XGE_Grp2,                  /*!< XGE (10 GIG Ethernet) master 2 */
				eCPT_MID_XGE_Grp3,                  /*!< XGE (10 GIG Ethernet) master 3 */
				eCPT_MID_reserved88,                 /*!< RAC_C_BE0 */
				eCPT_MID_reserved89,                 /*!< RAC_C_BE1 */
				eCPT_MID_reserved90,                 /*!< RAC_D_BE0 */
				eCPT_MID_reserved91,                 /*!< RAC_D_BE1 */
				/* QM2_CDMA master group */
				eCPT_MID_reserved92,
				eCPT_MID_reserved93,
				eCPT_MID_reserved94,
				eCPT_MID_reserved95,
				/* QM1_CDMA master group */
				eCPT_MID_QM1_CDMA_Grp0,              /*!< QM1_CDMA - master 0 */
				eCPT_MID_QM1_CDMA_Grp1,              /*!< QM1_CDMA - master 1 */
				eCPT_MID_QM1_CDMA_Grp2,              /*!< QM1_CDMA - master 2 */
				eCPT_MID_QM1_CDMA_Grp3,              /*!< QM1_CDMA - master 3 */
                eCPT_MID_reserved100,
                eCPT_MID_reserved101,
                eCPT_MID_PCIE_1_MST,                 /*!< PCIE 1 master */
                eCPT_MID_reserved103,
                eCPT_MID_reserved104,
                eCPT_MID_reserved105,
                eCPT_MID_reserved106,
                eCPT_MID_DAP,                        /*!< DAP  */
                eCPT_MID_reserved108,
                eCPT_MID_reserved109,
                eCPT_MID_reserved110,
                eCPT_MID_reserved111,
                /* NETCP_LOCAL master group */
                eCPT_MID_NETCP_LOCAL_Grp0,           /*!< NETCP_LOCAL - master 0 */
                eCPT_MID_NETCP_LOCAL_Grp1,           /*!< NETCP_LOCAL - master 1 */
                eCPT_MID_NETCP_LOCAL_Grp2,           /*!< NETCP_LOCAL - master 2 */
                eCPT_MID_NETCP_LOCAL_Grp3,           /*!< NETCP_LOCAL - master 3 */
                eCPT_MID_NETCP_LOCAL_Grp4,           /*!< NETCP_LOCAL - master 4 */
                eCPT_MID_NETCP_LOCAL_Grp5,           /*!< NETCP_LOCAL - master 5 */
                eCPT_MID_NETCP_LOCAL_Grp6,           /*!< NETCP_LOCAL - master 6 */
                eCPT_MID_NETCP_LOCAL_Grp7,           /*!< NETCP_LOCAL - master 7 */

                eCPT_MID_EDMA4_TC0_RD = 169,         /*!< EDMA4 TC0 Read */
				eCPT_MID_EDMA4_TC0_WR,               /*!< EDMA4 TC0 Write */
				eCPT_MID_EDMA4_TC1_RD,               /*!< EDMA4 TC1 Read */
				eCPT_MID_EDMA4_TC1_WR,               /*!< EDMA4 TC1 Write */
				eCPT_MID_TPCC4,                      /*!< EDMA4 CC TR */

				eCPT_MID_NETCP1_CFG_MST = 178,       /*!< NETCP1 CFG master */
				/* NETCP master group */
				eCPT_MID_NETCP_Grp0 = 180,           /*!< NETCP GLOBAL0 - master 0 */
				eCPT_MID_NETCP_Grp1,                 /*!< NETCP GLOBAL0 - master 1 */
				eCPT_MID_NETCP_Grp2,                 /*!< NETCP GLOBAL0 - master 2 */
				eCPT_MID_NETCP_Grp3,                 /*!< NETCP GLOBAL0 - master 3 */
                eCPT_MID_Cnt = 256
            } eCPT_MasterID;
			
#ifdef __cplusplus
}
#endif

#endif /* __CPTLIBARY_66AK2Exx_H */
