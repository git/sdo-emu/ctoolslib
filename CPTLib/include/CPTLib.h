#ifndef __CPTLIBRARY_H
#define __CPTLIBRARY_H
/*
 * CPTLib.h
 *
 * Common Platform (CP) Tracer Library API Definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types
#include <stdbool.h>

//Using forward slashes in the relative serach path for linux compatibility

#ifdef _STM_Logging
#include "../../STMLib/include/StmLibrary.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib.h
    CPT Library Function Prototypes
*/
/*! \mainpage
 
    \section intro Introduction
    
    The CPT Library provides a CP Tracer Module programming API. A CP Tracer unit provides
    statistical and event trace data for the slave module the CP Tracer unit is associated with.
    Typically a device will support CP Tracer units for each critical slave module.
    This library also provides CPT specific STM logging capabilities.

    \par Devices Supported
    
    Devices currently supported by the library are:
    \li TMS320TCI6612 and TMS320TCI6614
    \li TMS320C6657
    \li TMS320C6670 and TMS320TCI6616 (both devices support with the _C6670 build)
    \li TMS320C6672
    \li TMS320C6674
    \li TMS320C6678
    \li C66AK2Hxx
	\li 66AK2Exx
	\li TCI6630K2L
	\li 66AK2Gxx
    
    \n Note - The library builds provided in this package are for TMS320C6670, TMS320TCI6616, TMS320C6657
       and TMS320C6614 devices. The library must be recompiled to support a different device types. 
       See the Build Notes for additional detail on configuring the library for a specific device. 
    
    \par CPT Library Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.0 </TD> <TD> 7/15/2010 </TD> <TD> CPT Draft API Release (no code)  </TD>
<TR><TD> 0.1 </TD> <TD> 7/15/2010 </TD> <TD> CPT Draft API Release (no code). 
                                             Moved AddrExportMask to CPT_CfgOptions
                                             from CPT_CfgAddrFilter() and update meta
                                             data to include AddrExportMask value. 
                                             Moved EMU trigger out control to
                                             CPT_ModuleEnable()/CPT_ModuleDisable()
                                             from CPT_TrigQualifiers.  </TD>
<TR><TD> 0.2 </TD> <TD> 9/15/2010 </TD> <TD> Updated CPT Draft API Release (no code)  </TD>
<TR><TD> 0.3 </TD> <TD> 1/7/2011 </TD> <TD> First release implementation - minor API changes  </TD>
<TR><TD> 0.4 </TD> <TD> 5/3/2011 </TD> <TD> Added System Profile cases to eCPT_UseCase </TD>
<TR><TD> 0.5 </TD> <TD> 5/3/2011 </TD> <TD> Added support for TCI6614 and TCI6612 </TD>
<TR><TD> 0.6 </TD> <TD> 3/23/2012 </TD> <TD> Added support for C6657 </TD>
<TR><TD> 0.7 </TD> <TD> 3/23/2012 </TD> <TD> Added predefined symbol compatibility with other cToolLibs.
                                             Modified ownership mechanism to include who owns (IDE or Lib)
                                             and not generate errors if the library already owns the module.
                                             This fixes conflicts when trying to restart an application using
                                             the library without resetting the device. </TD>
<TR><TD> 0.8 </TD> <TD> 3/23/2012 </TD> <TD> Added forceOwnership parameter to CPT_CfgOptions.
                                             Updated all projects to CCSv5. </TD>
<TR><TD> 0.9 </TD> <TD> 12/19/2012 </TD> <TD> Fixed power-up and module enable bugs in CPT_OpenModule(). </TD>
<TR><TD> 0.10 </TD> <TD> 2/1/2013 </TD> <TD> Added C66AK2Hxx support </TD>
<TR><TD> 0.11 </TD> <TD> 5/15/2013 </TD> <TD> For C6670 target, Replaced TE_SS master ids with correct BCP master ids </TD>
<TR><TD> 0.12 </TD> <TD> 4/28/2014 </TD> <TD> Added support for TCI6630K2L and 66AK2Exx </TD>
<TR><TD> 0.13 </TD> <TD> 9/29/2014 </TD> <TD> Added missing CCS project files </TD>
<TR><TD> 0.14 </TD> <TD> 10/09/2014 </TD> <TD> Keystone2 Master IDs 12-15 are marked as reserved.
											  Updated master id naming and definitions to be compatible with datasheet.</TD>
<TR><TD> 0.15 </TD> <TD> 5/14/2015 </TD> <TD> Added 66AK2Gxx support.</TD>

</TABLE>
    
    \section conv Build Notes
    
    \par Conventions
    
    The following public naming conventions are used by the API
    \li CPT_ - Prefix for public CPT Library functions
    \li eCPT - Prefix for public CPT enumerations 
    
    \par Pre-defined Symbols
    
    The CPT Library supports the following pre-defined symbols that if defined
    enable the functionality at compile time.  

    \li _STM_Logging - If defined the following APIs, that require the library be opened with a valid STM Library handle,
        are included in the build:
        - CPT_LogMsg()
        - Private functions for meta data transport with STMLib. Meta data is needed to process
          the hw generated CP Tracer data for a specific use-case (such as conversion of
          CT Tracer throughput counter data to bytes/sec). If _STM_Logging is not defined
          meta data is not transported and no additional processing is performed on the CP Tracer data.
          See ::_CPT_CfgOptions which is used to define user provided meta data.

    \n Note - The minimum revision STMLib that supports CPTLib logging and meta data transport is version 3.1.

    The library must be configure for a specific device by providing at compile time one of the following pre-defined symbols:
    \li TCI6612 or _TCI6612 TMS320TCI6612
    \li TCI6614 or _TCI6614 TMS320TCI6614
    \li C6657 or _C6657 TMS320C6657
    \li C6670 or _C6670 TMS320C6670 and TMS320TCI6616
    \li C6672 or _C6672 TMS320C6672
    \li C6674 or _C6674 TMS320C6674
    \li C6678 or _C6678 TMS320C6678
    \li C66AK2Hxx or _C66AK2Hxx 66AK2Hxx
    \li TCI6630K2L or _TCI6630K2L TCI6630K2L
	\li C66AK2Exx or _66AK2Exx 66AK2Exx
	\li C66AK2Gxx or _66AK2Gxx 66AK2Gxx

    \par Directory Structure
    
    STM is a component of cTools so it resides within the cTools directory structure.
    
    @code
    
    |--cTools
       |
       |-- CPTLib
       |   |
       |   |-- doc
       |   |   |
       |   |   |--CPT_C6657_html
       |   |   |--CPT_C6670_html
       |   |   |--CPT_C6672_html
       |   |   |--CPT_C6674_html
       |   |   |--CPT_C6678_html
       |   |   |--CPT_C66AK2Hxx_html
       |   |   |--CPT_TCI6612_html
       |   |   |--CPT_TCI6614_html
       |   |   |--CPT_TCI6630K2L_html
       |   |   |--CPT_66AK2Exx_html
       |   |   |--CPT_66AK2Gxx_html
       |   |      |
       |   |      |-index.html (Doxygen API documentation)
       |   |     
       |   |-- include  (Public API include file)
       |   |-- src      (Private .c and .h files )
       |   |-- projects (Target specific library builds)
       |   |   |
       |   |   |--C6657
       |   |   |--C667x   (Can build TCI6616, C6670, C6672, C6674 or C6678 versions)
       |   |   |--C66AK2Hxx
       |   |   |--TCI6614
       |   |   |--TCI6630K2L
       |   |   |--66AK2Exx
       |   |   |--66AK2Gxx
       |-- Examples     (Target specific stand-alone example projects)
           |
           |--common    (files common across examples)
           |--C6657
           |  |-- CPT_L2_CorePac0 (C6657 specific)
           |  |-- CPT_SystemProfile_CorePac0 (C6657 specific)
           |--C667x
           |  |-- CPT_L2_CorePac0 (Can run on any C667x device or TCI6616)
           |  |-- CPT_SystemProfile_CorePac0 (Can run on any C667x or TCI6616)
           |--C66AK2Hxx
           |  |--CPT_L2_CorePac0
           |  |--CPT_SystemProfile_CorePacN
           |--66AK2Exx
           |  |--CPT_L2_CorePac0
           |  |--CPT_SystemProfile_CorePacN
           |--66AK2Gxx
           |  |--CPT_L2_CorePac0
           |  |--CPT_SystemProfile_CorePacN
           |--TCI6630K2L
           |  |--CPT_L2_CorePac0
           |  |--CPT_SystemProfile_CorePacN
           |--TCI6614   
              |-- CPT_L2_CorePac0 (TCI6614 specific)
              |-- CPT_SystemProfile_CorePac0 (TCI6614 specific)     
     
    @endcode

    \par Helper Functions
    
    Helper functions allow the library to be ported easily to different operating environments without modifications
    to the library directly. Within the library these functions are declared extern so the library will build but
    can not be linked without implementations for the following helper functions:
    
    \li void * cTools_memAlloc(size_t sizeInBytes); \n\n
    If allocation successful returns pointer to allocated memory, else if not successful return NULL.
    
    \li void * cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes); \n\n
    Map a physical address to a virtual address space. Return the virtual pointer if successful, else return NULL.
    Return phyAddr if virtual mapping not required.   
    
    \li void cTools_memFree(void * ptr); \n\n
    Free the memory at the ptr (returned from cTools_memAlloc()) call. 
    
    \li void cTools_memUnMap(void * vAddr, unsigned int mapSizeInBytes); \n\n
    Unmap virtual address space starting at vAddr (returned from cTools_memMap())of mapSizeInBytes length.
    If virtual address mapping not required simply return.    
    
    \li int cTools_mutexInit(uint32_t mutexId); \n\n
    Allocate a mutex object for the provided mutexId.   
    This function is meant to provide cross process locking for a specific resource. 
    cTools guarantees that mutexIds will be unique between libraries, and can be any value that fits in a uint32_t type. 
    The mutex is created in the unlocked state.
    This function returns 0 if the mutex was created successfully or if the mutex already exists 
    (regardless of state - locked or unlocked).    
    This function returns -1 if an error occurred during creation of the mutex. 
    \n\n
    The implementation of this function is dependent on the environment the CP Tracer library is executing within. 
    If the user guarantees that across processors and processes that each physical
    CP Tracer is only opened once at any time, then this function (as with all cTools mutex functions) may simply
    always return 0.
    For SMP systems or cases where there are multiple instances of CPTLib running across multiple processes,
    if the same physical CP Tracer can be opened multiple times on the same processor, then implementation
    of this function is most likely required.  
    In this case implementation will require allocating memory for the mutex in shared memory and possible
    use of the hardware semaphore to protect access.  
    If the implementation is for a single instance of CPTLib (single or multi-threaded) then the implementation may
    only require allocation of the mutex from the heap.  
    
    \li int cTools_mutexTrylock(uint32_t mutexId); \n\n
    Attempt to lock the mutexId. 
    If the lock is successful return 0.
    If the mutexId is already locked return -1.
    If this function is attempted on a mutexId that has not been opened the results are undefined. 
        
    \li int cTools_mutexUnlock(uint32_t mutexId); \n\n
    Attempt to unlock the mutexId. 
    If the unlock is successful return 0.
    If the mutexId was already unlocked return 0.    
    If this function is attempted on a mutexId that has not been opened the results are undefined.

    \par CPT Export Notes
    
    If you are using CCS to capture STM data (with either a trace enabled emulator or ETB)
    you should enable the CPT module's STM messages through the CSS 4.x Breakpoint view by
    creating a Trace job and settings the following Trace Properties:
    \li Trace Type: System
    \li STM Trace Type: Trace Export Configuration
    \li Enable HW Message Sources:
    \li CPT Event Profiling: True
    \n Note that CPT Event Profiling by default is disabled.
    
    If you are working remotely from CCS and using the ETB to capture STM data 
    then you must provide your own application code to enable the CPT module's STM messages. 

*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// The following defines provide compatibility with other cToolsLibs(like ETBLib.h)when used
// in combination with each other.
//
////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(TCI6614) && !defined(_TCI6614)
#define _TCI6614
#endif
#if defined(TCI6612) && !defined(_TCI6612)
#define _TCI6612
#endif
#if defined(C6674) && !defined(_C6674)
#define _C6674
#endif
#if defined(C6672) && !defined(_C6672)
#define _C6672
#endif
#if defined(C6657) && !defined(_C6657)
#define _C6657
#endif
#if defined(C66AK2Hxx) && !defined(_C66AK2Hxx)
#define _C66AK2Hxx
#endif
#if defined(TCI6630K2L) && !defined(_TCI6630K2L)
#define _TCI6630K2L
#endif
#if defined(C66AK2Exx) && !defined(_66AK2Exx)
#define _66AK2Exx
#endif
#if defined(C66AK2Gxx) && !defined(_66AK2Gxx)
#define _66AK2Gxx
#endif

/* C6670/8 are special cases because for some libraries, 
 * like ETBLib, the C6670/7C6678 cases are compatible with 
 * other devices (like TCI6614/TCI6612 in the C6670 case AND
 * C6674/C6672 in the C6678 case).  
 */
#if defined(C6678) && !defined(_C6672) && !defined(_C6674) \
                   && !defined(_C6678)
#define _C6678
#endif 
#if defined(C6670) && !defined(_TCI6614) && !defined(_TCI6612) \
                   && !defined(_C6670)
#define _C6670
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

// Note - the version definitions must have the end of line immediately after the value (packaging script requirement)
#define CPTLIB_MAJOR_VERSION        (0x0)
                                               /*!< Major version number - Incremented for API changes*/
#define CPTLIB_MINOR_VERSION        (0xF)
                                               /*!< Minor version number - Incremented for bug fixes  */
#define CPTLIB_FUNC                 (0xe89)    /*!< CP Tracer Module Func ID this library supports */
#define CPT_FUNC_ID_SCHEME          (0x0)      /*!< Library is functionally compatible with modules that have this id scheme */

#if defined(_C6670)

#include "CPTLib_C6670.h"

#elif defined(_C6671)

#include "CPTLib_C6671.h"

#elif defined(_C6672)

#include "CPTLib_C6672.h"

#elif defined(_C6674)

#include "CPTLib_C6674.h"

#elif defined(_C6678)

#include "CPTLib_C6678.h"

#elif defined(_C6657)

#include "CPTLib_C6657.h"

#elif defined(_TCI6614) && !defined(RUNTIME_DEVICE_SELECT)

#include "CPTLib_TCI6614.h"

#elif defined(_TCI6612) && !defined(RUNTIME_DEVICE_SELECT)

#include "CPTLib_TCI6612.h"

#elif defined(_C66AK2Hxx)

#include "CPTLib_C66AK2Hxx.h"

#elif defined(_TCI6630K2L)

#include "CPTLib_TCI6630K2L.h"

#elif defined(_66AK2Exx)

#include "CPTLib_66AK2Exx.h"

#elif defined(_66AK2Gxx)

#include "CPTLib_66AK2Gxx.h"

#endif

/*! \par CPT_Handle_t
    CPT Handle object. This is an incomplete structure, thus making the actual implementation 
    private to the CPTLib.
*/

typedef struct _CPT_Handle CPT_Handle_t; 

/*! \par CPT_Handle_Pntr
    Pointer to a CPT Handle object
*/

typedef CPT_Handle_t * CPT_Handle_Pntr;

/*! \par eCPT_Error
    CPT Library error codes
*/

typedef enum { 
    eCPT_Success = 0,                              /*!< Function completed successfully, handle allocated */
    eCPT_Error_Not_Compatible = -1,                /*!< Library and device module not compatible */
    eCPT_Error_Busy =-2 ,                          /*!< CP Tracer module already owned, potential conflict with Debugger*/
    eCPT_Error_Memory_Allocation =-3,              /*!< Memory allocation error */
    eCPT_Error_Module_Enabled = -4,                /*!< Can't change module state while module enabled */
    eCPT_Error_Invalid_Parameter = -5,             /*!< Invalid function parameter */
    eCPT_Error_NULL_STMHandle = -6,                /*!< Invalid function call because library opened with a NULL STM Handle */
    eCPT_Error_MappingError = -8,                  /*!< Attempt to map module's base address failed */
    eCPT_Error_ModuleEnableFailed = -9,            /*!< Module Enable Failed */
    eCPT_Error_ModuleDisableFailed = -10,          /*!< Module Disable Failed */
    eCPT_Error_InvalidHandlePointer = -11,         /*!< Invalid handle Pointer */
    eCPT_Error_DomainPowerUp = -12,                /*!< Error occurred while attempting to power-up the CP Tracer power domain */
    eCPT_Error_DomainEnable = -13,                 /*!< Error occurred while attempting to enable the CP Tracer module */
    eCPT_Error_PowerOrModuleDisabled = -14,        /*!< The domain power is off or the module is disabled. Both the
                                                        domain power and module are enabled by CPT_OpenModule(). If a function
                                                        finds the power has been turned off or the module disabled,
                                                        this error will be returned. */ 
    
    eCPT_Error_STM = -15                           /*!< error value - eCPT_Error_STM provides STMError value */

}eCPT_Error;


#ifdef RUNTIME_DEVICE_SELECT

#include "CPTLib_TCI6612.h"
#include "CPTLib_TCI6614.h"
typedef enum {DEVID_TCI6612, DEVID_TCI6614} eCPT_DeviceID;

eCPT_Error CPT_runtimeConfig(eCPT_DeviceID CPT_deviceID);
eCPT_Error CPT_getModID(char * modName, int * CPT_ModId);
eCPT_Error CPT_getMasterID(char * masterName, int * CPT_MasterId, int * CPT_MasterGroupCnt);
eCPT_Error CPT_getMetaDataSize(CPT_Handle_Pntr const pCPT_Handle, int * num_bytes);
eCPT_Error CPT_getMetaData(CPT_Handle_Pntr const pCPT_Handle, char * buf, int buf_size, int * num_bytes_returned);
eCPT_Error CPT_getDivideByFactor(int CPT_ModId, uint8_t * CPT_DivideByFactor);
eCPT_Error CPT_getModList(const char *** CPT_modList, int * CPT_listCnt);
eCPT_Error CPT_getMasterList(const char *** CPT_masterList, int * CPT_listCnt);

struct CPT_ArmingInfo {
		uint32_t arming_addr;
		uint32_t arming_value;
		uint32_t disarm_value;
};

eCPT_Error CPT_getArmingInfo(CPT_Handle_Pntr const pCPT_Handle, struct CPT_ArmingInfo * pCPT_ArmingInfo );
#endif


/*! \par CPT_CallBack
     \param[in] funcName   Constant char pointer to the function name. Normally provided by the compiler using the __FUNCTION__ macro.
     \param[in] ::eCPT_Error error returned by calling routine
*/
typedef void(*CPT_CallBack)(const char * funcName, eCPT_Error);

/*! \par eCPT_MsgSelects
    STM message generation select options
*/
typedef enum { eCPT_MsgSelect_None = 0,                 /*!< Don't modify STM message export selection  */
               eCPT_MsgSelect_Statistics = 1,           /*!< Select statistic counters for STM messages generation */
               eCPT_MsgSelect_Event_NewReq = 2,         /*!< Select Event B (New Request events) for STM messages generation */
               eCPT_MsgSelect_Event_LastWrt = 4,        /*!< Select Event C (Last write events) for STM messages generation */
               eCPT_MsgSelect_Event_LastRd = 8,         /*!< Select Event E (Last read events) for STM messages generation */
               eCPT_MsgSelect_Event_AccessStatus = 16,   /*!< Select access status for STM messages generation */
               eCPT_MsgSelect_Num = 5
             } eCPT_MsgSelects;

/*! \par eCPT_CntSelects
    Counter select options
*/
typedef enum { eCPT_CntSelect_None = 0,                 /*!< Don't Modify Counter enables */
               eCPT_CntSelect_Grant = 1,                 /*!< Enable grant counter */
               eCPT_CntSelect_Wait = 2,                 /*!< Enable wait counter */
               eCPT_CntSelect_WaitAndGrant = 3,         /*!< Enable wait and grant counters */
               eCPT_CntSelect_Num = 2
             } eCPT_CntSelects;
             
/*! \par eCPT_TrigOuts
    Trigger out select options
*/ 
typedef enum { eCPT_TrigOut_None = 0,                   /*!< Trigger out none - trigger out disabled */
               eCPT_TrigOut_EMU1 = 1,                   /*!< EMU1 Trigger out  */
               eCPT_TrigOut_EMU0 = 2                    /*!< EMU0 Trigger out  */
               
             } eCPT_TrigOut;

/*! \par eCPT_ThroughputCntID
    Counter select options
*/
typedef enum { eCPT_Throughput_Cnt0 = 0,                /*!< Throughput Counter 0 Select */
               eCPT_Throughput_Cnt1 = 1                 /*!< Throughput Counter 1 Select */
             } eCPT_ThroughputCntID;

/*! \par eCPT_MasterState
    Counter select options
*/
typedef enum { eCPT_Mstr_Disable_Grp,                    /*!< Disabled a master group. This disable also works for masters with a single id
                                                              and should be the typical disable used for most cases.
                                                              Master id groups are designated with an _n in the ::eCPT_MasterID table. */
               eCPT_Mstr_Enable_Grp,                     /*!< Enabled a master group. This enable also works for masters with a single id
                                                              and should be the typical enable used for most cases.
                                                              Master id groups are designated with an _n in the ::eCPT_MasterID table. */
               eCPT_Mstr_Disable,                        /*!< Disabled a single master*/
               eCPT_Mstr_Enable,                         /*!< Enabled a single master*/
               eCPT_Mstr_DisableALL,                     /*!< Disable All Masters */
               eCPT_Mstr_EnableAll                       /*!< Enable All Masters */
             } eCPT_MasterState;


/*! \par eCPT_SrcQual
    New Request source selection options. Any source not explicitly excluded is enabled for data capture.
    Note: Some CP Tracer modules DTYPE field are tied off to DMA access. This means that if you exclude
    DMA accesses for these CP Tracer modules you will in effect exclude all access types. See ::eCPT_MasterID
    for details.
*/ 
typedef enum { eCPT_SrcQual_IncludeALL = 0,             /*!< Include all sources */
               eCPT_SrcQual_ExCPUDataAccess = 1,        /*!< Exclude CPU Data Accesses */
               eCPT_SrcQual_ExCPUInstAccess = 2,        /*!< Exclude Inst Access */
               eCPT_SrcQual_ExDMAAccess = 4             /*!< Exclude DMA Access */               
             } eCPT_SrcQual; 
                                                           
/*! \par eCPT_RWQual
    Transaction (read/write) selection options
*/ 
typedef enum { eCPT_RWQual_None,                        /*!< Do not capture read or write transactions */
               eCPT_RWQual_ReadOnly,                    /*!< Capture only read transactions */
               eCPT_RWQual_WriteOnly,                   /*!< Capture only write transactions */ 
               eCPT_RWQual_ReadWrite                    /*!< Capture read and write transactions */
             } eCPT_RWQual;  

/*! \par eCPT_TrigInState
    Counter select options
*/
typedef enum { eCPT_TrigInDisable,                            /*!< EMU Trigger in disable*/
               eCPT_TrigInEnable                              /*!< EMU Trigger in enable*/
             } eCPT_TrigIn;
             
/*! \par CPT_Qualifiers
    Throughput counter qualifiers
*/                                                            
typedef struct _CPT_Qualifiers {
                eCPT_SrcQual NewReqSrcQual;                   /*!< Source qualifiers for Event B (New Request Events) */  
                eCPT_RWQual RWQual;                           /*!< Read/Write qualifiers for transactions */
                } CPT_Qualifiers;
                

/*! \par CPT_TrigQualifiers
    Trigger out qualifiers and enable/disable control
*/                 
typedef struct _CPT_TrigQualifiers{
                eCPT_TrigIn TrigInEnable;                   /*!< Trigger in enable/disable (events occurring
                                                                    between assertion of EMU0 and assertion
                                                                    of EMU1 are qualified. */ 
                eCPT_TrigOut TrigOutEnables;                /*!< Trigger out Enables.  */
                eCPT_SrcQual NewReqSrcQual;                 /*!< Trigger Out source qualifiers for Event B (New Request Events) */  
                eCPT_RWQual RWQual;                         /*!< Trigger Out Read/Write qualifiers for transactions */ 
                } CPT_TrigQualifiers;

/*! \par eCPT_FilterMode
    Address filter inclusive/Exclusive selection
*/                                                            
typedef enum { eCPT_FilterMode_Inclusive,                       /*!< Inclusive address filter*/
               eCPT_FilterMode_Exclusive                        /*!< Exclusive address filter*/
             } eCPT_FilterMode;

/*! \par eCPT_MsgPri
    Message transport priority
*/                                                            
typedef enum { eCPT_MsgPri_0,                       /*!< Priority level 0 - Highest Priority */
               eCPT_MsgPri_1,                       /*!< Priority level 1*/
               eCPT_MsgPri_2,                       /*!< Priority level 2*/
               eCPT_MsgPri_3,                       /*!< Priority level 3*/
               eCPT_MsgPri_4,                       /*!< Priority level 4*/
               eCPT_MsgPri_5,                       /*!< Priority level 5*/
               eCPT_MsgPri_6,                       /*!< Priority level 6*/
               eCPT_MsgPri_7                        /*!< Priority level 7 - Lowest priority, default priority */
             } eCPT_MsgPri; 
             
/*! \par eCPT_AddrMode
    CPT Address Mode
*/ 
typedef enum { eCPT_AddrMode_32bits,                /*!< 32 bit address mode */
               eCPT_AddrMode_36bits,                /*!< 36 bit address mode */
               eCPT_AddrMode_40bits,                /*!< 40 bit address mode */
               eCPT_AddrMode_44bits,                /*!< 44 bit address mode */
               eCPT_AddrMode_48bits                 /*!< 48 bit address mode */
             } eCPT_AddrMode;

/*! \par eCPT_IntStatus
    CPT Interrupt status
*/ 
typedef enum { eCPT_IntStatus_Inactive,             /*!< CPT Interrupt Inactive */
               eCPT_IntStatus_Active                /*!< CPT Interrupt Active */
             } eCPT_IntStatus;



/*! \par eCPT_IntState
    CPT Interrupt state 
*/ 
typedef enum { eCPT_IntMask_Disable,             /*!< CPT Interrupt Disable */
               eCPT_IntMask_Enable               /*!< CPT Interrupt Enable */
             } eCPT_IntMask;

/*! \par eCPT_WaitSelect
    CPT Wait for the next CP Tracer sample window to expire enabled/disable
*/ 
typedef enum {  eCPT_WaitDisable, 
                eCPT_WaitEnable
                }eCPT_WaitSelect;

/*! \par eCPT_UseCase
    Use Case element used by ::CPT_CfgOptions
    
    \n Note - Event messages can be generated with any use case. When only generating event
    messages either the eCPT_UseCase_TotalProfile or eCPT_UseCase_MasterProfile use cases can be used.
*/ 
typedef enum {  eCPT_UseCase_Raw,           /*!< CPT data provided on host will consist of raw CPT messages */
                eCPT_UseCase_TotalProfile,  /*!< The raw CPT messages will be processed into the following data:
                                                         
                                             \li Percentage of total slave activity utilized by selected masters
                                             \li Slave Bus Bandwidth (bytes/second*) utilized by the selected master
                                             \li Average Access Size of slave transactions
                                             \li Bus Utilization (transactions per second*)
                                             \li Contention Percentage
                                             \li Minimum Average Latency
                                                     
                                            \n Note - See CPUClockRateMhz on requirements for reporting time related data (bytes/second)
                                            */
                eCPT_UseCase_MasterProfile,  /*!< The raw CPT messages will be processed into the following data:  
                                                         
                                             \li Slave Bus Bandwidth (bytes/second*) utilized by master 0
                                             \li Slave Bus Bandwidth (bytes/second*) utilized by master 1
                                             \li Average Access Size of slave transactions for the two master groups
                                             \li Bus Utilization (transactions per second*)
                                             \li Contention Percentage
                                             \li Minimum Average Latency
                                                     
                                            \n Note - See CPUClockRateMhz on requirements for reporting time related data (bytes/second)
                                            */
                                            
               eCPT_UseCases_SysBandwidthProfile = 0xFFE1, /*!< This is the System Bandwidth Profile use case.  All data host processing is disabled
                                                except for the Slave Bus Bandwidth.
                                             */
               
               eCPT_UseCases_SysLatencyProfile = 0xF2F1 /*!< This is the System Latency Profile use case.  All data host processing is disabled
                                                except for the Slave Bus Average Latency.
                                             */
               
              } eCPT_UseCase;
               

/*! \par CPT_CfgOptions
    CPT Configuration Options 
*/ 
typedef struct _CPT_CfgOptions {
                bool ForceOwnership;           /*!< If true and another thread or core has ownership (not the debugger) then allow this open
                                                    to take ownership. Default for this parameter is false. */

                CPT_CallBack pCPT_CallBack;    /*!< ::CPT_CallBack is called by any CPT Library function prior to returning an error. May be set to NULL
                                                     to disable this feature. If CPT_OpenModule() is called
                                                     with a NULL CPT_CfgOptions pointer then the default for this parameter is NULL.*/ 
#ifdef _STM_Logging
                bool STM_LogMsgEnable;          /*!< Enable or disable library generated logging messages. The default for this parameter is enabled */ 
                STMHandle * pSTMHandle;         /*!< A pointer to a STM Handle for logging and providing meta data (possibly required by host tools).
                                                     If NULL STM logging is disabled. If CPT_OpenModule() is called
                                                     with a NULL CPT_CfgOptions pointer then the default for this parameter is NULL.*/
                int32_t STMMessageCh;           /*!< A channel to use for CPT Library STM Messages (if pSTMHandle is not NULL) generated by:
                                                    \li CPT_LogMsg() 
                                                    \li CPT_ModuleEnable() 
                                                    \li CPT_ModuleDisable()
                                                */
#endif
                eCPT_UseCase CPT_UseCaseId;      /*!< See ::eCPT_UseCase for details on each use case */
                
                uint32_t SampleWindowSize;       /*!< 32-bit non-zero value used to set the sample window size in CP Tracer clocks. Setting to 0 is not legal
                                                     and will result in CPT_OpenModule() returning ::eCPT_Error_Invalid_Parameter. If CPT_OpenModule() is called
                                                     with a NULL CPT_CfgOptions pointer then the default for this parameter is 64K. 
                                                     This value is provided to host tools through meta data STM messages and is used to calculate throughput
                                                     for statistic messages. */
                uint32_t AddrExportMask;         /*!< 6-bit value (bits 5:0) in the range of 0 to 37 that defines the 10 address bits used in STM message
                                                      generation:
                                                      \li 0 - bits 10:0 are exported
                                                      \li 1 - bits 11:1 are exported
                                                      \li ...
                                                      \li 37 - bits 47:37 are exported \n\n
                                                      Bits 32:6 are don't cares. If CPT_OpenModule() is called
                                                      with a NULL CPT_CfgOptions pointer then the default for this parameter is 0. 
                                                      This value is provided to host tools through meta data STM messages and is used
                                                      to adjust address display.*/ 
                int32_t CPUClockRateMhz;          /*!< CPU clock rate in Mhz. This value is provided to host tools through meta data STM messages and is used
                                                      to calculate throughput. 
                                                      If this value is non-zero the host decoder will convert throughput data to
                                                      Bytes/Second.
                                                      If this value is zero the host decoder will convert throughput data to
                                                      Bytes/CPU-Cycle.  */
                uint32_t DataOptions;                /*!< This is a bit field that enables sw filtering of certain data characteristics.
                                                       Bit
                                                       0 - If 1 suppress zero data, if 0 don't suppress zero data.
                                                           If a CP Tracer is enabled during cycles where no accesses are performed
                                                           this option allows the zero data provided in these cases to be suppressed
                                                           from the CP Tracer data provided on the host.
                                                           This option will have no effect on the actual STM bandwidth (in other words
                                                           all data is transmitted), but it may reduce host file sizes significantly and
                                                           in some cases decrease data processing times significantly.   
                                                    */
             
             }CPT_CfgOptions;


////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Function Definitions
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par CPT_OpenModule         
    
    Open a CPT Library API for a specific CP Tracer module. 
    
    \param[out] pCPT_Handle     A pointer to a NULL ::CPT_Handle_Pntr . If CPT_OpenModule exits
                                with the return value of ::eCPT_Success, pCPT_Handle 
                                is set to a valid CPT_Handle pointer.
    \param[in] CPT_ModId        ::eCPT_ModID is the enumerated identifier for the physical CP tracer module that is opened.
    \param[in] pCPT_CfgOptions  A pointer to the ::CPT_CfgOptions structure. If pCPT_CfgOptions is NULL all parameters are set to their default 
                                values. 
                                
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_Not_Compatible
                                \li ::eCPT_Error_Busy
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_Invalid_Parameter
                                \li ::eCPT_Error_Memory_Allocation
                                \li ::eCPT_Error_DomainPowerUp
                                \li ::eCPT_Error_DomainEnable

    \par Details:
    
    This function must be called and return ::eCPT_Success prior to calling any other CPT Library
    function that requires a ::CPT_Handle_Pntr. 
    If this function is called with a non-NULL pCPT_Handle pointer this function will exit and return
    ::eCPT_Error_InvalidHandlePointer immediately.
    
    Before any CP Tracer registers can be accessed, the DebugSS power domain (includes the CP Tracers)
    must be powered up and enabled. If the domain is powered down the library will attempt
    to power up and enable the domain. The transition (to the power up and enable state)
    is tested using a retry count (CPT_PU_RETRY_CNT). If the number of retries reaches 0 this function
    will exit with ::eCPT_Error_DomainPowerUp or ::eCPT_Error_DomainEnable errors.
     
    If the device's CP Tracer module is not compatible with this version of the library,
    this function will exit and return ::eCPT_Error_Not_Compatible.
    
    If the CP Tracer Module is already owned by the IDE (Debugger) this function will exit and
    return ::eCPT_Error_Busy. This error can occur if the IDE is using the module and has not
    released the CP Tracer module (clear both the Ownership and Destination Registers - set them
    to their reset state). The error can also occur if the ::eCPT_ModID requested is already
    opened (possibly by a different instance of the library), or the client software has changed
    the state of the CPT Register directly. In the case where a ownership conflict is not with the
    IDE, the forceOwnership configuration option can be used to avoid an ownership conflict if the
    users application is simply restarted (not reset). Note that the ownership check is performed
    by every CPT Library function to protect the client in case the IDE forces ownership to itself.
    
    Storage for CPT_Handle is allocated through a call to the client provided external function cTools_memAlloc().
    If a memory allocation error occurs this function returns ::eCPT_Error_Memory_Allocation.
    cTools_memMap(), also a client provided function, is called to map
    the physical CPT Module base address to a virtual address space.
    If a mapping error occurs this function returns ::eCPT_Error_MappingError.    
    The client provided cTools_mutexInit() and cTools_mutexTrylock() functions are called to initialize and
    attempt to lock the requested CP Tracer to this instance of the CPTLib. If the lock fails ::eCPT_Error_Busy is returned.
    See \ref index for client provided Helper Function prototypes used by CPT_OpenModule.
    
    If there are no errors detected the function exits with ::eCPT_Success.  
    
    Base addresses for each supported CP Tracer are provided in device specific include files.
    
    
    If pCPT_CfgOptions is a NULL pointer, the default configuration parameters are:
        \li CPT_CallBack is NULL.
        \li pSTMHandle is NULL.
        \li Sample window size is 64K.
        \li AddrExportMask is 0.
           
    If the function exits with ::eCPT_Success the device's CPT module default values are set to:
        \li Sample window size is set to 0 to leave unit disabled.
        \li Message export address mask set from pCPT_CfgOptions. If pCPT_CfgOptions is NULL the default value is used.
        \li All STM Export Message priorities are set to the lowest priority.
        \li Trigger in and Trigger out functions are disabled (EMU0 and EMU1) and the Trigger out Qualifiers are disabled. 
        \li All Trigger Out (EMU0/1), Throughput Counters and New Request Event (Event B) data selection qualifiers (CPU Data, CPU Inst, DMA)
            and read and write transactions are enabled. These must be explicitly disabled by calls to CPT_CfgQualifiers().
        \li Address filtering disabled.
        \li Access Status pacing is disabled.
        \li The CPT interrupt (on the sample window) is disabled.
        \li No masters enabled for statistic counting or event generation. Must call ::CPT_CfgMaster() to select a master to enable.   
        \li Message export is disabled. Must call ::CPT_ModuleEnable() to start export of CP Tracer STM Messages.

    If a client is satisfied with the default parameters after CPT_OpenModule()is called, the minimum set of functions that must be
    called to enable STM message generation are ::CPT_CfgMaster() and ::CPT_ModuleEnable().
    
    If pCPT_CfgOptions->pSTMHandle is not NULL the CPT Library is capable of generating both client provided
    messages (see CPT_LogMsg())and meta data messages (generated by CPT_ModuleEnable() and CPT_ModuleDisable()).
    Meta data messages provide information needed by data analysis tools (such as the Trace Analyzer).

    If pCPT_CfgOptions->pCPT_CallBack is set to a none NULL value, then any library function that returns an error 
    will call CPT_CallBack()prior to exiting the function.
    
    pCPT_CfgOptions->SampleWindowSize and pCPT_CfgOptions->AddrExportMask can only be set when the library is opened
    to gaurd against modification of these parameters during a STM data recording session. 

*/
#ifndef RUNTIME_DEVICE_SELECT
eCPT_Error CPT_OpenModule(CPT_Handle_Pntr * const pCPT_Handle, const eCPT_ModID CPT_ModId,  
                                                               CPT_CfgOptions const * const pCPT_CfgOptions );
#else
eCPT_Error CPT_OpenModule(CPT_Handle_Pntr * const pCPT_Handle, int CPT_ModId,
                                                               CPT_CfgOptions const * const pCPT_CfgOptions );
#endif

/*! \par CPT_CloseModule

    Close the CPT Library for a specific CP Tracer module. Disable STM export from the CP Tracer module, release ownership, and free the CPT_Handle storage.

    \param[in] pCPT_Handle Pointer to a CPT_Handle provided by CPT_OpenModule().
 
    \return ::eCPT_Error See details for conditions that cause the following errors:
                         \li ::eCPT_Success
                         \li ::eCPT_Error_InvalidHandlePointer
                         \li ::eCPT_Error_PowerOrModuleDisabled
                         \li ::eCPT_Error_Busy
    
    \par Details:

    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return ::eCPT_Error_InvalidHandlePointer immediately and take no other action.    
    If the CP Tracer module is enabled (see CPT_ModuleEnable()), STM data export is disabled (CPT_ModuleDisable() called automatically).        
    Ownership of the CP Tracer module is released.      
    The CPT_Handle is destroyed through a call to the client provided external function STM_memFree.
    The CPT Module Base Address is unmapped through a call to the client provided external function cTools_memUnMap.
    In the event a CPT module error is detected, the CPT_Handle is still destroyed.   
    The pCPT_Handle pointer is also set to NULL, thus making CPT Library functions called with
    this CPT_Handle after CPT_CloseModule is called invalid.
    If the CPT library was opened with a non-null CPT_CallBack function, prior to exiting with any error condition
    the CPT_CallBack function is called.

    This function may be used at any time with a valid pCPT_Handle (provided by CPT_OpenModule()).


    \n Prototype for client provided functions:
    \n void STM_memFree(void *p) 
    \n void STM_memUnMap(void * vAddr, unsigned int mapSizeInBytes) if virtual address mapping not required simply return.
 
*/

eCPT_Error CPT_CloseModule(CPT_Handle_Pntr * const pCPT_Handle );

/*! \par CPT_GetVersion

    Get CPT Library version and the Physical Module's version.

    \param[in] pCPT_Handle Pointer to a CPT_Handle provided by CPT_OpenModule(). 
    \param[out] pLibMajorVersion CPT Library Major Version (API modifications not backward compatible)  
    \param[out] pLibMinorVersion CPT Library Minor Version (Backward compatible modifications and bug fixes)
    \param[out] pSWFuncID The device�s CMI Module Functional ID the library is compatible with
    \param[out] pHwFuncID The actual device's CMI Module Functional ID
    \return ::eCPT_Error See details for conditions that cause the following errors:
                         \li ::eCPT_Success
                         \li ::eCPT_Error_InvalidHandlePointer
                         \li ::eCPT_Error_PowerOrModuleDisabled
    
    \par Details:
    
    This function provides the major and minor version numbers of the CPT library.
    These values can be compared with the following macro values in CPT_Common.h to confirm that the
    version of the library matches that of the header file.

    \li ::CPTLIB_MAJOR_VERSION
    \li ::CPTLIB_MINOR_VERSION

    The pCMISWFuncID value must match the pCMIHwFuncID value for a call to CMI_OpenModule not to
    return ::eCPT_Error_Not_Compatible error. 
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
    
    This function can be called while the CP Tracer is enabled (see CPT_ModuleEnable()) or disabled (see CPT_ModuleDisable()).
*/

eCPT_Error CPT_GetVersion(CPT_Handle_Pntr const pCPT_Handle, uint32_t * const pLibMajorVersion, 
                                                             uint32_t * const pLibMinorVersion,
                                                             uint32_t * const pSWFuncID, 
                                                             uint32_t * const pHwFuncID );

/*! \par CPT_ModuleEnable

    Enable the CP Tracer module's STM message export and counters.

    \param[in] pCPT_Handle Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] CPT_MsgEnables ::eCPT_MsgSelects provides selection for statistic and event STM messages enables
    \param[in] CPT_CntEnables ::eCPT_CntSelects provide selection of counters to enable. 
    \return ::eCPT_Error See details for conditions that cause the following errors:
                         \li ::eCPT_Success
                         \li ::eCPT_Error_InvalidHandlePointer
                         \li ::eCPT_Error_PowerOrModuleDisabled
                         \li ::eCPT_Error_Module_Enabled
                         \li ::eCPT_Error_Busy
    
    \par Details:
    
    Multiple ::eCPT_MsgSelects can be or'ed together to select multiple message sources at the same time. Multiple ::eCPT_CntSelects can
    be or'ed together to select multiple counters. 
    Any ::eCPT_MsgSelects or ::eCPT_CntSelects not selected are cleared (disabled).
    
    This is normally the last command issued to enable the CP Tracer counters, start STM message generation (if enabled) for statistic and event messages.
    
    Sample window size is set to the value provided CPT_openModule() through pCPT_CfgOptions to enable the physical unit.
    If pCPT_CfgOptions was NULL the default value is used (see CPT_openModule()).
        
    Parameters that must be set before calling this function are:
    
    \li Masters selected (see CPT_CfgMaster() )
    \li Qualifiers if any required (see CPT_setQualifiers()
    \li Filters if any required (see CPT_setAddressFilter())
         
     If CPT_OpenModule() was called with a non-NULL pCPT_CfgOptions->pSTMHandle,
     and CPT_CfgOptions->STM_LogMsgEnable is enabled,
     the appropriate CPT logging messages will be transported
     for each active ::eCPT_MsgSelects and ::eCPT_CntSelects: 
    \n Module "CPT", Data Message:"Statistic counters msg gen enabled", Type: "Message", Domain: "CP_TracerName", Class "CPTLib Message"
    \n Module "CPT", Data Message:"New Request event msg gen enabled", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    \n Module "CPT", Data Message:"Last Write event msg gen enabled ", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    \n Module "CPT", Data Message:"Last Read event msg gen enabled", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    \n Module "CPT", Data Message:"Access Status msg gen enabled", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    \n Module "CPT", Data Message:"Wait Counter enabled", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    \n Module "CPT", Data Message:"Grant Counter enabled", Type: "Message", Domain: "CP_TracerName", Class "CPT Message"
    
    If CPT_OpenModule() was called with a non-NULL pCPT_CfgOptions->pSTMHandle, the following CPT meta data messages will be transported:   
    \n Module: "CPTLib", Data Message:"CP Tracer UID, SID, major ver, minor ver, sampleWindowValue, AddrExportMask, CPUClockRateMhz", Type: "Meta", Domain: "CP Tracer Name", Class NULL

    The module is then enabled for activity collection, STM message generation if enabled and trigger outs if enabled.
    
    This function may be used at any time the physical CPT module is disabled (see CPT_ModuleDisable()).
    If the CP Tracer is already enabled this function will exit and return ::eCPT_Error_Module_Enabled. 
    
    During a STM message data recording session the client may call CPT_ModuleEnable() and CPT_ModuleDisable() as needed to restrict 
    CP Tracer STM data export to just those times where the CP Tracer data is relevant. If capturing data to a circular buffer, it's
    also advisable to disable the CP Tracer STM data export once the observation period is complete, stopping the flow of CP Tracer
    STM data coordinated with the event being monitored. 
    This will cause the meta data to be re-broadcast to allow for recovery if the meta data exported by the last call to 
    CPT_ModuleEnable() was lost due to the capture buffer wrapping, which is very likely in the ETB case. 

    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
*/

eCPT_Error  CPT_ModuleEnable (CPT_Handle_Pntr const pCPT_Handle, const eCPT_MsgSelects CPT_MsgEnables, 
                                                                 const eCPT_CntSelects CPT_CntEnables );

/*! \par CPT_ModuleDisable

    Disable the CP Tracer module's STM message export and counters.

    \param[in] pCPT_Handle Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] CPT_WaitSelect ::eCPT_WaitSelect provides selection to disable or
                              enable waiting for the next sample window to expire prior to disabling the CP Tracer. 
    \return ::eCPT_Error See details for conditions that cause the following errors:
                         \li ::eCPT_Success
                         \li ::eCPT_Error_InvalidHandlePointer
                         \li ::eCPT_Error_PowerOrModuleDisabled
                         \li ::eCPT_Error_Busy
    
    \par Details:
    
    If CPT_WaitSelect is set to eCPT_WaitEnable then prior to disabling the CP tracer, this function will spin in a wait
    loop until it detects the next expired sample window. This will guarantee the last CP tracer data is exported before
    the CP Tracer unit is disabled. The maximum number of cycles this function will wait for the sample window to expire
    is the sample window size multiplied by the CP tracers clock divide-by-factor(either 2 0r 3), so this function could
    be in the spin loop for a very long time (thus the user option to enable or disable this capability). 
    To disable the unit the sample window size is set to 0 disabling all STM message generation and counters. This function must be called
    prior to any CPT_cfgXXXX functions that changes the selected CP Tracer's programming.  
         
    If CPT_OpenModule() was called with a non-NULL pCPT_CfgOptions->pSTMHandle, 
    and CPT_CfgOptions->STM_LogMsgEnable is enabled,
    the following CPT logging messages will be transported.
    
    \n Module "CPT", Data Message:"Msg generation disabled", Type: Message, Domain: "CP_TracerName", Class "CPT Message"

    If CPT_OpenModule was called with a non-NULL pCPT_CfgOptions->pSTMHandle a CPT meta data message is transported.   
 
    The module is then disabled and STM message generation is suspended. 
    
    This function may be used at any time the CP Tracer is enabled (see CPT_ModuleDisable()). If the CP Tracer is already
    disabled this function does nothing and returns ::eCPT_Success.
    
    During a STM message data recording session the client may call CPT_ModuleEnable() and CPT_ModuleDisable() as needed to restrict 
    CP Tracer STM data export to just those times where the CP Tracer data is relevant. If capturing data to a circular buffer, it's
    also advisable to disable the CP Tracer STM data export once the observation period is complete, stopping the flow of CP Tracer
    STM data coordinated with the event being monitored. 
    This will cause the meta data to be re-broadcast to allow for recovery if the meta data exported by the last call to 
    CPT_ModuleEnable() was lost due to the capture buffer wrapping, which is very likely in the ETB case. 
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action. 
*/
eCPT_Error  CPT_ModuleDisable (CPT_Handle_Pntr const pCPT_Handle, eCPT_WaitSelect CPT_WaitSelect );

/*! \par CPT_CfgMaster

    Select a master to enable or disable for a Throughput Counter. Note that the master selections for Throughput Counter 0 also
    apply to New Request events and Access Status events.  
    
    \param[in] pCPT_Handle          Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] CPT_MasterID         ::eCPT_MasterID provides master id selection for throughput counting. Master Ids selected for
                                    Throughput Counter 0 also apply to Event B (New Request) and Event E (Last Read). 
    \param[in] CPT_ThroughputCnt    ::eCPT_ThroughputCntID provides Throughput Counter selection the CPT_MasterID is associated with.
                                  
    \param[in] CPT_MasterState      ::eCPT_MasterState provides enable or disable selection for a single bus master id or a bus master id 
                                    group (in both cases designated by CPT_MasterID) or can provide enable/disable of all bus masters
                                    (CPT_MasterID ignored). A master id group is a special case bus master that utilize more than one
                                    master id for different types of accesses. To accommodate these eCPT_MasterState also has 
                                    provisions for enabling/disabling all the masters in the group (eCPT_Mstr_Disable_Grp/eCPT_Mstr_Enable_Grp).
                                    Master id groups are designated with an _GrpN suffix in the ::eCPT_MasterID table. When modifying the state 
                                    (eCPT_Mstr_Disable_Grp/eCPT_Mstr_Enable_Grp) of a group id the "_Grp0" master id must be specified with
                                    the CPT_MasterID parameter.
                                    Using the group state selection (enabling or disabling)is the typical case and will work for masters
                                    that have just a single id (no _GrpN suffix).
    \return ::eCPT_Error            See details for conditions that cause the following errors:
                                    \li ::eCPT_Success
                                    \li ::eCPT_Error_InvalidHandlePointer
                                    \li ::eCPT_Error_PowerOrModuleDisabled
                                    \li ::eCPT_Error_Module_Enabled
                                    \li ::eCPT_Error_Busy
    
    \par Details:
        
    The default state, set by a successful call to CPT_OpenModule, is all masters are disabled for statistic
    counting and event generation.
    
    This function may be called multiple times to enable/disable more than one master. Enabling or disabling
    a master does not effect the state of other masters. 

    Note that not every master can physically access every slave. This function makes no attempt to
    protect the client from enabling masters that are invalid for the specific slave. If a statistic counter
    does not advance when enabled then the reason can be either the selected master is not generating the
    expected cycles or the selected master can't reach the slave.
        
    This function may only be called while the CP Tracer is disabled.          
    If the CP Tracer is already enabled ( see CPT_ModuleEnable()) the Master parameters will not be updated and
    this function will exit and return ::eCPT_Error_Module_Enabled.    

    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action. 
*/
#ifndef RUNTIME_DEVICE_SELECT
eCPT_Error CPT_CfgMaster( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MasterID CPT_MasterID, 
                                                             const eCPT_ThroughputCntID CPT_ThroughputCnt, 
                                                             const eCPT_MasterState CPT_MasterState );
#else
eCPT_Error CPT_CfgMaster( CPT_Handle_Pntr const pCPT_Handle, const int CPT_MasterID,
                                                             const eCPT_ThroughputCntID CPT_ThroughputCnt,
                                                             const eCPT_MasterState CPT_MasterState );
#endif

/*! \par CPT_CfgQualifiers

    Select qualifiers for Throughput Counters(0 and 1), New Request Events and Trigger out (on EMU0/1 pins). 
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] pCPT_TPCnt0Qual Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure provides
                               Event B (New Request) Data Qualifiers (CPU Data, CPU Inst, DMA) and read/write transaction
                               qualifiers for Throughput Counter 0.
    \param[in] pCPT_TPCnt1Qual Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure provides
                               Event B (New Request) Data Qualifiers (CPU Data, CPU Inst, DMA) and read/write transaction
                               qualifiers for Throughput Counter 1.
    \param[in] pCPT_TPEventQual Pointer to a ::CPT_Qualifiers structure. A CPT_Qualifiers structure provides
                               New Request Data Qualifiers (CPU Data, CPU Inst, DMA) and read/write transaction
                               qualifiers for New Request Event generation.
    \param[in] pCPT_TrigQual   Pointer to a ::CPT_TrigQualifiers structure.  A CPT_TrigQualifiers structure provides
                               enable/disable control of trigger inputs and outputs (using EMU0/1 signals),
                               and provides New Request Data Qualifiers (CPU Data, COU Inst, DAM) and
                               read/write transaction qualifiers for trigger outputs.
                              
    \return ::eCPT_Error       See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Module_Enabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    The default state, set by a successful call to CPT_OpenModule, is all dtype qualifiers are enabled, 
    all read/write qualifiers are enabled,  EMU0/1 start/stop qualification is disabled. 
        
    For a non-NULL argument structure, any qualifier that is not explicitly excluded through one of the
    structure elements will be enabled. 
    For a NULL argument structure, the qualifiers are not modified.           
    
    This function may only be called while the CP Tracer is disabled.
    If the CP Tracer is already enabled ( see CPT_ModuleActivityEnable())the Qualifier parameters will not be updated and
    this function will exit and return ::eCPT_Error_Module_Enabled.    

    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
    
    For some CP Tracers there are filtering limitations built into the implementation. Limitations are 
    defined per ::eCPT_MasterID.
*/

eCPT_Error CPT_CfgQualifiers( CPT_Handle_Pntr const pCPT_Handle,  CPT_Qualifiers const * const pCPT_TPCnt0Qual,
                                                                  CPT_Qualifiers const * const pCPT_TPCnt1Qual, 
                                                                  CPT_Qualifiers const * const pCPT_TPEventQual,
                                                                  CPT_TrigQualifiers const * const pCPT_TrigQual );

/*! \par CPT_CfgAddrFilter

    Modify address filters for Throughput Counters(0 and 1), New Request Events and Trigger out (on EMU0/1 pins). 
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] AddrFilterMSBs  Address filter MSBs (address bits 63:32). The bits used must be valid for the CP Tracer slave (See CPT_getAddrMode()). 
                               These bits are applied to both the start and end address.
    \param[in] StartAddrFilterLSBs Start address filter LSBs (address bits 31:0).
    \param[in] EndAddrFilterLSBs End address filter LSBs (address bits 31:0).
    \param[in] CPT_FilterMode ::eCPT_FilterMode provides selection for inclusive/exclusive address filter modes.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_Invalid_Parameter
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Module_Enabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    The default state, set by a successful call to CPT_OpenModule, is all address filters are disabled.
    
    Setting the EndAddrFilterLSBs to zero disables address filtering. Setting the EndAddrFilter to a non-zero value enables
    address filtering. 
    If the AddrFilterMSBs are set outside the range defined for the CP Tracer slave (from address mode range - see CPT_GetAddrMode()),
    the Address Filter parameters will not be updated and the function will exit with ::eCPT_Error_Invalid_Parameter.         
    
    This function may only be called while the CP Tracer is disabled.
    If the CP Tracer is already enabled ( see CPT_ModuleActivityEnable()) the Address Filter parameters will not be updated
    and this function will exit and return ::eCPT_Error_Module_Enabled.    
 
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
*/

eCPT_Error CPT_CfgAddrFilter( CPT_Handle_Pntr const pCPT_Handle, const uint32_t AddrFilterMSBs, 
                                                                 const uint32_t StartAddrFilterLSBs, 
                                                                 const uint32_t EndAddrFilterLSBs,
                                                                 const eCPT_FilterMode CPT_FilterMode );

/*! \par CPT_CfgPacing

    Configure Access Status pacing.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] PacingCntValue  24-bit value to set pacing counter. 
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_Invalid_Parameter
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Module_Enabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    The default Access Status Pacing state, set by a successful call to CPT_OpenModule, is disabled (Pacing Counter is set to zero).
    
    If PacingCntValue is set to zero, the Access Status STM messages are exported periodically as the sample window expires. 
    If the PacingCntValue is a non-zero value that is less than the sample window size, then the Access Status STM messages
    are exported periodically as the pacing counter expires (at a rate set by the PacingCntValue). In this case when the
    sample window expires an additional Access Status message is generated and the pacing counter restarted.
    In the case the PacingCntValue is greater than the Sample Window size, Access Status messages are only generated
    periodically as the sample window expires.
    
    If the PacingCntValue exceeds 24 bits the PacingCntValue will not be updated and the function will exit 
    with ::eCPT_Error_Invalid_Parameter.
    
    This function may only be called while the CP Tracer is disabled.
    If the CP Tracer is already enabled ( see CPT_ModuleActivityEnable()) the Pacing parameter will not be updated
    and this function will exit and return ::eCPT_Error_Module_Enabled. 
 
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
    
*/
eCPT_Error CPT_CfgPacing ( CPT_Handle_Pntr const pCPT_Handle, const uint32_t PacingCntValue );

/*! \par CPT_CfgMsgPriority

    Configure STM message priorities.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] CPT_StatisticMsgsPri  ::eCPT_MsgPri provides priority level for Statistic messages (from counters).
    \param[in] CPT_NewRequestEventMsgPri ::eCPT_MsgPri provides priority level for Event B (New Request Event) messages.
    \param[in] CPT_LastWriteEventMsgPri ::eCPT_MsgPri provides priority level for Event C (Last Write Event) messages.
    \param[in] CPT_LastReadEventMsgPri ::eCPT_MsgPri provides priority level for Event E (Last Read Event) messages.
    \param[in] CPT_AccessStatusMsgPri  ::eCPT_MsgPri provides priority level for Access Status messages.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Module_Enabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    The default priority for all message types, set by a successful call to CPT_OpenModule, is the lowest.
    
    This function may only be called while the CP Tracer is disabled.
    If the CP Tracer is already enabled ( see CPT_ModuleActivityEnable()) the STM message priorities will not be updated
    and this function will exit and return ::eCPT_Error_Module_Enabled. 
 
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action. 
*/
eCPT_Error CPT_CfgMsgPriority ( CPT_Handle_Pntr const pCPT_Handle, const eCPT_MsgPri CPT_StatisticMsgsPri,
                                                                   const eCPT_MsgPri CPT_NewRequestEventMsgPri,
                                                                   const eCPT_MsgPri CPT_LastWriteEventMsgPri,
                                                                   const eCPT_MsgPri CPT_LastReadEventMsgPri,
                                                                   const eCPT_MsgPri CPT_AccessStatusMsgPri );

/*! \par CPT_GetAddrMode

    Get Address mode for the specific CP Tracer.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[out] pAddrModeRange  ::eCPT_AddrMode provides the available address mode returned for the specific CP trace module.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Busy
                              
    
    \par Details:
    
    This function may be used at any time.
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action. 
 
*/


eCPT_Error CPT_GetAddrMode( CPT_Handle_Pntr const pCPT_Handle,  eCPT_AddrMode * const pAddrModeRange );

/*! \par CPT_GetCurrentState

    Get the current state of the specific CP Tracer's counters and Access Status.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[out] pCPT_ThroughPutCnt0  Return location for the current Throughput 0 Counter value.
    \param[out] pCPT_ThroughPutCnt1  Return location for the current Throughput 1 Counter value.
    \param[out] pCPT_WaitCnt    Return location for the current Wait Count (in CP Tracer clocks) value
    \param[out] pCPT_GrantCnt  Return location for the current Grant Counter value.
    \param[out] pAccessStatus  Return location for the current Access Status value.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Busy
                             
    \par Details:
    
    This function may be used at any time.
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.

    Note that this function provides values relative to the last sample window expiration and the values are read atomically 
    (with interrupts disabled). If all the registers are read while the current sample window expires the
    context of the registers will not match. So it's recommended that this function be called from an ISR (see CPT_CfgInt()) that:
    \li  Call CPT_ClrIntStatus() to clear the current CPT interrupt status
    \li  Call CPT_GetCurrentState() to get the current state values
    \li  Call CPT_GetIntStatus() to confirm confirm the sample window has not expired (eCPT_IntStatus_Inactive returned).
    If the call to CPT_GetIntStatus() returns eCPT_IntStatus_Active (indicates the sample window expired) either the
    sample window is set to small or your interrupt was delayed suffiecently to lose the current state context,
    in which case a retry of CPT_GetCurrentState() would be appropriate.
    
    Note: You can't check the sample counter register because this value is really just the counter's reload value.
     
*/

eCPT_Error CPT_GetCurrentState ( CPT_Handle_Pntr const pCPT_Handle, uint32_t * const pCPT_ThroughPutCnt0,
                                                                   uint32_t * const pCPT_ThroughPutCnt1,
                                                                   uint32_t * const pCPT_WaitCnt,
                                                                   uint32_t * const pCPT_GrantCnt, 
                                                                   uint32_t * const pAccessStatus );
/*! \par CPT_CfgInt

    Configure the CP Tracer for interrupt generation.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[in] CPT_IntMask     ::eCPT_IntMask provides interrupt disable and enable selection.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Busy
                            
    
    \par Details:
    
    The default CPT interrupt state, set by a successful call to CPT_OpenModule, is disabled.
    
    This function only provides access to the local CP tracer state. This function assumes the client has integrated appropriate
    CP Tracer interrupt setup and service handling into their application. 
    
    This function may be used at any time.

    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action. 
*/

eCPT_Error CPT_CfgInt  ( CPT_Handle_Pntr const pCPT_Handle, const eCPT_IntMask CPT_IntMask);
                                                             
/*! \par CPT_GetIntStatus

    Get the current interrupt status (active or inactive) of the specific CP Tracer.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
    \param[out] pCPT_IntStatus An ::eCPT_IntStatus value is returned reflecting the state of the CP Tracer interrupt.
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    This function only provides access to the local CP tracer state. This function assumes the client has integrated appropriate
    CP Tracer interrupt setup and service handling into their application. 
 
    This function may be used at any time.
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
*/
eCPT_Error CPT_GetIntStatus ( CPT_Handle_Pntr const pCPT_Handle, eCPT_IntStatus * const pCPT_IntStatus);

/*! \par CPT_ClrIntStatus

    Clear the current interrupt status of the specific CP Tracer.
     
    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule().
                              
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_Busy
    
    \par Details:
    
    This function only provides access to the local CP tracer state. This function assumes the client has integrated appropriate
    CP Tracer interrupt setup and service handling into their application.
    
    This function may be used at any time.
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
 
*/
eCPT_Error CPT_ClrIntStatus ( CPT_Handle_Pntr const pCPT_Handle );

#ifdef _STM_Logging
/*! \par CPT_LogMsg

    Issue a user provided CP Tracer specific STM message. 

    \param[in] pCPT_Handle     Pointer to a CPT_Handle provided by CPT_OpenModule(). 
    \param[in] FmtString       Format string (like printf format string) limited to a single int or unsigned int conversion character.
    \param[in] pValue          Pointer to formatted value. NULL if not used in format string.
    \return ::eCPT_Error        See details for conditions that cause the following errors:
                                \li ::eCPT_Success
                                \li ::eCPT_Error_InvalidHandlePointer
                                \li ::eCPT_Error_PowerOrModuleDisabled
                                \li ::eCPT_Error_STM
    
    \par Details:
    
    The following information will be provided in the CPT client STM message:
    \li Module - Pointer to the module name "CPTLib" . 
    \li Data Domain  - Pointer to the domain name, in this case it's the CPT Tracer's name string.
    \li Data Class   - Pointer to the class name, in this case it's "CPT User Message".
    \li Data Type    - Pointer to the data type, in this case it's "Message".
    \li Data Message - Pointer to the FmtString.
    \li value - If pValue is not NULL, value is provided.

    If CPT_OpenModule() is opened with a NULL STMHandle, this function exits
    and returns ::eCPT_Error_NULL_STMHandle.
    
    Note: _STMLogging must be defined for this function to be included by the complier.
    
    This function may be used at any time.
    
    If this function is called with a NULL pCPT_Handle pointer or the contents of the handle are not valid 
    this function will return eCPT_Error_InvalidHandlePointer immediately and take no other action.
    
    If a STM Library error is returned this function will exit with CPT_Error_STM + STM_Error.
    
*/

eCPT_Error  CPT_LogMsg(CPT_Handle_Pntr const pCPT_Handle, const char * FmtString, uint32_t * const pValue);

#endif



#ifdef __cplusplus
}
#endif

#endif /* __CPTLIBARY_H */
