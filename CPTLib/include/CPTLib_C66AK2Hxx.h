#ifndef __CPTLIBRARY_C66AK2Hxx_H
#define __CPTLIBRARY_C66AK2Hxx_H
/*
 * CPTLib_C66AK2Hxx.h
 *
 * Common Platform (CP) Tracer Library C66AK2Hxx device specific definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <stdint.h>              // The library uses C99 exact-width integer types 

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CPTLib_C66AK2Hxx.h
    C66AK2Hxx specific CP Tracer modules definitions
	Definitions include all Keystone2 devices (TCI6634K2K, TCI6636K2H, TCI6638K2K and 66AK2Hxx)
*/

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Public Typedefs and Enums
//
////////////////////////////////////////////////////////////////////////////////////////////////

/*! \par eCPT_ModID
    CP Tracer module ids
*/

// These are in SID order               
typedef enum {  eCPT_MSMC_0,             /*!< CP Tracer MSMC 0 module*/
                eCPT_MSMC_1,             /*!< CP Tracer MSMC 1 module*/
                eCPT_MSMC_2,             /*!< CP Tracer MSMC 2 module*/
                eCPT_MSMC_3,             /*!< CP Tracer MSMC 3 module*/
                eCPT_QM_MST,             /*!< CP Tracer Queue Manager Master module*/
                eCPT_DDR3A,              /*!< CP Tracer DDR3A module*/
                eCPT_SM,                 /*!< CP Tracer Semaphore module*/
                eCPT_QM_CFG1,            /*!< CP Tracer Queue Manager 1 Priority module*/
                eCPT_SCR3_CFG,           /*!< CP Tracer SCR3 Configuration module*/
                eCPT_L2_0,               /*!< CP Tracer L2 0 Memory Controller module*/
                eCPT_L2_1,               /*!< CP Tracer L2 1 Memory Controller module*/
                eCPT_L2_2,               /*!< CP Tracer L2 2 Memory Controller module*/
                eCPT_L2_3,               /*!< CP Tracer L2 3 Memory Controller module*/
                eCPT_L2_4,               /*!< CP Tracer L2 4 Memory Controller module*/
                eCPT_L2_5,               /*!< CP Tracer L2 5 Memory Controller module*/
                eCPT_L2_6,               /*!< CP Tracer L2 6 Memory Controller module*/
                eCPT_L2_7,               /*!< CP Tracer L2 7 Memory Controller module*/
                eCPT_RAC,                /*!< CP Tracer Receiver Accelerator Coprocessor module*/
                eCPT_RAC_CFG1,           /*!< CP Tracer Receiver Accelerator Coprocessor Configuration 1 module*/
                eCPT_TAC,                /*!< CP Tracer Transmit Accelerator Coprocessor module*/
                eCPT_QM_CFG2,            /*!< CP Tracer Queue Manager 2 Priority module*/
                eCPT_RAC_CFG2,           /*!< CP Tracer Receiver Accelerator Coprocessor Configuration 2 module*/
                eCPT_DDR3B,              /*!< CP Tracer DDR3B module*/
                eCPT_BCR_CFG,            /*!< CP Tracer BCR configuration module*/
                eCPT_TPCC_0_4,           /*!< CP Tracer EDMA3 TPCC0 and EDMA3 TPCC4 module*/
                eCPT_TPCC_1_2_3,         /*!< CP Tracer EDMA3 TPCC1, EDMA3 TPCC2 and EDMA3 TPCC3 module*/
                eCPT_INTC_CFG,           /*!< CP Tracer Interrupt Controller (INTC) configuration module*/
                eCPT_MSMC_4,             /*!< CP Tracer MSMC 4 module*/
				eCPT_MSMC_5,             /*!< CP Tracer MSMC 5 module*/
				eCPT_MSMC_6,             /*!< CP Tracer MSMC 6 module*/
				eCPT_MSMC_7,             /*!< CP Tracer MSMC 7 module*/
				eCPT_SPI_ROM_EMIF16,     /*!< CP Tracer SPI, ROM and EMIF16 modules*/
                eCPT_ModID_Last
               } eCPT_ModID;        

/*! \par eCPT_MasterID
    CP Tracer master ids
    
    The following table defines the list of masters that can be enabled for throughput counting and 
    New Request events.
    
    Note that some masters consist of a group of IDs designated with "_GrpN" suffix. For most situations 
    enabling the entire group rather than a single group is the typical use case. See CPT_CfgMaster()
    for details.
	
	Also note that master ids 140-167 and 174-177 correspond to the 32 CP tracer masters on Kepler2. These 
	CP tracer masters are connected to the STM module via a private interconnect. We intentionally did 
	not define these master IDs, because the transactions from these masters have no significance from 
	an application SW point of view. Also, the transactions from these masters cannot be traced at any 
	available CP tracers.
                                                                                                                       
    \par <DQR> Data Qualifier Restriction for certain Masters
    The dtype qualifier for all non-MSMC CP Tracers is tied off to the DMA value. This means that for these CP Tracers, if you
    exclude DMA cycles (see ::eCPT_SrcQual) all data accesses are filtered.
    
*/

// Master IDs 
typedef enum { 
	            eCPT_MID_GEM0,                      /*!< GEM0 */
	            eCPT_MID_GEM1,                      /*!< GEM1 */
	            eCPT_MID_GEM2,                      /*!< GEM2 */
	            eCPT_MID_GEM3,                      /*!< GEM3 */
	            eCPT_MID_GEM4,                      /*!< GEM4 */
	            eCPT_MID_GEM5,                      /*!< GEM5 */
	            eCPT_MID_GEM6,                      /*!< GEM6 */
	            eCPT_MID_GEM7,                      /*!< GEM7 */
	            eCPT_MID_A15_CPU0_0,                /*!< ARM A15 CPU0, Not applicable for TCI6634K2K */
				eCPT_MID_A15_CPU1_0,                /*!< ARM A15 CPU1, Not applicable for TCI6634K2K */
				eCPT_MID_A15_CPU2_0,                /*!< ARM A15 CPU2, Not applicable for TCI6634K2K */
				eCPT_MID_A15_CPU3_0,                /*!< ARM A15 CPU3, Not applicable for TCI6634K2K */
				eCPT_MID_reserved12,                
				eCPT_MID_reserved13,                
				eCPT_MID_reserved14,                
				eCPT_MID_reserved15,                
				eCPT_MID_GEM0_CFG,                  /*!< GEM0 CFG */
				eCPT_MID_GEM1_CFG,                  /*!< GEM1 CFG */
				eCPT_MID_GEM2_CFG,                  /*!< GEM2 CFG */
				eCPT_MID_GEM3_CFG,                  /*!< GEM3 CFG */
				eCPT_MID_GEM4_CFG,                  /*!< GEM4 CFG */
				eCPT_MID_GEM5_CFG,                  /*!< GEM5 CFG */
				eCPT_MID_GEM6_CFG,                  /*!< GEM6 CFG */
				eCPT_MID_GEM7_CFG,                  /*!< GEM7 CFG */
				eCPT_MID_reserved24,
                eCPT_MID_EDMA0_TC0_RD,              /*!< EDMA0 TC0 Read */
                eCPT_MID_EDMA0_TC0_WR,              /*!< EDMA0 TC0 Write */
                eCPT_MID_EDMA0_TC1_RD,              /*!< EDMA0 TC1 Read */
                eCPT_MID_HyperBridge0,              /*!< Hyperbridge0 */
                eCPT_MID_HyperBridge1,              /*!< Hyperbridge1 */
                eCPT_MID_SRIO_M,                    /*!< SRIO Master */
                eCPT_MID_PCIe,                      /*!< PCIe Master */
                eCPT_MID_EDMA0_TC1_WR,              /*!< EDMA0 TC1 Write */
                eCPT_MID_EDMA1_TC0_RD,              /*!< EDMA1 TC0 Read */
                eCPT_MID_EDMA1_TC0_WR,              /*!< EDMA1 TC0 Write */
                eCPT_MID_EDMA1_TC1_RD,              /*!< EDMA1 TC1 Read */
                eCPT_MID_EDMA1_TC1_WR,              /*!< EDMA1 TC1 Write */                
                eCPT_MID_EDMA1_TC2_RD,              /*!< EDMA1 TC2 Read */
                eCPT_MID_EDMA1_TC2_WR,              /*!< EDMA1 TC2 Write */
                eCPT_MID_EDMA1_TC3_RD,              /*!< EDMA1 TC3 Read */
                eCPT_MID_EDMA1_TC3_WR,              /*!< EDMA1 TC3 Write */
                eCPT_MID_EDMA2_TC0_RD,              /*!< EDMA2 TC0 Read */
                eCPT_MID_EDMA2_TC0_WR,              /*!< EDMA2 TC0 Write */
                eCPT_MID_EDMA2_TC1_RD,              /*!< EDMA2 TC1 Read */
                eCPT_MID_EDMA2_TC1_WR,              /*!< EDMA2 TC1 Write */                
                eCPT_MID_EDMA2_TC2_RD,              /*!< EDMA2 TC2 Read */
                eCPT_MID_EDMA2_TC2_WR,              /*!< EDMA2 TC2 Write */
                eCPT_MID_EDMA2_TC3_RD,              /*!< EDMA2 TC3 Read */
                eCPT_MID_EDMA2_TC3_WR,              /*!< EDMA2 TC3 Write */
                eCPT_MID_EDMA3_TC0_RD,              /*!< EDMA3 TC0 Read */
				eCPT_MID_EDMA3_TC0_WR,              /*!< EDMA3 TC0 Write */
				eCPT_MID_EDMA3_TC1_RD,              /*!< EDMA3 TC1 Read */
				eCPT_MID_MSMC,                      /*!< MSMC (note- for transactions initiated by MSMC internally and sent to the DDR) */
				eCPT_MID_EDMA3_TC1_WR,              /*!< EDMA3 TC1 Write */
				/* SRIO PKTDMA master group */
				eCPT_MID_SRIO_PKTDMA_Grp0,          /*!< SRIO_PKTDMA - master 0*/
				eCPT_MID_SRIO_PKTDMA_Grp1,          /*!< SRIO_PKTDMA - master 1*/
				eCPT_MID_FFTC_A,                    /*!< FFTC_A, Not applicable for 66AK2Hxx devices */
				eCPT_MID_FFTC_B,                    /*!< FFTC_B, Not applicable for 66AK2Hxx devices  */
				eCPT_MID_RAC_B_BE0,                 /*!< RAC_B_BE0, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_B_BE1,                 /*!< RAC_B_BE1, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_A_BE0,                 /*!< RAC_A_BE0, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_A_BE1,                 /*!< RAC_A_BE1, Not applicable for 66AK2Hxx devices */
				eCPT_MID_TPCC0,                     /*!< EDMA3 TPCC0 */
				eCPT_MID_TPCC1,                     /*!< EDMA3 TPCC1 */
				eCPT_MID_TPCC2,                     /*!< EDMA3 TPCC2 */
				eCPT_MID_FFTC_C,                    /*!< FFTC_C, Not applicable for 66AK2Hxx devices  */
				eCPT_MID_reserved66,                
				eCPT_MID_FFTC_D,                    /*!< FFTC_D, Not applicable for 66AK2Hxx devices  */
				/* QM Secondary CDMA master group */
				eCPT_MID_QM_second_Grp0,          /*!< QM Secondary CDMA - master 0 */
				eCPT_MID_QM_second_Grp1,          /*!< QM Secondary CDMA - master 1 */
				eCPT_MID_QM_second_Grp2,          /*!< QM Secondary CDMA - master 2 */
				eCPT_MID_QM_second_Grp3,          /*!< QM Secondary CDMA - master 3 */
				/* AIF master group  */
				eCPT_MID_AIF_Grp0,                  /*!< AIF - master 0, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp1,                  /*!< AIF - master 1, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp2,                  /*!< AIF - master 2, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp3,                  /*!< AIF - master 3, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp4,                  /*!< AIF - master 4, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp5,                  /*!< AIF - master 5, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp6,                  /*!< AIF - master 6, Not applicable for 66AK2Hxx devices */
				eCPT_MID_AIF_Grp7,                  /*!< AIF - master 7, Not applicable for 66AK2Hxx devices */
				eCPT_MID_reserved80,                
				eCPT_MID_BCP_DIO0,                  /*!< BCP Direct IO 0, Not applicable for 66AK2Hxx devices  */
				eCPT_MID_BCP_DIO1,                  /*!< BCP Direct IO 1, Not applicable for 66AK2Hxx devices  */
				eCPT_MID_TPCC3,                     /*!< EDMA3 TPCC3 */
				/* XGE (10 GIG Ethernet) master group */
				eCPT_MID_XGE_Grp0,                  /*!< XGE (10 GIG Ethernet) master 0 */
				eCPT_MID_XGE_Grp1,                  /*!< XGE (10 GIG Ethernet) master 1 */
				eCPT_MID_XGE_Grp2,                  /*!< XGE (10 GIG Ethernet) master 2 */
				eCPT_MID_XGE_Grp3,                  /*!< XGE (10 GIG Ethernet) master 3 */
				eCPT_MID_RAC_C_BE0,                 /*!< RAC_C_BE0, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_C_BE1,                 /*!< RAC_C_BE1, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_D_BE0,                 /*!< RAC_D_BE0, Not applicable for 66AK2Hxx devices */
				eCPT_MID_RAC_D_BE1,                 /*!< RAC_D_BE1, Not applicable for 66AK2Hxx devices */
				/* QM2_CDMA master group */
				eCPT_MID_QM2_CDMA_Grp0,              /*!< QM2_CDMA - master 0 */
				eCPT_MID_QM2_CDMA_Grp1,              /*!< QM2_CDMA - master 1 */
				eCPT_MID_QM2_CDMA_Grp2,              /*!< QM2_CDMA - master 2 */
				eCPT_MID_QM2_CDMA_Grp3,              /*!< QM2_CDMA - master 3 */
				/* QM1_CDMA master group */
				eCPT_MID_QM1_CDMA_Grp0,              /*!< QM1_CDMA - master 0 */
				eCPT_MID_QM1_CDMA_Grp1,              /*!< QM1_CDMA - master 1 */
				eCPT_MID_QM1_CDMA_Grp2,              /*!< QM1_CDMA - master 2 */
				eCPT_MID_QM1_CDMA_Grp3,              /*!< QM1_CDMA - master 3 */
                eCPT_MID_reserved100,
                eCPT_MID_reserved101,
                eCPT_MID_BCP_CDMA,                   /*!< BCP CDMA master, Not applicable for 66AK2Hxx devices */
                eCPT_MID_TAC_FEI0,                   /*!< TAC FEI0 master, Not applicable for 66AK2Hxx devices */
                eCPT_MID_TAC_FEI1,                   /*!< TAC FEI1 master, Not applicable for 66AK2Hxx devices */
                eCPT_MID_FFTC_E,                     /*!< FFTC_E, Not applicable for 66AK2Hxx devices  */
                eCPT_MID_FFTC_F,                     /*!< FFTC_F, Not applicable for 66AK2Hxx devices  */
                eCPT_MID_DAP,                        /*!< DAP  */
                eCPT_MID_USB = 168,                  /*!< USB  */
                eCPT_MID_EDMA4_TC0_RD,               /*!< EDMA4 TC0 Read */
				eCPT_MID_EDMA4_TC0_WR,               /*!< EDMA4 TC0 Write */
				eCPT_MID_EDMA4_TC1_RD,               /*!< EDMA4 TC1 Read */
				eCPT_MID_EDMA4_TC1_WR,               /*!< EDMA4 TC1 Write */
				eCPT_MID_TPCC4,                      /*!< EDMA3 TPCC4 */
				eCPT_MID_TAC_FEI2 = 179,             /*!< TAC FEI2 master, Not applicable for 66AK2Hxx devices */
				/* NETCP master group */
				eCPT_MID_NETCP_Grp0,                 /*!< NETCP - master 0 */
				eCPT_MID_NETCP_Grp1,                 /*!< NETCP - master 1 */
				eCPT_MID_NETCP_Grp2,                 /*!< NETCP - master 2 */
				eCPT_MID_NETCP_Grp3,                 /*!< NETCP - master 3 */
                eCPT_MID_Cnt = 256
            } eCPT_MasterID;

#ifdef __cplusplus
}
#endif

#endif /* __CPTLIBARY_C66AK2Hxx_H */
