/*
 * aet_timer_trigger.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =======================aet_timer_trigger.c============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
#include <aet_timer_trigger.h>
#include <c6x.h>

extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_timerZeroTrigger(AET_jobParams* params)
{
	AET_timerTriggerResources resources;
	AET_error status = AET_SOK;
	_AET_triggerBuilderInfo tbInfoStartCounter;
	_AET_triggerBuilderInfo tbInfoStartPCTrace;
	_AET_triggerBuilderInfo tbInfoStopPCTrace;
	_AET_triggerBuilderInfo tbInfoReloadCounter;

	/* 
	 Initialize the configword in the resources structure 
	 */
	_AET_initConfigWord(&resources.configWord);

	/* 
	 Select the trigger builders that the job will use
	 */

	/*
	 * Start Counter 0
	 */
	tbInfoStartCounter.tbType = _AET_TB_1_WIDE;
	tbInfoStartCounter.triggerBuilderNo = 1;
	tbInfoStartCounter.outputCntl = 0;

	/*
	 * Start Trace
	 */
	tbInfoStartPCTrace.tbType = _AET_TB_7_WIDE;
	tbInfoStartPCTrace.triggerBuilderNo = 1;
	tbInfoStartPCTrace.outputCntl = 0;

	/*
	 * Stop Trace
	 */
	tbInfoStopPCTrace.tbType = _AET_TB_3_WIDE;
	tbInfoStopPCTrace.triggerBuilderNo = 0;
	tbInfoStopPCTrace.outputCntl = 0;

	/*
	 * Stop/Reload Counter 0
	 */
	tbInfoReloadCounter.tbType = _AET_TB_1_WIDE;
	tbInfoReloadCounter.triggerBuilderNo = 2;
	tbInfoReloadCounter.outputCntl = 0;

	resources.trigBldrStartPCTrace = &tbInfoStartPCTrace;
	resources.trigBldrStopPCTrace = &tbInfoStopPCTrace;
	resources.trigBldrStartCounter = &tbInfoStartCounter;
	resources.trigBldrReloadCounter = &tbInfoReloadCounter;

	/* 
	 Set the trigger Builder bits in the configWord
	 */

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoStartCounter.tbType,
				tbInfoStartCounter.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoStartPCTrace.tbType,
				tbInfoStartPCTrace.triggerBuilderNo
				);
	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoStopPCTrace.tbType,
				tbInfoStopPCTrace.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoReloadCounter.tbType,
				tbInfoReloadCounter.triggerBuilderNo
				);


	/*
	 Reserve the resources that we need.  This will fail if the resources are
	 not available.
	 */
	if (status = _AET_timerZeroTriggerProgram(&resources, params))
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}

	/*
	 Insert the used resources into the job table and set the params->jobIndex
	 */
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);
	
	return status;
}

AET_error _AET_timerZeroTriggerProgram(
				AET_timerTriggerResources* resources,
				AET_jobParams* params
				)
{
	AET_error status = AET_SOK;
	_AET_tbCntlParams tbParamsStartCounter = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsStartPCTrace = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsStopPCTrace = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsReloadCounter = AET_TBCNTLPARAMS;

	_AET_PRINTF("---Begin Job Timer Zero Trigger---\n");

	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();

	/*
	 Get the base index of the trigger builder that is used for this job.  The
	 base address for aq given trigger builder is the address of the TB_ORS
	 (sometimes called TB_BES_ register.  The indexes for all of the other 
	 trigger builders are derived by the base index and an offset.
	 The table below shows the address of all of the trigger builder registers
	 */

	/* 
	----------------------------------------------------------------------------
					Trigger Builder Register Indexes 
	----------------------------------------------------------------------------
	| REGISTER NAME |  INDEX  |                  COMMENT 					   |
	|---------------------------------------------------------------------------
    | 					STATE MACHINE TRIGGERS                                 |
	|--------------------------------------------------------------------------|
	| SM_ORS_0W_0	|  0xA4	 | State Machine TB0 ORS Register	  			   |
	| SM_CNTL_0W_1	|  0xA5  | State Machine TB0 Trig 1 Control  			   |
	| SM_CEXP_0W_2  |  0xA6  | State Machine TB0 Trig 2-3 Control   		   |
	| SM_ORS_1W_0	|  0xA7	 | State Machine TB1 ORS Register	   			   |
	| SM_CNTL_1W_1	|  0xA8  | State Machine TB1 Trig 1 Control 			   |
	| SM_CEXP_1W_2  |  0xA9  | State Machine TB1 Trig 2-3 Control 			   |
	| SM_ORS_2W_0	|  0xAA	 | State Machine TB2 ORS Register	   			   |
	| SM_CNTL_2W_1	|  0xAB  | State Machine TB2 Trig 1 Control  			   |
	| SM_CEXP_2W_2  |  0xAC  | State Machine TB2 Trig 2-3 Control  			   |
	| SM_ORS_3W_0	|  0xAD	 | State Machine TB3 ORS Register	   			   |
	| SM_CNTL_3W_1	|  0xAE  | State Machine TB3 Trig 1 Control  			   |
	| SM_CEXP_3W_2  |  0xAF  | State Machine TB3 Trig 2-3 Control  			   |
	|--------------------------------------------------------------------------|
    | 					  WIDE TRIGGER BUILDERS                                |
	|--------------------------------------------------------------------------|
	| TB_W0_ORS_0W_0  | 0xC0 | 7-Wide TB0 Ors Register						   |
	| TB_W0_CNTL_0W_1 | 0xC1 | 7-Wide TB0 Trigger 1 Control					   |
	| TB_W0_CEXT_0W_2 | 0xC2 | 7-Wide TB0 Trigger 2-3 Control				   |
	| TB_W0_CEXT_0W_4 | 0xC3 | 7-Wide TB0 Trigger 4-5 Control				   |
	| TB_W0_CEXT_0W_6 | 0xC4 | 7-Wide TB0 Trigger 6-7 Control				   |
	| TB_W0_ORS_1W_0  | 0xC8 | 7-Wide TB1 Ors Register					   	   |
	| TB_W0_CNTL_1W_1 | 0xC9 | 7-Wide TB1 Trigger 1 Control					   |
	| TB_W0_CEXT_1W_2 | 0xCA | 7-Wide TB1 Trigger 2-3 Control				   |
	| TB_W0_CEXT_1W_4 | 0xCB | 7-Wide TB1 Trigger 4-5 Control				   |
	| TB_W0_CEXT_1W_6 | 0xCC | 7-Wide TB1 Trigger 6-7 Control				   |	  
	| TB1_ORS_2W_0  |  0xD0  | 3-Wide TB2 Ors Register						   |
	| TB1_CNTL_2W_1 |  0xD1  | 3-Wide TB2 Trigger 1 Control					   |
	| TB1_CEXT_2W_2 |  0xD2  | 3-Wide TB2 Trigger 2-3 Control				   |
	| TB1_ORS_3W_0  |  0xD0  | 3-Wide TB3 Ors Register						   |
	| TB1_CNTL_3W_1 |  0xD1  | 3-Wide TB3 Trigger 1 Control					   |
	| TB1_CEXT_3W_2 |  0xD2  | 3-Wide TB3 Trigger 2-3 Control				   |
	|--------------------------------------------------------------------------|
    | 					  1-BIT TRIGGER BUILDERS                               |
	|--------------------------------------------------------------------------|
	| TB_ORS_0      | 0x140  | 1-Wide TB0 Ors Register						   |
	| TB_CNTL_0		| 0x141  | 1-Wide TB0 Control Register					   |
	| TB_ORS_1      | 0x142  | 1-Wide TB1 Ors Register						   |
	| TB_CNTL_1		| 0x143  | 1-Wide TB1 Control Register					   |
	| TB_ORS_2      | 0x144  | 1-Wide TB2 Ors Register						   |
	| TB_CNTL_2		| 0x145  | 1-Wide TB2 Control Register					   |
	| TB_ORS_3      | 0x146  | 1-Wide TB3 Ors Register						   |
	| TB_CNTL_3		| 0x147  | 1-Wide TB3 Control Register					   |
	| TB_ORS_4      | 0x148  | 1-Wide TB4 Ors Register						   |
	| TB_CNTL_4		| 0x149  | 1-Wide TB4 Control Register					   |
	| TB_ORS_5      | 0x14A  | 1-Wide TB5 Ors Register						   |
	| TB_CNTL_5		| 0x14B  | 1-Wide TB5 Control Register					   |
	|--------------------------------------------------------------------------| 
	*/
	
	tbParamsStartCounter.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldrStartCounter->tbType,
			resources->trigBldrStartCounter->triggerBuilderNo
	);




	/* TB_CNTL  (Start/Advance Counter Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsStartCounter.tbOrsValue = 0;
	tbParamsStartCounter.tbType = resources->trigBldrStartCounter->tbType;
	tbParamsStartCounter.outputCntl = resources->trigBldrStartCounter->outputCntl;
	tbParamsStartCounter.triggers = 0x1;
	tbParamsStartCounter.boolOutput = 0xFFFF;
	_AET_pgmTrigBldr(
					&tbParamsStartCounter,
					params
					);

	tbParamsStartPCTrace.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldrStartPCTrace->tbType,
			resources->trigBldrStartPCTrace->triggerBuilderNo
		);




	/* TB_CNTL  (Start PC Trace Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsStartPCTrace.tbOrsValue = 0;
	tbParamsStartPCTrace.tbType = resources->trigBldrStartPCTrace->tbType;
	tbParamsStartPCTrace.outputCntl = resources->trigBldrStartPCTrace->outputCntl;
	tbParamsStartPCTrace.triggers = params->traceTriggers;
	tbParamsStartPCTrace.boolOutput = 0xF0F0;
	tbParamsStartPCTrace.evtCSelect = AET_TB_ESAC_CNT0;
	_AET_pgmTrigBldr(
					&tbParamsStartPCTrace,
					params
					);

	tbParamsStopPCTrace.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldrStopPCTrace->tbType,
			resources->trigBldrStopPCTrace->triggerBuilderNo
		);

	/* TB_CNTL  (Stop PC Trace Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsStopPCTrace.tbOrsValue = 0;
	tbParamsStopPCTrace.tbType = resources->trigBldrStopPCTrace->tbType;
	tbParamsStopPCTrace.outputCntl = resources->trigBldrStopPCTrace->outputCntl;
	tbParamsStopPCTrace.triggers = 0x1;
	tbParamsStopPCTrace.boolOutput = 0xF0F0;
	tbParamsStopPCTrace.evtCSelect = AET_TB_ESAC_CNT0;
	_AET_pgmTrigBldr(
					&tbParamsStopPCTrace,
					params
					);

	tbParamsReloadCounter.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrReloadCounter->tbType,
			resources->trigBldrReloadCounter->triggerBuilderNo
		);





	/* TB_CNTL  (Store Trace Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsReloadCounter.tbOrsValue = 0;
	tbParamsReloadCounter.tbType = resources->trigBldrReloadCounter->tbType;
	tbParamsReloadCounter.outputCntl = resources->trigBldrReloadCounter->outputCntl;
	tbParamsReloadCounter.triggers = 0x1;
	tbParamsReloadCounter.boolOutput = 0xF0F0;
	tbParamsReloadCounter.evtCSelect = AET_TB_ESAC_CNT0;
	_AET_pgmTrigBldr(
					&tbParamsReloadCounter,
					params
					);


	/* TB_DM */
	_AET_programTbDomainReg(resources->trigBldrStartCounter->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrStartPCTrace->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrStopPCTrace->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrReloadCounter->triggerBuilderNo, 0);
	/*
	 Add this trigger builder into the list of trigger builders to be enabled,
	 and enable them all
	 */
	if ((status = _AET_markEnableTb(resources->trigBldrStartCounter)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrStartPCTrace)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrStopPCTrace)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrReloadCounter)
								) != AET_SOK)
	{
		return status;
	}
	_AET_PRINTF("-----End Job Timer Zero Trigger-----\n");

	return status;
}
