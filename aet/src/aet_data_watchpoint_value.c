/*
 * aet_data_watchpoint_value.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ====================aet_data_watchpoint_value.c============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_data_watchpoint_value.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_dataWatchpointWithValue(AET_jobParams* params)
{
	AET_dataWatchpointWithValueResources resources;			
	AET_error status = AET_SOK;



	if ((status = _AET_jobInit(&resources.configWord,
								params->triggerType,
								&resources.trigBldr 
								))
								!= AET_SOK)
	{
		return status;
	}


	/* 
	 Select the comparators that the job will use
	 */
	if ((status = _AET_getAcmpWithDataNo((uint8_t)2, 
										  &resources.acmpNo[0], 
										  &resources.dcmpNo[0],
										  &resources.configWord
										  )) != AET_SOK)
		return status;





	/*
	 Reserve the resources that we need.  This will fail if the resources are 
	 not available.
	 */
	if ((status = _AET_reserveResources(
					&resources.configWord,
					params)
					) != AET_SOK)
		return status;
	/*
	 Program the Data Watchpoint job using the resources specified
	 */
	if ((status = _AET_dataWpWithValueProgram(&resources, params)) 
		!= AET_SOK)
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}
	/*
	 Insert this job into the global job table and store the job number
	 into the parameter structure passed by the user
	 */

	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);

	return status;

}

AET_error _AET_dataWpWithValueProgram(
				AET_dataWatchpointWithValueResources* resources,
				AET_jobParams* params
				)
{
	uint16_t aetDcmp1BaseIndex;
	uint16_t aetDcmp2BaseIndex;
	_AET_drcCntlParams drcParams1 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams2 = AET_DRCCNTLPARAMS;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;
	AET_error status = AET_SOK;

	drcParams1.readWrite = drcParams2.readWrite = params->readWrite;

	/* For a watchpoint with data, we need an Address comparator from each
	   pair, and the associated data qualifier. 
	*/

	_AET_PRINTF("---Begin Job Data Watchpoint with Value---\n");
	
	/* Disable All Trigger Builders */
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, TB_ENA), 0x0);

	/* 
	 Get the base addresses for all of the resources we're going to
	 use
	 */
	
	drcParams1.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[0]);
	drcParams2.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[1]);
	aetDcmp1BaseIndex = _AET_getDcmpBaseIndex(resources->dcmpNo[0]);
	aetDcmp2BaseIndex = _AET_getDcmpBaseIndex(resources->dcmpNo[1]);

		/*
	 aetTbBaseIndex is the base address of the Trigger Builder Registers
	 */
	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldr->tbType,
			resources->trigBldr->triggerBuilderNo
		);

	/* 
	 Program Address Comparator 1 Registers 
	 */

	/* Get the correct Byte Enable value depending on the parameters passed */
	drcParams1.byteEnableMask =
	drcParams2.byteEnableMask = 
		_AET_getByteEnableValue(
			params->dataAddress,
			params->refSize
			);


	/* DRC_NA_CNTL for the first Address Comparator */
	/* First Data Address Comparator */
	/* reference Size is selected by user */
	/* Qualify the read/write with specific data */
	/* User selects whether to watch for a read or write */
	/* Byte enable value selected by refSize and access size */
	/* 0xA = Event Sense (local only) */
	drcParams1.dataQualify = AET_DRC_DATAQUAL_QUALIFY;
	drcParams1.eventSenseMask = 0xA;
	drcParams1.cmpGreater = AET_CMP_FALSE;
	drcParams1.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(
				&drcParams1,
				params
				);


	/* DRC_AREF for the first Address Comparator */
	drcParams1.addressReference = params->dataAddress;
	_AET_pgmDrcArefReg(&drcParams1);

	
	/* DRC_DREFL for the first Data Comparator */
	/* Data value specified by the user (low) */
	_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 2),
					AET_FMK(AET_IAR_DCQ_DREFL_DREFL, 
						((int32_t)(params->value & 0xFFFFFFFF)))
						);

	/* DRC_DREFH for the first Data Comparator */
	/* Data value specified by the user (high) */ 
	_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 4),
					AET_FMK(AET_IAR_DCQ_DREFH_DREFH, 
						((int32_t)((params->value >> 32) & 0xFFFFFFFF)))
					);

	/* DRC_MREFL for the first Data Comparator */
	/* Mask value (low) */
	_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 3),
					AET_FMK(AET_IAR_DCQ_MREFL_MREFL,
						((int32_t)(params->valueMask & 0xFFFFFFFF)))
					);

	/* DRC_MREFH for the first Data Comparator */
	/* Mask value  (high) */
	_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 5),
					AET_FMK(AET_IAR_DCQ_MREFH_MREFH,
						((int32_t)((params->valueMask >> 32) & 0xFFFFFFFF)))
					);

	/* 
	 Program Address Comparator 2 Registers 
	 */
	/* DRC_NA_CNTL for the second Address Comparator */
	/* Second Data Address Comparator */
	/* reference Size is selected by user */
	/* Qualify the read/write with specific data */
	/* User selects whether to watch for a read or write */
	/* Byte enable value selected by refSize and access size */
	/* 0xA - Event Sense (local only) */
	drcParams2.dataQualify = AET_DRC_DATAQUAL_QUALIFY;
	drcParams2.eventSenseMask = 0xA;
	drcParams2.cmpGreater = AET_CMP_FALSE;
	drcParams2.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(	
				&drcParams2,
				params
				);

	/* DRC_AREF for the first Address Comparator */
	drcParams2.addressReference = params->dataAddress;
	_AET_pgmDrcArefReg(&drcParams2);

	
	/* DRC_DREFL for the first Data Comparator */
	/* Data value specified by the user (low) */
	_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 2),
					AET_FMK(AET_IAR_DCQ_DREFL_DREFL, 
						((int32_t)(params->value & 0xFFFFFFFF)))
						);

	/* DRC_DREFH for the first Data Comparator */
	/* Data value specified by the user (high) */ 
	_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 4),
					AET_FMK(AET_IAR_DCQ_DREFH_DREFH, 
						((int32_t)((params->value >> 32) & 0xFFFFFFFF)))
					);

	/* DRC_MREFL for the first Data Comparator */
	/* Mask value (low) */
	_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 3),
					AET_FMK(AET_IAR_DCQ_MREFL_MREFL, 
						((int32_t)(params->valueMask & 0xFFFFFFFF)))
					);

	/* DRC_MREFH for the first Data Comparator */
	/* Mask value  (high) */
	_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 5),
					AET_FMK(AET_IAR_DCQ_MREFH_MREFH, 
						((int32_t)((params->valueMask >> 32) & 0xFFFFFFFF)))
					);




	/* TB_CNTL */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Base index of the trigger builder */
	/* Trigger Builder Output Control */
	/* 0xAAAA is Trigger Builder lookup tabl (A input only) */
	/* Number of the trigger builder of the specified type */
	tbParams.tbOrsValue = 
		AET_FMK(AET_IAR_TB_ORS_MASK_A,
				1 << resources->acmpNo[0]) | (1 << resources->acmpNo[1]
				);
	tbParams.tbType = resources->trigBldr->tbType;
	tbParams.outputCntl = resources->trigBldr->outputCntl;
	tbParams.triggers = 1 << (resources->trigBldr->triggerNo - 1);
	tbParams.boolOutput = 0xAAAA;
	tbParams.tbNo = resources->trigBldr->triggerBuilderNo;	
	_AET_pgmTrigBldr(
				&tbParams,
				params
				);



	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);

	/* CIS_BUS_SEL */
	/* Set the input to the appropriate comparators to be the appropriate busses */
	_AET_globalParams.globRegs.cisBusSel &= ~(1 << (4 * resources->acmpNo[0]));
	_AET_globalParams.globRegs.cisBusSel |= (1 << (4 * resources->acmpNo[1]));

	/* Clear Out the Existing Data register entries */
	_AET_globalParams.globRegs.cisBusSel &= 0xFFFFFF33;

	switch (params->readWrite)
	{
		case AET_WATCH_READ:	
			_AET_globalParams.globRegs.cisBusSel |= 0xC8;
			break;
		case AET_WATCH_WRITE:
			_AET_globalParams.globRegs.cisBusSel |= 0x40;
			break;
		default:
			return AET_FINVALIDREADWRITETYPE;
	}

	/* Write the comparator input selector */
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo, 0);

	/* 
	 Add this trigger builder into the list of trigger builders to be enabled, 
	 and enable them all
	 */
	if ((status =_AET_markEnableTb(resources->trigBldr)
								   ) != AET_SOK)
	{
		return status;
	} 

	_AET_PRINTF("End Job Data Watchpoint with Value\n");

	return AET_SOK;
}


								
