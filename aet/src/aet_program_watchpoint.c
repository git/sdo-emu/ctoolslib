/*
 * aet_program_watchpoint.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =======================aet_program_watchpoint.c============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_program_watchpoint.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_programWatchpoint(AET_jobParams* params)
{
	AET_programWatchpointResources resources;
	uint8_t acmpNo;
	AET_error status = AET_SOK;


	if ((status = _AET_jobInit(&resources.configWord,
								params->triggerType,
								&resources.trigBldr 
								))
								!= AET_SOK)
	{
		return status;
	}

	/* 
	 Select the Comparator that the job will use 
	 */
	if ((status = _AET_getAcmpNo((uint8_t)1, &acmpNo)) != AET_SOK){
		return status;
	}
	

	/* 
	 Fill in the appropriate field in the configWord with the address comparator 
	 selected
	 */		
	resources.configWord.ACMP |= (1 << acmpNo);
	resources.acmpNo = acmpNo;


	/* 
	 Reserve the Resources that we need.  This will fail if the resources are 
	 not available
	 */
	if ((status = _AET_reserveResources(
			&resources.configWord,
			params)
			)!= AET_SOK)
		return status;

	/* 
	 Program the Program Watchpoint job using the resources specified
	 */
	if ((status = _AET_pgmWpProgram(&resources, params)) != AET_SOK)
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);
	return status;

}

AET_error _AET_pgmWpProgram(AET_programWatchpointResources* resources,
							AET_jobParams* params
							)
{
	AET_error status = AET_SOK;
	_AET_drcCntlParams drcParams1 = AET_DRCCNTLPARAMS;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;

	_AET_PRINTF("---Begin Job Program Watchpoint---\n");
	/* Disable All Trigger Builders */;
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, TB_ENA), 0x0);


	/* 
	 aetCmpBaseIndex is the base address of the Comparator Registers
	 This depends on the comparator that is selected and is used in 
	 the programmation of all of the registers used for this comparator
	 */
	drcParams1.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo);

	/*
	 aetTbBaseIndex is the base address of the Trigger Builder Registers
	 This depends on the trigger builder selected and is used to program
	 all of the registers associated with this trigger builder
	 */
	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldr->tbType,
			resources->trigBldr->triggerBuilderNo
		);

	/* DRC_NA_CNTL REGISTER
	Used to configure the Dual Range Comparator
	
	-----------------------------------------------------------------
	|3|3|2|2|2|2|2|2|2|2|2|2|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|0|0|0|0|0|       
	|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|
	|	                            |               |       | | | | |
	|          QUALIFIER            |     BYTE      | EVENT |G|L|E|S| 
	|           LOOKUP              |    ENABLE     | SENSE |T|T|C|C|
	|           [15:00]             |    [7:0]      | [3:0] | | | |D|
	-----------------------------------------------------------------
	GT - Greater Than
	LT - Less Than
	EC - Event Control
	SCD - Select Clock Domain

	    ================================================================
		QUALIFIER LOOKUP [15:00]
		-----------------------------------------------------------------
		| 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
		| 5 | 4 | 3 | 2 | 1 | 0 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
		|           |   |   |       |   |   |   |   |   |               |
		|  RESERVED | I | E |  REF  | E | S | E | D | R |    ACCESS     |
		|           | N | X |       | X | T | T | Q | / |               |
		|           | A | A |       | E | A | Y |   | W |               |
		|           | S | C |       |   | L | P |   |   |               |
		|           | E | T |       |   | L | E |   |   |               |
		|           | N |   |       |   |   |   |   |   |               |
		|           | S |   |       |   |   |   |   |   |               |
		|           | E |   |       |   |   |   |   |   |               |
		-----------------------------------------------------------------
		INASENSE - 	Specifies whether the event sense output should be 
					considered active high or active low
					0 - ACTIVE HIGH
					1 - ACTIVE LOW

		EXACT - Defines the exactness of the Address comparison
					0 - Byte touching address comparison method used
					1 - Exact address comparison method used

		REF - Defines the size of the address and data reference values
					00b - Byte
					01b - Half-Word
					10b - Word
					11b - Double Word

		EXE - Defines how qualification of PC bus events with single 
			  instruction dispatches is handled
					0 - Masks the qualification of PC bus events with single
						instruction dispatches
					1 - Permits the qualification of PC bus events with single
						instruction dispatches.

		STALL - Defines how qualification of PC bus events with active pipeline
				cycles is handled
					0 - Permits qualification of events with active pipeline 
						cycles.
					1 - Masks qualification of events with active pipeline 
						cycles.

		DQ - (Data Qualification) - Defines how qualification with the DCQ 
			 register compare is handled with the address compare.
					0 - Masks qualification of the DCQ with address compare.
					1 - Permits qualification of the DCQ with address compare.

		R/W - Defines whether the comparator detects read or write operations.
					0 - Detect Write operation
					1 - Detect Read operation

		ACCESS - Defines specific types of accesses that the comparator 
				 will trigger on

				 0000 - No Accesses
				 0001 - Byte
				 0010 - Halfword
				 0011 - Halfword, Byte
				 0100 - Word
				 0101 - Word, Byte
				 0110 - Word, Halfword
				 0111 - Word, Halfword, Byte
				 1000 - Doubleword
				 1001 - Doubleword, Byte
				 1010 - Doubleword, Halfword
				 1011 - Doubleword, Halfword, Byte
				 1100 - Doubleword, Word
				 1101 - Doubleword, Word, Byte
				 1110 - Doubleword, Word, Halfword
				 1111 - Doubleword, Word, Halfword, Byte
		================================================================

		================================================================
		BYTE ENABLE [7:0]
		================================================================
		This 8-bit field selects when the double word address is 
		declared equal.  These eight bits are programmed to control the
		equals, greater than, and less than conditions for the 
		comparison window.

		TABLE KEY
		---------
		B = BYTES ACCESSED
		R = REFERENCE SIZE
        ===============================================================
		|   COMPARISON   |   ALIGNMENT   |   REFERENCE   |   BE[7:0]  |
		|    CRITERIA    |               |     SIZE      |            |
        |================|===============|===============|============|
        |     B > R      |      00       |     BYTE      |  11111110  |
        |     B > R      |      00       |      HW       |  11111100  |
        |     B > R      |      00       |     WORD      |  11110000  |
        |     B > R      |      00       |      DW       |  00000000  |
        |     B > R      |      01       |     BYTE      |  11111100  |
        |     B > R      |      01       |      HW       |  11111000  |
        |     B > R      |      01       |     WORD      |  11100000  |
        |     B > R      |      01       |      DW       |  00000000  |
        |     B > R      |      10       |     BYTE      |  11111000  |
        |     B > R      |      10       |      HW       |  11110000  |
        |     B > R      |      10       |     WORD      |  11000000  |
        |     B > R      |      10       |      DW       |  00000000  |
        |     B > R      |      11       |     BYTE      |  11110000  |
        |     B > R      |      11       |      HW       |  11100000  |
        |     B > R      |      11       |     WORD      |  10000000  |
        |     B > R      |      11       |      DW       |  00000000  |
        |    B >= R      |      00       |  BYTE HW FW   |  11111111  |
        |    B >= R      |      01       |  BYTE HW FW   |  11111110  |
        |    B >= R      |      10       |  BYTE HW FW   |  11111100  |
        |    B >= R      |      11       |  BYTE HW FW   |  11111000  |
        |   B=R, B!=R    |      00       |     BYTE      |  00000001  |
        |   B=R, B!=R    |      00       |      HW       |  00000011  |
        |   B=R, B!=R    |      00       |     WORD      |  00001111  |
        |   B=R, B!=R    |      00       |      DW       |  11111111  |             
        |   B=R, B!=R    |      01       |     BYTE      |  00000010  |
        |   B=R, B!=R    |      01       |      HW       |  00000110  |
        |   B=R, B!=R    |      01       |     WORD      |  00011110  |
        |   B=R, B!=R    |      01       |      DW       |  11111110  |  
        |   B=R, B!=R    |      10       |     BYTE      |  00000100  |
        |   B=R, B!=R    |      10       |      HW       |  00001100  |
        |   B=R, B!=R    |      10       |     WORD      |  00111100  |
        |   B=R, B!=R    |      10       |      DW       |  11111100  |             
        |   B=R, B!=R    |      11       |     BYTE      |  00001000  |
        |   B=R, B!=R    |      11       |      HW       |  00011000  |
        |   B=R, B!=R    |      11       |     WORD      |  01111000  |
        |   B=R, B!=R    |      11       |      DW       |  11111000  |  
        |    B <= R      |      00       |     BYTE      |  00000001  |
        |    B <= R      |      00       |      HW       |  00000011  |
        |    B <= R      |      00       |     WORD      |  00001111  |
        |    B <= R      |      00       |      DW       |  11111111  |
        |    B <= R      |      01       |     BYTE      |  00000011  |
        |    B <= R      |      01       |      HW       |  00000111  |
        |    B <= R      |      01       |     WORD      |  00011111  |
        |    B <= R      |      01       |      DW       |  11111111  |
        |    B <= R      |      10       |     BYTE      |  00000111  |
        |    B <= R      |      10       |      HW       |  00001111  |
        |    B <= R      |      10       |     WORD      |  00111111  |
        |    B <= R      |      10       |      DW       |  11111111  |
        |    B <= R      |      11       |     BYTE      |  00001111  |
        |    B <= R      |      11       |      HW       |  00011111  |
        |    B <= R      |      11       |     WORD      |  01111111  |
        |    B <= R      |      11       |      DW       |  11111111  |
        |     B < R      |      00       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      01       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      10       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      10       |BYTE HW WORD DW|  11111110  |
        ---------------------------------------------------------------  

		===============================================================
		EVENT SENSE [3:0]
		===============================================================
		This 4-bit field is used a s lookup table to create the      
		comparator output.  This 4-bit field is used to generate the
		input to a 4 to 1 multiplexer whose select controls are the 
		compare out of the two drc comparators.  This field controls
		the sense of the magnitude comparator output, along with the 
		relationship of both comparators of the DRC pair.

		LOOKUP 			REMOTE			LOCAL 
		VALUE		  COMPARISON	  COMPARISON
		----------------------------------------
		ES[3]			 TRUE			TRUE
		ES[2]			 TRUE			FALSE
		ES[1]			 FALSE			TRUE
		ES[0]			 FALSE			FALSE
		
		GT - 1 - Local comparator event is true when input address bits not 
				 involved in crearing the four byte enables are greater than
				 the word reference address

		LT - 1 - Local comparator event is true when input address bits not 
				 involved in crearing the four byte enables are less than the
				 word reference address

		EC - Not Used

		SCD - Select Clock Domain
			0 - Select Clock Domain 0
			1 - Select Clock Domain 1


	*/
	/* Program Comparator Control Registers */		
	/* DRC_NA_CNTL */
	/* Reference Size is a don't care for program watchpoint.  Use byte */
	/* Don't Qualify with data */
	/* Watch for a Read */
	/* Access Size is a don't care */
	/* Byte Enable mask is a don't care */
	/* Event Sense Lookup is A (Local event) */
	drcParams1.readWrite = AET_WATCH_READ;
	drcParams1.accessType = AET_DRC_ACCESS_NONE;
	drcParams1.byteEnableMask = 0x0;
	drcParams1.eventSenseMask = 0xA;
	drcParams1.cmpGreater = AET_CMP_FALSE;
	drcParams1.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(
		&drcParams1,
		params
		);
	
	/* DRC_AREF */
	/* params->programAddress specifies address for breakpoint */
	drcParams1.addressReference = params->programAddress;
	_AET_pgmDrcArefReg(&drcParams1);

	
	/* TB_CNTL */
	/* Type of Trigger Builder (1-Wide, 3-wide, 7-wide */	
	/* Base index of trigger builder */
	/* Output control for trigger builder */
	/* Trigger Builder Lookup Table 0xAAAA (Event A only) */
	/* Trigger Builder Number */
	tbParams.tbOrsValue = 
		AET_FMK(
		AET_IAR_TB_ORS_MASK_A,
		1 << resources->acmpNo
		);
	tbParams.tbType = resources->trigBldr->tbType;
	tbParams.tbNo = resources->trigBldr->triggerBuilderNo;
	tbParams.outputCntl = resources->trigBldr->outputCntl;
	tbParams.triggers = 1 << (resources->trigBldr->triggerNo - 1);
	tbParams.boolOutput = 0xAAAA;
	_AET_pgmTrigBldr( 
					&tbParams,
					params
					);



	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);

	/* CIS_BUS_SEL */
	/* Set the input to the appropriate comparator to be the program bus */
	/* If ACMP number is 4 or 5, we don't need to do this. Those are only 
		 connected to program */
	if (resources->acmpNo < 4)

	{
		_AET_globalParams.globRegs.cisBusSel |= (1 << (1 + 4*resources->acmpNo));
	}


	_AET_iregWrite((uint16_t)	AET_indexof(CSL_Aet_iarRegs, CMPI_SEL), 
					_AET_globalParams.globRegs.cisBusSel
					);


	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo, 0);


	/* 
	 Add this trigger builder into the list of trigger builders to be enabled, 
	 and enable them all
	 */
	if ((status =_AET_markEnableTb(resources->trigBldr)
								   ) != AET_SOK)
	{
		return status;
	} 	

	_AET_PRINTF("-----End Job Program Watchpoint----\n");
	return status;
}
