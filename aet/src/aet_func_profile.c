/*
 * aet_func_profile.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ========================aet_func_profile.c=================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_func_profile.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_traceFuncProfile(AET_jobParams* params)
{
	AET_error status = AET_SOK;
	_AET_traceFuncProfileResources resources;

	/* Initialize the configWord in the resources structure */
	_AET_initConfigWord(&resources.configWord);


	 /* Select the Trigger Builders that Start the job will use */
	 resources.tbInfoStartPCTrace.triggerBuilderNo = 1;
	 resources.tbInfoStartPCTrace.tbType = _AET_TB_7_WIDE;
	 resources.tbInfoStartPCTrace.outputCntl = 0;
	 _AET_setTrigBldrBits(
		&resources.configWord,
		resources.tbInfoStartPCTrace.tbType,
		resources.tbInfoStartPCTrace.triggerBuilderNo
		);

	/* Select the Trigger Builders that the Stop job will use */
	 resources.tbInfoStopPCTrace.triggerBuilderNo = 0;
	 resources.tbInfoStopPCTrace.tbType = _AET_TB_3_WIDE;
	 resources.tbInfoStopPCTrace.outputCntl = 0;
	 _AET_setTrigBldrBits(
		&resources.configWord,
		resources.tbInfoStopPCTrace.tbType,
		resources.tbInfoStopPCTrace.triggerBuilderNo
		);


	resources.acmpNo[0] = 2;
	resources.acmpNo[1] = 3;
	resources.configWord.ACMP |= (1 << resources.acmpNo[0]);
	resources.configWord.ACMP |= (1 << resources.acmpNo[1]);

	
	/* 
	 Reserve the resources.  Params is passed to allocate AEGS.  Not used
	 here
	 */
	if (status = _AET_reserveResources(
			&resources.configWord,
			params)
			)
	{
		return status;
	}

	/*
	 Program the job, using the resources specified
	 */
	if (status = _AET_traceFuncProfileProgram(&resources, params))
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}
	
	/*
	 Insert the used resources into the job table and set the params->jobIndex
	 */
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);

	return status;

}


AET_error _AET_traceFuncProfileProgram(_AET_traceFuncProfileResources* resources,
										AET_jobParams* params)
{
	AET_error status = AET_SOK;
	_AET_drcCntlParams drcParams1 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams2 = AET_DRCCNTLPARAMS;
	_AET_tbCntlParams tbStartTraceParams = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbStopTraceParams = AET_TBCNTLPARAMS;
//	_AET_tbCntlParams tbStoreSampleParams = AET_TBCNTLPARAMS;

	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();


	/* Get the base indexes of each of the comparators that we will use */
	drcParams1.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[0]);
	drcParams2.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[1]);

	/* Set the parameters for each DRC and program the associated registers */
	drcParams1.byteEnableMask = 0xF;
	drcParams1.eventSenseMask = 0xA;
	drcParams1.cmpGreater = AET_CMP_FALSE;
	drcParams1.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(
		&drcParams1,
		params
	);

	drcParams1.addressReference = params->dataAddress;
	_AET_pgmDrcArefReg(
		&drcParams1
	);

	drcParams2.byteEnableMask = 0x0F;
	drcParams2.eventSenseMask = 0xA;
	drcParams2.cmpGreater = AET_CMP_FALSE;
	drcParams2.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(
		&drcParams2,
		params
	);

	drcParams2.addressReference = params->dataAddress;
	_AET_pgmDrcArefReg(
		&drcParams2
	);

	/* Start Trace Trigger */
	tbStartTraceParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->tbInfoStartPCTrace.tbType,
			resources->tbInfoStartPCTrace.triggerBuilderNo
			);

	tbStartTraceParams.tbOrsValue =
		AET_FMK(AET_IAR_TB_ORS_MASK_A,
			resources->configWord.AEG << 11 |
			1 << resources->acmpNo[0] |
			1 << resources->acmpNo[1] 
			);

	tbStartTraceParams.tbType = resources->tbInfoStartPCTrace.tbType;
	tbStartTraceParams.outputCntl = resources->tbInfoStartPCTrace.outputCntl;
	tbStartTraceParams.triggers = AET_TRACE_PA | AET_TRACE_TIMING | AET_TRACE_WA | AET_TRACE_WD;
	tbStartTraceParams.boolOutput = 0xAAAA;

	_AET_pgmTrigBldr(
		&tbStartTraceParams,
		params
	);

	/* Stop Trace Trigger */
	tbStopTraceParams.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->tbInfoStopPCTrace.tbType,
			resources->tbInfoStopPCTrace.triggerBuilderNo
			);

	tbStopTraceParams.tbOrsValue = 
		AET_FMK(AET_IAR_TB_ORS_MASK_A,
			resources->configWord.AEG << 11 |
			1 << resources->acmpNo[0] |
			1 << resources->acmpNo[1] 
			);

	tbStopTraceParams.tbType = resources->tbInfoStopPCTrace.tbType;
	tbStopTraceParams.outputCntl = resources->tbInfoStopPCTrace.outputCntl;
	tbStopTraceParams.triggers = AET_TRACE_STOP_PC | AET_TRACE_STOP_DATA;
	tbStopTraceParams.boolOutput = 0xAAAA;

	_AET_pgmTrigBldr(
		&tbStopTraceParams,
		params
	);







	/* General Stuff */
	/* FUNC_CNTL */
//	_AET_programFuncCtlReg(params);

	/* CIS_BUS_SEL */
	_AET_globalParams.globRegs.cisBusSel &= (~(1 << 4 * resources->acmpNo[0]));
	_AET_globalParams.globRegs.cisBusSel &= (~(1 << 4 * resources->acmpNo[1]));
//	_AET_globalParams.globRegs.cisBusSel |= (1 << 4 * resources->acmpNo[0]);
	_AET_globalParams.globRegs.cisBusSel |= (1 << 4 * resources->acmpNo[1]);
	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* _TB_DM */
	_AET_programTbDomainReg(resources->tbInfoStartPCTrace.triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->tbInfoStopPCTrace.triggerBuilderNo, 0);



	if (status = _AET_markEnableTb(&resources->tbInfoStartPCTrace))
	{
		return status;
	}

	if (status = _AET_markEnableTb(&resources->tbInfoStopPCTrace))
	{
		return status;
	}

	return status;
}

