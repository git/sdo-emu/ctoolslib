/*
 * aet_intr_stall_duration.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Last Update By: $Author: KarthikRamanaSankar $
 */

#include <aet_intr_stall_duration.h>
#include <c6x.h>

extern _AET_tbCntlParams AET_TBCNTLPARAMS;
extern _AET_globalVars _AET_globalParams;

AET_error _AET_IntrStallDuration(AET_jobParams* params)
{
	AET_IntrStallDurationResources resources;
	AET_error status = AET_SOK;
	_AET_triggerBuilderInfo tbInfoStartCounter0;
	_AET_triggerBuilderInfo tbInfoAetRtosIntr;
	_AET_triggerBuilderInfo tbInfoReloadCounter0;
	_AET_triggerBuilderInfo tbInfoStartCounter1;
	_AET_triggerBuilderInfo tbInfoReloadCounter1;
	_AET_triggerBuilderInfo tbInfoS0S1;
	_AET_triggerBuilderInfo tbInfoS1S0;

	/* Check if the specified event[0] belongs to the STALL signal class or is AET_EVT_MISC_STALL_PIPELINE */
	if((AET_EVENT_TYPE_STALL != _AET_getEventClass(params->eventNumber[0])) && (AET_EVT_MISC_STALL_PIPELINE != params->eventNumber[0]))
	{
		return AET_FINVALIDEVTCLASS;
	}

	/*
	 This use case requires 2 counters.
	 Check to see if both counters have been configured
	 */
	if ((_AET_globalParams.cntConfig[AET_CNT_0] == AET_COUNTER_NOTCONFIGURED) ||
		(_AET_globalParams.cntConfig[AET_CNT_1] == AET_COUNTER_NOTCONFIGURED))
	{
		return AET_FCNTNOTCONFIG;
	}

	/*
	 Check to see if both counters are configured correctly
	 in AET_COUNTER_TRAD_COUNTER mode
	 */
	if ((AET_COUNTER_TRAD_COUNTER != _AET_globalParams.cntConfig[AET_CNT_0]) ||
		(AET_COUNTER_TRAD_COUNTER != _AET_globalParams.cntConfig[AET_CNT_1]))
	{
		return AET_FCNTCONFIGINVALID;
	}

	/* 
	 Initialize the configword in the resources structure 
	 */
	_AET_initConfigWord(&resources.configWord);

	/* 
	 Select the trigger builders that the job will use
	 */

	/*
	 * Start Counter 0 (implements stall threshold detection)
	 */
	tbInfoStartCounter0.tbType = _AET_TB_1_WIDE;
	tbInfoStartCounter0.triggerBuilderNo = 1;
	tbInfoStartCounter0.outputCntl = 0;

	/*
	 * Stop/Reload Counter 0 (implements stall threshold detection)
	 */
	tbInfoReloadCounter0.tbType = _AET_TB_1_WIDE;
	tbInfoReloadCounter0.triggerBuilderNo = 2;
	tbInfoReloadCounter0.outputCntl = 0;

	/*
	 * Start Counter 1 (counts stalls cycles over the stall threshold)
	 */
	tbInfoStartCounter1.tbType = _AET_TB_1_WIDE;
	tbInfoStartCounter1.triggerBuilderNo = 3;
	tbInfoStartCounter1.outputCntl = 0;

	/*
	 * Stop/Reload Counter 1 on counter0 zero (overflow event)
	 */
	tbInfoReloadCounter1.tbType = _AET_TB_1_WIDE;
	tbInfoReloadCounter1.triggerBuilderNo = 4;
	tbInfoReloadCounter1.outputCntl = 0;

	/*
	 * Setup AET RTOS interrupt on counter0 zero (overflow event)
	 */
	tbInfoAetRtosIntr.tbType = _AET_TB_1_WIDE;
	tbInfoAetRtosIntr.triggerBuilderNo = 0;
	tbInfoAetRtosIntr.outputCntl = 1;

	/*
	 * Setup state0 --> state1 transition on counter0 zero (overflow event)
	 */
	tbInfoS0S1.tbType = _AET_TB_3_WIDE;
	tbInfoS0S1.triggerBuilderNo = 2;
	tbInfoS0S1.outputCntl = 0;

	/*
	 * Setup state1 --> state0 transition on end of long stall
	 */
	tbInfoS1S0.tbType = _AET_TB_3_WIDE;
	tbInfoS1S0.triggerBuilderNo = 3;
	tbInfoS1S0.outputCntl = 0;

	resources.trigBldrStartCounter0 = &tbInfoStartCounter0;
	resources.trigBldrReloadCounter0 = &tbInfoReloadCounter0;
	resources.trigBldrAetRtosIntr = &tbInfoAetRtosIntr;
	resources.trigBldrStartCounter1 = &tbInfoStartCounter1;
	resources.trigBldrReloadCounter1 = &tbInfoReloadCounter1;
	resources.trigBldrS0S1 = &tbInfoS0S1;
	resources.trigBldrS1S0 = &tbInfoS1S0;

	/* 
	 Set the trigger Builder bits in the configWord
	 */

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoStartCounter0.tbType,
				tbInfoStartCounter0.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoReloadCounter0.tbType,
				tbInfoReloadCounter0.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoAetRtosIntr.tbType,
				tbInfoAetRtosIntr.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoStartCounter1.tbType,
				tbInfoStartCounter1.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoReloadCounter1.tbType,
				tbInfoReloadCounter1.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoS0S1.tbType,
				tbInfoS0S1.triggerBuilderNo
				);

	_AET_setTrigBldrBits(
				&resources.configWord,
				tbInfoS1S0.tbType,
				tbInfoS1S0.triggerBuilderNo
				);

	/*
	 Reserve the resources that we need.  This will fail if the resources
	 are not available.
	 */
	if ((status = _AET_reserveResources(
					&resources.configWord,
					params)
					) != AET_SOK)
		return status;


	/*
	 Setup AEG and Trigger builders
	 */
	if (status = _AET_IntrStallDurationProgram(&resources, params))
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}

	/*
	 Insert the used resources into the job table and set the params->jobIndex
	 */
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);
	
	return status;
}

AET_error _AET_IntrStallDurationProgram(
		        AET_IntrStallDurationResources* resources,
				AET_jobParams* params
				)
{
	AET_error status = AET_SOK;
	_AET_tbCntlParams tbParamsStartCounter0 = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsAetRtosIntr = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsReloadCounter0 = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsStartCounter1 = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsReloadCounter1 = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsS0S1 = AET_TBCNTLPARAMS;
	_AET_tbCntlParams tbParamsS1S0 = AET_TBCNTLPARAMS;

	uint16_t aetAegBaseIndex;
	uint8_t aegNo;
	int32_t aegCntlCfg;

	_AET_PRINTF("---Begin Job Interrupt Stall Duration---\n");

	/* Determine the AEG Num from the config word */
	if(0x1 == resources->configWord.AEG)
	{
		aegNo = 0;
	}
	else if(0x2 == resources->configWord.AEG)
	{
		aegNo = 1;
	}
	else if(0x4 == resources->configWord.AEG)
	{
		aegNo = 2;
	}
	else if(0x8 == resources->configWord.AEG)
	{
		aegNo = 3;
	}
	else
	{
		return AET_FNOTAVAILAEG;
	}

	/* Setup AEG to generate level signal as output */
	aegCntlCfg = AET_FMK(AET_IAR_AUX_EVT_CNTL_TDR,0x0)|
			AET_FMKT(AET_IAR_AUX_EVT_CNTL_TMD,DISABLE)|
			AET_FMKT(AET_IAR_AUX_EVT_CNTL_SCD,DOMAIN0)|
			AET_FMKT(AET_IAR_AUX_EVT_CNTL_SYNC,ENABLE)|
			AET_FMKT(AET_IAR_AUX_EVT_CNTL_EDGE,DISABLE)|
			AET_FMKT(AET_IAR_AUX_EVT_CNTL_ALIGN, DISABLE)|
			AET_FMK(AET_IAR_AUX_EVT_CNTL_EP, 0xCC);

	 aetAegBaseIndex = _AET_getAegBaseIndex(aegNo);

	/* Configure the AEG */
	_AET_iregWrite(aetAegBaseIndex, aegCntlCfg);

	/* Program FUNC_CNTL */

	//Enable state machine
	_AET_globalParams.globRegs.cntFuncCntl |=
		AET_FMKT(AET_IAR_FUNC_CNTL_SME, ENABLE) |
		AET_FMKT(AET_IAR_FUNC_CNTL_SMM, FOUR_STATE) |
		AET_FMKT(AET_IAR_FUNC_CNTL_SMC, CONTINUOUS);

	_AET_globalParams.globRegs.cntFuncCntl |= 0x1B000000;
	_AET_iregWrite((uint16_t)(AET_indexof(CSL_Aet_iarRegs, FUNC_CNTL)),
									_AET_globalParams.globRegs.cntFuncCntl);


	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();

	/*
	 Get the base index of the trigger builder that is used for this job.  The
	 base address for aq given trigger builder is the address of the TB_ORS
	 (sometimes called TB_BES_ register.  The indexes for all of the other 
	 trigger builders are derived by the base index and an offset.
	 The table below shows the address of all of the trigger builder registers
	 */

	/* 
	----------------------------------------------------------------------------
					Trigger Builder Register Indexes 
	----------------------------------------------------------------------------
	| REGISTER NAME |  INDEX  |                  COMMENT 					   |
	|---------------------------------------------------------------------------
    | 					STATE MACHINE TRIGGERS                                 |
	|--------------------------------------------------------------------------|
	| SM_ORS_0W_0	|  0xA4	 | State Machine TB0 ORS Register	  			   |
	| SM_CNTL_0W_1	|  0xA5  | State Machine TB0 Trig 1 Control  			   |
	| SM_CEXP_0W_2  |  0xA6  | State Machine TB0 Trig 2-3 Control   		   |
	| SM_ORS_1W_0	|  0xA7	 | State Machine TB1 ORS Register	   			   |
	| SM_CNTL_1W_1	|  0xA8  | State Machine TB1 Trig 1 Control 			   |
	| SM_CEXP_1W_2  |  0xA9  | State Machine TB1 Trig 2-3 Control 			   |
	| SM_ORS_2W_0	|  0xAA	 | State Machine TB2 ORS Register	   			   |
	| SM_CNTL_2W_1	|  0xAB  | State Machine TB2 Trig 1 Control  			   |
	| SM_CEXP_2W_2  |  0xAC  | State Machine TB2 Trig 2-3 Control  			   |
	| SM_ORS_3W_0	|  0xAD	 | State Machine TB3 ORS Register	   			   |
	| SM_CNTL_3W_1	|  0xAE  | State Machine TB3 Trig 1 Control  			   |
	| SM_CEXP_3W_2  |  0xAF  | State Machine TB3 Trig 2-3 Control  			   |
	|--------------------------------------------------------------------------|
    | 					  WIDE TRIGGER BUILDERS                                |
	|--------------------------------------------------------------------------|
	| TB_W0_ORS_0W_0  | 0xC0 | 7-Wide TB0 Ors Register						   |
	| TB_W0_CNTL_0W_1 | 0xC1 | 7-Wide TB0 Trigger 1 Control					   |
	| TB_W0_CEXT_0W_2 | 0xC2 | 7-Wide TB0 Trigger 2-3 Control				   |
	| TB_W0_CEXT_0W_4 | 0xC3 | 7-Wide TB0 Trigger 4-5 Control				   |
	| TB_W0_CEXT_0W_6 | 0xC4 | 7-Wide TB0 Trigger 6-7 Control				   |
	| TB_W0_ORS_1W_0  | 0xC8 | 7-Wide TB1 Ors Register					   	   |
	| TB_W0_CNTL_1W_1 | 0xC9 | 7-Wide TB1 Trigger 1 Control					   |
	| TB_W0_CEXT_1W_2 | 0xCA | 7-Wide TB1 Trigger 2-3 Control				   |
	| TB_W0_CEXT_1W_4 | 0xCB | 7-Wide TB1 Trigger 4-5 Control				   |
	| TB_W0_CEXT_1W_6 | 0xCC | 7-Wide TB1 Trigger 6-7 Control				   |	  
	| TB1_ORS_2W_0  |  0xD0  | 3-Wide TB2 Ors Register						   |
	| TB1_CNTL_2W_1 |  0xD1  | 3-Wide TB2 Trigger 1 Control					   |
	| TB1_CEXT_2W_2 |  0xD2  | 3-Wide TB2 Trigger 2-3 Control				   |
	| TB1_ORS_3W_0  |  0xD0  | 3-Wide TB3 Ors Register						   |
	| TB1_CNTL_3W_1 |  0xD1  | 3-Wide TB3 Trigger 1 Control					   |
	| TB1_CEXT_3W_2 |  0xD2  | 3-Wide TB3 Trigger 2-3 Control				   |
	|--------------------------------------------------------------------------|
    | 					  1-BIT TRIGGER BUILDERS                               |
	|--------------------------------------------------------------------------|
	| TB_ORS_0      | 0x140  | 1-Wide TB0 Ors Register						   |
	| TB_CNTL_0		| 0x141  | 1-Wide TB0 Control Register					   |
	| TB_ORS_1      | 0x142  | 1-Wide TB1 Ors Register						   |
	| TB_CNTL_1		| 0x143  | 1-Wide TB1 Control Register					   |
	| TB_ORS_2      | 0x144  | 1-Wide TB2 Ors Register						   |
	| TB_CNTL_2		| 0x145  | 1-Wide TB2 Control Register					   |
	| TB_ORS_3      | 0x146  | 1-Wide TB3 Ors Register						   |
	| TB_CNTL_3		| 0x147  | 1-Wide TB3 Control Register					   |
	| TB_ORS_4      | 0x148  | 1-Wide TB4 Ors Register						   |
	| TB_CNTL_4		| 0x149  | 1-Wide TB4 Control Register					   |
	| TB_ORS_5      | 0x14A  | 1-Wide TB5 Ors Register						   |
	| TB_CNTL_5		| 0x14B  | 1-Wide TB5 Control Register					   |
	|--------------------------------------------------------------------------| 
	*/
	
	tbParamsStartCounter0.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrStartCounter0->tbType,
			resources->trigBldrStartCounter0->triggerBuilderNo
	);

	/* TB_CNTL  (Start/Advance Counter0 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsStartCounter0.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, resources->configWord.AEG << 11);
	tbParamsStartCounter0.tbType = resources->trigBldrStartCounter0->tbType;
	tbParamsStartCounter0.outputCntl = resources->trigBldrStartCounter0->outputCntl;
	tbParamsStartCounter0.triggers = 0x1;
	tbParamsStartCounter0.boolOutput = 0xAAAA;
	_AET_pgmTrigBldr(
					&tbParamsStartCounter0,
					params
					);

	tbParamsAetRtosIntr.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrAetRtosIntr->tbType,
			resources->trigBldrAetRtosIntr->triggerBuilderNo
	);

	/* TB_CNTL  (AET RTOS Interrupt Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsAetRtosIntr.tbOrsValue = 0;
	tbParamsAetRtosIntr.tbType = resources->trigBldrAetRtosIntr->tbType;
	tbParamsAetRtosIntr.outputCntl = resources->trigBldrAetRtosIntr->outputCntl;
	tbParamsAetRtosIntr.triggers = 0x1;
	tbParamsAetRtosIntr.boolOutput = 0xF0F0;
	tbParamsAetRtosIntr.evtCSelect = AET_TB_ESAC_CNT0;

	_AET_pgmTrigBldr(
					&tbParamsAetRtosIntr,
					params
					);

	tbParamsReloadCounter0.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrReloadCounter0->tbType,
			resources->trigBldrReloadCounter0->triggerBuilderNo
		);

	/* TB_CNTL  (Reload counter0 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsReloadCounter0.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, resources->configWord.AEG << 11);
	tbParamsReloadCounter0.tbType = resources->trigBldrReloadCounter0->tbType;
	tbParamsReloadCounter0.outputCntl = resources->trigBldrReloadCounter0->outputCntl;
	tbParamsReloadCounter0.triggers = 0x1;
	tbParamsReloadCounter0.boolOutput = 0x5555;

	_AET_pgmTrigBldr(
					&tbParamsReloadCounter0,
					params
					);

	tbParamsS0S1.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrS0S1->tbType,
			resources->trigBldrS0S1->triggerBuilderNo
	);

	/* TB_CNTL  (S0-->S1 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsS0S1.tbOrsValue = 0;
	tbParamsS0S1.tbType = resources->trigBldrS0S1->tbType;
	tbParamsS0S1.outputCntl = resources->trigBldrS0S1->outputCntl;
	tbParamsS0S1.triggers = 0x1;
	tbParamsS0S1.boolOutput = 0xF0F0;
	tbParamsS0S1.evtCSelect = AET_TB_ESAC_CNT0;
	params->stateQual = AET_STATEQUAL_STATE0;

	_AET_pgmTrigBldr(
					&tbParamsS0S1,
					params
					);

	tbParamsStartCounter1.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrStartCounter1->tbType,
			resources->trigBldrStartCounter1->triggerBuilderNo
	);

	/* TB_CNTL  (Start/Advance Counter1 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsStartCounter1.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, resources->configWord.AEG << 11);
	tbParamsStartCounter1.tbType = resources->trigBldrStartCounter1->tbType;
	tbParamsStartCounter1.outputCntl = resources->trigBldrStartCounter1->outputCntl;
	tbParamsStartCounter1.triggers = 0x1;
	tbParamsStartCounter1.boolOutput = 0xAAAA;
	params->stateQual = AET_STATEQUAL_STATE1;

	_AET_pgmTrigBldr(
					&tbParamsStartCounter1,
					params
					);

	tbParamsReloadCounter1.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrReloadCounter1->tbType,
			resources->trigBldrReloadCounter1->triggerBuilderNo
		);

	/* TB_CNTL  (Reload counter1 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the 
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */


	tbParamsReloadCounter1.tbOrsValue = 0;
	tbParamsReloadCounter1.tbType = resources->trigBldrReloadCounter1->tbType;
	tbParamsReloadCounter1.outputCntl = resources->trigBldrReloadCounter1->outputCntl;
	tbParamsReloadCounter1.triggers = 0x1;
	tbParamsReloadCounter1.boolOutput = 0xF0F0;
	tbParamsReloadCounter1.evtCSelect = AET_TB_ESAC_CNT0;
	params->stateQual = AET_STATEQUAL_STATE0;

	_AET_pgmTrigBldr(
					&tbParamsReloadCounter1,
					params
					);

	tbParamsS1S0.tbBaseIndex =
		_AET_getTbBaseIndex(
			resources->trigBldrS1S0->tbType,
			resources->trigBldrS1S0->triggerBuilderNo
	);

	/* TB_CNTL  (S1-->S0 Trigger Builder) */
	/* Trigger Builder ORS Value - 32 bit value representing the inputs to the
			two Trigger Builder Or Gates  */
	/* Trigger Builder Type (1-Wide, 3-Wide, 7-Wide) */
	/* Trigger Builder Base Index */
	/* Trigger Builder Output Control */
	/* Boolean Output Value for TB Lookup Table */
	/* Trace Triggers */
	/* C Input Control */
	/* D Input Control */
	tbParamsS1S0.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, resources->configWord.AEG << 11);
	tbParamsS1S0.tbType = resources->trigBldrS1S0->tbType;
	tbParamsS1S0.outputCntl = resources->trigBldrS1S0->outputCntl;
	tbParamsS1S0.triggers = 0x1;
	tbParamsS1S0.boolOutput = 0x5555;
	params->stateQual = AET_STATEQUAL_STATE1;

	_AET_pgmTrigBldr(
					&tbParamsS1S0,
					params
					);

	//set state qualification to none
	params->stateQual = AET_STATEQUAL_NONE;

	/* TB_DM */
	_AET_programTbDomainReg(resources->trigBldrStartCounter0->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrAetRtosIntr->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrReloadCounter0->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrStartCounter1->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrReloadCounter1->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrS0S1->triggerBuilderNo, 0);
	_AET_programTbDomainReg(resources->trigBldrS1S0->triggerBuilderNo, 0);
	/*
	 Add this trigger builder into the list of trigger builders to be enabled,
	 and enable them all
	 */
	if ((status = _AET_markEnableTb(resources->trigBldrStartCounter0)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrAetRtosIntr)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrReloadCounter0)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrStartCounter1)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrReloadCounter1)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrS0S1)
								) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_markEnableTb(resources->trigBldrS1S0)
								) != AET_SOK)
	{
		return status;
	}

	_AET_PRINTF("-----End Job Interrupt Stall Duration-----\n");

	return status;
}
