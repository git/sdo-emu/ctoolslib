/*
 * aet_evt_wm_startstop.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  =========================aet_evt_wm_startstop.c=============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 *  ============================================================================
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_evt_wm_startstop.h>
#include <aet_evt_timer_common.h>



extern _AET_globalVars _AET_globalParams;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_eventTriggeredWatermarkStartStop(AET_jobParams* jobParams)
{
	AET_error status = AET_SOK;
	AET_triggerType trigger;
	AET_triggerType origTriggerType = jobParams->triggerType;

	switch (jobParams->counterNumber){
		case AET_CNT_0:
			switch(jobParams->startStop){
			case AET_WATERMARK_START:
				trigger = AET_TRIG_WM0START;
				break;
			case AET_WATERMARK_STOP:
				trigger = AET_TRIG_WM0STOP;
				break;
			}
			break;
		case AET_CNT_1:
			switch(jobParams->startStop){
			case AET_WATERMARK_START:
				trigger = AET_TRIG_WM1START;
				break;
			case AET_WATERMARK_STOP:
				trigger = AET_TRIG_WM1STOP;
				break;
			}
			break;
		default:
			return AET_FCNTNUMBERINVALID;
	}

	jobParams->triggerType = trigger;

	/* Set up the job to start the WaterMark */
	status = AET_setupJob(AET_JOB_TRIG_ON_EVENTS, jobParams);

	if (status != AET_SOK)
		return status;

	/* Put the original parameters back */
	jobParams->triggerType = origTriggerType;

	return status;
}



