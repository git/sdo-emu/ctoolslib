/*
 * aet_gen_common.c
 *
 * AET Library Public Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  ============================aet_gen_common.c===============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_gen_common.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;


/*
	This function performs some of the common tasks that are necessary when
	starting an AET job.  It initializes the configuration word, allocates
	a trigger builder depending on the trigger type, and adds the resource
	information to the configuration word.

 */
AET_error _AET_jobInit(_AET_resourceStruct* configWordPtr,
					   AET_triggerType trigType,
					   const _AET_triggerBuilderInfo* *tbPtr
					   )
{
	AET_error status = AET_SOK;

	/* Intialize the ConfigWord */
	_AET_initConfigWord(configWordPtr);

	/*
	 * TODO: We need to update the _AET_getTriggerBuilder to take into
	 * consideration the triggers that are already programmed  Currently,
	 * it will only allocate one job per TB.
	 *
	 */
	if ((status = _AET_getTriggerBuilder(trigType, 
										 tbPtr
					     				 )) != AET_SOK)
	{
		return status;
	}

	if ((status = _AET_setTrigBldrBits(configWordPtr,
									   (**tbPtr).tbType,
									   (**tbPtr).triggerBuilderNo
									   )) != AET_SOK)
	{
		return status;
	}
			   
	return status;
}

void _AET_programFuncCtlReg(AET_jobParams* jobParams)
{
	if (jobParams->stateQual != AET_STATEQUAL_NONE)
	{
		_AET_globalParams.globRegs.cntFuncCntl |= 
			AET_FMKT(AET_IAR_FUNC_CNTL_SME, ENABLE) |
			AET_FMKT(AET_IAR_FUNC_CNTL_SMM, FOUR_STATE) |
			AET_FMKT(AET_IAR_FUNC_CNTL_SMC, CONTINUOUS);

	}
	_AET_globalParams.globRegs.cntFuncCntl |= 0x1B000000;
	_AET_iregWrite((uint16_t)(AET_indexof(CSL_Aet_iarRegs, FUNC_CNTL)), 
									_AET_globalParams.globRegs.cntFuncCntl);


}

void _AET_programTbDomainReg(uint8_t tbNo, uint8_t domain)
{
	if (domain == 0)
	{
		_AET_globalParams.globRegs.tbDomain &= ~(1 << tbNo);	
	} else {
		_AET_globalParams.globRegs.tbDomain |= ~(1 << tbNo);
	}

	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, TB_DM),
					_AET_globalParams.globRegs.tbDomain
					);
}



void _AET_programTbOrsReg(uint16_t tbBaseIndex, uint16_t aSide, uint16_t bSide)
{
	_AET_iregWrite (tbBaseIndex, 
					AET_FMK(AET_IAR_TB_ORS_MASK_A, aSide) |
					AET_FMK(AET_IAR_TB_ORS_MASK_B, bSide)
					);
}

