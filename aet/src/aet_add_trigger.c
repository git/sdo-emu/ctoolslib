/*
 * aet_add_trigger.c
 *
 * AET Library public API Definitions
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Created on: Jun 13, 2011
 *  ==============================aet_add_trigger.cc============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2012-10-16 11:50:41 -0400 (Thu, 02 Jun 2011) $
 *   Revision: $LastChangedRevision: 10572 $
 *   Last Update By: $Author: KarthikRamanaSankar $
 *
 *   Comments: updated for adding trigger builders in any order (release 4.11)
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
#include <aet_add_trigger.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;
extern TB_SETTING_DATABASE _tb_settings_db;

/*
 *
 */
AET_error _AET_addTrigger(AET_jobParams* params)
{
	/*
	 * TODO: 	Program the Additional Trigger Builder
	 * TODO: 	Make sure the Database keeps track of this trigger/trigger builder
	 * TODO: 	Return the Job number
	 */
	_AET_triggerBuilderInfo tbInfo;
	TB_SETTINGS* tbSettingsPtr;					/* Existing Trigger Builder */
	AET_addTriggerResources resources;	/* Add Trigger Resources */
	_AET_resourceStruct* resourcePtr;		/* Pointer to existing configWord */
	AET_error status = AET_SOK;					/* Error  Status */
	_AET_BOOL tbStatus = AET_FALSE;			/*Can we use existing TB */
	uint16_t* lutPtr;

	/*
	 * Check to see that the Job Number passed is indeed programmed.  If it has
	 * not been, then there is no job to add a trigger to.
	 */
	if ((_AET_globalParams.jobTable.entriesUsed & (1 << params->jobIndex)) == 0){
		return AET_FJOBNOTPROGRAMMED;
	}

	/*
	 * This is a pointer to the resources that are being used by the current job
	 */
	resourcePtr = &_AET_globalParams.jobTable.table[params->jobIndex];


	/*
	 * See if the trigger that we need is available on the current trigger
	 * builders being used by the existing job.
	 */
	tbStatus = _AET_checkForTriggerOnJobTbs(
							params->triggerType,
							resourcePtr,
							&tbInfo
							);

	/*
	 * Get a pointer to the existing trigger builder being used
	 */
	switch(tbInfo.tbType){
		case _AET_TB_1_WIDE:
			tbSettingsPtr = (TB_SETTINGS*)&_tb_settings_db.tb_1_wide_settings[tbInfo.triggerBuilderNo];
			lutPtr = (uint16_t*)&_tb_settings_db.tb_1_wide_settings[tbInfo.triggerBuilderNo].lutValue[0];
			break;
		case _AET_TB_3_WIDE:
			tbSettingsPtr = (TB_SETTINGS*)&_tb_settings_db.tb_3_wide_settings[tbInfo.triggerBuilderNo];
			lutPtr = (uint16_t*)&_tb_settings_db.tb_3_wide_settings[tbInfo.triggerBuilderNo].lutValue[0];
			break;
		case _AET_TB_7_WIDE:
			tbSettingsPtr = (TB_SETTINGS*)&_tb_settings_db.tb_7_wide_settings[tbInfo.triggerBuilderNo];
			lutPtr = (uint16_t*)&_tb_settings_db.tb_7_wide_settings[tbInfo.triggerBuilderNo].lutValue[0];
			break;
		default:
			return AET_FINVALIDTRIGBUILDER;
	}

	if (tbStatus == AET_TRUE){
		/*
		 * Try to implement the additional trigger on the existing trigger
		 * builder.  If that's not available, then drop out and try to allocate
		 * a new one.
		 */
		resources.newTriggerBuilder = AET_FALSE;

		/* Check if that trigger is available */
		if ((tbSettingsPtr->trigsUsed & (1 << (tbInfo.triggerNo - 1))) != 0)
			resources.newTriggerBuilder = AET_TRUE;

		/* Check if the Output control is compatible */
		if (((tbSettingsPtr->tbCntlValue & 0xC0000000) >> 30) != (tbInfo.outputCntl))
			resources.newTriggerBuilder= AET_TRUE;
	}

	status = _AET_pgmAddTrigger(params, &resources, tbSettingsPtr, lutPtr, &tbInfo);
	return status;
}

AET_error  _AET_pgmAddTrigger(
		AET_jobParams* params,
		AET_addTriggerResources* resources,
		TB_SETTINGS* existingResources,
		uint16_t* lutPtr,
		_AET_triggerBuilderInfo* tbInfo
		){


	AET_error status = AET_SOK;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;

	_AET_PRINTF("---Begin Job Add Trigger---\n");

	/* Add the trigger and record the resource */
	if (resources->newTriggerBuilder == AET_FALSE){
		uint32_t regVal;
		uint16_t tbBaseIndex;
		uint16_t otherHalf;
		uint16_t newVal;
		uint8_t cexpValue;


		_AET_PRINTF("--- (Existing Builder) ---\n");
		/* Yes, we can allocate on existing TB. */

		/* Get the Base Index of the Trigger Builder */
		tbBaseIndex = _AET_getTbBaseIndex(tbInfo->tbType, tbInfo->triggerBuilderNo);

		/*
		 * The only thing we need to program here is the Lookup table value.
		 * The trick is to get the correct one.  Don't forget about the
		 * other half.
		 */

		if (params->logicOrientation == AET_TRIG_LOGIC_STRAIGHTFORWARD){
			newVal = existingResources->initLutValue;
		}else{
			newVal = ~existingResources->initLutValue;
		}

		switch(tbInfo->triggerNo-1){
			case 0:
				regVal = ((existingResources->tbCntlValue) & ~0xFFFF) | (newVal & 0xFFFF);
				cexpValue = 0;
				break;
			case 1:
				otherHalf = lutPtr[2];
				regVal = (((uint32_t)(otherHalf)) << 16) & 0xFFFF0000 | (newVal & 0xFFFF);
				cexpValue = 1;
				break;
			case 2:
				otherHalf = lutPtr[1];
				regVal = (((uint32_t)(newVal)) << 16) & 0xFFFF0000 | (otherHalf & 0xFFFF);
				cexpValue = 1;
				break;
			case 3:
				otherHalf = lutPtr[4];
				regVal = (((uint32_t)(otherHalf)) << 16) & 0xFFFF0000 | (newVal & 0xFFFF);
				cexpValue = 2;
				break;
			case 4:
				otherHalf = lutPtr[3];
				regVal = (((uint32_t)(newVal)) << 16) & 0xFFFF0000 | (otherHalf & 0xFFFF);
				cexpValue = 2;
				break;
			case 5:
				otherHalf = lutPtr[6];
				regVal = (((uint32_t)(otherHalf)) << 16) & 0xFFFF0000 | (newVal & 0xFFFF);
				cexpValue = 3;
				break;
			case 6:
				otherHalf = lutPtr[5];
				regVal = (((uint32_t)(newVal)) << 16) & 0xFFFF0000 | (otherHalf & 0xFFFF);
				cexpValue = 3;
				break;

		}

		// Record the value in the Trigger Builder Settings Database
		lutPtr[tbInfo->triggerNo-1] = newVal;


		/* Disable Trigger Builders */
		_AET_DISABLE_TBS();

		/*
		 * Now, actually write the value to the AET register
		 */
		_AET_iregWrite((uint16_t)(tbBaseIndex + 1 + cexpValue), regVal);

		/* Re-Enable the Trigger Builders */
		status = enableTriggerBuilders();

	}else{
		/*
		 * Otherwise, we need to get a different trigger builder to use with
		 * this job.
		 */
		status = _AET_getTriggerBuilder(params->triggerType, &resources->trigBldr);
		if (status != AET_SOK)
			return status;



			/* Disable All Trigger Builders */
			_AET_DISABLE_TBS();

			/* Get the Base Index of the Trigger Builder */
			tbParams.tbBaseIndex =
					_AET_getTbBaseIndex(
							resources->trigBldr->tbType,
							resources->trigBldr->triggerBuilderNo
							);

			if (params->logicOrientation == AET_TRIG_LOGIC_STRAIGHTFORWARD){
				tbParams.boolOutput = existingResources->initLutValue;
			}else{
				tbParams.boolOutput = ~(existingResources->initLutValue);
			}

			tbParams.triggers = resources->trigBldr->triggerNo;
			tbParams.tbType = resources->trigBldr->tbType;



			/*
			 * Program a new trigger builder
			 */

			_AET_PRINTF("--- (New Trigger Builder) ---\n");


			tbParams.outputCntl = resources->trigBldr->outputCntl;
			tbParams.tbOrsValue = existingResources->tbOrsValue;

			_AET_pgmTrigBldr(
					&tbParams,
					params
					);

			/* TB_DB */
			_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo, 0);

			/*
			 * Add this trigger builder into the list of trigger builders to be enabled
			 * and enable them all
			 */
			if ((status = _AET_markEnableTb(resources->trigBldr)) != AET_SOK){
				return status;
			}


			_AET_PRINTF("---End Job Add Trigger---\n");


	}

	return status;




}


