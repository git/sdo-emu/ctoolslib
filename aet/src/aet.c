/*
 * aet.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =================================aet.c=====================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-09-20 10:53:55 -0500 (Fri, 20 Sep 2013) $
 *   Revision: $LastChangedRevision: 10658 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

/* Necessary include files */
#include <aet.h>
#include <_aet.h>
#include <aet_aeg_manager.h>
#include <aet_program_watchpoint.h>
#include <aet_program_watchpoint_range.h>
#include <aet_data_watchpoint.h>
#include <aet_data_watchpoint_range.h>
#include <aet_data_watchpoint_value.h>
#include <aet_evt_timer_common.h>
#include <aet_evt_timer_start.h>
#include <aet_evt_timer_stop.h>
#include <aet_evt_wm_startstop.h>
#include <aet_trace_startstop_on_pc.h>
#include <aet_trace_in_pc_range.h>
#include <aet_trace_in_data_range.h>
#include <aet_trace_on_event.h>
#include <aet_timer_trigger.h>
#include <aet_func_profile.h>
#include <aet_trigger_on_event.h>
#include <aet_add_trigger.h>
#include <aet_intr_on_stall.h>
#include <aet_count_stalls.h>
#include <aet_intr_stall_duration.h>
#include <c6x.h>

/* 
 Each API needs to be able to be allocated separately, so declare a separate
 .text section for each
 */

#pragma CODE_SECTION(AET_claim, ".text:claim")
#pragma CODE_SECTION(AET_init, ".text:init")
#pragma CODE_SECTION(AET_release, ".text:release")
#pragma CODE_SECTION(AET_releaseJob, ".text:releaseJob")
#pragma CODE_SECTION(AET_setupJob, ".text:setupJob")
#pragma CODE_SECTION(AET_enable, ".text:enable")
#pragma CODE_SECTION(AET_disable, ".text:disable")
#pragma CODE_SECTION(AET_configCounter, ".text:counterConfig")
#pragma CODE_SECTION(AET_clearCounters, ".text:clearCounters")
#pragma CODE_SECTION(AET_readCounter, ".text:readCounter")
#pragma CODE_SECTION(AET_enableEmuPins, ".text:enableEmuPins")
#pragma CODE_SECTION(AET_disableEmuPins, ".text:disableEmuPins")


/* Global Variables */
_AET_globalVars _AET_globalParams;  /* Global Structure for Parameters */

const _AET_debugInfo _AET_debugInfoDatabase[] = { _AET_DEBUG_TABLE };

/* 
 The _AET_bitSetTable contains the bits that must be set in the counter 
 configuration register for each type of configuration
 */
const int32_t _AET_bitSetTable[] = 
{
	0x8,		/* AET_COUNTER_TRAD_COUNTER  */
	0x9, 		/* AET_COUNTER_TRAD_EVENT */
	0xA, 		/* AET_COUNTER_TRAD_CONTINUOUS */
	0xB,		/* AET_COUNTER_TRAD_ONESHOT */
	0x108,		/* AET_COUNTER_MINWM_COUNTER */
	0x109,		/* AET_COUNTER_MINWM_EVENT */
	0x10A,		/* AET_COUNTER_MINWM_CONTINUOUS */
	0x10B,		/* AET_COUNTER_MINWM_ONESHOT */
	0x128,		/* AET_COUNTER_MAXWM_COUNTER */
 	0x129,		/* AET_COUNTER_MAXWM_COUNTER */
	0x12A,		/* AET_COUNTER_MAXWM_CONTINUOUS */
	0x12B, 		/* AET_COUNTER_MAXWM_ONESHOT */
};
/* 
 Base Address for AET Memory Mapped Registers
 */
CSL_Aet_mmrRegs *AET_mmrRegs = (CSL_Aet_mmrRegs*)0x1bc0020;

/*
 Pseudo Base Address for the indirect registers.  This turns into the offset
 */
CSL_Aet_iarRegs *AET_iarRegs = (CSL_Aet_iarRegs*)0x0;

/*
 Pointers to the AEGMUX registers in the interrupt controller
 */
static int32_t *AET_aegSelRegs0 = (int32_t*)0x01800140;
static int32_t *AET_aegSelRegs1 = (int32_t*)0x01800144;


/*******************
* PUBLIC FUNCTIONS *
*******************/

/*******************************************************************************
* AET_Claim
* 	Summary - Claims the AET Unit for use by the application
*
*	Responsibilities 
*		* Validate that AET has been initialized
*		* If AET already claimed, just return
*		* Claim the AET unit witha  write to appropriate location
*		* Verify that the claim was successful
*
*	Parameters - None
*
*	Returns - AET_error value
*			- AET_SOK - Claim was successful
*			- AET_FAIL_NOCLAIM - Claim was unsuccessful
*******************************************************************************/
AET_error AET_claim(void)
{
	/* Assert that AET_init has been called prior to calling claim */
	_AET_assert(_AET_globalParams.initStatus == AET_INITIALIZED);

	/* If we've already claimed, don't claim again, just return true */
	if (_AET_globalParams.AET_claimed == AET_TRUE)
		return AET_SOK;


	/* Claim the AET Unit */

	/* If we want to use the simulator to verify execution we can buildThis  with
	the _AET_SIMULATOR_ENABLE option and it will just act as though a claim
	occurred
	*/
#ifdef _AET_SIMULATE
	_AET_globalParams.AET_claimed = AET_TRUE;
	return AET_SOK;
#else

	/* Write a 1 to the PID_RD register */
	AET_mmrRegs->PID_RD = 0x1;

	/* 
	 If the claim worked correctly, bit 0 and 1 should be set, so read it back
	 and see 
	 */
	if ((AET_mmrRegs->PID_RD & 0x00000003) == 1)
	{
		/* Set global claimed flag to true */
		/* Tell the user that the claim was successful in debug mode */
		_AET_globalParams.AET_claimed = AET_TRUE;	
		_AET_PRINTF("AET Claimed\n");
		_AET_globalParams.aetState = AET_STATE_CLAIMED;
		return AET_SOK;
	} /* end if */
	else
	{
	 return AET_FNOCLAIM;
	}/* end else */
#endif
} /* End AET_claim */

/*******************************************************************************
* AET_configCounter
*
* 	Summary - 	Configures one of the AET counters according to the parameter 
*				structure passed to it.
*
*	Parameters - AET_counterConfigParameters*
*	Returns - AET_error value
*******************************************************************************/
AET_error AET_configCounter(AET_counterConfigParams* params)
{
	AET_error status = AET_SOK;

	uint8_t shiftcount; /* Since there is only a single counter config register,
						if we're configuring counter 1, we need to shift the 
						value 10 bits into the proper location.  */
	int32_t bitSet;

	/* 
	 Set the shift count.  There are 10 bits assigned to each counter.
	 */
	shiftcount = (uint8_t)(10 * params->counterNumber);
	
	/* 
	 Validate that the configuration mode is valid 
	 */
	if (params->configMode >= AET_NUM_COUNTER_CONFIGS)
		return AET_FINVALIDCNTMODE;
	
	/* 
	 Get the bits to set from the table 
	 */
	bitSet = _AET_bitSetTable[params->configMode];
	
	/* 
	 Mask off the old bits in the global counter register and set the new bits 
	 */
	_AET_globalParams.globRegs.cntFuncCntl &= ~(0x3FF << shiftcount);
	_AET_globalParams.globRegs.cntFuncCntl |= (bitSet << shiftcount);

	/*
	 Write the cached global value into the register
	 */
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, FUNC_CNTL), 
								_AET_globalParams.globRegs.cntFuncCntl);

	/* 
	 Record the config mode that this counter has been programmed to
	 */
	_AET_globalParams.cntConfig[params->counterNumber] = params->configMode;

	/* Write the Reload Register with the specified value */
	_AET_iregWrite((uint16_t)(AET_indexof(CSL_Aet_iarRegs, CNT_RELOAD) + 
					params->counterNumber),
					params->reloadValue);

	/* 
	 Currently, status should always be AET_SOK
	 */
	return status;
}/* End AET_configCounter */

/*******************************************************************************
* AET_clearCounter
*
* 	Summary - 	Clears the AET Counter Configurations
*
*	Parameters - None
*	Returns - AET_error value
*******************************************************************************/
AET_error AET_clearCounters(){
	AET_error status = AET_SOK;

	_AET_globalParams.globRegs.cntFuncCntl &= ~0xFFFFF;
	_AET_globalParams.cntConfig[AET_CNT_0] = AET_COUNTER_TRAD_COUNTER;
	_AET_globalParams.cntConfig[AET_CNT_1] = AET_COUNTER_TRAD_COUNTER;

	/*
	 Write the cached global value into the register
	 */
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, FUNC_CNTL),
								_AET_globalParams.globRegs.cntFuncCntl);

	return status;




}/* End AET_clearCounter */

/*******************************************************************************
* AET_enable
*
* 	Summary - 	Enables all of the currently programmed AET jobs.
*
*	Parameters - None
*	Returns - AET_error value
*******************************************************************************/
AET_error AET_enable(void)
{
	/* Check to see if we're really enabling AET or if we're re-enabling it */
	if (_AET_globalParams.aetState == AET_STATE_CLAIMED)
	{
		/* Enable the AET Unit */
		AET_mmrRegs->PID_RD = 0x2;

		if ((AET_mmrRegs->PID_RD & 0x00000003) == 1)
		{
			/* Can't Check, so assume we passed */
		}
		
		_AET_PRINTF("AET Enabled\n");
	}
	else
	{
		/* 
		 Just re-enable the trigger builders. This is a global enable.  A case
		 could be made to support only enabling individual jobs, but it hasn't
		 been yet
		 */
		_AET_iregWrite((uint16_t)(AET_indexof(CSL_Aet_iarRegs, TB_ENA)), 
						_AET_globalParams.globRegs.tbEnable
						);	

	}
	_AET_globalParams.aetState = AET_STATE_ENABLED;
	return AET_SOK;
} /* End AET_enable */



/*******************************************************************************
* AET_disable
*
* 	Summary - 	Disables all trigger builders .
*
*	Parameters - None
*	Returns - AET_error value
*******************************************************************************/
AET_error AET_disable(void)
{
	AET_error status = AET_SOK;
	
	/* 
	 Just disable all trigger builders.  AET is not really disabled, because
	 we'd have to release the AET resources and then claim them again, leaving
	 open the possibility of them being claimed by the host in the mean time.
	 */
	_AET_DISABLE_TBS();
	_AET_globalParams.aetState = AET_STATE_DISABLED;

	return status;
}

/*******************************************************************************
* AET_init
*
*	Summary - 	Initializes the application. Can only be called once, and must be 
*		called before calling any of the other API's.  Sets a global flag so that 
*		the rest of the application knows that it has been called
*
*	Responsibilities
*		* Initialize the Global Used Resources structure
*		* Initialize value of AET Claimed
*		* Initialize the resource manager structure
*		* Initialize the job table
*		* Initialize Counter Configuration
*		* Initialize Trigger Builder Structure
*		* Clear AEG Select Registers (64x+ and above)
*		* Set Initialized Flag
*		* Set AET State to Available
*
*	Parameters - None
*
*	Returns - None
*******************************************************************************/
void AET_init(void)
{
  static unsigned short init = 0;

	/* 
	 If we've already initialized, just return 
	 */
	if (init != 0)
		return;

	_AET_clearGlobalParams();


	/* 
	 Set the static variable specifying that we've already initialized
	 */

	init = 1;

	/*
	 Set the global initStatus element to mark initialized
	 */
	_AET_globalParams.initStatus = AET_INITIALIZED;
	_AET_globalParams.aetState = AET_STATE_AVAIL;
} /* End AET_init */


/*******************************************************************************
* AET_readCounter
*
* 	Summary - 	Reads the value in the specified AET counter
*
*	Parameters - AET_counters (AET_CNT_0, AET_CNT_1)
*	Returns - int32_t value in the counter
*******************************************************************************/
int32_t AET_readCounter(AET_counters counterNum)
{
	int32_t value;
	uint16_t cntIndex;

	/* 
	 Calculate the counter index based on the counter number 
	 */
	cntIndex = AET_indexof(CSL_Aet_iarRegs, CNT_RELOAD) + counterNum;

	/*
	 Read the value from the register
	 */
	value = _AET_iregRead(cntIndex);

	/*
	 Return the value.  We invert this because the counter counts down, rather 
	 than up.  
	 */
	return (~value);
} /* End AET_readcounter */

/*******************************************************************************
* AET_release
*
* 	Summary - 	Releases the AET unit ownership from the application.  
*
*	Parameters - None
*	Returns - AET_error
*			- AET_SOK if release is successful
*			- AET_FAIL_NORELEASE if release is unsuccessful
*******************************************************************************/
AET_error AET_release(void)
{
	/*
	 If the AET_init is not claimed, there's no reason to release it.  Just
	 return
	 */
	if (_AET_globalParams.AET_claimed == AET_FALSE)
		return AET_SOK;
	
	/* Release the AET Unit */
	AET_mmrRegs->PID_RD = 0x0;

	if ((AET_mmrRegs->PID_RD & 0x00000003) != 1)
	{
		/*
		 * Clear the counter configuration
		 */
		AET_clearCounters();

		/*
		 * Clear the Global Configuration
		 */
		_AET_clearGlobalParams();

		/* If successful, tell the user and return */
		_AET_globalParams.AET_claimed = AET_FALSE;
		_AET_PRINTF("AET Released\n");
		return AET_SOK;
	} /* end if */
	else
	{
		return AET_FNORELEASE;
	} /* end else */
} /* End AET_release */

/*******************************************************************************
* AET_releaseJob
*
* 	Summary - 	Releases a prior programmed job  
*
*	Parameters - AET_jobIndex - Index of the job in the job table
*	Returns - AET_error
*******************************************************************************/
AET_error AET_releaseJob(AET_jobIndex jobIndex)
{
	AET_error status = AET_SOK;

	/*
	 ASSERT that the jobIndex is in the allowed range
	 */
	_AET_assert(jobIndex < _AET_MAX_CONCURRENT_JOBS);

	
	/*
	 Remove this job from the job table, which in turn de-programs the registers
	 */		
	status = _AET_removeJobFromTable(jobIndex, &_AET_globalParams.jobTable.table[jobIndex]);

	return status;
} /* End AET_releaseJob */

/*******************************************************************************
* AET_setupJob
*
* Summary - 	Examines resources to see if programming a job is feasible, and if 
*		the appropriate resources are available, programs the job
*
*	Responsibilities
*		* Validate that Job Parameter pointer is not null
*		* Check to ensure AET has been claimed
*		* Call the appropriate Job programming function, passing the pointer to the 
*			parameters structure.
*		* Return the status returned by the job programming function.
*
*
*	Parameters 
*	- AET_jobType - Specifies the type of job to program
*	- AET_jobParameters* -	Specifies the user configurable parameters of the job
*
*	Returns - AET_error
*	- AET_SOK - Job Programming was successful
*	- AET_FNOJOBIMPLEMENTED - The job parameter specified is not a valid job type
*******************************************************************************/
AET_error AET_setupJob(AET_jobType jobType, AET_jobParams* params)
{
	AET_error status = AET_SOK;

	/* 
	 ASSERT that the parameter structure pointer isn't null
	 */
	_AET_assert(params != NULL);
	
	/*
	 Call is invalid if AET is not claimed
	 */
	if (_AET_globalParams.AET_claimed != AET_TRUE) 
		return AET_FNOTCLAIMED;

	/* 
	 Depending on the job type, call the appropriate API.  
	 */
	switch(jobType)
	{
		case AET_JOB_START_STOP_TRACE_ON_PC:
			status = _AET_traceStartStopOnPC(params);
			break;
		case AET_JOB_TRACE_IN_PC_RANGE:
			status = _AET_traceInPcRange(params);
			break;
		case AET_JOB_TRACE_IN_DATA_RANGE:
			status = _AET_traceInDataRange(params);
			break;
		case AET_JOB_TRACE_ON_EVENTS:
			status = _AET_traceOnEvent(params);
			break;
		case AET_JOB_TRIG_ON_PC:
			status = _AET_programWatchpoint(params);
			break;
		case AET_JOB_TRIG_ON_PC_RANGE:
			status = _AET_programWatchpointRange(params);
			break;
		case AET_JOB_TRIG_ON_DATA:
			status = _AET_dataWatchpoint(params);
			break;
		case AET_JOB_TRIG_ON_DATA_RANGE:
			status = _AET_dataWatchpointRange(params, AET_DRC_DATAQUAL_NO_QUALIFY);
			break;
		case AET_JOB_TRIG_ON_DATA_WITH_VALUE:
			status = _AET_dataWatchpointWithValue(params);
			break;
		case AET_JOB_TRIG_ON_DATA_RANGE_WITH_VALUE:
			status = _AET_dataWatchpointRange(params, AET_DRC_DATAQUAL_QUALIFY);
			break;
		case AET_JOB_TRIG_ON_TIMER_ZERO:
			status = _AET_timerZeroTrigger(params);
			break;
		case AET_JOB_TRIG_ON_EVENTS:
			status = _AET_triggerOnEvent(params);
			break;
		case AET_JOB_ADD_TRIGGER:
			status = _AET_addTrigger(params);
			break;
		case AET_JOB_INTR_ON_STALL:
			status = _AET_IntrOnStall(params);
			break;
		case AET_JOB_COUNT_STALLS:
			status = _AET_CountStall(params);
			break;
		case AET_JOB_INTR_STALL_DURATION:
			status = _AET_IntrStallDuration(params);
			break;
#ifdef _TMS320C6400_PLUS
		case AET_JOB_FUNC_PROFILE:
			status = _AET_traceFuncProfile(params);
			break;
		case AET_JOB_TIMER_START_ON_EVT:
			status = _AET_eventTriggeredTimerStart(params);
			break;
		case AET_JOB_TIMER_STOP_ON_EVT:
			status = _AET_eventTriggeredTimerStop(params);
			break;
		case AET_JOB_WATERMARK_START_END:
			status = _AET_eventTriggeredWatermarkStartStop(params);
			break;
#endif
		default:
			status = AET_FNOJOBIMPLEMENTED;
	}
	
	return status;
}

/*******************************************************************************
* AET_enableEmuPins
*
* 	Summary - 	On the 64x Plus, configures the TCU_CNTL register to pin out
* 				the EMU0 and EMU1 pins
*
*	Parameters - None
*
*	Returns - AET_error
*******************************************************************************/
AET_error AET_enableEmuPins(void)
{
#ifdef _TMS320C6400_PLUS
	TCU_CNTL = 0x1;
	TCU_CNTL = 0x0000C001;
	TCU_CNTL = 0x0000C002;
	AET_mmrRegs->PIN_MGR_0 = 0x44;
#endif
	return AET_SOK;
}

/*******************************************************************************
* AET_enableEmuPins
*
* 	Summary - 	On the 64x Plus, configures the TCU_CNTL register to its 
*				default value
*
*	Parameters - None
*
*	Returns - AET_error
*******************************************************************************/
AET_error AET_disableEmuPins(void)
{
#ifdef _TMS320C6400_PLUS
	TCU_CNTL = 0x1;
	TCU_CNTL = 0x0000C001;
	TCU_CNTL = 0x0000C002;
	AET_mmrRegs->PIN_MGR_0 = 0x0;
#endif
	return AET_SOK;
}

/* Private Functions */
/*******************************************************************************
* _AET_iregWrite
*
* 	Summary - 	Writes a value to one of the AET indirect registers
*
*	Parameters
*		uint16_t - Index of the register
*		int32_t - Value to be written
*
*	Returns - None
*******************************************************************************/
void _AET_iregWrite(uint16_t index, int32_t value)
{
	_AET_IAR_PRINTF(_AET_debugInfoDatabase[index].regName, value); 

	AET_mmrRegs->IAR_ADD = index;
	AET_mmrRegs->IAR_DAT = value;
}

/*******************************************************************************
* _AET_clearGlobalParams
*
* 	Summary - 	Clears all of the global parameters.  This should be done on the
* 	call to AET_init(), and also on the call to AET_release.  It must be done on
* 	Release so that we don't need to call AET_init again, but we're sure there
* 	are no existing settings
*
*	Parameters - None
*
*	Returns - None
*******************************************************************************/
void _AET_clearGlobalParams(void)
{
	  uint8_t cntCounter;

	/*
	 Initialize the global resource tracking structure so that all of the
	 elements are zero
	 */
	_AET_globalParams.AET_usedResources.CNT = 0;
	_AET_globalParams.AET_usedResources.TB7 = 0;
	_AET_globalParams.AET_usedResources.TB3 = 0;
	_AET_globalParams.AET_usedResources.TB1 = 0;
	_AET_globalParams.AET_usedResources.DCMP = 0;
	_AET_globalParams.AET_usedResources.ACMP = 0;
	_AET_globalParams.AET_usedResources.AEGMUX = 0;
	_AET_globalParams.AET_usedResources.AEG = 0;
	_AET_globalParams.AET_usedResources.SM = 0;
	_AET_globalParams.AET_usedResources.initTbNumber = NULL;
	_AET_globalParams.AET_usedResources.initTbType = _AET_TB_NONE;

	/*
	 Ensure that the Claimed status is False
	 */
	_AET_globalParams.AET_claimed = AET_FALSE;

	/*
	 Set all of the global shadowed registers to zero
	 */
	_AET_globalParams.globRegs.cisBusSel = 0;
	_AET_globalParams.globRegs.tbEnable = 0;
	_AET_globalParams.globRegs.tbDomain = 0;
	_AET_globalParams.globRegs.cntFuncCntl = 0;

	/*
	 Initialize the entries used element of the job table structure to be zero.
	 This effectively clears out the entire job table, since we only care about
	 the indexes that are specified in the entriesUsed bitfield
	 */
	_AET_globalParams.jobTable.entriesUsed = 0;

	/*
	 Initialize the counter configuration tracking array
	 */
	for (cntCounter = 0; cntCounter < AET_NUM_AET_CNT; cntCounter++)
	{
		_AET_globalParams.cntConfig[cntCounter] =
							AET_COUNTER_NOTCONFIGURED;
	}

	/* Initialize the AEG Resource manager */
	_AET_initAegResourceMgr();

	/* Initialize the Trigger Builder Settings Structure */
	_AET_initTbSettingsDB();

	/* Clear the AEG Select Registers */
#ifdef _TMS320C6400_PLUS
	*AET_aegSelRegs0 = 0;
	*AET_aegSelRegs1 = 0;
#endif
}


/*******************************************************************************
* _AET_iregRead
*
* 	Summary - 	Reads a value from one of the AET indirect registers
*
*	Parameters
*		uint16_t - Index of the register
*
*	Returns - int32_t Value in the register
*******************************************************************************/
int32_t _AET_iregRead(uint16_t index)
{
	int32_t value;

	AET_mmrRegs->IAR_ADD = index;
	value = AET_mmrRegs->IAR_DAT;

	return value;
}

/*******************************************************************************
* _AET_insertJobIntoTable
*
* 	Summary - 	Stores a jobs' resources in the resource table
*
*	Parameters
*		_AET_resourceStruct - Structure of resources for a job
*
*	Returns - AET_jobIndex - Index into the job table where these resources 
*			  are stored
*******************************************************************************/
AET_jobIndex _AET_insertJobIntoTable(_AET_resourceStruct configWord)
{
	uint16_t usedJobs;
	uint8_t counter = 0;

	usedJobs = _AET_globalParams.jobTable.entriesUsed;

	while ((usedJobs & (1 << counter)) != 0)
	{
		counter ++;
	}
	
	_AET_globalParams.jobTable.table[counter] = configWord;
	_AET_globalParams.jobTable.entriesUsed |= (1 << counter);
	return (AET_jobIndex) counter;
}

/*******************************************************************************
* _AET_removeJobFromTable
*
* 	Summary - 	Removes a job from the job table and reclaims all of the 
*				resources used by it
*
*	Parameters
*		AET_jobIndex  - Index of the job in the resource table
*		AET_resourceStruct* - Pointer to resource structure
*
*	Returns - AET_error
*******************************************************************************/
AET_error _AET_removeJobFromTable(AET_jobIndex jobIndex, _AET_resourceStruct* configWord)
{
	uint8_t  counter;

	/* Check to be sure the index is used */
	if ((_AET_globalParams.jobTable.entriesUsed & (1 << jobIndex)) == 0)
	{
		return AET_FJOBNOTPROGRAMMED;
	}

	/* ACMP */
	for (counter = 0; counter < AET_NUM_ACMP; counter++)
	{
		if ((configWord->ACMP & (1 << counter)) != 0)
		{
			_AET_clearACMP((uint8_t)counter);
			_AET_globalParams.AET_usedResources.ACMP &= ~(1 << counter);
		}
	}

	/* DCMP */
	for (counter = 0; counter < AET_NUM_DCMP; counter++)
	{
		if ((configWord->DCMP & (1 << counter)) != 0)
		{
			_AET_clearDCMP((uint8_t)counter);
			_AET_globalParams.AET_usedResources.DCMP &= ~(1 << counter);
		}
	}
	

	/* TB7 */
	for (counter = 0; counter < AET_NUM_TB_7WIDE; counter++)
	{
		if ((configWord->TB7 & (1 << counter)) != 0)
		{
			_AET_clearTB7((uint8_t)counter);
			_AET_globalParams.AET_usedResources.TB7 &= ~(1 << counter);
		}
	}

	/* TB3 */
	for (counter = 0; counter < AET_NUM_TB_3WIDE; counter++)
	{
		if ((configWord->TB3 & (1 << counter)) != 0)
		{
			_AET_clearTB3((uint8_t)counter);
			_AET_globalParams.AET_usedResources.TB3 &= ~(1 << counter);
		}
	}

	/* TB1 */
	for (counter = 0; counter < AET_NUM_TB_1WIDE; counter++)
	{
		if ((configWord->TB1 & (1 << counter)) != 0)
		{
			_AET_clearTB1((uint8_t)counter);
			_AET_globalParams.AET_usedResources.TB1 &= ~(1 << counter);
		}
	}

	/* AEGMUX */
	for (counter = 0; counter < AET_NUM_AEGMUXLINE; counter++)
	{
		if ((configWord->AEGMUX & (1 << counter)) != 0)
		{	
			_AET_globalParams.AET_usedResources.AEGMUX &= ~(1 << counter);
		}
	}

	/* AEG */
	_AET_reclaimAegs(configWord->AEG);

	/* Remove from the list of used entries */
	_AET_globalParams.jobTable.entriesUsed &= ~(1 << jobIndex);
	
	return AET_SOK;

}

_AET_drcByteEnable _AET_getByteEnableValue(int32_t dataAddress, AET_refSize refSize)
{
	_AET_drcByteEnable beValue;
	uint8_t mask;
	uint8_t alignment;

	/* Calculate alignment based on 2 LSB of address */
	alignment = (uint8_t)(dataAddress & 0x3);

	/* Set the mask depending on the reference size */
	switch (refSize)
	{
		case AET_REF_SIZE_BYTE:
			mask = (uint8_t)0x1;
			break;
		case AET_REF_SIZE_HALFWORD:
			mask = (uint8_t)0x3;
			break;
		case AET_REF_SIZE_WORD:
			mask = (uint8_t)0xF;
			break;
		case AET_REF_SIZE_DOUBLEWORD:
			mask = (uint8_t)0xFF;
			break;
		default:
			mask = (uint8_t)0xF;
			break;
	}

	beValue = (mask << alignment) & 0xFF;


	return beValue;
}

/* Programs the Dual Range Comparator Control Register based on
   the passed parameters
*/
void _AET_pgmDrcCntlReg(_AET_drcCntlParams* drcParams,
						AET_jobParams* jobParams
						)
{
	_AET_iregWrite(drcParams->cmpBaseIndex,
		AET_FMKT(AET_IAR_DRC_CNTL_QLU_INASENSE, ACTIVE_HIGH) |
		AET_FMKT(AET_IAR_DRC_CNTL_QLU_EXACT, INEXACT) |
		AET_FMK(AET_IAR_DRC_CNTL_QLU_REF, jobParams->refSize) |
		AET_FMKT(AET_IAR_DRC_CNTL_QLU_EXE, QUALIFY) |
		AET_FMKT(AET_IAR_DRC_CNTL_QLU_STALL, QUALIFY) |
		AET_FMKT(AET_IAR_DRC_CNTL_QLU_ETYPE, QUALIFY) |
		AET_FMK(AET_IAR_DRC_CNTL_QLU_DQ, drcParams->dataQualify) |
		AET_FMK(AET_IAR_DRC_CNTL_QLU_RW, jobParams->readWrite) |
		AET_FMK(AET_IAR_DRC_CNTL_QLU_ACCESS, drcParams->accessType ) |
		AET_FMK(AET_IAR_DRC_CNTL_BE, drcParams->byteEnableMask) |
		AET_FMK(AET_IAR_DRC_CNTL_ES, drcParams->eventSenseMask) |
		AET_FMK(AET_IAR_DRC_CNTL_GT, drcParams->cmpGreater) |
		AET_FMK(AET_IAR_DRC_CNTL_LT, drcParams->cmpLess) |	
		AET_FMKT(AET_IAR_DRC_CNTL_EC, NORMAL) |
		AET_FMKT(AET_IAR_DRC_CNTL_SCD, DOMAIN_0)
		);
}

/* Programs the Dual Range Comparator Address reference register
   based on the passed parameters
*/
void _AET_pgmDrcArefReg(
	_AET_drcCntlParams* drcParams
	)
{
	_AET_iregWrite((uint16_t)(drcParams->cmpBaseIndex + 1),
		AET_FMK(AET_IAR_DRC_AREF_AREF, drcParams->addressReference) 
	);
} 

/******************************************************************************
* _AET_getAegBaseIndex
*
* Summary:
* 	Calculates the base index for the specific Auxilliary Event Generator that
*   is to be used. This function is needed because the registers for AEG 4 are 
*	located in a different memory location than the rest of the AEG registers.
*
* Parameters:
*   aegNo -		Auxilliary event generator number.  This number is allocated by
*				the resource allocation.  
*
* Returns:
*	uint16_t value of the register index
*				
*******************************************************************************/
uint16_t _AET_getAegBaseIndex(uint8_t aegNo)
{
	uint16_t aetAegBaseIndex;

	if (aegNo == (uint8_t)3)
	{
		aetAegBaseIndex = 0x288;
	}
	else
	{
		aetAegBaseIndex = AET_indexof(CSL_Aet_iarRegs, AUX); 
		aetAegBaseIndex += aegNo * (sizeof(CSL_Aet_iarAuxRegs) >> 2);	
	}

	return aetAegBaseIndex;
}


/******************************************************************************
* _AET_triggerCexpAdd
* 	Parameters:
*		cexpRegNumber - Number of the CEXP register
* 		tbBaseIndex - Base Index to the trigger builder that is to be used
*		tableValue - 32 bit	Boolean value for the look up table
* 		
******************************************************************************/
AET_error _AET_triggerCexpAdd(uint8_t cexpNo, uint8_t tbBaseIndex, int32_t lutValue)
{
	AET_error status = AET_SOK;
	_AET_iregWrite((uint16_t)(tbBaseIndex + cexpNo), lutValue);
	return status;
}


/*******************************************************************************
* _AET_programAegMux
*   Parameters:
*	signalNumber - 	The number(0-7) of the line that the signal will be connected 
*					to
*
*	eventNumber - 	Actual number (0-255) of the event
*
*	returns: AET_error status message
*
*******************************************************************************/
AET_error _AET_programAegMux(uint8_t signalNumber, uint8_t eventNumber)
{
	int32_t regValue = 0;
	int32_t regMask;


	/* signalNumber will be AUX_EVT[9:16], which map to AEGEVT[0:7] */
	uint8_t aegSignalNo = (uint8_t)(signalNumber - 9);

	/* Check to be sure the signal value is feasible */
	if (aegSignalNo > (uint8_t)7)
		return AET_FSIGNALOUTOFRANGE;

	/* Get the bits in the correct location specified by the signalNumber */
	regValue = ((int32_t) eventNumber) << (8 * (aegSignalNo % 4));
	regMask = ~(0xFF << (8 * (aegSignalNo % 4)));
			
	if (aegSignalNo < (uint8_t)4)
	{
		*AET_aegSelRegs0 &= regMask;
		*AET_aegSelRegs0 |= regValue;
	}
	else
	{
		*AET_aegSelRegs1 &= regMask;
		*AET_aegSelRegs1 |= regValue;
	}

	_AET_AEGSEL(0, *AET_aegSelRegs0);
	_AET_AEGSEL(1, *AET_aegSelRegs1);

	return AET_SOK;
	
		
}
/******************************************************************************/
