/*
 * aet_evt_timer_stop.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =====================aet_evt_timer_stop.c==================================
 *  Revision Information
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_evt_timer_stop.h>
#include <aet_evt_timer_common.h>

extern _AET_globalVars _AET_globalParams;

AET_error _AET_eventTriggeredTimerStop(AET_jobParams *params)
{
	_AET_resourceStruct configWord;
	AET_error status = AET_SOK;

	_AET_evtTmrCheckInit(&configWord,
						 params->counterNumber,
						 AET_COUNTER_OP_MODE_GENERIC
						 ); 

	switch(params->counterNumber)
	{
		/* If this is counter 0, use AEG1, 1TB2*/
		case AET_CNT_0:
//			configWord.AEG = 0x2;
			configWord.TB1 = 0x4;
			break;
		/* If counter 1, use AEG3, 1TB4*/
		case AET_CNT_1:
//			configWord.AEG = 0x8;
			configWord.TB1 = 0x10;
			break;
		default:
			return AET_FCNTNUMBERINVALID;
	}

	/*
	 Reserve the resources that we need.  This will fail if the resources
	 are not available.
	 */
	if ((status = _AET_reserveResources(
					&configWord,
					params)
					) != AET_SOK)
		return status;

	/* 
	 Program the Counter Stop job 
	 */
	if ((status = _AET_eventTriggeredTimerCommon(params, 
												 AET_COUNTER_ACTION_STOP,
												 &configWord)
												 ) != AET_SOK)
	{
		_AET_reclaimResources(configWord);
		return status;
	}
	/* 
	 Insert this job into the global job table and stroe the job number
	 into the parameter structure passed by the user
	 */
	params->jobIndex = _AET_insertJobIntoTable(configWord);	 
	return status;

}
