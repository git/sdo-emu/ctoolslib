/*
 * aet_evt_timer_common.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =====================aet_evt_timer_common.c=================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_evt_timer_common.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_eventTriggeredTimerCommon(AET_jobParams* params, 
										_AET_counterAction action,	
										_AET_resourceStruct* configWord)
{
	uint16_t aetAegBaseIndex;
	uint8_t tbNo;
	uint8_t aegNo;
	uint8_t signalNo;
	AET_error status = AET_SOK;
	_AET_triggerBuilderInfo tbInfoPtr;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;

	/* Start/Stop Specific terms */
	uint8_t cnt0tbNo;
	uint8_t cnt0aegNo;
	uint8_t cnt1tbNo;
	uint8_t cnt1aegNo;
	int32_t aegCntlCfg;

	switch (action)
	{
		case AET_COUNTER_ACTION_STOP:
			cnt0tbNo = 2;
			cnt0aegNo = 1;
			cnt1tbNo = 4;
			cnt1aegNo = 3;
			aegCntlCfg = AET_FMK(AET_IAR_AUX_EVT_CNTL_TDR,0x0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_TMD,DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SCD,DOMAIN0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SYNC,DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_EDGE,DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_ALIGN, DISABLE)|
					AET_FMK(AET_IAR_AUX_EVT_CNTL_EP, 0xCC);
			break;
		default:	/* Assume start */
			cnt0tbNo = 1;
			cnt0aegNo = 0;
			cnt1tbNo = 3;
			cnt1aegNo = 2;
			aegCntlCfg = AET_FMK(AET_IAR_AUX_EVT_CNTL_TDR,0x0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_TMD,DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SCD,DOMAIN0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SYNC,DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_EDGE,ENABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_ALIGN, DISABLE)|
					AET_FMK(AET_IAR_AUX_EVT_CNTL_EP, 0x44);
			break;
	}


	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();

	/* 
	 Set up the base indexes of the trigger builder and the AEG resources
	 that are needed
	 */
	 switch (params->counterNumber)
	 {
		case AET_CNT_0:
			tbNo = (uint8_t)cnt0tbNo;
			aegNo = (uint8_t)cnt0aegNo;
			break;
		case AET_CNT_1:
			tbNo = (uint8_t)cnt1tbNo;
			aegNo = (uint8_t)cnt1aegNo;
			break;			
		default:
			return AET_FCNTNUMBERINVALID;
	 }

	 aetAegBaseIndex = _AET_getAegBaseIndex(aegNo);
	 tbParams.tbBaseIndex = 
	 	_AET_getTbBaseIndex(_AET_TB_1_WIDE, tbNo);
	 
	/* Configure the AEG */
	_AET_iregWrite(aetAegBaseIndex, aegCntlCfg);

	/* Do Dynamic Signal Allocation if necessary */
	if ((status = _AET_getEventNumber(params->eventNumber[0], &signalNo)) != AET_SOK)
	{
		return status;
	}

	/* AEG_ENA */
	_AET_iregWrite((uint16_t)(aetAegBaseIndex + 1),
				AET_FMK(AET_IAR_AE_ENA_ENA, 1 << signalNo));

	/* TB_CNTL */
	tbParams.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, configWord->AEG << 11);


	tbParams.tbType = _AET_TB_1_WIDE;
	tbParams.outputCntl = 0x0;
	tbParams.triggers = 0x1;
	tbParams.boolOutput = 0xAAAA;
	_AET_pgmTrigBldr(
				&tbParams,
				params
				);


	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);


	/* CIS_BIS_SEL */
	/* Set the input of the AEG to be Miscellaneous Events */
	_AET_globalParams.globRegs.cisBusSel &= 0xFFFCFFFF;
	_AET_globalParams.globRegs.cisBusSel |= 0x00020000; 

	/* If this is an external event, need to set up AEGMUX correctly */


	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* TB_DM */
	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(tbNo, 0);

	tbInfoPtr.tbType = _AET_TB_1_WIDE;
	tbInfoPtr.triggerBuilderNo = tbNo;

	/* Enable Trigger Builders */
	if ((status =_AET_markEnableTb(&tbInfoPtr)
								   ) != AET_SOK)
	{
		return status;
	} 


	_AET_PRINTF("---End Job Event Timer Stop---\n");

	return AET_SOK;

}


AET_error _AET_evtTmrCheckInit(_AET_resourceStruct* configWordPtr, 
							   AET_counters counterNumber,
							   _AET_counterOpMode operatingMode)
{

	AET_error status = AET_SOK;
	AET_counterConfig upperCntConfig;
	AET_counterConfig lowerCntConfig;
	
	if (operatingMode == AET_COUNTER_OP_MODE_GENERIC)
	{
		upperCntConfig = AET_COUNTER_TRAD_ONESHOT;
		lowerCntConfig = AET_COUNTER_TRAD_COUNTER;
	} else {
		upperCntConfig = AET_COUNTER_MAXWM_ONESHOT;
		lowerCntConfig = AET_COUNTER_MINWM_COUNTER;
	}


		/* 
	 Check to see if this counter has been configured 
	 */
	if (_AET_globalParams.cntConfig[counterNumber] ==
							AET_COUNTER_NOTCONFIGURED)
	{
		return AET_FCNTNOTCONFIG;
	}

	/* 
	 Check to see if the counter is configured correctly (i.e. 
	 */
	if (_AET_globalParams.cntConfig[counterNumber] < 
				lowerCntConfig) 
	{
		return AET_FCNTCONFIGINVALID;
	}

	if (_AET_globalParams.cntConfig[counterNumber] > 
				upperCntConfig) 
	{
		return AET_FCNTCONFIGINVALID;
	}		
	
	_AET_initConfigWord(configWordPtr);

	return status;	
}


