/*
 * aet_trace_startstop_on_pc.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =====================aet_trace_startstop_on_pc.c===========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_trace_startstop_on_pc.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;


AET_error _AET_traceStartStopOnPC(AET_jobParams* params)
{
	/*
	 * The Trace Start/Stop on PC job can be handled with the Add Trigger API.
	 */

	AET_error status = AET_SOK;
	AET_jobParams localParams;
	uint8_t traceTriggers, i, trigger_number;
	uint8_t triggerCount = 1;

	localParams = AET_JOBPARAMS; /* Initialize Job parameter structure */
	localParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;
	localParams.programAddress = params->programAddress;
	traceTriggers = params->traceTriggers;

	//Return error, if no triggers are set
	if(traceTriggers == 0)
	{
		return AET_FINVALIDTRIGMASK;
	}

	switch (params->traceActive)
	{
		case AET_TRACE_ACTIVE:
			traceTriggers &= 0x7F;

			/* Spin until we find the first trigger */
			while ((traceTriggers & triggerCount) == 0){
				triggerCount = triggerCount * 2;
			}

			for(i=0;i<31;i++)
			{
				trigger_number = triggerCount >> i;
				if(trigger_number == 1)
				{
					trigger_number = i+1;
					break;
				}
			}

			/*
			 * Program a Trigger on PC job with the first trigger and then mask
			 * off the existing trigger
			 */
			localParams.triggerType =(AET_triggerType) ((uint8_t)AET_TRIG_TRACE_STORE_PCTAG + trigger_number);
			status = AET_setupJob(AET_JOB_TRIG_ON_PC, &localParams);
			traceTriggers &= ~triggerCount;

			/* Now call Add Trigger for the rest of the triggers */
			while (traceTriggers != 0){
				triggerCount *= 2;
				if ((traceTriggers & triggerCount) != 0){

					for(i=0;i<31;i++)
					{
						trigger_number = triggerCount >> i;
						if(trigger_number == 1)
						{
							trigger_number = i+1;
							break;
						}
					}

					localParams.triggerType = (AET_triggerType) ((uint8_t)AET_TRIG_TRACE_STORE_PCTAG + trigger_number);
					status = AET_setupJob(AET_JOB_ADD_TRIGGER, &localParams);
					traceTriggers &= ~triggerCount;
				}
			}
			break;
		case AET_TRACE_INACTIVE:
			/*
			 * Ensure that since these are stop params, we are only using the
			 * lower 3 bits
			 */
			traceTriggers &= 0x7;

			/* Spin until we find the first trigger */
			while ((traceTriggers & triggerCount) == 0){
				triggerCount = triggerCount * 2;
			}

			for(i=0;i<31;i++)
			{
				trigger_number = triggerCount >> i;
				if(trigger_number == 1)
				{
					trigger_number = i+1;
					break;
				}
			}

			/*
			 * Program a Trigger on PC job with the first trigger and then mask
			 * off the existing trigger
			 */
			localParams.triggerType = (AET_triggerType) ((uint8_t)AET_TRIG_TRACE_END + trigger_number);
			status = AET_setupJob(AET_JOB_TRIG_ON_PC, &localParams);
			traceTriggers &= ~triggerCount;

			/* Now call Add Trigger for the rest of the triggers */
			while (traceTriggers != 0){
				triggerCount *= 2;
				if ((traceTriggers & triggerCount) != 0){

					for(i=0;i<31;i++)
					{
						trigger_number = triggerCount >> i;
						if(trigger_number == 1)
						{
							trigger_number = i+1;
							break;
						}
					}

					localParams.triggerType = (AET_triggerType) ((uint8_t)AET_TRIG_TRACE_END + trigger_number);
					status = AET_setupJob(AET_JOB_ADD_TRIGGER, &localParams);
					traceTriggers &= ~triggerCount;
				}
			}
			break;
		default:
			return AET_FINVALIDTRACEOPT;
	}
	
	//update job index from local params
	params->jobIndex = localParams.jobIndex;
	
	return status;
}


