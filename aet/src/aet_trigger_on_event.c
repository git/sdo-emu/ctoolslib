/*
 * aet_trigger_on_event.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ========================aet_trigger_on_event.c=============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-05-29 20:55:46 -0500 (Wed, 29 May 2013) $
 *   Revision: $LastChangedRevision: 10628 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_trigger_on_event.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_triggerOnEvent (AET_jobParams *params)
{
	AET_triggerOnEventResources resources;
	AET_error status = AET_SOK;
	

	/* 
	 Initialize the configWord and allocate a Trigger Builder
	 */
	if ((status = _AET_jobInit(&resources.configWord,
							   params->triggerType,
							   &resources.trigBldr
							   ))
							   != AET_SOK)
	{
		return status;
	}

	/*
	 Reserve the resources that we need.  This will fail if resources are
	 not available
	 */
	if ((status = _AET_reserveResources(
					&resources.configWord,
					params)
					) != AET_SOK)
	{
		return status;
	}


	/*
	 Program the Trigger on Event job using the resources specified
	 */
	if ((status = _AET_triggerOnEventProgram(params, &resources)) != AET_SOK)
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}

	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);


	return status;
	
}

AET_error _AET_triggerOnEventProgram(AET_jobParams* params,
									AET_triggerOnEventResources* resources)
{
	AET_error status = AET_SOK;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;

	_AET_PRINTF("---Begin Job Trigger On Event---\n");

	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();

	/* 
	 Set up the base indexes of the trigger builder.  
	 */ 
	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldr->tbType,
			resources->trigBldr->triggerBuilderNo
		);

	/* Configure the Trigger Builder Parameters */
	tbParams.tbType = resources->trigBldr->tbType;
	tbParams.outputCntl = resources->trigBldr->outputCntl;
	tbParams.triggers = 1 << (resources->trigBldr->triggerNo - 1);
	tbParams.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, resources->configWord.AEG << 11);
	tbParams.boolOutput = 0xAAAA;
	tbParams.tbNo = resources->trigBldr->triggerBuilderNo;
	
	_AET_pgmTrigBldr(
			&tbParams,
			params
			);

	/* FUNC_CNTL */
	_AET_programFuncCtlReg(params);

	/* TB_DB */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo,0);

	/* 
	 Add this trigger builder into the list of trigger builders to be enabled
	 and enable them all
	 */
	if ((status = _AET_markEnableTb(resources->trigBldr)) != AET_SOK)
	{
		return status;
	}

	_AET_PRINTF("-----End Job Trigger on Event-----\n");

	return status;
}
