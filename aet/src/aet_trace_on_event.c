/*
 * aet_trace_on_event.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =========================aet_trace_on_event.c==============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_trace_on_event.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_traceOnEvent(AET_jobParams* params)
{
	AET_error status = AET_SOK;
	_AET_resourceStruct configWord;

	/* Initialize the configWord */
	_AET_initConfigWord(&configWord);

	/* 
	 Static Resources for this job are:
	 AEG1, 7TB0
	 */
	configWord.TB7 = 0x1;


	/* 
	 Reserve the resources that we need.  This will fail if the resources
	 are not available.
	 */
	if (status = _AET_reserveResources(
					&configWord,
					params)
					)
		return status;

	/*
	 Program the Trace on Event Job
	 */
	if (status = _AET_traceOnEventProgram(params, &configWord))
	{
		_AET_reclaimResources(configWord);
		return status;
	}

	/*
	 Insert this job into the global job table and store the job number
	 into the parameter structure passed by the user
	 */
	params->jobIndex = _AET_insertJobIntoTable(configWord);
	
	return status;
}


AET_error _AET_traceOnEventProgram(AET_jobParams* params, _AET_resourceStruct* configWord)
{
	AET_error status = AET_SOK;
	_AET_triggerBuilderInfo tbInfoPtr;
	uint8_t triggerBuilderNo;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;


	_AET_PRINTF("---Begin Job Program Trace On Event---\n");

	/* Disable All Trigger Builders */
	_AET_DISABLE_TBS();

	/*
	 Set up the base indexes of the trigger builder and AEG resources.
	 These indexes are statically assigned to 7TB0, AEG1
	 */

	tbParams.tbType = _AET_TB_7_WIDE;
	triggerBuilderNo = 0;

	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			tbParams.tbType,	
			triggerBuilderNo
		);


	/* Program Trigger Builders */

	/*
	 Set the boolean output of the trigger builders depending on active /
	 inactive
	 */
	switch(params->traceActive)
	{
		case AET_TRACE_ACTIVE:
			tbParams.boolOutput = 0xAAAA;
			break;
		case AET_TRACE_INACTIVE:
			tbParams.boolOutput = 0x5555;
			break;
		default:
			return AET_FINVALIDTRACEOPT;
	}


	tbParams.tbOrsValue = AET_FMK(AET_IAR_TB_ORS_MASK_A, configWord->AEG << 11);

	tbParams.outputCntl = 0x0;
	tbParams.triggers = params->traceTriggers;
	_AET_pgmTrigBldr(
			&tbParams,
			params
			);

	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);


	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(triggerBuilderNo, 0);





	/*
	 Add this trigger builder to the list of trigger builders that will be enabled,
	 and enable them all
	 */

	tbInfoPtr.tbType = _AET_TB_7_WIDE;
	tbInfoPtr.triggerBuilderNo = 0;
	if ((status =_AET_markEnableTb(&tbInfoPtr)) != AET_SOK)
	{
		return status;
	} 


	_AET_PRINTF("---End Job Program Trace On Event---\n");

	return status;
}

