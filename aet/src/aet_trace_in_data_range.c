/*
 * aet_trace_in_data_range.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =====================aet_trace_in_data_range.c=============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_trace_in_data_range.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_traceInDataRange(AET_jobParams* params)
{
	AET_error status = AET_SOK;
	AET_traceInDataRangeResources resources;
	_AET_triggerBuilderInfo tbInfo;

	/* Initialize the configword in the resources structure */
	_AET_initConfigWord(&resources.configWord);

	/* 
	 Check to ensure that the Start Address < End Address.  If not, swap them 
	 */
	if (params->traceStartAddress > params->traceEndAddress)
	{
		int32_t tempAddress = params->traceStartAddress;
		params->traceStartAddress = params->traceEndAddress;
		params->traceEndAddress = tempAddress;
	}

	/* 
	 Select the trigger builder that the job will use.
	 */
	tbInfo.triggerBuilderNo = 0;
	tbInfo.tbType = _AET_TB_7_WIDE;
	

	/* 
	 In this scenario, we can have either a Trace Active Job or a trace 
	 inactive job.  This is different than the Trace in range on PC
	 because we have to let the Event Sense logic make an AND between
	 two comparators, and then we only need to or the two outputs to
	 the trigger builder.  Where trace_on_pc_in_range used A&B, we need
	 only use A here.
	 */
	switch(params->traceActive)
	{
		case AET_TRACE_ACTIVE:
			resources.tbLookUpTableValue = 0xAAAA;
			break;
		case AET_TRACE_INACTIVE:
			resources.tbLookUpTableValue = 0x5555;
			break;
		default:
			return AET_FINVALIDTRACEOPT;
	}

	resources.trigBldr = &tbInfo;

	/* 
	 Set the Trigger Builder bits in the configword
	 */
	_AET_setTrigBldrBits(&resources.configWord,
						tbInfo.tbType,
						tbInfo.triggerBuilderNo
						);

	/*
	 Fill in the appropriate field in the config word with the address
	 comparators selected.  These comparators are statically assigned.
	 */
	resources.acmpNo[0] = 2;
	resources.acmpNo[1] = 3;
	resources.acmpNo[2] = 0;
	resources.acmpNo[3] = 1;
	resources.configWord.ACMP |= (1 << resources.acmpNo[0]);
	resources.configWord.ACMP |= (1 << resources.acmpNo[1]);
	resources.configWord.ACMP |= (1 << resources.acmpNo[2]);
	resources.configWord.ACMP |= (1 << resources.acmpNo[3]);

	/*
	 Reserve the resources that we need.  This will fail if the resources are
	 not available
	 */
	if (status = _AET_reserveResources(
					&resources.configWord,
					params)
					)
	{
		return status;
	}

	/* 
	 Program the job using the resources specified
	 */
	if (status = _AET_traceInDataRangeProgram(&resources, params))
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}
	
	/*
	 Insert the used resources into the job table and set the params->jobIndex
	 */
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);

	return status;

}

AET_error _AET_traceInDataRangeProgram(AET_traceInDataRangeResources* resources,
										AET_jobParams* params)
{
	AET_error status = AET_SOK;
	_AET_drcCntlParams drcParams1 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams2 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams3 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams4 = AET_DRCCNTLPARAMS;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;
	_AET_PRINTF("---Begin Job Trace In Data Range---\n");

	/* Disable All TriggerBuilders */
	_AET_DISABLE_TBS();

	/* 
	 Get the base indexes of each of the comparators we will use.  The base 
	 index is actually the address of the dual range comparator control 
	 register.  All of the other addresses for programing the comparator will
	 be derived by the offset from the DRC_CNTL register.  The table below 
	 shows the indexes for all comparator registers.
	 */

/*
	----------------------------------------------------------------------------
						Comparator Register Indexes 
	----------------------------------------------------------------------------
	| REGISTER NAME | INDEX |                   COMMENT 					   |
	|---------------------------------------------------------------------------
	| DRC_CNTL_0	|  0x0	| Dual Range Comparator 0 Control register		   |
	| DRC_AREF_0	|  0x1  | Comparator 0 Address Reference				   |
	| DRC_DREFL_0	|  0x2  | Comparator 0 Data Reference Low				   |
	| DRC_MREFL_0	|  0x3  | Comparator 0 Mask Reference Low				   |
	| DRC_DREFH_0	|  0x4  | Comparator 0 Data Reference High				   |
	| DRC_MREFH_0	|  0x5  | Comparator 0 Mask Reference High				   |
	| DRC_CNTL_1	|  0x8	| Dual Range Comparator 1 Control register		   |
	| DRC_AREF_1	|  0x9  | Comparator 1 Address Reference				   |
	| DRC_DREFL_1	|  0xA  | Comparator 1 Data Reference Low				   |
	| DRC_MREFL_1	|  0xB  | Comparator 1 Mask Reference Low				   |
	| DRC_DREFH_1	|  0xC  | Comparator 1 Data Reference High				   |
	| DRC_MREFH_1	|  0xD  | Comparator 1 Mask Reference High				   |
	| DRC_CNTL_2	| 0x10	| Dual Range Comparator 2 Control register		   |
	| DRC_AREF_2	| 0x11  | Comparator 2 Address Reference				   |
	| DRC_DREFL_2	| 0x12  | Comparator 2 Data Reference Low				   |
	| DRC_MREFL_2	| 0x13  | Comparator 2 Mask Reference Low				   |
	| DRC_DREFH_2	| 0x14  | Comparator 2 Data Reference High				   |
	| DRC_MREFH_2	| 0x15  | Comparator 2 Mask Reference High				   |
	| DRC_CNTL_3	| 0x18	| Dual Range Comparator 3 Control register		   |
	| DRC_AREF_3	| 0x19  | Comparator 3 Address Reference				   |
	| DRC_DREFL_3	| 0x1A  | Comparator 3 Data Reference Low				   |
	| DRC_MREFL_3	| 0x1B  | Comparator 3 Mask Reference Low				   |
	| DRC_DREFH_3	| 0x1C  | Comparator 3 Data Reference High				   |
	| DRC_MREFH_3	| 0x1D  | Comparator 3 Mask Reference High				   |
	| DRC_CNTL_4	| 0x20	| Dual Range Comparator 4 Control register		   |
	| DRC_AREF_4	| 0x21  | Comparator 4 Address Reference				   |
	| DRC_DREFL_4	| 0x22  | Comparator 4 Data Reference Low				   |
	| DRC_MREFL_4	| 0x23  | Comparator 4 Mask Reference Low				   |
	| DRC_DREFH_4	| 0x24  | Comparator 4 Data Reference High				   |
	| DRC_MREFH_4	| 0x25  | Comparator 4 Mask Reference High				   |
	| DRC_CNTL_5	| 0x28	| Dual Range Comparator 5 Control register		   |
	| DRC_AREF_5	| 0x29  | Comparator 5 Address Reference				   |
	| DRC_DREFL_5	| 0x2A  | Comparator 5 Data Reference Low				   |
	| DRC_MREFL_5	| 0x2B  | Comparator 5 Mask Reference Low				   |
	| DRC_DREFH_5	| 0x2C  | Comparator 5 Data Reference High				   |
	| DRC_MREFH_5	| 0x2D  | Comparator 5 Mask Reference High				   |
	----------------------------------------------------------------------------	 
	*/

	drcParams1.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[0]);
	drcParams2.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[1]);
	drcParams3.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[2]);
	drcParams4.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[3]);

	/*
	 Get the base index of the trigger builder that is used for this job.  The
	 base address for a given trigger builder is the address of the TB_ORS 
	 (sometimes called TB_BES) register.  The indexes for all of the other 
	 trigger builder registers are derived by the base index and an offset.
	 The table below shows the address of all of the trigger builder registers.
	 */

	/* 
	----------------------------------------------------------------------------
					Trigger Builder Register Indexes 
	----------------------------------------------------------------------------
	| REGISTER NAME |  INDEX  |                  COMMENT 					   |
	|---------------------------------------------------------------------------
    | 					STATE MACHINE TRIGGERS                                 |
	|--------------------------------------------------------------------------|
	| SM_ORS_0W_0	|  0xA4	 | State Machine TB0 ORS Register	  			   |
	| SM_CNTL_0W_1	|  0xA5  | State Machine TB0 Trig 1 Control  			   |
	| SM_CEXP_0W_2  |  0xA6  | State Machine TB0 Trig 2-3 Control   		   |
	| SM_ORS_1W_0	|  0xA7	 | State Machine TB1 ORS Register	   			   |
	| SM_CNTL_1W_1	|  0xA8  | State Machine TB1 Trig 1 Control 			   |
	| SM_CEXP_1W_2  |  0xA9  | State Machine TB1 Trig 2-3 Control 			   |
	| SM_ORS_2W_0	|  0xAA	 | State Machine TB2 ORS Register	   			   |
	| SM_CNTL_2W_1	|  0xAB  | State Machine TB2 Trig 1 Control  			   |
	| SM_CEXP_2W_2  |  0xAC  | State Machine TB2 Trig 2-3 Control  			   |
	| SM_ORS_3W_0	|  0xAD	 | State Machine TB3 ORS Register	   			   |
	| SM_CNTL_3W_1	|  0xAE  | State Machine TB3 Trig 1 Control  			   |
	| SM_CEXP_3W_2  |  0xAF  | State Machine TB3 Trig 2-3 Control  			   |
	|--------------------------------------------------------------------------|
    | 					  WIDE TRIGGER BUILDERS                                |
	|--------------------------------------------------------------------------|
	| TB0_ORS_0W_0	|  0xC0  | 7-Wide TB0 Ors Register						   |
	| TB0_CNTL_0W_1 |  0xC1  | 7-Wide TB0 Trigger 1 Control					   |
	| TB0_CEXT_0W_2 |  0xC2  | 7-Wide TB0 Trigger 2-3 Control				   |
	| TB0_CEXT_0W_4 |  0xC3  | 7-Wide TB0 Trigger 4-5 Control				   |
	| TB0_CEXT_0W_6 |  0xC4  | 7-Wide TB0 Trigger 6-7 Control				   |
	| TB0_ORS_1W_0	|  0xC8  | 7-Wide TB1 Ors Register						   |
	| TB0_CNTL_1W_1 |  0xC9  | 7-Wide TB1 Trigger 1 Control					   |
	| TB0_CEXT_1W_2 |  0xCA  | 7-Wide TB1 Trigger 2-3 Control				   |
	| TB0_CEXT_1W_4 |  0xCB  | 7-Wide TB1 Trigger 4-5 Control				   |
	| TB0_CEXT_1W_6 |  0xCC  | 7-Wide TB1 Trigger 6-7 Control				   |	  
	| TB1_ORS_2W_0  |  0xD0  | 3-Wide TB2 Ors Register						   |
	| TB1_CNTL_2W_1 |  0xD1  | 3-Wide TB2 Trigger 1 Control					   |
	| TB1_CEXT_2W_2 |  0xD2  | 3-Wide TB2 Trigger 2-3 Control				   |
	| TB1_ORS_3W_0  |  0xD0  | 3-Wide TB3 Ors Register						   |
	| TB1_CNTL_3W_1 |  0xD1  | 3-Wide TB3 Trigger 1 Control					   |
	| TB1_CEXT_3W_2 |  0xD2  | 3-Wide TB3 Trigger 2-3 Control				   |
	|--------------------------------------------------------------------------|
    | 					  1-BIT TRIGGER BUILDERS                               |
	|--------------------------------------------------------------------------|
	| TB_ORS_0      | 0x140  | 1-Wide TB0 Ors Register						   |
	| TB_CNTL_0		| 0x141  | 1-Wide TB0 Control Register					   |
	| TB_ORS_1      | 0x142  | 1-Wide TB1 Ors Register						   |
	| TB_CNTL_1		| 0x143  | 1-Wide TB1 Control Register					   |
	| TB_ORS_2      | 0x144  | 1-Wide TB2 Ors Register						   |
	| TB_CNTL_2		| 0x145  | 1-Wide TB2 Control Register					   |
	| TB_ORS_3      | 0x146  | 1-Wide TB3 Ors Register						   |
	| TB_CNTL_3		| 0x147  | 1-Wide TB3 Control Register					   |
	| TB_ORS_4      | 0x148  | 1-Wide TB4 Ors Register						   |
	| TB_CNTL_4		| 0x149  | 1-Wide TB4 Control Register					   |
	| TB_ORS_5      | 0x14A  | 1-Wide TB5 Ors Register						   |
	| TB_CNTL_5		| 0x14B  | 1-Wide TB5 Control Register					   |
	|--------------------------------------------------------------------------| 
	*/

	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldr->tbType,
			resources->trigBldr->triggerBuilderNo
			);

	/* DRC_NA_CNTL Register */
	/*
	 The DRC_NA_CNTL register allows configuration of the Dual Range Comparator
	 In this scenario, we need to program two Address Comparator registers
	 in a pair.  The lower numbered one will be the start address for our
	 job. The higher one will be the end address for our job.  The tables below
	 detail the elements of the DRC_NA_CNTL register
     */

	/*
	-----------------------------------------------------------------
	|3|3|2|2|2|2|2|2|2|2|2|2|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|0|0|0|0|0|       
	|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|
	|	                            |               |       | | | | |
	|          QUALIFIER            |     BYTE      | EVENT |G|L|E|S| 
	|           LOOKUP              |    ENABLE     | SENSE |T|T|C|C|
	|           [15:00]             |    [7:0]      | [3:0] | | | |D|
	-----------------------------------------------------------------
	GT - Greater Than
	LT - Less Than
	EC - Event Control
	SCD - Select Clock Domain

	    ================================================================
		QUALIFIER LOOKUP [15:00]
		-----------------------------------------------------------------
		| 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
		| 5 | 4 | 3 | 2 | 1 | 0 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
		|           |   |   |       |   |   |   |   |   |               |
		|  RESERVED | I | E |  REF  | E | S | E | D | R |    ACCESS     |
		|           | N | X |       | X | T | T | Q | / |               |
		|           | A | A |       | E | A | Y |   | W |               |
		|           | S | C |       |   | L | P |   |   |               |
		|           | E | T |       |   | L | E |   |   |               |
		|           | N |   |       |   |   |   |   |   |               |
		|           | S |   |       |   |   |   |   |   |               |
		|           | E |   |       |   |   |   |   |   |               |
		-----------------------------------------------------------------
		INASENSE - 	Specifies whether the event sense output should be 
					considered active high or active low
					0 - ACTIVE HIGH
					1 - ACTIVE LOW

		EXACT - Defines the exactness of the Address comparison
					0 - Byte touching address comparison method used
					1 - Exact address comparison method used

		REF - Defines the size of the address and data reference values
					00b - Byte
					01b - Half-Word
					10b - Word
					11b - Double Word

		EXE - Defines how qualification of PC bus events with single 
			  instruction dispatches is handled
					0 - Masks the qualification of PC bus events with single
						instruction dispatches
					1 - Permits the qualification of PC bus events with single
						instruction dispatches.

		STALL - Defines how qualification of PC bus events with active pipeline
				cycles is handled
					0 - Permits qualification of events with active pipeline 
						cycles.
					1 - Masks qualification of events with active pipeline 
						cycles.

		DQ - (Data Qualification) - Defines how qualification with the DCQ 
			 register compare is handled with the address compare.
					0 - Masks qualification of the DCQ with address compare.
					1 - Permits qualification of the DCQ with address compare.

		R/W - Defines whether the comparator detects read or write operations.
					0 - Detect Write operation
					1 - Detect Read operation

		ACCESS - Defines specific types of accesses that the comparator 
				 will trigger on

				 0000 - No Accesses
				 0001 - Byte
				 0010 - Halfword
				 0011 - Halfword, Byte
				 0100 - Word
				 0101 - Word, Byte
				 0110 - Word, Halfword
				 0111 - Word, Halfword, Byte
				 1000 - Doubleword
				 1001 - Doubleword, Byte
				 1010 - Doubleword, Halfword
				 1011 - Doubleword, Halfword, Byte
				 1100 - Doubleword, Word
				 1101 - Doubleword, Word, Byte
				 1110 - Doubleword, Word, Halfword
				 1111 - Doubleword, Word, Halfword, Byte
		================================================================

		================================================================
		BYTE ENABLE [7:0]
		================================================================
		This 8-bit field selects when the double word address is 
		declared equal.  These eight bits are programmed to control the
		equals, greater than, and less than conditions for the 
		comparison window.

		TABLE KEY
		---------
		B = BYTES ACCESSED
		R = REFERENCE SIZE
        ===============================================================
		|   COMPARISON   |   ALIGNMENT   |   REFERENCE   |   BE[7:0]  |
		|    CRITERIA    |               |     SIZE      |            |
        |================|===============|===============|============|
        |     B > R      |      00       |     BYTE      |  11111110  |
        |     B > R      |      00       |      HW       |  11111100  |
        |     B > R      |      00       |     WORD      |  11110000  |
        |     B > R      |      00       |      DW       |  00000000  |
        |     B > R      |      01       |     BYTE      |  11111100  |
        |     B > R      |      01       |      HW       |  11111000  |
        |     B > R      |      01       |     WORD      |  11100000  |
        |     B > R      |      01       |      DW       |  00000000  |
        |     B > R      |      10       |     BYTE      |  11111000  |
        |     B > R      |      10       |      HW       |  11110000  |
        |     B > R      |      10       |     WORD      |  11000000  |
        |     B > R      |      10       |      DW       |  00000000  |
        |     B > R      |      11       |     BYTE      |  11110000  |
        |     B > R      |      11       |      HW       |  11100000  |
        |     B > R      |      11       |     WORD      |  10000000  |
        |     B > R      |      11       |      DW       |  00000000  |
        |    B >= R      |      00       |  BYTE HW FW   |  11111111  |
        |    B >= R      |      01       |  BYTE HW FW   |  11111110  |
        |    B >= R      |      10       |  BYTE HW FW   |  11111100  |
        |    B >= R      |      11       |  BYTE HW FW   |  11111000  |
        |   B=R, B!=R    |      00       |     BYTE      |  00000001  |
        |   B=R, B!=R    |      00       |      HW       |  00000011  |
        |   B=R, B!=R    |      00       |     WORD      |  00001111  |
        |   B=R, B!=R    |      00       |      DW       |  11111111  |             
        |   B=R, B!=R    |      01       |     BYTE      |  00000010  |
        |   B=R, B!=R    |      01       |      HW       |  00000110  |
        |   B=R, B!=R    |      01       |     WORD      |  00011110  |
        |   B=R, B!=R    |      01       |      DW       |  11111110  |  
        |   B=R, B!=R    |      10       |     BYTE      |  00000100  |
        |   B=R, B!=R    |      10       |      HW       |  00001100  |
        |   B=R, B!=R    |      10       |     WORD      |  00111100  |
        |   B=R, B!=R    |      10       |      DW       |  11111100  |             
        |   B=R, B!=R    |      11       |     BYTE      |  00001000  |
        |   B=R, B!=R    |      11       |      HW       |  00011000  |
        |   B=R, B!=R    |      11       |     WORD      |  01111000  |
        |   B=R, B!=R    |      11       |      DW       |  11111000  |  
        |    B <= R      |      00       |     BYTE      |  00000001  |
        |    B <= R      |      00       |      HW       |  00000011  |
        |    B <= R      |      00       |     WORD      |  00001111  |
        |    B <= R      |      00       |      DW       |  11111111  |
        |    B <= R      |      01       |     BYTE      |  00000011  |
        |    B <= R      |      01       |      HW       |  00000111  |
        |    B <= R      |      01       |     WORD      |  00011111  |
        |    B <= R      |      01       |      DW       |  11111111  |
        |    B <= R      |      10       |     BYTE      |  00000111  |
        |    B <= R      |      10       |      HW       |  00001111  |
        |    B <= R      |      10       |     WORD      |  00111111  |
        |    B <= R      |      10       |      DW       |  11111111  |
        |    B <= R      |      11       |     BYTE      |  00001111  |
        |    B <= R      |      11       |      HW       |  00011111  |
        |    B <= R      |      11       |     WORD      |  01111111  |
        |    B <= R      |      11       |      DW       |  11111111  |
        |     B < R      |      00       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      01       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      10       |BYTE HW WORD DW|  11111110  |
        |     B < R      |      10       |BYTE HW WORD DW|  11111110  |
        ---------------------------------------------------------------  

		===============================================================
		EVENT SENSE [3:0]
		===============================================================
		This 4-bit field is used a s lookup table to create the      
		comparator output.  This 4-bit field is used to generate the
		input to a 4 to 1 multiplexer whose select controls are the 
		compare out of the two drc comparators.  This field controls
		the sense of the magnitude comparator output, along with the 
		relationship of both comparators of the DRC pair.

		LOOKUP 			REMOTE			LOCAL 
		VALUE		  COMPARISON	  COMPARISON
		----------------------------------------
		ES[3]			 TRUE			TRUE
		ES[2]			 TRUE			FALSE
		ES[1]			 FALSE			TRUE
		ES[0]			 FALSE			FALSE
		
		GT - 1 - Local comparator event is true when input address bits not 
				 involved in crearing the four byte enables are greater than
				 the word reference address

		LT - 1 - Local comparator event is true when input address bits not 
				 involved in crearing the four byte enables are less than the
				 word reference address

		EC - Not Used

		SCD - Select Clock Domain
			0 - Select Clock Domain 0
			1 - Select Clock Domain 1
	*/

	/* 
	 Program the Comparator Control and Address Reference Registers

	 ==============LOWER ADDRESS 1 (START)===============
	 - Reference size is Byte
	 - Don't Qualify with data
	 - Read/Write is passed by user
	 - Watch for all sizes of reads/writes
	 - Use calculated byte enable value
	 - Event Sense is Lookup is 8 (Local & Remote)
	 - Greater than should be true
	 - Less than should be false
	 */
	drcParams1.byteEnableMask = 0xFF;
	drcParams1.eventSenseMask = 0x8;
	drcParams1.cmpGreater = AET_CMP_TRUE;
	drcParams1.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(	
		&drcParams1,
		params
		);

	drcParams1.addressReference = params->traceStartAddress;
	_AET_pgmDrcArefReg(&drcParams1);

	/* 
	 Program the Comparator Control and Address Reference Registers

	 ==============UPPER ADDRESS 1 (END)=================
	 - Reference size is Byte
	 - Don't Qualify with data
	 - Read/Write is passed by user
	 - Watch for all sizes of reads/writes
	 - Use calculated byte enable value
	 - Event Sense is Lookup is 8 (None)
	 - Greater than should be false
	 - Less than should be true
	 */
	drcParams2.byteEnableMask = 0xF;
	drcParams2.eventSenseMask = 0x0;
	drcParams3.cmpGreater = AET_CMP_FALSE;
	drcParams3.cmpLess = AET_CMP_TRUE;
	_AET_pgmDrcCntlReg(
		&drcParams2,
		params
		);

	drcParams2.addressReference = params->traceEndAddress;
	_AET_pgmDrcArefReg(&drcParams2);

	/*
	 ==============LOWER ADDRESS 2 (START)===============
	 - Reference size is Byte
	 - Don't Qualify with data
	 - Read/Write is passed by user
	 - Watch for all sizes of reads/writes
	 - Use calculated byte enable value
	 - Event Sense is Lookup is 8 (Local & Remote)
	 - Greater than should be true
	 - Less than should be false
	 */
	drcParams3.byteEnableMask = 0xFF;
	drcParams3.eventSenseMask = 0x8;
	drcParams3.cmpGreater = AET_CMP_TRUE;
	drcParams3.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg(	
		&drcParams3,
		params
		);

	drcParams3.addressReference = params->traceStartAddress;
	_AET_pgmDrcArefReg(&drcParams3);

	/* 
	 Program the Comparator Control and Address Reference Registers

	 ==============UPPER ADDRESS 2 (END)=================
	 - Reference size is Byte
	 - Don't Qualify with data
	 - Read/Write is passed by user
	 - Watch for all sizes of reads/writes
	 - Use calculated byte enable value
	 - Event Sense is Lookup is 0 (None)
	 - Greater than should be false
	 - Less than should be true
	 */
	drcParams4.byteEnableMask = 0xF;
	drcParams4.eventSenseMask = 0x0;
	drcParams4.cmpGreater = AET_CMP_FALSE;
	drcParams4.cmpLess = AET_CMP_TRUE;
	_AET_pgmDrcCntlReg(	
		&drcParams4,
		params
		);

	drcParams4.addressReference = params->traceEndAddress;
	_AET_pgmDrcArefReg(&drcParams4);

	/* TB_ORS Register */
	/*
	 The TB_ORS (sometimes referred to as TB_BES register) is a 16-input OR gate
	 at the A and B input of the Trigger Builder.  It enables us to OR multiple
	 signals together so that any of the signals can generate the trigger.  In
	 this scenario, we need to use inputs A and B to the trigger builder so that
	 we trigger when they are BOTH high and not when one is high, TB_ORS_A will 
	 specify the output of the low comparator, and TB_ORS_B will specify the 
	 high comparator.  The TB_ORS Register is a 32-bit register with the B-bits
	 in the upper 16, and the A bits in the lower 16.
	 */

	/*
	----------------------------------------------------------------------------
						Trigger Builder ORS Inputs
	----------------------------------------------------------------------------
	|  Index  |  						Name								   |
	----------------------------------------------------------------------------
	|    0    |  ACMP 0														   |
	|    1    |  ACMP 1														   |
	|    2    |  ACMP 2														   |
	|    3    |  ACMP 3														   |
	|    4    |  ACMP 4														   |
	|    5    |  ACMP 5														   |
	|    6    |  NOT USED    												   |
	|    7    |  NOT USED													   |
	|    8    |  NOT USED													   |
	|    9    |  NOT USED													   |
	|    A    |  SAE   														   |
	|    B    |  AEG1 														   |
	|    C    |  AEG2 														   |
	|    D    |  AEG3 														   |
	|    E    |  AEG4  														   |
	|    F    |  RESERVED													   |
	----------------------------------------------------------------------------
	 */

	/* TB_CNTL */
	/* 
		- Type of Trigger Builder (1-Wide, 3-Wide, 7-wide)
		- Base index of Trigger Builder
		- Output Control for Trigger Builder
		- Trigger Builder Lookup Table
		- Trigger Builder Number
	 */
	tbParams.tbOrsValue = 
		AET_FMK(AET_IAR_TB_ORS_MASK_A,
				1 << resources->acmpNo[0] | 1 << resources->acmpNo[2]
				);
	tbParams.tbType = resources->trigBldr->tbType;
	tbParams.outputCntl = 0x0;
	tbParams.triggers = params->traceTriggers;
	tbParams.boolOutput = resources->tbLookUpTableValue;
	tbParams.tbNo = resources->trigBldr->triggerBuilderNo;
	_AET_pgmTrigBldr(
				&tbParams,
				params
				);

	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);

	/* CIS_BUS_SEL */
	/* Set the input to the appropriate comparators to be the data bus */
	/* Zero specifies DA0, one specifies DA1 */
	_AET_globalParams.globRegs.cisBusSel &= (~(1 << 4 * resources->acmpNo[0]));
	_AET_globalParams.globRegs.cisBusSel &= (~(1 << 4 * resources->acmpNo[1]));
	_AET_globalParams.globRegs.cisBusSel |= (1 << (4 * resources->acmpNo[2]));
	_AET_globalParams.globRegs.cisBusSel |= (1 << (4 * resources->acmpNo[3]));
	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo, 0);

	/*
	 Add this trigger builder to the list of trigger builders that will be 
	 enabled, and enable them all
	 */
	if ((status =_AET_markEnableTb(resources->trigBldr)
								   ) != AET_SOK)
	{
		return status;
	} 

	_AET_PRINTF("---End Job Program Trace In Data Range---\n");
			
	return status;
}
