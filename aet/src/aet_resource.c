/*
 * aet_resource.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ===========================aet_resource.c===================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_resource.h>
#include <aet.h>
#include <_aet.h>

extern _AET_globalVars _AET_globalParams;


/* Private Functions */
AET_error _AET_available(_AET_resourceStruct configWord)
{

	if ((configWord.AEG & _AET_globalParams.AET_usedResources.AEG) != AET_SOK) 
		return AET_FNOTAVAILAEG;

	if ((configWord.CNT & _AET_globalParams.AET_usedResources.CNT) != AET_SOK) 
		return AET_FNOTAVAILCNT;

	if ((configWord.TB7 & _AET_globalParams.AET_usedResources.TB7) != AET_SOK) 
		return AET_FNOTAVAILTB7;

	if ((configWord.TB3 & _AET_globalParams.AET_usedResources.TB3) != AET_SOK) 
		return AET_FNOTAVAILTB3;

	if ((configWord.TB1 & _AET_globalParams.AET_usedResources.TB1) != AET_SOK) 
		return AET_FNOTAVAILTB1;

	if ((configWord.DCMP & _AET_globalParams.AET_usedResources.DCMP) != AET_SOK) 
		return AET_FNOTAVAILDCMP;

	if ((configWord.ACMP & _AET_globalParams.AET_usedResources.ACMP) != AET_SOK) 
		return AET_FNOTAVAILACMP;

	return AET_SOK;
}

/*******************************************************
*
*Inputs:
*	numACMP - Number of ACMP's needed
*
*Returns - 
*	acmpNo - Pointer to an array with the acmp numbers
*			 inserted.
*
*********************************************************/
AET_error _AET_getAcmpNo(uint8_t numAcmp, uint8_t* acmpNo)
{
	uint8_t acmpTable[AET_NUM_ACMP] = {AET_ACMP_ORDER};
	uint8_t counter;
	uint8_t eachAcmp;
	uint16_t numReserved = 0;
	unsigned short currentFound;

	for (eachAcmp = 0; eachAcmp < numAcmp; eachAcmp++)
	{
		currentFound = 0;
		counter = 0;
		while (currentFound != 1  && counter < AET_NUM_ACMP)
		{	
			if ((_AET_globalParams.AET_usedResources.ACMP & (1 << acmpTable[counter])) == 0)	
			{

				if ((numReserved & (1 << acmpTable[counter])) == 0)
				{
					acmpNo[eachAcmp] = acmpTable[counter];
					currentFound = 1;
					numReserved |= 1 << acmpTable[counter];
				}
			}
			counter++;
		}

		if (currentFound == 0)
			return AET_FNOTAVAILACMP;

	}

	return AET_SOK;
}

/*******************************************************
*
*Inputs:
*	numACMP - Number of Comparators needed
*
*Returns - 
*	acmpNo - Pointer to an array with the acmp numbers
*			 inserted.
*	dcmpNo - Pointer to an array with the dcmp numbers
*			 inserted
*
*********************************************************/
AET_error _AET_getAcmpWithDataNo(uint8_t numCmp,
								uint8_t* acmpNo,
								uint8_t* dcmpNo,
								_AET_resourceStruct* configWord
								)
{
	uint8_t eachDcmp;
	uint8_t eachAcmp;

	/* 
	 First check DCMP availability 
	 */
	for (eachDcmp = 0; eachDcmp < numCmp; eachDcmp++)
	{
		if ((_AET_globalParams.AET_usedResources.DCMP & (1 << eachDcmp))== 0)
		{
			dcmpNo[eachDcmp] = eachDcmp;
		}
		else
			return AET_FNOTAVAILDCMP;
	}
		
	/* 
	 Now we need one ACMP from each pair 
	 */
	for (eachAcmp = 0; eachAcmp < numCmp; eachAcmp++)
	{
		if ((_AET_globalParams.AET_usedResources.ACMP & (1 << (eachAcmp * 2)))== 0)
		{
				acmpNo[eachAcmp] = 2 * eachAcmp;			
		}
		else
		{
			if ((_AET_globalParams.AET_usedResources.ACMP & ( 1 << ((eachAcmp * 2) + 1)))== 0)
			{
				acmpNo[eachAcmp] = 1 + (2 * eachAcmp);
			}
			else
				return AET_FNOTAVAILACMP;
		}
	}
	
	configWord->ACMP |= (1 << acmpNo[0]);
	configWord->ACMP |= (1 << acmpNo[1]);
	configWord->DCMP |= (1 << dcmpNo[0]);
	configWord->DCMP |= (1 << dcmpNo[1]);
	
	return AET_SOK;
}


/*******************************************************
*
* Inputs - None
*
* Returns - acmpNo - Pointer to an array with the acmp 
*		 	numbers inserted
*
********************************************************/
AET_error _AET_getDataAcmpQuad(uint8_t* acmpNo,
							   _AET_resourceStruct* configWord
							   )
{
	AET_error status = AET_SOK;
	
	/* Check for Pair 0-3 */
	if ((_AET_globalParams.AET_usedResources.ACMP & (0xF))== 0)
	{
		acmpNo[0] = (uint8_t)2;
		acmpNo[1] = (uint8_t)3;	
		acmpNo[2] = (uint8_t)0;
		acmpNo[3] = (uint8_t)1;

	configWord->ACMP |= (1 << acmpNo[0]);
	configWord->ACMP |= (1 << acmpNo[1]);
	configWord->ACMP |= (1 << acmpNo[2]);
	configWord->ACMP |= (1 << acmpNo[3]);

	}
	else
	{
		status = AET_FNOTAVAILACMP;
	}
	return status;

}


/*******************************************************
*  
*Inputs - numDataAcmp - Number of data address 
*		  comparators needed
* 
*Returns - acmpNo - Pointer to an array with the acmp
* 		  numbers inserted
* 
********************************************************/
AET_error _AET_getDataAcmpNo(uint8_t numCmp,
							uint8_t* acmpNo,
							_AET_resourceStruct* configWord
							) 
{
	uint8_t eachDataAcmp;
	unsigned short currentFound;
	uint8_t counter;
	uint8_t numReserved = 0;


	/* 
	 If we're asking for more ACMP's than are available,
	 simply return
	 */

	 if (numCmp > AET_NUM_DATA_ACMP)

		return AET_FNOTAVAILACMP;

	for (eachDataAcmp = 0; eachDataAcmp < numCmp; eachDataAcmp++)
	{
		currentFound = 0;
		counter = 0;
		while ((currentFound != 1) && (counter < AET_NUM_DATA_ACMP))
		{
			if ((_AET_globalParams.AET_usedResources.ACMP & (1 << counter))==0)
			{
				if ((numReserved & (1 << counter))== 0)
				{
					acmpNo[eachDataAcmp] = counter;
					currentFound = 1;
					numReserved |= 1 << counter;
				}
			}
			counter++;
		}

		if (currentFound == 0)
			return AET_FNOTAVAILACMP;
	}

	configWord->ACMP |= (1 << acmpNo[0]);
	configWord->ACMP |= (1 << acmpNo[1]);

	
	return AET_SOK;
}


  
/*
 * Initialize the configWord so that all values are zero
 */
void _AET_initConfigWord(_AET_resourceStruct* configWord)
{
	configWord->AEG = 0;
	configWord->CNT = 0;
	configWord->TB7 = 0;
	configWord->TB3 = 0;
	configWord->TB1 = 0;
	configWord->DCMP = 0;
	configWord->ACMP = 0;
	configWord->AEGMUX = 0;
	configWord->SM = 0;
	configWord->initTbNumber = 0x7;
	configWord->initTbType = _AET_TB_NONE;

}





uint16_t _AET_getAcmpBaseIndex(uint8_t acmpNo)
{
	uint16_t baseIndex;

	baseIndex = (AET_indexof(CSL_Aet_iarRegs, CMP)) +
				(acmpNo * (sizeof(CSL_Aet_iarCmpRegs)>>2));
	return baseIndex;
}


uint16_t _AET_getDcmpBaseIndex(uint8_t dcmpNo)
{
	uint16_t baseIndex;

	baseIndex = (AET_indexof(CSL_Aet_iarRegs, CMP)) + 
				(dcmpNo * (sizeof(CSL_Aet_iarCmpRegs)>>2));

	return baseIndex;
}


AET_error _AET_reserveResources(_AET_resourceStruct* configWord,
								AET_jobParams* jobParams)
{
	int32_t csr;
	AET_error status = AET_SOK;
	uint8_t aegsUsed;
	/*
	 Disable Interrupts so that no ISR can possibly claim the resource 
	 after we check for availability but before we reserve them.
	 */
	csr = _disable_interrupts();

	if ((status = _AET_available(*configWord)) != AET_SOK)
	{
		_restore_interrupts(csr);
		return status;
	}

	if (status = _AET_allocateAllAegs(jobParams, &aegsUsed))
	{
		return status;
	}

	configWord->AEG = aegsUsed;

	/* Reserve the Resources */
	_AET_globalParams.AET_usedResources.CNT |= configWord->CNT;
	_AET_globalParams.AET_usedResources.TB7 |= configWord->TB7;
	_AET_globalParams.AET_usedResources.TB3 |= configWord->TB3;
	_AET_globalParams.AET_usedResources.TB1 |= configWord->TB1;
	_AET_globalParams.AET_usedResources.DCMP |= configWord->DCMP;
	_AET_globalParams.AET_usedResources.ACMP |= configWord->ACMP;
	_AET_globalParams.AET_usedResources.AEGMUX |= configWord->AEGMUX;

					
	_restore_interrupts(csr);

	return status;

}

void _AET_reclaimResources(_AET_resourceStruct configWord)
{
	_AET_reclaimAegs(configWord.AEG);
	/* Clear the Resources */
	_AET_globalParams.AET_usedResources.CNT &= ~configWord.CNT;
	_AET_globalParams.AET_usedResources.TB7 &= ~configWord.TB7;
	_AET_globalParams.AET_usedResources.TB3 &= ~configWord.TB3;
	_AET_globalParams.AET_usedResources.TB1 &= ~configWord.TB1;
	_AET_globalParams.AET_usedResources.DCMP &= ~configWord.DCMP;
	_AET_globalParams.AET_usedResources.ACMP &= ~configWord.ACMP;
	_AET_globalParams.AET_usedResources.AEGMUX &= ~configWord.AEGMUX;
} 

void _AET_clearACMP(uint8_t numAcmp)
{
	uint16_t aetAcmpBaseIndex;

	/* Get the index of the specific ACMP */
	aetAcmpBaseIndex = (AET_indexof(CSL_Aet_iarRegs, CMP)) +
							(numAcmp * (sizeof(CSL_Aet_iarCmpRegs)>>2));


	/* DRC_CNTL */
	_AET_iregWrite((uint16_t)aetAcmpBaseIndex, 0x0);

	/* DRC_AREF */
	_AET_iregWrite((uint16_t)(aetAcmpBaseIndex + 1), 0x0);

	return;

}

void _AET_clearDCMP(uint8_t numDcmp)
{
	uint16_t aetDcmpBaseIndex;

	/* Get the index of the specific DCMP */
	aetDcmpBaseIndex = (AET_indexof(CSL_Aet_iarRegs, CMP)) +
							(numDcmp * (sizeof(CSL_Aet_iarCmpRegs)>>2));


	/* DCQ_DREFL*/
	_AET_iregWrite((uint16_t)(aetDcmpBaseIndex + 2), 0x0);

	/* DCQ_MREFL */
	_AET_iregWrite((uint16_t)(aetDcmpBaseIndex + 3), 0x0);

	/* DCQ_DREFH*/
	_AET_iregWrite((uint16_t)(aetDcmpBaseIndex + 4), 0x0);

	/* DCQ_MREFH */
	_AET_iregWrite((uint16_t)(aetDcmpBaseIndex + 5), 0x0);

	return;

}

void _AET_clearAEG(uint8_t numAeg)
{
	uint16_t aetAegBaseIndex;

	/* Get the index of the specific AEG */
	aetAegBaseIndex = (AET_indexof(CSL_Aet_iarRegs, AUX)) +
							(numAeg * (sizeof(CSL_Aet_iarAuxRegs)>>2));

	/*AUX_EVT_CNTL */
	_AET_iregWrite((uint16_t)aetAegBaseIndex, 0x0);

	/*AE_ENA*/
	_AET_iregWrite((uint16_t)(aetAegBaseIndex + 1), 0x0);
	
}

void _AET_clearTB7(uint8_t numTb7)
{

	uint16_t aetTb7BaseIndex;

	/* Disable this Trigger Builder */
	_AET_globalParams.globRegs.tbEnable &= ~(1 << (numTb7 + _AET_TB_ENA_7_WIDE_START));
	_AET_iregWrite((uint16_t)	AET_indexof(CSL_Aet_iarRegs, TB_ENA), 
					_AET_globalParams.globRegs.tbEnable
					);

	aetTb7BaseIndex = _AET_getTbBaseIndex(_AET_TB_7_WIDE, (uint8_t)numTb7);


	/* TB_ORS */
	_AET_iregWrite((uint16_t)aetTb7BaseIndex, 0x0);

	/* TB_CNTL_1 */
	_AET_iregWrite((uint16_t)(aetTb7BaseIndex + 1), 0x0);

	/* TB_CEXT_W_2 */
	_AET_iregWrite((uint16_t)(aetTb7BaseIndex + 2), 0x0);

	/* TB_CEXT_W_3 */
	_AET_iregWrite((uint16_t)(aetTb7BaseIndex + 3), 0x0);

	/* TB_CNTL_W_4 */
	_AET_iregWrite((uint16_t)(aetTb7BaseIndex + 4), 0x0);


}

void _AET_clearTB3(uint8_t numTb3)
{

	uint16_t aetTb3BaseIndex;

	/* Disable this Trigger Builder */
	_AET_globalParams.globRegs.tbEnable &= ~(1 << (numTb3 + _AET_TB_ENA_3_WIDE_START));
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, TB_ENA), 
					_AET_globalParams.globRegs.tbEnable
					);

	aetTb3BaseIndex = _AET_getTbBaseIndex(_AET_TB_3_WIDE, numTb3);

	/* SM_ORS */
	_AET_iregWrite((uint16_t)aetTb3BaseIndex, 0x0);

	/* SM_CNTL_1 */
	_AET_iregWrite((uint16_t)(aetTb3BaseIndex + 1), 0x0);

	/* SM_CEXT_W_2 */
	_AET_iregWrite((uint16_t)(aetTb3BaseIndex + 2), 0x0);


}

void _AET_clearTB1(uint8_t numTb1)
{

	uint16_t aetTb1BaseIndex;

	/* Disable this Trigger Builder */
	_AET_globalParams.globRegs.tbEnable &= ~(1 << (numTb1 + _AET_TB_ENA_1_WIDE_START));
	_AET_iregWrite((uint16_t)AET_indexof(CSL_Aet_iarRegs, TB_ENA), 
					_AET_globalParams.globRegs.tbEnable
					);

	aetTb1BaseIndex = _AET_getTbBaseIndex(_AET_TB_1_WIDE, numTb1);						

	/* TB_ORS */
	_AET_iregWrite((uint16_t)aetTb1BaseIndex, 0x0);

	/* STB_CNTL_1 */
	_AET_iregWrite((uint16_t)(aetTb1BaseIndex + 1), 0x0);


}
/*******************************************************************************
*
* _AET_getEventNumber()
*
*	inputs:
*		Miscellaneous Event number.  This number was passed by the user.  If 
*		the number is not one of the system events, we return the 
*		miscellaneous event number.  If it is a system signal, we return the 
*		event number of the signal to which it was allocated
*
*		Pointer to an 8-bit unsigned int.  This value contained at this 
*		address is set to the number of the signal allocated
*
*	returns: AET_error status message
*		
*******************************************************************************/
AET_error _AET_getEventNumber(uint16_t miscEvtNo,
							  uint8_t* signalNo)
{
	AET_error status = AET_SOK;
	int32_t temp1, temp2;

	if ((miscEvtNo >= AET_GEM_MISC_EVT_START) &&
		(miscEvtNo <= AET_GEM_MISC_EVT_END))
	{  /* This is not a system event, just a regular miscellaneous event */
		temp1 = AET_GEM_MISC_EVT_START;
		temp2 = miscEvtNo - temp1;
		*signalNo = (uint8_t)temp2;
	}
	else
	{	/* This is a system event.  Need to dynamically allocate signal */
		/* Allocate a system signal and program the register*/
		
		if ((status = _AET_systemSignalAlloc(signalNo))!= AET_SOK)
			return status;
		
		/* If we're ok to this point, program the register */
		if ((status = _AET_programAegMux(*signalNo, miscEvtNo))!=AET_SOK)
			return status;
	}

	return status;
}	
/*******************************************************************************
*
* _AET_systemSignalAlloc()
*
*	inputs: Pointer to an 8-bit unsigned int. The value contained at this 
*			address is set to the number of the signal allocated
*
*	returns: AET_error status message.
*		AET_SOK - Signal was allocated properly
*		AET_FNOTAVAILAEGMUX - No AEGMUX signals are available
*
*******************************************************************************/
AET_error _AET_systemSignalAlloc(uint8_t* signalNo)
{
	uint8_t counter;

	for (counter = 0; counter < AET_NUM_AEGMUXLINE; counter++)
	{
		if ((_AET_globalParams.AET_usedResources.AEGMUX & (1 << counter))==0)
		{
			_AET_globalParams.AET_usedResources.AEGMUX |= (1 << counter);
			*signalNo = counter + 9;
			return AET_SOK;
		}
	}
	return AET_FNOTAVAILAEGMUX;	

}
/******************************************************************************/

/*******************************************************************************
*
* _AEToaegAlloc()
*
*	inputs: Pointer to an 8-bit unsigned int. The value contained at this 
*			address is set to the number of the OAEG allocated
*
*	returns: AET_error status message.
*		AET_SOK - Signal was allocated properly
*		AET_FNOTAVAILAEG - No AEG is available
*       AET_FINVALIDSIGVAL - Signal value is invalid.  eg. If a miscellaneous 
*		event job is already programmed and a memory event job is requested,
*		this value should be returned.
*
*******************************************************************************/
AET_error _AET_oaegAlloc(uint8_t* aegNo)
{
	AET_error status = AET_SOK;
	uint8_t aegCounter;

	for (aegCounter = 0; aegCounter < AET_NUM_AEG; aegCounter++)
	{
		if (!((_AET_globalParams.AET_usedResources.AEG) & (1 << aegCounter)))
		{
			*aegNo = aegCounter;
			return status;
		}
	}
	status = AET_FNOTAVAILAEG;
	return status;
}


