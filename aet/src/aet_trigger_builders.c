/*
 * aet_trigger_builders.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ======================aet_trigger_builders.c================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2012-10-16 11:50:41 -0400 (Thu, 02 Jun 2011) $
 *   Revision: $LastChangedRevision: 10572 $
 *   Last Update By: $Author: KarthikRamanaSankar $
 *
 *   Comments: updated for adding trigger builders in any order (release 4.11)
 *
 *
 *  ================================= OVERVIEW =================================
 *  This file is responsible for functions that implement any type of resource
 *  management dealing with the allocation of trigger builders.
 *  ============================================================================
 */

 
/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_trigger_builders.h>
#include <stdio.h>
extern _AET_globalVars _AET_globalParams;


// Create Global Settings Database
TB_SETTING_DATABASE _tb_settings_db;

const _AET_triggerBuilderInfo AET_triggerBuilderDatabase[] = 
{
	/*
	 * Trigger Type | Trigger Builder Type | Trigger Builder Number |  Output Control | Trigger Number | Alloc Order
	 */

	/* CPU Halt Triggers */
	{AET_TRIG_HALT_CPU, _AET_TB_1_WIDE, 0, 0, 1, 0},				/* 0 */
	{AET_TRIG_HALT_CPU, _AET_TB_1_WIDE, 2, 2, 1, 1},
	{AET_TRIG_HALT_CPU, _AET_TB_1_WIDE, 1, 2, 1, 2},
	{AET_TRIG_HALT_CPU, _AET_TB_1_WIDE, 4, 2, 1, 3},
	{AET_TRIG_HALT_CPU, _AET_TB_1_WIDE, 5, 3, 1, 4},
	{AET_TRIG_HALT_CPU, _AET_TB_3_WIDE, 4, 2, 1, 5},				/* 5 */
	{AET_TRIG_HALT_CPU, _AET_TB_3_WIDE, 5, 2, 1, 6},

	/* AINT (Interrupt) Triggers */
	{AET_TRIG_AINT, _AET_TB_1_WIDE, 0, 1, 1, 0},
	{AET_TRIG_AINT, _AET_TB_1_WIDE, 1, 3, 1, 1},
	{AET_TRIG_AINT, _AET_TB_1_WIDE, 2, 3, 1, 2},
	{AET_TRIG_AINT, _AET_TB_1_WIDE, 3, 3, 1, 3},					/* 10 */
	{AET_TRIG_AINT, _AET_TB_1_WIDE, 4, 3, 1, 4},

	/* Trace Stop Triggers */
	{AET_TRIG_TRACE_PCSTOP, _AET_TB_3_WIDE, 0, 0, 1, 0}, 			/* Stop PC Trace */
	{AET_TRIG_TRACE_PCSTOP, _AET_TB_3_WIDE, 1, 1, 1, 1},
	{AET_TRIG_TRACE_PCSTOP, _AET_TB_3_WIDE, 2, 3, 1, 2},
	{AET_TRIG_TRACE_PCSTOP, _AET_TB_3_WIDE, 4, 3, 1, 3},			/* 15 */

	{AET_TRIG_TRACE_TIMINGSTOP, _AET_TB_3_WIDE, 0, 0, 2, 0}, 		/* Stop Timing Trace */
	{AET_TRIG_TRACE_TIMINGSTOP, _AET_TB_3_WIDE, 1, 1, 2, 1},
	{AET_TRIG_TRACE_TIMINGSTOP, _AET_TB_3_WIDE, 2, 3, 2, 2},
	{AET_TRIG_TRACE_TIMINGSTOP, _AET_TB_3_WIDE, 4, 3, 2, 3},

	{AET_TRIG_TRACE_DATASTOP, _AET_TB_3_WIDE, 0, 0, 3, 0}, 			/* Stop Data Trace */
	{AET_TRIG_TRACE_DATASTOP, _AET_TB_3_WIDE, 1, 1, 3, 1},			/* 21 */
	{AET_TRIG_TRACE_DATASTOP, _AET_TB_3_WIDE, 2, 3, 3, 2},
	{AET_TRIG_TRACE_DATASTOP, _AET_TB_3_WIDE, 4, 3, 3, 3},

	/* Trace Suspend Triggers */
	{AET_TRIG_TRACE_PCSUSPEND, _AET_TB_3_WIDE, 0, 1, 1, 0},     	/* Suspend PC Trace */
	{AET_TRIG_TRACE_PCSUSPEND, _AET_TB_3_WIDE, 1, 0, 1, 1},			/* 25 */
	{AET_TRIG_TRACE_PCSUSPEND, _AET_TB_3_WIDE, 3, 3, 1, 2},
	{AET_TRIG_TRACE_PCSUSPEND, _AET_TB_3_WIDE, 5, 3, 1, 3},

	{AET_TRIG_TRACE_TIMINGSUSPEND, _AET_TB_3_WIDE, 0, 1, 2, 0}, 	/* Suspend Timing Trace */
	{AET_TRIG_TRACE_TIMINGSUSPEND, _AET_TB_3_WIDE, 1, 0, 2, 1},
	{AET_TRIG_TRACE_TIMINGSUSPEND, _AET_TB_3_WIDE, 3, 3, 2, 2},		/* 30 */
	{AET_TRIG_TRACE_TIMINGSUSPEND, _AET_TB_3_WIDE, 5, 3, 2, 3},

	{AET_TRIG_TRACE_DATASUSPEND, _AET_TB_3_WIDE, 0, 1, 3, 0},   	/* Suspend Data Trace */
	{AET_TRIG_TRACE_DATASUSPEND, _AET_TB_3_WIDE, 1, 0, 3, 1},
	{AET_TRIG_TRACE_DATASUSPEND, _AET_TB_3_WIDE, 3, 3, 3, 2},
	{AET_TRIG_TRACE_DATASUSPEND, _AET_TB_3_WIDE, 5, 3, 3, 3},		/* 35 */


	/* Watermark Counter Triggers */
	{AET_TRIG_WM0START, _AET_TB_3_WIDE, 0, 3, 1, 0},
	{AET_TRIG_WM1START, _AET_TB_3_WIDE, 0, 3, 3, 0},
	{AET_TRIG_WM0STOP, _AET_TB_3_WIDE, 1, 2, 1, 0},
	{AET_TRIG_WM1STOP, _AET_TB_3_WIDE, 1, 2, 2, 0},

	/* Standard Counter Triggers */
	{AET_TRIG_CNT0_START, _AET_TB_1_WIDE, 1, 0, 1, 0},				/* 40 */
	{AET_TRIG_CNT0_STOP, _AET_TB_1_WIDE, 2, 0, 1, 0},
	{AET_TRIG_CNT1_START, _AET_TB_1_WIDE, 3, 0, 1, 0},
	{AET_TRIG_CNT1_STOP, _AET_TB_1_WIDE, 4, 0, 1, 0},

	/* External Triggers */
	{AET_TRIG_ET0, _AET_TB_7_WIDE, 0, 1, 1, 0},
	{AET_TRIG_ET0, _AET_TB_3_WIDE, 0, 2, 1, 1},						/* 45 */
	{AET_TRIG_ET1, _AET_TB_7_WIDE, 0, 1, 2, 0},
	{AET_TRIG_ET1, _AET_TB_3_WIDE, 0, 2, 2, 1},

	/* Trace Store Triggers */
	{AET_TRIG_TRACE_STORE_TIMING,_AET_TB_7_WIDE, 0, 0, 1, 0},
	{AET_TRIG_TRACE_STORE_TIMING, _AET_TB_7_WIDE, 1, 1, 2, 0},

	{AET_TRIG_TRACE_STORE_PC, _AET_TB_7_WIDE, 0, 0, 2, 0},			/* 50 */

	{AET_TRIG_TRACE_STORE_RA, _AET_TB_7_WIDE, 0, 0, 3, 0},
	{AET_TRIG_TRACE_STORE_RA, _AET_TB_7_WIDE, 1, 1, 3, 0},

	{AET_TRIG_TRACE_STORE_WA, _AET_TB_7_WIDE, 0, 0, 4, 0},
	{AET_TRIG_TRACE_STORE_WA, _AET_TB_7_WIDE, 1, 1, 4, 0},

	{AET_TRIG_TRACE_STORE_RD, _AET_TB_7_WIDE, 0, 0, 5, 0},			/* 55 */
	{AET_TRIG_TRACE_STORE_RD, _AET_TB_7_WIDE, 1, 1, 5, 0},

	{AET_TRIG_TRACE_STORE_WD, _AET_TB_7_WIDE, 0, 0, 6, 0},
	{AET_TRIG_TRACE_STORE_WD, _AET_TB_7_WIDE, 1, 1, 6, 0},

	{AET_TRIG_TRACE_STORE_PCTAG, _AET_TB_7_WIDE, 0, 0, 7, 0},
	{AET_TRIG_TRACE_STORE_PCTAG, _AET_TB_7_WIDE, 1, 1, 7, 0},		/* 60 */


  /* Trace Start Triggers */
	{AET_TRIG_TRACE_START_TIMING, _AET_TB_7_WIDE, 1, 0, 1, 0},
	{AET_TRIG_TRACE_START_PC, _AET_TB_7_WIDE, 1, 0, 2, 0},
	{AET_TRIG_TRACE_START_RA, _AET_TB_7_WIDE, 1, 0, 3, 0},
	{AET_TRIG_TRACE_START_WA, _AET_TB_7_WIDE, 1, 0, 4, 0},
	{AET_TRIG_TRACE_START_RD, _AET_TB_7_WIDE, 1, 0, 5, 0},			/* 65 */
	{AET_TRIG_TRACE_START_WD, _AET_TB_7_WIDE, 1, 0, 6, 0},
	{AET_TRIG_TRACE_START_PCTAG, _AET_TB_7_WIDE, 1, 0, 7, 0},

	/* Trace Trigger */
	{AET_TRIG_TRACE_TRIGGER, _AET_TB_1_WIDE, 0, 2, 1, 0},
	{AET_TRIG_TRACE_TRIGGER, _AET_TB_1_WIDE, 5, 0, 1, 1},


	/* Trace End */
	{AET_TRIG_TRACE_END, _AET_TB_1_WIDE, 0, 3, 1, 0},				/* 70 */
	{AET_TRIG_TRACE_END, _AET_TB_1_WIDE, 2, 1, 1, 1},
	{AET_TRIG_TRACE_END, _AET_TB_1_WIDE, 3, 2, 1, 2},
	{AET_TRIG_TRACE_END, _AET_TB_1_WIDE, 4, 1, 1, 3},
	{AET_TRIG_TRACE_END, _AET_TB_1_WIDE, 5, 1, 1, 4},


	/* State Transition Triggers */
	{AET_TRIG_STATE_0_TO_1, _AET_TB_3_WIDE, 2, 0, 1, 0}, 			/* 75 - _AET_STATE_0_TO_1 */
	{AET_TRIG_STATE_0_TO_2, _AET_TB_3_WIDE, 2, 0, 2, 0}, 			/* _AET_STATE_0_TO_2 */
	{AET_TRIG_STATE_0_TO_3, _AET_TB_3_WIDE, 2, 0, 3, 0}, 			/* _AET_STATE_0_TO_3 */

	{AET_TRIG_STATE_1_TO_0, _AET_TB_3_WIDE, 3, 0, 1, 0}, 			/* _AET_STATE_1_TO_0 */
	{AET_TRIG_STATE_1_TO_2, _AET_TB_3_WIDE, 3, 0, 2, 0}, 			/* _AET_STATE_1_TO_2 */
	{AET_TRIG_STATE_1_TO_3, _AET_TB_3_WIDE, 3, 0, 3, 0}, 			/* 80 - _AET_STATE_1_TO_3 */

	{AET_TRIG_STATE_2_TO_0, _AET_TB_3_WIDE, 4, 0, 1, 0}, 			/* _AET_STATE_2_TO_0 */
	{AET_TRIG_STATE_2_TO_1, _AET_TB_3_WIDE, 4, 0, 2, 0}, 			/* _AET_STATE_2_TO_1 */
	{AET_TRIG_STATE_2_TO_3, _AET_TB_3_WIDE, 4, 0, 3, 0},			/* _AET_STATE_2_TO_3 */

	{AET_TRIG_STATE_3_TO_0, _AET_TB_3_WIDE, 5, 0, 1, 0}, 			/* _AET_STATE_3_TO_0 */
	{AET_TRIG_STATE_3_TO_1, _AET_TB_3_WIDE, 5, 0, 2, 0}, 			/* 85 - _AET_STATE_3_TO_1 */
	{AET_TRIG_STATE_3_TO_2, _AET_TB_3_WIDE, 5, 0, 3, 0}, 			/* _AET_STATE_3_TO_2 */
};




/******************************************************************************
* _AET_pgmTrigBldr
*
* TODO: Update _AET_pgmTrigBldr documentation
*
* Parameters:
*   *tbParams -
*   	- tbBaseIndex -> Base Index of the Trigger Builder
*   	- tbOrsValue -> Value to be programmed into the TB_ORS register
*   	- tbType -> The type of trigger builder to be used
*   	- outputControl -> The output control of the triggers
*   	- boolOutput -> Logic orientation
*	*jobParams -
*		- stateQual -> Specifies if the job is to be qualified by a State Machine
*						state
*
*
*
*
* Returns:
*	AET_error code
*
*******************************************************************************/
AET_error _AET_pgmTrigBldr(
					_AET_tbCntlParams* tbParams,
					AET_jobParams* jobParams
					)
{
	AET_error status = AET_SOK;
	uint8_t numCexp;
	uint8_t cexpMaskValue;
	uint8_t cexpCounter;

	struct trig_bldr_regs{
		uint32_t tbOrs;
		uint32_t tbCntl;
		uint32_t cexp[3];
		uint8_t tbsUsed;
	}myTbRegs = {0, 0, {0, 0, 0}, 0};


	/* Program the TB ORS Register  and store the programmed value into the
	 * configWord
	 */

	myTbRegs.tbOrs = tbParams->tbOrsValue;

#if 0
	/* If watermark job, then two outputs must be enabled */
	if (tbParams->tbType == _AET_TB_3_WIDE)
	{
		if ((tbParams->tbBaseIndex == AET_indexof(CSL_Aet_iarRegs, SM))
			&& (tbParams->outputCntl == 3))
	    	{
	          tbParams->triggers = 5;
	        }

	    if ((tbParams->tbBaseIndex == (AET_indexof(CSL_Aet_iarRegs, SM)+(sizeof(CSL_Aet_iarSmRegs)>>2)))
	    	&& (tbParams->outputCntl == 2))
	        {
	          tbParams->triggers = 3;
	        }
	}
# endif

	/* Get the number of triggers */
	switch (tbParams->tbType)
	{
		case _AET_TB_7_WIDE:
			numCexp = 3;
			cexpMaskValue = 0x7F;
			break;
		case _AET_TB_3_WIDE:
			numCexp = 1;
			cexpMaskValue = 0x7;
			break;
		case _AET_TB_1_WIDE:
			numCexp = 0;
			cexpMaskValue = 0x1;
			break;
		default:
			return AET_FINVALIDTRIGBUILDER;
	}

	/* Check to see if we need to qualify this job by a state.  If it is,
	   make the necessary changes in the boolean output and the event D
	   select
	*/
	if (jobParams->stateQual != AET_STATEQUAL_NONE)
	{
		int8_t tbNo = -1;

		tbParams->boolOutput &= _AET_STATE_MACHINE_QUAL_MASK;
		/* Program the State Extension registers */

		/* Select the trigger builder being used */
		tbNo = tbParams->tbNo;

		/*
		 * Here, we are just using the State Extension registers to qualify the
		 * trigger builders for state.  I believe that this has been deprecated
		 * in favor of embedding the logic into term D.  It's hard to find
		 * documentation that explains the SEXT implementation correctly.  We
		 * can leave this alone for now, but may want to revisit.  This might be
		 * a candidate for elimination.
		 */

		if (tbNo >= 0)
		{
			if (tbParams->tbType == _AET_TB_1_WIDE)
			{
				_AET_iregWrite(0xB4, (~jobParams->stateQual & 0x1) << (4 * tbNo));
			}


			if (tbParams->tbType == _AET_TB_7_WIDE)
			{
				_AET_iregWrite(0xB5, (~jobParams->stateQual & 0x1) << (4 * tbNo));
			}
		}

	}

	/* Check to see if we need to program trigger 1 */
	if ((tbParams->triggers & 0x1) == 1)
	{
		/* Program the TB_CNTL register with the LUT vale */
		myTbRegs.tbCntl = AET_FMK(AET_IAR_TB_CNTL_OC, tbParams->outputCntl)|
				AET_FMK(AET_IAR_TB_CNTL_ESD, jobParams->stateQual )|
				AET_FMK(AET_IAR_TB_CNTL_ESC, tbParams->evtCSelect)|
				AET_FMK(AET_IAR_TB_CNTL_ST_DIS, 0x0)|
				AET_FMK(AET_IAR_TB_CNTL_BP_OP, (tbParams->boolOutput & 0xFFFF));
	}
	else
	{
		/* Program the TB_CNTL register with the 0x0 as the LUT value */
		myTbRegs.tbCntl = AET_FMK(AET_IAR_TB_CNTL_OC, tbParams->outputCntl)|
				AET_FMK(AET_IAR_TB_CNTL_ESD, jobParams->stateQual)|
				AET_FMK(AET_IAR_TB_CNTL_ESC, tbParams->evtCSelect)|
				AET_FMK(AET_IAR_TB_CNTL_ST_DIS, 0x0)|
				AET_FMK(AET_IAR_TB_CNTL_BP_OP, 0x0);

	}

	for (cexpCounter = 1; cexpCounter <= numCexp; cexpCounter++)
	{

		int32_t cexpValue;
		uint16_t cexpShift = (uint16_t)((2 * cexpCounter) - 1);
		uint16_t cexpMask = (uint16_t)((0x3 << cexpShift) & cexpMaskValue);

		switch((cexpMask & tbParams->triggers) >> cexpShift)
			{
				case 0x0: /* Neither trigger is used */
					cexpValue = 0;
					break;
				case 0x1:
					cexpValue = tbParams->boolOutput;
					break;
				case 0x2:
					cexpValue = (tbParams->boolOutput << 16) & 0xFFFF0000;
					break;
				case 0x3:
					cexpValue = ((tbParams->boolOutput << 16) | tbParams->boolOutput) & 0xFFFFFFFF;
					break;
				default:
					return AET_FINVALIDTRIGMASK;
			}

		/* Program the CEXT Register */
		myTbRegs.cexp[cexpCounter-1] = cexpValue;


	}

	// Program the registers
	_AET_iregWrite(tbParams->tbBaseIndex, myTbRegs.tbOrs);
	_AET_iregWrite((uint16_t)(tbParams->tbBaseIndex + 1), myTbRegs.tbCntl);
	for (cexpCounter = 0; cexpCounter < numCexp; cexpCounter++){
		_AET_iregWrite ((uint16_t)(tbParams->tbBaseIndex + 2 + cexpCounter), myTbRegs.cexp[cexpCounter]);
	}

	/*
	 * Store the results in the database
	 */
	switch (tbParams->tbType)
	{

		case _AET_TB_7_WIDE:
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].common.tbOrsValue = myTbRegs.tbOrs;
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].common.tbCntlValue = myTbRegs.tbCntl;
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[0] = (uint16_t)(myTbRegs.tbCntl & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[1] = (uint16_t)(myTbRegs.cexp[0] & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[2] = (uint16_t) ((myTbRegs.cexp[0] >> 16) & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[3] = (uint16_t)(myTbRegs.cexp[1] & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[4] = (uint16_t) ((myTbRegs.cexp[1] >> 16) & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[5] = (uint16_t)(myTbRegs.cexp[2] & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].lutValue[6] = (uint16_t) ((myTbRegs.cexp[2] >> 16) & 0xFFFF);
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].common.trigsUsed |= tbParams->triggers;
			_tb_settings_db.tb_7_wide_settings[tbParams->tbNo].common.initLutValue = tbParams->boolOutput;
			break;
		case _AET_TB_3_WIDE:
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].common.tbOrsValue = myTbRegs.tbOrs;
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].common.tbCntlValue = myTbRegs.tbCntl;
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].lutValue[0] = (uint16_t)(myTbRegs.tbCntl & 0xFFFF);
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].lutValue[1] = (uint16_t)(myTbRegs.cexp[0] & 0xFFFF);
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].lutValue[2] = (uint16_t) ((myTbRegs.cexp[0] >> 16) & 0xFFFF);
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].common.trigsUsed |= tbParams->triggers;
			_tb_settings_db.tb_3_wide_settings[tbParams->tbNo].common.initLutValue = tbParams->boolOutput;
			break;
		case _AET_TB_1_WIDE:
			_tb_settings_db.tb_1_wide_settings[tbParams->tbNo].common.tbOrsValue = myTbRegs.tbOrs;
			_tb_settings_db.tb_1_wide_settings[tbParams->tbNo].common.tbCntlValue = myTbRegs.tbCntl;
			_tb_settings_db.tb_1_wide_settings[tbParams->tbNo].lutValue[0] = myTbRegs.tbCntl;
			_tb_settings_db.tb_1_wide_settings[tbParams->tbNo].common.trigsUsed |= tbParams->triggers;
			_tb_settings_db.tb_1_wide_settings[tbParams->tbNo].common.initLutValue = tbParams->boolOutput;
			break;
		default:
			return AET_FINVALIDTRIGBUILDER;
	}

	return status;
}// _AET_pgmTrigBldr

uint16_t _AET_getTbBaseIndex(_AET_triggerBuilderType tbType,
							uint8_t triggerBuilderNo
							)
{
	uint16_t aetTbBaseIndex;

	switch(tbType)
	{
		case _AET_TB_1_WIDE:
			aetTbBaseIndex = (AET_indexof(CSL_Aet_iarRegs, TB_CNTL_GRP_1));
			aetTbBaseIndex += triggerBuilderNo
								* (sizeof(CSL_Aet_iarTb_cntl_grp_1Regs)>>2);
			break;
		case _AET_TB_3_WIDE:
			if (triggerBuilderNo > 1)
				aetTbBaseIndex = 0x9E + triggerBuilderNo * 3;
			else
				aetTbBaseIndex = 0xD0 + triggerBuilderNo * 8;
			break;
		case _AET_TB_7_WIDE:
			aetTbBaseIndex = (AET_indexof(CSL_Aet_iarRegs, TB_CNTL_GRP_W));
			aetTbBaseIndex += triggerBuilderNo
								* (sizeof(CSL_Aet_iarTb_cntl_grp_wRegs)>>2);
			break;
		default:
			aetTbBaseIndex = (AET_indexof(CSL_Aet_iarRegs, TB_CNTL_GRP_1));
			aetTbBaseIndex += triggerBuilderNo
								* (sizeof(CSL_Aet_iarTb_cntl_grp_1Regs)>>2);
	}

	return aetTbBaseIndex;
} // _AET_getTbBaseIndex

/*******************************************************************************
* _AET_getNumCexp
*   Parameters:
*	_AET_triggerBuilderType - Type of trigger builder
*
*	returns: Number of CEXP registers for that type of trigger builder
*
*******************************************************************************/
uint8_t _AET_getNumCexp(_AET_triggerBuilderType tbType)
{
	uint8_t numCexp;

	switch (tbType)
	{
		case _AET_TB_7_WIDE:
			numCexp = (uint8_t)3;
			break;
		case _AET_TB_3_WIDE:
			numCexp = (uint8_t)1;
			break;
		default:
			numCexp = (uint8_t)0;
	}

	return numCexp;
} //_AET_genNumCexp
/******************************************************************************/

/*******************************************************************************
* _AET__markTriggerBuilderForEnable
*   Parameters:
*	uint8_t -						The number of the trigger builder to enable
*	_AET_triggerBuilderType -	The type of trigger builder (1-wide, 3-wide,
*								7-wide
*
*	returns: AET_error status message
*
*******************************************************************************/
AET_error _AET_markTrigBldrForEnable(const _AET_triggerBuilderInfo *trigBldr)
{
	AET_error status = AET_SOK;

	 switch(trigBldr->tbType)
	 {
		case _AET_TB_1_WIDE:
				_AET_globalParams.globRegs.tbEnable |=
					(1 << (trigBldr->triggerBuilderNo + (uint8_t)_AET_TB_ENA_1_WIDE_START));
				break;
		case _AET_TB_3_WIDE:
				_AET_globalParams.globRegs.tbEnable |=
					(1 << (trigBldr->triggerBuilderNo + (uint8_t)_AET_TB_ENA_3_WIDE_START));
				break;
		case _AET_TB_7_WIDE:
				_AET_globalParams.globRegs.tbEnable |=
					(1 << (trigBldr->triggerBuilderNo + (uint8_t)_AET_TB_ENA_7_WIDE_START));
				break;
		default:
			status = AET_FINVALIDTRIGBUILDER;
	 }

	return status;
}//_AET__markTriggerBuilderForEnable
/******************************************************************************/

/*******************************************************************************
 * _AET_enableTriggerBuilders
 *******************************************************************************/
AET_error enableTriggerBuilders()
{
	/* Enable the Trigger Builders */
	_AET_iregWrite((uint16_t)(AET_indexof(CSL_Aet_iarRegs, TB_ENA)),
					_AET_globalParams.globRegs.tbEnable
					);

	return AET_SOK;
}//_AET_enableTriggerBuilders


/*******************************************************************************
 * _AET_markEnableTb
 *******************************************************************************/
AET_error _AET_markEnableTb(const _AET_triggerBuilderInfo *trigBldr)
{
	AET_error status = AET_SOK;

	if ((status = _AET_markTrigBldrForEnable(trigBldr))!= AET_SOK)
	{
		return status;
	}

	status = enableTriggerBuilders();

	return status;

}//_AET_markEnableTb
/******************************************************************************/

/*******************************************************************************
 * _AET_initTbSettingsDB
 *******************************************************************************/
void _AET_initTbSettingsDB(void){

	uint8_t count1, count2;

	for (count1 =0; count1 <6; count1++){
		_tb_settings_db.tb_1_wide_settings[count1].common.tbOrsValue = 0;
		_tb_settings_db.tb_1_wide_settings[count1].common.tbCntlValue = 0;
		_tb_settings_db.tb_1_wide_settings[count1].common.initLutValue = 0;
		_tb_settings_db.tb_1_wide_settings[count1].common.trigsUsed = 0;
		for (count2 = 0; count2 < 1; count2++){
			_tb_settings_db.tb_1_wide_settings[count1].lutValue[count2] = 0;
		}
	}

	for (count1 =0; count1 <6; count1++){
		_tb_settings_db.tb_3_wide_settings[count1].common.tbOrsValue = 0;
		_tb_settings_db.tb_3_wide_settings[count1].common.tbCntlValue = 0;
		_tb_settings_db.tb_3_wide_settings[count1].common.initLutValue = 0;
		_tb_settings_db.tb_3_wide_settings[count1].common.trigsUsed = 0;
		for (count2 = 0; count2 < 3; count2++){
			_tb_settings_db.tb_3_wide_settings[count1].lutValue[count2] = 0;
		}
	}

	for (count1 =0; count1 <2; count1++){
		_tb_settings_db.tb_7_wide_settings[count1].common.tbOrsValue = 0;
		_tb_settings_db.tb_7_wide_settings[count1].common.tbCntlValue = 0;
		_tb_settings_db.tb_7_wide_settings[count1].common.initLutValue = 0;
		_tb_settings_db.tb_7_wide_settings[count1].common.trigsUsed = 0;
		for (count2 = 0; count2 < 7; count2++){
			_tb_settings_db.tb_7_wide_settings[count1].lutValue[count2] = 0;
		}
	}

	return;
}//_AET_initTbSettingsDB
/******************************************************************************/

/*******************************************************************************
 _AET_getTriggerBuilder
	trigType - specifies what type of trigger to be generated
	**tbPtr - Reference to a pointer into the database where the information
	          about the triggerbuilder can be found.  The function will fill
			  this with the appropriate address and return it.
 ******************************************************************************/

AET_error _AET_getTriggerBuilder(AET_triggerType trigType,
					const _AET_triggerBuilderInfo* *tbPtr
					)
{

	uint8_t counter;
	uint8_t numElements =sizeof(AET_triggerBuilderDatabase)/sizeof(_AET_triggerBuilderInfo);


	for (counter = 0; counter < numElements; counter++){
		if (AET_triggerBuilderDatabase[counter].trigType != trigType){
			continue;
		}
		*tbPtr = &AET_triggerBuilderDatabase[counter];
		switch(AET_triggerBuilderDatabase[counter].tbType){
		case _AET_TB_1_WIDE:
			if (((_AET_globalParams.AET_usedResources.TB1) & (1 << AET_triggerBuilderDatabase[counter].triggerBuilderNo))==0){
				return AET_SOK;
			}
			break;
		case _AET_TB_3_WIDE:
			if (((_AET_globalParams.AET_usedResources.TB3) & (1 << AET_triggerBuilderDatabase[counter].triggerBuilderNo))==0){
				return AET_SOK;
			}
			break;
		case _AET_TB_7_WIDE:
			if (((_AET_globalParams.AET_usedResources.TB7) & (1 << AET_triggerBuilderDatabase[counter].triggerBuilderNo))==0){
				return AET_SOK;
			}
			break;
		default:
			return AET_FINVALIDTRIGBUILDER; /* The Trigger Builder type is not valid */

		}
	}
	return AET_FNOTAVAIL;	/* There is no trigger builder available to generate the desired trigger */

}//_AET_getTriggerBuilder

/*******************************************************************************
 * _AET_setTrigBldrBits
 ******************************************************************************/
AET_error _AET_setTrigBldrBits(_AET_resourceStruct* configWord, 
						  _AET_triggerBuilderType tbType,
						  uint8_t triggerBuilderNo
						  )
{
	switch (tbType)
	{
		case _AET_TB_1_WIDE:
			configWord->TB1 |= (1 << triggerBuilderNo);
			break;
		case _AET_TB_3_WIDE:
			configWord->TB3 |= (1 << triggerBuilderNo);
			break;
		case _AET_TB_7_WIDE:
			configWord->TB7 |= (1 << triggerBuilderNo);
			break;
		default:
			return AET_FINVALIDTRIGBUILDER;
	}/* End Switch */

	configWord->initTbType = tbType;
	configWord->initTbNumber = triggerBuilderNo;

	return AET_SOK;

}//_AET_setTrigBldrBits

/*******************************************************************************
 * _AET_checkForTriggerOnJobTbs
 ******************************************************************************/
_AET_BOOL _AET_checkForTriggerOnJobTbs(AET_triggerType triggerType,
							_AET_resourceStruct* resourcePtr,
							_AET_triggerBuilderInfo* tbInfo
							){
	uint8_t usedMask;
	uint8_t count;

	if (resourcePtr->TB7 != 0){
		usedMask = resourcePtr->TB7;
		count = 0;
		while (usedMask != 0){
			if (usedMask & 1){
				if (_AET_checkForTriggerOnTb(triggerType, _AET_TB_7_WIDE, count, tbInfo) == AET_TRUE){
					return AET_TRUE;
				}
			}
			count++;
			usedMask = usedMask >> 1;
		}
	}

	if (resourcePtr->TB3 != 0){
		usedMask = resourcePtr->TB3;
		count = 0;
		while (usedMask != 0){
			if (usedMask & 1){
				if (_AET_checkForTriggerOnTb(triggerType, _AET_TB_3_WIDE, count, tbInfo) == AET_TRUE){
					return AET_TRUE;
				}
			}
			count++;
			usedMask = usedMask >> 1;
		}

	}

	if (resourcePtr->TB1 != 0){
		tbInfo->tbType = _AET_TB_1_WIDE;
		tbInfo->triggerBuilderNo = resourcePtr->initTbNumber;
		return AET_FALSE;
	}
	return AET_FALSE;
}//_AET_checkForTriggerOnJobTbs

/*******************************************************************************
 * _AET_checkForTriggerOnTb
 ******************************************************************************/
_AET_BOOL _AET_checkForTriggerOnTb(AET_triggerType trigType,
								_AET_triggerBuilderType tbType,
								uint8_t tbNo,
								_AET_triggerBuilderInfo* tbInfo
								){

	_AET_triggerBuilderInfo* tbPtr = (_AET_triggerBuilderInfo*)&AET_triggerBuilderDatabase;
	uint8_t counter;

	/*
		 * At this point, we have the Trigger Builder Number, Type, and Trigger
		 * Check to see if they are all in the same location.  Also check to
		 * be sure that that trigger is available.
		 */

		/*
		 * Loop through the table of triggers, checking to find a match
		 */
	for (counter = 0; counter < _AET_NUM_TRIGGERS; counter++){
		if (tbPtr->trigType == trigType)
		{
			/*
			 * Match found, is it the correct tbType?
			 */
			if (tbPtr->tbType != tbType){
				tbPtr++;
				continue;
			}

			/*
			 * Correct TB Type found, it is the correct tb Number?
			 */
			if (tbPtr->triggerBuilderNo != tbNo){
				tbPtr++;
				continue;
			}


			tbInfo->tbType = tbPtr->tbType;
			tbInfo->triggerBuilderNo = tbPtr->triggerBuilderNo;
			tbInfo->triggerNo = tbPtr->triggerNo;
			tbInfo->outputCntl = tbPtr->outputCntl;
			return AET_TRUE;
		}
		tbPtr++;
	}
	return AET_FALSE;
}//_AET_checkForTriggerOnTb
/* ****************************************************************************/



