/*
 * aet_data_watchpoint_range.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =======================aet_data_watchpoint_range.c=========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*

0        1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
 
*/

#include <aet_data_watchpoint_range.h>
#include <c6x.h>

extern _AET_globalVars _AET_globalParams;
extern _AET_drcCntlParams AET_DRCCNTLPARAMS;
extern _AET_tbCntlParams AET_TBCNTLPARAMS;

AET_error _AET_dataWatchpointRange(AET_jobParams* params, _AET_drcDataQualify doQual)
{
	AET_dataWatchpointRangeResources resources;
	int32_t tempAddress;
	AET_error status = AET_SOK;


	if ((status = _AET_jobInit(&resources.configWord,
								params->triggerType,
								&resources.trigBldr 
								))
								!= AET_SOK)
	{
		return status;
	}

	/* Select the comparators that the job will use
	*/
	if ((status = _AET_getDataAcmpQuad(&resources.acmpNo[0],
									   &resources.configWord
									   )) != AET_SOK)
		return status;


	/* 
	 Reserve the resources that we need.  This will fail if the resources are
	 not available
	 */
	if ((status = _AET_reserveResources(
				&resources.configWord, 
				params)
				) != AET_SOK)
		return status;

	/* 
	 Make sure that the lower address is in the lower parameter 
	 */
	if ( params->dataStartAddress > params->dataEndAddress)
	{
		/* Swap them in the params Structure*/
		tempAddress = params->dataStartAddress;
		params->dataStartAddress = params->dataEndAddress;
		params->dataEndAddress = tempAddress;
	}

	/* 
	 Program the Data Watchpoint job using the resources specified
	 */
	if ((status = 
		_AET_dataWpRangeProgram(&resources, params, doQual)) != AET_SOK)
	{
		_AET_reclaimResources(resources.configWord);
		return status;
	}
	/* 
	 Insert this job into the global job table and store the job number 
	 into the parameter structure passed by the user
	 */
	params->jobIndex = _AET_insertJobIntoTable(resources.configWord);
	return status;
}

AET_error _AET_dataWpRangeProgram(
					AET_dataWatchpointRangeResources* resources,
					AET_jobParams* params,
					_AET_drcDataQualify doQual
					)
{
	_AET_drcCntlParams drcParams1 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams2 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams3 = AET_DRCCNTLPARAMS;
	_AET_drcCntlParams drcParams4 = AET_DRCCNTLPARAMS;
	_AET_tbCntlParams tbParams = AET_TBCNTLPARAMS;

	AET_error status = AET_SOK;

	_AET_BANNER("Begin", "Data Range Watchpoint");	
	/*
	 Disable All TriggerBuilders
	 */
	_AET_iregWrite((uint16_t) AET_indexof(CSL_Aet_iarRegs, TB_ENA), 0x0);

	/*
	 Get the base address for all of the resources that we're going to use
	 */

	
	drcParams1.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[0]);
	drcParams2.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[1]);
	drcParams3.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[2]);
	drcParams4.cmpBaseIndex = _AET_getAcmpBaseIndex(resources->acmpNo[3]);

	/*
	 aetTbBaseIndex is the base address of the Trigger Builder Registers
	 */

	tbParams.tbBaseIndex = 
		_AET_getTbBaseIndex(
			resources->trigBldr->tbType,
			resources->trigBldr->triggerBuilderNo
			);


	/* Program Comparator Control Registers */
	/* DRC_NA_CNTL for the first Comparator */
	/* Reference size passed by user */
	/* Qualify with Data based on passed parameter */
	/* Read/Write passed by user */
	/* Watch for all sizes of reads/writes */
	/* Use calculated byte enable value */
	/* 0xA for Event Sense (LOCAL AND REMOTE)*/
	/* Greater Than should be true */
	/* Less Than should be false */
	drcParams1.dataQualify = doQual;
	drcParams1.byteEnableMask = 0xFF;
	drcParams1.eventSenseMask = 0x8;
	drcParams1.cmpGreater = AET_CMP_TRUE;
	drcParams1.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg( 
		&drcParams1,
		params
		);

	/* DRC_AREF */
	/* Data Address to watch */
	drcParams1.addressReference = params->dataStartAddress;
	_AET_pgmDrcArefReg(&drcParams1);

	/* DRC_NA_CNTL for the second Comparator */
	/* Reference size passed by user */
	/* Qualify with Data based on passed parameter */
	/* Read/Write passed by user */
	/* Watch for all sizes of reads/writes */
	/* Use calculated byte enable value */
	/* 0x8 for Event Sense (LOCAL AND REMOTE)*/
	/* Greater Than should be false */
	/* Less Than should be true */
	drcParams2.dataQualify = doQual;
	drcParams2.byteEnableMask = 0xF;
	drcParams2.eventSenseMask = 0x8;
	drcParams2.cmpGreater = AET_CMP_FALSE;
	drcParams2.cmpLess = AET_CMP_TRUE;
	_AET_pgmDrcCntlReg(	
		&drcParams2,
		params
		);

	/* DRC_AREF */
	/* Data Address to watch */
	drcParams2.addressReference = params->dataEndAddress;
	_AET_pgmDrcArefReg(&drcParams2);

	/* Program Comparator Control Registers */
	/* DRC_NA_CNTL for the third Comparator */
	/* Reference size passed by user */
	/* Qualify with Data based on passed parameter */
	/* Read/Write passed by user */
	/* Watch for all sizes of reads/writes */
	/* Use calculated byte enable value */
	/* 0xA for Event Sense (LOCAL AND REMOTE)*/
	/* Greater Than should be true */
	/* Less Than should be false */
	drcParams3.dataQualify = doQual;
	drcParams3.byteEnableMask = 0xFF;
	drcParams3.eventSenseMask = 0x8;
	drcParams3.cmpGreater = AET_CMP_TRUE;
	drcParams3.cmpLess = AET_CMP_FALSE;
	_AET_pgmDrcCntlReg( 
		&drcParams3,
		params
		);

	/* DRC_AREF */
	/* Data Address to watch */
	drcParams3.addressReference = params->dataStartAddress;
	_AET_pgmDrcArefReg(&drcParams3);

	/* DRC_NA_CNTL for the fourth Comparator */
	/* Reference size passed by user */
	/* Qualify with Data based on passed parameter */
	/* Read/Write passed by user */
	/* Watch for all sizes of reads/writes */
	/* Use calculated byte enable value */
	/* 0x8 for Event Sense (LOCAL AND REMOTE)*/
	/* Greater Than should be false */
	/* Less Than should be true */
	drcParams4.dataQualify = doQual;
	drcParams4.byteEnableMask = 0xF;
	drcParams4.eventSenseMask = 0x8;
	drcParams4.cmpGreater = AET_CMP_FALSE;
	drcParams4.cmpLess = AET_CMP_TRUE;
	_AET_pgmDrcCntlReg(	
		&drcParams4,
		params
		);

	/* DRC_AREF */
	/* Data Address to watch */
	drcParams4.addressReference = params->dataEndAddress;
	_AET_pgmDrcArefReg(&drcParams4);


	/* Program The Data Comparison registers if necessary */
	if (doQual == AET_DRC_DATAQUAL_QUALIFY)
	{
			uint16_t aetDcmp1BaseIndex;
			uint16_t aetDcmp2BaseIndex;
			aetDcmp1BaseIndex = _AET_getDcmpBaseIndex(0);
			aetDcmp2BaseIndex = _AET_getDcmpBaseIndex(1);

		/* DRC_DREFL for the first Data Comparator */
		/* Data value specified by the user (low) */
		_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 2),
					AET_FMK(AET_IAR_DCQ_DREFL_DREFL, 
						((int32_t)(params->value & 0xFFFFFFFF)))
						);

		/* DRC_DREFH for the first Data Comparator */
		/* Data value specified by the user (high) */ 
		_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 4),
					AET_FMK(AET_IAR_DCQ_DREFH_DREFH, 
						//((int32_t)((params->value >> 32) & 0xFFFFFFFF)))
						((int32_t)(params->value & 0xFFFFFFFF)))
					);

		/* DRC_MREFL for the first Data Comparator */
		/* Mask value (low) */
		_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 3),
					AET_FMK(AET_IAR_DCQ_MREFL_MREFL,
						((int32_t)(params->valueMask & 0xFFFFFFFF)))
					);

		/* DRC_MREFH for the first Data Comparator */
		/* Mask value  (high) */
		_AET_iregWrite((uint16_t)(aetDcmp1BaseIndex + 5),
					AET_FMK(AET_IAR_DCQ_MREFH_MREFH,
						((int32_t)((params->valueMask >> 32) & 0xFFFFFFFF)))
					);

		/* DRC_DREFL for the second Data Comparator */
		/* Data value specified by the user (low) */
		_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 2),
					AET_FMK(AET_IAR_DCQ_DREFL_DREFL, 
						((int32_t)(params->value & 0xFFFFFFFF)))
						);

		/* DRC_DREFH for the second Data Comparator */
		/* Data value specified by the user (high) */ 
		_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 4),
					AET_FMK(AET_IAR_DCQ_DREFH_DREFH, 
						//((int32_t)((params->value >> 32) & 0xFFFFFFFF)))
						((int32_t)(params->value & 0xFFFFFFFF)))
					);

		/* DRC_MREFL for the second Data Comparator */
		/* Mask value (low) */
		_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 3),
					AET_FMK(AET_IAR_DCQ_MREFL_MREFL,
						((int32_t)(params->valueMask & 0xFFFFFFFF)))
					);

		/* DRC_MREFH for the second Data Comparator */
		/* Mask value  (high) */
		_AET_iregWrite((uint16_t)(aetDcmp2BaseIndex + 5),
					AET_FMK(AET_IAR_DCQ_MREFH_MREFH,
						((int32_t)((params->valueMask >> 32) & 0xFFFFFFFF)))
					);

	}

	/* TB_CNTL */
	/* Type of Trigger Builder (1-wide, 3-wide, 7-wide */
	/* Base index of trigger builder */
	/* Output control for trigger builder */
	/* Trigger Builder Lookup Table 0xAAAA (Event A only) */
	/* Triggger Builder Number */
	tbParams.tbOrsValue = 
		AET_FMK(AET_IAR_TB_ORS_MASK_A, 
				1 << resources->acmpNo[0] | 1 << resources->acmpNo[2]
				);
	tbParams.tbType = resources->trigBldr->tbType;
	tbParams.outputCntl = resources->trigBldr->outputCntl;		
	tbParams.triggers = 1 << (resources->trigBldr->triggerNo - 1);
	tbParams.boolOutput = 0xAAAA;
	tbParams.tbNo = resources->trigBldr->triggerBuilderNo;	
	_AET_pgmTrigBldr(
				&tbParams,
				params
				);
		
	/* FUNC_CNTL */	
	_AET_programFuncCtlReg(params);


	/* CIS_BUS_SEL */
	/* Set the input to the appropriate comparator to be the data bus */
	/* A zero specifies DA0, a one specifies DA1 */
	_AET_globalParams.globRegs.cisBusSel |= (1 << (4*resources->acmpNo[0]));
	_AET_globalParams.globRegs.cisBusSel |= (1 << (4*resources->acmpNo[1]));
	_AET_globalParams.globRegs.cisBusSel &= ~(1 << (4*resources->acmpNo[2]));
	_AET_globalParams.globRegs.cisBusSel &= ~(1 << (4*resources->acmpNo[3]));

	/*
	 * Depending on Whether this is a read or a write watch, we must adjust
	 * the correct bus
	 */
	if (params->readWrite == AET_WATCH_WRITE){
		_AET_globalParams.globRegs.cisBusSel &= 0xFFFFFFF33;
		_AET_globalParams.globRegs.cisBusSel |= 0x40;
	}else{
		_AET_globalParams.globRegs.cisBusSel &= 0xFFFFFFF33;
		_AET_globalParams.globRegs.cisBusSel |= 0xC8;
	}

	_AET_iregWrite((uint16_t) (AET_indexof(CSL_Aet_iarRegs, CMPI_SEL)),
					_AET_globalParams.globRegs.cisBusSel
					);

	/* TB_DM */
	/* Set this trigger builder domain to zero */
	_AET_programTbDomainReg(resources->trigBldr->triggerBuilderNo, 0);


	/*
	 Add this trigger builder into the list of trigger builders to be enabled,
	 and enable them all
	 */
	if ((status =_AET_markEnableTb(resources->trigBldr)
								   ) != AET_SOK)
	{
		return status;
	} 
	
	_AET_BANNER("End", "Data Range Watchpoint");

	return AET_SOK;   

}

