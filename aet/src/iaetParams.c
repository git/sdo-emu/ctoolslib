/*
 * iaetParams.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =================================iaet.c====================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <_aet.h>
#include <aet.h>

/* 
 Declare the default _AET_drcCntlParams structure to allow for initialization
 */
const _AET_drcCntlParams AET_DRCCNTLPARAMS = {
	sizeof(_AET_drcCntlParams),			/* size */
	NULL,								/* cmpBaseIndex */
	AET_REF_SIZE_BYTE,					/* accessRefSize */
	AET_DRC_DATAQUAL_NO_QUALIFY,		/* dataQualify */
	AET_WATCH_WRITE,					/* readWrite */
	AET_DRC_ACCESS_ANY,					/* accessType */
	NULL,								/* byteEnableMask */
	NULL,								/* eventSenseMask */
	AET_CMP_FALSE,						/* cmpGreater */
	AET_CMP_FALSE,						/* cmpLess */
	NULL								/* addressReference */
};

/* 
 Declare the default _AET_tbCntlParams structure to allow for initialization
 There is a slight difference in the implementation of the trigger builders 
 between 64x and 64x+.  This is accommodated for in the #ifdef.
 */
const _AET_tbCntlParams AET_TBCNTLPARAMS = {
	sizeof (_AET_tbCntlParams),			/* size */
	NULL,								/* tbOrsValue */
	NULL,								/* tbBaseIndex */
	NULL,								/* boolOutput */
	_AET_TB_1_WIDE,						/* tbType */
	NULL,								/* triggers */
#ifdef _TMS320C6400_PLUS
	AET_TB_ESAC_CNT0,					/* evtCSelect */
#else
	AET_TB_ESAC_NONE,					/* evtCSelect */
#endif
	NULL,								/* outputCntl */
	NULL								/* tbNo */
};
