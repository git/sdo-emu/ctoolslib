/*
 * iaet.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  =================================iaet.c====================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet.h>

/*
 *  ======AET_JOBPARAMS======
 * This ststic initialization defines the default parameters used to
 * create instances of AET objects
 *
 */
const AET_jobParams AET_JOBPARAMS = {
	sizeof(AET_jobParams),			/* size */
	AET_TRIG_NONE,					/* triggerType */
	NULL,							/* programAddress */
	NULL,							/* programRangeStartAddress */
	NULL,							/* programRangeEndAddress */
	NULL,							/* dataAddress */
	NULL,							/* dataStartAddress */
	NULL,							/* dataEndAddress */
	NULL,							/* traceStartAddress */
	NULL,							/* traceEndAddress */
	AET_REF_SIZE_BYTE,				/* refSize */
	AET_WATCH_WRITE,				/* readWrite */
	NULL,							/* value */
	0xFFFFFFFFFFFFFFFF,				/* valueMask */
	AET_EVT_NONE,					/* eventNumbers */			
	AET_EVT_NONE,
	AET_EVT_NONE,
	AET_EVT_NONE,
	AET_EVT_NONE,					/* startEventNumber */
	AET_EVT_NONE,					/* endEventNumber */
	AET_EVT_TRIG_LEVEL_DEFAULT,		/* eventTriggerConfig */
	AET_WATERMARK_START,			/* startStop */
	AET_CNT_0,						/* counterNumber */
	AET_TRACE_INACTIVE,             /* traceActive */
	NULL,							/* traceTriggers */
	AET_TRIG_LOGIC_STRAIGHTFORWARD, /* logicOrientation */
	AET_STATEQUAL_NONE,				/* stateQual */
	NULL							/* jobIndex */
};

const AET_counterConfigParams AET_COUNTERCONFIGPARAMS = {
	sizeof(AET_counterConfigParams),	/* size of this structure */
	AET_CNT_0,							/* Counter Number*/
	AET_COUNTER_TRAD_COUNTER,			/* Counter Mode */
	0xFFFFFFFF							/* Reload Value */
};

