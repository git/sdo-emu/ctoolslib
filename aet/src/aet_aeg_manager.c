/*
 * aet_aeg_manager.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ==============================aet_aeg_manager.c============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-05-29 20:55:37 -0500 (Wed, 29 May 2013) $
 *   Revision: $LastChangedRevision: 10627 $
 *   Last Update By: $Author: KarthikRamanaSankar $ 
 */

/*       1         2         3         4         5         6         7
12345678901234567890123456789012345678901234567890123456789012345678901234567890
*/

#include <aet_aeg_manager.h>
#include <aet_resource.h>
extern _AET_globalVars _AET_globalParams;

_AET_aegDatabase aegDatabase;

/*******************************************************************************
* _AET_allocateAllAegs
*
* Summary: This function allocates all of the Aegs for a job.  If one of the 
* AEG allocations fails, it is the responsibility of this function to go and
* clean up whatever has already been programmed.  It is not necessary to update
* any of the AEG registers, but the user of the signal must be subtracted from
* the aegDatabase
*
*
*
*
* Inputs:
*	AET_jobParams* jobParams - Pointer to the jobParams structure which contains
*		the array of events 
*	uint8_t* aegNos - Pointer to an unsigned integer which will be returned as a
*		bitfield to indicate which AEGs were used.  
*
*
* Returns:
*	AET_error - Error code
*		- Values
*			AET_SOK - Success
* 			AET_FNOTAVAILAEG - Not enough AEGs avaiable  
* 			AET_FNOEVENTSPECIFIED - No events populated.
*			AET_FINVALIDEVTCOMBO - The events are not all of the same class
*			AET_FINVALIDEVTSPEC - One or more of the events specified is 
*									invalid
*			AET_FINVALIDEVENTCLASS - The event class does not match the current
*									setting
*
* Other:
*	The bitfields in aegNos are set as the events are programmed.
*
*
*******************************************************************************/
AET_error _AET_allocateAllAegs(AET_jobParams* jobParams, 
							  uint8_t* aegNos)
{
	AET_error status = AET_SOK;
	_AET_aegEventClass eventClass;
	uint8_t signalCounter, thisAeg;
	
	*aegNos = 0;

	/* Check to see that all of the signals are of the same family */
	if (status = _AET_validateAegParams(jobParams, &eventClass))
		return status;

	/* 
	 Check to see that the family of these events is the one programmed 
	 in the AEG database.  if it is not, then fail.  If it's uninitialized,
	 then initialize it.
	 */
	if (eventClass == AET_EVENT_TYPE_UNINITIALIZED)
	{
		// DO Nothing.  We're not using these here
	}
	else
	{
		if (aegDatabase.eventClass == AET_EVENT_TYPE_UNINITIALIZED)
		{
			aegDatabase.eventClass = eventClass;
			_AET_globalParams.globRegs.cisBusSel &= 0xFFFCFFFF;
			_AET_globalParams.globRegs.cisBusSel |= ((uint8_t)eventClass << 16);
		}
		else if (eventClass != aegDatabase.eventClass)
		{
			return AET_FINVALIDEVTCLASS;
		}
		else
		{
		// Do Nothing, this is already set properly
		}
	}
	
	/* Program CIS_BUS_SEL */
	_AET_iregWrite(AET_indexof(CSL_Aet_iarRegs, CMPI_SEL),
				_AET_globalParams.globRegs.cisBusSel
				);
	
	/*
	 Now begin to allocate each signal individually.  if we hit an AET_EVT_NONE,
	 we should bail out early because we won't have any more signals
	 */

	_AET_PRINTF("---Begin Allocating Aux Evt Generators to signals---\n");
	for (signalCounter=0;signalCounter < AET_NUM_AEG; signalCounter++)
	{
		if (jobParams->eventNumber[signalCounter] == AET_EVT_NONE)
		{
			break;
		}
		if (status = (_AET_allocateSingleAeg(
						jobParams->eventNumber[signalCounter], 
						&thisAeg,
						jobParams->eventTriggerConfig)))
		{
			_AET_deAllocateAllAegs(*aegNos);
			return status;
		}

		*aegNos |= 1 << thisAeg;
	}
	_AET_PRINTF("---End Allocating Aux Evt Generators to signals---\n");

	return status;	
}


/*******************************************************************************
* _AET_deAllocateAllAegs
*
* Summary: This function de-allocates multiple AEGs for a specific job
*
*
*
* Inputs:
* 	uint8_t aegNos
*
* Returns:
*	AET_error status - Error code 
*	
*		- Values
*			AET_SOK - Success
* 			AET_FNOAEGUSERS - There are no users of this AEG
*
* Other:

*******************************************************************************/
AET_error _AET_deAllocateAllAegs(uint8_t aegNos)
{
	AET_error status = AET_SOK;
	AET_error internalStatus;
	uint8_t signalNo;

	for (signalNo = 0; signalNo < AET_NUM_AEG; signalNo++)
	{
		if (1 << signalNo && aegNos)
		{
			if (internalStatus = _AET_deAllocateSingleAeg(signalNo))
			{
				status = internalStatus;
			}

		}

	}
	
	return status;
}

/*******************************************************************************
* _AET_deAllocateSingleAeg
*
* Summary: This function de-allocates a single AEG 
*
*
*
*
* Inputs:
* 	uint8_t aegNo
*
* Returns:
*	AET_error status - Error code 
*	
*		- Values
*			AET_SOK - Success
* 			AET_FNOAEGUSERS - There are no users of this AEG
*
* Other:

*******************************************************************************/
AET_error _AET_deAllocateSingleAeg(uint8_t aegNo)
{
	if (aegDatabase.aegAlloc[aegNo].numUsers > 0)
	{
		aegDatabase.aegAlloc[aegNo].numUsers--;
		return AET_SOK;
	}
	else
	{
		aegDatabase.aegAlloc[aegNo].numUsers = 0;
		return AET_FNOAEGUSERS;
	}
}

/*******************************************************************************
* _AET_getEventClass
* 	Summary - returns the class of the event that is passed in.
*
*	Parameters - eventNumber
*	Returns - _AET_aegEventClass
*			- AET_EVENT_TYPE_MEMORY - Memory Event Group
*			- AET_EVENT_TYPE_STALL - Stall Event Group
*			- AET_EVENT_TYPE_MISC - Miscellaneous Event Group
*			- AET_EVENT_TYPE_INVALID - Event number is invalid
*******************************************************************************/
_AET_aegEventClass _AET_getEventClass(uint16_t eventNumber)
{
	_AET_aegEventClass eventClass;

	/* Check to see if the event is a Memory Event */
	if ((eventNumber >= AET_GEM_MEM_EVT_START) &&
		(eventNumber <= AET_GEM_MEM_EVT_END))
	{
		eventClass = AET_EVENT_TYPE_MEMORY;
	}
	/* Check to see if the event is a Stall Event */
	else if ((eventNumber >= AET_GEM_STALL_EVT_START) &&
			 (eventNumber <= AET_GEM_STALL_EVT_END))
	{
		eventClass = AET_EVENT_TYPE_STALL;
	}
	/* Check if it's a miscellaneous event */
	else if (((eventNumber <  AET_NUM_EXT_EVTS))||
			 ((eventNumber >= AET_GEM_MISC_EVT_START) &&
			  (eventNumber <= AET_GEM_MISC_EVT_END)))
	{
		eventClass = AET_EVENT_TYPE_MISC;
	}
	else 
	{
		eventClass = AET_EVENT_TYPE_INVALID;
	}
			

	return eventClass;
}

/*******************************************************************************
* _AET_initAegResourceMgr
* 
*	Summary - initializes resources needed by the AEG resource manager.  This is
*		called by the AET_init function.  
*	Returns - void
*
*******************************************************************************/
void _AET_initAegResourceMgr()
{
	int i;

		aegDatabase.eventClass = AET_EVENT_TYPE_UNINITIALIZED;
	for (i=0; i<AET_NUM_AEG; i++)
	{
		aegDatabase.aegAlloc[i].evtNo = AET_EVT_NONE;
		aegDatabase.aegAlloc[i].numUsers = 0;
	}
}

/*******************************************************************************
* _AET_programAeg
*
*	Summary: Accepts an integer representing the number of the AEG and connects 
*	a specific event to that 
*******************************************************************************/
AET_error _AET_programAeg(uint8_t aegNo, uint16_t eventNumber, AET_evtTrigConfig trigConfig)
{
	AET_error status = AET_SOK;
	_AET_aegEventClass eventClass;
	uint16_t aetAegBaseIndex;
	uint8_t signalNo;

	aetAegBaseIndex = _AET_getAegBaseIndex(aegNo);
	eventClass = _AET_getEventClass(eventNumber);



	/* Configure the AEG */
	/* AEG_CNTL */
	_AET_iregWrite(aetAegBaseIndex,
					AET_FMK(AET_IAR_AUX_EVT_CNTL_TDR, 0x0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_TMD, DISABLE)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SCD, DOMAIN0)|
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_SYNC, ENABLE)|
					AET_FMK(AET_IAR_AUX_EVT_CNTL_EDGE, trigConfig >> 9) |
					AET_FMKT(AET_IAR_AUX_EVT_CNTL_ALIGN, DISABLE)|
					AET_FMK(AET_IAR_AUX_EVT_CNTL_EP, (uint8_t)trigConfig & 0xff )
					);

	switch (eventClass)
	{
		case AET_EVENT_TYPE_MISC:
			/* Do Dynamic Signal Allocation if Necessary */
			if (status = _AET_getEventNumber(eventNumber, &signalNo))
				return status;
			break;
		case AET_EVENT_TYPE_MEMORY:
			signalNo = eventNumber - AET_GEM_MEM_EVT_START;
			break;
		case AET_EVENT_TYPE_STALL:
			signalNo = eventNumber - AET_GEM_STALL_EVT_START;
			break;
			
	}


	/* AEG_ENA */
	_AET_iregWrite(aetAegBaseIndex + 1,
					AET_FMK(AET_IAR_AE_ENA_ENA, 1 << signalNo)
					);
	return status;
}
/*******************************************************************************
* _AET_reclaimAegs
*
* Summary: We are reclaiming the Auxilliary Event Generators here.  There may
* be multiple jobs that use the same AEG, so we must check to see that no other
* jobs are using it before it can be totally reclaimed.
*******************************************************************************/
void _AET_reclaimAegs(uint8_t aegsUsed)
{
	uint8_t i, totalAegsUsed = 0;

	for (i=0; i<AET_NUM_AEG; i++)
	{
		if (aegsUsed & (1 << i))
		{
			aegDatabase.aegAlloc[i].numUsers--;
		}
		totalAegsUsed += aegDatabase.aegAlloc[i].numUsers;
		if (totalAegsUsed == 0)
		{
			aegDatabase.eventClass = AET_EVENT_TYPE_UNINITIALIZED;
		}
	}
}

/*******************************************************************************
* _AET_validateAegParams
*
* Summary: Validates that appropriate AEG Event parameters are passed  This 
* includes verifying that they are all of the same eventClass.  It also puts 
* populated parameters at the beginning of the array.
*
*
*
* Inputs:
* 	AET_jobParams* params - Pointer to the jobparameters structure which contains
*  		the array of events to analyze.
*	_AET_aegEventClass* eventClass - Pointer to an _AET_eventClass object that 
*		will be set to whatever the class of events is
*
*
* Returns:
*	AET_error - Error code 
*	
*		- Values
*			AET_SOK - Success
*			AET_FINVALIDEVTCOMBO - The events are not all of the same class
*			AET_FINVALIDEVTSPEC - One or more of the events specified is 
*									invalid
*
* Other:
*	The eventClass location will be set to the appropriate class for this set
*	of events.
*
*******************************************************************************/
AET_error _AET_validateAegParams(AET_jobParams* jobParams,
								 _AET_aegEventClass* eventClass)
{
	AET_error status = AET_SOK;
	_AET_aegEventClass currentClass;

	uint8_t i, j;
	*eventClass = AET_EVENT_TYPE_UNINITIALIZED;

	
	/* */
	for (i=0; i<AET_NUM_AEG; i++)
	{
		/* If this event is AET_EVT_NONE, no need to check it */
		if (jobParams->eventNumber[i] == AET_EVT_NONE)
		{
			continue;
		}

		/* Get the class of the current signal */
		currentClass = _AET_getEventClass(jobParams->eventNumber[i]);

		/* 
		 If the class is invalid, no reason to continue. Return the 
		 error.  Otherwise, note that we have at least one valid event
		 */
		if (currentClass == AET_EVENT_TYPE_INVALID)
		{
			return AET_FINVALIDEVTSPEC;
		}

		
		/* 
		 If the global class type has been initialized already, be sure the 
		 currentClass matches it.  If it hasn't been initialized, then 
		 just set it to the currentClass.
		 */
		if (*eventClass == AET_EVENT_TYPE_UNINITIALIZED)
		{
			*eventClass = currentClass;
		}
		else
		{
			if (currentClass != *eventClass)
			{
				return AET_FINVALIDEVTCOMBO;
			}
		}
	}

	/* Put the events in order so that all AET_EVT_NONE's are at the end.  */
	for (i=0; i<AET_NUM_AEG; i++)
	{
		if (jobParams->eventNumber[i] == AET_EVT_NONE)
		{
			for (j=i; j<AET_NUM_AEG; j++)
			{
				if (jobParams->eventNumber[j] != AET_EVT_NONE)
				{
					jobParams->eventNumber[i] = jobParams->eventNumber[j];
					jobParams->eventNumber[j] = AET_EVT_NONE;
				}

			}
		}
	}

	return status;
}




/*******************************************************************************
* _AET_checkAegEvents
* - Check for at least one valid event
* - Check to ensure that all events are from the same family
* - Put the events first and the AET_EVT_NONE at the end.
* - Ensure that none of the events are invalid
*
* Possible return codes:
* - AET_SOK
* - AET_FNOEVENTSPECIFIED
* - AET_FINVALIDEVTCOMBO
* - AET_FINVALIDEVTSPEC
* 
*
*******************************************************************************/
AET_error _AET_checkAegEvents(AET_jobParams* jobParams)
{
	AET_error status = AET_SOK;
	_AET_aegEventClass currentClass;
	_AET_aegEventClass globalClass = AET_EVENT_TYPE_UNINITIALIZED;

	uint8_t counter, counter2;
	unsigned short validEvtPresent = AET_FALSE;

	for (counter = 0; counter < AET_NUM_AEG; counter++)
	{
		/* 
		 If the signal is AET_EVT_NONE, skip it
		 */
		if (jobParams->eventNumber[counter] == AET_EVT_NONE)
		{
			continue;
		}

		currentClass = _AET_getEventClass(jobParams->eventNumber[counter]);
		
		if (AET_EVENT_TYPE_INVALID == currentClass)
		{
			return AET_FINVALIDEVTSPEC;
		}


		if (AET_EVENT_TYPE_UNINITIALIZED == globalClass)
		{
			globalClass = currentClass;
			validEvtPresent = AET_TRUE;
		}
		else
		{
			if (currentClass != globalClass)
			{
				return AET_FINVALIDEVTCOMBO;
			}
		}
		
	}
	
	if (AET_FALSE == validEvtPresent)
	{
		return AET_FNOEVENTSPECIFIED;
	}

	for (counter = 0; counter < AET_NUM_AEG; counter++)
	{
		if (AET_EVT_NONE != jobParams->eventNumber[counter])
		{
			continue;
		}

		for (counter2 = counter; counter2 < AET_NUM_AEG; counter2++)
		{
			if (AET_EVT_NONE != jobParams->eventNumber[counter2])
			{
				jobParams->eventNumber[counter] = jobParams->eventNumber[counter2];
				jobParams->eventNumber[counter2] = AET_EVT_NONE;
			}
		}
	}
	return status;

}




/*******************************************************************************
* _AET_aegAlloc
* 	Summary - Accepts a pointer to the AET_jobParams structure where it can grab 
*			  the event parameters.  It will go through and allocate each of 
*			  these events to a particular signal and then return a bitfield of
* 			  the signals that it uses.   
*
*	Parameters - pointer to the _AET_jobParams structure
*	Returns - AET_ERROR
*******************************************************************************/
AET_error _AET_aegAlloc(uint8_t* aegNo, AET_jobParams* jobParamsPtr)
{
	AET_error status = AET_SOK;

	uint8_t aegCounter, eventCounter;
	uint8_t usedAegs, numEvents;
	unsigned short signalAlloc;

	*aegNo = 0;
	aegCounter = 0;
	eventCounter = 0;
	numEvents = 0;
	usedAegs = _AET_globalParams.AET_usedResources.AEG;

	/* First find if we have enough AEGs available to do the whole job */
	for (eventCounter = 0; eventCounter < AET_NUM_AEG; eventCounter++)
	{
		signalAlloc = AET_FALSE;
		/* Check to see if we're done with all the events */
		if (jobParamsPtr->eventNumber[eventCounter] == AET_EVT_NONE)		{
			break;
		}
		else
		{
			numEvents++;
		}

		/* Find an available AEG */
		;
		for (aegCounter = 0; aegCounter < AET_NUM_AEG; aegCounter++)
		{
			if (!(usedAegs & (1 << aegCounter)))
			{
				usedAegs |= (1 << aegCounter);
				*aegNo |= (1 << aegCounter);
				signalAlloc = AET_TRUE;
				/* Set the value in the AE_ENA register */
				break;
			}
		}

		if (signalAlloc == AET_FALSE)
		{
			return AET_FNOTAVAILAEGMUX;
		}

	}


	return status;

}




/*******************************************************************************
* _AET_programAegs
* 	Summary - Accepts a uint8_t that represents a bitfield for the number of AEGs
*			  used.  It uses information gathered from the jobParams structure
*			  to program each of the AEGs to point to the correct signal.
*	Parameters - uint8_t representing a bifield of used aegs.
*			   - pointer to the _AET_jobParams structure
*
*	Returns - AET_ERROR
*******************************************************************************/
AET_error _AET_programAegs(uint8_t aegs, AET_jobParams* jobParams, AET_evtTrigConfig trigConfig)
{
	AET_error status = AET_SOK;
	int i, j;

	i = 0;
	while (jobParams->eventNumber[i] != AET_EVT_NONE)
	{
		for (j=0; j< AET_NUM_AEG; j++)
		{
			if (aegs & (1 << j))
			{
				aegs &= ~(1 << j);
				if (status = _AET_programAeg(j, jobParams->eventNumber[i], trigConfig))
				{
					return status;
				}
				i++;
				break;
			}
		}
		if (AET_NUM_AEG == i)
		{
			break;
		}
	}
	return status;
}



/*******************************************************************************
* _AET_reserveAegs
*
*******************************************************************************/
AET_error _AET_reserveAegs(AET_jobParams* jobParams, uint8_t* aegsAllocated, AET_evtTrigConfig trigConfig)
{
	AET_error status = AET_SOK;
	

	/* Check to see that we have valid events */
	if (status = _AET_checkAegEvents(jobParams))
	{
		return status;
	}

	/* Check to see that the event combination is valid */
	/* This will also program the CIS_BUS_SEL register for the
	   proper class of signal */
//	if (status = _AET_validateAegParams(jobParams))
//	{
//		return status;
//	}

	/* Allocate the AEGs for each signal */
	if (status = _AET_aegAlloc(aegsAllocated, jobParams))
	{
		return status;
	}

	/* Finally, program the AEGs */
	status = _AET_programAegs(*aegsAllocated, jobParams, trigConfig);
	return status;

}







/*******************************************************************************
* _AET_allocateSingleAeg
*
* Summary: This function allocates a single AEG for a sprcific signal.  If the
* signal is already allocated to an AEG, the number of that AEG is returned. If
* it is not, then a new AEG is allocated if possible.  If no AEG is available,
* an error status will be returned
*
*
*
* Inputs:
* 	AET_eventNumber eventNumber - Specifies the event number to be allocated
*	uint8_t* aegNo - specifies the AEG number allocated for the signal
*
*
* Returns:
*	AET_error - Error code 
*	
*		- Values
*			AET_SOK - Success
* 			AET_FNOTAVAILAEG - AEG not available to allocate for this signal
*
* Other:
*	The aegNo value is a secondary return parameter.  This value specifies the
*	number of the AEG that has been allocated for this signal.
*
*
*******************************************************************************/
AET_error _AET_allocateSingleAeg(uint16_t eventNumber,
								 uint8_t* aegNo,
								 AET_evtTrigConfig trigConfig)
{
	uint8_t aegCounter;

	/* 
	 Check if this signal has already been allocated.  If it has, set aegNo to 
	 the number of the AEG it has been allocated to and return.
	 */
	for (aegCounter=0; aegCounter < AET_NUM_AEG; aegCounter++)
	{
		if (aegDatabase.aegAlloc[aegCounter].evtNo == eventNumber)
		{
			*aegNo = aegCounter;
			aegDatabase.aegAlloc[aegCounter].numUsers++;
			return AET_SOK;
		}
	}

	/*
	 Attempt to allocate the signal to a new AEG.  If allocation is successful,
	 set the aegNo to the number of the new AEG allocated.  
	 */
	for (aegCounter=0; aegCounter < AET_NUM_AEG; aegCounter++)
	{
		if (aegDatabase.aegAlloc[aegCounter].numUsers == 0)
		{
			_AET_programAeg(aegCounter, eventNumber, trigConfig);
			*aegNo = aegCounter;
			aegDatabase.aegAlloc[aegCounter].numUsers++;
			return AET_SOK;
		}
	}

	return AET_FNOTAVAILAEG;
}




