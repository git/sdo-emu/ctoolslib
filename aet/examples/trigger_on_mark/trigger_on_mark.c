/*
 * trigger_on_mark.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ===========================trigger_on_mark.c===============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the capability of the AET Target library to 
 * generate a trigger whenever a specific event happens.  In this specific case
 * AET will generate a halt when it sees a MARK 0 instruction.  A Mark 
 * instruction executes like a NOP but generates an Event for the AET Unit.  The
 * MARK instruction is only available on 64x+ devices.
 ******************************************************************************/
#include <aet.h>
#include <stdio.h>

void func1();

/* Function Prototypes */
void main()
{
	/* Declare the Parameters Structure */
	AET_jobParams params = AET_JOBPARAMS;
	AET_jobIndex jobIndex;
	volatile Uint8 x = 0;

	params = AET_JOBPARAMS; /* Initialize parameters structure */

	/* Set the parameters structure for the appropriate trigger and event. */
	params.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;
	params.triggerType = AET_TRIG_HALT_CPU;

 
	/* Initialize AET */
	AET_init();
	
	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set up the Trace Start job */
	if (AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &params))
		return;

	jobIndex = params.jobIndex;

	printf("The Job index is %d\n", jobIndex);


	/* Enable AET */
	if (AET_enable())
		return;

	/* Now just run in an infinite loop */  
	while (x == 0)
	{
		func1();
	}

	/* Clean Up */
	AET_releaseJob(jobIndex);
	AET_release();

}


void func1()
{
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" MARK 1");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" MARK 0");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");

}



