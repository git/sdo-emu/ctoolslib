/*
 * event_triggered_watermark.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ===================event_triggered_watermark.c=============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-17 08:09:02 -0600 (Thu, 17 Jan 2013) $
 *   Revision: $LastChangedRevision: 10592 $
 */


#include <aet.h>
#include <stdio.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
 *	This example is designed to demonstrate the Event Triggered Watermark
 *	counter start/stop functionality of the AET target library.  The target 
 *  application programs the counter to begin a cycle watermark window on a MARK0
 *  instruction. The watermark window closes on a MARK1 instruction. In the example,
 *  the watermark window is set to retain the MAX value of all windows. A read of
 *  the counter displays the MAX value has been achieved over the set of instances
 *  in this application.
 *
 *  In this example, the watermark window is kept open first for 0x100 delay
 *  loop iterations and then for 0x1000 delay loop iterations. The max watermark
 *  captured will be equal to the time taken by the second 0x1000 iterations delay
 *  loop.
 *  ============================================================================
 *  
 */

/* Function Prototypes */
void func1();

void main()
{
	/* Local Variables*/
    
    /* 
      Trigger Event Parameters 

	  These structures hold the parameters that are passed to the AET 
	  library
	  */
    AET_jobParams wmStartParams = AET_JOBPARAMS;
	AET_jobParams wmStopParams = AET_JOBPARAMS;
	AET_jobParams tmStartParams = AET_JOBPARAMS;
    AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;

	/* 
	 Job Indexes*
	 
	 For each Job that we set up, we need to remember it's index in the job
	 table so that the job can be cleared later
	 */
	AET_jobIndex jobWmStart, jobWmStop, jobTmStart;

	/* 
	 Value returned from the watermark counter
	 */
	Uint32 wmCountValue;

    /* Iteration Number */
    Uint32 iterator;

	wmStartParams = AET_JOBPARAMS; /* Initialize Job Parameter Structure */
	wmStopParams = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	/**************************************************************************
	* AET PROGRAMMING STARTS HERE
	***************************************************************************/

    /* Initialize AET */
    AET_init();

    /* Claim the AET resource */
    if (AET_claim())
        return;


    /* Set the configuration parameters */
    counter0.configMode = AET_COUNTER_MAXWM_CONTINUOUS;
    counter0.counterNumber = AET_CNT_0;
    counter0.reloadValue = 0xFFFFFFFF;  /* 
    									 This is the value reloaded
										 into the watermark counter
										 at the end of a watermark
										 window
										 */

    /* Configure the counter */
    AET_configCounter(&counter0);

	/* 
	 Configure a Watchpoint Job to start counter 0 
	 
	 If we don't somehow kickstart the counter, we will never see any values.  
	 A watchpoint job has been arbitrarily chosen to generate this trigger.  
	 The trigger is generated when the address specified by &func1 is 
	 encountered.
	 */
	tmStartParams.programAddress = (Uint32) &func1;
	tmStartParams.triggerType = AET_TRIG_CNT0_START;

	/* Program the job to kickstart the counter */
	if (AET_setupJob(AET_JOB_TRIG_ON_PC, &tmStartParams))
		return;

	/* Store the index of the kickstart job */
	jobTmStart = tmStartParams.jobIndex;

    /* 
     Set the trigger parameters for Watermark Start 
     
     We want to tie the MARK0 event to the start event for watermark counter 0
     */
	wmStartParams.counterNumber = AET_CNT_0;
	wmStartParams.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;
	wmStartParams.startStop = AET_WATERMARK_START;

    /* 
     Set the trigger parameters for Watermark Stop 
     
     We want to tie the MARK1 event to the start event for watermark counter 0
     */
	wmStopParams.counterNumber = AET_CNT_0;
	wmStopParams.eventNumber[0] = AET_EVT_MISC_MARK_INS_1;
	wmStopParams.startStop = AET_WATERMARK_STOP;

    /* Program the counter for the start event */
    if (AET_setupJob(AET_JOB_WATERMARK_START_END, &wmStartParams))
        return;

	/* Store the index of the watermark start job */
	jobWmStart = wmStartParams.jobIndex;

	/* Program the counter for the stop event */
	if (AET_setupJob(AET_JOB_WATERMARK_START_END, &wmStopParams))
		return;
	
	/* Store the index of the watermark stop job */
	jobWmStop = wmStopParams.jobIndex;

	    /* Enable AET */
    if ( AET_enable())
        return;

	/* Call func1, which should start the counter */
	func1();

	asm (" MARK 0"); //open max watermark window

	// Each iteration roughly takes ~10 cycles. Total cycles = 0xA00
    /* Loop and continually print the timer value */
    for (iterator = 0; iterator < 0x100; iterator++)
    {
    	asm (" nop");
    }

    asm (" MARK 1"); //close max watermark window

    asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");

	asm (" MARK 0"); //open max watermark window

	// Each iteration roughly takes ~10 cycles. Total cycles = 0xA000
	// Max watermark captured should be ~0xA000 cycles
    for (iterator = 0; iterator < 0x1000; iterator ++)
	{
		asm (" nop");
	}

    asm (" MARK 1"); //close max watermark window

	/* Read the value from the counter */
    wmCountValue = AET_readCounter(AET_CNT_0);


	/* Clear all jobs */
	AET_releaseJob(jobWmStart);
	AET_releaseJob(jobWmStop);
	AET_releaseJob(jobTmStart);

	/* Generate a trigger to stop the counter */
	tmStartParams.programAddress = (Uint32) &func1;
	tmStartParams.triggerType = AET_TRIG_CNT0_STOP;

	if (AET_setupJob(AET_JOB_TRIG_ON_PC, &tmStartParams))
		return;

	jobTmStart = tmStartParams.jobIndex;

	/* Call func1 to generate the Stop Trigger */
	func1();

	/* Clear the Stop Job */
	AET_releaseJob(jobTmStart);

	/* Print the result */
	printf("The value in the counter is 0x%x\n", wmCountValue);
	

}



void func1(void)
{
        asm (" nop");
        asm (" nop");
        asm (" nop");
        asm (" nop");
        asm (" nop");
        asm (" nop");
}