/*
 * state_machine.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ===============================state_machine.c=============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/*  ===========================================================================
 *  ==                              OVERVIEW                                 ==
 *  ===========================================================================
 *
 ******************************************************************************/

#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void testfunc1();
void testfunc2();

volatile Uint32 testDataLocation = 0;
static Uint32 count = 0;

void main()
{
	AET_jobParams stateOneJob = AET_JOBPARAMS;
	AET_jobParams haltJob = AET_JOBPARAMS;
	AET_jobIndex stateOneJobIndex;
	AET_jobIndex haltJobIndex;

	

	/* 
	 Set the first job to go to state 1 when testDataLocation is 
	 written to
	 */
	stateOneJob.dataAddress = (Uint32) &testDataLocation;
	stateOneJob.triggerType = AET_TRIG_STATE_0_TO_1;
	stateOneJob.readWrite = AET_WATCH_WRITE;
	stateOneJob.refSize = AET_REF_SIZE_WORD;
 
	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;
	
	/* Setup the AET job with selected parameters */
	if (AET_setupJob(AET_JOB_TRIG_ON_DATA, &stateOneJob))
		return;

	stateOneJobIndex = stateOneJob.jobIndex;
	printf("The jobIndex for the State job is %d\n", stateOneJobIndex);

		if (AET_enable())
		return;

	/* 
	 Program the second job to halt on a call to func2, but
	 qualify it with state 1
	 */
	haltJob.programAddress = (Uint32) &testfunc2;
	haltJob.triggerType = AET_TRIG_HALT_CPU;
	haltJob.stateQual = AET_STATEQUAL_STATE1;
	
		/* Setup the AET job */
	if (AET_setupJob(AET_JOB_TRIG_ON_PC, &haltJob))
		return;

	haltJobIndex = haltJob.jobIndex;
	printf("The jobIndex for the Halt job is %d\n", haltJobIndex);
		/* Enable AET */
	if (AET_enable())
		return;

	/* Call testfunc1*/
	testfunc1();

}

void testfunc1()
{
	


	while (1)
	{

		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");


		count++;
		if (count==60)
		{
			testDataLocation=10;
		}

		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");
		asm(" nop");


		testfunc2();
	}
}

void testfunc2()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");

}
