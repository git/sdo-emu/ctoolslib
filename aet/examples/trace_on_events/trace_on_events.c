/*
 * trace_on_events.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ==============================trace_on_events.c============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 11:20:52 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10596 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the functionality of the Trace on Event 
 * function of the AET Target Library.  The Trace Event is triggered upon 
 * executing a MARK0 instruction.
 ******************************************************************************/
#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void func1();
void func2();
void voidFunc();
void voidFuncWithArgs(int, int*);
int  intFunc();
int  intFuncWithArgs(int, int);
int  recurseFunc(int);

int zzz = 9;
int xxx = 7;
int yyy = 8;
short aaa = 9;
char ccc = 10;

void main()
{

	AET_jobParams params;
	AET_jobIndex jobNumber;

	params = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	params.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;
	params.traceActive = AET_TRACE_ACTIVE;
	params.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;

 
	AET_init();
	
	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set up the desired job */
	if (AET_setupJob(AET_JOB_TRACE_ON_EVENTS, &params))
		return;

	jobNumber = params.jobIndex;

	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	/* Call Func1 (which calls Func2 and starts Trace) */
	func1();

	/* We'll Never get here, but if we did, we'd want to clean up */
	AET_releaseJob(jobNumber);
	AET_release();


}

void func1()
{
	/* Generic Variables */
	int a = 1, cnt=0;
    int ret;
	int i;

	asm (" MARK 0");

	func2();
	for (i = 0; i < 50; i++)
	{
		aaa++;
		ccc++;
	}

	xxx++;
	yyy++;
	cnt = 1;
	voidFunc();
	cnt++;
	voidFuncWithArgs(a, &ret);
	cnt++;
	ret = intFunc();
	cnt++;
	ret = recurseFunc(ret);
}

void func2()
{
	int i = 0;
	int j = 0;

	asm (" MARK 0");

	for (i=0; i<100; i++)
	{
		j += 1;

	}

	i++;
}


/*
 *  ======== VoidFunc ========
 */
void voidFunc()
{
	int i=0;

	asm (" MARK 0");

	i = i + 25;
	
}

/*
 *  ======== VoidFuncWithArgs ========
 */
void voidFuncWithArgs(int a1, int* pa2)
{
	int ret=0;

	asm (" MARK 0");

	ret = intFuncWithArgs(a1, 5);
	*pa2 = a1+ret;
}

/*
 *  ======== IntFunc ========
 */
int  intFunc()
{
	int ret=1;

	asm (" MARK 0");

	ret = ret * 10;
	return ret;
}

/*
 *  ======== IntFuncWithArgs ========
 */
int  intFuncWithArgs(int a1, int a2)
{
	asm (" MARK 0");

	a1 = a1 * a2;
	return a1;
}

/*
 *  ======== RecurseFunc ========
 */
int  recurseFunc(int a1)
{
   int b= 0;

   asm (" MARK 0");

   if(a1 == 1)
      return 1;
   b = a1;
   b *= recurseFunc(a1 -1);
   return b;
}