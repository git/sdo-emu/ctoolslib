/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  @(#) DSP/BIOS_Examples 5,3,0 08-08-2006 (biosEx-h04)
 */
/*
 *  ======== slice.tci ========
 *  Tconf include file imported by slice.tcf which sets up global, platform
 *  independent BIOS objects, properties, and parameters.
 *
 */

/* Increase the buffer size of the LOG_system LOG object */

bios.LOG_system.bufLen = 2048;
bios.LOG_system.logType = "circular";

/* Create a trace LOG object for printing basic program output.  */

var trace = bios.LOG.create("trace");
trace.bufLen = 1024;
trace.logType = "circular";

/* Create a PRD object to cause tasks to yield periodically */

var prd0 = bios.PRD.create("prd0");
prd0.period = 1;
prd0.mode = "continuous";
prd0["fxn"] = prog.extern("prdfxn0");

/* Create a PRD object to post a semaphore periodically */

var prd1 = bios.PRD.create("prd1");
prd1.period = 16;
prd1.mode = "continuous";
prd1["fxn"] = prog.extern("prdfxn1");

/* Create a semaphore for code synchronization */

var sem = bios.SEM.create("sem");
sem.count = 0;

/* Create three TSKs of equal priority and one TSK with higher priority */

var TSK1 = bios.TSK.create("TSK1");
TSK1["fxn"] = prog.extern("task");
TSK1.priority = 1;
TSK1.arg0 = 1;

var TSK2 = bios.TSK.create("TSK2");
TSK2["fxn"] = prog.extern("task");
TSK2.priority = 1;
TSK2.arg0 = 2;

var TSK3 = bios.TSK.create("TSK3");
TSK3["fxn"] = prog.extern("task");
TSK3.priority = 1;
TSK3.arg0 = 3;

var TSK4 = bios.TSK.create("TSK4");
TSK4["fxn"] = prog.extern("hi_pri_task");
TSK4.priority = 2;
TSK4.arg0 = 4;

var TSK5 = bios.TSK.create("TSK5");
TSK5["fxn"] = prog.extern("keepAlive");
TSK5.priority = 1;
TSK5.stackSize = 0xA0;
