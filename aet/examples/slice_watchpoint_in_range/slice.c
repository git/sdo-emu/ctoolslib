/*
 * slice.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *
 *  @(#) DSP/BIOS_Examples 5,3,0 08-08-2006 (biosEx-h04)
 */
/*
 *  ======== slice.c ========
 *  This example utilizes time-slice scheduling among three
 *  tasks of equal priority.  A fourth task of higher
 *  priority periodically preempts execution.
 *  
 *  A PRD object drives the time-slice scheduling. Every
 *  millisecond, the PRD object calls TSK_yield()
 *  which forces the current task to relinquish access to
 *  to the CPU.  The time slicing could also be driven by
 *  a CLK object (as long as the time slice was the same interval
 *  as the clock interrupt), or by another hardware
 *  interrupt.
 *
 *  The time-slice scheduling is best viewed in the Execution
 *  Graph with SWI logging and PRD logging turned off.
 *
 *  Because a task is always ready to run, this program
 *  does not spend time in the idle loop.  Calls to IDL_run()
 *  are added to force the update of the Real-Time Analysis
 *  tools.  Calls to IDL_run() are within a TSK_disable(),
 *  TSK_enable() block because the call to IDL_run()
 *  is not reentrant.
 *  
 *  In this example, with AETLIB and HOOK switch fxn programmed
 *  to trap on stack overflow we make the hi-pri task have 
 *  too small a task stack hence it should halt
 */

#include <std.h>

#include <clk.h>
#include <idl.h>
#include <log.h>
#include <sem.h>
#include <swi.h>
#include <tsk.h>

#include "slicecfg.h"

Void task(Arg id_arg);
Void hi_pri_task(Arg id_arg);
Void keepAlive();
Uns counts_per_us;      /* hardware timer counts per microsecond */

/*
 *  ======== main ========
 */
Void main()
{
    LOG_printf(&trace, "Slice example started!");
    counts_per_us = CLK_countspms() / 1000;
}


/*
 * ======== task ========
 */
Void task(Arg id_arg)
{
    Int id = ArgToInt(id_arg);
    LgUns time;
    LgUns prevtime;

    /*
     * The while loop below simulates the work load of
     * the time sharing tasks
     */
    for (;;) {

        time = CLK_gethtime() / counts_per_us;

        /* print time only every 200 usec */
        if (time >= prevtime + 200) {
            prevtime = time;
            LOG_printf(&trace, "Task %d: time is(us) Ox%x", id, (Int)time);
        }

        /* check for rollover */
        if (prevtime > time) {
            prevtime = time;
        }

        /*
         * pass through idle loop to pump data to the Real-Time
         * Analysis tools
         */
        TSK_disable();
        IDL_run();
        TSK_enable();
    }
}


/*
 * ======== hi_pri_task ========
 */
Void hi_pri_task(Arg id_arg)
{
    Int id = ArgToInt(id_arg);

    for (;;) {
        LOG_printf(&trace, "Task %d here", id);
        SEM_pend(&sem, SYS_FOREVER);
    }
}


/*
 *  ======== keepAlive ========
 *  simply to check app is still alive. We set this TSKs 
 *  stack size to be too low so that AETLIB watchpoint catches it.
 *  You can validate that this TSK runs normally (no stack ovflw) if you
 *  bump up stack size (e.g. double it).
 */
Void keepAlive()
{
    for (;;) {
       LOG_printf(&trace, "Still alive...");
       TSK_sleep(100);
    }
}


Void prdfxn0()
{
    TSK_yield();
}


Void prdfxn1()
{
    SEM_post(&sem);
}


