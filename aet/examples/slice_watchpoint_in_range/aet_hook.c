/*
 * aet_hook.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ==============================aet_hook.c===================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *  ===========================================================================
 *
 *  
 *  This code checks for stack overflows in BIOS TSK-based applications
 *
 *  Each BIOS TSK has its own stack hence static GUI-based CCS AET
 *  programmation is insufficient since you quickly run out of 
 *  AET resources if # TSKs > 2, which is the case in most apps.
 *  
 *  Hence, approach here is to *reprogram* AET jobs on the fly to 
 *  catch stack overflow in the next TSK that BIOS scheduler will run.
 *  Setting up jobs in BIOS Hook switch function context enables this.
 *  We set up a Watchpoint in range. Any writes to that small range
 *  near the top of stack occur signal danger! We set AET to trigger
 *  an interrupt if we hit the danger range. In that ISR function
 *  we can take whatever action needed to deal with the overflow.
 *
 *  A second goal of this code is to be application-agnostic. You should
 *  be able to take this C code + TCI file and add it to any project.
 *  There's no need to "add a call in main()" or other application mods.
 *  Instead you simply add this file to your project, import the TCI
 *  file, and link AETLIB. No app src code mods are necessary.
 * 
 *  Note - to actually determine how much stack your functions use
 *  run call_graph.pl from 
 *  https://www-a.ti.com/downloads/sds_support/applications_packages/index.htm
 */
#include <std.h>
#include <hook.h>
#include <log.h>
#include <c64.h>

#include <aet.h>           // include AETLIB target library

#include <c6x.h>           // needed for TCU_CNTL for AET RTOS interrupt


// if we get within this range from top of stack its 'dangerous'!
#define STACK_RANGE_CHECK          (0x10)
#define STACK_TRAP_HWIVEC_USED     (4)

// function called by ISR to trap on stack overflow
Void stackOverflowTrap();

extern LOG_Obj trace;

/* Data Watchpoint Structure */
static AET_jobParams params;
#ifdef TRACE
static AET_jobParams traceParams;
#endif

/*
 *  aetInitHookFxn - to be plugged into BIOS HOOK init fxn
 *  sets up AET resources and non-changing parameters
 */
/* ARGSUSED - get rid of remark since we know we dont need arg id */
Void aetInitHookFxn(HOOK_Id id) 
{
   /* Initialize AET */
   AET_init();

   /* Claim the AET resource */
   if (AET_claim())
      return;
  
   /* 
    *  refSize determines distance from the given addr that will gen a trigger
    *  e.g. 
    *  Address = 0x80010000, Reference Size = Byte
    *  Trigger on Read/Write access to byte address 0x80010000 only
    *  Address = 0x80010000, Reference Size = Halfword
    *  Trigger on Read/Write access to byte addresses 0x80010000-0x80010001
    *  Address = 0x80010000, Reference Size = Word
    *  Trigger on Read/Write access to byte addresses 0x80010000-0x80010003
    *  Address = 0x80010000, Reference Size = Double Word
    *  Trigger on Read/Write access to byte addresses 0x80010000-0x80010007
    */
   params = AET_JOBPARAMS;
   params.triggerType = AET_TRIG_AINT;   // trigger an ISR function
   params.readWrite = AET_WATCH_WRITE;
   params.refSize = AET_REF_SIZE_BYTE;

   #ifdef TRACE
	traceParams.programAddress = (Uint32) &C64_enableIER;
	traceParams.traceTriggers = AET_TRACE_PA;
	traceParams.traceActive = AET_TRACE_ACTIVE;


	/* Set up the desired job */
	if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &traceParams))
		return;

	#endif
   /* Enable AET */
   if (AET_enable()) {
      return;
   }

   // Enables the AET RTOS Interrupt in the Execution Control Unit
   AET_CLEAR_RTOS_INT();
   AET_ENABLE_RTOS_INT();

   // enable ISR HWIINTn we use to trap stack overflow detections
   C64_enableIER( 1 << STACK_TRAP_HWIVEC_USED );
}


/*
 *  aetSwitchHookFxn - to be plugged into BIOS HOOK switch fxn
 *  Reprograms AET watchpoint to new/next TSK Stack address range
 *  
 *  If stack is too small AET will trigger the AET interrupt 
 *  we have plugged, where we can 'handle' the stack overflow
 *
 *  If you double the stack-size of the problematic TSK you'll
 *  observe that the trap ISR function never gets called.
 */
/* ARGSUSED - get rid of remark since we know we dont need arg currTask */
Void aetSwitchHookFxn(TSK_Handle currTask, TSK_Handle nextTask)
{
   Ptr tskTopStackPtr = nextTask->stack;
   Ptr tskTopStackMarker = (Ptr)((Uint32)tskTopStackPtr + STACK_RANGE_CHECK);
   AET_error stat;
   static Int first = TRUE;


   /* Clear prev AET Job except 1st time since must first setup job */
   if (first) {
      AET_CLEAR_RTOS_INT();
	  AET_ENABLE_RTOS_INT();
      first = FALSE;
   } else {
      AET_releaseJob(params.jobIndex);    
   }
    
   // setup for next task's top of stack marker range
   params.dataStartAddress = (Uint32) tskTopStackPtr;
   params.dataEndAddress =  (Uint32) tskTopStackMarker;


   /* Setup the AET job to trap on next task stack overflow */
   stat = AET_setupJob(AET_JOB_TRIG_ON_DATA_RANGE, &params);
   if (stat != AET_SOK) {
      return;
   }

}


/*
 *  stackOverflowTrap - HWI ISR function triggered if we access
 *  'dangerous' range. This is more useful than simply halting CPU since 
 *  we can take programmatic action. In this test we just LOG_printf
 */
Void stackOverflowTrap()
{
   LOG_printf(&trace, "WARNING - stack overflow imminent!");
   LOG_disable(&trace);
   AET_CLEAR_RTOS_INT();     // enable re-entering of this ISR fxn
}


