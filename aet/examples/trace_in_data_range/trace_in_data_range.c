/*
 * trace_in_data_range.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =========================trace_in_data_range.c=============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the Trace In Data Range functionality of the
 * AET Target Library.  The Trace trigger is active when reads or writes occur
 * between two specified addresses.  In this case, we will create an array of 
 * 32 bit values and trace whent he top half of the array is written to / read
 * from
 */
 
#include <aet.h>
#include <stdio.h>

#define sizeOfBigArray 0x80
/* Function Prototypes */
void func1();

volatile Uint32 bigArray[sizeOfBigArray];
volatile Uint32 temp;

void main()
{
	/* Declare the Parameters Structure */
	AET_jobParams params;

	/* Declare variables for both Jobs */
	AET_jobIndex jobIndex;

	params = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	/* Set the parameters structure for the first job. */
	params.traceActive = AET_TRACE_ACTIVE;
	params.traceStartAddress = (Uint32) &bigArray[0];
	params.traceEndAddress = (Uint32) &bigArray[sizeOfBigArray/2];
	params.readWrite = AET_WATCH_WRITE;
	params.traceTriggers = AET_TRACE_PCTAG|AET_TRACE_WA|AET_TRACE_RA|AET_TRACE_RD|AET_TRACE_WD;


	/* Initialize AET */
	AET_init();
	
	/* Claim the AET resource */ 
	if (AET_claim())
		return; 
	
//	AET_traceConfig(0xF, 0x77, 1, AET_TRACE_CACHE_PROFILING_NORMAL);
	

	/* Set up the Trace Start job */
	if (AET_setupJob(AET_JOB_TRACE_IN_DATA_RANGE, &params))
		return;

	jobIndex = params.jobIndex;

	printf("The Start Job index is %d\n", jobIndex);


	/* Enable AET */
	if (AET_enable())
		return;

	/* Call Func 1*/
	func1();


	/* Release the first job.  Switch to Reads, and do it again */
	AET_releaseJob(jobIndex);
	params.readWrite=AET_WATCH_READ;

	/* Set up the Trace Start job */
	if (AET_setupJob(AET_JOB_TRACE_IN_DATA_RANGE, &params))
		return;

	jobIndex = params.jobIndex;

	printf("The Start Job index is %d\n", jobIndex);

	/* Call Func 1*/
	func1();

	AET_releaseJob(jobIndex);
	AET_release();

}

void func1()
{	
	int i,j;


	for (i=0; i<sizeOfBigArray; i++)
	{
		/* Write values forward to bigArray */
/*		bigArray[i] = (2 * i * i) + 4; */
		bigArray[i] = i;

		for (j=0; j<sizeOfBigArray; j++)
		{
			asm (" nop");
		}
	}

	for (i=sizeOfBigArray-1; i>=0; i--)
	{
		/* Write values backward to bigArray */
/*		bigArray[i] = (4 * i) - 3; */
		bigArray[i] = i;

		for (j=0; j<100; j++)
		{
			asm (" nop");
		}
		

	}

	for (i=0; i<sizeOfBigArray; i++)
	{
		/* Read values forward from bigArray */
		temp = bigArray[i];

		for (j=0; j<100; j++)
		{
			asm (" nop");
		}
	}


	for (i=sizeOfBigArray-1; i>=0; i--)
	{
		/* Read values forward from bigArray */
		temp = bigArray[i];

		for (j=0; j<100; j++)
		{
			asm (" nop");
		}
	}

}


