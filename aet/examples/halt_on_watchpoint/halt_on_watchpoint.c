/*
 * halt_on_watchpoint.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =============================halt_on_watchpoint.c==========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */


/*  ===========================================================================
 *  ==                              OVERVIEW                                 ==
 *  ===========================================================================
 *
 *  This application demonstrates the functionality of a the Halt on Program 
 *  Watchpoint of the AET Target Library.  The watchpoint is triggered on 
 *  encountering a specific PC value.  In this example, the PC value is the 
 *  the address of the test_func2() function.  Encountering this address in the 
 *  PC will cause the watchpoint to be triggered.  AET breakpoints are
 *  always imprecise because they execute when the instruction is retired from 
 *  the CPU pipeline.The delay should be in the neighborhood of 6-9 cycles. 
 ******************************************************************************/

#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void test_func1();
void test_func2();

void main()
{
	AET_jobParams params; /* AET parameters */
	AET_jobIndex jobNumber;				/* Index in job table */

	/*
	 Initialize Parameter Structure
	 */
	params = AET_JOBPARAMS;

	/* 
	 Set the Watchpoint to halt when it encounters 
	 test_func2.  There can be a few cycles of delay
	 when using a watchpoint of this kind.  But it 
	 should halt within the test_func2 scope.
	 */
	params = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	params.programAddress = (Uint32) &test_func2;
	params.triggerType = AET_TRIG_HALT_CPU;
	
	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Setup the AET job */
	if (AET_setupJob(AET_JOB_TRIG_ON_PC, &params))
		return;
	
	/* Remember the index of this job */
	jobNumber = params.jobIndex;

	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	/* Call test_func1, and we should halt in test_func2() */
	test_func1();

	/* Clear the job that we've programmed */
	AET_releaseJob(jobNumber);

	return;


}

void test_func1()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	test_func2();
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");

}

void test_func2()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
}
