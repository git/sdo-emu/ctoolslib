/*
 * hooks_profiling_impl.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//----------------------------------------------------------------------------
// HOOKS_PROFILING_IMPL.C
//----------------------------------------------------------------------------
#include "hooks_profiling_hdr.h"

//----------------------------------------------------------------------------
// Define the global written to when the thread changes.  Placed in
// a section named ".far:hooks_profiling_watch" so it can be specially
// allocated in the link command file, if desired.  If not so allocated,
// it becomes yet another piece of the .far section.  If the user builds
// with --mem_model:data=near then they will have to deal with this unexpected
// piece of far data.
//----------------------------------------------------------------------------
#pragma DATA_SECTION(hooks_profiling_watch_thread, ".far:hooks_profiling_watch")
far uintptr_t hooks_profiling_watch_thread;

//----------------------------------------------------------------------------
// Avoid cut-n-paste of the hook functions from the header file to here.  So,
// if optimization is not being used (i.e. _INLINE is off), include hooks_
// profiling_hdr.h a 2nd time so ordinary out-of-line hook functions are
// compiled into this file.  The header file exposes the source to the hook
// functions, even after multiple inclusion, if HOOKS_PROFILING_IMPLEMENTATION
// is defined.
//----------------------------------------------------------------------------
#if !defined(_INLINE)
#define HOOKS_PROFILING_IMPLEMENTATION
#include "hooks_profiling_hdr.h"

//----------------------------------------------------------------------------
// In general, if this file is compiled the same as all the others, then
// it will work.  However, it is possible to get in trouble in this one
// scenario:  If some of the code using hook functions is compiled with
// optimization OFF, and this file is compiled with optimization ON, then no
// definition of the hook functions is available and you get an error at link
// time about undefined symbols.  You can never get in trouble compiling this
// file with optimization OFF.  
//----------------------------------------------------------------------------
// The best way to build this file is with no optimization and -mo.  The
// -mo switch puts each function in its own subsection.  If those functions
// are never called, then the linker removes them.
//----------------------------------------------------------------------------
#else
#warn Building hooks_profiling_impl.c with optimization may cause errors
#warn at link time.  See comments in this file for more detail.
#endif

//----------------------------------------------------------------------------
// HOOKS_PROFILING_INIT
//----------------------------------------------------------------------------
#include <aet.h>
#include <aet_dynamic_call_graph.h>

#pragma NO_HOOKS(hooks_profiling_init)
void hooks_profiling_init()
{

   
}

//--------------------------------------------------------------------------
// These asm's put an entry into the .pinit section.  The .pinit section is
// a list of functions called at boot time, i.e. before main runs.  It is
// ostensibly for C++ global constructors.  But it works well here, too.
// This at the end of the file to avoid messing up any other section.
//--------------------------------------------------------------------------
asm(" .sect .pinit");
asm(" .word _hooks_profiling_init");
