/*
 * aet_dynamic_callgraph.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ==========================aet_dynamic_call_graph.c==========================
 * Sets up AETLIB programming for statistical profiling analysis for trace to 
 * capture PC and timing information at a specified interval.  
 *
 *  Revision Information
 *   Changed: $LastChangedDate$
 *   Revision: $LastChangedRevision$
 */
#if defined(_TMS320C6X)
  #if __TI_COMPILER_VERSION__ < 6001000
  #error "Must use C6000 Compiler Version 6.1.x or higher"
  #endif
#endif

#include <std.h>
#include <aet.h>
#include <aet_dynamic_call_graph.h>

/*
 * ========== aetStartDynamicCallGraph ==========
 */
AET_error aetStartDynamicCallGraph(Uint32 threadAddress)
{
	AET_error status = AET_SOK;
	AET_jobParams params;

	/* 
	Uncomment this declaration if using jobindex
	*/
	/*
	AET_jobIndex jobIndex;
	*/

	/* Initialize Job Parameter Structure */
	params = AET_JOBPARAMS; 

	/* Set the one parameter that we need */
	params.dataAddress = threadAddress;
	params.eventNumber[0]= AET_EVT_MISC_MARK_INS_0;
	params.eventNumber[1]= AET_EVT_MISC_MARK_INS_1;


	/* Initialize AET */
	AET_init();
	
	/* Claim the AET resource */
	if (status = AET_claim())
		return status;

	/* Set up job to trace the Mark Instrunctions */
	if (status = AET_setupJob(AET_JOB_FUNC_PROFILE, &params))
		return status;

/* Uncomment if job index is necessary.  We don't need it here because
   we will likely never want to stop the dynamic callgraph
*/

/* 	jobIndex = params.jobIndex; */


	/* Enable AET */
	status = AET_enable();

	return status;
}





