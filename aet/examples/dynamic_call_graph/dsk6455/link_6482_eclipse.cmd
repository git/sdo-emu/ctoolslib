/******************************************************************************/
/*  link_6482.cmd                                                             */
/*  Copyright (c) 1997-2007  Texas Instruments Incorporated                   */
/* Automated Revision Information                                             */
/*  Changed: $LastChangedDate: 2007-07-17 09:43:11 -0500 (Tue, 17 Jul 2007) $ */
/*  Revision: $LastChangedRevision: 3347 $                                    */
/******************************************************************************/
-stack 0x800
-heap 0x800

-l rts64plus.lib
-l aetlib.l64P

MEMORY
{
   L1D:		o=0x00f00000 l=0x00008000
   L1P:		o=0x00e00000 l=0x00008000
   L2:      o=0x00800000 l=0x001F0000
   L2A:  	o=0x009F0000 l=0x00010000
   RESMEM:	o=0xE0000000 l=0x01000000
}
   
SECTIONS
{ 
  .bss        > L2
  .far        > L2
  .text       > L2
  .cinit      > L2
  .const      > L2
  .stack      > L2
  .cio		  > L2
  .sysmem	  > L2
  .switch	  > L2
  .pinit 	  > L2
}