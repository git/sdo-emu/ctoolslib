/*
 * interrupt_stall_duration.c
 *
 * AET Library public API Definitions
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <aet.h>
#include <stdio.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
  This project is an example application, which shows how to use
  AET_JOB_INTR_STALL_DURATION AETLib feature. A brief description of this
  feature is as follows:
  An AET interrupt will be generated upon detecting a CPU pipeline stall
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) extending beyond a user specified
  number of cycles. Specifically, the AET is programmed to do the following:
  - decrement counter0 on each cycle during a pipeline stall, AND
  - when the counter0 reaches zero (counter0 overflow event)
    -- generate AET interrupt
    -- transition from state0 to state1
    -- reload counter1
  - decrement counter1 on (each cycle during a pipeline stall && state1 is true)
  - when pipeline stall is de-asserted
    -- reload counter0
    -- transition from state1 to state0

  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_INTR_STALL_DURATION
  job is setup to generate an AET interrupt, whenever the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles) cycles. AET counter 1 is used to
  count the number of stalled cycles above the STALL_CYCLE_THRESHOLD. The total duration
  of a stall whose duration is greater than STALL_CYCLE_THRESHOLD = STALL_CYCLE_THRESHOLD +
  read AET counter 1 value. The worst case pipeline stall
  in this example occurs, when the application code tries to read a variable stored in
  DDR3 memory. This example provides a count of number of times CPU pipeline stalled for more
  than STALL_CYCLE_THRESHOLD cycles and the duration of these stalls.

  CtoolsLib Dependencies: This example requires latest version of DSPTraceLib and ETBLib
  Platform RTSC dependencies: For Keystone1: ti.platforms.evm6670 RTSC package and for
                              Keystone2: ti.platforms.evmTCI6636K2H RTSC package

 *  ============================================================================
 */

/**************** CAUTION: Please Read ****************************************/
//STALL_CYCLE_THRESHOLD should always be greater than the maximum pipeline stall
//cycle period encountered while servicing the AET RTOS interrupt, otherwise the
//AET RTOS interrupt will be generated in an infinite loop, resulting in control
//never been returned back to Application code
#define STALL_CYCLE_THRESHOLD 100
#define AET_RTOS_INTR_VEC_NUM 5
#define C66X_COREPAC_AET_RTOS_EVENT 9

Hwi_Handle AetRtosIntrHwi; //HWI handle for AET RTOS interrupt

//count specifying the number of times the stall cycle threshold is exceeded by the
//instrumented application
uint32_t exceed_stall_threshold_count = 0;

#define STALL_DURATION_ARRAY_SIZE 1024
//Array for storing stall duration values
uint32_t stall_duration_values[STALL_DURATION_ARRAY_SIZE];

/*******************************************************************************
 * FUNCTIONS AND PARAMETERS USED FOR EXAMPLE APPLICATION
 */

#define N_APP_FUNCTION_RUNS 100

#pragma DATA_SECTION(globalCntr,"APPSW_DDR3");
uint32_t globalCntr[10] = {0, };
#pragma DATA_SECTION(globalMarker,"APPSW_DDR3");
uint32_t globalMarker = 0;

void endFunction()
{
	volatile int32_t dummy1 =10, dummy2=0;

	dummy2 = dummy1 - 1;

	//generate cache misses - read/write from DDR
    globalMarker = 10;

    *(unsigned int *)(0x80010000) = 10;
    *(unsigned int *)(0x80020000) = 10;
    *(unsigned int *)(0x80030000) = 10;
    *(unsigned int *)(0x80040000) = 10;
    *(unsigned int *)(0x80050000) = 10;
    *(unsigned int *)(0x80060000) = 10;
    *(unsigned int *)(0x80070000) = 10;
    *(unsigned int *)(0x80080000) = 10;
    *(unsigned int *)(0x80090000) = 10;
    *(unsigned int *)(0x800A0000) = 10;

    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;

    globalMarker = 20;

    return;
}

void startFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;

    globalCntr[8]++;

    return;
}


void appFunction()
{
   volatile int32_t dummy1 =100, dummy2=0;

   dummy2 = dummy1 - 1;

   while(dummy2 > 1)
   {
      dummy2--;
        if(dummy2 & 1)
        {
            endFunction();
        }
        if(dummy2 & 0x4)
        {
            startFunction();
        }
   }

}

void runFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;
}

void doneFunction(void)
{
    volatile uint32_t temp;
}

AET_jobIndex AetStallDurationJobIndex;
/*
 *  ======== taskFxn ========
 */
void taskFxn(UArg a0, UArg a1)
{
	unsigned int idx;
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
	AET_counterConfigParams counter1 = AET_COUNTERCONFIGPARAMS;
	AET_jobParams AetStallDurationJobParams = AET_JOBPARAMS;

    //Clear AET interrupt flag in TCU_CNTL register
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	//Enable AET interrupt in TCU_CNTL register
    if((TCU_CNTL & 0x00020002) != 0x00020002)
    {
        TCU_CNTL = 0x1;
        TCU_CNTL = 0x000A0000;
        TCU_CNTL = 0x00020002;
    }

	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set the counter0 configuration parameters */
	counter0.configMode = AET_COUNTER_TRAD_COUNTER;
	counter0.counterNumber = AET_CNT_0;
	counter0.reloadValue = STALL_CYCLE_THRESHOLD;

	/* Configure the counter0 */
	AET_configCounter(&counter0);

	/* Set the counter1 configuration parameters */
	counter1.configMode = AET_COUNTER_TRAD_COUNTER;
	counter1.counterNumber = AET_CNT_1;
	counter1.reloadValue = 0xFFFFFFFF;

	/* Configure counter1 */
	AET_configCounter(&counter1);

	/* Setup Interrupt stall duration AET job parameters */
	AetStallDurationJobParams.eventNumber[0] = AET_EVT_MISC_STALL_PIPELINE;

	/* Program Interrupt stall duration AET job */
	if (AET_setupJob(AET_JOB_INTR_STALL_DURATION, &AetStallDurationJobParams))
		return;

	AetStallDurationJobIndex = AetStallDurationJobParams.jobIndex;

	printf("AET Interrupt Stall duration job index is %d\n", AetStallDurationJobIndex);

	/* Enable AET */
	if ( AET_enable())
		return;

	runFunction();

    for(idx = 0; idx < N_APP_FUNCTION_RUNS; idx++)
    {
        appFunction();
    }

    doneFunction();

    printf("App Code execution completed\n");

	AET_releaseJob(AetStallDurationJobIndex);
	AET_release();

	printf("No of times CPU pipeline stalled for more than %d cycles: %d\n",STALL_CYCLE_THRESHOLD,exceed_stall_threshold_count);

	//print stall duration values
	uint32_t stall_duration_entries_count;

	if(exceed_stall_threshold_count > STALL_DURATION_ARRAY_SIZE)
	{
		stall_duration_entries_count = STALL_DURATION_ARRAY_SIZE;
	}
	else
	{
		stall_duration_entries_count = exceed_stall_threshold_count;
	}

	for(idx=0; idx<stall_duration_entries_count; idx++)
	{
		printf("CPU stall#%d, stalled duration: %d cycles\n",idx+1,stall_duration_values[idx]);
	}

	return;
}

//AET RTOS interrupt service routine
void AetRtosIsr(UArg arg)
{
	//Increment count for exceeding stall threshold
	exceed_stall_threshold_count++;

	//Determine stall duration
	stall_duration_values[exceed_stall_threshold_count-1] = STALL_CYCLE_THRESHOLD + AET_readCounter(AET_CNT_1);

	Hwi_clearInterrupt(AET_RTOS_INTR_VEC_NUM);

	//Please Note: comment the below line of code if the stall threshold
	//is less than the max period of pipeline stall during the AET interrupt
	//context switch and AET interrupt servicing code (non app code), otherwise
	//the AET RTOS interrupt will always be triggered in an infinite loop, without
	//control passing to the application code

	//If AET interrupt flag is not cleared, this interrupt will not be generated
	//again and again

	//Clear AET interrupt flag in TCU_CNTL register to re-arm the AET interrupt
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	return;
}

/*
 *  ======== main ========
 */
int main()
{ 
    Task_Handle task;
    Error_Block eb;

    Error_init(&eb);
    task = Task_create(taskFxn, NULL, &eb);
    if (task == NULL) {
        BIOS_exit(0);
    }

    //Clear AET RTOS interrupt at startup
    Hwi_clearInterrupt(AET_RTOS_INTR_VEC_NUM);

    Hwi_Params AetRtosIntr_hwiParams;

    Hwi_Params_init(&AetRtosIntr_hwiParams);
    Error_init(&eb);

    // set the event id of the peripheral assigned to this interrupt
    AetRtosIntr_hwiParams.eventId = C66X_COREPAC_AET_RTOS_EVENT;

    // Configure interrupt 5 to invoke "AetRtosIsr".

    AetRtosIntrHwi = Hwi_create(AET_RTOS_INTR_VEC_NUM, AetRtosIsr, &AetRtosIntr_hwiParams, &eb);

    Hwi_enable();

    BIOS_start();    /* does not return */
    return(0);
}
