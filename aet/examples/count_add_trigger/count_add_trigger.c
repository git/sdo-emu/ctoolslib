/*
 * count_add_trigger.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =============================count_add-trigger.c==========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2011-06-02 13:32:35 -0400 (Thu, 02 Jun 2011) $
 *   Revision: $LastChangedRevision: 10244 $
 */


/*  ===========================================================================
 *  ==                              OVERVIEW                                 ==
 *  ===========================================================================
 *
 *  This application demonstrates the functionality of a the Add Trigger
 *  Function of the AET Target Library.  Initially, Counter 0 is triggered to
 *  increment whenever a Mark 0 instruction is encountered.  After the mark loop
 *  is called 1000 times, we execute some AET code to add a Counter 1 trigger to
 *  the existing job.  So, each counter increments each time a Mark 0 is
 *  executed.  The output shows that initially, Counter 0 is incrementing and
 *  Counter 1 remains at 0.  Then, both counters begin incrementing.  The
 *  diffference in the count values is measured and when both are running, it
 *  can be seen to be constant.
 ******************************************************************************/

#include <aet.h>
#include <_aet.h>


/* Function Prototypes */
void markloop();
void printstatus();

void main()
{
	/* Declare the Parameters for the AET Job */
	AET_jobParams params = AET_JOBPARAMS;

	/*
	 * Declare the Parameters for the counters.  We will use
	 * both counters here
	 */
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
	AET_counterConfigParams counter1 = AET_COUNTERCONFIGPARAMS;

	AET_jobIndex jobNumber;				/* Index in job table */

	/* Set the configuration parameters */
	counter0.configMode = AET_COUNTER_TRAD_CONTINUOUS;
	counter0.counterNumber = AET_CNT_0;
	counter0.reloadValue = 0xFFFFFFFF; 	/* This is not really necessary in this
											mode.  Reload has no meaning in
											traditional mode
										 */

	counter1.configMode = AET_COUNTER_TRAD_CONTINUOUS;
	counter1.counterNumber = AET_CNT_1;
	counter1.reloadValue = 0xFFFFFFFF; 	/* This is not really necessary in this
											mode.  Reload has no meaning in
											traditional mode
										 */

	uint16_t loopcounter;

	/*
	 Initialize Parameter Structure
	 */
	params.counterNumber = AET_CNT_0;
	params.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;
	params.triggerType = AET_TRIG_CNT0_START;


	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Configure the counters */
	AET_configCounter(&counter0);
	AET_configCounter(&counter1);

	
	/* Program the counter for the event */
	if (AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &params))
		return;


	/* Remember the index of this job */
	jobNumber = params.jobIndex;

	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	for (loopcounter =0; loopcounter < 10000; loopcounter++)
	{
		/*
		 * On the 10th Loop, add the trigger for counter 2.  From this point on,
		 * the counts between the two counters should differ always by 1000.
		 */
		if (loopcounter == 1000 ){
			params.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;
			params.jobIndex = jobNumber;
			params.triggerType = AET_TRIG_CNT1_START;
			if (AET_setupJob(AET_JOB_ADD_TRIGGER, &params))
				return;

			// Re-Enable AET
			AET_enable();
		}
		markloop();

		if ((loopcounter % 100) == 0){
			printstatus();
		}



	}


	/* Clear the job that we've programmed */
	AET_releaseJob(jobNumber);

	return;


}

void markloop()
{
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" MARK 0");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" MARK 1");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");
	asm (" nop");

}

void printstatus(){
	uint32_t counter_0_value, counter_1_value;
	counter_0_value = AET_readCounter(AET_CNT_0);
	counter_1_value = AET_readCounter(AET_CNT_1);

	printf("Counter 0: %d Counter 1: %d - Difference: %d\n",
				counter_0_value,
				counter_1_value,
				counter_0_value - counter_1_value
			);

}
