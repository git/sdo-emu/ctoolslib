/*
 * trace_on_pc.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ================================trace_on_pc.c==============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the functionality of a the Trace on PC 
 * function of the AET Target Library.  The Trace start job is triggered upon
 * encountering a specific PC value. Trace will continue throughout the rest
 * of the application. 
 ******************************************************************************/
#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void func1();
void func2();

void main()
{
	AET_jobParams params;
	AET_jobIndex jobNumber;

	params = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	params.programAddress = (Uint32) &func2;
	params.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;
	params.traceActive = AET_TRACE_ACTIVE;
 
	AET_init();
	
	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set up the desired job */
	if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &params))
		return;

	jobNumber = params.jobIndex;

	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	/* Call Func1 (which calls Func2 and starts Trace) */
	func1();
	func1();
	func1();
}

void func1()
{
	while (1)
	{
		func2();
	}
}

void func2()
{
	int i = 0;
	int j = 0;

	for (i=0; i<100; i++)
	{
		j += 1;
	}

	i++;
}


