/*
 * aet_wm_intlat.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ========================= aet_wm_intlat.c =================================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 *  ===========================================================================
 *
 *  Sets up AETLIB programming for a watermark counter
 *  to store the worst-case interrupt latency by 
 *  opening the 'window' on AEGEVT0 tied to the CPU Timer 
 *  Interrupt, and closingt the window on the 1st instruction
 *  of the ISR itself.
 *
 */

#include <std.h>

#include <aet.h>


/*
 *  ======== aetWatermarkIntLat ========
 */
AET_error aetWatermarkIntLat( Uint32 isrStartPc )
{
    AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
    AET_error stat = AET_SOK;
    AET_jobParams jobParams;

    /* Initialize AET */
    AET_init();

    /* Claim the AET resource */
    stat = AET_claim();
    if (stat != AET_SOK) {
        return stat;
    }

    /* 
     *  Set the watermark ctr configuration parameters.
     *  Continuous mode means to count cycles when the watermark window is valid
     *  We use AET counter 0 (arbitrarily).
     *  We set the reload value to be the Maximum (0xFFFFFFFF)
     */
    counter0.configMode = AET_COUNTER_MAXWM_CONTINUOUS;
    counter0.counterNumber = AET_CNT_0;
    counter0.reloadValue = 0xFFFFFFFF; 

    AET_configCounter(&counter0);

    /* 
     *  Create AET job to kickstart the counter.  
     *  In this case, we specify to trigger on PC.
     *  Can use some function that you know is going to be executed, or just 
     *  some PC address that you know will be executed.  
     *  This is due to present AETLIB limitation that cant trigger on event, 
     *  so the timer could be started on the interrupt event we're monitoring
     */
    jobParams = AET_JOBPARAMS;
    jobParams.programAddress = isrStartPc;
    jobParams.triggerType = AET_TRIG_CNT0_START;

    stat = AET_setupJob(AET_JOB_TRIG_ON_PC, &jobParams);
    if (stat != AET_SOK) {
        return stat;
    }

	/* 
	 *  use a Watermark Start/End job to start the watermark window.  
	 *  At present we also have to manually configure (via e.g. GEL or Memory 
	 *  Window) the GEM interrupt controller so that AEGEVT0 points to the 
	 *  interrupt that we're interested in (e.g. INT14 timer ISR)
	 *  We then tell AETLIB Watermark Counter 0 to open watermark window on 
	 *  AEGEVT0.
	 */
    jobParams = AET_JOBPARAMS;
    jobParams.counterNumber = AET_CNT_0;
    jobParams.eventNumber[0] = AET_GEM_CPUINT_14;
    jobParams.startStop = AET_WATERMARK_START;

    stat = AET_setupJob(AET_JOB_WATERMARK_START_END, &jobParams);
    if (stat != AET_SOK) {
        return stat;
    }

	/* 
	 *  configure the watermark close event.  
	 *  We do this with a typical Trigger on PC job.  
	 *  Summarized, we are generating a WM0STOP trigger 
	 *  when we encounter the specified PC of first intruction of ISR
	 */
    jobParams = AET_JOBPARAMS;
    jobParams.counterNumber = AET_CNT_0;
    jobParams.programAddress = isrStartPc;
    jobParams.triggerType = AET_TRIG_WM0STOP;

    stat = AET_setupJob(AET_JOB_TRIG_ON_PC, &jobParams);
    if (stat != AET_SOK) {
        return stat;
    }

    /* Enable AET */
    stat = AET_enable();
    if (stat != AET_SOK) {
        return stat;
    }

    return AET_SOK;

}


