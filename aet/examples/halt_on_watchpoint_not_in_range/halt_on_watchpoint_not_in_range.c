/*
 * halt_on_watchpoint_not_in_range.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =======================halt_on_watchpoint_not_in_range.c===================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the functionality of a the Halt on Program 
 * Watchpoint outside of a Range of the AET Target Library.  The watchpoint is 
 * triggered on encountering PC values that are outside of a specified range.
 * The trigger is an imprecise breakpoint, so there will be a few cycles before 
 * the halt is seen.  AET breakpoints arealways imprecise because they execute 
 * when the instruction is retired from the CPU pipeline.The delay should 
 * be in the neighborhood of 7-9 cycles. 
 ******************************************************************************/
#include <aet.h>
#include <stdio.h>

#pragma CODE_SECTION(test_func1, ".text:func1")
#pragma CODE_SECTION(test_func2, ".text:func2")
extern far int PROTECTED_MEM_START;

/* Function Prototypes */
 void test_func1();
 void test_func2();
 void dummyFunc();



void main()
{

	AET_jobParams params;
	AET_jobIndex jobNumber;

	/*
	 Initialize Parameter Structure
	 */
	params = AET_JOBPARAMS;

	/* 
	 Set the Watchpoint to halt when it encounters 
	 a program address outside of 0x0 - 0x900000.
	 There can be a few cycles of delay
	 when using a watchpoint of this kind.  But it 
	 should halt within the test_func1 or test_func2
	 scope.
	 */

	params.programRangeStartAddress = 0;
	params.programRangeEndAddress = (int)&PROTECTED_MEM_START;
	params.triggerType = AET_TRIG_HALT_CPU;
	params.logicOrientation = AET_TRIG_LOGIC_INVERTED;
	
	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

	if (AET_setupJob(AET_JOB_TRIG_ON_PC_RANGE, &params))
		return;

	jobNumber = params.jobIndex;

	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	/* Call test_func1, and we should halt in test_func2() */
	test_func1();

	/* Clear the job that we've programmed */
	AET_releaseJob(jobNumber);

	asm("_dummyFuncStop:"); /* Mark the address of the end of main */

	return;

}


void test_func1()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	test_func2();
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");

}

void test_func2()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
}
