/*
 * statistical_profiling.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ===========================statistical_profiling.c=========================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates how statistical profiling works.  There are a
 * number of functions that consume a various number of cycles.
 ******************************************************************************/
#include <aet.h>
#include <aet_stat_profile.h>
#include <stdio.h>


#define sampleInterval 34567

extern void func5000();

/* Function Prototypes */
void func500();
void func1500();
void func40000();
void func80000();


void main()
{
	
	aetStatProfileStart(sampleInterval);
	while(1)
	{
		func500();
	}
	//aetStatProfileEnd();
}

void func500()
{
	int i;
	for (i=0; i<500; i++)
	{
		asm(" nop");
	}
	func1500();
}


void func1500()
{
	int i;
	for (i=0; i<1500; i++)
	{
		asm(" nop");
	}

	/* Don't profile func80000() */
	aetStatProfileStop();
	func80000();
	aetStatProfileStart(sampleInterval);

	func5000();
	func40000();
}


void func40000()
{
	int i;
	for (i=0; i<40000; i++)
	{
		asm(" nop");
	}
}

void func80000()
{
	int i;
	for (i=0; i<80000; i++)
	{
		asm(" nop");
	}
}
