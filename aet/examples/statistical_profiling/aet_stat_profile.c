/*
 * aet_stat_profile.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================aet_stat_profile.c==============================
 * Sets up AETLIB programming for statistical profiling analysis for trace to 
 * capture PC and timing information at a specified interval.  
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

#include <aet.h>
#include <aet_stat_profile.h>

static 	AET_jobIndex startJobIndex;
static 	AET_jobIndex triggerJobIndex;
static int profileStarted = 0;

/*
 * ========== aetStatProfileStart ==========
 */
AET_error aetStatProfileStart( Uint32 cycleInterval )
{
	AET_error status = AET_SOK;
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
	AET_jobParams triggerJobParams = AET_JOBPARAMS;

	if (profileStarted == 0)
	{
		/* Initialize AET */
		AET_init();

		/* Claim the AET Resource*/
		status = AET_claim();
		if (status != AET_SOK) {
			return status;
		}

		/* Set the counter configuration parameters */
		counter0.configMode = AET_COUNTER_TRAD_COUNTER;
		counter0.counterNumber = AET_CNT_0;
		counter0.reloadValue = cycleInterval;

		AET_configCounter(&counter0);


		/*
		 Now set up the parameters for the trace trigger job
		 */
		triggerJobParams.traceTriggers = AET_TRACE_PA | AET_TRACE_TIMING;
		triggerJobParams.counterNumber = AET_CNT_0;
		
		/* Set up the trace trigger job */
		if (status = AET_setupJob(AET_JOB_TRIG_ON_TIMER_ZERO, &triggerJobParams)){
			return status;
		}

		triggerJobIndex = triggerJobParams.jobIndex;

		status = AET_enable();

		profileStarted = 1;
	}
	else
	{
		status = AET_enable();
	}
	return status;

}

AET_error aetStatProfileStop()
{
	AET_error status = AET_SOK;

	status = AET_disable();

	return status;
}

AET_error aetStatProfileEnd()
{
	AET_error status = AET_SOK;

	AET_releaseJob(startJobIndex);
	AET_releaseJob(triggerJobIndex);
	AET_release();

	profileStarted = 0;
	return status;
}


