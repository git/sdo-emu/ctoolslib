/*
 * halt_on_data_with_value.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =============================halt_on_data_with_value=======================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */


/*  ===========================================================================
 *  ==                              OVERVIEW                                 ==
 *  ===========================================================================
 *
 *  This Example demonstrates the programmation of an AET generated hardware
 *  watchpoint when a read or write occurs to a specific location, with the 
 *  qualification of a specific value.  In this example, the hardware watches 
 *  for a write to the memory location occupied by memVariable1, with the 
 *  specific value of 0xF0F0F0F0.  Any other write to this location will not
 *  trigger a halt.   This halt, generated by the AET hardware, is imprecise
 *  and will show a skid of 7-9 cycles past the location where memVariable1 was
 *  written.
 */

#include <aet.h>
#include <stdio.h>

/* Global Variables */
Uint32 memVariable1, memVariable2;

/* Function Prototypes */
void test_func();

void main()
{
	AET_jobParams params; /* Parameter Structure */
	AET_jobIndex jobNumber;					/* Job Number returned by AETLIB*/

	/*
	 Initialize Parameter Structure
	 */
	params = AET_JOBPARAMS;

	/* 
	 * Set the Watchpoint to halt when a write to MemVariable1 occurs with the 
	 * value 0xF0F0F0F0.  There can be a few cycles of delay when using a
	 * watchpoint of this kind, but the halt should occur a few cycles after 
	 * the write occurs. AET breakpoints are always imprecise because they 
	 * execute when the instruction is retired from the CPU pipeline.The delay
	 * should  be in the neighborhood of 7-9 cycles.
	 */
	 
	params = AET_JOBPARAMS; /* Initialize Job Parameter Structure */

	params.value = 0xF0F0F0F0;
	params.dataAddress = (Uint32) &memVariable1;
	params.triggerType = AET_TRIG_HALT_CPU;
	params.readWrite = AET_WATCH_WRITE;
	params.refSize = AET_REF_SIZE_WORD;
	
	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;
	
	/* Setup the AET job with selected parameters */
	if (AET_setupJob(AET_JOB_TRIG_ON_DATA_WITH_VALUE, &params))
		return;

	/* Remember the index of this job in the job table */
	jobNumber = params.jobIndex;

	/* Tell the user what the job index is */
	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable())
		return;

	test_func();

	/* Clear AET Job */
	AET_releaseJob(jobNumber);
		
	return;


}

void test_func()
{
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	/* Write to memory variable2.  We shouldn't halt here */
	memVariable2 = 0x12345678l;
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	/* Read from memory variable1.  We shouldn't halt here */
	memVariable2 = memVariable1;
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	/* Write to memory variable1 but not correct value.  We shouldn't halt here */
	memVariable1 = 0xBEEFFEED;
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	/* Write to memory variable1 with correct value.  Should trigger a halt */
	memVariable1 = 0xF0F0F0F0;
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
	asm(" nop");
}
