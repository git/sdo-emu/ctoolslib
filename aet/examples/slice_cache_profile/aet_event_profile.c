/*
 * aet_event_profile.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* aet_event_profile.c */
#include <aet.h>
#include <stdio.h>


void aetInitEventProfile()
{
	AET_jobParams params;
	AET_jobIndex jobNumber;

	params = AET_JOBPARAMS;	/* Initialize Job Parameter Structure */

	params.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;
	params.traceActive = AET_TRACE_ACTIVE;
	params.eventNumber[0] = AET_EVT_MEM_L1D_RM_HITS_EXT_CABLE_A;
	params.eventNumber[1] = AET_EVT_MEM_L1D_RM_HITS_EXT_CABLE_B;

	AET_init();

	/* Claim the AET resource */
	if (AET_claim() != AET_SOK)
	{
		return;
	}

	/* Set up the desired job */
	if (AET_setupJob(AET_JOB_TRACE_ON_EVENTS, &params) != AET_SOK)
	{
		return;
	}

	jobNumber = params.jobIndex;
	
	printf("The index is %d\n", jobNumber);

	/* Enable AET */
	if (AET_enable() != AET_SOK)
	{
		return;
	}

	
}

