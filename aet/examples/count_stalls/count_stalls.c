/*
 * count_stalls.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *
 *  ===================event_triggered_timer_start.c===========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */


#include <aet.h>
#include <stdio.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
  This project is an example application, which shows how to use
  AET_JOB_COUNT_STALLS AETLib feature. A brief description of this
  feature is as follows:
  The AET Counter1 will be advanced by 1, upon detecting a CPU pipeline stall
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) 
  extending beyond a user specified number of cycles. Specifically, 
  the AET is programmed to do the following:
  - decrement AET counter0 on each cycle during a pipeline stall, AND
  - reload the AET counter0 when pipeline stall is de-asserted, AND
  - generate trigger (in this case, AET counter1 event) when the AET counter0 reaches zero
  - Advance AET Counter1 by one, whenever the AET counter1 event is triggered

  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_COUNT_STALLS
  job is setup to count (Using AET counter1) the number of times, the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles) cycles.
 *  ============================================================================
 */

#define STALL_CYCLE_THRESHOLD 100

/*******************************************************************************
 * FUNCTIONS AND PARAMETERS USED FOR EXAMPLE APPLICATION
 */

#define N_APP_FUNCTION_RUNS 1000

#pragma DATA_SECTION(globalCntr,"APPSW_DDR3");
uint32_t globalCntr[10] = {0, };
#pragma DATA_SECTION(globalMarker,"APPSW_DDR3");
uint32_t globalMarker = 0;

void endFunction()
{
	volatile int32_t dummy1 =10, dummy2=0;

	dummy2 = dummy1 - 1;

    globalMarker = 10;

    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;

    globalMarker = 20;

    return;
}

void startFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;

    globalCntr[8]++;

    return;
}


void appFunction()
{
   volatile int32_t dummy1 =100, dummy2=0;

   dummy2 = dummy1 - 1;

   while(dummy2 > 1)
   {
      dummy2--;
        if(dummy2 & 1)
        {
            endFunction();
        }
        if(dummy2 & 0x4)
        {
            startFunction();
        }
   }

}

void runFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;
}

void doneFunction(void)
{
    volatile uint32_t temp;
}

AET_jobIndex AetCountStallsJobIndex;

/*
 *  ======== main function ========
 */
void main(void)
{
	unsigned int idx;
	unsigned int counter1_value, exceed_stall_threshold_count;
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
	AET_counterConfigParams counter1 = AET_COUNTERCONFIGPARAMS;
	AET_jobParams AetCountStallsJobParams = AET_JOBPARAMS;

	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set the counter0 configuration parameters */
	counter0.configMode = AET_COUNTER_TRAD_COUNTER;
	counter0.counterNumber = AET_CNT_0;
	counter0.reloadValue = STALL_CYCLE_THRESHOLD;

	/* Configure counter0 */
	AET_configCounter(&counter0);

	/* Set the counter1 configuration parameters */
	counter1.configMode = AET_COUNTER_TRAD_COUNTER;
	counter1.counterNumber = AET_CNT_1;
	counter1.reloadValue = 0xFFFFFFFF;

	/* Configure counter1 */
	AET_configCounter(&counter1);

    counter1_value = AET_readCounter(AET_CNT_1);

    printf("Initial AET Counter1 value: 0x%x\n",counter1_value);

	/* Setup Count Stalls AET job parameters */
	AetCountStallsJobParams.eventNumber[0] = AET_EVT_MISC_STALL_PIPELINE;

	/* Program the counter for the event */
	if (AET_setupJob(AET_JOB_COUNT_STALLS, &AetCountStallsJobParams))
		return;

	AetCountStallsJobIndex = AetCountStallsJobParams.jobIndex;

	printf("AET Count Stalls job index is %d\n", AetCountStallsJobIndex);

	/* Enable AET */
	if ( AET_enable())
		return;

	runFunction();

    for(idx = 0; idx < N_APP_FUNCTION_RUNS; idx++)
    {
        appFunction();
    }

    doneFunction();

    printf("App Code execution completed\n");

    exceed_stall_threshold_count = AET_readCounter(AET_CNT_1);

    printf("Final AET Counter1 value: 0x%x\n",exceed_stall_threshold_count);

	AET_releaseJob(AetCountStallsJobIndex);
	AET_release();

	printf("No of times CPU pipeline stalled for more than %d cycles: %d\n",STALL_CYCLE_THRESHOLD,exceed_stall_threshold_count);

	return;
}
