/*
 * watermark_add_trigger.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =======================watermark_add_trigger.c==============================
 *  Revision Information
 *   Changed: $LastChangedDate: 2011-06-02 13:32:35 -0400 (Thu, 02 Jun 2011) $
 *   Revision: $LastChangedRevision: 10244 $
 */


#include <aet.h>
#include <stdio.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
 *	This example is designed to demonstrate the capability of the AET_addTrigger
 *	job when the added trigger is located on the same trigger builder as the 
 *	original trigger.  Mark 0 events will be used to open the watermark window.
 *	Mark 1 events will be used to end the watermark window.  This test should 
 *	initially show that only counter 0 is being triggered.  Then, when the 
 *	AET_addTrigger function is called, we should see both counters being 
 *	triggered.  
 *  ============================================================================
 *  

 */

typedef enum{
	TRUE,
	FALSE
}boolean;

/* Function Prototypes */
void testFunc1(boolean);
void testFunc2(void);

void main()
{
	/* Local Variables*/
    
	/* 
		Trigger Event Parameters 

	These structures hold the parameters that are passed to the AET 
	library
	*/
	AET_jobParams wm0StartParams = AET_JOBPARAMS;
	AET_jobParams wm0StopParams = AET_JOBPARAMS;
	AET_jobParams cnt0StartParams = AET_JOBPARAMS;
	AET_jobParams cnt0StopParams = AET_JOBPARAMS;
	AET_jobParams wm1StartParams = AET_JOBPARAMS;
	AET_jobParams wm1StopParams = AET_JOBPARAMS;
	AET_jobParams cnt1StartParams = AET_JOBPARAMS;
	AET_jobParams cnt1StopParams = AET_JOBPARAMS;

	
	/* 
	 * We will configure both Counters in the same way, so we can use a single configuration parameter
	 * structure
	 */
	AET_counterConfigParams counterConfig = AET_COUNTERCONFIGPARAMS;
	AET_error status = AET_SOK;

	/* 
	 Job Indexes*
	 
	 For each Job that we set up, we need to remember it's index in the job
	 table so that the job can be cleared later
	 */
	AET_jobIndex jobWm0Start, 
					jobWm0Stop,
					jobCnt0Start,
					jobCnt0Stop;
	/* 
	 Value returned from the watermark counter
	 */
	uint32_t wm0CountValue;
	uint32_t wm1CountValue;

	uint32_t counter;



	/**************************************************************************
	* AET PROGRAMMING STARTS HERE
	***************************************************************************/

    /* Initialize AET */
    AET_init();

    /* Claim the AET resource */
    if (AET_claim())
        return;


    /* Set the configuration parameters for Counter 0 */
    counterConfig.configMode = AET_COUNTER_MAXWM_CONTINUOUS;
    counterConfig.counterNumber = AET_CNT_0;
    counterConfig.reloadValue = 0xFFFFFFFF;  /* 
    									 This is the value reloaded
										 into the watermark counter
										 at the end of a watermark
										 window
										 */

		/* Configure Counter 0 */
		AET_configCounter(&counterConfig);

    counterConfig.counterNumber = AET_CNT_1;

    /* Configure Counter 1 */
		AET_configCounter(&counterConfig);

	/* 
	 Configure a Watchpoint Job to start counter 0 
	 
	 If we don't somehow kickstart the counter, we will never see any values.  
	 A watchpoint job has been arbitrarily chosen to generate this trigger.  
	 The trigger is generated when the address specified by &func1 is 
	 encountered.  We'll also add a trigger to this job to start counter 1.
	 */
	cnt0StartParams.programAddress = (uint32_t) &testFunc2;
	cnt0StartParams.triggerType = AET_TRIG_CNT0_START;

	/* Program the job to kickstart the counter */
	status = AET_setupJob(AET_JOB_TRIG_ON_PC, &cnt0StartParams);
	if (status != AET_SOK)
		return;

	/* Store the index of the kickstart job */
	jobCnt0Start = cnt0StartParams.jobIndex;

	/* Add a trigger to the existing job to start Counter 1 in the same location. */
	cnt1StartParams.triggerType = AET_TRIG_CNT1_START;
	cnt1StartParams.jobIndex = jobCnt0Start;
	cnt1StartParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;
	
	status = AET_setupJob(AET_JOB_ADD_TRIGGER, &cnt1StartParams);
		if (status != AET_SOK)
			return;




	/* Configure the jobs to start/stop the watermark sections */
	wm0StartParams.triggerType = AET_TRIG_WM0START;
	wm0StartParams.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;

	/* Configure the jobs to start/stop the watermark sections */
	wm0StopParams.triggerType = AET_TRIG_WM0STOP;
	wm0StopParams.eventNumber[0] = AET_EVT_MISC_MARK_INS_1;;

	status = AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &wm0StartParams);
	if (status != AET_SOK)
		return;


	/* Store the index of the watermark 0 start job */
	jobWm0Start = wm0StartParams.jobIndex;

	/* Program the counter for the stop event */
	status = AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &wm0StopParams);
	if (status != AET_SOK)
		return;

	
	/* Store the index of the watermark stop job */
	jobWm0Stop = wm0StopParams.jobIndex;

	    /* Enable AET */
    if ( AET_enable())
        return;

	/* Call TestFunc2 to kick off Counter */
	testFunc2();

    for (counter =0; counter < 60000; counter++){
    	if ((counter % 5000) == 0){
    		/* Read the Counters and Print the output */
    		wm0CountValue = AET_readCounter(AET_CNT_0);
    		wm1CountValue = AET_readCounter(AET_CNT_1);
    		printf("Counter 0: 0x%x   Counter 1: 0x%x\n", wm0CountValue,wm1CountValue);
    	}
    	testFunc1(FALSE);
    }

    /* Here, we're going to add the Counter 1 trigger. */
	/* Configure the jobs to start/stop the watermark sections */

    wm1StartParams.jobIndex = jobWm0Start;
    wm1StartParams.triggerType = AET_TRIG_WM1START;
    wm1StartParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;

	status = AET_setupJob(AET_JOB_ADD_TRIGGER, &wm1StartParams);
	if (status != AET_SOK)
		return;


	wm1StopParams.jobIndex = jobWm0Stop;
	wm1StopParams.triggerType = AET_TRIG_WM1STOP;
	wm1StopParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;

	status = AET_setupJob(AET_JOB_ADD_TRIGGER, &wm1StopParams);
	if (status != AET_SOK)
		return;


    for (counter =0; counter < 60000; counter++){
    	if ((counter % 5000) == 0){
    		/* Read the Counters and Print the output */
    		wm0CountValue = AET_readCounter(AET_CNT_0);
    		wm1CountValue = AET_readCounter(AET_CNT_1);
    		printf("Counter 0: 0x%x   Counter 1: 0x%x\n", wm0CountValue,wm1CountValue);
    	}
    	testFunc1(TRUE);
    }


	/* Clear all existing jobs */
	AET_releaseJob(jobWm0Start);
	AET_releaseJob(jobWm0Stop);
	AET_releaseJob(jobCnt0Start);

	cnt0StopParams.programAddress = (uint32_t) &testFunc2;
	cnt0StopParams.triggerType = AET_TRIG_CNT0_STOP;

	status = AET_setupJob(AET_JOB_TRIG_ON_PC, &cnt0StopParams);
	if (status != AET_SOK)
		return;

	jobCnt0Stop = cnt0StopParams.jobIndex;

	/* Add Trigger to Stop Counter 1 */
	cnt1StopParams.triggerType = AET_TRIG_CNT1_STOP;
	cnt1StopParams.jobIndex	= jobCnt0Stop;
	cnt1StopParams.logicOrientation = AET_TRIG_LOGIC_STRAIGHTFORWARD;

	status = AET_setupJob(AET_JOB_ADD_TRIGGER, &cnt1StopParams);
	if (status != AET_SOK)
		return;

	/* Call TestFunc2 to stop the Counters */
	testFunc2();

	AET_releaseJob(jobCnt0Stop);
	AET_release();



}


void testFunc1(boolean addCycles){
	volatile uint16_t count;

	// Window should start here
	_mark(0);

	for (count = 0; count < 0x10; count++)
	{
		// Do Nothing...just delay
	}

	if (TRUE == addCycles){
		for (count = 0; count < 0x100; count++)
		{
			// Do Nothing...just delay
		}
	}

	// Window should end here
	_mark(1);

	return;
}

void testFunc2(void){
	// Do nothing here.  We just use this as a point to generate the end trigger
	return;
}
