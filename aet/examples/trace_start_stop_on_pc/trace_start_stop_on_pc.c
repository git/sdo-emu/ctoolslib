/*
 * trace_start_stop_on_pc.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  =========================trace_start_stop_on_pc============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates both the Start Trace on PC and the Stop Trace
 * on PC functionailty of the AET Target Library.  The Trace Start Trigger will
 * trigger upon encountering the start address of func2.  The Trace Stop Trigger
 * will trigger upon encountering the start address of func3.
 ******************************************************************************/
#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void func1();
void func2();
void func3();


void main()
{
	/* Declare the Parameters Structure */
	AET_jobParams params;

	/* Declare variables for both Jobs */
	AET_jobIndex startJob, endJob;

	params = AET_JOBPARAMS; /* Initialize parameters structure */

	/* Set the parameters structure for the first job. */
	params.programAddress = (Uint32) &func2;
	params.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;
	params.traceActive = AET_TRACE_ACTIVE;
 
	/* Initialize AET */
	AET_init();
	
	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set up the Trace Start job */
	if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &params))
		return;

	startJob = params.jobIndex;

	printf("The Start Job index is %d\n", startJob);

	/* Now, adjust the parameters for the Trace Stop job */
	params.programAddress = (Uint32) &func3;
	params.traceTriggers = AET_TRACE_STOP_PC | AET_TRACE_STOP_TIMING;
	params.traceActive = AET_TRACE_INACTIVE;

	/* Set up the Trace Stop job */
	if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &params))
		return;

	endJob = params.jobIndex;
	
	printf("The End Trace Job index is %d\n", endJob);

	/* Enable AET */
	if (AET_enable())
		return;

	/* Call Func1 (which calls Func2 and starts Trace) */
	func1();

	
	/* Clean Up */
	AET_releaseJob(startJob);
	AET_releaseJob(endJob);
	AET_release();

}

void func1()
{
	int counter;

	for (counter = 0; counter < 3; counter++)
	{
		func2();
	}

}

void func2()
{
	int i = 0;
	int j = 0;

	for (i=0; i<3; i++)
	{
		j += 1;
	}

	i++;

	func3();
}

void func3()
{
	int k = 0;
	int l = 0;

	for (k=0; k<20; k++)
	{
		l+=1;
	}
}




