/*
 * interrupt_on_stall.c
 *
 * AET Library public API Definitions
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <aet.h>
#include <stdio.h>
#include "DSPTraceExport.h"
#include "ETBInterface.h"

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
  This project is an example application, which shows how to use
  AET_JOB_INTR_ON_STALL AETLib feature. A brief description of this
  feature is as follows:
  An AET interrupt will be generated upon detecting a CPU pipeline stall
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) 
  extending beyond a user specified number of cycles. Specifically, 
  the AET is programmed to do the following:
  - decrement a counter on each cycle during a pipeline stall, AND
  - reload the counter when pipeline stall is de-asserted, AND
  - generate trigger (in this case, AET interrupt) when the counter reaches zero

  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_INTR_ON_STALL
  job is setup to generate an AET interrupt, whenever the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles) cycles. The AET, DSPTrace export and ETB
  are setup to capture DSP PC+Timing trace exactly before the occurrence of the AET
  interrupt. In this way, the first pipeline stall which exceeds STALL_CYCLE_THRESHOLD
  cycles can be accurately traced and analyzed further. The worst case pipeline stall
  in this example occurs, when the application code tries to read a variable stored in
  DDR3 memory for the first time. The worst case pipeline stall is ~140 cycles.
  This example also provides a count of number of times CPU pipeline stalled for more
  than STALL_CYCLE_THRESHOLD cycles.

  CtoolsLib Dependencies: This example requires latest version of DSPTraceLib and ETBLib
  Platform RTSC dependencies: For Keystone1: ti.platforms.evm6670 RTSC package and for
                              Keystone2: ti.platforms.evmTCI6636K2H RTSC package

 *  ============================================================================
 */

/*******************************************************************************
 *               INSTRUCTIONS FOR DECODING TRACE DATA
 *******************************************************************************
 *  - Trace, ETB based, data is collected and stored in c:\temp\etbdata.bin
 *  - bin2tdf utility is required, located in:
 *    -> cd <CCS install dir>\ccs_base\emulation\analysis\bin
 *  - To convert binary trace data captured by the ETB, use the following
 *     command line:
 *    -> bin2tdf -bin C:/temp/etbdata.bin -app <outfile_path> -procid 66x
 *       -sirev 1 -rcvr ETB -output C:/temp/interrupt_on_stall.tdf
 *  - The interrupt_on_stall.tdf file may be opened from the CCS menu Tools->
 *     Trace Analyzer->Open Trace File In New View...
 *******************************************************************************
 ******************************************************************************/

/**************** CAUTION: Please Read ****************************************/
//STALL_CYCLE_THRESHOLD should always be greater than the maximum pipeline stall
//cycle period encountered while servicing the AET RTOS interrupt, otherwise the
//AET RTOS interrupt will be generated in an infinite loop, resulting in control
//never been returned back to Application code
#define STALL_CYCLE_THRESHOLD 100
#define AET_RTOS_INTR_VEC_NUM 5
#define C66X_COREPAC_AET_RTOS_EVENT 9

#define BYTE_SWAP32(n) \
	( ((((uint32_t) n) << 24) & 0xFF000000) |	\
	  ((((uint32_t) n) <<  8) & 0x00FF0000) |	\
	  ((((uint32_t) n) >>  8) & 0x0000FF00) |	\
	  ((((uint32_t) n) >> 24) & 0x000000FF) )

Hwi_Handle AetRtosIntrHwi; //HWI handle for AET RTOS interrupt

//ETBLib variables
eETB_Error  etbRet;
ETB_errorCallback pETBErrCallBack =0;
uint8_t coreID =0;
ETBHandle* pETBHandle=0;
ETBStatus etbStatus;
uint32_t etbWidth, retSize=0;
uint32_t pBuffer[1024];

//DSPTraceLib variables
eDSPTrace_Error dspRet;
DSPTrace_errorCallback pDSPErrorCallBack=0;
DSPTraceHandle* pDSPHandle=0;

//flag specifying whether the code to be instrumented is currently running or not
uint32_t app_code_running = 0;
//flag for ETB trace capture completion status
uint32_t trace_capture_complete = 0;
//count specifying the number of times the stall cycle threshold is exceeded by the
//instrumented application
uint32_t exceed_stall_threshold_count = 0;

/*******************************************************************************
 * FUNCTIONS AND PARAMETERS USED FOR EXAMPLE APPLICATION
 */

#define N_APP_FUNCTION_RUNS 100

#pragma DATA_SECTION(globalCntr,"APPSW_DDR3");
uint32_t globalCntr[10] = {0, };
#pragma DATA_SECTION(globalMarker,"APPSW_DDR3");
uint32_t globalMarker = 0;

void endFunction()
{
	volatile int32_t dummy1 =10, dummy2=0;

	dummy2 = dummy1 - 1;

    globalMarker = 10;

    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;
    dummy2--;
    globalCntr[dummy2]++;

    globalMarker = 20;

    return;
}

void startFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;

    globalCntr[8]++;

    return;
}


void appFunction()
{
   volatile int32_t dummy1 =100, dummy2=0;

   dummy2 = dummy1 - 1;

   while(dummy2 > 1)
   {
      dummy2--;
        if(dummy2 & 1)
        {
            endFunction();
        }
        if(dummy2 & 0x4)
        {
            startFunction();
        }
   }

}

void runFunction()
{
   volatile int32_t dummy1 =0, dummy2=0;
   dummy1++;

   dummy2 += dummy1 + 20;
}

void doneFunction(void)
{
    volatile uint32_t temp;
}

AET_jobIndex AetRtosIntrJobIndex;
AET_jobIndex startJob;

/*
 *  ======== taskFxn ========
 */
void taskFxn(UArg a0, UArg a1)
{
	unsigned int idx;
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;
	AET_jobParams AetRtosIntrJobParams = AET_JOBPARAMS;
	AET_jobParams StartTraceParams = AET_JOBPARAMS;

    //Clear AET interrupt flag in TCU_CNTL register
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	//Enable AET interrupt in TCU_CNTL register
    if((TCU_CNTL & 0x00020002) != 0x00020002)
    {
        TCU_CNTL = 0x1;
        TCU_CNTL = 0x000A0000;
        TCU_CNTL = 0x00020002;
    }

	/*** Setup ETB receiver ***/
	/* Open ETB module */
	etbRet = ETB_open(pETBErrCallBack, eETB_Circular, coreID, &pETBHandle, &etbWidth);
    if(etbRet != eETB_Success)
	{
		printf("Error opening ETB\n");
		return;
	}

	/* Enable ETB receiver */
	etbRet = ETB_enable(pETBHandle, 0);
	if(etbRet != eETB_Success)
	{
		printf("Error enabling ETB\n");
		return;
	}

	/*** Setup Trace Export ***/
	/* Open DSP Trace export module */
	dspRet = DSPTrace_open( pDSPErrorCallBack, &pDSPHandle);
	if(dspRet != eDSPTrace_Success)
	{
		printf("Error opening DSP Trace Export block\n");
		return;
	}
    /* Setup trace export clock to FCLK/3 */
	dspRet= DSPTrace_setClock(pDSPHandle, 3);
	if(dspRet != eDSPTrace_Success)
	{
		printf("Error setting up DSP trace export clock\n");
		return;
	}

	dspRet= DSPTrace_enable(pDSPHandle, 0, 0);
    if(dspRet != eDSPTrace_Success)
	{
		printf("Error enabling DSP trace export\n");
		return;
	}

    /* Set up start trace AET job parameters */
	StartTraceParams.programAddress = (uint32_t) &runFunction;
	StartTraceParams.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;
	StartTraceParams.traceActive = AET_TRACE_ACTIVE;

	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */
	if (AET_claim())
		return;

    /* ******************************* IMP. NOTE ****************************/
    /* DSPTrace_setState will not change the TEND state until after         */
    /* AET_claim is executed successfully. Must clear TEND if currently set.*/
    /* ******************************* IMP. NOTE ****************************/
	{
		uint32_t traceState = 0;
		uint32_t new_traceState = DSPTRACE_CLR_TEND;
		DSPTrace_getState(pDSPHandle, &traceState);
		if ( (traceState & DSPTRACE_STATE_TEND) == DSPTRACE_STATE_TEND ){

			DSPTrace_setState(pDSPHandle, new_traceState);
		}

		DSPTrace_getState(pDSPHandle, &traceState);
		if ( (traceState & DSPTRACE_STATE_TEND) == DSPTRACE_STATE_TEND )
			printf("TEND detected - BAD\n");
		else
			printf("TEND not detected - GOOD\n");
	}

	/* Set up the desired start trace job */
	if (AET_setupJob(AET_JOB_START_STOP_TRACE_ON_PC, &StartTraceParams))
	{
		printf("Error setting up AET resources\n");
		return;
	}

	startJob = StartTraceParams.jobIndex;

	printf("AET start trace job index is %d\n", startJob);

	/* Set the counter configuration parameters */
	counter0.configMode = AET_COUNTER_TRAD_COUNTER;
	counter0.counterNumber = AET_CNT_0;
	counter0.reloadValue = STALL_CYCLE_THRESHOLD;

	/* Configure the counter */
	AET_configCounter(&counter0);

	/* Setup Interrupt on stall AET job parameters */
	AetRtosIntrJobParams.counterNumber = AET_CNT_0;
	AetRtosIntrJobParams.eventNumber[0] = AET_EVT_MISC_STALL_PIPELINE;

	/* Program the counter for the event */
	if (AET_setupJob(AET_JOB_INTR_ON_STALL, &AetRtosIntrJobParams))
		return;

	AetRtosIntrJobIndex = AetRtosIntrJobParams.jobIndex;

	printf("AET Interrupt on Stall job index is %d\n", AetRtosIntrJobIndex);

	/* Enable AET */
	if ( AET_enable())
		return;

	runFunction();

	app_code_running = 1;

    for(idx = 0; idx < N_APP_FUNCTION_RUNS; idx++)
    {
        appFunction();
    }

    doneFunction();

    printf("App Code execution completed\n");

    app_code_running = 0;

	if(1 == trace_capture_complete)
	{
	    /*** Get ETB data ***/

	    /* Now disable trace capture - ETB receiver */
		etbRet = ETB_disable(pETBHandle);
		if(etbRet != eETB_Success)
		{
			return;
		}

	    /* Disable DSP Trace Export */
		dspRet= DSPTrace_disable(pDSPHandle);
		if(dspRet != eDSPTrace_Success)
		{
			return;
		}

		/* Check the ETB status */
		etbRet= ETB_status(pETBHandle, &etbStatus);
		if(etbRet != eETB_Success)
		{
			printf("Error getting ETB status\n");
			return;
		}

		if(etbStatus.canRead == 1)
		{
			if(etbStatus.isWrapped == 1)
				printf ("ETB is wrapped; ETB words = %d\n", etbStatus.availableWords);
			else
				printf ("ETB is not wrapped; ETB words = %d\n", etbStatus.availableWords);

			etbRet = ETB_read(pETBHandle, pBuffer, etbStatus.availableWords, 0, etbStatus.availableWords, &retSize);
			if(etbRet != eETB_Success)
			{
				printf("Error reading ETB data\n");
				return;
			}
		}

		/* Transport ETB data */
		/* this example uses JTAG debugger via CIO to transport data to host PC */
		/* An app can deploy any other transport mechanism to move the ETB buffer to the PC */
		FILE* fp = fopen("C:\\temp\\etbdata.bin", "wb");
		if(fp)
		{
			uint32_t sz = 0;
			uint32_t i = 1;
			size_t fret;
			char *le = (char *) &i;

			while(sz < retSize)
			{
				uint32_t etbword = *(pBuffer+sz);
				if(le[0] != 1) //Big endian
					etbword = BYTE_SWAP32(etbword);

				fret = fwrite((void*) &etbword, 4, 1, fp);

				if ( fret < 1 ) {
					printf("Error writing data to  - %s \n", "C:\\temp\\etbdata.bin");
					return;
				}

				sz++;
			}

			fclose(fp);
			printf("Successfully transported ETB data\n");
		}
	}
	else //stop trace, disable ETB, disable DSPTrace and do not export trace data from ETB
	{
		/* ******************************* IMP. NOTE ****************************/
		/* Setting TEND will terminate Trace gracefully if you don't have an    */
		/* End Trace Job or an End All Trace job.                               */
		/* ******************************* IMP. NOTE ****************************/
		{
			uint32_t traceState = 0;
			uint32_t new_traceState = DSPTRACE_SET_TEND;
			DSPTrace_getState(pDSPHandle, &traceState);
			if ( (traceState & DSPTRACE_STATE_TEND) != DSPTRACE_STATE_TEND ){

				DSPTrace_setState(pDSPHandle, new_traceState);
			}
		}

		/* Now disable trace capture - ETB receiver */
		etbRet = ETB_disable(pETBHandle);
		if(etbRet != eETB_Success)
		{
			printf("Error disabling ETB\n");
			return;
		}

		/* Disable DSP Trace Export */
		dspRet= DSPTrace_disable(pDSPHandle);
		if(dspRet != eDSPTrace_Success)
		{
			printf("Error disabling DSP trace export\n");
			return;
		}

	}

	/*** Now we are done and close all the handles **/
	etbRet  = ETB_close(pETBHandle);
	if(etbRet != eETB_Success)
	{
		printf("Error closing ETB\n");
		return;
	}


	dspRet= DSPTrace_close(pDSPHandle);
	if(dspRet != eDSPTrace_Success)
	{
		printf("Error closing DSP trace export module\n");
		return;
	}

    AET_releaseJob(startJob);
	AET_releaseJob(AetRtosIntrJobIndex);
	AET_release();

	printf("No of times CPU pipeline stalled for more than %d cycles: %d\n",STALL_CYCLE_THRESHOLD,exceed_stall_threshold_count);

	return;
}

//AET RTOS interrupt service routine
void AetRtosIsr(UArg arg)
{
	if(1 == app_code_running)
	{
		//Increment count for exceeding stall threshold
		exceed_stall_threshold_count++;

		if(0 == trace_capture_complete)
		{
			/* ******************************* IMP. NOTE ****************************/
		    /* Setting TEND will terminate Trace gracefully if you don't have an    */
		    /* End Trace Job or an End All Trace job.                               */
		    /* ******************************* IMP. NOTE ****************************/
			uint32_t traceState = 0;
			uint32_t new_traceState = DSPTRACE_SET_TEND;
			DSPTrace_getState(pDSPHandle, &traceState);
			if ( (traceState & DSPTRACE_STATE_TEND) != DSPTRACE_STATE_TEND ){

				DSPTrace_setState(pDSPHandle, new_traceState);
			}

			//Set trace_capture_complete flag
			trace_capture_complete = 1;
		}
	}

	Hwi_clearInterrupt(AET_RTOS_INTR_VEC_NUM);

	//Please Note: comment the below line of code if the stall threshold
	//is less than the max period of pipeline stall during the AET interrupt
	//context switch and AET interrupt servicing code (non app code), otherwise
	//the AET RTOS interrupt will always be triggered in an infinite loop, without
	//control passing to the application code

	//If AET interrupt flag is not cleared, this interrupt will not be generated
	//again and again

	//Clear AET interrupt flag in TCU_CNTL register to re-arm the AET interrupt
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	return;
}

/*
 *  ======== main ========
 */
int main()
{ 
    Task_Handle task;
    Error_Block eb;

    Error_init(&eb);
    task = Task_create(taskFxn, NULL, &eb);
    if (task == NULL) {
        BIOS_exit(0);
    }

    //Clear AET RTOS interrupt at startup
    Hwi_clearInterrupt(AET_RTOS_INTR_VEC_NUM);

    Hwi_Params AetRtosIntr_hwiParams;

    Hwi_Params_init(&AetRtosIntr_hwiParams);
    Error_init(&eb);

    // set the event id of the peripheral assigned to this interrupt
    AetRtosIntr_hwiParams.eventId = C66X_COREPAC_AET_RTOS_EVENT;

    // Configure interrupt 5 to invoke "AetRtosIsr".

    AetRtosIntrHwi = Hwi_create(AET_RTOS_INTR_VEC_NUM, AetRtosIsr, &AetRtosIntr_hwiParams, &eb);

    Hwi_enable();

    BIOS_start();    /* does not return */
    return(0);
}
