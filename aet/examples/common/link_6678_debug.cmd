/******************************************************************************/
/*  link_6678_debug.cmd                                                       */
/*  Copyright (c) 1997-2007  Texas Instruments Incorporated                   */
/* Automated Revision Information                                             */
/*  Changed: $LastChangedDate: 2011-05-10 14:20:09 -0400 (Tue, 10 May 2011) $ */
/*  Revision: $LastChangedRevision: 10153 $                                   */
/******************************************************************************/
-stack 0x4000
-heap 0x4000

-l rts6600_elf.lib
-l aetlib_d.l66_elf

MEMORY
{
   SRAM1:	  o=0x00e00000 l=0x00004000	/* 16K of SRAM */
   SRAM2:	  o=0x00800000 l=0x00040000	/* 256K of SRAM */
   L1D:		  o=0x00f00000 l=0x00008000	/* 32K of L1D Cache */
   L1P:		  o=0x00e04000 l=0x00004000	/* 16K of L1P Cache */
   L2:      o=0x00840000 l=0x00040000 /* 256K of L2 Cache */
   DDR3:    o=0x80000000 l=0x40000000 /* 1GB of DDR3 */
   RESMEM:	o=0xE0000000 l=0x01000000
}
   
SECTIONS
{ 
	GROUP (NEAR_DP_RELATIVE)
	{
		.neardata
		.rodata
		.bss
	} > SRAM2
  .far        > SRAM2
  .text       > SRAM2
  .cinit      > SRAM2
  .const      > SRAM2
  .stack      > SRAM2
  .cio		    > SRAM2
  .sysmem	    > SRAM2
  .switch	    > SRAM2
  .fardata	  > SRAM2
}
