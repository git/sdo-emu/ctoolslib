/******************************************************************************/
/*  link_6482_trig_pc_out_of_range_debug.cmd                                  */
/*  Copyright (c) 1997-2007  Texas Instruments Incorporated                   */
/* Automated Revision Information                                             */
/*  Changed: $LastChangedDate: 2012-09-14 07:24:52 -0500 (Fri, 14 Sep 2012) $ */
/*  Revision: $LastChangedRevision: 10559 $                                    */
/******************************************************************************/
-stack 0x800
-heap 0x800

-l rts64plus.lib
-l aetlib_d.l64P 

MEMORY
{
   L1D:		o=0x00f00000 l=0x00008000
   L1P:		o=0x00e00000 l=0x00008000
   L2:      o=0x00800000 l=0x001F0000
   L2A:  	o=0x009F0000 l=0x00010000
   RESMEM:	o=0xE0000000 l=0x01000000
}
   

SECTIONS
{ 
	GROUP (NEAR_DP_RELATIVE)
	{
		.neardata
		.rodata
		.bss
	} > L2
  .far        > L2
  .text:func1 > L2A
  .text:func2 > L2A
  .text       > L2
  .cinit      > L2
  .const      > L2
  .stack      > L2
  .cio		  > L2
  .sysmem	  > L2
  .switch	  > L2
}

_PROTECTED_MEM_START = 0x009F0000;

