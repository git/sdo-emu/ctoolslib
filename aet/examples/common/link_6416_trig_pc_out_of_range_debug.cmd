/******************************************************************************/
/*  link_6416_trig_pc_out_of_range_debug.cmd                                  */
/*  Copyright (c) 1997-2007  Texas Instruments Incorporated                   */
/* Automated Revision Information                                             */
/*  Changed: $LastChangedDate: 2007-07-17 09:43:11 -0500 (Tue, 17 Jul 2007) $ */
/*  Revision: $LastChangedRevision: 3347 $                                    */
/******************************************************************************/
-stack 0x800
-heap 0x800

-l rts6400.lib
-l aetlib_d.l64 

MEMORY
{

	
   VEC:     o=0x00000000 l=0x00000800
   L2:      o=0x00000800 l=0x00080000
   EXTMEM:  o=0x80000000 l=0x00100000
   EXTMEM2: o=0x80100000 l=0x00100000
}
   
SECTIONS
{ 
	GROUP (NEAR_DP_RELATIVE)
	{
		.neardata
		.rodata
		.bss
	} > EXTMEM
  .far        > EXTMEM
  .text:func1 > EXTMEM2
  .text:func2 > EXTMEM2
  .text       > EXTMEM
  .cinit      > EXTMEM
  .const      > EXTMEM
  .stack      > EXTMEM
  .switch 	  > EXTMEM
  .sysmem	  > EXTMEM
  .cio		  > EXTMEM
}

_PROTECTED_MEM_START = 0x80100000;		