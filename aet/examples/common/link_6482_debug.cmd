/******************************************************************************/
/*  link_6482_debug.cmd                                        */
/*  Copyright (c) 1997-2007  Texas Instruments Incorporated                   */
/* Automated Revision Information                                             */
/*  Changed: $LastChangedDate: 2012-09-14 07:24:52 -0500 (Fri, 14 Sep 2012) $ */
/*  Revision: $LastChangedRevision: 10559 $                                    */
/******************************************************************************/
-stack 0x4000
-heap 0x4000

-l rts64plus.lib
-l aetlib_d.l64P 

MEMORY
{

	
   VEC:     o=0x00000000 l=0x00000800
   L2:      o=0x00000800 l=0x00080000
   INTMEM:  o=0x00800000 l=0x001FFFF0
   RESMEM:	o=0x009FFFF0 l=0x10
}
   
SECTIONS
{ 
	GROUP (NEAR_DP_RELATIVE)
	{
		.neardata
		.rodata
		.bss
	} > INTMEM
  .far        > INTMEM
  .text       > INTMEM
  .cinit      > INTMEM
  .const      > INTMEM
  .stack      > INTMEM
  .cio		  > INTMEM
  .sysmem	  > INTMEM
  .switch	  > INTMEM
  .fardata	  > INTMEM  
}
