/*
 * trace_in_pc_range.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ==========================trace_in_pc_range.c===============================
 *
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

/******************************************************************************
 * SUMMARY 
 *
 * This application demonstrates the Trace In Range functionality of the AET
 * Target Library.  The Trace trigger is active between the two Program 
 * addresses
 */
 
#include <aet.h>
#include <stdio.h>

/* Function Prototypes */
void func1();
void func2(unsigned long*);
void dummyfunc();


void main()
{
	/* Declare the Parameters Structure */
	AET_jobParams params;

	/* Declare variables for both Jobs */
	AET_jobIndex jobIndex;

	params = AET_JOBPARAMS; /* Initialize parameters structure */

	/* Set the parameters structure for the first job. */
	params.traceActive = AET_TRACE_ACTIVE;
	params.traceStartAddress = (Uint32) &func1;
	params.traceEndAddress = (Uint32) &dummyfunc;  
		/* dummyfunc is implemented just to find the end of func1. */
	params.traceTriggers = AET_TRACE_TIMING | AET_TRACE_PA;

 
	/* Initialize AET */
	AET_init();
	
	/* Claim the AET resource */
	if (AET_claim())
		return;

	/* Set up the Trace Start job */
	if (AET_setupJob(AET_JOB_TRACE_IN_PC_RANGE, &params))
		return;

	jobIndex = params.jobIndex;

	printf("The Start Job index is %d\n", jobIndex);


	/* Enable AET */
	if (AET_enable())
		return;

	/* Call Func1 (which calls Func2 and starts Trace) */
	func1();

	
	/* Clean Up */
	AET_releaseJob(jobIndex);
	AET_release();

}


void func1()
{
	unsigned long counter = 0;
	int i, j;

	for (j = 0; j < 5; j++)
	{
		for (i = 0; i < 5; i++)
		{
			func2(&counter);
			counter++;
		}
	counter++;
	}
#if defined(__TI_EABI__) 
  asm ("dummyfunc:");  /* This label marks the end of func1(); */
#else
	asm ("_dummyfunc:");  /* This label marks the end of func1(); */
#endif
}

void func2(unsigned long *countvalue)
{
	int i;
	for (i = 0; i < 100; i++)
	{
		(*countvalue)++;
	}

}


