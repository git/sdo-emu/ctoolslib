/*
 * mark_event_counter.c
 *
 * AET Library public API Definitions 
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *

 *  ===================mark_triggered_counter.c===========================
 *  Revision Information
 *   Changed: $LastChangedDate: 2013-01-18 15:47:09 -0600 (Fri, 18 Jan 2013) $
 *   Revision: $LastChangedRevision: 10598 $
 */

#include <aet.h>
#include <stdio.h>

/*
 *  ============================================================================
 *                                 OVERVIEW
 *  ============================================================================
 *  This example is designed to demonstrate the functionality of the MARK 
 *  instruction.  The Mark instruction are special assembly instructions that 
 *  generate events in the AET hardware.  In the application, the MARK 
 *  instruction is treated as a nop.  There are 4 separate MARK instructions, 
 *  MARK 0, MARK 1, MARK 2, and MARK 3.  The same MARK instructions can be used
 *  to generate multiple AET triggers.  Multiple Mark instructions can be used to
 *  generate the same AET trigger.  
 *
 *  In the case of this example, we ar using the MARK instruction to increment a
 *  counter.  Each For each 1000 increments of the counter, the value will be 
 *  printed to stdio to show that it is actually executing.
 *
 *  NOTE: At the time this example was created, the TI C compiler does not 
 *  support the MARK instruction.  However, the assembler does.  Be aware that 
 *  the compiler may shift around instructions for scheduling reasons, so if the
 *  asm (" MARK N"); instruction is used in the middle of C code it could be 
 *  moved and the results might not be exactly as you expect.  Once the compiler 
 *  supports the MARK instructions, it will be able to ensure that they are 
 *  scheduled in the appropriate place.
 *  ============================================================================
 */
void main()
{
	/* Trigger Event Parameters */
	AET_jobParams timerStartParams = AET_JOBPARAMS;

	/* Counter 0 Configuration Parameters */
	AET_counterConfigParams counter0 = AET_COUNTERCONFIGPARAMS;

	/* Value read from the counter */
	Uint32 counterValue;

	/* Iteration Number */
	Uint32 iterator = 1;

	/* Initialize AET */
	AET_init();

	/* Claim the AET resource */ 
	if (AET_claim())
		return;

	/* Use Counter 0 */
	counterValue = AET_readCounter(AET_CNT_0);
	printf ("The initial value of the counter is 0x%x\n", counterValue);

	/* Set the configuration parameters */
	counter0.configMode = AET_COUNTER_TRAD_COUNTER;
	counter0.counterNumber = AET_CNT_0;
	counter0.reloadValue = 0xFFFFFFFF; 	/* This is not really necessary in this 
											mode.  Reload has no meaning in
											traditional mode 
										 */

	/* Configure the counter */
	AET_configCounter(&counter0);

	/* Set the trigger parameters */


	timerStartParams.counterNumber = AET_CNT_0;
	timerStartParams.eventNumber[0] = AET_EVT_MISC_MARK_INS_0;
	timerStartParams.triggerType = AET_TRIG_CNT0_START;

	/* Program the counter for the event */
	if (AET_setupJob(AET_JOB_TRIG_ON_EVENTS, &timerStartParams))
		return;

	/* Enable AET */
	if ( AET_enable())
		return;

	/* Loop and continually print the timer value */
	while(1)
	{
		asm (" nop");		
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" MARK 0");  /* 
							At this point, the MARK 0 event should increment 
							the counter
						  */
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		counterValue = AET_readCounter(AET_CNT_0);
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		if ((iterator % 1000) == 0)
		{
			printf ("Read #%d of the counter is %d\n", iterator, counterValue);		
		}
		asm (" nop");		
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" nop");
		asm (" MARK 1");  /* 
							At this point, the counter should NOT increment 
							because this is a different MARK instruction.
						  */
		asm (" nop");
		asm (" nop");
		iterator++;	
	}
	
											


}
