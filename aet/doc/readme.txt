--------------------------------------------------------------------------------
                               readme.txt
--------------------------------------------------------------------------------
Revision 4.18 

- DESCRIPTION
- RESOURCES
- SIGNIFICANT CHANGES IN THIS RELEASE
- FILES
- APPLICATION PROGRAMMING INTERFACE
- BUILDING THE LIBRARIES AND EXAMPLES
- NOTES
- EXAMPLES
- COMPANION RELEASES
- RELEASE INFORMATION
  - New Features
  - Bugs Fixed
  - Known Issues


--------------------------------------------------------------------------------
|                               DESCRIPTION                                    |
--------------------------------------------------------------------------------
The AET target library is designed to allow the user to program advanced event 
triggering jobs from within target code. This library is meant to be a prototype 
libray from which an actual product will be based.

--------------------------------------------------------------------------------
|                                RESOURCES                                     |
--------------------------------------------------------------------------------
For additonal information about AETLIB, be sure to check out the AETLIB Wiki
support page.  Here you can find Tips and Tricks for using AETLIB, examples, 
and frequently asked questions.  
http://processors.wiki.ti.com/index.php/AETLIB

For AETLIB Support, you can post a mesage on the TI e2e Community in the 
C6000 Forum
http://e2e.ti.com/support/dsp/tms320c6000_high_performance_dsps/default.aspx

--------------------------------------------------------------------------------
|                       SIGNIFICANT CHANGES IN THIS RELEASE                    |
--------------------------------------------------------------------------------
| AETLIB 4.0                                                                   |
| ==========                                                                   |
| The dependency on the Chip Support Library header file "tistdtypes" to build |
| the libraries has been eliminated.  The tistdtypes header file declared the  |
| bit length types of the format Uint32 and Int16, along with custom types such|
| as Bool, Ptr, and Char.  The AET Library now depends on the bit length types |
| defined in the stdint.h, which are of the format uint32_t and int16_t.  The  |
| stdint.h file is delivered with the TI C6000 compiler.  The AETLIB source no |
| longer makes any reference to the tistdypes format.                          |
|                                                                              |
| If you have an existing AETLIB application using the tistdtypes format, there|
| is nothing that you need to do to be compatible with AETLIB 4.0.  By default |
| the legacy types are still supported.  If you wish to strictly use the stdint|
| types in your project, then you only need to define the preprocessor symbol  |
| _AET_DISABLE_TI_STD_TYPES in your application.  This will disable inclusion  |
| of the legacy types in the aet.h header file.                                   |
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
|                                  FILES                                       |
--------------------------------------------------------------------------------
The following is the directory structure for the aet_target_lib package

\aet_target_lib
  \aet
    \doc      - Documentation on using the AET Target Library, in addition to 
                size and performance information.
    \examples - Various examples that demonstrate the functionality of the
                AET Target Library.
    \include  - Header files needed for calling AET Target Library functions
    \lib      - Debug and Release libraries for 64x, 64x+, 674x, and 66x devices 
                compiled in Big/Little Endian and COFF/ELF format.

--------------------------------------------------------------------------------
|                   APPLICATION PROGRAMMING INTERFACE                          |
--------------------------------------------------------------------------------
  Exposed Generic Functions 
  -------------------------
  AET_claim - Claims the AET unit.  Must be called after AET_init and prior to 
    any of the other APIs.
  AET_release - Releases the AET unit and makes it "Available"
  AET_configCounter - Configures the selected counter for mode and operating 
    mode
  AET_readCounter - Returns the value contained in an AET counter value register
  AET_init - Initializes resource database.  Init must be called prior to 
    calling any APIs.
  AET_enable - Enables the AET jobs that are currently programmed.
  AET_disable - Disables all AET jobs that are currently programmed.
  AET_setupJob - Sets up an AET job based on the passed parameters.  See the 
  documentation for a description of the parameter structures necessary
  AET_releaseJob - Clears a previously programmed AET job.  
  AET_enableEmuPins - Enables EMU pins on 64x+ devices for I/O
  AET_disableEmuPins - Disables EMU pins on 64x+ devices
  AET_readCounter - Returns the value contained in an AET counter value register
  AET_addTrigger - Allows additional triggers to be added to existing jobs.  
  This will allow reuse of some limited resources.
  
--------------------------------------------------------------------------------
|                  BUILDING THE LIBRARIES AND EXAMPLES                         |
--------------------------------------------------------------------------------

  Versions of AETLIB prior to 4.0 were dependent on a Chip Support Library 
  header for building the libraries and examples.  This restriction has been
  removed. For chip specific events, the library still requires the soc.h file
  provided with the chip support library for that specific device.  
     
--------------------------------------------------------------------------------
|                                NOTES                                         |
--------------------------------------------------------------------------------
********************************************************************************
* Gel File For Recovering from AETLIB Breakpoint Halt                          *
********************************************************************************
* - There has always been a limitation on the AETLIB breakpoints for 64x+      *
* devices where the user could never recover after halting at an AETLIB        *
* generated halt.  AETLIB now comes with a gel file, located in the following  *
* path                                                                         *
* $INSTALL_DIR$\aet\examples\common\aetlib.gel                          *
*                                                                              *
* The function needed to be called can be accessed from the menu      *
* Gel->AETLIB->ClearAetlibBp.  Another possible solution which will require no *
* intervention is to call this function from the OnHalt callback function.  In *
* this manner, the function will always be called when the DSP halts.          *
********************************************************************************

********************************************************************************
* Examples May Depend on Chip Support Library                                  * 
********************************************************************************
* - AET Target Library versions 1.20 and greater now depend on a chip support  *
*  library header file in order to define chip specific event numbers.  Chip   *
*  specific events are defined in the soc.h file that comes with the Chip      *
*  Support Library package.  Any application that wishes to use a Chip         *
*  Specific Event must include soc.h and use the appropriate mnemonic value.   *
********************************************************************************
  
--------------------------------------------------------------------------------
|                                EXAMPLES                                      |
--------------------------------------------------------------------------------

  -------------------------------------
  dynamic_call_graph (slice.pjt) (64x+)
  -------------------------------------

  This project demonstrates the ability to generate thread aware dynamic callgraph 
  data with Trace.  The example is based on the Bios Slice example, which has a 3
  separate Tasks that repetitively defer to each other.  Additionally, there is a 
  high priority interrupt task. The dmoe will run through all of the tasks.  Trace 
  will capture the entry and exit points and their timestamps for every function, 
  along with the id and timestamp for each thread switch.  The trace_csv_scripts 
  application can then post-process the captured data and generate a dynamic 
  call-graph.  See the Companion Releases section of this document for a link to 
  the trace_csv_scripts

  KNOWN ISSUES
  ------------
  - This example needs a version of C6000 Code Generation Tools of version 6.1.x
  or greater.  The existing .out file has been built with revision 6.1.0 Beta 2.  
  The 6.1.x code generation tools implement the function entry/exit hooks needed
  to allow Trace to capture the appropriate data.
  

  -------------------------------------------
  event_triggered_timer_start.pjt (64x+, 66x)
  -------------------------------------------
  This project shows the functionality of the AET timer triggered by a 
  miscellaneous event signal.  In the demo, the miscellaneous event signal is 
  that for an execute cycle.  This was the simplest miscellaneous signal to 
  generate.  The demo runs through a loop where it prints the value of the 
  counter a number of times.  (At this point, the job has not been programmed, 
  so all values are the same).  Finally the timer start job is programmed and
  enabled, and then the counter value is printed a number of times again.  In 
  these cases, it will be apparent that the counter has started.
  
  KNOWN ISSUES
  ------------   
    - None    
 
  ------------------------------------------
  event_triggered_timer_stop.pjt (64x+, 66x)
  ------------------------------------------
  This project builds on the event_triggered_timer_start.pjt functionality. It 
  uses the same job to get the timer started.  The application will print out
  a number of samples of the counter to show that it is running.  The timer 
  start job is then cleared and a timer stop job is added.  Again, execute 
  cycles are the triggering miscellaneous signal.  Once the stop job is enabled,
  the timer will then stop counting.
  
  KNOWN ISSUES
  ------------   
    - None   
    
  -------------------------------------
  event_triggered_watermark (64x+, 66x)
  -------------------------------------
  This project shows the functionality of the watermark timer. In order to use 
  a watermark timer, we need a timer window start event and a timer window end
  event.  Since we need two different signals, execute cycles are no longer
  sufficient.  So, external triggering is used.  ET0 (EMU0) is toggled (by 
  touching to ground) in order to generate the start window event.  ET1 (EMU1) 
  is toggled (by touching to ground) in order to generate the end window event.
  The counter is configured to be in MAX watermark mode, meaning it will keep 
  track of the largest number of cycles between ET0 and ET1 events.  The 
  application runs in a continuous loop, and periodically prints out the value
  of the watermark couner.  As the periods between ET0 and ET1 get larger, the 
  value contained in the watermark register will increase.
  
  KNOWN ISSUES
  ------------   
    - None        
    
  ---------------------------------
  halt_on_data.pjt (64x, 64x+, 66x)
  ---------------------------------
  This project shows the functionality of the data watchpoint scenario.  In the 
  demo, a watchpoint is set to execute a CPU halt when a specific address is 
  written to.  In this case, the specific address is the address of the memory 
  variable memVariable1.  The CPU halt occurs when memVariable1 is written to 
  for the first time after calling AET_enable.
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs.     

  KNOWN ISSUES
  ------------
  * On the 64x+ and 66x target there is a bit called the BRK_EN bit that needs 
    to be set in order for a hardware breakpoint to halt the CPU.  This bit is 
    in a hardware block that needs to be claimed by the application before it 
    can be written.  If Code Composer Studio is being used, this hardware block 
    will already be claimed by the debugger, and thus the target will not have 
    access to it.  The user can manually set this bit from the debugger to get 
    the  examples to work.  From the command window, this can be done by issuing 
    the command "?BRKEN = 0x2"
    If this is issued, the breakpoint should halt as expected.

    This issue applies to the following Demos
      halt_on_watchpoint
      halt_on_data
      halt_on_data_with_value
      halt_on_data_range
      halt_on_watchpoint_in_range
      halt_on_watchpoint_not_in_range
      trigger_on_event

  LIMITATIONS
  -----------
  * When the CPU is halted by watchpoints, programmed by the target 
    application, they will continue to halt on each subsequent run. There is 
    now a way to recover from this problem, and it's contained in a .gel file 
    that's shipped with AETLIB.  

    This limitation applies to the following demos
      halt_on_watchpoint
      halt_on_data
      halt_on_data_range
      halt_on_data_with_value 
      halt_on_watchpoint_in_range
      halt_on_watchpoint_not_in_range 
      
  ---------------------------------------
  halt_on_data_range.pjt (64x, 64x+, 66x)
  ---------------------------------------
  This project shows the functionality of the range data watchpoint scenario.  
  In the demo, a watchpoint is set to execute a CPU halt when a specific range 
  of addresses in an array are written to.  
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs. 
  
  KNOWN ISSUES
  ------------
  * See Known issues for halt_on_data.pjt   
  
  --------------------------------------------
  halt_on_data_with_value.pjt (64x, 64x+, 66x)
  --------------------------------------------
  This project shows the functionality of the data watchpoint with value 
  scenario.  In the demo, a watchpoint is set to execute a CPU halt when a 
  specific address is written to with a specific value.  In this case, the 
  specific address is the address of the memory variable memVariable1 and the 
  value is 0x0F0F0F0F.  The CPU halt occurs when memVariable1 is written to with
  this value for the first time after calling AET_enable.
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs. 
  
  KNOWN ISSUES
  ------------
  * See Known issues for halt_on_data.pjt           
    
  ---------------------------------------
  halt_on_watchpoint.pjt (64x, 64x+, 66x)
  ---------------------------------------
  This project shows the functionality of the program watchpoint scenario.  In 
  the demo, a watchpoint is set to execute a CPU halt when a specific PC value 
  is encountered.  In this case, the specific value is the address of the 
  test_func2 function.  The CPU halt occurs when test_func2 is called for the 
  first time after calling AET_enable.
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs.
  
  KNOWN ISSUES
  ------------
  * See Known issues for halt_on_data.pjt

  ------------------------------------------------
  halt_on_watchpoint_in_range.pjt (64x, 64x+, 66x)
  ------------------------------------------------
  This project shows the functionality of the program watchpoint in range 
  scenario.  In the demo, we place two symbols in the code called 
  dummyFuncStart and dummyFuncStop.  These mark the start and end of the range
  that we are interested in.  The CPU should halt when it enters this range.
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs. 
  
  KNOWN ISSUES
  ------------
  * See Known issues for halt_on_data.pjt  
  
  ----------------------------------------------------
  halt_on_watchpoint_not_in_range.pjt (64x, 64x+, 66x)
  ----------------------------------------------------
  This project shows the functionality of the program watchpoint  NOT in range 
  scenario.  In this scenario, we use a special linker command file which places
  the func1 and func2 .text sections in a separate section of memory from all of
  the other .text sections.  We set the start range address at 0, and the end 
  range address at 0x80100000 which is the boundary where the other section of
  memory starts.  We should trigger when we exit memory range 0x00000000 - 
  0x80100000.
  
  This is an imprecise breakpoint, so there will be ~5 cycles of skid before the
  CPU halt actually occurs.

  AET_readCounter - Returns the value contained in an AET counter value register   
  
  KNOWN ISSUES
  ------------      
  * See Known issues for halt_on_data.pjt  
  
  ----------------------------------
  mark_event_counter.pjt (64x+, 66x)
  ----------------------------------
  The 64x+ devices implement an 4 assembly instructions which generate Events to
  the AET Unit.  These instructions are called Mark instructions and at 
  implemented in assembly as MARK 0, MARK 1, MARK 2, and MARK 3.  In the example,
  the MARK 0 instruction is used to trigger an AET counter to increment. The MARK
  instruction is set in a loop and when the loop counter is a multiple of 1000, 
  the value of the counter is read and printed.  This should be the same value
  as the loop counter.  You will also see a MARK 1 instruction that is executed.
  However, the counter should NOT increment on this instruction since only 
  MARK 0 was specified. 

  KNOWN ISSUES
  ------------
   - None

  --------------------------
  slice_cache_profile (64x+)
  --------------------------
  This project shows how to use Trace and the System Events to do cache analysis
  on an application.  The triggers are set up to store a trace sample whenever 
  either of the L1D Read Miss Hits External Cacheable events occurs.  The 
  results can then be post processed to determine, at a function level, which 
  functions cause the most L1D read misses.  The script used to generate this 
  information is the trace_event_analysis.pl script contained in the Trace
  CSV Scripting companion release.

  Known Issues
  ------------
   - None

  --------------------------------------------------------------
  slice_watchpoint_in_range.pjt (64x+) (Release Build Only)
  --------------------------------------------------------------
  This project shows the functionality of the interrupt trigger on access to a
  specific memory range.  This is a DSP/BIOS application with dynamic tasks.  
  In the hook functions that switch tasks, AET is programmed to watch for writes
  to the top of the next tasks stack, watching for a stack overflow.  If the 
  stack is about to overflow, an interrupt is taken, and a message is printed
  in the DSP/Bios message log.  
  
  KNOWN ISSUES
  ------------
    - None
    
  -------------------------------------
  slice_watermark_intlatency.pjt (64x+) 
  -------------------------------------
  This project shows how to use the watermark counter to track the worst case 
  interrupt latency for a specific interrupt in an application.  The watermark
  counter keeps track of the cycles between when an interrupt request occurs
  and when the first instruction of the interrupt service routine happens. 
  
  KNOWN ISSUES
  ------------
    - At present we also have to manually configure (via e.g. GEL or Memory 
      Window) the GEM interrupt controller so that AEGEVT0 points to the 
      interrupt that we're interested in (e.g. INT14 timer ISR) We then tell 
      AETLIB Watermark Counter 0 to open watermark window on AEGEVT0.  See the
      documentation for the interrupt controller on the 64x+ device.
      
  ----------------------------------
  state_machine.pjt (64x, 64x+, 66x)
  ----------------------------------
  This project shows how to use the state machine to qualify an AET job by 
  state.  We program two jobs in this example.  The first job generates a 
  trigger to go from State 0 To State 1 whenever a write occurs to a 
  specified address (testDataLocation).  The second job generates a halt 
  trigger whenever the function testfunc2 is executed.  This job is also 
  qualified by the state machine being in state 1.  So, you will see that 
  testfunc2 is executed a number of times prior to testDataLocation being
  written to.  But after testDataLocation is written to, testfunc2 will 
  halt immediately upon the first execution.

  KNOWN ISSUES
  ------------
  * See Known issues for halt_on_data.pjt 

  -------------------------------------
  statistical_profiling.pjt (64x+, 66x)
  -------------------------------------
  This project shows how to gather statistical profiling information, which uses
  an AET timer to trigger trace PC capture at specified time intervals.  The 
  data can then be post-processed to show a breakdown of the execution cycles
  as related to the function that contains them.

  KNOWN ISSUES
  ------------
    - None
    
  -----------------------------------
  trace_in_data_range.pjt (64x+, 66x)
  -----------------------------------  
  This project shows the functionality of the trace_in_data_range functionality.
  This scenario generates trace data for all instructions that write to a 
  specified range of data memory.  The example is set up so that all instructions 
  that write to the top half of an array are traced. 

  KNOWN ISSUES
  ------------   
    - None       
  
  ---------------------------------
  trace_in_pc_range.pjt (64x+, 66x)
  ---------------------------------
  This project shows the functionality of the trace in program range scenatio.
  In this scenario, we specify a range of Program addresses.  Each instruction 
  within that program range will be traced.  (The ability is also there to trace
  everything outside of that range by specifying the traceActive parameter as
  inactive)  

  KNOWN ISSUES
  ------------   
    - None   
    * See Known issues for halt_on_data.pjt 
  -------------------------------
  trace_on_events.pjt (64x+, 66x)
  -------------------------------
  This project shows the functionality of the trace on event scenario.  In this
  scenario, a trace trigger is generated each time a particular event occurs.  
  In the example, the event that we are using is the execute cycles trigger, 
  which generates trace data everytime an execute cycle occurs.  (This would be
  a way to trace all execute cycles without any stall cycles)  This scenario can 
  be used with any external event, so we could use it to capture trace data when
  a particular interrupt signal occurs, etc.

  KNOWN ISSUES
  ------------   
    - None    
  
  ---------------------------
  trace_on_pc.pjt (64x+, 66x)
  ---------------------------
  This project shows the functionality of the trace start on encountered pc 
  scenario.  This scenario generates a start trace trigger whenever a specified
  PC is encountered.  In the example, the start of func2 is specified as the 
  start PC.  The function func1 is called from main 3 times, and func2 is called
  from within func1.  In the trace data we should see the beginning of the trace
  data in func 1, and two subsequent calls to func 1 and func 2.

  KNOWN ISSUES
  ------------   
    - None  

  ----------------------------------
  trace_on_timer_trigger (64x+, 66x)
  ----------------------------------
  This project is a more basic variation on the statistical profiling example.  
 

  -------------------------------
  trigger_on_mark.pjt (64x+, 66x)
  -------------------------------
  See the description on the MARK instruction in the mark_event_counter.pjt 
  section.  This example shows how to generate a generic trigger when a mark 
  instruction is encountered.
  
  -------------------------------
  interrupt_on_stall (64x+, 66x)
  -------------------------------
  This project is an example application, which shows how to use 
  AET_JOB_INTR_ON_STALL AETLib feature. A brief description of this 
  feature is as follows:
  An AET interrupt will be generated upon detecting a CPU pipeline stall 
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) 
  extending beyond a user specified number of cycles. Specifically, 
  the AET is programmed to do the following:
  - decrement a counter on each cycle during a pipeline stall, AND
  - reload the counter when pipeline stall is deasserted, AND
  - generate trigger (in this case, AET interrupt) when the counter reaches zero
  
  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely 
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_INTR_ON_STALL 
  job is setup to generate an AET interrupt, whenever the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles). The AET, DSPTrace export and ETB 
  are setup to capture DSP PC+Timing trace exactly before the occurence of the AET
  interrupt. In this way, the first pipeline stall which exceeds STALL_CYCLE_THRESHOLD
  cycles can be accurately traced and analyzed further. The worst case pipeline stall
  in this example occurs, when the application code tries to read a variable stored in
  DDR3 memory for the first time. The worst case pipeline stall is ~140 cycles.
  This example also provides a count of number of times CPU pipeline stalled for more 
  than STALL_CYCLE_THRESHOLD cycles.

  CtoolsLib Dependencies: This example requires latest version of DSPTraceLib and ETBLib
  Platform RTSC dependencies: For Keystone1: ti.platforms.evm6670 RTSC package and for
                              Keystone2: ti.platforms.evmTCI6636K2H RTSC package       

  -------------------------------
  count_stalls (64x+, 66x)
  -------------------------------
  This project is an example application, which shows how to use
  AET_JOB_COUNT_STALLS AETLib feature. A brief description of this
  feature is as follows:
  The AET Counter1 will be advanced by 1, upon detecting a CPU pipeline stall
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) 
  extending beyond a user specified number of cycles. Specifically, 
  the AET is programmed to do the following:
  - decrement AET counter0 on each cycle during a pipeline stall, AND
  - reload the AET counter0 when pipeline stall is de-asserted, AND
  - generate trigger (in this case, AET counter1 event) when the AET counter0 reaches zero
  - Advance AET Counter1 by one, whenever the AET counter1 event is triggered

  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_COUNT_STALLS
  job is setup to count (Using AET counter1) the number of times, the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles) cycles.
  
  -------------------------------
  interrupt_stall_duration (64x+, 66x)
  -------------------------------
  This project is an example application, which shows how to use
  AET_JOB_INTR_STALL_DURATION AETLib feature. A brief description of this
  feature is as follows:
  An AET interrupt will be generated upon detecting a CPU pipeline stall
  (any event part of the AET stall signal group or AET_EVT_MISC_STALL_PIPELINE) extending beyond a user specified
  number of cycles. Specifically, the AET is programmed to do the following:
  - decrement counter0 on each cycle during a pipeline stall, AND
  - when the counter0 reaches zero (counter0 overflow event)
    -- generate AET interrupt
    -- transition from state0 to state1
    -- reload counter1
  - decrement counter1 on (each cycle during a pipeline stall && state1 is true)
  - when pipeline stall is de-asserted
    -- reload counter0
    -- transition from state1 to state0

  Example description:
  AET counter 0 is used to count the number of stalled cycles (precisely
  AET_EVT_MISC_STALL_PIPELINE event is used). The AET_JOB_INTR_STALL_DURATION
  job is setup to generate an AET interrupt, whenever the period of CPU pipeline
  stall exceeds STALL_CYCLE_THRESHOLD (100 cycles) cycles. AET counter 1 is used to
  count the number of stalled cycles above the STALL_CYCLE_THRESHOLD. The total duration
  of a stall whose duration is greater than STALL_CYCLE_THRESHOLD = STALL_CYCLE_THRESHOLD +
  read AET counter 1 value. The worst case pipeline stall
  in this example occurs, when the application code tries to read a variable stored in
  DDR3 memory. This example provides a count of number of times CPU pipeline stalled for more
  than STALL_CYCLE_THRESHOLD cycles and the duration of these stalls.

  CtoolsLib Dependencies: This example requires latest version of DSPTraceLib and ETBLib
  Platform RTSC dependencies: For Keystone1: ti.platforms.evm6670 RTSC package and for
                              Keystone2: ti.platforms.evmTCI6636K2H RTSC package
							  
--------------------------------------------------------------------------------
|                          COMPANION RELEASES                                  |
--------------------------------------------------------------------------------
The latest release of the following companion releases can be found at these
links.

CToolsLib
  - AETLIB is a member of the CTools Family of Libraries.  You can find 
    information on the entire set of libraries at the following link:
    http://processors.wiki.ti.com/index.php/CToolsLib
    
    You can download the most recent versions of these libraries at:
    https://gforge.ti.com/gf/project/ctoolslib/frs/  

Trace CSV Scripting Package
https://www-a.ti.com/downloads/sds_support/applications_packages/trace_csv_scripts/index.htm
  * Note: This package was designed for use with CCSv3.x. While it is still 
    useful with CCSv4 and CCSv5, the steps for capturing and saving the trace 
    data have changed.  The package itself only documents the steps for CCSv3.


================================================================================
Version 4.10 Table of Contents
================================================================================
Release Date: 03/27/2015
4.18.00-01 New Features
        - None 
4.18.00-02 Bugs Fixed
        - The job index for AET_JOB_START_STOP_TRACE_ON_PC job is always returned as zero, which is incorrect and the job index
          should be the one which is generated while inserting the current job into the AET job table.

Release Date: 11/21/2014
4.17.00-01 New Features
        - AET_JOB_INTR_STALL_DURATION job is added. This feature provides a way for
          App SW to determine the total stall duration of a CPU stall, whose stall
          duration is greater than a user specified number of CPU cycles. 
4.17.00-02 Bugs Fixed
        - None
		
Release Date: 10/30/2014
4.16.00-01 New Features
        - Allow either AET_EVT_MISC_STALL_PIPELINE or any event from the stall event
		  group to be used with AET_JOB_INTR_ON_STALL and AET_JOB_COUNT_STALLS jobs 
4.16.00-02 Bugs Fixed
        - None

Release Date: 10/29/2014
4.15.00-01 New Features
        - AET_JOB_COUNT_STALLS job is added. This job counts the total number of 
		  CPU pipeline stalls (or any event part of the AET stall signal group) 
		  extending beyond a user specified number of CPU cycles.
4.15.00-02 Bugs Fixed
        - None
		
Release Date: 07/25/2014
4.14.00-01 New Features
        - AET_JOB_INTR_ON_STALL job is added. An AET interrupt will be generated 
		  upon detecting a CPU pipeline stall (any event part of the AET stall signal group) 
		  extending beyond a user specified number of CPU cycles.
4.14.00-02 Bugs Fixed
        - SDSCM00049669: Remove printf from debug AETLib version.
		
Release Date: 09/20/2013
4.13.00-01 New Features
        - None
4.13.00-02 Bugs Fixed
        - Code review for un-intialized global variables. Fixed un-initialized global variable issues in aet.c and aet_trigger_builders.c.

Release Date: 05/30/2013
4.12.00-01 New Features
        - Added support to count events belonging to groups (STALL and MEMORY) other than MISC group.

Release Date: 01/18/2013
4.11.00-01 New Features
        - None
4.11.00-02 Bugs Fixed
        - SDSCM00046060: The AET_AddTrigger API did not allow triggers to be added in any order. Only a particular order always works.
        - SDSCM00046060: Resolved confict between both the AET_ENABLE_RTOS_INT() macro and DSPTraceLib claiming/enabling the TCU.
        - SDSCM00045891: AETLib AET_JOB_START_STOP_TRACE job does not work for data trace
4.11.00-03 updates to event_triggered_watermark and trace_on_events example projects

Release Date: 09/06/2012
4.10.00-01 New Features
4.10.00-02 Bugs Fixed
4.10.00-03 Known Bugs  

4.10.00-01 New Features
	- AET_AddTrigger API Added - Gives the capability to add a trigger to an
		existing AET Job.  This enables more flexibility with the AET Resources.
4.10.00-02 Bugs Fixed
	- SDSCM00044857 - Programming TRIG ON DATA Range after Watermark Job causes 
										infinitely triggered interrupts
	- SDSCM00044877 - ADD Trigger Does not work with 7-Wide Trigger builders
	- SDSCM00044879 - A few AETLIB Debug Register Names are incorrect	
	- SDSCM00044880 - The file aet_trace_between_seq_events.c should not be part 
										of the build
	- SDSCM00044886 - Type Warnings in aet_aeg_manager.c
	- SDSCM00044887 - AET Trigger On Data Range with value doesn't work properly
	- SDSCM00044888 - Programming and Releasing Trigger On Event jobs does not 
										reclaim all resources
	- SDSCM00045053 - Trace Stop on PC trigger is hard-coded to TB3W #0.  This 
										causes conflicts when another job claims that resource 
										first. 
4.10.00-03 Known Bugs  

  - The BIOS based examples have not yet been updated to CCSv5 projects, or with 
   configurations for C66x devices.  These examples are:
    - dynamic_call_graph
    - slice_cache_profile
    - slice_watchpoint_in_range
    - slice_watermark_intlatency
   
   These will be updated in a future release of AETLIB.

================================================================================
Version 4.10ALPHA Table of Contents
================================================================================
Release Date: 3Q - 2012

4.01.00ALPHA-01 New Features
4.01.00ALPHA-02 Bugs Fixed
4.01.00ALPHA-03 Known Bugs  

4.01.00ALPHA-01 New Features
    
4.01.00ALPHA-02 Bugs Fixed
  - SDSCM00044130 - Uninitialized value warning for internalStatus in 
    aet_aeg_manager.c
  - SDSCM00044131 - AETLIB 4.-0 has a number of cases where warnings for 
    conversion from 16-bit to 8-bit types occurs

4.01.00ALPHA-03 Known Bugs  
  - The BIOS based examples have not yet been updated to CCSv5 projects, or with 
   configurations for C66x devices.  These examples are:
    - dynamic_call_graph
    - slice_cache_profile
    - slice_watchpoint_in_range
    - slice_watermark_intlatency
   
   These will be updated in a future release of AETLIB.
    
================================================================================
Version 4.00 Table of Contents
================================================================================
Release Date: 2Q - 2012

4.00.00-01 New Features
4.00.00-02 Bugs Fixed
4.00.00-03 Known Bugs  

4.00.00-01 New Features
  - Eliminated Dependency on Chip Support Library to build the libraries.
  - All example and library projects migrated from CCSv4 to CCSv5.  CCSv4 
    projects are not supported. 
    
4.00.00-02 Bugs Fixed
  - SDSCM00039916 -   AET should not depend on CSL 
  - SDSCM00040414 - AETLIB is missing the 66x Example linker command files 
    link_6678.com and link_6678_debug.cmd  

4.00.00-03 Known Bugs  
  - The BIOS based examples have not yet been updated to CCSv5 projects, or with 
   configurations for C66x devices.  These examples are:
    - dynamic_call_graph
    - slice_cache_profile
    - slice_watchpoint_in_range
    - slice_watermark_intlatency
   
   These will be updated in a future release of AETLIB.

================================================================================
Version 3.20 Table of Contents
================================================================================
Release Date: 05/24/2011

3.20.00-01 New Features
3.20.00-02 Bugs Fixed
3.20.00-03 Known Bugs  

3.20.00-01 New Features
  - Rearchitected directory structure.  Instead of aet_target_lib\ti\sdo\aet, the 
    structure is now aet_target_lib\aet.  All relative paths between files 
    remain the same.  This was done to align with the other CToolsLib libraries.
  - Updated the examples to also include C6678 targets, and to include projects
    compatible with CCSv5.  
  - Added ELF Binary Builds for 64x+ devices

3.20.00-02 Bugs Fixed
  - none

3.20.00-03 Known Bugs  
  - The BIOS based examples have not yet been updated to CCSv5 projects, or with 
   configurations for C66x devices.  These examples are:
  - dynamic_call_graph
   - slice_cache_profile
   - slice_watchpoint_in_range
   - slice_watermark_intlatency
   
   These will be updated in a future release of AETLIB.

================================================================================
Version 3.10 Table of Contents
================================================================================
Release Date: 09/30/2010

3.10.00-01 New Features
3.10.00-02 Bugs Fixed
3.10.00-03 Known Bugs  

3.10.00-01 New Features
  - CCSv4 Compatible Projects now included for building the libraries and 
    examples.  These projects can be found in the directory under the existing 
    CCSv3 Compatible Projects.  A user can build with either CCSv3 or CCSv4.

3.10.00-02 Bugs Fixed
  - none

3.10.00-03 Known Bugs  
  - None

================================================================================
Version 3.00 Table of Contents
================================================================================
Release Date: 09/30/2010

3.00.00-01 New Features
3.00.00-02 Bugs Fixed
3.00.00-03 Known Bugs  

3.00.00-01 New Features
  - ELF Binaries now included in AETLIB Build
  - C66x Binaries now included in AETLIB Build

3.00.00-02 Bugs Fixed
  - none

3.00.00-03 Known Bugs  
  - None

================================================================================
Version 2.70 Table of Contents
================================================================================
Release Date: 03/05/2010

2.70.00-01 New Features
2.70.00-02 Bugs Fixed
2.70.00-03 Known Bugs  

2.70.00-01 New Features
  - None
  
2.70.00-02 Bugs Fixed
  - SDSCM00035398 - AETLIB doesn't allocate the Trigger Builders correctly for 
    the Watermark Event scenario
    
2.70.00-03 Known Bugs  
  - None

================================================================================
Version 2.62 Table of Contents
================================================================================
Release Date: 02/10/2010

2.62.00-01 New Features
2.62.00-02 Bugs Fixed
2.62.00-03 Known Bugs  

2.62.00-01 New Features
  - None
  
2.62.00-02 Bugs Fixed
  - SDSCM00035096 - AETLIB doesn't allocate events properly for watermark between 
    events
    
2.62.00-03 Known Bugs  
  - None

================================================================================
Version 2.61 Table of Contents
================================================================================
Release Date: Not Released

2.61.00-01 New Features
2.61.00-02 Bugs Fixed
2.61.00-03 Known Bugs  

2.61.00-01 New Features
  - None
  
2.61.00-02 Bugs Fixed
  - SDSCM00034559 - AETLIb doesn't initialize the AEG variable in the used 
    resources structure
    
2.61.00-03 Known Bugs  
  - None

================================================================================
Version 2.60 Table of Contents
================================================================================
Release Date: 05/20/2009

2.60.00-01 New Features
2.60.00-02 Bugs Fixed
2.60.00-03 Known Bugs  

2.60.00-01 New Features
  - Added capability for using Stop Trace Triggers.  These are different from the 
    Trace End trigger and should be used when it is desired to be able to restart
    trace from within a single execution.  The trigger enuerations for these are

      - AET_TRIG_TRACE_PCSTOP
      - AET_TRIG_TRACE_TIMINGSTOP
      - AET_TRIG_TRACE_DATASTOP

2.60.00-02 Bugs Fixed
  - SDSCM00032018 - AETLIB doesn't support Stop Trace Triggers
  
2.60.00-03 Known Bugs  
  - None

================================================================================
Version 2.50 Table of Contents
================================================================================
Release Date: 01/29/2009

2.50.00-01 New Features
2.50.00-02 Bugs Fixed
2.50.00-03 Known Bugs  

2.50.00-01 New Features
  - None
  
2.50.00-02 Bugs Fixed
  - SDSCM00030251 - AET Target Library doesn't generate correct trigger builder 
  for event Trace End
  
2.50.00-03 Known Bugs  
  - None

================================================================================
Version 2.40 Table of Contents
================================================================================ 
Release Date: 11/17/2008

2.40.00-01 New Features
2.40.00-02 Bugs Fixed
2.40.00-03 Known Bugs  

2.40.00-01 New Features
  - Added GEL file for recovering from a halt at an AETLIB breakpoint
  
2.40.00-02 Bugs Fixed
  - SDSCM00028920 - Interrupt Event numbers are slightly off in the aet.h header 
    file
  - SDSCM00029026 - WaterMark Counter 1 does not seem to work with AETLIB

2.40.00-03 Known Bugs  
 - None

================================================================================
Version 2.30 Table of Contents
================================================================================ 
Release Date: 07/29/2008

2.30.00-01 New Features
2.30.00-02 Bugs Fixed
2.30.00-03 Known Bugs  

2.30.00-01 New Features
  - None
  
2.30.00-02 Bugs Fixed
  - None
  
2.30.00-03 Known Bugs  
  - None

================================================================================
Version 2.20 Table of Contents
================================================================================ 
Release Date: 05/29/2008

2.20.00-01 New Features
2.20.00-02 Bugs Fixed
2.20.00-03 Known Bugs  

2.20.00-01 New Features
  - Re-added support for 64x Devices for generic AET support.  Trace is only
    supported on the 64x+ devices.
  - Removed support for AET_traceConfig API.  This API really doesn't belong in
    this library, as it only works for ETB trace and another library is needed
    for configuring trace to work with ETB.  This functionality should be 
    supported in the library that can configure trace.

2.20.00-02 Bugs Fixed
  - SDSCM00025400 - AETLIB does not support Trigger On Data Address range with 
    Value (Range)
  - SDSCM00025462 - AET_traceConfig API does not belong in AETLIB

2.20.00-03 Known Bugs  
  - None

================================================================================
Version 2.00 Table of Contents
================================================================================ 
Release Date: 02/14/2008

2.00.00-01 New Features
2.00.00-02 Bugs Fixed
2.00.00-03 Known Bugs  

2.00.00-01 New Features
  - Removed support for 64x Devices.  Only 64x+ Devices are now supported
  - Created real Bios TSK based example for dynamic callgraph.  

2.00.00-02 Bugs Fixed
  - SDSCM23215 - AET Watchpoints don't seem to trigger on certain addresses
  - SDSCM23585 - AETLIB Call Graph Profiling Triggers don't generate a timestamp 
    for Thread switch

2.00.00-03 Known Bugs  
  - None

================================================================================
Version 1.30 Table of Contents
================================================================================ 
Release Date: 01/04/2008

1.30.00-01 New Features
1.30.00-02 Bugs Fixed
1.30.00-03 Known Bugs  

1.30.00-01 New Features
  - Added triggering capability for Trace Function Profiling

1.30.00-02 Bugs Fixed
  - None

1.20.00-03 Known Bugs  
  - None 
  
================================================================================
Version 1.20 Table of Contents
================================================================================ 
Release Date: 08/21/2007

1.20.00-01 New Features
1.20.00-02 Bugs Fixed
1.20.00-03 Known Bugs  

1.20.00-01 New Features
  - Dependency on Chip Support Library for external event numbers.

1.20.00-02 Bugs Fixed
  - SDSCM00019232 - Event numbers specific to a device should be contained within 
    the devices CSL header, not within AETLIB

1.20.00-03 Known Bugs  
  - None
  
================================================================================
Version 1.10 Table of Contents
================================================================================
Release Date: 06/21/2007

1.10.00-01 New Features
1.10.00-02 Bugs Fixed
1.10.00-03 Known Bugs  

1.10.00-01 New Features
  - Support for triggering on Multiple Events Added.  
  - Support for Memory and Stall Events Added.
  - Support for multiple uses of the same AEG Event added
  - AET_disable API for start/stop of triggering without removing job.
  - 2 Mark instruction examples added.
  - Trace Examples added for 64x devices (6416)
    
1.10.00-02 Bugs Fixed 
  - SDSCM00017552 - Trigger on Event does not work with multiple events
  - SDSCM00017598 - Documentation of AET_Enable/AET_Disable comments are reversed
  - SDSCM00017599 - Invalid Trigger Mask Error Code Not Documented
  - SDSCM00017600 - The State Machine Enumerations are not documented
  - SDSCM00017651 - Statistical Profiling Release Example won't build
  - SDSCM00017652 - Slice_watchpoint_in_range example complains that 
    6416_link_debug.cmd is missing
  - SDSCM00017653 - Halt on data range does not work for the 6416
  - SDSCM00017654 - The state machine example doesn't work on 6416
  - SDSCM00017662 - Trace in PC range example doesn't work on the 6416
  - SDSCM00018023 - State Machine example does not work on the 6416
  - SDSCM00018291 - Initialzation structures in aet.h should be declared as extern far

1.10.00-03 Known Bugs
  - None
  
================================================================================
Version 1.00.02 Table of Contents
================================================================================
Release Date: 02/09/2007 

1.00.02-01 New Features
1.00.02-02 Bugs Fixed
1.00.02-03 Known Bugs
  
1.00.02-01 New Features
  - Trigger on Event scenario and Example Added
  - Statistical Profiling Trace functionality and Example added.
  - Slice_watchpoint_in_range modified to trigger interrupt rather than halt
  - Slice_watermark_intlatency example added for using Watermark counters to
    find the worst case interrupt latency.

1.00.02-02 Bugs Fixed
  - Bugzilla#1337 - Footprint of AETLib can be improved by merging some common 
    code into single functions   
  - Bugzilla#1356 - Help file has comments reversed for AET_traceConfig and 
    AET_enableEmuPins 
  - Bugzilla#1380 - Dynamic Allocation of AEG Event is done incorrectly 
  - Bugzilla#1384 - RTOS_INT macros won't compile on C64x  
  - Bugzilla#1387 - AET watermark jobs should enable both watermarks 

1.00.02-03 Known Bugs
  - SDSCM00018023 - State Machine example does not work on the 6416
 
================================================================================
Version 1.00 Table of Contents
================================================================================
Release Date: 10/24/2006

1.00.00-01 New Features
1.00.00-02 Bugs Fixed
1.00.00-03 Known Bugs

1.00.00-01 New Features
  - Trace Trigger Generation 
  - Trace Configuration API
  - Trace Demos

  1.00.00-02 Bugs Fixed
  - Bugzilla#1332 - No Initialization Structure for JobParams, CounterConfigParams 
    structures 

  1.00.00-03 Known Bugs
  - Bugzilla#1337 - Footprint of AETLib can be improved by merging some common code 
    into single functions 

================================================================================
Version 0.08 Table of Contents
================================================================================
Release Date: 09/27/2006

0.08.00-01. New Features
0.08.00-02. Bugs Fixed
0.08.00-03. Known Bugs

0.08.00-01. New Features
  - Support for external events added.  
  
0.08.00-02. Bugs Fixed
  - Bugzilla#1268 - The AET target library doesn't have a means for selecting 
    external events 
  - Bugzilla#1269 - If programming of a job fails, the resources allocated for that 
    job cannot be reclaimed 
  - Bugzilla#1276 - Trigger Builder Addresses other than TB0 are not calculated 
    correctly
  - Bugzilla#1277 - Clear jobs does not work correctly for some trigger builders
  - Bugzilla#1313 - All API's trigger builders are miscalcualted unless they are 
      trigger builder zero 

0.08.00-03. Known Bugs
  - None

================================================================================
Version 0.07 Table of Contents
================================================================================
Release Date: 06/14/2006

0.07.00-01. New Features
0.07.00-02. Bugs Fixed
0.07.00-03. Known Bugs

0.07.00-01. New Features
  - AET_setupJob Parameter structure consolidated
  - Watchpoint in range functionality added.
  - Watchpoint in range demo added
  
0.07.00-02. Bugs Fixed
  - Bugzilla#1236 Trigger on data watchpoint doesn't respect size value   
  - Bugzilla#1253 Incorrect AEG used for Event Triggered Timer Stop when using 
    counter 1  
  - Bugzilla#1254 AET_removeJob does not clear used resources  
  - Bugzilla#1255 Trigger on data does not support 64-bit data values  

0.07.00-03. Known Bugs
  - None

================================================================================
Version 0.06 Table of Contents
================================================================================
Release Date: 06/08/2006

0.06.00-01. New Features
0.06.00-02. Bugs Fixed
0.06.00-03. Known Bugs

0.06.00-01. New Features
  - 64x+ Support added.  All 64x examples ported to 64x+.  
  - 64x+ specific timer functionality added.
  
0.06.00-02. Bugs Fixed
  - Bugzilla#1235 64+ library doesn't allocate correct resources for data watchpoint
  - Bugzilla#1240 Trigger on watchpoint examples/APIs only trigger on 32-bit 
    reads/writes. 

0.06.00-03. Known Bugs
  - Bugzilla#1337 Footprint of AETLib can be improved by merging some common code 
    into single functions 

================================================================================
Version 0.05 Table of Contents
================================================================================
Release Date: 05/09/2006

0.05.00-01. New Features
0.05.00-02. Bugs Fixed
0.05.00-03. Known Bugs  

0.05.00-01. New Features
  - Dynamic resource allocation added for trigger on watchpoint jobs.  
  - All job programmations go through the same AET_setupJob API and are cleared
    by the AET_releaseJob API
  - Debug version of the library implements a printf to show the indexes and 
    values written by the library for all indirect register writes.

0.05.00-02. Bugs fixed
  - Bugzilla#1130 AET_cycles_sequential should verify that the same event is not 
    used for start and stop trigger.  
  - Bugzilla#1137 Each job disables all trigger builders and doesn't keep track of 
    the ones it cleared  
  - Bugzilla#1169 Linker Command File Specifies External memory as INTMEM  
  - Bugzilla#1177 Instead of Bool, AET functions should return a status number  
  - Bugzilla#1178 Direct and Indirect Registers should be contained within separate 
    XML files and generate separate register structures  
  - Bugzilla#1179 Move examples into separate directories, with separate src, inc 
    directories  

0.05.00-03. Known Bugs
  - Bugzilla#1132 Joule version of Watchpoint demos will not halt on watchpoint  
  - Bugzilla#1133 The event timing sequential demo seems to only update the first 
    time. 

================================================================================
Version 0.04.00 Table of Contents
================================================================================
Release Date: 03/16/2006

0.00.01-01. New Features
0.00.01-02. Bugs Fixed
0.00.01-03. Known Bugs

0.00.01-01. New Features

  - First Release

0.00.01-02. Bugs fixed

  - First Release

0.00.01-03. Known Bugs

  - First Release

--------------------------------------------------------------------------------
AUTOMATED REVISION INFORMATION
 *   Changed: $LastChangedDate: 2013-09-20 11:05:11 -0500 (Fri, 20 Sep 2013) $
 *   Revision: $LastChangedRevision: 10660 $
--------------------------------------------------------------------------------

