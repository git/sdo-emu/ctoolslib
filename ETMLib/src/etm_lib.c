/*
 * etm_lib.c
 *
 * Embedded Trace Macrocell (ETM) Library Implementation
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/*! \file etm_lib.c
    \brief ETM Library API source code

*/
#ifdef _DEBUG
    #include <stdio.h>
#endif
#include <stdlib.h>
#include "etm_lib.h"
#include "etm_registers.h"

/* Enumeration for ETM and PTM types */
typedef enum _ETM_Type
{
	ETM_TYPE = 0,     /*!< TBR type */
	PTM_TYPE          /*!< ETB type */
}ETM_Type;

#define ETM_MAJOR_MINOR_REV (0x23)
#define PTM_MAJOR_MINOR_REV (0x31)

// Time out count used in ETM lib implementation
#define TIMEOUT_CNT (100)

/**
* Check whether the ARM Trace macro cell is a ETM or PTM implementation
* returns ETM_TYPE = 0 - for ETM implementation and PTM_TYPE = 1 - for PTM implementation
*/
static ETM_Type check_etm_type(uint32_t base_address)
{
	uint32_t rev_id;
	// Read ETM ID register
	rev_id = (ETM_ID_REG(base_address) >> 4) & 0xFF;

	if(rev_id == ETM_MAJOR_MINOR_REV)
	{
		// Return TI-ETB type
		return(ETM_TYPE);
	}
	else if(rev_id == PTM_MAJOR_MINOR_REV)
	{
		//Return TBR type
		return(PTM_TYPE);
	}

	//By default always return ETM type
	return(ETM_TYPE);
}

/******************************************************************************/
/*! \par etm_hw_support
    \brief This structure is declared in the ETM handle structure and stores
           the device specific implementation defined ETM module information.
*/ 
struct etm_hw_support {
    uint8_t max_context_ids;
    uint8_t max_counters;
    uint8_t max_decoders;
    uint8_t max_data_comparators;
    uint8_t max_address_comparators;
};

/********************************************************************************/
/*! \par Private _etm_handle structure
    \brief Internally used library information
    
    Handle structure for ETM interface, opaque pointer setup in etm_lib.h to keep
    from having to change the user source code based on a library update.
 */
struct _etm_handle {
    uint32_t base_address;
    uint8_t core_id;
    void (*p_callback)(etmhandle_t const handle,
                        const char *func,
                        enum etm_error err);

    uint16_t active_comparators;     /*!< Mask used to indicate the address
                                          comparators that are currently being
                                          used in the trace configuration */
    uint8_t active_count;
    struct  etm_hw_support hardware;
    bool configured;
};

/********************************************************************************/
/* Function prototypes used by API functions */

extern void * cTools_memAlloc(size_t sizeInBytes);
extern void   cTools_memFree(void * ptr);
extern void * cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes);
extern void   cTools_memUnMap(void * vAddr, unsigned int mapSizeInBytes);

static inline enum etm_error etm_validate_input(etmhandle_t const handle);

/*******************************************************************************
 * PUBLIC LIBRARY FUNCTIONS
 *
 */
/*! \copydoc etm_open
 */
enum etm_error etm_open(etmhandle_t * const p_handle,
                        const struct etm_config *const p_config)
{
    uint32_t    regvalue;
    uint32_t    *p_virtual_address;
    uint32_t    timeout_cnt;
    uint32_t    program_active, os_lock_state, os_lock_implemented;
    etmhandle_t handle;
    enum etm_error status = ETM_SUCCESS;
    
    /* Verify input parameters, handle pointer must be NULL and the base address
     *  value must be something other than '0', check that module has been
     *  powered, and get virtual address mapping if necessary.
     */
    if(*p_handle != NULL) {
        return ETM_ERR_HANDLE;
    }
    
    /* Default parameters are not used, the configuration MUST pass a base
     *  address value into this API call.
     */
    if((p_config == NULL) || (p_config->base_address == 0)) {
        status = ETM_ERR_CONFIGURATION;
    }
    else {
        p_virtual_address = (uint32_t *)cTools_memMap(p_config->base_address,
                                                  ETM_REGSPACE_SIZE);
        if(p_virtual_address == NULL) {
            status = ETM_ERR_MEM_MAPPING;
        }

        if(status == ETM_SUCCESS)
        {
            /* Check to see if ETM register locking is implemented and if so if it is
             *  locked
             */
            regvalue = ETM_LOCK_STAT_REG((uint32_t)p_virtual_address);
            if(regvalue & ETM_LOCKING_IMPLEMENTED) {
                ETM_LOCK_ACC_REG((uint32_t)p_virtual_address) = ETM_UNLOCK_VALUE;
            }
        
            /* The value of this bit indicates whether the ETM Trace Registers are
             *  accessible. Usually, an external input to the ETM, driven by the
             *  system power controller, controls the value of this bit. This read
             *  will also clear the PDSR Sticky Register state bit if the ETM
             *  powered up bit is set.
             */
            if((ETM_POWER_DOWN_REG((uint32_t)p_virtual_address) &
                        ETM_POWERED_UP) == 0) {
                cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
                status = ETM_ERR_POWER;
            }

        }

        /* Check bit in configuration register to verify the debugger has not
         *  disabled software writes to ETM registers, also check the disable
         *  debugger writes bit, indication that another software instance has
         *  already taken ownership of this hardware module.
         */
        if(status == ETM_SUCCESS)
        {
        	if(check_etm_type((uint32_t)p_virtual_address) == ETM_TYPE)
        	{
				if(ETM_CONTROL_REG((uint32_t)p_virtual_address) &
					   (ETM_CTRL_DISABLE_SW_WR | ETM_CTRL_DISABLE_DBG_WR)) {
					cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
					status = ETM_ERR_OWNERSHIP;
				}
				else {

					/* Make sure we have the memory available required for this module
					 */
					handle = (etmhandle_t)cTools_memAlloc(sizeof(struct _etm_handle));
					if(handle == NULL) {
						cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
						status = ETM_ERR_MEMORY;
					}
				}
        	}
        	else //PTM implementation
        	{
        		/* Make sure we have the memory available required for this module
        							 */
				handle = (etmhandle_t)cTools_memAlloc(sizeof(struct _etm_handle));
				if(handle == NULL) {
					cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
					status = ETM_ERR_MEMORY;
        	    }
            }
        }
    }
    if(status != ETM_SUCCESS) {
        if(p_config->p_callback != NULL) {
            p_config->p_callback(NULL, __FUNCTION__, status);
        }
        return status;
    }
    
    *p_handle = handle;
    handle->base_address = (uint32_t)p_virtual_address;
    handle->core_id = p_config->core_id;
    handle->p_callback   = p_config->p_callback;
    handle->active_comparators = 0;
    handle->active_count       = 0;
    handle->configured         = false;
    
    /* Currently, the library is not using anything other than the address
     *  comparators. If functionality is added, collect additional maximum
     *  values from configuration code register.
     */
    /* The value indicates the number of pairs, multiply by 2 to get the
     *  total number of comparators.
     */
    handle->hardware.max_address_comparators =
                            (ETM_CONFIG_REG(handle->base_address) & 0xF) << 1;
    
    /* Before configuring anything on the ETM, make sure the programming bit
     *  is set and the powerdown bit is clear. After setting, the code must wait
     *  for the programming bit in the status register to show it has been set.
     * The power down bit enables the ETM power to be controlled externally and
     *  must be cleared by trace software tools at the beginning of a debug
     *  session.
     */

    os_lock_implemented = ETM_OS_LSR_REG(handle->base_address) & ETMOSLSR_IMPLEMENTED;

    if(os_lock_implemented == ETMOSLSR_IMPLEMENTED)
    {
    	//Release OS lock
    	ETM_OS_LAR_REG(handle->base_address) = ETM_OS_UNLOCK_VALUE;

    	//Wait for the lock to get released
    	os_lock_state = ETM_OS_LSR_REG(handle->base_address) & ETM_OSLOCK_STAT_BIT;
		timeout_cnt = 0;
		while(os_lock_state != 0) {
			os_lock_state = ETM_OS_LSR_REG(handle->base_address) & ETM_OSLOCK_STAT_BIT;
			timeout_cnt++;
			if(timeout_cnt > TIMEOUT_CNT) {
				cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
				cTools_memFree(handle);
				if(p_config->p_callback != NULL) {
					p_config->p_callback(NULL, __FUNCTION__, ETM_ERR_STOP_TIMEOUT);
				}
				return ETM_ERR_STOP_TIMEOUT;  /******************** EARLY RETURN */
			}
		}
    }

    //Enable the PTM from power down
    regvalue = ETM_CONTROL_REG(handle->base_address);
    regvalue &= ~ETM_CTRL_PWRDOWN;
    ETM_CONTROL_REG(handle->base_address) = regvalue;

    //Set the PTM programming bit
    ETM_CONTROL_REG(handle->base_address) |= ETM_CTRL_ETM_PROGRAM;
    
    timeout_cnt = 0;
    do {
        program_active =
                    ETM_STATUS_REG(handle->base_address) & ETM_STAT_PROGRAM_BIT;
        timeout_cnt++;
        if(timeout_cnt > TIMEOUT_CNT) {
            cTools_memUnMap(p_virtual_address, ETM_REGSPACE_SIZE);
            cTools_memFree(handle);
            if(p_config->p_callback != NULL) {
                p_config->p_callback(NULL, __FUNCTION__, ETM_ERR_STOP_TIMEOUT);
            }
            return ETM_ERR_STOP_TIMEOUT;  /******************** EARLY RETURN */
        }
    } while(program_active == 0);

    //Setup ETM/PTM ATB Trace ID register
    ETM_TRACE_ID_REG(handle->base_address) = (0x20 + handle->core_id) & 0x7F; //0x20 - base ATB ID for ARM trace

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
		/* *** DEFAULT CONFIGURAION REGISTER SETTINGS ***
		 * DISABLE_DBG_WR - disable register writes from debugger.
		 *  Note: Debugger can halt the processor to simulate software accesses.
		 *         This means that, even if this bit is set, the debugger might
		 *         be able to access the ETM registers.
		 * PORT_MODE_DYN - trace port clocking mode set to dynamic, for capture
		 *                 on chip.
		 * ETMEN - port selection bit that controls the external ETMEN pin.
		 * PORT_8BIT - port size, determines how many external pins are available
		 *             to output the trace information, number of bits in TRACEDATA.
		 *             Currently this value is hardcoded in the decoder to 8.
		 * TRACE_ADDR - data access set to trace only the address portion of the
		 *              access. The data portion may get enabled during individual
		 *              trace sessions.
		 * ETM_PROGRAM - while asserted, all ETM functions are disabled.
		 */
		regvalue = ETM_CTRL_DISABLE_DBG_WR |
				   ETM_CTRL_PORT_MODE_DYN  |
				   ETM_CTRL_ETMEN          |
				   ETM_CTRL_PORT_8BIT      |
				   ETM_CTRL_TRACE_ADDR     |
				   ETM_CTRL_ETM_PROGRAM;
		ETM_CONTROL_REG(handle->base_address) = regvalue;
    }

    /* Clear ETM registers to get ready for configuration.
     *  All input parameters have been verified, no need to check return value.
     */
    etm_config_clear(handle);

    /* Triggers are not currently being utilized, disable by setting NOT always
     *  true.
     */
    ETM_TRIGGER_EVENT_REG(handle->base_address) = ETM_EVT_NOTA |
                                                  ETM_TGR_HARD_WIRED_INPUT;

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
		/* Set the FIFO level to maximum value. Reading the register back will give
		 *  the actual maximum value.
		 * This value could get set differently if data suppression is enabled. When
		 *  enabled, will cause data tracing to be disabled when the FIFO reaches
		 *  the level set in the level register. Instruction tracing continues and
		 *  since it is normally a much lower bandwith requirement, will prevent
		 *  overflow.
		 */
		ETM_FIFO_LEVEL_REG(handle->base_address) = 0xFF;
    }

    return ETM_SUCCESS;
} 

/******************************************************************************/
/*! \copydoc etm_close
 */
enum etm_error etm_close(etmhandle_t *const p_handle)
{
    uint32_t    regvalue;
    etmhandle_t handle;

    if(p_handle == NULL) {
        return ETM_ERR_HANDLE;
    }
    handle = *p_handle;
    
    /* Clear control register bits and set programming bit */
    ETM_CONTROL_REG(handle->base_address) |= ETM_CTRL_ETM_PROGRAM;
    regvalue = ETM_CONTROL_REG(handle->base_address);
    regvalue |= ETM_CTRL_ETM_PROGRAM;
    regvalue &= ~(ETM_CTRL_DISABLE_DBG_WR | ETM_CTRL_ETMEN);
    ETM_CONTROL_REG(handle->base_address) = regvalue;
    
    /* Clear triggers, filters, comparator registers, and address access type
     *  registers
     */
    etm_config_clear(handle);

    cTools_memUnMap((void *)&handle->base_address, ETM_REGSPACE_SIZE);
    cTools_memFree(handle);
    
    /* Invalidate the users's handle */
    *p_handle = NULL;
    
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_get_version
 */
enum etm_error etm_get_version(etmhandle_t const handle,
                               uint32_t *p_major, uint32_t *p_minor,
                               uint32_t *p_etm_major, uint32_t *p_etm_minor)
{
    enum etm_error status;

    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    if(p_major != NULL)
    {
        *p_major = ETMLIB_MAJOR_VERSION;
    }
    if(p_minor != NULL)
    {
        *p_minor = ETMLIB_MINOR_VERSION;
    }
    if(p_etm_major != NULL)
    {
        /* 0 - ETMv1, 1 - ETMv2, 2 - ETMv3 */
        *p_etm_major = (ETM_ID_REG(handle->base_address) >> 8) & 0xF;
        *p_etm_major = *p_etm_major + 1;
    }
    if(p_etm_minor != NULL)
    {
        *p_etm_minor = (ETM_ID_REG(handle->base_address) >> 4) & 0xF;
    }
    
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_get_status
 */
enum etm_error etm_get_status(etmhandle_t const handle, uint32_t *p_status)
{
    enum etm_error status;

    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    if(p_status != NULL)
    {
        /* Provide status for the Trigger, program, and overflow bits */
        *p_status = ETM_STATUS_REG(handle->base_address) &
                                                    (ETM_STAT_TRIGGER_FLAG |
                                                     ETM_STAT_PROGRAM_BIT  |
                                                     ETM_STAT_OVERFLOW);
    }
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_enable_trace
 */
enum etm_error etm_enable_trace(etmhandle_t const handle,
                             const struct etm_trace_control *const p_control)
{
    enum etm_error status = ETM_SUCCESS;
    uint32_t regvalue;
    uint32_t timeout_cnt;
    uint32_t program_active;
    
    if(handle == NULL) {
        return ETM_ERR_HANDLE;
    }
    else if((handle->base_address == 0) || (p_control == NULL)) {
        status = ETM_ERR_PARAMETER;
    }
    else if((ETM_CONTROL_REG(handle->base_address) &
                                                ETM_CTRL_ETM_PROGRAM) == 0) {
        status = ETM_ERR_TRACE_ACTIVE;
    }
    else if(handle->configured == false) {
        status = ETM_ERR_CONFIGURATION;
    }
    else if(ETM_TRACE_CTRL_REG(handle->base_address) & ETM_TRACE_CTRL_ENABLE) {

    	uint32_t trace_ss_start_mask;

    	if(check_etm_type(handle->base_address) == ETM_TYPE)
    	{
    		trace_ss_start_mask = ETM_TRACE_SS_START_MASK;
    	}
    	else
    	{
    		trace_ss_start_mask = PTM_TRACE_SS_START_MASK;
    	}

        /* If the Trace start/stop logic is enabled, make sure that a start
         *  comparator has been configured, otherwise this enable_trace function
         *  will have no effect and tracing will never get enabled.
         */
        if((ETM_TRACE_SS_REG(handle->base_address) & trace_ss_start_mask) == 0) {
            status = ETM_ERR_TRACE_START;
        }
    }
    if((status != ETM_SUCCESS) && (handle->p_callback != NULL)) {
        handle->p_callback(handle, __FUNCTION__, status);
        return status;
    }
    
    regvalue = ETM_CONTROL_REG(handle->base_address);

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
		/* Apply trace control configuration parameters.
		 *  Data Suppression is used to disable data tracing if a FIFO level is
		 *  reached. Instruction tracing is enabled, but lower bandwidth
		 *  requirements.
		 *  Monitor CPRT (Coprocessor Register Transfers
		 */
		if(p_control->suppress_data == true) {
			regvalue |= ETM_CTRL_SUPPRESS_DATA;
		}
		else {
			regvalue &= ~ETM_CTRL_SUPPRESS_DATA;
		}

		if(p_control->monitor_cprt == true) {
			regvalue |= ETM_CTRL_FILTER_CPRT;
		}
		else {
			regvalue &= ~ETM_CTRL_FILTER_CPRT;
		}

	    /* The following code sets up the instruction trace (TRACE_EVT) and data
	     *  trace (VDATA_EVT) event registers. If the TRACE_ALL option has been
	     *  requested, the instructions and data should be traced. The event
	     *  registers are set for Hard-wired input to provide a permanently
	     *  active (always true) resource.
	     */
	    regvalue &= ~ETM_CTRL_TRACE_DATA_MASK;
	    if(p_control->type == ETM_TRACE_ALL) {
	        regvalue |= ETM_CTRL_TRACE_ADDR;
	        ETM_TRACE_EVT_REG(handle->base_address) = ETM_TGR_HARD_WIRED_INPUT;
	        ETM_VDATA_EVT_REG(handle->base_address) = ETM_TGR_HARD_WIRED_INPUT;
	    }
	    else {
	        ETM_TRACE_EVT_REG(handle->base_address) = ETM_TGR_HARD_WIRED_INPUT;
	        ETM_VDATA_EVT_REG(handle->base_address) = 0;
	    }
    }
    else //PTM implementation, No data tracing support
    {
    	ETM_TRACE_EVT_REG(handle->base_address) = ETM_TGR_HARD_WIRED_INPUT;
    }

    if(p_control->cycle_accurate_tracing == true) {
        regvalue |= ETM_CTRL_CYCLE_ACCURATE;
    }
    else {
        regvalue &= ~ETM_CTRL_CYCLE_ACCURATE;
    }
    
    /* Clear programming bit to enable tracing */
    regvalue &= ~ETM_CTRL_ETM_PROGRAM;
    ETM_CONTROL_REG(handle->base_address) = regvalue;

    /* Verify that the ETM PROG bit is cleared correctly */
	program_active =
				ETM_STATUS_REG(handle->base_address) & ETM_STAT_PROGRAM_BIT;
	timeout_cnt = 0;
	while(program_active == 1) {
		program_active =
					ETM_STATUS_REG(handle->base_address) & ETM_STAT_PROGRAM_BIT;
		timeout_cnt++;
		if(timeout_cnt > TIMEOUT_CNT) {
			if(handle->p_callback != NULL) {
				handle->p_callback(handle, __FUNCTION__, ETM_ERR_STOP_TIMEOUT);
			}
			return ETM_ERR_STOP_TIMEOUT;  /******************** EARLY RETURN */
		}
	}

    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_stop_trace
 */
enum etm_error etm_stop_trace(etmhandle_t const handle)
{
    uint32_t regvalue;
    uint32_t timeout_cnt;
    uint32_t program_active;
    
    if(handle == NULL) {
        return ETM_ERR_HANDLE;
    }
    
    /* The ETM programming bit is set to disable all ETM functions */
    regvalue = ETM_CONTROL_REG(handle->base_address);
    regvalue |= ETM_CTRL_ETM_PROGRAM;
    ETM_CONTROL_REG(handle->base_address) = regvalue;
    
    /* Verify all ETM activities have completed by waiting for the programming
     *  bit in the ETM status register to show it is set.
     */
    program_active =
                ETM_STATUS_REG(handle->base_address) & ETM_STAT_PROGRAM_BIT;
    timeout_cnt = 0;
    while(program_active == 0) {
        program_active =
                    ETM_STATUS_REG(handle->base_address) & ETM_STAT_PROGRAM_BIT;
        timeout_cnt++;
        if(timeout_cnt > TIMEOUT_CNT) {
            if(handle->p_callback != NULL) {
                handle->p_callback(handle, __FUNCTION__, ETM_ERR_STOP_TIMEOUT);
            }
            return ETM_ERR_STOP_TIMEOUT;  /******************** EARLY RETURN */
        }
    }
    
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_clear
 */
enum etm_error etm_config_clear(etmhandle_t const handle)
{
    int32_t cnt;
    enum etm_error status;
    
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    ETM_TRACE_SS_REG(handle->base_address) = 0;
    ETM_TRACE_EVT_REG(handle->base_address) = 0;
    ETM_TRACE_CTRL_REG(handle->base_address) = 0;

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
		ETM_VDATA_EVT_REG(handle->base_address) = 0;
		ETM_VDATA_IE_REG(handle->base_address) = 0;
		ETM_VDATA_IER_REG(handle->base_address) = 0;
    }
    for(cnt = 0; cnt < handle->hardware.max_address_comparators; cnt++) {
        ETM_ADR_COMP_REG(handle->base_address, cnt) = 0;
        ETM_ACC_TYPE_REG(handle->base_address, cnt) = 0;
    }

    handle->configured = false;
    handle->active_comparators = 0;
    handle->active_count       = 0;
    
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_trace_on
 */
enum etm_error etm_config_trace_on(etmhandle_t const handle)
{
    enum etm_error status;
    
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    if(handle->active_count >= handle->hardware.max_address_comparators) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
        }
        return ETM_ERR_RESOURCE;
    }

    /* Set the ETM filter registers for instructions and data to exclude
     *  nothing, the event registers will get set when the tracing is
     *  enabled.
     */
    ETM_TRACE_CTRL_REG(handle->base_address) = ETM_TRACE_CTRL_EXCLUDE;
    
    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
    	ETM_VDATA_IER_REG(handle->base_address)  = ETM_VDATA_IER_EXCLUDE_ONLY;
    }

    handle->configured = true;
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_inst_start
 */
enum etm_error etm_config_inst_start(etmhandle_t const handle,
                                   const uint32_t start_address)
{
    uint8_t  comparator;
    enum etm_error status;
    
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    /**********************************************************************/
    /* TODO 1:
     *  The check for comparators and finding an available comparator is
     *   performed in several API functions. Collapse this functionality into
     *   a single function. Pass the pointer to the current function name
     *   (__FUNCTION__) to call the error callback with the correct value.
     */
     
    /* Setting the start condition only consumes a single comparator, check to
     *  make sure one is available, account for the added resources, set
     *  the address passed in as parameter to the selected comparator register,
     *  and set the remaining ETM registers for a start condition.
     */
    if(handle->active_count >= handle->hardware.max_address_comparators) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
        }
        return ETM_ERR_RESOURCE;
    }
    handle->active_count++;
    
    /* The active_comparators parameter in the handle structure is used as a
     *  mask value to indicate (set to 1) the comparators that are currently
     *  in use.
     * The comarator value that is found is used as an index input to the
     *  macros that set the appropriate registers below.
     */
    for(comparator = 0;
        comparator < handle->hardware.max_address_comparators; comparator++) {
        if((handle->active_comparators & (1 << comparator)) == 0) {
            handle->active_comparators |= (1 << comparator);
            break;
        }
    }
    
    /* The Access Type Register is set for an access size of word for the ARM
     *  instruction execution. Filtering is not performed on the size of the
     *  access for instruction address comparisons. A single address comparator
     *  matches if the value in the Address Comp Value Register matches the
     *  address of the access, regardless of the setting of the size field.
     * When an instruction address comparison must match from multiple
     *  instruction sets, the field size must be set to largest instruction
     *  size required.
     */
    ETM_ADR_COMP_REG(handle->base_address, comparator) = start_address;
    ETM_ACC_TYPE_REG(handle->base_address, comparator) = (ETM_ACCTYPE_WORD |
                                                          ETM_ACCTYPE_INST_EXE);
    ETM_TRACE_SET_START(handle->base_address, comparator);
    
    /* The EXCLUDE nothing option must be set if this control register is set to
     *  enable tracing. Exclude nothing is selected by setting the exclude bit
     *  and not setting any of the range comparator bits in this register.
     */
    ETM_TRACE_CTRL_REG(handle->base_address) = (ETM_TRACE_CTRL_ENABLE |
                                                ETM_TRACE_CTRL_EXCLUDE);

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {

		/* Set the data registers to include everything, by setting the Include/
		 *  Exclude Range register to Exclude nothing. This value may get
		 *  overridden when tracing is enabled if the instruction only option
		 *  if provided.
		 * Check the Include/Exclude Range register and the Include/Exclude
		 *   comparator register before setting for everything. These registers
		 *   may have been previously configured with a different configuration
		 *   call.
		 */
		if(((ETM_VDATA_IER_REG(handle->base_address) & 0xFFFF) == 0) &&
		   (ETM_VDATA_IE_REG(handle->base_address) == 0)) {
			ETM_VDATA_IER_REG(handle->base_address) = ETM_VDATA_IER_EXCLUDE_ONLY;
		}
    }
    
    handle->configured = true;
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_inst_end
 */
enum etm_error etm_config_inst_end(etmhandle_t const handle,
                                   const uint32_t end_address)
{
    int32_t        comparator;
    enum etm_error status;
    
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    /*
     *  SEE TODO 1 DESCRIPTION
     */
     
    /* Setting the end condition only consumes a single comparator, check to
     *  make sure one is available, account for the added resources, set
     *  the address passed in as parameter to the selected comparator register,
     *  and set the remaining ETM registers for an end condition.
     */
    if(handle->active_count >= handle->hardware.max_address_comparators) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
        }
        return ETM_ERR_RESOURCE;
    }
    handle->active_count++;
    
    for(comparator = 0;
        comparator < handle->hardware.max_address_comparators; comparator++) {
        if((handle->active_comparators & (1 << comparator)) == 0) {
            handle->active_comparators |= (1 << comparator);
            break;
        }
    }
    
    ETM_ADR_COMP_REG(handle->base_address, comparator) = end_address;
    ETM_ACC_TYPE_REG(handle->base_address, comparator) = (ETM_ACCTYPE_WORD |
                                                          ETM_ACCTYPE_INST_EXE);

    ETM_TRACE_SET_END(handle->base_address, comparator);


    /* Make sure the Trace start/stop enable bit is set in the Control register */
    ETM_TRACE_CTRL_REG(handle->base_address) = (ETM_TRACE_CTRL_ENABLE |
                                                ETM_TRACE_CTRL_EXCLUDE);

    handle->configured = true;
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_inst_range
 */
enum etm_error etm_config_inst_range(etmhandle_t const handle,
                                   const struct etm_inst_range *const p_range)
{
    int32_t        comparator;
    enum etm_error status;
    uint32_t       regvalue;
    uint32_t       comparator_mask;
    bool  found = false;
    
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    if(p_range->low_address >= p_range->high_address) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_ADDRESS_ORDER);
        }
        return ETM_ERR_ADDRESS_ORDER;
    }
    
    /* Setting an instruction range does not allow for both inclusion and
     *  exclusion during a single trace session. If a comparison has already
     *  been setup for this trace configuration (one of the lower 8 bits for
     *  the address comparator pair selection would be set), make sure this
     *  request is for the same range type.
     */
    regvalue = ETM_TRACE_CTRL_REG(handle->base_address);

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
    	comparator_mask = ETM_TRACE_CTRL_TYPE_MASK;
    }
    else
    {
    	comparator_mask = PTM_TRACE_CTRL_TYPE_MASK;
    }

    if((regvalue & comparator_mask) != 0) {
        if(((regvalue & ETM_TRACE_CTRL_EXCLUDE) &&        /* Exclude Option */
            (p_range->type == ETM_INCLUDE)) ||
           (((regvalue & ETM_TRACE_CTRL_EXCLUDE) == 0) && /* Include Option */
             (p_range->type == ETM_EXCLUDE)))
        {
            if(handle->p_callback != NULL) {
                handle->p_callback(handle, __FUNCTION__, ETM_ERR_RANGE_TYPE);
            }
            return ETM_ERR_RANGE_TYPE;
        }
    }
    
    /*
     *  SEE TODO 1 DESCRIPTION
     */
     
    /* Setting a comparison range requires a pair of comparators starting on an
     *  even boundary. Verify availablity, locate the first available pair and
     *  set control, comparator, and access type registers.
     */
    for(comparator = 0;
        comparator < handle->hardware.max_address_comparators; comparator+=2) {
        if((handle->active_comparators & (3 << comparator)) == 0) {
            handle->active_comparators |= (3 << comparator);
            found = true;
            break;
        }
    }
    
    if(found == false) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
        }
        return ETM_ERR_RESOURCE;
    }
    handle->active_count += 2;

    if(p_range->type == ETM_EXCLUDE) {
        regvalue |= ETM_TRACE_CTRL_EXCLUDE;
    }
    /* Divide the comparator by 2 (shifting right by 1 bit) to get the
     *  comparator pair value, then use that value to set the correct bit in the
     *  lower 8-bits of the TraceEnable control register.
     * Ex: If comparator value is 6 (4th pair), then the shift value would be
     *      6/2=3 and bit 3 would get set.
     * NOTE: The comparator value is NOT getting shifted back to the left by one
     *       bit, the bit "1" is getting shifted to the left by the comparator
     *       value.
     */
    regvalue |= (1 << (comparator >> 1));
    ETM_TRACE_CTRL_REG(handle->base_address) = regvalue;
    
    ETM_ADR_COMP_REG(handle->base_address, comparator) = p_range->low_address;
    ETM_ACC_TYPE_REG(handle->base_address, comparator) = (ETM_ACCTYPE_WORD |
                                                          ETM_ACCTYPE_INST_EXE);
    ETM_ADR_COMP_REG(handle->base_address, comparator+1) =
                                                          p_range->high_address;
    ETM_ACC_TYPE_REG(handle->base_address, comparator+1) = (ETM_ACCTYPE_WORD |
                                                          ETM_ACCTYPE_INST_EXE);

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {

		/* Set the data registers to include everything, could get disabled on call
		 *  to enable tracing.
		 *  Well, only enable if none of the individual or range comparators have
		 *   been selected.
		 */
		if(((ETM_VDATA_IER_REG(handle->base_address) & 0xFFFF) == 0) &&
		   (ETM_VDATA_IE_REG(handle->base_address) == 0)) {
			ETM_VDATA_IER_REG(handle->base_address) = ETM_VDATA_IER_EXCLUDE_ONLY;
		}
    }
    
    handle->configured = true;
    return ETM_SUCCESS;
}

/******************************************************************************/
/*! \copydoc etm_config_data_address
 */
enum etm_error etm_config_data_address(etmhandle_t const handle,
                                   const struct etm_data_address *const p_data)
{
	if(check_etm_type(handle->base_address) == ETM_TYPE)
	{
		int32_t        comparator;
		uint32_t       regvalue;
		enum etm_error status;

		status = etm_validate_input(handle);
		if(status != ETM_SUCCESS) {
			if((handle != NULL) && (handle->p_callback != NULL)) {
				handle->p_callback(handle, __FUNCTION__, status);
			}
			return status;
		}

		/*
		 *  SEE TODO 1 DESCRIPTION
		 */

		 /* Setting the data address only consumes a single comparator, check to
		 *  make sure one is available, account for the added resources, set
		 *  the address passed in as parameter to the selected comparator register,
		 *  and set the remaining ETM registers for a data address comparison.
		 */
		if(handle->active_count >= handle->hardware.max_address_comparators) {
			if(handle->p_callback != NULL) {
				handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
			}
			return ETM_ERR_RESOURCE;
		}
		handle->active_count++;

		/* Set the specific values for the access type register from input
		 * paramters
		 */
		switch(p_data->type) {
			case ETM_DATA_READ:
				regvalue = ETM_ACCTYPE_DATA_R;
			break;
			case ETM_DATA_WRITE:
				regvalue = ETM_ACCTYPE_DATA_W;
			break;
			case ETM_DATA_READ_OR_WRITE:  /* Set default to read or write */
			default:
				regvalue = ETM_ACCTYPE_DATA_RW;
			break;
		}
		switch(p_data->width) {
			case ETM_WIDTH_8BIT:
				regvalue |= ETM_ACCTYPE_BYTE;
			break;
			case ETM_WIDTH_16BIT:
				regvalue |= ETM_ACCTYPE_HALFWORD;
			break;
			case ETM_WIDTH_32BIT:
			default:
				regvalue |= ETM_ACCTYPE_WORD;
			break;
		}

		for(comparator = 0;
				comparator < handle->hardware.max_address_comparators;
				comparator++) {
			if((handle->active_comparators & (1 << comparator)) == 0) {
				handle->active_comparators |= (1 << comparator);
				break;
			}
		}

		ETM_ADR_COMP_REG(handle->base_address, comparator) = p_data->address;
		ETM_ACC_TYPE_REG(handle->base_address, comparator) = regvalue;

		if(p_data->r_type == ETM_EXCLUDE) {
			ETM_VDATA_IE_REG(handle->base_address) |= ((1 << comparator) << 16);
		}
		else {
			ETM_VDATA_IE_REG(handle->base_address) |= (1 << comparator);
		}

		/* Make sure the exclude only flag is not set in the control register */
		ETM_VDATA_IER_REG(handle->base_address) &= ~ETM_VDATA_IER_EXCLUDE_ONLY;
		handle->configured = true;
		return ETM_SUCCESS;
	}
	else //PTM implementation
	{
		//Return error, data address tracing not supported
		return ETM_ERR_NOT_SUPPORTED;
	}
}

/******************************************************************************/
/*! \copydoc etm_config_data_range
 */
enum etm_error etm_config_data_range(etmhandle_t const handle,
                                   const struct etm_data_range *const p_data)
{
	if(check_etm_type(handle->base_address) == ETM_TYPE)
	{
		int32_t        comparator;
		uint32_t       regvalue;
		enum etm_error status;
		bool  found = false;

		status = etm_validate_input(handle);
		if(status != ETM_SUCCESS) {
			if((handle != NULL) && (handle->p_callback != NULL)) {
				handle->p_callback(handle, __FUNCTION__, status);
			}
			return status;
		}

		if(p_data->low_address >= p_data->high_address) {
			if(handle->p_callback != NULL) {
				handle->p_callback(handle, __FUNCTION__, ETM_ERR_ADDRESS_ORDER);
			}
			return ETM_ERR_ADDRESS_ORDER;
		}

		/* Set the specific values for the access type register from input
		 * paramters
		 */
		switch(p_data->acc_type) {
			case ETM_DATA_READ:
				regvalue = ETM_ACCTYPE_DATA_R;
			break;
			case ETM_DATA_WRITE:
				regvalue = ETM_ACCTYPE_DATA_W;
			break;
			case ETM_DATA_READ_OR_WRITE:  /* Set default to read or write */
			default:
				regvalue = ETM_ACCTYPE_DATA_RW;
			break;
		}
		switch(p_data->width) {
			case ETM_WIDTH_8BIT:
				regvalue |= ETM_ACCTYPE_BYTE;
			break;
			case ETM_WIDTH_16BIT:
				regvalue |= ETM_ACCTYPE_HALFWORD;
			break;
			case ETM_WIDTH_32BIT:
			default:
				regvalue |= ETM_ACCTYPE_WORD;
			break;
		}

		/*
		 *  SEE TODO 1 DESCRIPTION
		 */

		/* Setting a comparison range requires a pair of comparators starting on an
		 *  even boundary. Verify availablity, locate the first available pair and
		 *  set control, comparator, and access type registers.
		 */
		for(comparator = 0;
				comparator < handle->hardware.max_address_comparators;
				comparator+=2) {
			if((handle->active_comparators & (3 << comparator)) == 0) {
				handle->active_comparators |= (3 << comparator);
				found = true;
				break;
			}
		}

		if(found == false) {
			if(handle->p_callback != NULL) {
				handle->p_callback(handle, __FUNCTION__, ETM_ERR_RESOURCE);
			}
			return ETM_ERR_RESOURCE;
		}
		handle->active_count += 2;

		/*>>1(divide by 2) to get pair num*/
		if(p_data->r_type == ETM_EXCLUDE) {
			ETM_VDATA_IER_EXCLUDE(handle->base_address, (comparator >> 1));
		}
		else {
			/* Make sure exclude only bit is not set */
			ETM_VDATA_IER_REG(handle->base_address) &= ~ETM_VDATA_IER_EXCLUDE_ONLY;
			ETM_VDATA_IER_INCLUDE(handle->base_address, (comparator >> 1));
		}

		ETM_ADR_COMP_REG(handle->base_address, comparator) = p_data->low_address;
		ETM_ACC_TYPE_REG(handle->base_address, comparator) = regvalue;
		comparator++;
		ETM_ADR_COMP_REG(handle->base_address, comparator) = p_data->high_address;
		ETM_ACC_TYPE_REG(handle->base_address, comparator) = regvalue;

		handle->configured = true;
		return ETM_SUCCESS;
	}
	else //PTM implementation
	{
		//Return error, data address tracing not supported
		return ETM_ERR_NOT_SUPPORTED;
	}
}

/******************************************************************************/
/*! \copydoc etm_get_dcm
 */
enum etm_error etm_get_dcm(etmhandle_t const handle, struct etm_dcm *p_dcm)
{
    uint32_t       regvalue;
    enum etm_error status;
    
    /* The validate_input verifies the ETM is inactive */
    status = etm_validate_input(handle);
    if(status != ETM_SUCCESS) {
        if((handle != NULL) && (handle->p_callback != NULL)) {
            handle->p_callback(handle, __FUNCTION__, status);
        }
        return status;
    }
    
    if(p_dcm == NULL) {
        if(handle->p_callback != NULL) {
            handle->p_callback(handle, __FUNCTION__, ETM_ERR_PARAMETER);
        }
        return ETM_ERR_PARAMETER;
    }
    
    /* Need to get the endian value, default to little endian */
#ifdef _big_endian__
    p_dcm->endian = 0;
#else
    p_dcm->endian = 1;
#endif

    /* Instrinsic function that returns the value of the CPSR (Current program
     *  status register), Thumb mode is indicated with bit 0x20
     */
    regvalue = _get_CPSR();
    if(regvalue & 0x20) {
        p_dcm->target_arm_mode = false;
    }
    else {
        p_dcm->target_arm_mode = true;
    }
    
    /* From a comment in the DeviceConfigArm.cpp file, the decoder only accepts
     *  an 8-bit packet size, value hard-coded to 0x8.
     */
    p_dcm->packet_size = 0x8;
    
    p_dcm->id_register   = ETM_ID_REG(handle->base_address);

    if(check_etm_type(handle->base_address) == ETM_TYPE)
    {
    	p_dcm->data_access   = ((ETM_CONTROL_REG(handle->base_address)) >> 2) & 0x3;
    }

    p_dcm->control_reg   = ETM_CONTROL_REG(handle->base_address);
    p_dcm->config_reg    = ETM_CONFIG_REG(handle->base_address);
    p_dcm->sync_freq_reg = ETM_SYNC_FREQ_REG(handle->base_address);
    p_dcm->signal_size   = 0;
    
    return ETM_SUCCESS;
}
/******************************************************************************/
/*! \brief Private validation method to check input parameters

    All configuration API functions will call this function to validate the
    paramters that are passed into the function. The ETM control register is
    checked to make sure the programming bit is set, verifying the configuration
    is allowed.

    \param[in]  handle - specific ETM handle
    
    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
*/
inline enum etm_error etm_validate_input(etmhandle_t const handle)
{
    if((handle == NULL) || (handle->base_address == 0)) {
        return ETM_ERR_HANDLE;
    }
    if((ETM_CONTROL_REG(handle->base_address) & ETM_CTRL_ETM_PROGRAM) == 0) {
        return ETM_ERR_TRACE_ACTIVE;
    }
    return ETM_SUCCESS;
}





