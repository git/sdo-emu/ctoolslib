#ifndef __ETM_REGISTERS_H
#define __ETM_REGISTERS_H
/*
 * etm_registers.h
 *
 * Embedded Trace Macrocell (ETM) Library Register definitions
 *
 * Copyright (C) 2009, 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/*! \file etm_registers.h
    \brief ETM Architecture Specification register definitions, refer to the
            specification version 3.4 for details on the registers defined
            below.
*/


#include <stdint.h>

#define ETM_REGSPACE_SIZE  0x300  /* ETM Trace Registers 0x000-0x2FC */

/******************************************************************************/
/*! ETM Control Register (R/W)  (Offset 0x000)
    - [27:25] Core select
    - [24] Instrumentation resources access control
    - [23] Disable software writes
    - [22] Disable register writes from the debugger
    - [21] Port size upper bit[3], use with bits [6:4]
    - [20] Data-only mode -- NOT SUPPORTED
    - [19] Filter (CPRT) Coprocessor Register Transfers
    - [18] Suppress data
    - [17:16] Port mode, uses bit 13 as upper bit[2]
    - [15:14] Context ID size
    - [13] Port mode [2], trace port clocking mode with bits 17:16
    - [12] Cycle-accurate tracing
    - [11] ETM port selection, controls external ETMEN pin
    - [10] ETM programming
    - [9]  Debug request control, 1 and trigger, the DBGRQ output is asserted
    - [8]  Branch output, 1: all branch addresses are output
    - [7]  Stall processor -- NOT SUPPORTED
    - [6:4] Port size [2:0]
    - [3:2] Data access, b10-address only, b11-address and data
    - [1]  Monitor CPRT
    - [0]  ETM power down, enables ETM power to be controlled externally
 */
#define ETM_CTRL_OFFSET   0x000
                                                   /*! \cond include_all_defs */
#define ETM_CONTROL_REG(ba) *(volatile uint32_t *)((ba)+ETM_CTRL_OFFSET)

#define ETM_CTRL_CORE_SELECT(val) ((val & 0x7) << 25)
#define ETM_CTRL_INST_RES_CTRL    (1 << 24)  /* Instrumentation Resource Ctl */
#define ETM_CTRL_DISABLE_SW_WR    (1 << 23)  /* Disable software writes */
#define ETM_CTRL_DISABLE_DBG_WR   (1 << 22)
#define ETM_CTRL_PORT_SIZE_3      (1 << 21)  /* Used with bits [6:4] */
#define ETM_CTRL_INST_TRC_DISABLE (1 << 20)  /* Data only tracing */
#define ETM_CTRL_FILTER_CPRT      (1 << 19)  /* Coprocessor Register Transfers*/
#define ETM_CTRL_SUPPRESS_DATA    (1 << 18)

/* PORT MODE [2:0], [2] is bit 13, [1:0] is bits [17:16] */
#define ETM_CTRL_PORT_MODE_DYN ((0 << 13) | (0 << 16))
#define ETM_CTRL_PORT_MODE_1_1 ((1 << 13) | (0 << 16))
#define ETM_CTRL_PORT_MODE_1_2 ((1 << 13) | (2 << 16))
#define ETM_CTRL_PORT_MODE_1_3 ((1 << 13) | (1 << 16))
#define ETM_CTRL_PORT_MODE_1_4 ((1 << 13) | (3 << 16))
#define ETM_CTRL_PORT_MODE_2_1 ((0 << 13) | (1 << 16))
/* CONTEXT ID SIZE [15:14] */
#define ETM_CTRL_CONTEXTID_NONE (0 << 14)
#define ETM_CTRL_CONTEXTID_8    (1 << 14)
#define ETM_CTRL_CONTEXTID_16   (2 << 14)
#define ETM_CTRL_CONTEXTID_32   (3 << 14)

#define ETM_CTRL_CYCLE_ACCURATE   (1 << 12)
#define ETM_CTRL_ETMEN            (1 << 11)  /* Controls external ETMEN pin */
#define ETM_CTRL_ETM_PROGRAM      (1 << 10)  /* ETM is being programmed */
#define ETM_CTRL_DBGRQ            (1 << 9)
#define ETM_CTRL_BRANCH_OUTPUT    (1 << 8)
#define ETM_CTRL_STALL_PROCESSOR  (1 << 7)

/* PORT SIZE [3:0], [3] is bit 21, [2:0] bits [6:4] */
#define ETM_CTRL_PORT_1BIT     (1 << 21)
#define ETM_CTRL_PORT_2BIT    ((1 << 21) | (1 << 4))
#define ETM_CTRL_PORT_4BIT    (0 << 4)
#define ETM_CTRL_PORT_8BIT    (1 << 4)
#define ETM_CTRL_PORT_16BIT   (2 << 4)
#define ETM_CTRL_PORT_24BIT   (3 << 4)
#define ETM_CTRL_PORT_32BIT   (4 << 4)
#define ETM_CTRL_PORT_48BIT   (5 << 4)
#define ETM_CTRL_PORT_64BIT   (6 << 4)

/* DATA ACCESS TRACING [3:2] */
#define ETM_CTRL_TRACE_DATA_MASK   0xC
#define ETM_CTRL_TRACE_NO_DATA (0 << 2)
#define ETM_CTRL_TRACE_DATA    (1 << 2)
#define ETM_CTRL_TRACE_ADDR    (2 << 2)
#define ETM_CTRL_TRACE_BOTH    (3 << 2)
    
#define ETM_CTRL_MONITOR_CPTR     (1 << 1)
#define ETM_CTRL_PWRDOWN          (1 << 0)

/*! \endcond */
/******************************************************************************/
/*! ETM Configuration Code Register (R)  (Offset 0x004)
    - Defines the implementation defined configuration
    - A8 value reads 0x8d294024
    - [7:4] Number of data value comparators   (2)
    - [3:0] Number of address comparator pairs (4)
 */
#define ETM_CONFIG_OFFSET  0x004
                                                   /*! \cond include_all_defs */
#define ETM_CONFIG_REG(ba) *(volatile uint32_t *)((ba)+ETM_CONFIG_OFFSET)

#define ETM_CONFIG_MEM_MAP_SUPPORT  (1 << 27)
#define ETM_CONFIG_TRACE_START_STOP (1 << 26)
#define ETM_CONFIG_NUM_CONTEXT_IDS(regval)  (((regval) >> 24) & 0x3)
#define ETM_CONFIG_FIFOFULL         (1 << 23)
#define ETM_CONFIG_NUM_EXT_OUTPUTS(regval)  (((regval) >> 20) & 0x7)
#define ETM_CONFIG_NUM_EXT_INPUTS(regval)   (((regval) >> 17) & 0x7)
#define ETM_CONFIG_SEQUENCER        (1 << 16)
#define ETM_CONFIG_NUM_COUNTERS(regval)     (((regval) >> 13) & 0x7)
#define ETM_CONFIG_NUM_DECODERS(regval)     (((regval) >> 8) & 0x1f)
#define ETM_CONFIG_NUM_DATA_COMP(regval)    (((regval) >> 4) & 0xf)
#define ETM_CONFIG_NUM_ADDR_COMP(regval)    ((regval) & 0xf)

/*! \endcond */
/******************************************************************************/
/*! ETM Trigger Event Register (R/W post v3.1)  (Offset 0x008)
    - Bit assignements for the register are:
      - [6:0]   - Resource A
      - [13:7]  - Resource B
      - [16:14] - Fcn.
    - Each Resource space is defined as:
      - [6:4] - Resource type
      - [3:0] - Resource index
 */
#define ETM_TRIGGER_EVENT_OFFSET 0x008
                                                   /*! \cond include_all_defs */
#define ETM_TRIGGER_EVENT_REG(ba) \
                         (*(volatile uint32_t *)((ba)+ETM_TRIGGER_EVENT_OFFSET))

/* RESOURCE TYPE DEFINITIONS */
#define ETM_TGR_SINGLE_ADDR_COMP(idx) ((0 << 4) | ((idx) & 0xf)) /* range 0-15 */
#define ETM_TGR_RANGE_ADDR_COMP(idx)  ((1 << 4) | ((idx) & 0x7)) /* range 0-7  */
#define ETM_TGR_INSTRUMENTATION(idx)  ((1 << 4) | (idx))         /* range 8-11 */
#define ETM_TGR_ICE_WP_COMP(idx)      ((2 << 4) | ((idx) & 0x7)) /* range 0-7  */
#define ETM_TGR_MEM_MAP_DECODE(idx)   ((3 << 4) | ((idx) & 0xf)) /* range 0-15 */
#define ETM_TGR_COUNTER_AT_ZERO(idx)  ((4 << 4) | ((idx) & 0x3)) /* range 0-3  */
#define ETM_TGR_SEQUENCER(idx)        ((5 << 4) | (idx))         /* range 0-2  */
#define ETM_TGR_CONTEXT_ID_COMP(idx)  ((5 << 4) | (idx))         /* range 8-10 */
#define ETM_TGR_TRACE_START_STOP      ((5 << 4) | 0xf)
#define ETM_TGR_EXT_INPUT(idx)        ((6 << 4) | ((idx) & 0x3)) /* range 0-3  */
#define ETM_TGR_EX_EXT_INPUT(idx)     ((6 << 4) | (idx))         /* range 8-11 */
#define ETM_TGR_NON_SECURE            ((6 << 4) | 0xd)
#define ETM_TGR_TRACE_PROHIBITED      ((6 << 4) | 0xe)
#define ETM_TGR_HARD_WIRED_INPUT      ((6 << 4) | 0xf)

/* BOOLEAN FUNCTION ENCODING FOR EVENTS */
#define ETM_EVT_A             (0 << 14)
#define ETM_EVT_NOTA          (1 << 14)
#define ETM_EVT_A_AND_B       (2 << 14)
#define ETM_EVT_NOTA_AND_B    (3 << 14)
#define ETM_EVT_NOTA_AND_NOTB (4 << 14)
#define ETM_EVT_A_OR_B        (5 << 14)
#define ETM_EVT_NOTA_OR_B     (6 << 14)
#define ETM_EVT_NOTA_OR_NOTB  (7 << 14)

/*! \endcond */
/******************************************************************************/
/*! ETM Status Register (R/W)  (Offset 0x010)
    - [3] Trigger bit
        - Set when the trigger occurs, and prevents the trigger from being
           output until the ETM is next programmed.
    - [2] Trace start/stop resource status\n
        - If set to 1, it indicates that a trace on address has been matched,
           without a corresponding trace off address match.
    - [1] Programming bit value\n
        - Contains the current effective value, must wait for this bit to be
           set before the ETM can get programmed.
    - [0] Untraced overflow flag
 */
#define ETM_STATUS_OFFSET 0x010
                                                   /*! \cond include_all_defs */
#define ETM_STATUS_REG(ba) (*(volatile uint32_t *)((ba)+ETM_STATUS_OFFSET))

#define ETM_STAT_TRIGGER_FLAG (1 << 3)
#define ETM_STAT_TRACE_ON     (1 << 2)
#define ETM_STAT_PROGRAM_BIT  (1 << 1)
#define ETM_OSLOCK_STAT_BIT   (1 << 1)
#define ETM_STAT_OVERFLOW      1

/*! \endcond */
/******************************************************************************/
/*! Trace Start/Stop Resource Control Register (Offset 0x018)
    - [31:16] Stop address comparator
    - [15:0]  Start address comparator
 */
#define ETM_TRACE_SS_OFFSET  0x018
                                                   /*! \cond include_all_defs */
#define ETM_TRACE_SS_REG(ba) (*(volatile uint32_t *)((ba)+ETM_TRACE_SS_OFFSET))

#define ETM_TRACE_SET_START(badr, c)  ETM_TRACE_SS_REG(badr) |= (1 << (c))
#define ETM_TRACE_SET_END(badr, c)    ETM_TRACE_SS_REG(badr) |= (1 << ((c)+16))

#define ETM_TRACE_SS_START_MASK  0xFFFF
#define PTM_TRACE_SS_START_MASK  0xFF

/*! \endcond */
/******************************************************************************/
/*! TraceEnable Control 2 (Include/Exclude) Register (Offset 0x01C)
    - [15:0]  Include/Exclude comparator selection
 */
#define ETM_TRACE_IE_OFFSET  0x01C
                                                   /*! \cond include_all_defs */
#define ETM_TRACE_IE_REG(ba) *(volatile uint32_t *)((ba)+ETM_TRACE_IE_OFFSET)


/*! \endcond */
/******************************************************************************/
/*! TraceEnable Event Register (Offset 0x020)
    - Refer to \link ETM_TRIGGER_EVENT_OFFSET \endlink Trigger Event register
      definition above
 */
#define ETM_TRACE_EVT_OFFSET  0x020
                                                   /*! \cond include_all_defs */
#define ETM_TRACE_EVT_REG(ba) (*(volatile uint32_t *)((ba)+ETM_TRACE_EVT_OFFSET))


/*! \endcond */
/******************************************************************************/
/*! TraceEnable Control 1 Register (Offset 0x024)
    - [25]  Trace control enable
    - [24]  Exclude/include flag (0:Include, 1:Exclude)
    - [7:0] Address range comparators for include/exclude control
 */
#define ETM_TRACE_CTRL_OFFSET  0x024
                                                   /*! \cond include_all_defs */
#define ETM_TRACE_CTRL_REG(ba) *(volatile uint32_t *)((ba)+ETM_TRACE_CTRL_OFFSET)

#define ETM_TRACE_CTRL_ENABLE  (1 << 25)
#define ETM_TRACE_CTRL_EXCLUDE (1 << 24)

#define ETM_TRACE_CTRL_TYPE_MASK  0xff
#define PTM_TRACE_CTRL_TYPE_MASK  0xf

/*! \endcond */
/******************************************************************************/
/*! FIFOFULL Level Register (Offset 0x02C)
    - [7:0] Bytes left in FIFO, below which suppress data signal is asserted
 */
#define ETM_FIFO_LEVEL_OFFSET  0x02C
                                                   /*! \cond include_all_defs */
#define ETM_FIFO_LEVEL_REG(ba) (*(volatile uint32_t *)((ba)+ETM_TRACE_CTRL_OFFSET))

/*! \endcond */
/******************************************************************************/
/*! ViewData Event Register (Offset 0x030)
    - Refer to \link ETM_TRIGGER_EVENT_OFFSET \endlink Trigger Event register
      definition above
 */
#define ETM_VDATA_EVT_OFFSET  0x030
                                                   /*! \cond include_all_defs */
#define ETM_VDATA_EVT_REG(ba) (*(volatile uint32_t *)((ba)+ETM_VDATA_EVT_OFFSET))


/*! \endcond */
/******************************************************************************/
/*! ViewData Control 1 (Include/Exclude) Register (Offset 0x034)
    - [31:16] Exclude comparator selection
    - [15:0]  Include comparator selection
 */
#define ETM_VDATA_IE_OFFSET  0x034
                                                   /*! \cond include_all_defs */
#define ETM_VDATA_IE_REG(ba) (*(volatile uint32_t *)((ba)+ETM_VDATA_IE_OFFSET))


/*! \endcond */
/******************************************************************************/
/*! ViewData Control 3 (Include/Exclude Range Comp) Register (Offset 0x03C)
    - [16]   Exclude only flag
    - [15:8] Exclude address range comparator selection
    - [7:0]  Include address range comparator selection
 */
#define ETM_VDATA_IER_OFFSET  0x03C
                                                   /*! \cond include_all_defs */
#define ETM_VDATA_IER_REG(ba) (*(volatile uint32_t *)((ba)+ETM_VDATA_IER_OFFSET))

#define ETM_VDATA_IER_EXCLUDE_ONLY    (1 << 16)
#define ETM_VDATA_IER_EXCLUDE(badr,c) ETM_VDATA_IER_REG(badr) |= (1 << (c)) << 8
#define ETM_VDATA_IER_INCLUDE(badr,c) ETM_VDATA_IER_REG(badr) |= (1 << (c))

/*! \endcond */
/******************************************************************************/
/*! Address Comparator Value Registers  (Offset 0x040)
    - Offset 0x040 - 0x07C
      - A8 implementation supports 8 comparators 0x040 - 0x060
    - 32-bit address value
 */
#define ETM_ADR_COMP_OFFSET 0x040
                                                   /*! \cond include_all_defs */
#define ETM_ADR_COMP_REG(ba,n) \
    (*(volatile uint32_t *)(((ba)+ETM_ADR_COMP_OFFSET)+((n)*4)))

/*! \endcond */
/******************************************************************************/
/*! Address Access Type Registers  (Offset 0x080)
    - Offset 0x080 - 0x0BC
      - A8 implementation supports 8 registers 0x080 - 0x0a0
    - [11:10] Secure level control
    - [9:8]   Context ID comparator control
    - [7]     Exact match bit
    - [6:5]   Data comparison control
    - [4:3]   Comparison access size
    - [2:0]   Access type
 */
#define ETM_ACC_TYPE_OFFSET 0x080
                                                   /*! \cond include_all_defs */
#define ETM_ACC_TYPE_REG(ba,n) \
    (*(volatile uint32_t *)(((ba)+ETM_ACC_TYPE_OFFSET)+((n)*4)))

/* Comparison access size options */
#define ETM_ACCTYPE_BYTE      0
#define ETM_ACCTYPE_HALFWORD (1 << 3)
#define ETM_ACCTYPE_WORD     (3 << 3)
/* Access type options */
#define ETM_ACCTYPE_INST_EXE  1
#define ETM_ACCTYPE_DATA_RW   4
#define ETM_ACCTYPE_DATA_R    5
#define ETM_ACCTYPE_DATA_W    6


/*! \endcond */
/******************************************************************************/
/*! Synchronization Frequency Register  (Offset 0x1E0)
  - [11:0] Holds the trace synchronization frequency value
 */
#define ETM_SYNC_FREQ_OFFSET 0x1E0
                                                   /*! \cond include_all_defs */
#define ETM_SYNC_FREQ_REG(ba) (*(volatile uint32_t *)((ba)+ETM_SYNC_FREQ_OFFSET))

/*! \endcond */
/******************************************************************************/
/*! ETM ID Register  (Offset 0x1E4)
  - [11:8] Major version number
  - [7:4]  Minor version number
 */
#define ETM_ID_OFFSET 0x1E4
                                                   /*! \cond include_all_defs */
#define ETM_ID_REG(ba) (*(volatile uint32_t *)((ba)+ETM_ID_OFFSET))

#define ETM_TRACE_ID_OFFSET 0x200
#define ETM_TRACE_ID_REG(ba) (*(volatile uint32_t *)((ba)+ETM_TRACE_ID_OFFSET))

#define ETM_OS_LAR_OFFSET 0x300
#define ETM_OS_LAR_REG(ba) (*(volatile uint32_t *)((ba)+ETM_OS_LAR_OFFSET))

#define ETMOSLSR_IMPLEMENTED (1 << 3)

#define ETM_OS_LSR_OFFSET 0x304
#define ETM_OS_LSR_REG(ba) (*(volatile uint32_t *)((ba)+ETM_OS_LSR_OFFSET))

/*! \endcond */
/******************************************************************************/
/*! ETM Power-Down Status Register (RO v3.3 or later)
    Offset 0x314
    - Bit assignements for the register are:
      - [1] Sticky register state
            -  0: ETM Trace registers have not been powered down since last read
            -  1: ETM has been powered down, registers have lost their state
      - [0] ETM powered up bit (1: ETM is powered)
 */
#define ETM_POWER_DOWN_OFFSET 0x314
                                                   /*! \cond include_all_defs */
#define ETM_POWER_DOWN_REG(ba) (*(volatile uint32_t *)((ba)+ETM_POWER_DOWN_OFFSET))

#define ETM_POWER_REG_LOST_STATE  (1 << 1)
#define ETM_POWERED_UP             1

/*! \endcond */
/******************************************************************************/
/*! 

 */
#define ETM_LOCK_ACC_OFFSET  0xFB0
                                                   /*! \cond include_all_defs */
#define ETM_LOCK_ACC_REG(ba) (*(volatile uint32_t *)((ba)+ETM_LOCK_ACC_OFFSET))

#define ETM_UNLOCK_VALUE 0xC5ACCE55

#define ETM_OS_UNLOCK_VALUE 0x5A5A5A5A // Use any value other than 0xC5ACCE55, to unlock the PTM trace registers

/*! \endcond */
/******************************************************************************/
/*! 

 */
#define ETM_LOCK_STAT_OFFSET  0xFB4
                                                   /*! \cond include_all_defs */
#define ETM_LOCK_STAT_REG(ba) (*(volatile uint32_t *)((ba)+ETM_LOCK_STAT_OFFSET))

#define ETM_LOCKED               (1 << 1)
#define ETM_LOCKING_IMPLEMENTED   1

/*! \endcond */
/******************************************************************************/
/*! 

 */

#endif

