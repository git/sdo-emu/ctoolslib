#ifndef __ETM_LIBRARY_H
#define __ETM_LIBRARY_H
/*
 * etm_lib.h
 *
 * ARM Embedded Trace Macrocell (ETM) Library API Definitions
 *
 * Copyright (C) 2010, 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdint.h>  /* The library uses C99 exact-width integer types */

#ifdef __cplusplus
extern "C" {
#endif

/*! \file etm_lib.h
    \brief ETM Library Enumerations, Definitions, Structures, and Function
           Prototypes
*/

/* The mainpage for doxygen moved to the end of this file */

/*******************************************************************************
 *
 * Public Enumerations, Definitions, and Structures
 *
 ******************************************************************************/

/* Note - The version definitions must have the end of line immediately after
 *         the value (packaging script requirement)
 */
#define ETMLIB_MAJOR_VERSION    (0x0)
                                      /*!< Major version number -
                                            Incremented for API changes
                                      */
#define ETMLIB_MINOR_VERSION    (0x6)
                                      /*!< Minor version number -
                                            Incremented for bug fixes
                                      */

/******************************************************************************/
/*! \par bool
    \brief Create boolean definitions if not already defined
*/
#if !defined(__bool_true_false_are_defined) && !defined(_STM_Logging)
typedef enum
{ 
    false = 0,
    true = 1
    
}bool;
#endif

/******************************************************************************/
/*! \par etm_error
    \brief ETM Library Error Codes
*/
enum etm_error {
    ETM_SUCCESS           = 0,  /*!< Function completed successfully          */
    ETM_ERR_HANDLE        = -1, /*!< Invalid parameter in handle structure    */
    ETM_ERR_MEMORY        = -2, /*!< Could not allocated necessary memory     */
    ETM_ERR_OWNERSHIP     = -3, /*!< Already opened for this core by another
                                      instance or the debugger */
    ETM_ERR_COMPATIBILITY = -4, /*!< Library is not register compatible with the
                                      hardware version */
    ETM_ERR_CONFIGURATION = -5, /*!< The etm_enable_trace function called
                                      without a valid configuration */
    ETM_ERR_STOP_TIMEOUT  = -6, /*!< Timeout waiting for programming bit, after
                                      getting set in configuration register, to
                                      show it is set in the status register */
    ETM_ERR_TRACE_ACTIVE  = -7, /*!< Trying to make a configuration API call
                                      while a trace session is active */
    ETM_ERR_RESOURCE      = -8, /*!< All of the ETM hardware resources used */
    ETM_ERR_ADDRESS_ORDER = -9, /*!< Range configuration where the low address
                                      is greater than the high address */
    ETM_ERR_RANGE_TYPE    = -10,/*!< Trying to change the range type (include/
                                      exclude) from a previous configuration for
                                      a single session. Include and exclude not
                                      allowed for a single session */
    ETM_ERR_POWER         = -11,/*!< The ETM power down status register has a
                                      status that the module is not powered */
    ETM_ERR_MEM_MAPPING   = -12,/*!< The call to map physical address to a
                                      virtual address has failed */
    ETM_ERR_PARAMETER     = -13,/*!< One of the input parameters is invalid */
    ETM_ERR_TRACE_START   = -14, /*!< Trace start/stop logic enabled without a
                                      valid start comparitor configured. */
    ETM_ERR_NOT_SUPPORTED = -15 /*!< Feature not supported by the current
                                     PTM implementation. */
};

/******************************************************************************/
/*! \par etm_trace_type
    \brief Type of address to trace, valid options are for Instruction and Data
            addresses, or Instruction only addresses. Tracing only data address
            without instructions is not allowed for this ETM implemenation. For
            PTM implementation, Instruction only addresses are traceable.
            data address tracing is not supported.
*/
enum etm_trace_type {
    ETM_TRACE_PROGRAM_ONLY = 1, /*!< Trace instruction addresses only, no data*/
    ETM_TRACE_ALL               /*!< Trace instruction and data addresses, only applicable for ETM Implementation.
                                     This option not available for PTM implementation */
};

/******************************************************************************/
/*! \par etm_range_type
    \brief Include or exclude option for a specified range.
*/
enum etm_range_type {
    ETM_INCLUDE = 1,
    ETM_EXCLUDE
};

/******************************************************************************/
/*! \par etm_data_access_type
    \brief Select tracing for a data address read/write or both.
*/
enum etm_data_access_type {
    ETM_DATA_READ = 1,
    ETM_DATA_WRITE,
    ETM_DATA_READ_OR_WRITE
};

/******************************************************************************/
/*! \par etm_access_width
    \brief These values are used to select the comparison access size field,
            bits [4:3] of the Address Access Type registers at offsets
            0x80 to 0xBC.
           The access width field is used to generate a match when the byte,
            halfword, or word at the selected address matches, even if the
            32-bit address in the Address Comparator Value Register does not
            match completely.
*/
enum etm_access_width {
    ETM_WIDTH_8BIT = 1,
    ETM_WIDTH_16BIT,
    ETM_WIDTH_32BIT
};

/******************************************************************************/
/*! \par etmhandle_t
    \brief opaque handle pointer used in API calls 
*/ 
typedef struct _etm_handle * etmhandle_t;


/******************************************************************************/
/*! \par etm_config
    \brief Configuration Options 
*/ 
struct etm_config {
    uint32_t base_address;
    /*!< ETM module base address value for the specific hardware
          implementation.
    */
    
    uint8_t core_id; /*!< 0-3: ARM core id in the MPU SS. */

    void (*p_callback)(etmhandle_t const handle,
                        const char *func,
                        enum etm_error err);
    /*!< Error callback pointer that can be defined to a valid function or NULL
          if not used. This function will get called directly before the return
          from an error in the API functions.
    */
};

/******************************************************************************/
/*! \par etm_trace_control
    \brief ETM trace control structure 
*/ 
struct etm_trace_control {
    enum etm_trace_type type; /*!< Instruction or Instruction and data
                                    tracing enabled.
                               */
    bool       suppress_data; /*!< If enabled(true), causes data
                                    tracing to be disabled when the FIFO is
                                    close to overflow, set in the FIFOFULL
                                    level register. Not supported for PTM implementation.
                               */
    bool       monitor_cprt;   /*!< Enable tracing Coprocessor Register
                                         Transfers. Not supported for PTM implementation.
                                */
    bool       cycle_accurate_tracing; /*!< When enabled, a precise
                                        cycle count of executed instructions
                                        can be extracted from the trace.
                                        */
};

/******************************************************************************/
/*! \par etm_inst_range
    \brief ETM instruction range structure 
*/ 
struct etm_inst_range {
    uint32_t            low_address;  /*!< Instruction range start address */
    uint32_t            high_address; /*!< Instruction range end address, this
                                      value MUST be greater that the start.
                                      */
    enum etm_range_type type;         /*!< INCLUDE/EXCLUDE the range specified*/
};

/******************************************************************************/
/*! \par etm_data_address
    \brief ETM data address structure 
*/ 
struct etm_data_address {
    uint32_t                  address; /*!< Data address to trace */
    enum etm_range_type       r_type;  /*!< INCLUDE or EXCLUDE data address */
    enum etm_data_access_type type;    /*!< Read, Write, or Both */
    enum etm_access_width     width;   /*!< Byte, half-word, or word */
};

/******************************************************************************/
/*! \par etm_data_range
    \brief ETM data range structure 
*/ 
struct etm_data_range {
    uint32_t                  low_address;  /*!< Data range start address */
    uint32_t                  high_address; /*!< Data range end address */
    enum etm_range_type       r_type;       /*!< INCLUDE/EXCLUDE data range */
    enum etm_data_access_type acc_type;     /*!< Read, Write, or Both */
    enum etm_access_width     width;        /*!< Byte, half-word, or word */
};

/******************************************************************************/
/*! \par etm_dcm
    \brief ETM device configuration structure 
*/ 
struct etm_dcm {
    uint8_t  endian;          /*!< 1 - little, 0 - big */
    bool     target_arm_mode; /*!< true/false */
    uint8_t  packet_size;     /*!< 4,8,16... Hardcoded to 0x8 (8-bit) */
    uint32_t id_register;     /*!< ETM ID register */
    uint8_t  data_access;     /*!< Current config for data tracing */
    uint8_t  buffer_wrapped;  /*!< ETB buffer status */
    uint32_t control_reg;     /*!< ETM control register */
    uint32_t config_reg;      /*!< ETM configuration code register */
    uint32_t sync_freq_reg;   /*!< ETM synchronization frequency register*/
    uint32_t signal_size;     /*!< ETM bits in signal? HC to zero... */
    uint32_t sample_size;     /*!< ETB buffer size * 4 */
};

/*******************************************************************************
 *
 * Public Function Definitions
 *
 ******************************************************************************/

/******************************************************************************/
/*!
    Prior to using the ETM library, it must be opened for a specific instance
    associated with a specific core. The device power-down status register is
    checked first to verify that the ETM is powered up and the trace registers
    are accessible. Configuration API calls are allowed after etm_open has
    completed. The number of comparators and other chip specific values are
    discovered and stored during this function.\n
    For SMP configurations, it is expected the user will open an instance
    of the library per core and setup each identically.
     
    \param[out] p_handle - A pointer to a NULL etmhandle_t. If etm_open exits
                 with the return value of ETM_SUCCESS, the pointer is set to a
                 valid etmhandle_t.
    \param[in]  p_config - A pointer to a etm_config structure.
    
    \return The following etm_error enumerations are possible:
        \li  ETM_SUCCESS
        \li  ETM_ERR_HANDLE
        \li  ETM_ERR_MEMORY
        \li  ETM_ERR_OWNERSHIP
        \li  ETM_ERR_CONFIGURATION - base address value in configuration invalid
        \li  ETM_ERR_POWER
        \li  ETM_ERR_MEM_MAPPING
                            
*/
enum etm_error etm_open(etmhandle_t *const p_handle,
                        const struct etm_config *const p_config);

/******************************************************************************/
/*!
    Close will disable the ETM module and free the handle memory, also sets the
    handle to NULL that was passed to the API. The ETM module is disabled and
    necessary registers are returned to initial values. 
    
    \param[out] p_handle - specific ETM handle
    
    \return
        \li ETM_SUCCESS - module is disabled and handle memory released
        \li ETM_ERR_HANDLE
*/
enum etm_error etm_close(etmhandle_t *const p_handle);

/******************************************************************************/
/*!
    Provides the library major and minor versions and determines if the current
    revision of the library is compatible with the ETM module the library is
    opened for (handle). Also provides the current ETM version based on the
    ETM_ID register. A NULL pointer can get passed into the function if any of
    the parameters are not needed.

    \param[in]  handle  - specific ETM handle
    \param[out] p_major - major version of ETM library
    \param[out] p_minor - minor version of ETM library
    \param[out] p_etm_major - major version of ETM hardware module
    \param[out] p_etm_minor - minor version of ETM hardware module
    
    \return
        \li ETM_SUCCESS - compatible version
        \li ETM_ERR_HANDLE
*/
enum etm_error etm_get_version(etmhandle_t const handle,
                               uint32_t *p_major, uint32_t *p_minor,
                               uint32_t *p_etm_major, uint32_t *p_etm_minor);

/******************************************************************************/
/*!
    Provides information about the current status of the trace and trigger
    logic. The status value that is populated in the address that is passed as a
    parameter is defined in the etm_status enumeration.

    \param[in]  handle - specific ETM handle
    \param[out] p_status - ETM status returned, based on etm_status enumeration
    
    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
*/
enum etm_error etm_get_status(etmhandle_t const handle, uint32_t *p_status);

/******************************************************************************/
/*!
    This function will enable the trace system to start tracing based on the
    settings from the configuration API function call(s).  If this function is
    called after a stop trace has occurred, it will use the last configuration
    settings. The trace is stopped either by a call to etm_stop_trace or by an
    end trace trigger.

    \param[in]  handle - specific ETM handle
    \param[in]  p_control - etm_trace_control structure that defines parameters
                            specific to this trace session.

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_PARAMETER
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_CONFIGURATION
        \li ETM_ERR_TRACE_START

*/
enum etm_error etm_enable_trace(etmhandle_t const handle,
                           const struct etm_trace_control *const p_control);

/******************************************************************************/
/*!
    The ETM is halted by setting the programming bit. This function will not
    return until the ETM status register indicates the value of the programming
    bit has been set. This ensures the FIFO is empty before allowing register
    modifications.  The STOP_TIMEOUT error will get returned if the status
    register does not show the bit has been set in a reasonable amount of time.
    This function must be called with a successful return to be able to
    configure another trace session.

    \param[in]  handle - specific ETM handle

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_STOP_TIMEOUT

*/
enum etm_error etm_stop_trace(etmhandle_t const handle);

/******************************************************************************/
/*!
    This function is used to clear the previous configuration. Calling this
    function is not allowed during an active trace session.

    \param[in]  handle - specific ETM handle

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
*/
enum etm_error etm_config_clear(etmhandle_t const handle);

/******************************************************************************/
/*!
    Configure the ETM to trace all instructions or all instructions and data
    based on the input parameter to the enable trace function. This
    configuration starts tracing immediately when the etm_enable_trace function
    is called.

    \param[in]  handle - specific ETM handle

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_trace_on(etmhandle_t const handle);

/******************************************************************************/
/*!
    The start function sets an instruction address that is used to start tracing
    when the instruction is executed. This function can be called multiple times
    to setup multiple starting locations.
    
    \note When the <b>etm_config_inst_start</b> and <b>etm_config_inst_end</b>
    functions are used, they enable the Trace start/stop logic to enable and
    disable tracing. If the start configuration does not occur, or the start
    address is not executed, tracing will not get enabled, even though other
    configurations such as an instruction address range has been configured.

    \param[in]  handle - specific ETM handle
    \param[in]  start_address

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_inst_start(etmhandle_t const handle,
                                   const uint32_t start_address);

/******************************************************************************/
/*!
    The end function sets an instruction address that is used to stop tracing
    when the instruction is executed. This function can be called multiple times
    to setup multiple stop locations.

    \param[in]  handle - specific ETM handle
    \param[in]  end_address

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_inst_end(etmhandle_t const handle,
                                   const uint32_t end_address);

/******************************************************************************/
/*!
    This function will set a low and high instruction address range that is used
    to either include or exclude tracing while the address is in the specified
    range. Multiple ranges can be configured, but all must be the same range
    type (include or exclude) during a single trace session.

    \param[in]  handle - specific ETM handle
    \param[in]  p_range - pointer to etm_inst_range structure

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_ADDRESS_ORDER
        \li ETM_ERR_RANGE_TYPE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_inst_range(etmhandle_t const handle,
                                   const struct etm_inst_range *const p_range);

/******************************************************************************/
/*!
    Function used to configure tracing a specific data address. Only available for
    ETM implementations. Not supported for PTM implementations.

    \param[in]  handle - specific ETM handle
    \param[in]  p_data - pointer to etm_data_address structure

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_data_address(etmhandle_t const handle,
                                   const struct etm_data_address *const p_data);

/******************************************************************************/
/*!
    This function will set a low and high data address range that is used to
    either include or exclude tracing the specified access type while the
    address is in the specified range. Multiple configurations can be configured
    including and excluding ranges during a single trace session. Only available for
    ETM implementations. Not supported for PTM implementations.

    \param[in]  handle - specific ETM handle
    \param[in]  p_data - pointer to etm_data_range structure

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE
        \li ETM_ERR_TRACE_ACTIVE
        \li ETM_ERR_RESOURCE

*/
enum etm_error etm_config_data_range(etmhandle_t const handle,
                                   const struct etm_data_range *const p_data);


/******************************************************************************/
/*!
    This function will collect the information from the ETB and ETM registers
    for the application to build the necessary device configuration file.

    \param[in]  handle - specific ETM handle
    \param[in]  p_dcm  - pointer to etm_dcm structure

    \return
        \li ETM_SUCCESS
        \li ETM_ERR_HANDLE

*/
enum etm_error etm_get_dcm(etmhandle_t const handle, struct etm_dcm *p_dcm);

/******************************************************************************/
/******************************************************************************/
/* *****  END OF API FUNCTION DEFINITIONS  ***** */
/******************************************************************************/
/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __ETM_LIBARY_H */

/******************************************************************************/
/* *****  DOXYGEN  ***** */
/******************************************************************************/
/*! \mainpage
\par Introduction
The ETM Library provides the interface to configure and control instruction and
data tracing during program execution. A trace session is defined as the time
frame from the trace system getting armed (enabled and waiting for a start trace
condition), to the time when the trace completes, either from an end trace
condition that has been configured or a manual call to stop tracing.
\par
ETM configuration is not allowed during a trace session, it must occur between
the ETM open and arm API calls, or between the stop_trace and another arm_trace
call.
\par
The library supports the following configurations:
Trace everything
\li Configure an address to start tracing.
\li Configure an address to stop tracing.
\li Configure an address range to include or exclude tracing. \n<i>Instruction
    tracing can only be inclusive or exclusive during a session, not both.</i>
\li Configure a data address to trace
\li Configure a data address range to include or exclude tracing.
\par
Additional information provided by the library:
\li Version of the ETM library and module
\li Current status of the trace system
\li Clear the current configuration
\par Trace using start/stop versus setting a range:
Start/stop will trace everything between the start and stop address, including
any subroutines that are outside of a function address range. For example, func1
starts at address 0x800022B0 and returns at address 0x80002310, func2 is called
from func1 and starts at address 0x80002590 and returns at address 0x80002784.
Using a start/stop from 0x800022B0 to 0x80002310 will trace both functions.
Using a range from 0x800022B0 to 0x80002310 will only trace func1.\n
\note
Care should be taken when using the ability to include or exclude tracing
based on a range. There is a requirement with the ETM module that the value in
the second comparator must be greater than the first value. The compiler can
move instructions to different addresses that would cause this requirement to
fail, or result in instructions getting traced that were not expected, when
using symbols or labels instead of actual address values. The following logic
is used for the range:\n
<i>(address >= range start address) AND (address < range end address)</i>
\par
\note
When the <b>etm_config_inst_start</b> and <b>etm_config_inst_end</b>
functions are used, they enable the Trace start/stop logic to enable and
disable tracing. If the start configuration does not occur, or the start
address is not executed, tracing will not get enabled, even though other
configurations such as an instruction address range has been configured.

\par Instruction Tracing
This library does not provide the ability to disable instruction tracing, the
current implementation on the cortexA8 does not support data only tracing. This
results in the ability to trace instructions with data or without data. Unless
specified as a data related API function, the function is assumed to be an
instruction related configuration.

\par Data Tracing
Tracing data addresses can be globally enabled or disabled for each trace
session as well as filtering which data addresses are traced. Data addresses
will only get traced while an instruction address trace session is active. The
value(s) that are set for the data address or address range can be either the
actual data address(es) or the instruction address(es) that is/are executing the
data read or write.

\par Devices supported by the library are:

 - CortexA8 (Netra/Appleton)

\n\n
\par ETM Library Revision History
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 0.2 </TD> <TD> 8/26/2011 </TD>
                                <TD> ETM Draft API Release (no code)  </TD>
<TR><TD> 0.3 </TD> <TD> 10/21/2011 </TD>
                                <TD> ETMLib API Code Release </TD>
</TABLE>

\par Conventions
The following public naming conventions are used by the API
\li etm_ - Prefix for public ETM Library functions, enumerations,
            and structures
\li ETM_ - Prefix for public ETM enumeration values 

\par Directory Structure
ETM is a component of cTools so it resides within the cTools directory structure.
@code
|-- CToolsLib\
  |
  +-- ETMLib\
  | |
  | +-- doc\
  | | +-- html\
  | | | +-- index.html  - The HTML index page for the "ETM API Referenece"
  | | | +-- . . .
  | | +-- etm_doc.doxy  - Doxygen configuration file to generate the Guide
  | |
  | +-- include\  (Public API include file)
  | +-- lib\      (Library API file)
  | +-- src\      (Private .c and .h files )
  | +-- projects\ (Target specific library builds)
  | | +-- A8\
  |
  +-- Examples\     (Target specific stand-alone example projects)
  | +-- common\   (files common across examples)
  | | +-- cToolsHelper.c  - helper functions used in examples
  | | +-- . . .
  | |
  | +-- C6A816x\  (Netra A8 based examples)
  | | |
  | | +-- ETM_Ex_A8\
  | |
  |
@endcode

\par Helper Functions
Helper functions allow the library to be ported easily to different operating
environments without modifications to the library directly. Within the library,
these functions are declared extern so the library will build but can not be
linked without implementations for the following helper functions:

\li void *cTools_memAlloc(size_t sizeInBytes); \n\n
If the allocation is successful, returns a pointer to the allocated memory,
else, if not successful, returns NULL.

\li void *cTools_memMap(unsigned int phyAddr, unsigned int mapSizeInBytes);\n\n
Map a physical address to a virtual address space. Return the virtual pointer if
successful, else return NULL. Return phyAddr if virtual mapping not required.   

\li void cTools_memFree(void * ptr); \n\n
Free the memory at the ptr (returned from cTools_memAlloc()) call. 

\li void cTools_memUnMap(void * vAddr, unsigned int mapSizeInBytes); \n\n
Unmap virtual address space starting at vAddr (returned from cTools_memMap())of
mapSizeInBytes length. If virtual address mapping not required simply return.    

*/
