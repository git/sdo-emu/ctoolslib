/*
 * ITM Library - itm.c
 *
 * ITM Example public functions.
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "itm.h"
#include <stdint.h>

/*******************************************************************
 * Constants
 *******************************************************************/
const uint32_t ITM_BASE_ADDRESS = 0xE0000000;

const unsigned ITM_STRING_PORT = 0;
const unsigned ITM_BINARY_PORT_MIN = 1;
const unsigned ITM_BINARY_PORT_MAX = 31;

/* Note - This library was developed with a LM4F232 running at 16 Mhz
 * and a JTAG TCK of 10Mhz. Under these conditions the maximum ITM
 * FIFO full retry count was 5. The ITM_MAX_RETRY count was set to
 * accommodate the minimum XDS2xx JTAG clock rate (.5 Mhz). You may
 * want to decrease this value if you consistently run with a higher
 * JTAG TCK rate, or the value may need to be increased if the device
 * functional rate is increased.
 *
 * If you do not enable the ITM module (through CCS) the ITM FIFO
 * will always indicate empty, thus you can leave ITM library calls in
 * your code without any penalty beyond the cycles it takes to execute
 * the ITM functions.
 */
const unsigned ITM_MAX_RETRY = 120;


const unsigned ITM_MAX_STRING_SIZE = 256;

/*******************************************************************
 * Typedefs
 *******************************************************************/
typedef volatile uint32_t * ITM_port_addr32_t;
typedef volatile uint16_t * ITM_port_addr16_t;
typedef volatile uint8_t * ITM_port_addr8_t;

/*******************************************************************
 * Statics
 *******************************************************************/
eITM_Error last_real_error = eITM_Success;

#ifdef DEBUG
static uint32_t port_write_cnt = 0;
static uint32_t max_retry_value = 0;
static uint32_t max_retry_hit = 0;
#endif

/*******************************************************************
 * Private Functions
 *******************************************************************/

/* If the ITM FIFO is full, wait for up to ITM_MAX_RETRY */
static eITM_Error port_wait(ITM_port_addr32_t port)
{
	/* Wait for fifo not full */
	uint32_t retry = 0;
	while (*port == 0) {
		if (retry++ > ITM_MAX_RETRY) {
			return eITM_Error_FIFOBusy;
		}
	}

/* If this is a debug version, save the max retry count */
#ifdef DEBUG
	port_write_cnt++;

	if (retry == max_retry_value) max_retry_hit++;

    if (retry > max_retry_value) {
        max_retry_value = retry;
        max_retry_hit = 0;
    }

#endif

	return  eITM_Success;
}

/* If the previous error is eITM_Success, set the new error */
static void set_error(eITM_Error error) {
    if (last_real_error ==  eITM_Success) {
        last_real_error = error;
    }
}

/*******************************************************************
 * Public Functions
 *******************************************************************/
/* Provide the first error since the last time this function was called */
eITM_Error ITM_getFirstError(void)
{
    eITM_Error error = last_real_error;
    last_real_error = eITM_Success;
    return error;
}

/* Send a null terminated string to the port */
eITM_Error ITM_put_string(const char* data)
{
    ITM_port_addr32_t port_addr = (ITM_port_addr32_t)ITM_BASE_ADDRESS + ITM_STRING_PORT;
	unsigned datapos  = 0;
	unsigned portpos  = 0;
	uint32_t portdata = 0;
	unsigned portcnt  = 0;
	unsigned data_end_sent = 0;

	while (!data_end_sent)
	{
		eITM_Error err = port_wait(port_addr);
		if (err != eITM_Success) {
		    set_error(err);
		    return err;
		}

		/* Reset port data */
		portdata = 0;

		/* Get the next 4 bytes of data */
		for (portpos=0; portpos<4; ++portpos) {
			portdata |= data[datapos] << (8*portpos);
			if ('\0' != data[datapos]) {
				++datapos;
				portcnt++;
			} else {
			    data_end_sent = 1;
			    break;
			}
		}

		/* Write the next 4 bytes of data */
		*port_addr = portdata;

		/* This check prevents the while loop from cycling through
		 * memory until a '\0' is found, in case the buffer (or
		 * pointer) is trashed.
		 */
		if (portcnt >= ITM_MAX_STRING_SIZE) {
		    /* Terminate the current message */
	        eITM_Error err = port_wait(port_addr);
	        if (err != eITM_Success) {
	            set_error(err);
	            return err;
	        }

	        *port_addr = 0;

	        set_error(eITM_Error_MaxStringExceeded);
	        return (eITM_Error_MaxStringExceeded);
		}
	} /* End of the while loop */

	return eITM_Success;
}


/* Send a 32 bit value to the port */
eITM_Error ITM_put_32(ITM_port_t port, uint32_t data)
{
    eITM_Error err;
    ITM_port_addr32_t port_addr;

    if ((port < ITM_BINARY_PORT_MIN) || (port > ITM_BINARY_PORT_MAX)) {
        set_error(eITM_Error_PortInvalidForOp);
        return eITM_Error_PortInvalidForOp;
    }

    port_addr = (ITM_port_addr32_t)(ITM_BASE_ADDRESS + (4*port));

    err = port_wait(port_addr);
    if (err != eITM_Success) {
        set_error(err);
        return err;
    }

	*port_addr = data;

    return eITM_Success;
}


/* Send a 16 bit value to the port */
eITM_Error ITM_put_16(ITM_port_t port, uint16_t data)
{
    eITM_Error err;
    ITM_port_addr16_t port_addr;

    if ((port < ITM_BINARY_PORT_MIN) || (port > ITM_BINARY_PORT_MAX)) {
        set_error(eITM_Error_PortInvalidForOp);
        return eITM_Error_PortInvalidForOp;
    }

    /* Cast port for 16-bit data */
    port_addr = (ITM_port_addr16_t)(ITM_BASE_ADDRESS + (4*port));

    err = port_wait((ITM_port_addr32_t)port_addr);
    if (err != eITM_Success) {
        set_error(err);
        return err;
    }

	*port_addr = data;

    return eITM_Success;
}


/* Send a 8 bit value to the port */
eITM_Error ITM_put_8(ITM_port_t port, uint8_t data)
{
    eITM_Error err;
    ITM_port_addr8_t port_addr;

    if ((port < ITM_BINARY_PORT_MIN) || (port > ITM_BINARY_PORT_MAX)) {
        set_error(eITM_Error_PortInvalidForOp);
        return eITM_Error_PortInvalidForOp;
    }

    /* Cast port for 8-bit data */
    port_addr = (ITM_port_addr8_t)(ITM_BASE_ADDRESS + (4*port));

    err = port_wait((ITM_port_addr32_t)port_addr);
    if (err != eITM_Success) {
        set_error(err);
        return err;
    }

	*port_addr = data;

    return eITM_Success;
}
