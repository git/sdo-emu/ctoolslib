/*
 * ITM Library - itm.h
 *
 * ITM Example public functions.
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef __ITM_H
#define __ITM_H

#ifdef __cplusplus
extern "C" {
#endif

/*! \file itm.h
    ITM Library Function Prototypes
*/
/*! \mainpage
 
    \section intro Introduction
    
    The ITM Library provides an ITM Transport API for string and binary data logging.
    The ITM module provides 32 ports to transport data over. CCS is utilized to configure
    the ITM module and SWO Trace export (see \ref export below for details). CCS restricts
    string messages to port 0, while all other ports may be used for binary data (32, 16,
    or 8 bit values).
    
    This library will work with any M3/M4 devices with an ITM module. The device and XDS
    used must also support SWD (Serial Wire Debug) with SWO (Serial Wire Output) Trace.
    Both XDS2xx and XDSICDI Emulators support SWD with SWO Trace.
    
    \note 
 
    If CCS is not used to configure the device for SWO Trace export (see \ref export
    below for details) the ITM FIFO full check performed by all ITM transport calls will
    always indicate empty and the data written to the ITM ports discarded. Thus, when not
    using CCS there is no penalty beyond the cycles it takes to execute the ITM functions.
    
    \par ITM Library Revision History
    
<TABLE>
<TR><TD> Revision </TD> <TD>Date</TD> <TD>Notes</TD></TR>
<TR><TD> 1.0 </TD> <TD> 1/7/2011 </TD> <TD> First release  </TD>
<TR><TD> 1.1 </TD> <TD> 9/29/2014 </TD> <TD> Added missing CCS project files  </TD>
</TABLE>
    
    \section conv Build Notes
    
    \par Conventions
    
    The following public naming conventions are used by the API
    \li ITM_ - Prefix for public ITM Library functions, typedefs and defines
    \li eITM - Prefix for public ITM Library enumerations 
    
    \par Pre-defined Symbols
    
    The ITM Library supports the following pre-defined symbols that if defined
    enable the functionality at compile time.  

    \li DEBUG - If defined, additional functionality is included to provide debug assistance.
    Static values are provided for the number of writes to the ITM ports, the max number of retries
    required before the ITM port's FIFO full bit was clear, and the number of times the max
    retries occured. See the source for more details.

    \par Directory Structure
    
    ITM is a component of cTools so it resides within the cTools directory structure.
    
    @code
    
    |--cTools
       |
       |-- ITMLib
       |   |
       |   |-- doc
       |   |   |
       |   |   |--html
       |   |      |
       |   |      |-index.html (Doxygen API documentation)
       |   |     
       |   |-- include  (Public API include file)
       |   |-- src      (Private .c and .h files )
       |   |-- projects (Target specific library builds)
       |   |   |
       |   |   |--M3   (Will work with M3 and M4 devices)
       |-- Examples     (Target specific stand-alone example projects)
           |
           |--common    (files common across examples)
           |--LM4F232
              |-- ITM_Ex            
     
    @endcode

    \section export ITM Export Notes
    
    CCS must be used to configure ITM and SWO Trace export. The minimum revision of CCS and
    emupack that supports ITM and SWO Trace export are CCSv5.5 and emupack 5.1.340.0. Also,
    if the XDS firmware is not at the proper revision level, CCS will generate an error when
    you try to use ITM and SWO Trace. The XDS2xx firmware needs to be at least version 1.0.0.5
    for ITM and SWO Trace export support. Check with your specific XDS vendor for firmware update
    instructions if necessary. 
    
    When using CCS to capture ITM data (with either an XDS2xx or XDSICDI) you need to configure
    the ITM module and SWO Trace through CSS 5.5's "Tools->Hardware Trace Analyzer->Custom Core Trace"
    configuration window. The Transport type by default should be "SWO Trace". 
    Simply hit the Start button to automatically configure the connected device's ITM module and SWO Trace,
    and to bring up the Trace Viewer window. The Trace Viewer window is updated every time the core is halted.  

*/

#include <stdint.h>

/*! \par ITM_port_t
    ITM port typedef.
    
    Note: Port 0 is reserved for string data, while binary values can be exported over ports 1 to 31.
    
*/
typedef unsigned ITM_port_t;

// Note - the version definitions must have the end of line immediately after the value (packaging script requirement)
#define ITMLIB_MAJOR_VERSION        (0x1)
                                                /*!< Major version number - Incremented for API changes*/
#define ITMLIB_MINOR_VERSION        (0x1)
                                                /*!< Minor version number - Incremented for bug fixes  */
/*! \par eITM_Error
    ITM Library error codes
*/
typedef enum { 
    eITM_Success = 0,                           /*!< Function completed successfully  */
    eITM_Error_PortInvalidForOp = -1,           /*!< Port Invalid For Op - This typically means the calling
                                                 *   function attempted to write a binary value to a port outside
                                                 *   the range of valid binary ports (1 to 31).
                                                 */   
    eITM_Error_FIFOBusy = -2,                   /*!< FIFO Busy - Retry count exceeded */
    eITM_Error_MaxStringExceeded = -3           /*!< Max string length exceeded */
}eITM_Error;

/*! \par ITM_NUM_PORTS
    Number of ITM transport ports.   
*/
#define ITM_NUM_PORTS 32                        /*!< There are 32 transport ports.  */

/*! \par ITM_getFirstError

    Get the first error to occur since the last time ITM_getFirstError was called.  
    
    \return ::eITM_Error            See ::eITM_Error for individual error details:
    
    \par Details:
        
    For every transport function (ITM_put_n), the first error that occurs is saved. When ITM_getFirstError
    is called the saved error is returned, and then cleared allowing the next error to be saved. If no errors
    have occurred eITM_Success is returned. This allows the user to make a series of ITM transport calls and
    then check for errors once, after the series is complete.
     
*/
eITM_Error ITM_getFirstError(void);

/*! \par ITM_put_string

    Transport a string.  
    
    \param[in] data               Const char pointer to a NULL terminated string to be transported.

    \return ::eITM_Error          The following errors may be returned:
                                  \li ::eITM_Success
                                  \li ::eITM_Error_FIFOBusy
                                  \li ::eITM_Error_MaxStringExceeded
    
    \par Details:
        
    Each byte of the string is transported over ITM port 0. A string may contain sub-strings,
    each of which is terminated with a newline. Each substring will get a time-stamp.
    If the string length exceeds 256 characters, the string is terminated and the
    function returns ::eITM_Error_MaxStringExceeded.
    
    Note:  ITM_getFirstError() may also be used to check for errors after a series of ITM_put calls,
    rather than checking the error returned from every call to ITM_put_string.
     
*/
eITM_Error ITM_put_string(const char* data);

/*! \par ITM_put_32

    Transport a 32-bit value.  
    
    \param[in] port               An ITM port in the range of 1 to 31 (port 0 reserved for strings)
    \param[in] data               32-bit value to be transported.

    \return ::eITM_Error          The following errors may be returned:
                                  \li ::eITM_Success
                                  \li ::eITM_Error_PortInvalidForOp
                                  \li ::eITM_Error_FIFOBusy
    
    \par Details:
        
    The 32-bit data value is transported over the selected ITM port. 
    
    Note:  ITM_getFirstError() may also be used to check for errors after a series of ITM_put calls,
    rather than checking the error returned from every call to ITM_put_32.
     
*/
eITM_Error ITM_put_32(ITM_port_t port, uint32_t data);

/*! \par ITM_put_16

    Transport a 16-bit value.  
    
    \param[in] port               An ITM port in the range of 1 to 31 (port 0 reserved for strings)
    \param[in] data               16-bit value to be transported.

    \return ::eITM_Error          The following errors may be returned:
                                  \li ::eITM_Success
                                  \li ::eITM_Error_PortInvalidForOp
                                  \li ::eITM_Error_FIFOBusy
    
    \par Details:
        
    The 16-bit data value is transported over the selected ITM port.
    
    Note:  ITM_getFirstError() may also be used to check for errors after a series of ITM_put calls,
    rather than checking the error returned from every call to ITM_put_16.
     
*/
eITM_Error ITM_put_16(ITM_port_t port, uint16_t data);

/*! \par ITM_put_8

    Transport a 8-bit value.  
    
    \param[in] port               An ITM port in the range of 1 to 31 (port 0 reserved for strings)
    \param[in] data               8-bit value to be transported.

    \return ::eITM_Error          The following errors may be returned:
                                  \li ::eITM_Success
                                  \li ::eITM_Error_PortInvalidForOp
                                  \li ::eITM_Error_FIFOBusy
    
    \par Details:
        
    The 8-bit data value is transported over the selected ITM port.
    
    Note:  ITM_getFirstError() may also be used to check for errors after a series of ITM_put calls,
    rather than checking the error returned from every call to ITM_put_8.
     
*/
eITM_Error ITM_put_8(ITM_port_t port, uint8_t data);

#ifdef __cplusplus
}
#endif

#endif /* __ITM_H */
